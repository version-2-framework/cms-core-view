import { Howl } from "howler";
import SoundController from "../utils/SoundController";
import noop from "../../../cms-core-logic/src/utils/noop";
import { getGameViewLogger } from "../../../cms-core-logic/src/logging/LogManager";
import RngSingleton from "../../../cms-core-logic/src/maths/RngSingleton";

const log = getGameViewLogger();

/**
 * Default Spin Sound Facade. This is most appropriate for generic games, where there
 * are no FreeSpins or Respins (or no requirement to play very specific sounds on
 * specific spins or scenarios).
 * @implements {SlotSpinSoundController}
 */
export class ConfigurableSpinSoundController
{
    /**
     * Constructs a new Configurable SpinSoundController. The config param is an object
     * which specifies how the controller that is built should behaves for each of the
     * play sound methods.
     * @param {ConfigurableSpinSoundConfig} config 
     * @param {SoundController} soundController 
     */
    constructor(config, soundController)
    {
        /**
         * @private
         * @type {SoundController}
         */
        this._soundController = soundController;

        /**
         * @type {ConfigurableSpinSoundConfig}
         * @private
         */
        this._soundConfig = config;

        /**
         * @private
         * @type {PlayGameIdleSound}
         */
        this._playGameIdleSound = createGameIdleSound(
            this._soundConfig.gameIdleSound, this._soundController);

        /**
         * @private
         * @type {PlayGameIdleWinSequenceSound}
         */
        this._playGameIdleWinSequenceSound = createGameIdleWinSequenceSound(
            this._soundConfig.gameIdleWinSequenceSound, this._soundController);

        /**
         * @private
         * @type {PlaySpinAnticipationSound}
         */
        this._playSpinAnticipationSound = createSpinAnticipationSound(
            this._soundConfig.spinAnticipationSound, this._soundController);

        /**
         * @private
         * @type {GetReelRollSound}
         */
        this._getReelRollSound = createGetReelRollSoundMethod(
            this._soundConfig.reelRollSoundConfig, this._soundController);
        
        /**
         * The method which will play reel start sounds.
         * @private
         * @type {PlayReelStartSound}
         */
        this._playReelStartSound = createPlayReelStartSoundMethod(
            this._soundConfig.reelStartSoundConfig, this._soundController);

        /**
         * The method which will play reel stop sounds.
         * @private
         * @type {PlayReelStopSound}
         */
        this._playReelStopSound = createPlayReelStopSoundMethod(
            this._soundConfig.reelStopSoundConfig, this._soundController);

        /**
         * The method which will play Symbol Win sounds.
         * @private
         * @type {PlaySymbolWinSound}
         */
        this._playSymbolWinSound = createPlaySymbolWinSoundMethod(
            this._soundConfig.symbolWinSoundConfig, this._soundController);

        /**
         * The method which will play a Spin 2 Idle sound.
         * @private
         * @type {PlaySpin2IdleSound}
         */
        this._playSpin2IdleSound = createPlaySpin2IdleSoundMethod(
            this._soundConfig.spin2IdleSounds, this._soundController);
    };


    /**
     * @public
     * @inheritDoc
     */
    playGameIdleSound()
    {
        log.debug(`SpinSoundController.playGameIdleSound()`);
        this._playGameIdleSound();
    };


    /**
     * @public
     * @inheritDoc
     */
    playGameIdleWinSequenceSound()
    {
        log.debug(`SpinSoundController.playGameIdleWinSequenceSound()`);
        this._playGameIdleWinSequenceSound();
    };


    /**
     * @public
     * @inheritDoc
     * @param {SpinPhaseType} spinPhaseType 
     * @param {number} spinRoundIndex 
     * @param {number} spinIndex 
     */
    playSpinAnticipationSound(spinPhaseType, spinRoundIndex, spinIndex) {
        log.debug(`SpinSoundController.playSpinAnticipationSound(spinPhaseType:${spinPhaseType},spinRoundIndex:${spinRoundIndex},spinIndex:${spinIndex})`);
        this._playSpinAnticipationSound(spinPhaseType, spinRoundIndex, spinIndex);
    };


    /**
     * @public
     * @inheritDoc
     * @param {number} reelIndex
     * @param {number} numReelsAlreadySpun 
     */
    playReelStartSound(reelIndex, numReelsAlreadySpun) {
        log.debug(`SpinSoundController.playReelStartSound(reelIndex:${reelIndex},numReelsAlreadySpun:${numReelsAlreadySpun})`);
        this._playReelStartSound(reelIndex, numReelsAlreadySpun);
    };


    /**
     * @public
     * @inheritDoc
     * @param {number} reelIndex 
     * @return {ReelRollSound}
     */
    getReelRollSound(reelIndex) {
        return this._getReelRollSound(reelIndex);
    };


    /**
     * @public
     * @inheritDoc
     * @param {SymbolConfig[]} symbols
     * @param {number} reelIndex
     * @param {number} numReelsAlreadyStopped
     * @param {SpinResultPresentation} presentation
     */
    playReelStopSound(symbols, reelIndex, numReelsAlreadyStopped, presentation) {
        log.debug(`SpinSoundController.playReelStopSound(reelIndex:${reelIndex},numReelsAlreadyStopped:${numReelsAlreadyStopped})`);
        this._playReelStopSound(symbols, reelIndex, numReelsAlreadyStopped, presentation);
    };


    /**
     * @public
     * @inheritDoc
     * @param {SymbolWinPresentation} winPresentation 
     * @param {boolean} isQuickWin 
     * @param {number} progressiveIndex
     */
    playSymbolWinSound(winPresentation, isQuickWin, progressiveIndex) {
        log.debug(`SpinSoundController.playSymbolWinSound(isQuickWin:${isQuickWin}, progressiveIndex:${progressiveIndex})`);
        this._playSymbolWinSound(winPresentation, isQuickWin, progressiveIndex);
    };


    /**
     * @public
     * @inheritDoc
     * @param {SymbolConfig[][][]} symbols 
     */
    playSpin2IdleSound(symbols) {
        log.debug(`SpinSoundController.playSpin2IdleSound()`);
        this._playSpin2IdleSound(symbols);
    };
};


//--------------------------------------------------------------------------------------------------
// Factory methods for creating "play game idle sound" methods
//--------------------------------------------------------------------------------------------------
/**
 * Factory method for creating a simple Play Game Idle Sound method.
 * @param {GameIdleSoundConfig} config 
 * @param {SoundController} soundController 
 * @return {PlayGameIdleSound}
 */
function createGameIdleSound(config, soundController)
{
    if (!config) {
        return noop;
    }

    /** @type {PlayGameIdleSound} */
    let playSoundMethod = null;

    if (config.type === "none") {
        playSoundMethod = noop;
    }
    else
    if (config.type === "random") {
        playSoundMethod = () =>
        {
            let soundId = config.soundIds[RngSingleton.getRandom(config.soundIds.length)];
            soundController.playSound(soundId, false, config.soundVolume);
        }
    }

    return playSoundMethod;
};


//--------------------------------------------------------------------------------------------------
// Factory methods for creating "play game idle win sequence sound" methods
//--------------------------------------------------------------------------------------------------
/**
 * Factory method for creating a Play Game Idle Win Sequence Sound method.
 * @param {GameIdleWinSequenceSoundConfig} config 
 * @param {SoundController} soundController 
 * @return {PlayGameIdleWinSequenceSound}
 */
function createGameIdleWinSequenceSound(config, soundController)
{
    if (!config) {
        return noop;
    }

    /** @type {PlayGameIdleWinSequenceSound} */
    let playSoundMethod = null;

    if (config.type === "none") {
        playSoundMethod = noop;
    }
    else
    if (config.type === "random") {
        playSoundMethod = () =>
        {
            let soundId = config.soundIds[RngSingleton.getRandom(config.soundIds.length)];
            soundController.playSound(soundId, false, config.soundVolume);
        }
    }

    return playSoundMethod;
};


//--------------------------------------------------------------------------------------------------
// Factory methods for creating "play spin anticipation sounds" methods
//--------------------------------------------------------------------------------------------------
/**
 * Factory method for the Spin Anticipation Sound Player. If the config supplied is null or
 * undefined, then a noop method will be returned instead.
 * @param {SpinAnticipationSoundConfig | null} config
 * @param {SoundController} soundController
 * @return {PlaySpinAnticipationSound}
 */
function createSpinAnticipationSound(config, soundController)
{
    if (!config) {
        return noop;
    }

    /** @type {PlaySpinAnticipationSound} */
    let playSoundMethod = null;

    if (config.type === "simple")
    {
        let soundId = config.soundId;
        let soundVolume = config.soundVolume;
        playSoundMethod = (spinPhaseType, spinRoundIndex, spinIndex) => {
            soundController.playSound(soundId, false, soundVolume);
        };
    }
    else
    if (config.type === "looping")
    {
        let currSoundIndex = 0;
        playSoundMethod = (spinPhaseType, spinRoundIndex, spinIndex) =>
        {
            let soundId = config.sounds[currSoundIndex];
            let loopSound = false;
            let volume = config.soundVolume;

            currSoundIndex += 1;
            if (currSoundIndex >= config.sounds.length) {
                currSoundIndex = 0;
            }

            soundController.playSound(soundId, loopSound, volume);
        }
    }
    else
    if (config.type === "random")
    {
        playSoundMethod = (spinPhaseType, spinRoundIndex, spinIndex) =>
        {
            let numSounds = config.sounds.length;
            let randomIndex = RngSingleton.getRandom(numSounds);
            let soundId = config.sounds[randomIndex];
            let volume = config.soundVolume;

            soundController.playSound(soundId, false, volume);
        }
    }

    return playSoundMethod;
};


//--------------------------------------------------------------------------------------------------
// Methods for creating "play reel start sound"
//--------------------------------------------------------------------------------------------------
// TODO: Wieners original PIDSpinner actually loops a reel start sound, and tweens it upwards in
// volume. This is either a variation of the reel start sound functionality, or is separate: it
// can probably be specified for all "spinning" spinners.
/**
 * 
 * @param {ReelStartSoundConfig} config 
 * @param {SoundController} soundController 
 * @return {PlayReelStartSound}
 */
function createPlayReelStartSoundMethod(config, soundController)
{
    if (!config) {
        return noop;
    }

    /** @type {PlayReelStartSound} */
    let playStartSoundMethod = null;

    if (config.type === "none")
    {
        playStartSoundMethod = noop;
    }
    else
    if (config.type === "simple")
    {
        playStartSoundMethod = (reelIndex, numReelsAlreadySpun) =>
        {
            soundController.playSound(config.soundId, false, config.soundVolume);
        }
    }
    else
    if (config.type === "perReel")
    {
        playStartSoundMethod = (reelIndex, numReelsAlreadySpun) =>
        {
            let soundId = `${config.soundPrefix}${(reelIndex+1).toString().padStart(config.padLength,"0")}`;
            soundController.playSound(soundId, false, config.soundVolume);
        }
    }
    else
    if (config.type === "progressive")
    {
        playStartSoundMethod = (reelIndex, numReelsAlreadySpun) =>
        {
            let soundId = `${config.soundPrefix}${(numReelsAlreadySpun+1).toString().padStart(config.padLength,"0")}`;
            soundController.playSound(soundId, false, config.soundVolume);
        }
    }

    return playStartSoundMethod;
};


//--------------------------------------------------------------------------------------------------
// Methods for creating "get reel roll sound"
//--------------------------------------------------------------------------------------------------
/**
 * Creates a GetReelRollSound method. If config is null or undefined, then a noop method
 * will be returned.
 * @param {ReelRollSoundConfig} config 
 * @param {SoundController} soundController 
 * @return {GetReelRollSound}
 */
function createGetReelRollSoundMethod(config, soundController)
{
    if (!config) {
        return () => null;
    }

    /** @type {GetReelRollSound} */
    let getReelRollSoundMethod = null;

    if (config.type === "none") {
        getReelRollSoundMethod = () => null;
    }
    else
    if (config.type === "simple") {
        getReelRollSoundMethod = reelIndex =>
        {
            return { soundId:config.soundId, maxVolume:config.maxVolume };
        }
    }
    else
    if (config.type === "perReel") {
        getReelRollSoundMethod = reelIndex =>
        {
            return {
                soundId : `${config.soundPrefix}${(reelIndex+1).toString().padStart(config.padLength,"0")}`,
                maxVolume : config.maxVolume
            }
        }
    }

    return getReelRollSoundMethod;
};


//--------------------------------------------------------------------------------------------------
// Methods for creating "play reel stop sounds"
//--------------------------------------------------------------------------------------------------
/**
 * Creates a method which will play reel stop sounds. If the config supplied is null,
 * then a noop method will be returned.
 * @param {PrimaryReelStopSoundConfig | null} config 
 * @param {SoundController} soundController 
 * @return {PlayReelStopSound}
 */
function createPlayReelStopSoundMethod(config, soundController)
{
    if (!config) {
        return noop;
    }

    let playDefaultReelStopSoundMethod =
        createSingleReelStopSoundMethod(config.standardReelStopSound, soundController);

    /**
     * The actual method to return. By default, this is the "play reel stop sound" method,
     * but may be overriden by the more advanced "check for special symbols, or else play
     * standard reel stop sound" method.
     * @type {PlayReelStopSound}
     */
    let playStopSound = playDefaultReelStopSoundMethod;

    if (config.specialSymbolIds && config.specialSymbolStopSounds)
    {
        if (config.specialSymbolIds.length != config.specialSymbolStopSounds.length)
        {
            // TODO: Issue a warning
        }
        else
        {
            let specialSymbolIds = config.specialSymbolIds;
            let specialSymbolStopSounds = config.specialSymbolStopSounds;

            /** @type {{[id:number]:PlayReelStopSound}} */
            let soundMap = {};

            /** @type {{[id:number]:ReelStopSoundFilter}} */
            let filterMap = {};

            for (let specialSymSoundIndex = 0; specialSymSoundIndex < specialSymbolIds.length; specialSymSoundIndex ++)
            {
                let specialSymbolId = specialSymbolIds[specialSymSoundIndex];
                let specialSymbolSoundConfig = specialSymbolStopSounds[specialSymSoundIndex];
                let playSoundMethod = createSingleReelStopSoundMethod(specialSymbolSoundConfig, soundController);

                soundMap[specialSymbolId] = playSoundMethod;

                if (specialSymbolSoundConfig.filter) {
                    filterMap[specialSymbolId] = specialSymbolSoundConfig.filter;
                }
            }

            // The actual method first checks if any special symbol id exists on the reels.
            // The first, most high priority symbol id found, is picked as the special sound
            // that will be played. If no such sound is played, we fall back to the default
            // "stop reel sound".
            playStopSound = (symbols, reelIndex, numReelsAlreadyStopped, spinPresentation) =>
            {
                let playStopSoundForReel = null;

                for (let specialSymbolIndex = 0; specialSymbolIndex < specialSymbolIds.length; specialSymbolIndex ++)
                {
                    let specialSymbolId = specialSymbolIds[specialSymbolIndex];

                    for (let symIndex = 0; symIndex < symbols.length; symIndex ++) {
                        if (symbols[symIndex].id === specialSymbolId) {
                            // This is a symbol we have a special sound for. Now, check if there is
                            // a custom filter: if there is, check if we are allowed to play the
                            // sound: if not, we can play the sound, no problem.
                            if (filterMap[specialSymbolId] === undefined || 
                                filterMap[specialSymbolId](symbols, reelIndex, numReelsAlreadyStopped, spinPresentation) === true)
                            {
                                playStopSoundForReel = soundMap[specialSymbolId];
                                break;
                            }
                        }
                    };

                    if (playStopSoundForReel !== null) {
                        break;
                    }
                };

                if (playStopSoundForReel === null) {
                    playStopSoundForReel = playDefaultReelStopSoundMethod;
                }

                playStopSoundForReel(symbols, reelIndex, numReelsAlreadyStopped);
            }
        }
    }

    return playStopSound;
};


/**
 * Factory class, to get a method which will play a specific reel stop sound.
 * @param {ReelStopSoundConfig} config 
 * @param {SoundController} soundController 
 * @return {PlayReelStopSound}
 */
function createSingleReelStopSoundMethod(config, soundController)
{
    if (!config) {
        return noop;
    }

    /**
     * The method which will explicitly play the reel stop sound (and nothing more)
     * @type {PlayReelStopSound}
     */
    let playStopSoundMethod = null;

    if (config.type === "none")
    {
        playStopSoundMethod = noop;
    }
    else
    if (config.type === "simple")
    {
        playStopSoundMethod = () =>
        {
            soundController.playSound(config.soundId, false, config.soundVolume);
        };
    }
    else
    if (config.type === "perReel")
    {
        if (config.soundPrefix)
        {
            playStopSoundMethod = (symbols, reelIndex, numReelsAlreadyStopped) =>
            {
                let soundId = `${config.soundPrefix}${(reelIndex+1).toString().padStart(config.padLength,"0")}`;
                soundController.playSound(soundId, false, config.soundVolume);
            };
        }
        else
        if (config.soundIds)
        {
            let soundIds = config.soundIds;
            playStopSoundMethod = (symbols, reelIndex, numReelsAlreadyStopped) =>
            {
                let soundId = soundIds[reelIndex];
                soundController.playSound(soundId, false, config.soundVolume);
            }
        }
    }
    else
    if (config.type === "progressive")
    {
        if (config.soundPrefix)
        {
            playStopSoundMethod = (symbols, reelIndex, numReelsAlreadyStopped) =>
            {
                let soundId = `${config.soundPrefix}${(numReelsAlreadyStopped+1).toString().padStart(config.padLength,"0")}`;
                soundController.playSound(soundId, false, config.soundVolume);
            }
        }
        else
        if (config.soundIds)
        {
            playStopSoundMethod = (symbols, reelIndex, numReelsAlreadyStopped) =>
            {
                let soundId = numReelsAlreadyStopped < config.soundIds.length?
                    config.soundIds[numReelsAlreadyStopped] : config.soundIds[config.soundIds.length - 1];
                soundController.playSound(soundId, false, config.soundVolume);
            }
        }
    }

    return playStopSoundMethod;
};


//--------------------------------------------------------------------------------------------------
// Methods for creating "play symbol win sounds"
//--------------------------------------------------------------------------------------------------
// TODO: This method needs to take "isQuickWin" into account properly. 
// Currently, we play a single sound, and we play it only on "quick win"
/**
 * Creates a method which will play Symbol Win sounds. If the config supplied is null or
 * undefined, then a noop method will be returned instead.
 * @param {SymbolWinSoundConfig | null} config 
 * @param {SoundController} soundController 
 * @return {PlaySymbolWinSound}
 */
function createPlaySymbolWinSoundMethod(config, soundController)
{
    if (!config) {
        return noop;
    }

    /** @type {PlaySymbolWinSound} */
    let playSymbolWinSound = null;

    if (config.type === "simple")
    {
        playSymbolWinSound = (winPresentation, isQuickWin) =>
        {
            if (isQuickWin || (!isQuickWin && config.useInIdleWin))
            {
                // Set default sound id.
                let soundId = config.defaultSoundId;

                // Default soundId can be overriden with a custom special symbol id.

                /** @type {SymbolConfig} */
                let symbol;

                // We first check for any symbolId specified in "multiHandMetaData" (which, if present,
                // takes explicit priority). If not present, check the primary hand win for winning symbol
                // data.
                if (winPresentation.multiHandMetaData &&
                    winPresentation.multiHandMetaData.winningSymbol)
                {
                    symbol = winPresentation.multiHandMetaData.winningSymbol;
                }
                else
                if (winPresentation.win &&
                    winPresentation.win.winningSymbol)
                {
                    symbol = winPresentation.win.winningSymbol;
                }

                // Only if we actually found winningSymbolData - and if special symbol sounds are
                // configured - will we override the default sound.
                if (config.specialSymbolSounds && symbol && (symbol.id in config.specialSymbolSounds))
                {
                    soundId = config.specialSymbolSounds[symbol.id];
                }

                soundController.playSound(soundId, false, config.soundVolume);
            }
        }
    }
    else
    if (config.type === "symbolId")
    {
        playSymbolWinSound = (winPresentation, isQuickWin) =>
        {
            if (isQuickWin || (!isQuickWin && config.useInIdleWin))
            {
                /** @type {SymbolConfig} */
                let symbol;

                // If it's a multiHand win, use any supplied meta-data as preference over primary
                // hand win (as primary hand win may not exist...)
                if (winPresentation.multiHandMetaData &&
                    winPresentation.multiHandMetaData.winningSymbol)
                {
                    symbol = winPresentation.multiHandMetaData.winningSymbol;
                }
                else
                if (winPresentation.win &&
                    winPresentation.win.winningSymbol)
                {
                    symbol = winPresentation.win.winningSymbol;
                }

                if (symbol)
                {
                    let symbolId = winPresentation.win.winningSymbol.id;
                    let soundId = `${config.soundPrefix}_${symbolId.toString().padStart(config.padLength,"0")}`;
                    soundController.playSound(soundId, false, config.soundVolume);
                }
                // TODO: Should probably have a default fall-back sound here as the else case
            }
        }
    }
    else
    if (config.type === "winlineWin")
    {
        playSymbolWinSound = (winPresentation, isQuickWin) =>
        {
            if (isQuickWin || (!isQuickWin && config.useInIdleWin))
            {
                let soundId = config.defaultSoundId;

                /** @type {SymbolConfig} */
                let symbol;
                let winlineId = 0;

                // If multihand metadata present, check this for explicit flags.
                // Otherwise, fall back to the primary hand win (if its present).
                // Otherwise, all we can do is play any default sound.
                if (winPresentation.multiHandMetaData) {
                    let metaData = winPresentation.multiHandMetaData;
                    symbol = metaData.winningSymbol;
                    winlineId = metaData.lineId;
                }
                else
                if (winPresentation.win) {
                    symbol = winPresentation.win.winningSymbol;
                    winlineId = winPresentation.win.lineId;
                }
                
                // Special symbol ids will override winline id.
                if (config.specialSymbolSounds && symbol && (symbol.id in config.specialSymbolSounds))
                {
                    soundId = config.specialSymbolSounds[symbol.id];
                }
                else
                if (winlineId > 0)
                {
                    soundId = `${config.winlineSoundPrefix}_${winlineId.toString().padStart(config.padLength,"0")}`;
                }
                else
                {
                    log.debug(`SpinSoundController.playSymbolWinSound: play default sound ${soundId}`);
                }

                soundController.playSound(soundId, false, config.soundVolume);
            }
        }
    }
    else
    if (config.type === "progressive")
    {
        if (config.soundIds)
        {
            let soundIds = config.soundIds;

            playSymbolWinSound = (winPresentation, isQuickWin, progressiveIndex) =>
            {
                if (isQuickWin || (!isQuickWin && config.useInIdleWin))
                {
                    let numSounds = soundIds.length;
                    let soundId = progressiveIndex >= numSounds? soundIds[numSounds - 1] : soundIds[progressiveIndex];


                    /** @type {SymbolConfig} */
                let symbol;

                // We first check for any symbolId specified in "multiHandMetaData" (which, if present,
                // takes explicit priority). If not present, check the primary hand win for winning symbol
                // data.
                if (winPresentation.multiHandMetaData &&
                    winPresentation.multiHandMetaData.winningSymbol)
                {
                    symbol = winPresentation.multiHandMetaData.winningSymbol;
                }
                else
                if (winPresentation.win &&
                    winPresentation.win.winningSymbol)
                {
                    symbol = winPresentation.win.winningSymbol;
                }

                    if (config.specialSymbolSounds && symbol && (symbol.id in config.specialSymbolSounds))
                {
                    soundId = config.specialSymbolSounds[symbol.id];
                }
                    soundController.playSound(soundId, false, config.soundVolume);
                }
            }
        }
        else
        if (config.soundPrefix)
        {
            playSymbolWinSound = (winPresentation, isQuickWin, progressiveIndex) =>
            {
                if (isQuickWin || (!isQuickWin && config.useInIdleWin))
                {
                    let soundId = `${config.soundPrefix}_${progressiveIndex.toString().padStart(config.padLength, "0")}`;
                    soundController.playSound(soundId, false, config.soundVolume);
                }
            }
        }
        else playSymbolWinSound = noop;
    }
    else
    if (config.type === "random")
    {
        playSymbolWinSound = (winPresentation, isQuickWin, progressiveIndex) =>
        {
            if (isQuickWin || (!isQuickWin && config.useInIdleWin))
            {
                let numSounds = config.soundIds.length;
                let soundId = config.soundIds[RngSingleton.getRandom(numSounds)];
                soundController.playSound(soundId, false, config.soundVolume);
            }
        }
    }

    return playSymbolWinSound;
};


//--------------------------------------------------------------------------------------------------
// Create Spin 2 Idle sounds
//--------------------------------------------------------------------------------------------------
/**
 * Creates the "play spin 2 idle sound" method.
 * @param {Spin2IdleSoundConfig | null} config 
 * @param {SoundControllerApi} soundController
 * @return {PlaySpin2IdleSound}
 */
function createPlaySpin2IdleSoundMethod(config, soundController)
{
    if (!config) {
        return noop;
    }

    let defaultSoundMethod = createDefaultSpin2SoundMethod(config.defaultSound, soundController);

    /** @type {PlaySpin2IdleSound} */
    let methodToReturn = defaultSoundMethod;
    
    // Hmm, now i need to work out how "tease sounds" can be configured in a future-proof manor.
    if (config.teaseSounds && config.teaseSounds.length > 0)
    {
        // Build the list of tease sound configs
        let teaseSoundMethods = [];

        config.teaseSounds.forEach(teaseSoundConfig => {
            teaseSoundMethods.push(createSingleIdleTeaseSoundMethod(teaseSoundConfig, soundController));
        });

        // Now we can prepare the method to return
        methodToReturn = (symbols) =>
        {
            // We will cache here, if we triggered a tease sound.
            let teaseSoundWasTriggered = false;
            
            // Execute all tease sound methods, in priority order: however, if any method indicates
            // that it played a sound, we stop the search. We only want to play 1 tease sound, and
            // we want to play the most high priority sound available.
            for (let i = 0; i < teaseSoundMethods.length; i ++) {
                teaseSoundWasTriggered = teaseSoundMethods[i](symbols);
                if (teaseSoundWasTriggered) {
                    break;
                }
            }

            // If no tease sound was triggered, then we play the default idle sound.
            if (!teaseSoundWasTriggered) {
                defaultSoundMethod();
            }
        }
    }
    
    return methodToReturn;
}

/**
 * Creates the method to play a "default" sound for spin 2 idle.
 * @param {Spin2IdleDefaultSoundConfig | null} config 
 * @param {SoundControllerApi} soundController
 * @return {() => void}
 */
function createDefaultSpin2SoundMethod(config, soundController)
{
    let idleSoundMethod = noop;

    if (config) {
        if (config.type === "none") {
            idleSoundMethod = noop;
        }
        else
        if (config.type === "simple") {
            let soundId = config.soundId;
            let soundVolume = config.soundVolume;
            idleSoundMethod = () => {
                soundController.playSound(soundId, false, soundVolume);
            }
        }
    }

    return idleSoundMethod;
}

let playSpin2IdleTeaseSoundNoop = () => false;

/**
 * Creates a single idle tease sound method, from the provided config. The method returned has a slightly
 * special api: because we are using "priority based" triggering, the method both triggers the sound (if
 * required), and returns a boolean to indicate if it played a sound (allowing us to skip any lower
 * priority methods, so we don't play more than one sound).
 * @param {Spin2IdleTeaseSoundConfig} config
 * @param {SoundControllerApi} soundController
 * @return {(symbols:SymbolConfig[][][]) => boolean}
 * A special method, which accepts the same input parameters as a "PlaySpin2IdleSound" method instance,
 * but also returns a boolean. The boolean indicates if the method actually triggered a sound. This is
 * a quick and dirty way for us to perform priority based checks (eg: we execute conditional logic to
 * determine whether we *need* to play a sound) in the same method which also triggers the sound (if we
 * need the results of those logical checks, in order to determine which sound to play, it may be more
 * efficient if we can perform them in a single step - and certainly simpler, logically).
 */
function createSingleIdleTeaseSoundMethod(config, soundController)
{
    if (!config) {
        return playSpin2IdleTeaseSoundNoop;
    }

    /** @type {(symbols:SymbolConfig[][][]) => boolean} */
    let playSoundMethod = playSpin2IdleTeaseSoundNoop;

    // Scatter based rules doesn't mean we look for a scatter symbol: we look for any arbitrary symbol
    // id, but we check against "scatter rules", eg: we simply count however many of the target symbol
    // are found absolutely anywhere on the target hand, and if it exceeds a minimum number, we trigger
    // the sound
    if (config.type === "scatter")
    {
        let targetSymbolId = config.symbolId;
        let minNumSym = config.minNumSym;
        let soundId = config.soundId;
        let soundVolume = config.soundVolume;

        playSoundMethod = (symbols) =>
        {
            let playSound = false;

            for (let handIndex = 0; handIndex < symbols.length; handIndex ++) {
                let symbolsOnHand = symbols[handIndex];
                let numSymFound = 0;

                for (let reelIndex = 0; reelIndex < symbolsOnHand.length; reelIndex ++) {
                    let symbolsOnReel = symbolsOnHand[reelIndex];
                    for (let posIndex = 0; posIndex < symbolsOnReel.length; posIndex ++) {
                        if (symbolsOnReel[posIndex].id === targetSymbolId) {
                            numSymFound += 1;
                        }
                    }
                }

                if (numSymFound >= minNumSym) {
                    playSound = true;
                }

                if (playSound) {
                    break;
                }
            }

            if (playSound) {
                soundController.playSound(soundId, false, soundVolume);
            }

            return playSound;
        }
    }

    return playSoundMethod;
}

