/**
 * Events fired directly by a Big Win animation implementation: these are to be fired
 * on the Big Win view class itself (so they are only for the use of the immediate user
 * of the Big Win View): they are NOT fired globally.
 */
class C_BigWinViewEvent
{
    constructor()
    {
        /**
         * Fired by Big Win view when its animation sequence has completed.
         */
        this.COMPLETE = "BigWinAnim.Complete";
    }
}

export default new C_BigWinViewEvent;