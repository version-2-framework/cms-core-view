class C_TooltipOrientation
{
    constructor()
    {
        /**
         * @public
         * @type {TooltipOrientation}
         */
        this.VERTICAL = "vertical";

        /**
         * @public
         * @type {TooltipOrientation}
         */
        this.HORIZONTAL = "horizontal";
    }
}

export default (new C_TooltipOrientation);