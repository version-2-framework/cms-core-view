// TODO: As of 06.10.2020, this set of constants is completely redundant. All of the
// default sounds listed here, are game specific (to Fowl Play Centurion) - this was
// always an inappropriate way to handle things.

const WARN_MSG = 'This constant is deprecated, and no longer used in library code. Please refactor your game code to use a game constant.';

/**
 * "Names of sounds"
 */
class C_Snd
{
    constructor()
    {
        this.SPIN_BUT_SND = "snd_btn_spinner_click";
        
        /**
         * The id of the default button press sound.
         */
        this.BTN_DOWN = "btn";

        this.FREESPIN_INTRO = "snd_prompt_freespins";
        this.FREESPIN_MUSIC = "snd_freespins_background_loop";
        this.FREESPIN_OUTRO = "snd_prompt_freespins_end";

        this.BIG_WIN_START = "big_win_switch";
        this.BIG_WIN_SWITCH = "big_win_switch";
        this.BIG_WIN_LOOPED = "big_win_looped_";
        this.BIG_WIN_END = "big_win_end_";

        /**
         * Id of the default sound played during Total Winnings animation.
         */
        this.TOTAL_WINNINGS = "total-winnings"

        this.WIN_POPUP = "win-popup";
        this.WIN_LINE = "win-line";
        this.BIG_WIN = "big-win";
    }

    get MUSIC_BONUS() {
        console.warn(`C_Snd.MUSIC_BONUS: ${WARN_MSG}`);
        return "snd_bonusgame_loop";
    }
}

export default (new C_Snd);