/**
 * Standard view events, for use for communicating within the Bonus View layer.
 */
class C_BonusViewEvent
{
    constructor()
    {
        /**
         * The main bonus game has completed, with all winnings shown. Begin the Bonus Outro sequence,
         * at the end of which, control is passed back to Game Controller.
         */
        this.SHOW_BONUS_OUTRO = "BonusView.ShowBonusOutro";

        /**
         * When a Bonus Selector of some kind is shown, and the selection of a Bonus Type has been made,
         * this event signals to begin the Bonus Type selected. As standard, the Bonus Type value will be
         * passed along with this event (BonusType will be a number).
         */
        this.START_SELECTED_BONUS = "BonusView.StartSelectedBonus";

        this.TOTAL_WINNINGS_COMPLETE = "BonusView.TotalWinningsComplete";
    }
}

export default new C_BonusViewEvent();