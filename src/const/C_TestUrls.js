/**
 * Default test urls for the V1 platform.
 * @type {TestServerConfig[]}
 */
export const V1_URLS =
	[
		{
			name : "Localhost connection",
			url : "http://localhost:8080/rest/test/html5/"
		},
		{
			name : "Internal WMG connection",
			url : "http://10.199.0.91:8080/rest/test/html5"
		},
		{
			name : "HTTPS Ext. WMG connection (via DNS)",
			url : "https://test.wmgaming.it:8443/rest/test/html5"
		},
		{
			name : "Ext. WMG connection (via DNS)",
			url : "http://test.wmgaming.it:8080/rest/test/html5"},
		{
			name : "WMG External Connection (via IP)",
			url : "http://89.97.212.74:8080/rest/test/html5"
		},
		{
			name : "CMS Gaming Server",
			url : "http://192.168.0.221:8080/rest/test/html5"
		}
	];

/**
 * Default test urls for the V2 platform.
 * @type {TestServerConfig[]}
 */
export const V2_URLS =
	[
		{
			name:"Localhost connection",
			url:"http://localhost:8080/acs/rest/comms"
		},
		{
			name:"Internal WMG connection",
			url:"http://10.199.0.32:8080/acs/rest/comms"
		},
		{
			name:"HTTPS Ext. WMG connection (via DNS)",
			url:"https://testv2.wmgaming.it:8443/acs/rest/comms"
		},
		{
			name:"Ext. WMG connection (via DNS)",
			url:"http://testv2.wmgaming.it:8080/acs/rest/comms"
		},
		{
			name:"WMG External Connection (via IP)",
			url:"http://89.97.212.74:8080/acs/rest/comms"
		},
		{
			name:"CMS Gaming Server",
			url:"http://192.168.0.221:8080/rest/test/html5"
		}
	];
