/**
 * Events related to game flow in the visual part of the game
 */
class C_ViewEvent
{
    constructor()
    {
        this.CREATE_FULLSCREEN_PROMPT = "CREATE_FULLSCREEN_PROMPT";
        
        /**
         * One-off event, fired each time a reel starts spinning. The reelIndex will be passed
         * as the event data.
         */
        this.SINGLE_REEL_SPIN_STARTED = "Reels.SingleReelSpinStarted";

        // TODO: This needs configuring better....
        /**
         * One-off event, fired each time a reel reaches its "pre-stop" phase. App.config provides
         * a field called preReelStopDelay: this is a configurable time in seconds, and refers to
         * an additional amount of time hat [TODO: define this better, i don't think the implementation
         * is quite correct, because we are basically adding a permanent extra delay, and we might not
         * want it on every spin]
         */
        this.SINGLE_REEL_SPIN_STOP_STARTED = "Reels.SingleReelSpinStopStarted";

        /**
         * Event to indicate that a reel is "settling": this means that it has not stopped, but
         * visually may look as good as dammit stopped (it may basically be in the final position,
         * but settlings / wobbling back and forth). Not all spinners may really support this (if they
         * don't, they can simply dispatch this event immediately before the REEL_SPIN_STOP_COMPLETE
         * event). Generally, this is event is probably fired roughly when we trigger a reel stop
         * sound (but not necessarily).
         */
        this.SINGLE_REEL_SPIN_SETTLING = "Reels.SingleReelSpinSettling";

        /**
         * One-off event, fired each time a reel reaches its "stop" state (has fully stopped spinning)
         */
        this.SINGLE_REEL_SPIN_STOP_COMPLETE = "Reels.SingleReelSpinStopComplete";

        /**
         * Event fired when all reels have stopped (but before any Result Presentations may start)
         */
        this.ALL_REELS_STOPPED = "Reels.AllReelsStopped";

        //--------------------------------------------------------------------------------------------------
        // The following are sequence hook events - ReelGroup informs WinPresentationLayer that it's
        // time to show a certain hook sequence, WinPresentationLayer shows any sequence it wants, and
        // informs ReelView when its done.
        //--------------------------------------------------------------------------------------------------
        /**
         * Event dispatched by ReelGroup - for the consumption of WinPresentationView - indicating that
         * any Pre Spin Round presentation should now be shown (ReelGroup will wait for Pre Spin Round
         * Presentation complete event to understand if the action is completed)
         */
        this.SHOW_PRE_SPIN_ROUND_PRESENTATION = "Reels.ShowPreSpinRoundPresentation";

        /**
         * Event dispatched by Win Presentation View - for the consumption of ReelGroup - indicating that
         * the Pre Spin Round presentation has completed.
         */
        this.PRE_SPIN_ROUND_PRESENTATION_COMPLETE = "Reels.PreSpinRoundPresentationComplete";

        /**
         * Event dispatched by ReelGroup - for the consumption of WinPresentationView - indicating that
         * any Pre Spin Start presentation should now be shown (ReelGroup will wait for Pre Spin Start
         * Presentation complete event to understand if the action is completed)
         */
        this.SHOW_PRE_SPIN_START_PRESENTATION = "Reels.ShowPreSpinStartPresentation";

        /**
         * Event dispatched by Win Presentation View - for the consumption of ReelGroup - indicating that
         * the Pre Spin Start presentation has completed.
         */
        this.PRE_SPIN_START_PRESENTATION_COMPLETE = "Reels.PreSpinStartPresentationComplete";

        /**
         * Event dispatched by ReelGroup - for the consumption of WinPresentationView - indicating that
         * any Pre Spin Stop presentation should now be shown (ReelGroup will wait for Pre Spin Stop
         * Presentation complete event to understand if the action is completed)
         */
        this.SHOW_PRE_SPIN_STOP_PRESENTATION = "Reels.ShowPreSpinStopPresentation";

        /**
         * Event dispatched by Win Presentation View - for the consumption of ReelGroup - indicating that
         * the Pre Spin Stop presentation has completed.
         */
        this.PRE_SPIN_STOP_PRESENTATION_COMPLETE = "Reels.PreSpinStopPresentatioComplete";

        /**
         * Event dispatched by ReelGroup - for the consumption of WinPresentationView - indicating that
         * any Post Spin Stop presentation should now be shown (ReelGroup will wait for Post Spin Stop
         * Presentation complete event to understand if the action is completed)
         */
        this.SHOW_POST_SPIN_STOP_PRESENTATION = "Reels.ShowPostSpinStopPresentation";

        /**
         * Event dispatched by Win Presentation View - for the consumption of ReelGroup - indicating that
         * the Post Spin Stop presentation has completed.
         */
        this.POST_SPIN_STOP_PRESENTATION_COMPLETE = "Reels.PostSpinStopPresentationComplete";

        /**
         * Event dispatched by ReelGroup - for the consumption of WinPresentationView - indicating that
         * any Post Spin Wins presentation should now be shown (ReelGroup will wait for Post Spin Wins
         * Presentation complete event to understand if the action is completed)
         */
        this.SHOW_POST_SPIN_WINS_PRESENTATION = "Reels.ShowPostSpinWinsPresentation";

        /**
         * Event dispatched by Win Presentation View - for the consumption of ReelGroup - indicating that
         * the Post Spin Wins presentation has completed.
         */
        this.POST_SPIN_WINS_PRESENTATION_COMPLETE = "Reels.PostSpinWinsPresentationComplete";
        
        /**
         * Event dispatched by ReelGroup - for the consumption of WinPresentationView - indicating that
         * any Post Spin Result presentation should now be shown (ReelGroup will wait for Post Spin Result
         * Presentation complete event to understand if the action is completed)
         */
        this.SHOW_POST_SPIN_RESULT_PRESENTATION = "Reels.ShowPostSpinResultPresentation";

        /**
         * Event dispatched by Win Presentation View - for the consumption of ReelGroup - indicating that
         * the Post Spin Result presentation has completed.
         */
        this.POST_SPIN_RESULT_PRESENTATION_COMPLETE = "Reels.PostSpinResultPresentationComplete";

        /**
         * Event dispatched by Win Presentation View - for the consumption of ReelGroup - indicating that
         * the Post Spin Wins presentation has completed.
         */
        this.POST_SPIN_WINS_PRESENTATION_COMPLETE = "Reels.PostSpinWinsPresentationComplete";

        /**
         * Event dispatched by ReelGroup - for the consumption of WinPresentationView - indicating that
         * any Post Spin Round presentation should now be shown (ReelGroup will wait for Post Spin Round
         * Presentation complete event to understand if the action is completed)
         */
        this.SHOW_POST_SPIN_ROUND_PRESENTATION = "Reels.ShowPostSpinRoundPresentation";

        /**
         * Event dispatched by Win Presentation View - for the consumption of ReelGroup - indicating that
         * the Post Spin Round presentation has completed.
         */
        this.POST_SPIN_ROUND_PRESENTATION_COMPLETE = "Reels.PostSpinRoundPresentationComplete";

        //--------------------------------------------------------------------------------------------------
        // These are events broadcast by ReelGroup during the Wins sequence for an individual Spin Result
        // Presentation. This sequence uses more static timing (understood by ReelGroup), and we expect
        // the WinPresentationLayer to latch onto this timing.
        //--------------------------------------------------------------------------------------------------
        this.SHOW_REELS_IDLE = "Reels.ShowReelsIdle";

        this.SPIN_ROUND_PRESENTATION_STARTED = "Reels.SpinRoundPresentationStarted";

        /**
         * One-off event fired, when a new Spin Result presentation is started.
         */
        this.SPIN_RESULT_PRESENTATION_STARTED = "Reels.SpinResultPresentationStarted";

        /**
         * For any SpinResult, if there are a sequence of Slot Wins to show (SymbolWins, SpecialWins) that
         * must be shown after the reels stop spinning, then this event is fired when that sequence starts.
         */
        this.SPIN_RESULT_WIN_PRESENTATIONS_STARTED = "Reels.SpinResultWinPresentationStarted";

        /**
         * For any SpinResult, if there are a sequence of Slot Wins to show (SymbolWins, SpecialWins) that
         * must be shown after the reels stop spinning, then this event is fired when that sequence completes.
         */
        this.SPIN_RESULT_WIN_PRESENTATIONS_COMPLETE = "Reels.SpinResultWinPresentationComplete";

        this.BIG_WIN_PRESENTATION_STARTED = "Reels.BigWinPresentationStarted";

        this.BIG_WIN_PRESENTATION_COMPLETE = "Reels.BigWinPresentationComplete";

        /**
         * Event fired by ReelGroup, to indicate that the presentation of a single Symbol Win
         * has commenced. The Symbol Win Presentation data which describes the win which is now
         * being shown, will be passed along with the event.
         */
        this.SYMBOL_WIN_PRESENTATION_STARTED = "Reels.SymbolWinPresentationStarted";

        /**
         * Event fired by ReelGroup, to indicate that the presentation of a single Symbol Win
         * has completed. The Symbol Win Presentation data which describes the win which has
         * now stopped being shown, will be passed along with the event.
         */
        this.SYMBOL_WIN_PRESENTATION_COMPLETE = "Reels.SymbolWinPresentationComplete";

        this.SPECIAL_WIN_PRESENTATION_STARTED = "Reels.SpecialWinPresentationStarted";

        this.SPECIAL_WIN_PRESENTATION_COMPLETE = "Reels.SpecialWinPresentationComplete";

        this.TOTAL_WIN_PRESENTATION_STARTED = "Reels.TotalWinPresentationStarted";

        this.TOTAL_WIN_PRESENTATION_COMPLETE = "Reels.TotalWinPresentationComplete";

        this.SPIN_RESULT_PRESENTATION_COMPLETE = "Reels.SpinResultPresentationComplete";

        this.SPIN_ROUND_PRESENTATION_COMPLETE = "Reels.SpinRoundPresentationComplete";        
    }
}

export default (new C_ViewEvent);