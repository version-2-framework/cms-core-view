/**
 * Events fired directly by a Total Win animation implementation: these are to be fired
 * on the Total Win view class itself (so they are only for the use of the immediate user
 * of the Total Win View): they are NOT fired globally.
 */
class C_TotalWinViewEvent
{
    constructor()
    {
        /**
         * Fired by Total Win view when its animation sequence has completed.
         */
        this.COMPLETE = "TotalWinAnim.Complete";
    }
}

export default new C_TotalWinViewEvent;