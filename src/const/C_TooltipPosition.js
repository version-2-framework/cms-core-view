class C_TooltipPosition
{
    constructor()
    {
        /**
         * @public
         * @type {TooltipPosition}
         */
        this.ABOVE = "above";

        /**
         * @public
         * @type {TooltipPosition}
         */
        this.BELOW = "below";

        /**
         * @public
         * @type {TooltipPosition}
         */
        this.LEFT = "left";

        /**
         * @public
         * @type {TooltipPosition}
         */
        this.RIGHT = "right";
    }
}

export default (new C_TooltipPosition);