/**
 * UI event constants.
 */
class C_UIEvent
{
    constructor()
    {
        this.UPDATE = "UPDATE";
    }
}

export default (new C_UIEvent);