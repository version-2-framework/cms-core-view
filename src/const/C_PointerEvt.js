/**
 * Pointer event constants.
 */
class C_PointerEvt
{
    constructor()
    {
        this.ROLLOUT = "pointerout";
        this.DOWN = "pointerdown";
        this.UP = "pointerup";
        this.UPOUTSIDE = "pointerupoutside";
        this.MOVE = "pointermove";
        this.OVER = 'pointerover';

        this.BUT_ROLLOVER = "BUT_ROLLOVER";
        this.BUT_ROLLOUT = "BUT_ROLLOUT";
    }
}

export default (new C_PointerEvt);