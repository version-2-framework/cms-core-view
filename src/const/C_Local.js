/**
 * Cookie names.
 */
class C_Local
{
    constructor()
    {
        this.SOUND_ENABLED = "SOUND_ENABLED";
        this.MUSIC_ENABLED = "MUSIC_ENABLED";
        this.LAST_SETTINGS_TAB = "LAST_SETTINGS_TAB";
        this.AUTO_SPIN_NUMBER = "AUTO_SPIN_NUMBER";
        this.STOP_ON_FREESPIN = "STOP_ON_FREESPIN";
        this.STOP_ON_BONUS = "STOP_ON_BONUS";
        this.QUICK_SPIN = "QUICK_SPIN";
        this.LOSS_LIMIT = "LOSS_LIMIT";
        this.WIN_LIMIT = "WIN_LIMIT";

        this.SPIN_BUT_LAST_LANDSCAPE_X = "SPIN_BUT_LAST_LANDSCAPE_X";
        this.SPIN_BUT_LAST_LANDSCAPE_Y = "SPIN_BUT_LAST_LANDSCAPE_Y";
        this.SPIN_BUT_LAST_PORTRAIT_X = "SPIN_BUT_LAST_PORTRAIT_X";
        this.SPIN_BUT_LAST_PORTRAIT_Y = "SPIN_BUT_LAST_PORTRAIT_Y";
    }
}

export default (new C_Local);