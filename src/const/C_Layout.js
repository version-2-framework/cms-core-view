/**
 * Constants used for layout of windows.
 */
class C_Layout
{


    constructor()
    {
        this.VERTICAL = "VERTICAL";
        this.TAB = "TAB";

    }
}

export default (new C_Layout);