/**
 * Events dispatched by a ReelView instance. These are local events: they
 * are dispatched only by a ReelView instance itself (not on the global
 * event channel), and are basically for the consumption of ReelGroup.
 */
class C_ReelEvent
{
    constructor()
    {
        /**
         * Indicates that the Reel's infinite spin state has been fully entered.
         * Most spinner implementations can enter the infinite spin state in an
         * arbitrary way, but need some time before we can consider the inifnite
         * spin to have fully begun (exactly how much time, is a detail of the
         * spinner implementation being used). For example, a spinner which needs
         * to accelerate up to full speed (or has some kind of "pull back, lunge
         * forward" effect at the start of inifnite spin) will want to use this
         * event to indicate that any animations shown at the start of the spin
         * are completed (and the reel can be considered "fully at infinite spin":
         * at this point, the spin is safely interruptible).
         */
        this.INFINITE_SPIN_STARTED = "INFINITE_SPIN_STARTED";

        /**
         * Event fired, to indicate that a reel is "settling" - eg: it basically is
         * in its resting place, but may be wobbling back and forth a bit before it
         * actually "stops". Some spinners may not make a meaningful distinction
         * between this and stopped, due to the nature of their spin animation (if
         * not, then those spinners should simply dispatch this event immediately
         * before the reel stopped event). Other spinners, which may simulate real
         * reelbands spinning, may have a more significant delay between this and
         * reel stopped. In general, we probably want to show "spin stopped"
         * visual effects around here (especially for those kind of spinners which
         * may take a while to settle).
         */
        this.REEL_SETTLING = "REEL_SETTLING";

        /**
         * Indicates that the Ree's "stop on symbols" state is completed: the
         * reel has fully reached its target set of symbol positions, and is no
         * longer spinning.
         */
        this.REEL_STOPPED = "REEL_STOPPED";
    }
};

export default new C_ReelEvent();