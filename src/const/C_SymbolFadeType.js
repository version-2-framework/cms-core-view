/**
 * Constants for the way in which non-highlighted symbols are displayed when a winline is highlighted
 */

/**
 * Specifies that no fade effect will be applied.
 */
export const NONE = "NONE";

/**
 * Specifies that an alpha fade effect will be applied.
 */
export const ALPHA = "ALPHA";

/**
 * Specifies that a colour tinting fade effect will be applied.
 */
export const TINT = "TINT";