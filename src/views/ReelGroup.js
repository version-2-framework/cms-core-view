import BaseView from "./BaseView";
import C_GameEvent from "../../../cms-core-logic/src/const/C_GameEvent";
import C_ViewEvent from "../const/C_ViewEvent";
import * as C_RewardGroup from "../../../cms-core-logic/src/const/C_RewardGroup";
import C_SpinPhase from "../../../cms-core-logic/src/const/C_SpinPhase";
import C_SymbolType from "../../../cms-core-logic/src/const/C_SymbolType";
import { SpinModelUtils } from "../../../cms-core-logic/src/model/SpinModelUtils";
import C_BigWinViewEvent from "../const/C_BigWinViewEvent";
import C_ReelEvents from "../const/C_ReelEvents";
import C_TotalWinViewEvent from "../const/C_TotalWinViewEvent";

// TODO: Name changed (used to be "SpinPhase"), but its not used - can probably
// be safely removed, as there is no plan to use it.
/**
 * Constants indicate which SpinPhase the ReelGroup is currently displaying.
 */
const ReelGroupSpinState =
{
    IDLE : "idle",
    INFINITE_SPIN : "infiniteSpin",
    PRE_SPIN_STOP : "preSpinStop",
    STOPPING : "stopping",
    STOPPED : "stopped"
};


/**
 * A view which is a group of all the reels
 */
class ReelGroup extends BaseView
{
    constructor()
    {
        super();
        
        /**
         * @private
         * @type {SpinModelUtils}
         */
        this._spinUtils = this.app.spinModelUtils;

        /**
         * The number of reelband graphics currently being shown as actively spinning. This
         * value should be updated only once a reel has fully commenced its infinite spin.
         * @private
         * @type {number}
         */
        this._numReelsSpinning = 0;

        /**
         * The target number of reels that must be spun, for the current spin. It is not always
         * guaranteed that all available reels will actually spin, so: in order to know when all
         * required reels have started spinning (for a given spin), its usefull to cache the
         * actual number that we want to start spinning.
         * @private
         * @type {number}
         */
        this._targetNumReelsToSpin = 0;

        /**
         * Tracks the number of reels that have already stopped, for the current spin.
         * @private
         * @type {number}
         */
        this._numReelsAlreadyStopped = 0;

        // TODO: To add support for "symbol win sound" in idle win (which is supposed to exist),
        // the handling of this field will need to change (as well as the idle symbol win sound
        // code being added in..)
        /**
         * Tracks the number of Symbol Wins which have been shown for the current spin result
         * (in the QUICK WIN sequence... and later, possibly the idle win sequence).
         * @private
         * @type {number}
         */
        this._numSymbolWinsShown = 0;

        /**
         * Indicates whether (for the current spin), all reels which need to be spun, have fully
         * entered their "infinite spin" state. This is a pre-condition to bringing the reels to
         * a halt for the current spin.
         * @private
         * @type {boolean}
         */
        this._allReelsInInfiniteSpin = false;

        /**
         * Indicates if the minimum initial duration of any new "spin starting" animation has
         * elapsed. This is a pre-condition to bringing the reels to a halt for the current spin.
         * @private
         * @type {boolean}
         */
        this._isMinStartSpinTimeComplete = false;

        /**
         * Indicates if the Spin Results for our current spin are available. This is a pre-condition
         * to bringing the reels to a half for the current spin.
         * 
         * RUSSELLS WIP:
         * for respins, they will all be available in one go (that's the idea).
         * @private
         * @type {boolean}
         */
        this._stopSymbolsAvailable = false;
        
        /**
         * TimelineMax instance, which is used to get the reel spin started for all reels.
         * @private
         * @type {gsap.TimelineMax}
         */
        this._startSpinTimeline = null;

        /**
         * Timelinemax instance, which is used to bring the reel spin to a stop for all reels.
         * @private
         * @type {TimelineMax}
         */
        this._stopSpinTimeline = null;

        /**
         * The timeline that plays the win presentation sequence.
         * @private
         * @type {gsap.TimelineMax}
         */
        this._winPresentationTimeline = null;

        // TODO: This name is too ambiguous: it needs improving !!
        /**
         * Data object which tracks some information about a Spin Result Win Presentation that is
         * in progress, specifically: the current win amounts shown. These fields are cleared back
         * to 0, when a new Spin Result presentation begins. They are used when skipping to the end
         * of the current spin result winnings presentation (as we know how much of each win value
         * has been added to master model, we can add any remaining values that haven't yet been
         * shown). This is because we update the model after each individual win is shown (so that
         * we get "winnings update" events dispatched around our GUI).
         * @private
         */
        this._currSpinResultWinPresentation =
        {
            /**
             * Tracks the current amount of credit won shown during the sequence.
             */
            creditWon : 0,

            /**
             * Tracks the current amount of superbet won shown during the sequence.
             */
            superbetWon : 0,

            /**
             * Tracks the current number of FreeSpins won shown during the sequence.
             */
            numFreeSpinsWon : 0,

            /**
             * Tracks whether the Credit Total Winnings animation may have already been shown.
             */
            totalCreditWonShown : false
        };

        /**
         * An "infinite spin" is invoked when the game client is fetching Spin Phase results from
         * the server. The reels will keep spinning indefinitely, until results are available, at
         * which point we move forward to showing spin results. When we are restoring the game to
         * a Spin Phase, we skip the infinite spin state (as results are available before any spin
         * is shown). For this reason, we track whether an infinite spin is on-going: if the Reel
         * Group is instructed to show spin results, but no infinite spin is in progress, we can
         * skip straight to simply showing the results.
         * @private
         * @type {boolean}
         */
        this._isInfiniteSpinInProgress = false;

        /**
         * Tracks whether a spin animation is in progress (either infinite spin, or not). In the
         * future, we could probably replace these 2 seperate flags with a single state tracking
         * flag (and have methods to check for specific sub-states in a readable way): for now,
         * this will do just fine.
         * 
         * This flag is true from the moment ANY spin animation starts (whether we use the 
         * infinite spin step, or not), and false as soon as all reels have come to a halt.
         * @private
         * @type {boolean}
         */
        this._isSpinAnimationInProgress = false;

        /**
         * Tracks whether we are currently showing a "pre-spin start" seuqence (includes pre-spin
         * round, pre-spin start : everything up to the reels actually starting)
         * @private
         * @type {boolean}
         */
        this._isPreSpinPresentationInProgress = false;

        /**
         * @private
         * @type {number}
         */
         this._currentSpinRoundIndex = 0;

        /**
         * @private
         * @type {number}
         */
        this._currentSpinResultIndex = 0;

        /**
         * Tracks which type of sequence is currently being shown (eg: Spin1, Spin2, FreeSpin)
         * @private
         * @type {SpinPhaseType}
         */
        this._currSpinPhase = C_SpinPhase.SPIN_1;

        /**
         * Big Win animation sequence.
         * @private
         * @type {BigWinView}
         */
        this._bigWinAnimation = null;

        /**
         * Win Value animation
         * @private
         * @type {PIXI.DisplayObject}
         */
        this._winValueAnimation = null;

        /**
         * Reference to any total win animation that may be currently shown.
         * @private
         * @type {PIXI.DisplayObject}
         */
        this._totalWinAnimation = null;
        
        /**
         * Tracks if a call to stop the reels on a set of target symbols, has already been invoked.
         * @private
         * @type {boolean}
         */
        this._stoppingStarted = false;

        /**
         * Configuration for the "Skip Win Presentation" functionality.
         * @private
         * @type {SkipWinPresentationConfig}
         */
        this._skipWinConfig = this.config.skipWinConfig;

        /**
         * Flag, which tracks if we should currently be showing any holds pattern on the symbols
         * (we can explicitly set the state of a symbol to IDLE or HELD, and in some 2 spin games,
         * symbols may show an alternate visual state for HELD).
         * @private
         * @type {boolean}
         */
        this._showHoldsPatternEnabled = false;
    };


    /**
     * Creates graphical content for the ReelGroup
     * @public
     */
    create()
    {
        const config = this.config;
        const layout = this.getActiveSlotLayout();

        const reelsPosX = layout.reelsPosX;
        const reelsPosY = layout.reelsPosY;
        
        /**
         * @private
         * @type {SlotSpinConfig}
         */
        this._spinConfig = config.spinConfig;

        /**
         * Ordered set of ReelViews. Index 0 is the left-most reel.
         * @private
         * @type {ReelView[]}
         */
        this._reels = [];

        /**
         * Container for symbols.
         * @private
         * @type {PIXI.Container}
         */
        this._symContainer = new PIXI.Container();
        this._symContainer.x = reelsPosX;
        this._symContainer.y = reelsPosY;

        /**
         * A layer above the symbols, available to spinners: spinner implementations can write
         * customized graphics (which sit on top of the symbols) for their spin animations, into
         * this layer, and be confident that no matter how wide the graphics are, they are always
         * shown above the symbols
         * @private
         * @type {PIXI.Container}
         */
        this._spinnerOverlayContainer = new PIXI.Container();

        this.addChild(this._symContainer);
        this.addChild(this._spinnerOverlayContainer);

        let numReels = config.numReels;

        let initialSymbolsForMainHand = this.spinModel.getFinalSymbolIds()[0];

        for (let reelIdx = 0; reelIdx < numReels; reelIdx++)
        {
            let reelView = this.gameFactory.getReel(this, this._symContainer, this._spinnerOverlayContainer, reelIdx);
            reelView.create(initialSymbolsForMainHand[reelIdx]);
            this._reels.push(reelView);
        }

        this.sortSymbols();

        this.addListener(this.dispatcher, C_GameEvent.GAME_IDLE_STARTED, this.showIdle, this);
        this.addListener(this.dispatcher, C_GameEvent.GAME_IDLE_ENDED, this.killIdleWinningsSequence, this);
        
        this.addListener(this.dispatcher, C_GameEvent.FETCHING_SPIN_1_PHASE_RESULTS, this.startInfiniteSpin1, this);
        this.addListener(this.dispatcher, C_GameEvent.FETCHING_SPIN_2_PHASE_RESULTS, this.startInfiniteSpin2, this);
        
        this.addListener(this.dispatcher, C_GameEvent.ABORT_INFINITE_SPIN_ANIMATION, this.abortInfiniteSpinPhase, this);

        this.addListener(this.dispatcher, C_GameEvent.SHOW_SPIN2_HOLDS_PATTERN, this.showHoldsPatternOnSymbols, this);
        this.addListener(this.dispatcher, C_GameEvent.HIDE_SPIN2_HOLDS_PATTERN, this.hideHoldsPatternOnSymbols, this);
        this.addListener(this.dispatcher, C_GameEvent.HOLDS_PATTERN_CHANGED, this.updateIdleHoldsPatternOnSymbols, this);
        
        this.addListener(this.dispatcher, C_GameEvent.SHOW_SPIN_1_PHASE_RESULTS, this.beginVisualizationOfSpin1PhaseResults, this);
        this.addListener(this.dispatcher, C_GameEvent.SHOW_SPIN_2_PHASE_RESULTS, this.beginVisualizationOfSpin2PhaseResults, this);
        this.addListener(this.dispatcher, C_GameEvent.SHOW_FREESPIN_PHASE_RESULTS, this.beginVisualizationOfFreeSpinPhaseResults, this);

        this.addListener(this.dispatcher, C_GameEvent.SKIP_TO_END_OF_SLOT_WIN_PRESENTATION, this.skipToEndOfSpinResultWinPresentation, this);

        this.addListener(this.dispatcher, C_GameEvent.SHOW_SCATTER_WIN_SEQUENCE, this.showBonusScatterWinSequence, this);
        this.addListener(this.dispatcher, C_GameEvent.PLAY_REEL_STOP_SOUND, this.playReelStoppedSound, this);

        this.addListener(this.dispatcher, C_GameEvent.SHOW_SPIN2_IDLE_STATE, this.playSpin2IdleSounds, this);
    };


    // RUSSELLS TODO
    // Public? Private? Purpose?
    // It IS public - its being called by the spinner - that belongs to a reel - that has a reference
    // to its parent reelgroup.
    /**
     * 
     * @public
     */
    sortSymbols()
    {
        if (this.config.symDepthSortOrderById)
        {
            this._symContainer.children.sort(this.symCompare.bind(this));
        }
    };


    // RUSSELLS TODO
    // Public? Private? Purpose? Is this een used anywhere ??
    /**
     * @private
     * @param {SymbolView} s1 
     * @param {SymbolView} s2 
     */
    symCompare(s1, s2)
    {
        let id1 = s1.getId ? s1.getId() : -1;
        let id2 = s2.getId ? s2.getId() : -1;

        let sortRes = 0;

        let orderIdx1 = this.config.symDepthSortOrderById.indexOf(id1);
        let orderIdx2 = this.config.symDepthSortOrderById.indexOf(id2);

        // TODO: I cannot yet tell if zOrder is a standard PIXI property
        // or if this is something custom that wiener added.
        let z1 = s1.zOrder ? s1.zOrder : 0;
        let z2 = s2.zOrder ? s2.zOrder : 0;

        if (z1 > z2)
        {
            sortRes = 1;
        }
        else if (z1 < z2)
        {
            sortRes = -1;
        }
        else if (orderIdx1 > orderIdx2)
        {
            sortRes = 1;
        }
        else if (orderIdx1 < orderIdx2)
        {
            sortRes = -1;
        }
        else if (s1.x > s2.x)
        {
            sortRes = 1;
        }
        else if (s1.x < s2.x)
        {
            sortRes = -1;
        }
        else if (s1.y > s2.y)
        {
            sortRes = 1;
        }
        else if (s1.y < s2.y)
        {
            sortRes = -1;
        }

        return sortRes;
    };


    /**
     * Shows idle visual state. If there were any wins on the last Spin Result, cycle through
     * all of the Symbol Wins, and show any animations that exist for each symbol involved.
     * @private
     */
    showIdle()
    {
        this.log.info('ReelGroup.showIdle()');

        let lastSpinResult = this.getCurrentSpinResult();
        if (lastSpinResult && lastSpinResult.symbolWins.length > 0) {
            this.spinSoundController.playGameIdleWinSequenceSound();
            this.showIdleWinningsSequence();
        }
        else
        {
            this.log.info('ReelGroup.showIdle: no wins to show');

            this.spinSoundController.playGameIdleSound();
            this._reels.forEach(reelView => reelView.showIdleState());
            this._dispatcher.emit(C_ViewEvent.SHOW_REELS_IDLE);
        }
    };


    // TODO: Holds triggers (from SlotGameController) need some further tidying up:
    // it is clear that these are idle methods.
    /**
     * Method called, whenever we receive an event indicating that holds pattern should be shown.
     * We enable showing holds on symobls, and update the visual pattern shown.
     * @private
     */
    showHoldsPatternOnSymbols()
    {
        this.log.debug('ReelGroup.showHoldsPatternOnSymbols()');

        this._showHoldsPatternEnabled = true;

        // We may receive the event indicating that holds pattern should be shown while a spin
        // animation is in progress: if so, we will ignore the request (if a spin was already
        // in progress, then we should already have shown the holds pattern: the extra request
        // usually comes in cases where a game is restored to spin2, and we need to ensure that
        // holds are shown).

        if (!this._isSpinAnimationInProgress) {
            this.updateIdleHoldsPatternOnSymbolsToCurrent();
        }
    };


    /**
     * Method called whenever we receive an event indicating that holds pattern is no longer being
     * shown: we disable the ability to show holds patterns on the symbols.
     * @private
     */
    hideHoldsPatternOnSymbols()
    {
        this.log.debug('ReelGroup.hideHoldsPatternOnSymbols()');

        this._showHoldsPatternEnabled = false;
    };


    /**
     * Method called whenever we receive an event indicating that holds pattern has changed.
     * We will show the update holds pattern on the symobls (but only if "showing holds pattern
     * on symbols" is currently enabled).
     * @private
     */
    updateIdleHoldsPatternOnSymbols()
    {
        this.log.debug(`ReelGroup.updateIdleHoldsPatternOnSymbols(showPatternEnabled:${this._showHoldsPatternEnabled})`);

        // Only update holds pattern if showing it is enabled,
        // or if no spin animation is already in progress.
        if (this._showHoldsPatternEnabled && !this._isSpinAnimationInProgress) {
            this.updateIdleHoldsPatternOnSymbolsToCurrent();
        }
    };


    /**
     * Updates the IDLE holds pattern on the set of symbols, to the current player holds pattern.
     * This means that all symbols are set to HELD or IDLE, as appropriate. Should be called only
     * when in an IDLE like state, not during any kind of spin (where we may need to set symbols
     * to HELD or SPINNING).
     * 
     * This method does not attempt to check if it is valid to currently be showing holds: it
     * simply does the heavy lifting of translating the holds pattern into setting all symbols
     * to the correct visual state (IDLE if not held, HELD if held).
     * @private
     */
    updateIdleHoldsPatternOnSymbolsToCurrent()
    {
        this.log.debug('ReelGroup.updateIdleHoldsPatternOnSymbolsToCurrent()');

        let currHoldsPattern = this.spinModel.getPlayerHoldsPattern();
        this.updateIdleHoldsPatternOnSymbolsTo(currHoldsPattern);
    };


    /**
     * Updates the IDLE holds pattern on the set of symbols, to a given player holds pattern.
     * This means that all symbols are set to HELD or IDLE, as appropriate. Should be called only
     * when in an IDLE like state, not during any kind of spin (where we may need to set symbols
     * to HELD or SPINNING).
     * 
     * This method does not attempt to check if it is valid to currently be showing holds: it
     * simply does the heavy lifting of translating the holds pattern into setting all symbols
     * to the correct visual state (IDLE if not held, HELD if held).
     * @param {HoldsPattern} holdsPattern 
     */
    updateIdleHoldsPatternOnSymbolsTo(holdsPattern)
    {
        let patternForPrimaryHand = holdsPattern.pattern[0];
        let shouldHoldIndividualSymbols = holdsPattern.type === "holdSymbols";

        this.log.debug(`ReelGroup.updateIdleHoldsPatternOnSymbolsTo(holdsPattern:${JSON.stringify(patternForPrimaryHand)})`);
        
        // Set every symbol to either show HELD or IDLE state. If we are holding
        // reels, then we must set every symbol on a reel to HELD. If we are holding
        // individual symbols, then we need to check (for each visible symbol) if
        // that symbol on the reel has been explicitly held or not.
        this._reels.forEach((reel, reelIndex) => {
            let patternForReel = patternForPrimaryHand[reelIndex];
            let reelIsHeld = patternForReel;
            reel.iterateSymbols((symbol, symIsVisible, visiblePosIndex) =>
            {
                let holdSymbol = symIsVisible &&
                    ((!shouldHoldIndividualSymbols && reelIsHeld) ||
                     (shouldHoldIndividualSymbols && patternForReel[visiblePosIndex]));

                if (holdSymbol) {
                    symbol.showHeldState();
                }
                else
                {
                    symbol.showIdleState();
                }
            });
        });
    };


    // RUSSELLS TODO: do i need to rename this, to match the idea that its "the event called when
    // GameController says we are fetching spin results" ?? This might be clearer.

    /**
     * @private
     */
    startInfiniteSpin1() {
        this.startInfiniteSpin(C_SpinPhase.SPIN_1);
    };


    /**
     * @private
     */
    startInfiniteSpin2() {
        this.startInfiniteSpin(C_SpinPhase.SPIN_2);
    };


    /**
     * An infinite spin is started, while we are fetching results from the server. So,
     * this call is directly bound to that event... 
     * 
     * @private
     * @param {SpinPhaseType} spinState
     */
    startInfiniteSpin(spinState)
    {
        this.log.info(`ReelGroup.startInfiniteSpin(spinState:${spinState})`);

        this._currentSpinRoundIndex = 0;
        this._currentSpinResultIndex = 0;
        this._isInfiniteSpinInProgress = true;
        this._isSpinAnimationInProgress = true;
        this._isPreSpinPresentationInProgress = true;
        this._stopSymbolsAvailable = false;
        this._currSpinPhase = spinState;

        // This is called here, in order to clear all symbols either to IDLE
        // or HELD state, as is appropriate. When playing a 2 spin game - and
        // using autoplay - we will be skipping IDLE and IDLE2 states, so we
        // need to do a little more to handle HELD / IDLE states for symbols.
        // And we may have presentations shown before the spin starts.
        this.updateIdleHoldsPatternOnSymbolsToCurrent();

        // Show the preSpin round presentation, only once this is complete can we actually
        // start all reels spinning: pre-spin round is a special hook to do other interesting
        // stuff before the spin.
        this.showPreSpinRoundPresentation(() => this.showPreSpinStartPresentation());
    };


    /**
     * @private
     */
    abortInfiniteSpinPhase()
    {
        this.log.info('ReelGroup.onFetchResultsFailed()');

        // If we are already in infinite spin, we can abort it now. Otherwise, we are probably
        // in a chain of "pre-spin-starting" presentations. We will defer aborting the spin,
        // until we have finished that sequence.
        if (this._isPreSpinPresentationInProgress)
        {
            this.log.debug('ReelGroup: preSpinSequence in progress, queue abortion until after it finishes');
            this._abortSpinOncePreSpinPresentationsComplete = true;
        }
        else
        {
            this.log.debug('ReelGroup no preSpinSequence running, abort infinite spin now');
            this.abortInfiniteSpinAnimation();
        }
    };


    // TODO: "Infinite spin" currently has a confused meaning - a phase (including pre spin sequences
    // that might be shown by WinPresentationView), and the actual infinite spin on the reels. This
    // needs some cleaning up in terms of internal naming within this class.
    /**
     * Kills an infinite spin animation - effectively reverting the reels to their pre-spin state. This
     * is invoked when an attempt to fetch spin results has failed. All reel spins are killed,
     * and symbols are reset to their values before the spin started.
     * 
     * Can be overriden in subclasses, to add additional functionality (genreally best to simply
     * call the base method, and use this as an extendable hook to add some more actions to).
     * @protected
     */
    abortInfiniteSpinAnimation()
    {
        this.log.info('ReelGroup.killInfiniteSpin()');

        this._isInfiniteSpinInProgress = false;
        this._isSpinAnimationInProgress = false;

        // Kills any queued calls to get a reel to start spinning
        if (this._startSpinTimeline)
        {
            this._startSpinTimeline.kill();
            this._startSpinTimeline = null;
        }

        // Kill any spin animation on all of the reels.
        this._reels.forEach(reel => {
            reel.off(C_ReelEvents.INFINITE_SPIN_STARTED, this.onReelInfiniteSpinStarted, this);
            reel.off(C_ReelEvents.REEL_SETTLING, this.onReelSettling, this);
            reel.off(C_ReelEvents.REEL_STOPPED, this.onReelStopped, this);
            reel.abortSpin();
            reel.showIdleState();
        });


        // Revert the symbols shown on the reels, to whatever should have
        // been there, before the aborted infinite spin was started.
        let symbolsBeforeSpin = this.spinModel.getFinalSymbolIds();

        this.log.info(`symbolsBeforeSpin = ${JSON.stringify(symbolsBeforeSpin)}`);

        this.setAllSymbolsTo(symbolsBeforeSpin);

        this.dispatcher.emit(C_GameEvent.INFINITE_SPIN_ANIMATION_ABORTED);
    };


    /**
     * @private
     */
    beginVisualizationOfSpin1PhaseResults()
    {
        this.log.debug('ReelGroup.beingVisualizationOfSpin1PhaseResults()');

        this._currSpinPhase = C_SpinPhase.SPIN_1;

        this.beginVisualizationOfAllSpinRoundResults();
    };


    /**
     * @private
     */
    beginVisualizationOfSpin2PhaseResults()
    {
        this.log.debug('ReelGroup.beingVisualizationOfSpin2PhaseResults()');

        this._currSpinPhase = C_SpinPhase.SPIN_2;

        this.beginVisualizationOfAllSpinRoundResults();
    };


    /**
     * @private
     */   
    beginVisualizationOfFreeSpinPhaseResults()
    {
        this.log.debug('ReelGroup.beingVisualizationOfFreeSpinPhaseResults()');

        this._currSpinPhase = C_SpinPhase.FREESPIN;

        this.beginVisualizationOfAllSpinRoundResults();
    };


    /**
     * Begins showing all rounds / spins for the current Spin-line phase (works for Spin1, Spin2
     * & FreeSpin). Called when the Game Controller indicates that Spin Phase Results are available.
     * When this happens, we may either
     * a) be showing an infinite spin sequence of some kind (while we wait for results)
     * b) be waiting until results are available, before starting to show the sequence (this is the
     *    standard case for FreeSpins, but is also expected when showing any restored Spin-like phase
     *    result sequence)
     * 
     * If the case of (a), we skip immediately to bringing the infinite spin of the reels to a halt,
     * and then show the full sequence. In the case of (b), we start by asking Win Presentation Overlay
     * to show any Pre-Spin Round presentation required, before starting any sequence of spins (in the
     * case of (a), we can assume that is such a sequence was required, it will already have been shown).
     * @private
     */
    beginVisualizationOfAllSpinRoundResults()
    {
        if (this._isInfiniteSpinInProgress)
        {
            this.log.debug('ReelGroup.beginVisualizationOfAllSpinRoundResults: an infinite spin is in progress, bring it to a halt');

            this.actionOnSpinResultsAvailable();
        }
        else
        {
            this.log.debug('ReelGroup.beginVisualizationOfAllSpinRoundReults: no infinite spin in progress, skip straight to showing results');

            // If no infinite spin is in progress, it means either we are restoring
            // the spin phase, or we didn't bother with an infinite spin sequence
            // to start with.
            this._isSpinAnimationInProgress = true;
            this._currentSpinRoundIndex = 0;
            this._currentSpinResultIndex = 0;
            this._stopSymbolsAvailable = true;

            this.showPreSpinRoundPresentation(() => this.startRespin());
        }
    };


    /**
     * Hook to show an optional pre-spin-round presentation sequence. The actual sequence is deferred
     * to WinPresentationLayer. The presentation will be shown exactly once for each spin round, which
     * means only once in total for [Single Spin, Spin 1, Spin 2], and for FreeSpin phase, it will be
     * shown as many times as there are FreeSpin rounds in the FreeSpin phase.
     * 
     * This presentation must be shown before a Spin Round starts, and therefore there are 2 slightly
     * different use cases:
     * - we already know spin results (and will skip the infinite spin phase)
     * - we do not know spin results, and must show an infinite spin phase
     * For this reason, this method accepts a function parameter - this is the action which will be
     * called once the pre-spin round presentation has finished.
     * @private
     * @param {() => void} actionAfterPresentationComplete
     */
    showPreSpinRoundPresentation(actionAfterPresentationComplete)
    {
        this.log.debug('ReelGroup.showPreSpinRoundPresentation()');

        let spinRoundPresentation = this.getCurrentSpinRoundPresentation();

        // Once we get the notification that PreSpinRoundPresentation is completed, we
        // can invoke the next action (ReelGroup is effectively paused until this
        // notification is received)
        this._dispatcher.once(C_ViewEvent.PRE_SPIN_ROUND_PRESENTATION_COMPLETE, actionAfterPresentationComplete);

        // Inform listsners
        // - show any pre Spin Round Presentation
        // - explicitly give them data indicating where we have reached.
        this._dispatcher.emit(
            C_ViewEvent.SHOW_PRE_SPIN_ROUND_PRESENTATION,
            spinRoundPresentation,
            this._currSpinPhase,
            this.getNewSpinPhasePresentationStateModel());
    };


    /**
     * Starts a spin, when we already have results available.
     * @private
     */
    startRespin()
    {
        this.log.info('ReelGroup.startRespin()');
        
        this._isInfiniteSpinInProgress = false;
        this.showPreSpinStartPresentation();
    };


    /**
     * Shows the pre-spin-start special sequence to the player. This isn't actiually handled
     * by the ReelGroup: it's carried out by the implementation of the Win Presentation
     * overlay that is in use by the game client. The ReelGroup simply passed the notification
     * that it is time to show this sequence, and waits for a notification back that the
     * sequence is complete
     * 
     * This method has some special logi : if ReelGroup received an instruction to abort the
     * infinite spin phase (eg: because fetching results failed), then we defer this action
     * until the pre-spin start animation is completed. This means that all pre-spin sequences
     * are completed.
     * @private
     */
    showPreSpinStartPresentation()
    {
        this.log.debug('ReelGroup.showPreSpinStartPresentation()');

        let spinPresentation = this.getCurrentSpinResultPresentation();

        this._dispatcher.once(C_ViewEvent.PRE_SPIN_START_PRESENTATION_COMPLETE, () => {
            this._isPreSpinPresentationInProgress = false;

            if (this._abortSpinOncePreSpinPresentationsComplete)
            {
                this._abortSpinOncePreSpinPresentationsComplete = false;
                this.abortInfiniteSpinAnimation();
            }
            else
            {
                this.startAllReelsSpinning();
            }
        });

        this._dispatcher.emit(C_ViewEvent.SHOW_PRE_SPIN_START_PRESENTATION, spinPresentation, this._currSpinPhase);
    };


    /**
     * Returns the Spin Phase Timings to use for the current spin. This takes into account whether the
     * current spin phase is Spin1, Spin2 or FreeSpin, as well as the way that the timings may have
     * been configured.
     * @private
     * @return {SlotSpinPhaseTimings}
     */
    getSpinTimingsConfigForCurrSpinState()
    {
        let spinState = this._currSpinPhase;
        let timingsConfig = this._spinConfig.spinTimings;
        
        if (spinState === C_SpinPhase.SPIN_2 && this._spinConfig.spin2Timings) {
            timingsConfig = this._spinConfig.spin2Timings;
        }
        else
        if (spinState === C_SpinPhase.FREESPIN && this._spinConfig.freeSpinTimings) {
            timingsConfig = this._spinConfig.freeSpinTimings;
        }

        // There are multiple potential future configurations of timings. For now,
        // we either return the "simple" configuration directly, or a default value
        if (timingsConfig.type === "simple") {
            return timingsConfig;
        }
        // For now, return a default config
        else
        {
            return {
                reelStartDelayTime : 0,
                reelStopDelayTime : 0.2,
                teaseReelStopDelayTime : 0.4,
                minSpinTime : 0
            };
        }
    };


    /**
     * Starts all reels spinning (unless its a spin2, and any of the reels should be held).
     * @private
     */
    startAllReelsSpinning()
    {
        this.log.info('ReelGroup.startAllReelsSpinning()');

        this.spinSoundController.playSpinAnticipationSound(
           this._currSpinPhase, this._currentSpinRoundIndex, this._currentSpinResultIndex);

        let spinTimingsConfig = this.getSpinTimingsConfigForCurrSpinState();
        let holdsPattern = this.spinModel.getPlayerHoldsPattern();
        let isSpin2 = this._currSpinPhase === C_SpinPhase.SPIN_2;
        
        this._numReelsSpinning = 0;
        this._numReelsAlreadyStopped = 0;
        this._numSymbolWinsShown = 0;
        this._isMinStartSpinTimeComplete = false;
        this._allReelsInInfiniteSpin = false;
        this._stoppingStarted = false;

        this.dispatcher.emit(C_GameEvent.SPIN_ANIMATION_STARTED, {spinState:this._currSpinPhase});

        if (isSpin2) {
            this.log.debug(`ReelGroup.startAllReelsSpinning: isSpin2, holdsPattern:${JSON.stringify(holdsPattern)}`);
        }

        this._startSpinTimeline = new TimelineMax();

        let delay = 0;

        // Calculate the number of reels that must be spun
        let numReelsToSpin = this._reels.length;
        if (isSpin2) {
            numReelsToSpin = 0;
            holdsPattern.pattern[0].forEach(reelIsHeld => {
                if (reelIsHeld === false) {
                    numReelsToSpin += 1;
                }
            });
        }

        this.log.debug(`ReelGroup.startAllReelsSpinning: numReelsToSpin = ${numReelsToSpin}`);

        this._targetNumReelsToSpin = numReelsToSpin;

        // TODO: This now works for "holdReels" again, but holdSymbols needs some refactoring of the
        // actual ReelView components.
        this._reels.forEach((reel, reelIndex) =>
        {
            // TODO: this "isHeld" I think was a Wiener change, but they didn't document it.
            // We should not be setting a public field like this on reel, especially when it
            // is not explicitly spelt out what the field does and who changes it. I think
            // i cannot remove this yet, because PIDSpinner uses it ? But now that we have
            // "holdSymbols" functionality, it may be worth examining the api here.
            
            let holdWholeReel = false;
            if (isSpin2 && holdsPattern.type === "holdReels") {
                holdWholeReel = holdsPattern.pattern[0][reelIndex];
            }

            reel.isHeld = holdWholeReel;

            // TODO: This current logic will fail if we ever implement "per symbol holds".
            // The options would be
            // 1) only hold the reel is holds mode is "holdReels": otherwise, we spin the
            //    reel, but pass in data for which symbols are held. Games where indivudal
            //    symbols are held or not, would probably involve special spinners, where
            //    each symbol is a reel anyway
            // 2) We can treat each symbol as a reel AT THIS LEVEL: however, that would
            //    require substantial rewrites of the slot code.
            // So, (1) is probably correct, and we can switch to the terminology of "whole
            // reel is held" (otherwise we spin, and tell the spinner which symbols need
            // spinning). It can currently be seen, that we are in the process of moving
            // towards this functionality (as of 12.03.2021)
            
            if (holdWholeReel)
            {
                // Show the hold pattern on the reel. When playing a 2 spin game where we
                // show the holds pattern via the symbols, then in autoplay we will have
                // skipped "idle" and "idle2" states of the slot game, in which case we
                // must explicitly ensure that the holds pattern is shown when spin2 starts
                // (when not in autoplay, the holds state will shown during spin2 idle).
                reel.showHeldState();
            }
            else
            {
                this.log.debug(`ReelGroup: start spinning reel ${reelIndex}, delay before start = ${delay}`);

                // We make sure the symbols are shown as spinning: there may be a short delay
                // before the reel actually starts spinning. And we may be in autoplay, and
                // this might be a 2 spin game where we show holds via the symbols, in which
                // case this call is important (to clear the holds pattern shown via the symbols).
                // There is no guarantee that our spinner does this for us.
                reel.showSpinningState();

                this._startSpinTimeline.call(this.startReelSpinning, [reelIndex], this, delay);

                delay += spinTimingsConfig.reelStartDelayTime;
            }
        });

        // For the case where there are no reels to spin (all reels held), we need
        // to explicitly set this flag to true (it is a pre-condition of bringing
        // the spin to an end, and will not be called if there are no "reel has
        // fully started infinite spin" events getting fired).
        if (numReelsToSpin === 0) {
            this._allReelsInInfiniteSpin = true;
        }

        let minSpinTime = (numReelsToSpin > 0)? spinTimingsConfig.minSpinTime : 0.2;
        
        // Enforce min spin time, with an absolute call here
        this._startSpinTimeline.call(() => {
            this.log.debug(`ReelGroup: min spin time of ${minSpinTime} elapsed`);
        }, null, null, minSpinTime);

        // Once all reels started spinning, we are in a position to allow the reels to stop.
        // This call is deliberately appended onto the end of the timeline: it will only fire
        // once all reels have started spinning, and the minimum spin time has been enforced.
        // Spinner implementations generally enforce their own rules about making sure that
        // a start animation has fully completed, before showing the "spin stop" animation:
        // this logic is here solely to ensure that the "start infinite spin" calls have actually
        // been made, before we tell the reels that they must stop spinning.
        this._startSpinTimeline.call(this.actionOnMinSpinTimeElapsed, null, this);
    };


    /**
     * Starts an individual reel spinning (in the "infinite spin" phase)
     * @private
     * @param {number} reelIndex
     * The index of the reel that should start spinning/
     */
    startReelSpinning(reelIndex)
    {
        let reel = this._reels[reelIndex];
        let reelProgressiveIndex = this._numReelsSpinning;
        let numReelsInvolvedInSpin = this._targetNumReelsToSpin;

        /** @type {SingleReelSpinEventData} */
        let reelSpinEventData = {
            reelIndex,
            reelProgressiveIndex,
            numReelsInvolvedInSpin
        };
        
        this.spinSoundController.playReelStartSound(reelIndex, this._numReelsSpinning);
        
        reel.once(C_ReelEvents.INFINITE_SPIN_STARTED, this.onReelInfiniteSpinStarted, this);
        reel.startInfiniteSpin(this._currSpinPhase);

        this.dispatcher.emit(C_ViewEvent.SINGLE_REEL_SPIN_STARTED, reelSpinEventData);
    };


    /**
     * Callback fired, when a reel's infinite spin has fully commenced. This method checks
     * if the required number of reels have fully entered their infinite spin phase: if they
     * have, then it can invoke the appropriate follow on action (to indicate that the "all
     * reels in infinite spin" pre-condition to stopping the reels, has been met).
     * @private
     * @param {number} reelIndex
     */
    onReelInfiniteSpinStarted(reelIndex)
    {
        this._numReelsSpinning += 1;

        this.log.debug(`ReelGroup.onReelInfiniteSpinStarted(reelIndex:${reelIndex},numReelsSpinning:${this._numReelsSpinning})`);

        if (this._numReelsSpinning === this._targetNumReelsToSpin)
        {
            this.actionWhenAllReelsEnteredInfiniteSpin();
        }
    };


    /**
     * Action invoked, when all reels have entered their infinite spin phase. This is a
     * pre-condition of entering "reels stopping" state for ReelGroup. We set the flag to
     * indicate that all reels (that needed spinning) have reached full infinite spin, and
     * then we attempt to enter "reels stopping" state (other pre-conditions may still need
     * to be met, for that to work).
     */
    actionWhenAllReelsEnteredInfiniteSpin()
    {
        this.log.debug('ReelGroup: all reels have entered their infinite spin phase');

        this._allReelsInInfiniteSpin = true;

        this.tryAndEnterReelsStoppingState();
    };


    /**
     * Invoked when the minimum start spin time has completed. This is a pre-condition of
     * entering "reels stopping" state for ReelGroup. We set the flag to indiciate that min
     * spin time has passed, and then we attempt to enter "reels stopping" state (other pre
     * conditions may still need to be met, for that to work).
     * @private
     */
    actionOnMinSpinTimeElapsed()
    {
        this.log.info('ReelGroup.actionOnMinSpinTimeElapsed()');

        this._isMinStartSpinTimeComplete = true;

        this.tryAndEnterReelsStoppingState();
    };


    /**
     * @private
     */
    actionOnSpinResultsAvailable()
    {
        this.log.info('ReelGroup.actionOnSpinResultsAvailable()');

        this._stopSymbolsAvailable = true;
        
        this.tryAndEnterReelsStoppingState();
    };


    // TODO: I suspect after the change, this method gets triggered with pre-conditions met:
    // probably, the flags need resetting.
    // YES: This is clearly the issue, as most of these flags get reset in "startAllReelsSpinning"
    // Which seems logical, but it effectively buggers the current flow once i add in "pre spin start
    // sequence". Lukcily, this is easy to fix, it just needs a bit of refactoring within ReelGroup.
    /**
     * @private
     */
    tryAndEnterReelsStoppingState()
    {
        this.log.debug('ReelGroup.tryAndEnterReelsStoppingState()');

        if (this._allReelsInInfiniteSpin &&
            this._isMinStartSpinTimeComplete &&
            this._stopSymbolsAvailable)
        {
            this.log.debug('ReelGroup: all pre-conditions for spin stopping met, start the stopping sequence');

            // We clear some status flags here - the ones that we don't want being rechecked
            this._isInfiniteSpinInProgress = false;
            this._allReelsInInfiniteSpin = false;
            this._isMinStartSpinTimeComplete = false;

            this.showPreSpinStopPresentation();
        }
    };


    /**
     * Shows the pre-spin-stop special sequence to the player. This isn't actiually handled
     * by the ReelGroup: it's carried out by the implementation of the Win Presentation
     * overlay that is in use by the game client. The ReelGroup simply passed the notification
     * that it is time to show this sequence, and waits for a notification back that the
     * sequence is complete
     * @private
     */
    showPreSpinStopPresentation() {
        this.log.debug('ReelGroup.showPreSpinStopPresentation()');

        let spinPresentation = this.getCurrentSpinResultPresentation();

        // Wait for explicit notification that any preSpinStop sequence has completed
        // ReelGroup is effectively paused, until it receives this event
        this._dispatcher.once(C_ViewEvent.PRE_SPIN_STOP_PRESENTATION_COMPLETE, () => this.stopReels());

        // Explicitly instruct the WinPresentationView to show any sequence
        this._dispatcher.emit(
            C_ViewEvent.SHOW_PRE_SPIN_STOP_PRESENTATION,
            spinPresentation,
            this._currSpinPhase,
            this.getNewSpinPhasePresentationStateModel());
    };


    /**
     * Brings all reels to a stop, showing the final set of symbols for the current
     * spin.
     * @private
     */
    stopReels()
    {
        this.log.info(`ReelGroup.stopReels()`);

        if (!this._stoppingStarted)
        {
            this._stoppingStarted = true;

            this._startSpinTimeline.kill();
            this._startSpinTimeline = null;

            this._stopSpinTimeline = new TimelineMax();

            let spinTimingsConfig = this.getSpinTimingsConfigForCurrSpinState();

            this.log.info(`ReelGroup.stopReels: spinTimings = ${JSON.stringify(spinTimingsConfig)}`);
            
            if (this._numReelsSpinning > 0)
            {
                let delay = 0;
                let currSpinResultPresentation = this.getCurrentSpinResultPresentation();
                let currSpinResult = currSpinResultPresentation.spinResult;
                let spinType = currSpinResult.spinType;
                let handIndex = 0;
                let reelProgressiveIndex = 0;
                let numReelsTotal = this._reels.length;
                let numReelsInvolvedInSpin = this._targetNumReelsToSpin;

                this.log.debug(`currSpinResult = ${JSON.stringify(currSpinResult)}`);

                for (let reelIndex = 0; reelIndex < this._reels.length; reelIndex++)
                {
                    let reel = this._reels[reelIndex];
                    let reelIsSpinning = !reel.isHeld;

                    // TODO: review this holds logic, because it almost certainly breaks in
                    // certain edge cases.
                    if (reelIsSpinning)
                    {
                        reel.once(C_ReelEvents.REEL_SETTLING, this.onReelSettling, this);
                        reel.once(C_ReelEvents.REEL_STOPPED, this.onReelStopped, this)

                        //let preStopTriggerDelay = delay;
                        //let stopReelTriggerDelay = delay + this.app.config.preReelStopTriggerDelay;

                        let reelFinalSymbols = currSpinResult.finalSymbolIds[handIndex][reelIndex];
                        let stickyPositions = currSpinResult.stickySymbolsMap[handIndex][reelIndex];

                        /** @type {SingleReelSpinEventData} */
                        let reelSpinEventData =
                        {
                            reelIndex,
                            reelProgressiveIndex,
                            numReelsInvolvedInSpin
                        }

                        // TODO: At some point, restore this concept of "pre-reel-stop trigger".
                        // It was a good concept, but is not directly needed right now.
                        /*
                        // First, we dispatch a special event hook - informing anyone interested, that the given reel
                        // will be stopping in a certain amount of time. We pass the reelIndex, finalSymbols for the
                        // reel, as well as the delay in seconds before the reel will commence its stop phase.
                        this.addDelayedCall(
                            preStopTriggerDelay,
                            this.dispatcher.emit,
                            [C_ViewEvent.SINGLE_REEL_SPIN_STOP_STARTED, reelIndex, reelFinalSymbols, this.app.config.preReelStopDelay],
                            this.dispatcher);

                        // Then we tell the reel itself to stop spinning (there will be some kind of delay before it
                        // actually does, based on the Reel Spinner implementation being used)
                        this.addDelayedCall(stopReelTriggerDelay, () => reel.spinToSymbols(reelFinalSymbols, spinType, stickyPositions));
                        */

                        this.log.debug(`ReelGroup: stop spinning reel ${reelIndex}, delay before commencing stop = ${delay}`);
                        
                        this._stopSpinTimeline.call(
                            () => {
                                this.dispatcher.emit(C_ViewEvent.SINGLE_REEL_SPIN_STOP_STARTED, reelSpinEventData)
                                reel.spinToSymbols(reelFinalSymbols, spinType, stickyPositions);
                            },
                            null, this,
                            delay);

                        // Delay before the next reel starts to stop. This can either be standard delay, or "tease" delay
                        // (where we drag out how long it takes the reel to stop). Because we append delay - the first
                        // reel stops by definition with a delay of 0 - then, counterintuitively, we actually need to
                        // determine the delay for the next reel (and add it now!!). We also don't need to do this on the
                        // final reel (because there will be no subsequent reel to stop)
                        if (reelIndex < numReelsTotal - 1)
                        {
                            let nextReelIndex = reelIndex + 1;
                            let nextReelMustBeTeased = currSpinResultPresentation.teaseReels[nextReelIndex];
                            let requiredDelayForNextReel = nextReelMustBeTeased?
                                spinTimingsConfig.teaseReelStopDelayTime : spinTimingsConfig.reelStopDelayTime;

                            delay += requiredDelayForNextReel;
                        }

                        

                        reelProgressiveIndex += 1;
                    }
                }
            }
            else
            {
                this.log.info('ReelGroup.stopImmediately: no reels are spinning (all are held)');

                this.onAllReelsStopped();
            }
        }
    };


    /**
     * @private
     * @param {number} reelIndex 
     * Index of the reel that is settling.
     */
    onReelSettling(reelIndex)
    {
        let numReelsInvolvedInSpin = this._targetNumReelsToSpin;
        let reelProgressiveIndex = numReelsInvolvedInSpin - this._numReelsSpinning;

        /** @type {SingleReelSpinEventData} */
        let reelSpinEventData =
        {
            reelIndex,
            reelProgressiveIndex,
            numReelsInvolvedInSpin
        }

        this._dispatcher.emit(C_ViewEvent.SINGLE_REEL_SPIN_SETTLING, reelSpinEventData);
    };


    /**
     * Action invoked when a single spinning reel is brought to reset.
     * @param {number} reelIndex
     * @private
     */
    onReelStopped(reelIndex)
    {
        let numReelsInvolvedInSpin = this._targetNumReelsToSpin;
        let reelProgressiveIndex = numReelsInvolvedInSpin - this._numReelsSpinning;

        this._numReelsSpinning --;

        /** @type {SingleReelSpinEventData} */
        let reelSpinEventData =
        {
            reelIndex,
            reelProgressiveIndex,
            numReelsInvolvedInSpin
        }

        this.dispatcher.emit(C_ViewEvent.SINGLE_REEL_SPIN_STOP_COMPLETE, reelSpinEventData);

        this.log.debug(`ReelGroup.reel[${reelIndex}].onReelStopped: numReelsSpinning=${this._numReelsSpinning},eventData:${JSON.stringify(reelSpinEventData)}`);

        if (this._numReelsSpinning === 0)
        {
            this.onAllReelsStopped();
        }
    };


    /**
     * Plays a reel stopped sound, whenever a primary hand reel has reached its "about to stop" point of the spin.
     * This generally happens a short way before actual stopping (eg: for different spinner implementations, we
     * want to play a spin stopping sound a little in advance of the reel actually coming to a rest).
     * @private
     * @param {number} reelIndex
     * The index of the reel (within the primary hand) that has stopped.
     */
    playReelStoppedSound(reelIndex)
    {
        let presentation = this.getCurrentSpinResultPresentation();
        let reelSymbols = this.getCurrentSpinResult().finalSymbolIds[0][reelIndex];
        let numReelsAlreadyStopped = this._numReelsAlreadyStopped;
        this.spinSoundController.playReelStopSound(reelSymbols, reelIndex, numReelsAlreadyStopped, presentation);
        this._numReelsAlreadyStopped += 1;
    };


    /**
     * Action to invoke when all reels have stopped spinning.
     * @private
     */
    onAllReelsStopped()
    {
        if (this._stopSpinTimeline) {
            this._stopSpinTimeline.kill();
            this._stopSpinTimeline = null;
        }

        this._isSpinAnimationInProgress = false;

        this.dispatcher.emit(C_ViewEvent.ALL_REELS_STOPPED);
        this.dispatcher.emit(C_GameEvent.ALL_REELS_STOPPED);
        this.dispatcher.emit(C_GameEvent.SPIN_ANIMATION_FINISHED);

        // RUSSELLS TODO:
        // I think it would be good here to snap the view of symbols to the intended
        // final symbols of the spin - although in theory we should not need to, it might
        // mitigate issues that would occur if "holds" and "symbols shown" get out of sync.
        // Now, that shouldn't happen if the client functions correctly, right ? Well, out
        // in the reel world, we have had several real issues occur (on the backend) that
        // result in the client being delivered a result (for the player's session, no less,
        // so it's a real, accredited result) that doesn't match their holds pattern - and
        // the behaviour of the client, at this point, breaks, with the wrong symbols being
        // shown. So, snapping symbols to final position can dodge edge case bug conditions
        // such as this: the player might see something weird (symbols suddenly change), BUT
        // the new symbols match the info about wins that they are shown (which is the more
        // important condition)

        this.showPostSpinStopPresentation();
    };


    /**
     * Shows the post-spin-stop presentation. This is an (optional) special sequence, triggered
     * immediately after all reels have come to a halt. The actual sequence will be shown by the
     * Win Presentation Layer (as there is really no standard concept of what this sequence will
     * be), and ReelGroup will listen out for an event to indicate that the sequence has completed
     * (as we cannot define a single value for how long the sequence must last for).
     * 
     * Example use cases:
     * - we want to show a special animation after the reels have stopped spinning, but before we
     *   show ANY wins (including any big win sequence)
     * - we might have a special symbol present, for which we whish to perform some kind of special
     *   animation (a wild symbol which expands across multiple reels).
     * 
     * Of course, the exact use case will vary from game to game: its for this reason, that Reel
     * Group simply provides a hook at this point, allowing the Win Presentation layer for the game
     * client to do whatever it wants.
     * @private
     */
    showPostSpinStopPresentation() {
        this.log.debug('ReelGroup.showPostSpinStopPresentation()');

        let spinPresentation = this.getCurrentSpinResultPresentation();
        this._dispatcher.once(C_ViewEvent.POST_SPIN_STOP_PRESENTATION_COMPLETE, () => this.actionAfterPostSpinStopPresentation());
        
        this._dispatcher.emit(
            C_ViewEvent.SHOW_POST_SPIN_STOP_PRESENTATION,
            spinPresentation,
            this._currSpinPhase,
            this.getNewSpinPhasePresentationStateModel());
    };


    //--------------------------------------------------------------------------------------------------
    // Here is our new, improved, win presentaiton sequencing functionality
    // This method doesn't explicitly need to live within ReelGroup. Its simply the most
    // convenient place for it to exist, for the time being.
    // TODO: We need additional meta-data for this sequence: for any win sequence, we could potentially
    // count up the sequence of wins individually (eg: show a win amount), or we can show some kind of
    // "counter" going up with the wins. This is simpler to conceive if we use groups for the individual
    // reward groups (simpler to set a single flag), however, this takes away a little flexibility about
    // sequencing win events (although... why would we want to sequence different reward group win events
    // separately?). There is probably a half-way house i can arrive at in this regard
    //--------------------------------------------------------------------------------------------------
    /**
     * After the "post spin stop presentation" has completed, we must do one of 2 things
     * 1) If there are some slot wins to show, we trigger the sequence of slot win presentations. This is
     *    optional: although we could trigger the sequence, and the sequence would be empty, we want there
     *    to be start and end events for the sequence (for example, we want our background to show a special
     *    animation during the entire duration of the winnings sequence). Once the sequence of wins has been
     *    shown, we move to the "post spin wins presentation"
     * 2) If there are no slot wins, we skip straight to the "post spin wins presentation"
     * @private
     */
    actionAfterPostSpinStopPresentation()
    {
        let roundIndex = this._currentSpinRoundIndex;
        let spinIndex = this._currentSpinResultIndex;
        this.log.debug(`ReelGroup.actionAfterPostSpinStopPresentation(round:${roundIndex},spin:${spinIndex})`);

        let spinPresentation = this.getCurrentSpinResultPresentation();

        // If there are any wins to show, we trigger the "Spin Result Win Presentation" sequence
        // This sequence is triggered optionally: that's because we want "start" / "end" events
        // for the sequence, to only be fired if we are actually showing anything.
        if (spinPresentation.slotWins.length > 0)
        {
            this.log.debug(`ReelGroup: there are ${spinPresentation.slotWins.length} wins to show, trigger spinResultWinPresentation`);
            this.showSpinResultWinPresentation(spinPresentation.slotWins);
        }
        else
        {
            // If no wins, we can skip straight to the post spin wins presentation
            this.log.debug(`ReelGroup: no SlotWins to show for postSpinWinningsSequence: skip straight to postSpinWinsPresentation`);
            this.showPostSpinWinsPresentation();
        }
    };


    /**
     * If a Spin Result Win presentation is in progress, then this method will skip to the end of it.
     * - current spin result win presentation sequence will be aborted.
     * - all winning symbols from the spin result will be highlighted briefly. (TODO)
     * - any Credit/Superbet/FreeSpin winnings from the spin which have not yet been logged, will be
     *   added to the model simulation value.
     * - the next statefull action (pos spin wins presentation) will be invoked.
     * @private
     */
    skipToEndOfSpinResultWinPresentation()
    {
        this.log.info(`ReelGroup.skipToEndOfSpinResultWinPresentation()`);

        // If a WinPresentationTimeline exists, we can assume that the WinPresentation
        // is in progress, and that there is something for us to interrupt.
        if (this._winPresentationTimeline) {
            this.log.debug(`ReelGroup.skipToEndOfSpinResultWinPresentation: sequence in progress, can skip`);

            let currSpinResult = this.getCurrentSpinResult();
            let currSpinPresentation = this.getCurrentSpinResultPresentation();

            // We need to check if we have to manually show Total Credit Won
            let showTotalWinIfSkip = currSpinPresentation.showTotalWinIfSkip;
            let totalCreditWonAlreadyShown = this._currSpinResultWinPresentation.totalCreditWonShown;
            let showTotalCreditWin = showTotalWinIfSkip && !totalCreditWonAlreadyShown;

            this.log.debug(`ReelGroup.skipToEndOfSpinResultWinPresentation: showTotalWinIfSkip=${showTotalWinIfSkip}`);
            this.log.debug(`ReelGroup.skipToEndOfSpinResultWinPresentation: totalCreditWonAlreadyShown=${totalCreditWonAlreadyShown}`);
            this.log.debug(`ReelGroup.skipToEndOfSpinResultWinPresentation: showTotalCreditWon=${showTotalCreditWin}`);

            // Step 1: kill any win presentation that is in progress
            
            // Kill BigWin animation if it exists..
            if (this._bigWinAnimation) {
                this._bigWinAnimation.destroy();
                this._bigWinAnimation = null;
            }

            // Kill win value animation if THAT exists..
            if (this._winValueAnimation) {
                this._winValueAnimation.destroy();
                this._winValueAnimation = null;
            }

            // Kill any total win animation..
            if (this._totalWinAnimation) {
                this._totalWinAnimation.destroy();
                this._totalWinAnimation = null;
            }
            
            // Step 2: kill the win presentation timeline. This aborts any queued win animations that
            // are supposed to be shown at a future point.
            this._winPresentationTimeline.kill();
            this._winPresentationTimeline = null;

            // Step 3: update all model fields to the expected final value (taking into account that
            // we may have partially shown some wins in the meantime)
            let creditDelta = currSpinResult.totalCreditWon - this._currSpinResultWinPresentation.creditWon;
            if (creditDelta > 0) {
                this.log.debug(`ReelGroup.skipToEnd: totalCreditWon so far = ${this._currSpinResultWinPresentation.creditWon}`);
                this.log.debug(`ReelGroup.skipToEnd: totalCreditWon for spin = ${currSpinResult.totalCreditWon}`);
                this.log.debug(`ReelGroup.skipToEnd: creditDelta = ${creditDelta}`);
                this._model.recordCreditWin(creditDelta);
                
            }

            let superbetDelta = currSpinResult.totalSuperBetWon - this._currSpinResultWinPresentation.superbetWon;
            if (superbetDelta > 0) {
                this._model.recordSuperBetWin(superbetDelta);
            }

            let numFreeSpinsDelta = currSpinResult.numFreeSpinsWon - this._currSpinResultWinPresentation.numFreeSpinsWon;
            if (numFreeSpinsDelta > 0) {
                this._model.recordFreeSpinsWin(numFreeSpinsDelta);
            }

            // Now that we have actually skipped the win presentation, we need to tell the GUI that it's
            // no longer valid to skip any win animation.
            this._dispatcher.emit(C_GameEvent.DISABLE_SKIP_SPIN_WIN_PRESENTATION);

            // Step 4: Show a brief pause, with all winning symbols from the spin results highlighted simultaneously.
            // Because winnings meters should all now be updated (because we set some values on model), this should
            // give the player a chance to take in the new, updated values. At the end of this pause, we broadcast
            // the event indicating that the SpinResultWinPresenation has completed, and we defer control back to
            // the method which would originbally have been called at the end of the WinPresenation timeline that we
            // killed..

            let endWinPresentationTimeline = new TimelineMax();

            if (showTotalCreditWin)
            {
                let totalWon = currSpinResult.totalCreditWon;

                endWinPresentationTimeline.call(() => {
                    this.showQuickWinStateOnSymbols(currSpinPresentation.winMap, true);
                    this.stopBgMusicDuringTotalWinIfRequired();
                });

                endWinPresentationTimeline.addPause("+=0.1", () => {
                    this.log.debug(`ReelGroup.skipToEndOfSpinResultWinPresentation: timeline is paused? ${endWinPresentationTimeline.paused()}`);

                    this.showTotalWonForRewardGroup(C_RewardGroup.CREDIT, totalWon, () => {
                        this.log.debug('ReelGroup.skipToEndOfSpinResultWinPresentation: additional TotalWinnings animation (after skip win) completed');
                        this._totalWinAnimation = null;
                        endWinPresentationTimeline.resume();
                    });
                });

                // NOTE: These magic delays are required, in order to make TimelineMax's "addPause" functionality
                // work correctly: it seems to struggle if there are events with the same time-stamp as the pause /
                // unpause (pausing simply doesn't happen as intended).
                endWinPresentationTimeline.call(() => this.dispatcher.emit(C_ViewEvent.SPIN_RESULT_WIN_PRESENTATIONS_COMPLETE), null, null, "+=0.1");
                endWinPresentationTimeline.call(() => this.sound.restoreMainGameMusic());
                endWinPresentationTimeline.call(() => this.showPostSpinWinsPresentation(), null, null, "+=0.1");
            }
            else
            {
                endWinPresentationTimeline.call(() => this.showQuickWinStateOnSymbols(currSpinPresentation.winMap, true));
                endWinPresentationTimeline.call(() => this.dispatcher.emit(C_ViewEvent.SPIN_RESULT_WIN_PRESENTATIONS_COMPLETE));
                endWinPresentationTimeline.call(() => this.sound.restoreMainGameMusic());
                endWinPresentationTimeline.call(() => this.showPostSpinWinsPresentation(), null, null, "+=0.5");
            }
        }
        else
        {
            this.log.debug(`ReelGroup.skipToEndOfSpinResultWinPresentation: no sequence in progress, cannot skip!!`);
        }
    };


    /**
     * Shows a sequence of slot win presentations, according to the order of the data passed to param
     * "slotWinPresentations".
     * some win presentations)
     * @private
     * @param {SlotWinPresentation[]} slotWinPresentations
     */
    showSpinResultWinPresentation(slotWinPresentations)
    {
        this.log.info(`ReelGroup.showSpinResultWinPresentation: ${JSON.stringify(slotWinPresentations)}`);

        // Clear any cached values for current amounts won
        this._currSpinResultWinPresentation.creditWon = 0;
        this._currSpinResultWinPresentation.superbetWon = 0;
        this._currSpinResultWinPresentation.numFreeSpinsWon = 0;
        this._currSpinResultWinPresentation.totalCreditWonShown = false;

        let timeline = new TimelineMax();

        // Inform the world that the win presentations sequence has started.
        // We might want to trigger some animations for the whole sequence,
        // and do it when the sequence starts and ends (instead of triggering
        // animations from individual events in the sequence)
        timeline.call(() => this.dispatcher.emit(C_GameEvent.HIDE_SPIN2_HOLDS_PATTERN));
        timeline.call(() => this.dispatcher.emit(C_ViewEvent.SPIN_RESULT_WIN_PRESENTATIONS_STARTED));

        // Assemble each win presentation into our timeline.
        slotWinPresentations.forEach(slotWinPresentation => {
            if (slotWinPresentation.type === "bigWin")
            {
                // The delay before pause is required, because TimelineMax seems to have a bug (or behaviour
                // that I do not understand), where pausing when nothing is in the timeline, doesn't
                // actually work.
                timeline.addPause("+=0.1", () => {
                    this.startBigWinPresentation(slotWinPresentation, () => {
                        this.endBigWinPresentation(slotWinPresentation);
                        this.log.debug('ReelGroup: BigWin animation completed, resume win presentation timeline');
                        timeline.resume();
                    });
                });
            }
            else
            if (slotWinPresentation.type === "symbolWin")
            {
                let symbolWinDuration = this._spinConfig.symbolWinDuration;
                timeline.call(this.startQuickWinSymbolWinPresentation, [slotWinPresentation], this);
                timeline.call(this.endSymbolWinPresentation, [slotWinPresentation], this, `+=${symbolWinDuration}`);
            }
            // NOTE:
            // Special win is the only case which currently makes me doubt using timeline - might we want
            // to vary the length of a special win presentation (considering that it will usually be
            // executed by the win presentation layer ?)
            else
            if (slotWinPresentation.type === "specialWin")
            {
                // TODO: add this field? will it work ok, to use a standard length for all special win simulations ?
                let specialWinDuration = this.app.config.specialWinDuration;
                timeline.call(this.startSpecialWinPresentation, [slotWinPresentation], this);
                timeline.call(this.endSpecialWinPresentation, [slotWinPresentation], this, `+=${specialWinDuration}`);
            }
            else
            if (slotWinPresentation.type === "totalWin")
            {
                // The delay before pause is required, because TimelineMax seems to have a bug (or behaviour
                // that I do not understand), where pausing when nothing is in the timeline, doesn't
                // actually work.
                timeline.addPause("+=0.1", () => {
                    this.log.debug(`ReelGroup.showTotalWin: timeline is paused? ${timeline.paused()}`);

                    this._currSpinResultWinPresentation.totalCreditWonShown = true;

                    this.startTotalWinPresentation(slotWinPresentation, () => {
                        this.endTotalWinPresentation(slotWinPresentation);
                        this.log.debug('ReelGroup: TotalWinnings animation completed, resume win presentation timeline');
                        timeline.resume();
                    })
                });
            }
        });

        // At this point, all individual win presentations have been completed: all that our timeline
        // needs to do, is perform clean up, and inform the world that we have finished the sequence

        // Clears the reference to this timeline instance: we insert a small delay here, because we
        // may have hadd an "addPause" (including as the last item in the timeline): apparently if
        // TimelineMax has other events (added AFTER an addPause), but at the same time, it triggers
        // those events, and the pause doesn't work (so the timeline continues)? It's not terribly
        // clear if this is expected (or a big), but this delay solves the problem.
        timeline.call(() => this._winPresentationTimeline = null, null, null, "+=0.1");
        
        // At the end of the timeline, explicitly disable the ability to skip win presentations
        if (this._skipWinConfig.enabled) {
            timeline.call(() => this.dispatcher.emit(C_GameEvent.DISABLE_SKIP_SPIN_WIN_PRESENTATION));
        }

        // Finally, indicate to the outside world that our win presentation sequence
        // has completed.
        timeline.call(() => this.dispatcher.emit(C_ViewEvent.SPIN_RESULT_WIN_PRESENTATIONS_COMPLETE));
        timeline.call(() => this.sound.restoreMainGameMusic());
        timeline.call(this.showPostSpinWinsPresentation, null, this);
        
        this._winPresentationTimeline = timeline;
    };


    /**
     * Immediately starts a new "Big Win" presentation, with a specific data configuration. Once the Big Win
     * presentation has been completed, the "onBigWinComplete" callback will be executed.
     * @private
     * @param {BigWinPresentation} bigWinPresentation
     * Data description of the Big Win presentation that needs to be shown.
     * @param {()=>void} onBigWinComplete
     * A callback, that will be executed once the Big Win presentation has completed.
     */
    startBigWinPresentation(bigWinPresentation, onBigWinComplete)
    {
        // TODO: The BigWin view should not be added here: it should be handled by the WinPresentationView

        if (this._skipWinConfig.enabled) {
            if (this._skipWinConfig.allowSkipBigWin) {
                this.dispatcher.emit(C_GameEvent.ENABLE_SKIP_SPIN_WIN_PRESENTATION);
            }
            // We need to specifically disable it, if it was previously enabled.
            else this.dispatcher.emit(C_GameEvent.DISABLE_SKIP_SPIN_WIN_PRESENTATION);
        }
        
        
        this.log.debug(`ReelGroup.showBigWinPresentation(${JSON.stringify(bigWinPresentation)})`);

        let totalStake = this._model.getTotalBet();
        let bigWinAmount = bigWinPresentation.rewardValue;
        let wonAsMultipleOfTotalStake = bigWinAmount / totalStake;

        this.dispatcher.emit(C_ViewEvent.BIG_WIN_PRESENTATION_STARTED, bigWinPresentation);

        this._bigWinAnimation = this.gameFactory.getBigWin();
        this._bigWinAnimation.once(C_BigWinViewEvent.COMPLETE, onBigWinComplete);

        // RUSSELLS TODO: this is messy: originally wiener were adding BigWin from ReelGroup (to the parent
        // of ReelGroup). This no longer works (due to scaling and positioning applied to ReelGroups parent).
        // Probably, the Big Win animation is a responsibility of WinPresentationView. In any case, adding
        // to "parent.parent" works for now, but i think it can be handled better at a future stage.
        this.parent.parent.addChild(this._bigWinAnimation); 
        this._bigWinAnimation.create(wonAsMultipleOfTotalStake, totalStake, bigWinAmount);

        if (bigWinPresentation.highlightSymbols) {
            this.showBigWinStateOnSymbols(bigWinPresentation.highlightSymbols, true);
        }
        else
        {
            this.fadeAllSymbols();
        }

        this.stopBgMusicDuringBigWinIfRequired();
    };


    /**
     * Ends the Big Win Presentation.
     * @private
     * @param {BigWinPresentation} bigWinPresentation 
     */
    endBigWinPresentation(bigWinPresentation)
    {
        this.log.debug('ReelGroup.endBigWinPresentation()');

        this.dispatcher.emit(C_ViewEvent.BIG_WIN_PRESENTATION_COMPLETE, bigWinPresentation);
        
        // TODO: destroy the big win view
        this._bigWinAnimation.destroy(true);
        this._bigWinAnimation = null;

        this.restoreBgMusicAfterBigWinIfRequired();
    };


    /**
     * Shows a single Symbol Win Presentation. This method is called as part of the "Quick" win presentaiton,
     * which is the sequence in which we count up the player's winnings.
     * @private
     * @param {SymbolWinPresentation} symbolWinPresentation 
     */
    startQuickWinSymbolWinPresentation(symbolWinPresentation)
    {
        this.log.debug('ReelGroup.startQuickWinSymbolWinPresentation()');

        this.dispatcher.emit(C_ViewEvent.SYMBOL_WIN_PRESENTATION_STARTED, symbolWinPresentation);

        if (this._skipWinConfig.enabled) {
            this.dispatcher.emit(C_GameEvent.ENABLE_SKIP_SPIN_WIN_PRESENTATION);
        }

        this.spinSoundController.playSymbolWinSound(symbolWinPresentation, true, this._numSymbolWinsShown);

        this._numSymbolWinsShown += 1;

        this.showQuickSymbolWinPresentation(symbolWinPresentation);
    };


    /**
     * Shows the visual elements associated with a Quick Symbol Win Presentation.
     * 
     * The method is marked as protected, because it may be overriden in specialized sub-classes of ReelGroup.
     * For example, there may be specialized cases where the visual aspects of a Quick SymbolWinPresentation
     * must be handled differently (this is specifically intended for multi-hand games: you should NOT be
     * overriding this method in general game-specific use cases).
     * @protected
     * @param {SymbolWinPresentation} symbolWinPresentation 
     */
    showQuickSymbolWinPresentation(symbolWinPresentation)
    {
        // If the symbol win is on a winline, check if a target reel index
        // and symbol position index that is designated for that winline:
        // if these values exist, these are the positions at which we will
        // place the win text value that is shown. Otherwise, we pass null
        // arguments for those values when showing the win value (and the
        // win value will be shown at default positions).
        let targetReelIndex = null, targetPosIndex = null;
        let winlineId = symbolWinPresentation.win.lineId;
        if (winlineId > 0)
        {
            let winlineData = this.config.winlines[winlineId];
            targetReelIndex = winlineData.targetReelIndexForWin !== undefined? winlineData.targetReelIndexForWin : null;
            targetPosIndex = winlineData.targetPosIndexForWin !== undefined? winlineData.targetPosIndexForWin : null;
            
            this.log.debug(`ReelGroup: position winValue: winlineId:${winlineId}, targetReelIndex:${targetReelIndex},targetPosIndex:${targetPosIndex}`);
        }

        let symbolWin = symbolWinPresentation.win;

        this.highlightSymbolWin(symbolWin, false, true);
        this.showSingleWinValue(symbolWin, targetReelIndex, targetPosIndex);
    };
                

    /**
     * @private
     * @param {SymbolWinPresentation} symbolWinPresentation 
     */
    endSymbolWinPresentation(symbolWinPresentation)
    {
        this.log.debug('ReelGroup.endSymbolWinPresentation()');
        this._winValueAnimation = null;
        this.dispatcher.emit(C_ViewEvent.SYMBOL_WIN_PRESENTATION_COMPLETE, symbolWinPresentation);
    };


    /**
     * @private
     * @param {SpecialWinPresentation} specialWinPresentation 
     */
    startSpecialWinPresentation(specialWinPresentation)
    {
        this.log.debug('ReelGroup.startSpecialWinPresentation()');

        this._dispatcher.emit(C_ViewEvent.SPECIAL_WIN_PRESENTATION_STARTED, specialWinPresentation);

        if (this._skipWinConfig.enabled) {
            this.dispatcher.emit(C_GameEvent.ENABLE_SKIP_SPIN_WIN_PRESENTATION);
        }
        
        // TODO: Needs implementing !
    };


    /**
     * @private
     * @param {SpecialWinPresentation} specialWinPresentation 
     */
    endSpecialWinPresentation(specialWinPresentation)
    {
        this.log.debug('ReelGroup.endSpecialWinPresentation()');

        this._dispatcher.emit(C_ViewEvent.SPECIAL_WIN_PRESENTATION_COMPLETE, specialWinPresentation);
        
        // TODO: Needs implementing !
    };


    /**
     * Immediately starts a new "Total Win" presentation, with a specific data configuration. Once the Total
     * Win presentation has been completed, the "onTotalWinComplete" callback will be executed.
     * @private
     * @param {TotalWinPresentation} totalWinPresentation
     * Data description of the Total Win presentation that needs to be shown.
     * @param {()=>void} onTotalWinComplete
     * A callback, that will be executed once the Total Win presentation has completed.
     */
    startTotalWinPresentation(totalWinPresentation, onTotalWinComplete)
    {
        this.log.debug('ReelGroup.startTotalWinPresentation()');

        this._dispatcher.emit(C_ViewEvent.TOTAL_WIN_PRESENTATION_STARTED, totalWinPresentation);

        // TODO: This may not be the long-term desired behaviour in relation to skipping total win
        if (this._skipWinConfig.enabled) {
            this.dispatcher.emit(C_GameEvent.ENABLE_SKIP_SPIN_WIN_PRESENTATION);
        }

        // TODO: This basically suspends it for a single reward group, which is probably about
        // correct - except its never going to be restored at the moment, if we have more than
        // 1 reward group's total winnings being shown. So the logic may need some reworking.
        this.stopBgMusicDuringTotalWinIfRequired();

        this.showTotalWonForRewardGroup(totalWinPresentation.rewardGroup, totalWinPresentation.totalWon, onTotalWinComplete);
    };


    // TODO: THis documentation can definitely be improved
    /**
     * Clears up, after a Total Win presentation has completed.
     * @private
     * @param {TotalWinPresentation} totalWinPresentation 
     */
    endTotalWinPresentation(totalWinPresentation)
    {
        this.log.debug('ReelGroup.endTotalWinPresentation()');

        this._dispatcher.emit(C_ViewEvent.TOTAL_WIN_PRESENTATION_COMPLETE, totalWinPresentation);

        this._totalWinAnimation = null;

        this.restoreBgMusicAfterTotalWinCompleteIfRequired();
    };


    /**
     * The post-spin-wins presentation is shown after any and all win sequences for the Spin Result
     * are shown (even if there were no win sequences). The sequence will be shown by the Win
     * Presentation Layer: it is simply another hook, to append any special animation in the Win
     * Presentation Layer that we might want, after all spin wins have been awarded to the player
     * (maybe for a specific game client, we want to show a "tidy up" animation). We don't know how
     * long the sequence will take (even for a game which implements the sequence, it could vary
     * in length, depending on what results were shown for the spin).
     * @private
     */
    showPostSpinWinsPresentation()
    {
        this.log.debug('ReelGroup.showPostSpinWinsPresentation()');

        let spinPresentation = this.getCurrentSpinResultPresentation();
        this._dispatcher.once(C_ViewEvent.POST_SPIN_WINS_PRESENTATION_COMPLETE, () => this.showPostSpinResultPresentation());
        
        this._dispatcher.emit(
            C_ViewEvent.SHOW_POST_SPIN_WINS_PRESENTATION,
            spinPresentation,
            this._currSpinPhase,
            this.getNewSpinPhasePresentationStateModel());
    };


    /**
     * Action to call when an individual Spin result has been shown (including any win
     * presentation).
     * The spin result may be a single spin from a sequence of respins for either a Spin
     * Phase Result, or a FreeSpin round result. The GameController is only interested
     * in the Spin Phase having started / ended: the actual implementation of ReelGroup
     * gets to implement the actual sequencing of spin results, so this method determines
     * if we need to show another Spin Round / Spin Result, or inform GameController that
     * we have finished showing the Spin Phase Results.
     * @private
     */
    showPostSpinResultPresentation()
    {
        this.log.debug('ReelGroup.showPostSpinResultPresentation()');

        let spinPresentation = this.getCurrentSpinResultPresentation();
        this._dispatcher.once(C_ViewEvent.POST_SPIN_RESULT_PRESENTATION_COMPLETE, () => this.onPostSpinResultPresentationComplete());
        
        this._dispatcher.emit(
            C_ViewEvent.SHOW_POST_SPIN_RESULT_PRESENTATION,
            spinPresentation,
            this._currSpinPhase,
            this.getNewSpinPhasePresentationStateModel());
    };


    /**
     * Action invoked when the post spin result presentation is complete: determines if its the
     * end of the Spin Round or not.
     * @private
     */
    onPostSpinResultPresentationComplete()
    {
        this.log.debug('ReelGroup.onPostSpinResultPresentationComplete()');

        let numSpinsCompleteForCurrRound = this._currentSpinResultIndex + 1;
        let allRounds = this._spinModel.getPhaseResults().rounds;
        let currRound = allRounds[this._currentSpinRoundIndex];
        let numSpinsInCurrRound = currRound.spins.length;
        let numSpinsRemainingForRound = numSpinsInCurrRound - numSpinsCompleteForCurrRound;

        if (numSpinsRemainingForRound > 0)
        {
            this.log.info(`ReelGroup.onSpinResultPresentationComplete: ${numSpinsRemainingForRound} spins remaining for this round`);

            this._currentSpinResultIndex += 1;
            this.startRespin();
        }
        else
        {
            this.showPostSpinRoundPresentation();
        }
    }


    /**
     * Triggers any Post Spin Round presentation, and waits for it to complete, before moving on.
     * @private
     */
    showPostSpinRoundPresentation() {
        this.log.debug('ReelGroup.showPostSpinRoundPresentation()');

        let spinRoundPresentation = this.getCurrentSpinRoundPresentation();
        this._dispatcher.once(C_ViewEvent.POST_SPIN_ROUND_PRESENTATION_COMPLETE, () => this.onSpinRoundPresentationComplete());
        
        this._dispatcher.emit(
            C_ViewEvent.SHOW_POST_SPIN_ROUND_PRESENTATION,
            spinRoundPresentation,
            this._currSpinPhase,
            this.getNewSpinPhasePresentationStateModel());
    };


    /**
     * Action triggered when any Post Spin Round presentation has completed. Determines if we have more
     * rounds in the spin phase, and if so, starts them (otherwise, ends the spin phase).
     * @private
     */
    onSpinRoundPresentationComplete() {
        this.log.debug('ReelGroup.onSpinRoundPresentationComplete()');

        this.dispatcher.emit(C_ViewEvent.SPIN_ROUND_PRESENTATION_COMPLETE);

        let numRoundsInCurrPhase = this._currentSpinRoundIndex + 1;
        let numRoundsInPhase = this._spinModel.getPhaseResults().rounds.length;
        let numRoundsRemainingForPhase = numRoundsInPhase - numRoundsInCurrPhase;
        if (numRoundsRemainingForPhase > 0)
        {
            this.log.info(`ReelGroup.onSpinRoundPresentationComplete: ${numRoundsRemainingForPhase} rounds remaining for this phase`);

            this.dispatcher.emit(C_ViewEvent.SPIN_ROUND_PRESENTATION_STARTED);

            this._currentSpinRoundIndex += 1;
            this._currentSpinResultIndex = 0;
            this.startRespin();
        }
        else
        {
            this.log.info('ReelGroup.onSpinResultPresentationComplete: no rounds left for current phase');

            this.notifyControllerThatSpinPhasePresentationIsFinished();
        }
    };


    /**
     * Notifies the GameController that the whole presentation for a Spin Phase Result
     * (all individual spins, all wins) has been shown to the player: there is nothing
     * more for the view to do. This implicitly passes state control flow back to the
     * GameController.
     * @private
     */
    notifyControllerThatSpinPhasePresentationIsFinished()
    {
        this.log.info('ReelGroup.notifyControllerThatSpinPhasePresentationIsFinished()');

        if (this._currSpinPhase === C_SpinPhase.SPIN_1) {
            this.dispatcher.emit(C_GameEvent.SPIN_1_PHASE_RESULTS_SHOWN);
        }
        else
        if (this._currSpinPhase === C_SpinPhase.SPIN_2) {
            this.dispatcher.emit(C_GameEvent.SPIN_2_PHASE_RESULTS_SHOWN);
        }
        else
        if (this._currSpinPhase === C_SpinPhase.FREESPIN) {
            this.dispatcher.emit(C_GameEvent.FREESPIN_PHASE_RESULTS_SHOWN);
        }
    };


    /**
     * Shows a looping winning sequence for idle state.
     * @protected This method may be overridden by a subclass of ReelGroup, when a custom
     * idle winnings sequence is required.
     */
    showIdleWinningsSequence()
    {
        this.log.info('ReelGroup.showIdleWinningsSequence()');

        let currSpinResultPresentation = this.getCurrentSpinResultPresentation();

        /** @type {SymbolWinPresentation[]} */
        let idleSymbolWinPresentations = currSpinResultPresentation.idleWins;

        // If there are more than 1 win to show, we repeat infinitely, otherwise: if only one win,
        // we do not need to repeat at all.
        let numRepeats = idleSymbolWinPresentations.length > 1? -1 : 0;

        let numSymbolWinsShown = 0;
        let winDuration = this._spinConfig.idleSymbolWinDuration;
        
        let idleWinAnimTimeline = new TimelineMax({repeat:numRepeats});
        let isQuickWin = false;

        // Initially, reset the "current win counter"
        idleWinAnimTimeline.call(() => numSymbolWinsShown = 0);

        idleSymbolWinPresentations.forEach(idleSymbolWinPresentation => {
            idleWinAnimTimeline.call(() => {
                this.showIdleSymbolWin(idleSymbolWinPresentation);
                this.spinSoundController.playSymbolWinSound(idleSymbolWinPresentation, isQuickWin, numSymbolWinsShown);
                numSymbolWinsShown += 1;
            });

            idleWinAnimTimeline.to({}, winDuration, {});
        });
        
        /**
         * A TimelineMax instance, that contains a looping animation of the idle win animations.
         * @private
         * @type {gsap.TimelineMax}
         */
        this._idleWinAnimTimeline = idleWinAnimTimeline;
    };


    /**
     * Shows a single Symbol Win Presentation, in the Idle Win sequence. Broken out into a separate
     * method, so that we can override it in extended implementations of ReelGroup (eg: for Reel
     * Group's that support multi-hand wins).
     * @protected
     * @param {SymbolWinPresentation} symbolWinPresentation 
     */
    showIdleSymbolWin(symbolWinPresentation) {
        let showSymbolAnimations = true;
        let fadeNonWinningSymbols = true;
        this.highlightSymbolWin(symbolWinPresentation.win, showSymbolAnimations, fadeNonWinningSymbols);
    };


    /**
     * Kills an idle winnings sequence that may be in progress.
     * @protected This method may be overridden by a subclass of ReelGroup, when a custom
     * idle winnings sequence is required (and the mechanism to stop it repeating must
     * change).
     */
    killIdleWinningsSequence()
    {
        this.log.info('ReelGroup.killIdleWinningsSequence()');

        if (this._idleWinAnimTimeline)
        {
            this._idleWinAnimTimeline.kill();
            this._idleWinAnimTimeline = null;
        }

        // Reset all reels to idle visual state as well, to stop any win animations that might
        // still have been in progress.
        this._reels.forEach(reel => {
            reel.showIdleState();
        });
    };


    /**
     * Checks if the Quick Win sequence should be shown.
     * @private
     * @return {boolean}
     * Returns true of the quick win sequence should be shown, or false if the full win sequence is
     * required.
     */
    shouldShowQuickWinSequenceForCurrentSpinResult() {
        return this._model.isAutoplayInProgress();
    }


    /**
     * Shows the total winnings, for a single discrete win (eg: a Symbol Win). This method
     * should be called for all symbol wins by default (this method will decide whether a
     * win value should be shown, and what type, and will delegate to the appropriate mechanism
     * to do this, based on the win data passed).
     * @private
     * @param {SlotWin} win
     * @param {number | null} [symbolPositionIndex]
     * An optional symbol position, at which the win value should be shown. This is in "visible"
     * range, eg: if there are 3 visible symbols, then a value of 1, indicates that the win should
     * be shown vertically half way down the 2nd visible symbol on the reel. When this parameter is
     * not passed, then a default vertical y position will be picked.
     */
    showSingleWinValue(win, reelIndex=null, symbolPositionIndex=null)
    {
        // For now, nothing to show for bonus win..
        if (win.rewardGroup === C_RewardGroup.BONUS) {
            return;
        }
        
        // RUSSELL'S TODO: arguably, we need to add some
        // smarter logic here, based on "rewardType". This
        // wants testing with a game that uses multipliers.

        let targetPosX, targetPosY;
        
        if (reelIndex !== null && symbolPositionIndex !== null) {
            let symbolBounds = this.reelPositionUtil.getBoundsOfSymbolInScreenSpace(reelIndex, symbolPositionIndex);
            targetPosX = symbolBounds.centerX;
            targetPosY = symbolBounds.centerY;
        }
        else
        {
            let reelsBounds = this.reelPositionUtil.getBoundsOfReelsInScreenSpace();
            targetPosX = reelsBounds.centerX;
            targetPosY = reelsBounds.centerY;
        }

        // TODO: This value needs to be moved to the main config object somewhere
        let winDurationSecs = 1;    
        let winVal = this.gameFactory.getWinValue(win.rewardValue, winDurationSecs);
        
        // We add the winValue to the stage, so that it's the reels frame and any other layers.
        // Russells note: This isn't a great thing to do in my opinion: ideally, perhaps the
        // dedicated WinPresentationView would handle this (we would simply tell it to show
        // a win, and abstractly tell it where the win needs to be). For now this works ok, so
        // this is a change for a future update.
        this.app.stage.addChild(winVal);

        // Create and position the winline. The api of "winValue" says that its origin
        // is its top left, so we need to manually offset it (so that its center is
        // placed at the target x / positions)
        winVal.create();
        winVal.x = targetPosX - (winVal.width * 0.5);
        winVal.y = targetPosY - (winVal.height * 0.5);
        winVal.playAnimation();

        this._winValueAnimation = winVal;

        this.recordSlotWinValueToModel(win);
    };


    /**
     * Records a Slot Win value to the model.
     * @protected
     * @final
     * @param {SlotWin} win 
     */
    recordSlotWinValueToModel(win)
    {
        // RUSSELL'S TODO: I think this can be moved out of this code. We could only
        // update the winnings when the final amount (total win) is shown for the
        // reward group. This is actually a quite neat solution to the overall problem,
        // but will Sam like it ??
        if (win.rewardGroup === C_RewardGroup.CREDIT) {
            this._currSpinResultWinPresentation.creditWon += win.rewardValue;
            this._model.recordCreditWin(win.rewardValue);
        }
        else
        if (win.rewardGroup === C_RewardGroup.SUPERBET) {
            this._currSpinResultWinPresentation.superbetWon += win.rewardValue;
            this._model.recordSuperBetWin(win.rewardValue);
        }
        else
        if (win.rewardGroup === C_RewardGroup.FREESPIN) {
            this._currSpinResultWinPresentation.numFreeSpinsWon += win.rewardValue;
            this._model.recordFreeSpinsWin(win.rewardValue);
        }
    };


    /**
     * Records a winning result's values to the model.
     * @protected
     * @final
     * @param {WinningResult} winningResult 
     */
    recordWinningResultValuesToModel(winningResult)
    {
        if (winningResult.totalCreditWon > 0) {
            this._model.recordCreditWin(winningResult.totalCreditWon);
            this._currSpinResultWinPresentation.creditWon += winningResult.totalCreditWon;
        }

        if (winningResult.totalSuperBetWon > 0) {
            this._model.recordSuperBetWin(winningResult.totalSuperBetWon);
            this._currSpinResultWinPresentation.superbetWon += winningResult.totalSuperBetWon;
        }

        // TODO: Add in FreeGames ? And other types ?
    };


    // TODO: I think this method does NOT need to be overridable, or protected.
    /**
     * Immediately shows a Total Win Animation for a specific reward group, illustrating the total
     * amount won for a specific Reward Group. Once the Total Win animation has completed, the
     * onTotalWinComplete callback will be executed.
     * @protected
     * This method may be overridden by a game-specific sub-class, if there is a requirement to
     * fundamentally modify the way that the total winnings sequence is implemented.
     * @param {number} rewardGroup
     * The RewardGroup to show total winnings for. This must be a value specied in the RewardGroup
     * constants. @see C_RewardGroup
     * @param {number} amountWon 
     * The total amount won (for the RewardGroup in question).
     * @param {() => void} onTotalWinComplete
     * A callback, executed once the Total Win animation has completed.
     */
    showTotalWonForRewardGroup(rewardGroup, amountWon, onTotalWinComplete)
    {
        this.log.info(`ReelGroup.showTotalWonForRewardGroup(${rewardGroup},amountWon:${amountWon})`);

        this.dispatcher.emit(C_GameEvent.TOTAL_WIN_SHOWN);

        let totalWinningsPopup = this.gameFactory.getTotalWinningsPopup();
        totalWinningsPopup.create(rewardGroup, amountWon);
        totalWinningsPopup.once(C_TotalWinViewEvent.COMPLETE, onTotalWinComplete);
        this.parent.parent.addChild(totalWinningsPopup);

        this._totalWinAnimation = totalWinningsPopup;
    };


    /**
     * Hights the symbols involved in a specific SymbolWin.
     * @protected
     * @final
     * @param {SymbolWin} symbolWin
     * A Symbol Win instance, which describes the win which is being shown.
     * @param {boolean} playAnimation
     * Indicates if the symbol's win animation should also be played.
     * @param {boolean} fadeNonWinningSymbols
     * Indicates if Symbol Graphics not involved in the win should be shown
     * in a faded state (so that the winning Symbol Graphics stand out more)
     */
    highlightSymbolWin(symbolWin, playAnimation, fadeNonWinningSymbols)
    {
        this.log.debug(`ReelGroup.highlightSymbolWin(win:${JSON.stringify(symbolWin)},playAnimation:${playAnimation},fadeNonWinningSymbols:${fadeNonWinningSymbols})`);

        // This logic tells the Reel to show a winning state, and
        // passes a map of symbols positions that want highlighting.
        this._reels.forEach((reelView, reelIndex) => {
            let winningSymMapForReel = symbolWin.winningPositionsMap[reelIndex];
            if (playAnimation) {
                reelView.showIdleWinState(winningSymMapForReel, fadeNonWinningSymbols);
            }
            else
            {
                reelView.showQuickWinState(winningSymMapForReel, fadeNonWinningSymbols);
            }
        });
    };


    /**
     * Sets symbols in to "BigWin" visual state, according to a map.
     * @protected
     * @final
     * @param {boolean[][][]} symbolMap 
     * @param {boolean} fadeOtherSymbols 
     */
    showBigWinStateOnSymbols(symbolMap, fadeOtherSymbols)
    {
        this.log.debug('ReelGroup.showBigWinStateOnSymbols()');

        let primaryHandMap = symbolMap[0];

        this._reels.forEach((reelView, reelIndex) => {
            let highlightSymForReel = primaryHandMap[reelIndex];
            reelView.showBigWinState(highlightSymForReel, fadeOtherSymbols);
        });
    };


    /**
     * Sets symbols in to "QuickWin" visual state, according to a map.
     * @protected
     * @final
     * @param {boolean[][][]} symbolMap 
     * @param {boolean} fadeOtherSymbols 
     */
    showQuickWinStateOnSymbols(symbolMap, fadeOtherSymbols)
    {
        this.log.debug('ReelGroup.showQuickWinStateOnSymbols()');

        let primaryHandMap = symbolMap[0];

        this._reels.forEach((reelView, reelIndex) => {
            let highlightSymForReel = primaryHandMap[reelIndex];
            reelView.showQuickWinState(highlightSymForReel, fadeOtherSymbols);
        });
    };


    /**
     * Sets symbols in to "IdleWin" visual state, according to a map.
     * @protected
     * @final
     * @param {boolean[][][]} symbolMap 
     * @param {boolean} fadeOtherSymbols 
     */
    showIdleWinStateOnSymbols(symbolMap, fadeOtherSymbols)
    {
        this.log.debug('ReelGroup.showIdleWinStateOnSymbols()');

        let primaryHandMap = symbolMap[0];

        this._reels.forEach((reelView, reelIndex) => {
            let highlightSymForReel = primaryHandMap[reelIndex];
            reelView.showIdleWinState(highlightSymForReel, fadeOtherSymbols);
        });
    };
    


    /**
     * Sets all symbols to a faded state.
     * @protected
     * @final
     */
    fadeAllSymbols()
    {
        this._reels.forEach(reelView => {
            reelView.showFadeState();
        });
    };


    /**
     * Plays any Spin 2 idle sounds, when we enter the Spin 2 idle visual state.
     * @private
     */
    playSpin2IdleSounds()
    {
        let symbols = this.spinModel.getFinalSymbolIds();
        this.spinSoundController.playSpin2IdleSound(symbols);
    };


    // TODO: Here is a use case for special win presentation, eg: "dropping chickens" is a Fowl Play Gold
    // Win Presentation Layer implementation. This method needs some refactoring, because it should be
    // coordinating what it does with the Win Presentation Layer (eg: Win Presentation Layer will be
    // responsible for timing ?)
    /**
     * Shows the Bonus Scatter win sequence to the player. This sequence is shown while the game client is
     * fetching Bonus Phase Results, immediately before the bonus is launched. It is distinct from showing
     * any wins belonging to the Bonus Reward Group (in this sequence, we may highlight the same symbols,
     * but we are showing more detailed win information, which could expand in the future to include special
     * types of win that affect the results awarded in the bonus).
     * 
     * Bonus Scatter Win sequence is simply an attract mode, to distract the player from any pause that
     * occurs while we fetch bonus phase results. So, all we have to do, is highlight (for any hand reported
     * as triggering a bonus win) all scatter symbols on it. Some games may do some special animation here.
     * @private
     */
    showBonusScatterWinSequence()
    {
        let mainHandHasBonusWin = this._spinModel.getPhaseResults().hasBonusWinPerHand[0];
        let symbols = this.getCurrentSpinResult().finalSymbolIds;
        let bonusSymbolMap = this._spinUtils.getMapOfSymbolsMatchingId(this.config.bonusSymId, symbols);
        let scatterWinDuration = this._spinConfig.scatterWinDuration;

        this.log.info(`ReelGroup.showBonusScatterWinSequence(): should be showing ${JSON.stringify(bonusSymbolMap)}`);
        
        if (mainHandHasBonusWin)
        {
            this.showIdleWinStateOnSymbols(bonusSymbolMap, true);
        }
        
        TweenMax.delayedCall(
            scatterWinDuration,
            this.dispatcher.emit,
            [C_GameEvent.SCATTER_WIN_SEQUENCE_SHOWN],
            this.dispatcher);
    };


    /**
     * Builds and returns a fresh instance of the "Spin Phase Result Presentation Model". This is basically
     * a wrapper object for "Spin Phase Result Presentation", which some extra meta-data attached : this
     * meta-data is the "transitive" data, describing current active state
     * - spin phase presentation describes WHAT to show
     * - extra "transitive" data describes where we currently are in showing it.
     * This model is for the consumption of outside parties interested in what is going on in ReelGroup:
     * first and foremost, "WinPresentationView", which often needs useful data to show its hook sequences.
     * @protected
     * @return {SpinPhasePresentationStateModel}
     */
    getNewSpinPhasePresentationStateModel() {
        return {
            roundIndex : this._currentSpinRoundIndex,
            spinIndex : this._currentSpinResultIndex,
            spinPhase : this._currSpinPhase
        }
    };


    /**
     * Returns the Spin Round result currently being shown to the player. If no results are available, this
     * will return null.
     * @protected
     * @return {SpinRoundResult | null}
     */
    getCurrentRoundResult()
    {
        let currRoundResult = null;
        let phaseResults = this._spinModel.getPhaseResults();
        if (phaseResults) {
            currRoundResult = phaseResults.rounds[this._currentSpinRoundIndex];
        }
        return currRoundResult;
    };


    /**
     * Returns the presentation for the current Spin Round that is being shown. If no spin or
     * freespin phase is currently in progress (but results from a recent phase are available),
     * then this will return the last SpinRound Presentation shown. Otherwise, it must return
     * null.
     * @protected
     * @return {SpinRoundResultPresentation | null}
     */
    getCurrentSpinRoundPresentation() {
        let currRoundPresentation = null;
        let phasePresentation = this._spinModel.getResultsPresentation();
        if (phasePresentation) {
            currRoundPresentation = phasePresentation.rounds[this._currentSpinRoundIndex];
        }
        return currRoundPresentation;
    };


    /**
     * Returns the spin result currently being shown to the player. If no results are available, this will
     * return null.
     * @protected
     * @return {SpinResult | null}
     */
    getCurrentSpinResult()
    {
        let currSpinResult = null;
        let phaseResults = this._spinModel.getPhaseResults();
        if (phaseResults) {
            currSpinResult = phaseResults.rounds[this._currentSpinRoundIndex].spins[this._currentSpinResultIndex];
        }
        return currSpinResult;
    };


    /**
     * Returns the presentation for the current Spin Result that is being shown. If no spin or
     * freespin phase is currently in progress (but results from a recent phase are available),
     * then this will return the last SpinResult Presentation shown. Otherwise, it must return
     * null.
     * @protected
     * @return {SpinResultPresentation | null}
     */
    getCurrentSpinResultPresentation() {
        let currSpinPresentation = null;
        let currRoundPresentation = this.getCurrentSpinRoundPresentation();
        if (currRoundPresentation) {
            currSpinPresentation = currRoundPresentation.spins[this._currentSpinResultIndex];
        }
        return currSpinPresentation;
    };


    /**
     * Finds the number of Symbol Wins (for the current spin result) that match a specific reward group.
     * This refers to ALL Symbol Wins (regardless of hand).
     * @protected
     * @param {number} rewardGroup 
     * @return {number}
     */
    getNumSymbolWinsWithRewardGroup(rewardGroup) {
        let currSpinResult = this.getCurrentSpinResult();
        let symbolWins = currSpinResult.symbolWins;
        let numMatchingSymbolWins = 0;

        symbolWins.forEach(symbolWin => {
            if (symbolWin.rewardGroup === rewardGroup) {
                numMatchingSymbolWins += 1;
            }
        });

        return numMatchingSymbolWins;
    };


    /**
     * Determines the total won for a reward group, for the current spin, only from symbol wins.
     * @pivate
     * @param {number} rewardGroup 
     * @return {number}
     */
    getTotalWonForRewardGroupFromSymbolWins(rewardGroup) {
        let currSpinResult = this.getCurrentSpinResult();
        let symbolWins = currSpinResult.symbolWins;
        let totalWonForRewardGroup = 0;

        symbolWins.forEach(symbolWin => {
            if (symbolWin.rewardGroup === rewardGroup) {
                totalWonForRewardGroup += symbolWin.rewardValue;
            }
        });

        return totalWonForRewardGroup;
    };


    /**
     * Updates all primary hand symbols to a specific set of ids.
     * @protected
     * @param {SymbolsMatrix} symbolsMatrix
     * The set of symbols that all symbols should be synced to.
     */
    setAllSymbolsTo(symbolsMatrix)
    {
        let numReelViews = this._reels.length;

        symbolsMatrix[0].forEach((setOfSymbolsOnReel, reelIndex) => {
            if (reelIndex < numReelViews)
            {
                let reelView = this._reels[reelIndex];

                reelView.iterateSymbols((symbolView, inVisibleArea, visibleSymIndex) => {
                    if (inVisibleArea) {
                        let symbolConfig = setOfSymbolsOnReel[visibleSymIndex];
                        symbolView.setId(symbolConfig.id);
                    }
                });
            }
        });
    };


    /**
     * 
     * @private
     */
    stopBgMusicDuringTotalWinIfRequired()
    {
        /** @type {LoopingMainGameBgMusicConfig} */
        let bgMusicConfig = this.config.music.mainGameBgMusic;

        if (bgMusicConfig && bgMusicConfig.suspendDuringSlotTotalWin) {
            this.sound.stopMusic();
        }
    };


    /**
     * @private
     */
    stopBgMusicDuringBigWinIfRequired()
    {
        /** @type {LoopingMainGameBgMusicConfig} */
        let bgMusicConfig = this.config.music.mainGameBgMusic;

        if (bgMusicConfig && bgMusicConfig.suspendDuringSlotBigWin) {
            this.sound.stopMusic();
        }
    };


    /**
     * @private
     */
    restoreBgMusicAfterBigWinIfRequired()
    {
        /** @type {LoopingMainGameBgMusicConfig} */
        let bgMusicConfig = this.config.music.mainGameBgMusic;
        
        if (bgMusicConfig && bgMusicConfig.restoreAfterSlotBigWin) {
            this.sound.restoreMainGameMusic();
        }
    };


    /**
     * @private
     */
    restoreBgMusicAfterTotalWinCompleteIfRequired()
    {
        /** @type {LoopingMainGameBgMusicConfig} */
        let bgMusicConfig = this.config.music.mainGameBgMusic;
        
        if (bgMusicConfig && bgMusicConfig.restoreAfterTotalWinComplete) {
            this.sound.restoreMainGameMusic();
        }
    }
}

export default ReelGroup