import BaseView from "./BaseView";
import Button from "../ui/Button";
import C_PointerEvt from "../const/C_PointerEvt";
import C_GameEvent from "../../../cms-core-logic/src/const/C_GameEvent";
import * as LogManager from "../../../cms-core-logic/src/logging/LogManager";

// TODO: There is a disconnect right now between how this class works, and how it should work.
// DialogParams is not very helpfull - partly because there is no configuration for sizing
// and positioning assets (as soon as you change a button texture, you potentially have a problem
// with how it gets sized and positioned). Being able to configure this class from a data object
// is fine though. In any case, I don't think we want to change this class between games - and if
// we do, i suspect that we may want to use a different implementation - OR we need the config
// class to be much smarter.
/**
 * @class DialogView - Creates dialog windows
 * Works with dialogParams object from Config.js
 */
class DialogView extends BaseView
{
    /**
     * @param {DialogViewModel} dialogVm
     */
    constructor(dialogVm)
    {
        super();

        this.log = LogManager.getDefaultLogger();
        this.log.debug(`DialogView.constructor(type:${dialogVm.type})`);

        this.createDialog(dialogVm);

        this.addListener(this.dispatcher, C_GameEvent.HIDE_DIALOG_VIEW, this.closeDialog, this);
    }


    /**
     * @private
     * @param {DialogViewModel} dialogVm 
     */
    createDialog(dialogVm)
    {
        let dialogStyle = this.app.config.dialogStyle;
        
        let textHeight = Math.round(this.layout.relHeight(dialogStyle.textHeight));
        let dialogHeight = Math.round(this.layout.relHeight(dialogStyle.dialogHeight));
        let dialogWidth  = Math.round(dialogHeight * dialogStyle.dialogAspect);
        let maxTextWidth = Math.round(dialogWidth * 0.9);
        let headerHeight = this.layout.relHeight(dialogStyle.headerHeight);
        let footerHeight = this.layout.relHeight(dialogStyle.footerHeight);
        let bodyHeight = dialogHeight - headerHeight - footerHeight;
        let headerRect = new PIXI.Rectangle(0, 0, dialogWidth, headerHeight);
        let bodyRect = new PIXI.Rectangle(0, headerHeight, dialogWidth, bodyHeight);
        let footerRect = new PIXI.Rectangle(0, headerHeight + bodyHeight, dialogWidth, footerHeight);
        
        /**
         * A dark semi-transparent background, that partially hides anything else shown
         * beneath the Dialog.
         * @private
         */
        this.darkBg = this.assets.getSprite('popup_bg');
        this.darkBg.interactive = true;
        this.layout.setToRectangle(this.darkBg, 0, 0, 100, 100);
        this.addChild(this.darkBg);

        /**
         * The main container for the Dialog content.
         * @private
         */
        this.dialogGroup = new PIXI.Container();
        this.addChild(this.dialogGroup);

        // Main background to the dialog
        
        let dialogBg = new PIXI.Graphics();
        dialogBg.beginFill(dialogStyle.bgColour);
        dialogBg.drawRect(0, 0, dialogWidth, dialogHeight);
        dialogBg.endFill();
        this.dialogGroup.addChild(dialogBg);

        let bodyTextString = this.getBodyTextString(dialogVm);

        this.log.debug(`DialogView: bodyTextString = ${bodyTextString}`);
        
        let bodyTextField = new PIXI.Text(bodyTextString, {
            fontFamily : dialogStyle.font,
            fill : dialogStyle.bodyTxtColour,
            fontSize : textHeight,
            align : "center",
            wordWrap : true,
            wordWrapWidth : maxTextWidth
        });
        
        this.layout.limitSize(bodyTextField, 90, 90, bodyRect);
        this.layout.horizontalAlignCenter(bodyTextField, 0, bodyRect);
        this.layout.verticalAlignMiddle(bodyTextField, 0, bodyRect);
        this.dialogGroup.addChild(bodyTextField);

        
        let rawHeaderTextString = this.getHeaderString(dialogVm);
        let headerTextString = rawHeaderTextString.length > 40?
            `${rawHeaderTextString.slice(0, 37)}...` : rawHeaderTextString;

        this.log.debug(`DialogView: headerTextString = ${headerTextString}`);

        let headerTextField = new PIXI.Text(headerTextString, {
            fontFamily : dialogStyle.font,
            fill : dialogStyle.bodyTxtColour,
            fontSize : textHeight
        });
        headerTextField.anchor.set(0.5);
        //headerTextField.y = dialogBg.y + (dialogStyle.textHeight * 0.5);
        this.layout.horizontalAlignCenter(headerTextField, 0, headerRect);
        this.layout.verticalAlignMiddle(headerTextField, 0, headerRect);
        this.dialogGroup.addChild(headerTextField);

        if (headerTextField.width > maxTextWidth) {
            this.layout.setWidth(headerTextField, maxTextWidth);
        }

        // TODO: Now, let's clamp the size of the header text: its not multi-line formatted, so it can get
        // too big, as see with recent cases for.. Vincitu? testing

        let numFooterBtns = 0;
        if (dialogVm.type === "multiChoice") {
            numFooterBtns = dialogVm.options.length;
        }
        else
        if (dialogVm.type === "dismissableNotification" ||
            dialogVm.type === "connectionError")
        {
            numFooterBtns = 1;
        }
        // TODO: Handle the other type of notifications..

        // Determining footer button size is a little tricky. We shouldn't use an exact size, because
        // we might have some long text strings. If there is only a single footer button, then this
        // doesn't present an issue - we can let the button be as wide as needed, up to a certain maximum
        // width, and this will look fine. If we have more than 1 button, we simply set a target width
        // which all buttons will conform to.
        let minBtnWidth, maxBtnWidth;
        let useExactBtnWidth = false;
        if (numFooterBtns === 1) {
            maxBtnWidth = Math.round(dialogWidth * 0.8);
            minBtnWidth = Math.round(dialogWidth * 0.4);
            useExactBtnWidth = false;
        }
        else
        {
            maxBtnWidth = minBtnWidth = Math.round(dialogWidth * 0.8 / numFooterBtns);
            useExactBtnWidth = true;
        }

        

        /**
         * @private
         * @param {DialogTextButtonConfig} buttonConfig 
         * @return {PIXI.DisplayObject}
         */
        let createFooterBtn = buttonConfig =>
        {
            let btnGroup = new PIXI.Container();
            let btnText = this.locale.getString(buttonConfig.localizationId, buttonConfig.localizationMods);
            let textWidthRatio = 0.9;
            let maxTextWidth = maxBtnWidth * 0.9;
            let btnHeight = Math.round(this.layout.relHeight(dialogStyle.btnHeight));
            let btnRadius = btnHeight * 0.5;

            // Create text at target height. We will measure how wide it is,
            // and determine if we need to do anything special
            let btnTextGraphic = new PIXI.Text(btnText, {
                fontFamily : dialogStyle.font,
                fontSize : textHeight,
                fill : dialogStyle.buttonTxtColour
            });
            btnTextGraphic.anchor.set(0.5, 0.5);
            this.layout.setHeight(btnTextGraphic, textHeight);
            
            let btnWidth;
            if (useExactBtnWidth) {
                btnWidth = maxBtnWidth;
            }
            else
            {
                // If not using exact width (eg: there is only one button), we size the button dynamically.
                // It can be up to maxBtnWidth, but must be at least minBtnWidth, and we try and target the
                // size of the text plus padding.
                btnWidth = Math.max(minBtnWidth, Math.min(maxBtnWidth, btnTextGraphic.width / textWidthRatio));
            }

            let btnGraphic = new PIXI.Graphics();
            btnGraphic.beginFill(dialogStyle.btnColour);
            btnGraphic.drawRoundedRect(0, 0, btnWidth, btnHeight, btnRadius);
            btnGraphic.endFill();
            btnGraphic.interactive = true;
            btnGraphic.buttonMode = true;

            if (btnTextGraphic.width > maxTextWidth) {
                this.layout.setWidth(btnTextGraphic, maxTextWidth);
            }

            btnTextGraphic.x = btnGraphic.x + btnGraphic.width  * 0.5;
            btnTextGraphic.y = btnGraphic.y + btnGraphic.height * 0.5;

            btnGroup.addChild(btnGraphic);
            btnGroup.addChild(btnTextGraphic);
            this.layout.verticalAlignMiddle(btnGroup, 0, footerRect);

            btnGraphic.on(C_PointerEvt.DOWN, () => {
                this.log.debug(`pressed, executing action for dialog footer button`);
                this.closeDialog();
                if (buttonConfig.action) {
                    buttonConfig.action();
                }
            });

            return btnGroup;
        }

        // Create any footer buttons that are required.
        let footerButtons = [];
        if (dialogVm.type === "multiChoice") {
            dialogVm.options.forEach((footerButtonConfig, footerButtonIndex) => {
                let footerButton = createFooterBtn(footerButtonConfig);

                footerButtons.push(footerButton);
            });
        }
        // TODO: There are other cases where we need to add a single OK button
        else
        if (dialogVm.type === "dismissableNotification") {
            this.log.debug(`DialogView: is dismissableNotification, add "closeDialog" footer button`);

            let footerButton = createFooterBtn({
                localizationId : "T_button_ok",
                action : () => {
                    this.closeDialog();
                }
            });

            footerButtons.push(footerButton);
        }
        else
        if (dialogVm.type === "connectionError") {
            let buttonTextConfig = dialogVm.buttonText;

            let footerButton = createFooterBtn({
                localizationId : buttonTextConfig? buttonTextConfig.localizationId : "T_button_ok",
                localizationMods : buttonTextConfig? buttonTextConfig.localizationMods : null,
                action : () => {
                    this.closeDialog();
                }
            });

            footerButtons.push(footerButton);
        }

        // Layout all of the footer buttons
        if (footerButtons.length === 1)
        {
            let button = footerButtons[0];
            this.layout.horizontalAlignCenter(button, 0, footerRect);
            this.dialogGroup.addChild(button);
        }
        else
        {
            footerButtons.forEach((footerButton, footerButtonIndex) => {
                let propWidthBetween = 1 / numFooterBtns;
                let propOffsetX = propWidthBetween * 0.5;
                let propPosX = propOffsetX + (propWidthBetween * footerButtonIndex);

                footerButton.x = footerRect.x + (footerRect.width * propPosX) - (maxBtnWidth * 0.5);    

                this.dialogGroup.addChild(footerButton);
            });
        }
        
        // Centering the dialog container so it can be tweened properly
        this.dialogGroup.pivot.x = dialogWidth * 0.5;
        this.dialogGroup.pivot.y = dialogHeight * 0.5;

        this.dialogGroup.x += dialogWidth;
        this.dialogGroup.y += dialogHeight;

        let tweenTime = 0.2;
        TweenMax.fromTo(this.darkBg, tweenTime, { alpha:0 }, { alpha:0.7 });
        TweenMax.fromTo(
            this.dialogGroup,
            tweenTime,
            { alpha:0, width: this.dialogGroup.width * 0.85, height: this.dialogGroup.height * 0.85 },
            { alpha:1, width: this.dialogGroup.width,        height: this.dialogGroup.height });
    }


    /**
     * Closes the dialog view with animation, before invoking the close button action once
     * the animation is completed. This method will only be called when the top right X button
     * is added to the dialog view.
     * @private
     * @param {() => void} [actionOnHidden]
     * An optional callback to be executed once the dialog view "hide" animation is finished.
     */
    closeDialog(actionOnHidden)
    {
        this.dispatcher.removeListener(C_GameEvent.HIDE_DIALOG_VIEW);

        let tweenTime = 0.2;

        TweenMax.to(this.darkBg, tweenTime, { alpha:0 });

        TweenMax.to(
            this.dialogGroup,
            tweenTime,
            {
                width:  this.dialogGroup.width  * 0.85,
                height: this.dialogGroup.height * 0.85,
                alpha : 0,
                onComplete: () => {
                    this._dispatcher.emit(C_GameEvent.DIALOG_VIEW_DISMISSED);

                    // TODO: Will we still need this ?
                    if (actionOnHidden) {
                        actionOnHidden();
                    }
                    super.destroy();
                },
                onCompleteScope:this
            });
    };


    /**
     * Returns the most appropriate text string to show for header, ready localized.
     * @private
     * @param {DialogViewModel} dialogVm
     * @return {string}
     */
    getHeaderString(dialogVm)
    {
        let headerTextString = "";

        if (dialogVm.headerString) {
            headerTextString = dialogVm.headerString;
        }
        else
        if (dialogVm.header)
        {
            let headerTextConfig = dialogVm.header;
            
            headerTextString = this.locale.getString(
                headerTextConfig.localizationId,
                headerTextConfig.localizationMods);
        }

        return headerTextString;
    };


    /**
     * Returns the most appropriate text string to show for main body message, ready localized.
     * @private
     * @param {DialogViewModel} dialogVm 
     * @return {string}
     */
    getBodyTextString(dialogVm)
    {
        let bodyTextString = "";

        if (dialogVm.messageString) {
            bodyTextString = dialogVm.messageString;

            this.log.debug(`DialogView.getBodyTextString: return messageString field (${bodyTextString})`);
        }
        else
        if (dialogVm.message)
        {
            let bodyTextConfig = dialogVm.message;

            this.log.debug(`DialogView.getBodyTextString: return localized message (config:${JSON.stringify(bodyTextConfig)})`);
            
            bodyTextString = this.locale.getString(
                bodyTextConfig.localizationId,
                bodyTextConfig.localizationMods);
        }

        return bodyTextString;
    };
}
export default DialogView