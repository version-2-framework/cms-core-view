import BaseView from "./BaseView";

/**
 * Default implementation of the GameLogo component - for use on desktop or mobile.
 * Simply shows a single texture, within the target bounded area of the screen.
 */
export default class DefaultGameLogo
    extends BaseView
{
    /**
     * @public
     * @param {number} centerPosX
     * @param {number} centerPosY
     * @param {number} targetHeight
     */
    constructor(centerPosX, centerPosY, targetHeight)
    {
        super();

        let textureId = this.config.uiStyle.gameLogoTexture;

        /**
         * @private
         * @type {PIXI.Sprite}
         */
        this._gameLogo = this.assets.getSprite(textureId);
        this._gameLogo.anchor.set(0.5, 0.5);
        this._gameLogo.x = centerPosX;
        this._gameLogo.y = centerPosY;
        this._gameLogo.name = "Gui.DefaultGameLogo";

        this.layout.setHeight(this._gameLogo, targetHeight);

        this.addChild(this._gameLogo);
    }
}