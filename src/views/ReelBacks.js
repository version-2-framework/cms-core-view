import BaseView from "./BaseView";
import C_PointerEvt from "../const/C_PointerEvt";
import C_GameEvent from "../../../cms-core-logic/src/const/C_GameEvent";
import * as KeyCode from "../input/KeyCode";
import * as KeyPhase from "../input/KeyPhase";

const NOT_HELD_TEXTURE_ID = "reelsBgColour";
const HELD_TEXTURE_ID = "reelsBgHeldColour";

const CROSS_FADE_DURATION = 0.12;

class ReelBacks extends BaseView
{
    constructor()
    {
        super();

        /**
         * @private
         * @type {number}
         */
        this._numReels = this.config.numReels;

        /**
         * Indicates if the holds pattern should be shown. This will be true when we are in some
         * kind of spin 2 related state.
         * @type {boolean}
         * @private
         */
        this._showingPattern = false;

        /**
         * Indicates if holds input is currently enabled, eg: if the player is currently allowed
         * to change the holds pattern. Sometimes, the pattern is shown (because we are in a spin
         * 2 related state), but we are not allowed to change the holds pattern (eg: while the
         * actual spin 2 is in progress). Both values need to be tracked separately.
         * @private
         * @type {boolean}
         */
        this._isHoldsInputEnabled = false;

        /**
         * Configuration for the reelbacks component.
         * @private
         * @type {SlotReelBacksConfig}
         */
        this._reelBacksConfig = this.app.config.slotReelBacksConfig;

        /**
         * Indicates of the "held layer" should be shown using an animated cross-fade.
         * @private
         * @type {boolean}
         */
        this._useCrossFade = this._reelBacksConfig.useCrossFade;
    };


    // TODO: I refactored it so that ReelGroup is passed in (which is more sane),
    // but does it need to be passed in at all ??
    /**
     * @public
     */
    create()
    {
        /**
         * Ordered list of reel background graphics - index 0 is left-most reelback, final index is
         * right-most reelback.
         * @private
         * @type {PIXI.Sprite[]}
         */
        this._reelNotHeldLayer = [];
        
        /**
         * Ordered list of reel background graphics - index 0 is left-most reelback, final index is
         * right-most reelback. This is the layer that we add touch events to.
         * @private
         * @type {PIXI.Sprite[]}
         */
        this._reelHeldLayer = [];

        let config = this.config;
        let layout = this.getActiveSlotLayout();

        let numReels = config.numReels;
        let currPosX = 0;
        let reelWidth = layout.symWidth;
        let reelGap = layout.reelGap;

        for (let reelIndex = 0; reelIndex < numReels; reelIndex++)
        {
            let numSyms = config.numSymsPerReel[reelIndex];
            let reelBackWidth = reelWidth;
            if (reelIndex === 0) {
                reelBackWidth += config.reelsMarginLeft + (reelGap * 0.5);
            }
            else
            if (reelIndex === numReels - 1) {
                reelBackWidth += config.reelsMarginRight + (reelGap * 0.5);
            }
            else
            {
                reelBackWidth += reelGap;
            }

            let reelBackHeight = numSyms * config.symHeight + ((numSyms - 1) * config.symGap) + config.reelsMarginTop + config.reelsMarginBottom;

            
            let reelNotHeldLayer = this.assets.getSprite(NOT_HELD_TEXTURE_ID);
            reelNotHeldLayer.x = currPosX;
            reelNotHeldLayer.width = reelBackWidth;
            reelNotHeldLayer.height = reelBackHeight;
            
            let reelHeldLayer = this.assets.getSprite(HELD_TEXTURE_ID);
            reelHeldLayer.x = currPosX;
            reelHeldLayer.width = reelBackWidth;
            reelHeldLayer.height = reelBackHeight;
            reelHeldLayer.visible = false;
            reelHeldLayer.alpha = 0;
            reelHeldLayer.reelIndex = reelIndex;
            
            this.addChild(reelNotHeldLayer);
            this.addChild(reelHeldLayer);

            this._reelNotHeldLayer.push(reelNotHeldLayer);
            this._reelHeldLayer.push(reelHeldLayer);

            currPosX += reelBackWidth;

            if (config.showReelSeparator)
            {
                if (reelIndex < numReels - 1)
                {
                    let reelSeparatorWidth = layout.reelsSeparatorWidth;
                    let reelSeparator = this.assets.getSprite("reelsDividerColour");
                    reelSeparator.anchor.set(0.5, 0);
                    reelSeparator.width = reelSeparatorWidth;
                    reelSeparator.height = reelBackHeight;
                    reelSeparator.x = currPosX;
                    this.addChild(reelSeparator);
                }
            }   
        }
        
        this.x = layout.reelsPosX;
        this.y = layout.reelsPosY;

        if (this._reelBacksConfig.allowKeyboardInput)
        {
            this.setupHoldsKeyboardInput();
        }

        this.dispatcher.on(C_GameEvent.HOLDS_PATTERN_CHANGED, this.showCurrentHoldsPattern, this);
        this.dispatcher.on(C_GameEvent.ENABLE_HOLDS_INPUT, this.enableHoldsInput, this);
        this.dispatcher.on(C_GameEvent.DISABLE_HOLDS_INPUT, this.disableHoldsInput, this);
        this.dispatcher.on(C_GameEvent.SHOW_SPIN2_HOLDS_PATTERN, this.enableShowingHoldsPattern, this);
        this.dispatcher.on(C_GameEvent.HIDE_SPIN2_HOLDS_PATTERN, this.disableShowingHoldsPattern, this);
    };


    /**
     * Enables keyboard input for changing the holds pattern. This attempts to use the keyboard number
     * buttons in order to toggle the state of individual reels. If there is ever a future use case
     * where we have a 2 spin game with more than 10 reels... this will become problematic!! However,
     * we can disable keyboard input for holds completely.
     * @private
     */
    setupHoldsKeyboardInput()
    {
        this.log.debug('ReelBacks.setupHoldsKeyboardInput()');

        let keyCodes = [
            KeyCode.NUMBER_1,
            KeyCode.NUMBER_2,
            KeyCode.NUMBER_3,
            KeyCode.NUMBER_4,
            KeyCode.NUMBER_5,
            KeyCode.NUMBER_6,
            KeyCode.NUMBER_7,
            KeyCode.NUMBER_8,
            KeyCode.NUMBER_9
        ];

        let numReels = this.app.config.numReels;

        if (numReels > keyCodes.length)
        {
            this.log.warn(`ReelBacks.setupHoldsKeyboardInput: there are ${numReels} reels: we don't have enough number keys! Keyboard input not enabled`);
        }
        else
        {
            for (let reelIndex = 0; reelIndex < numReels; reelIndex ++)
            {
                let keyCode = keyCodes[reelIndex];
                let keyPhase = KeyPhase.PRESSED;

                // For now, we don't need to remove these actions, because
                // we never destroy the slot view components (they exist
                // permanently on the display list until the game client
                // gets closed).
                this.keyboardManager.addKeyAction(keyCode, keyPhase, () => this.onReelKeyPressed(reelIndex));
            }
        }
    };


    /**
     * @private
     */
    enableHoldsInput()
    {
        this._isHoldsInputEnabled = true;

        // Play an optional sound when holds input is first enabled.
        if (this._reelBacksConfig.toggleHoldsSounds &&
            this._reelBacksConfig.toggleHoldsSounds.holdsEnabledSound)
        {
            let soundId = this._reelBacksConfig.toggleHoldsSounds.holdsEnabledSound;
            this.sound.playSound(soundId, false, 1);
        }

        this._reelHeldLayer.forEach(reelHeldLayer => {
            reelHeldLayer.interactive = true;
            reelHeldLayer.on(C_PointerEvt.DOWN, this.onReelTouched, this);
        });
    };


    /**
     * @private
     */
    disableHoldsInput()
    {
        this._isHoldsInputEnabled = false;

        this._reelHeldLayer.forEach(reelHeldLayer => {
            reelHeldLayer.interactive = false;
            reelHeldLayer.off(C_PointerEvt.DOWN, this.onReelTouched, this);
        });
    };
    

    /**
     * Updates the visual state of the holds pattern.
     * @private
     */
    enableShowingHoldsPattern()
    {
        this.log.debug('ReelBacks.showHoldsPattern()');

        if (this._showingPattern === false) {
            this._showingPattern = true;
            this.showCurrentHoldsPattern();

            // TODO: Consider hooks for when the pattern should be shown / hidden,
            // eg: when individual spin results are shown (this requires syncing
            // with ReelGroup). If the event of a 2 spin game, where there might
            // be respins during spin 2, then we would want this functionality to
            // exist, because its likely we don't want holds being shown all the
            // time.
        }
    };


    /**
     * Clears any holds pattern shown
     * @private
     */
    disableShowingHoldsPattern()
    {
        this.log.debug('ReelBacks.hideHoldsPattern()');

        if (this._showingPattern) {
            this._showingPattern = false;
            for (let reelIndex = 0; reelIndex < this._numReels; reelIndex ++) {
                this.setReelBgState(reelIndex, false, false);
            }
        }
    };


    /**
     * Shows the current holds pattern. Can be called to update the holds pattern shown
     * @private
     */
    showCurrentHoldsPattern() {
        let holdsPattern = this.spinModel.getPlayerHoldsPattern();
        if (holdsPattern.type === "holdReels") {
            let patternPerReel = holdsPattern.pattern[0];
            for (let reelIndex = 0; reelIndex < this._numReels; reelIndex ++) {
                let reelIsHeld = patternPerReel[reelIndex];
                this._reelHeldLayer[reelIndex].visible = true;
                this.setReelBgState(reelIndex, reelIsHeld, true);
            };
        }
        else
        if (holdsPattern.type === "holdSymbols") {
            // TODO: Implement showing "symbols" pattern. The way the reelbacks are built
            // will probably need to change, to support this scenario.
        }
    };


    /**
     * Invoked when a reel back is pressed.
     * @private
     * @param {{target:PIXI.Sprite & {reelIndex:number}}} touchEvent 
     */
    onReelTouched(touchEvent)
    {
        let reelBg = touchEvent.target;
        let reelIndex = reelBg.reelIndex;
        this.toggleReelHeld(reelIndex);
    };


    /**
     * Invoked when a keyboard button corresponding to a reel is pressed.
     * @private
     * @param {number} reelIndex
     * The index of the reel associated with the key press.
     */
    onReelKeyPressed(reelIndex)
    {
        if (this._isHoldsInputEnabled)
        {
            this.toggleReelHeld(reelIndex);
        }
    };


    /**
     * Toggles the reel held state for a specific reel, based on its reelIndex.
     * @private
     * @param {number} reelIndex 
     */
    toggleReelHeld(reelIndex)
    {
        // TODO: If we do introduce "multi hand holds patterns", this functionality
        // may need changing, or will certainly need some better documentation.
        let currHoldState = this.spinModel.getPlayerHoldsPattern().pattern[0][reelIndex];

        let newHoldState = !currHoldState;

        this.setReelBgState(reelIndex, newHoldState, true);

        // Play the most appropriate sound (not held or held sound)
        if (this._reelBacksConfig.toggleHoldsSounds)
        {
            let soundId;

            if (newHoldState)
            {
                soundId = this._reelBacksConfig.toggleHoldsSounds.heldSoundsPerReel[reelIndex];
            }
            else
            {
                soundId = this._reelBacksConfig.toggleHoldsSounds.unheldSoundsPerReel[reelIndex];
            }

            this.sound.playSound(soundId, false, 1);
        }

        this.spinModel.setReelHoldState(0, reelIndex, newHoldState);

        this.log.debug(`ReelBacks.toggleReelHeld: holds pattern updated to ${JSON.stringify(this.spinModel.getPlayerHoldsPattern())}`);

        this.dispatcher.emit(C_GameEvent.HOLDS_PATTERN_CHANGED);
    };


    /**
     * Updates the visual state of a specicif reel.
     * @private
     * @param {number} reelIndex
     * The index of the reel to update.
     * @param {boolean} isHeld
     * The new visual / held state of the reel.
     * @param {boolean} visibleAfter
     * Indicates if the reelBg should be visible, after any transition.
     */
    setReelBgState(reelIndex, isHeld, visibleAfter)
    {
        let reelHeldLayer = this._reelHeldLayer[reelIndex];

        if (this._useCrossFade)
        {
            // Kill any existing cross-fade, and start the new one. Instead of using
            // full target duration, we calculate how long the tween should last,
            // based on how much alpha actually has to change.
            let startAlpha = reelHeldLayer.alpha;
            let finalAlpha = isHeld? 1 : 0;
            let deltaAlpha = Math.abs(finalAlpha - startAlpha);
            let duration = deltaAlpha * CROSS_FADE_DURATION;
            let onComplete = !visibleAfter? () => reelHeldLayer.visible = false : null;
            
            TweenMax.killTweensOf(reelHeldLayer);
            TweenMax.to(reelHeldLayer, duration, { alpha:finalAlpha, ease:"Linear.easeNone", onComplete });
        }
        else
        {
            reelHeldLayer.alpha = isHeld? 1 : 0;
            reelHeldLayer.visible = visibleAfter;
        }
    };
}

export default ReelBacks