import BaseView from "./BaseView";
import {gsap} from "gsap";

class BaseBonusView extends BaseView
{

    constructor()
    {
        super();

        /**
         * @protected
         * @type {BonusPhaseModel}
         */
        this._bonusModel = this.app.bonusModel;

        /**
         * @private
         * @type {DialogController}
         */
        this._dialogController = this.app.dialogController;
    };


    /**
     * @protected
     * @returns {BonusPhaseModel}
     */
    get bonusModel()
    {
        return this._bonusModel;
    };


    /**
     * @public
     * @override
     * @inheritDoc
     */
    destroy()
    {
        super.destroy();

        this.cancelAutoSelectionTimer();
    };


    /**
     * Informs the server of a single selection. If the operation succeeds, then
     * onSucess is invoked. If the operation fails, then an error popup will be
     * displayed to the player (giving them the chance of a second attempt, if
     * that is possible).
     * @protected
     * @param {number} selectionId
     * @param {() => void} onSuccess
     * Callback executed when the server successfully completes.
     */
    informServerOfSingleSelection(selectionId, onSuccess)
    {
        this.log.info(`BaseBonusView.informServerOfSingleSelection(selectionId:${selectionId})`);

        let informServerOfSelection = () =>
        {
            this.app.services.sendBonusPhaseSelectionRequest(
                selectionId,
                () => {
                    this.log.debug(`BonusView: server acknowledges selection notification`);
                    if (onSuccess) onSuccess();
                },
                err => {
                    this.log.error(`BonusView: selection notification failed`);

                    this.showCommsErrorDialog(err, () => {
                        return this.informServerOfSingleSelection(selectionId, onSuccess);
                    });
                });
        };

        return informServerOfSelection();
    };


    /**
     * Informs the server of a multi selection. If the operation succeeds, then
     * onSucess is invoked. If the operation fails, then an error popup will be
     * displayed to the player (giving them the chance of a second attempt, if
     * that is possible).
     * @protected
     * @param {number[]} selectionIds
     * The list of selections to send to the server.
     * @param {() => void} onSuccess
     * Callback executed when the server successfully completes.
     */
    informServerOfMultiSelection(selectionIds, onSuccess)
    {
        this.log.info(`BaseBonusView.informServerOfMultiSelection(selectionIds:${selectionIds})`);

        let informServerOfSelection = () =>
        {
            this.app.services.sendBonusPhaseMultiSelectionRequest(
                selectionIds,
                () => {
                    this.log.debug(`BonusView: server acknowledges selection notification`);
                    if (onSuccess) onSuccess();
                },
                err => {
                    this.log.error(`BonusView: selection notification failed`);

                    this.showCommsErrorDialog(err, () => {
                        return this.informServerOfMultiSelection(selectionIds, onSuccess);
                    });
                });
        };

        return informServerOfSelection();
    };


    /**
     * Utility method that starts an auto-selection timer running, using default timings. Any existing
     * auto-selection timer will be killed and cleared when this method is called.
     * @protected
     * @param {() => void} makeSelection
     * Method which will execute an auto-matic selection.
     * @param {number} [standardDelay]
     * Delay to use when the game is not in autoplay, in seconds (optional)
     * @param {number} [autoplayDelay]
     * Delay to use when the game IS in autoplay, in seconds (optional)
     */
    startAutoSelectionTimer(makeSelection, standardDelay=20, autoplayDelay=10)
    {
        if (this._autoSelectionTimer) {
            this.log.debug(`BaseBonusView.startAutoSelectionTimer: cancelling existing timer`);
            this._autoSelectionTimer.kill();
            this._autoSelectionTimer = null;
        }

        let autoplayInProgress = this._model.isAutoplayInProgress();
        let timeout = autoplayInProgress? autoplayDelay : standardDelay;

        this.log.debug(`BaseBonusView.startAutoSelectionTimer: with ${timeout} seconds countdown`);

        /**
         * Delayed call to execute an auto-selection
         * @private
         * @type {TweenMax}
         */
        this._autoSelectionTimer = gsap.delayedCall(timeout, makeSelection);
    };


    /**
     * Cancels any auto-selection timer that may be running. The method checks if any timer
     * is running, so its safe to call at any point.
     * @protected
     */
    cancelAutoSelectionTimer()
    {
        this.log.debug('BaseBonusView.cancelAutoSelectionTimer()');

        if (this._autoSelectionTimer) {
            this._autoSelectionTimer.kill();
            this._autoSelectionTimer = null;
        }
    };


    /**
     * Handles a comms error. Will present a dialog view to the player. If the commsError
     * is a connection error, and a retry action is supplied, then  the view will offer the
     * player the oppurtunity to 
     * @protected
     * @param {ServiceExecutionError} error
     * @param {() => void} [actionOnRetry]
     * A "retry" method: if the Service Execution Error passed to param error indicates that it
     * is a simple comms error (eg: connection timeout), and a value is passed to actionOnRetry,
     * then the Dialog View shown will offer the player a button labelled something like "Try Again":
     * pressing it will close the Dialog View, and invoke actionOnRetry. The actionOnRetry should
     * invoke a series of steps that will reattempt the failed comms operation (and any associated
     * on-screen animations).
     */
    showCommsErrorDialog(error, actionOnRetry = null)
    {
        this.log.info(`BaseBonusView.showCommsErrorDialog(${JSON.stringify(error)},retryOptionSupplied:${actionOnRetry!=null})`);

        this._dialogController.showServiceExecutionErrorDialog(error, actionOnRetry);
    };
}


export default BaseBonusView