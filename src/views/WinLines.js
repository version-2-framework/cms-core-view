import BaseView from "./BaseView";
import C_PointerEvt from "../const/C_PointerEvt";
import C_GameEvent from "../../../cms-core-logic/src/const/C_GameEvent";

class WinLines extends BaseView
{

    constructor()
    {
        super();

        /**
         * Ordered list of Winline Number buttons. Index 0 will contain the button for Winline 1, etc.
         * @private
         * @type {PIXI.Container[]}
         */
        this._winlineButtons = [];

        /**
         * Ordered list of winline graphics. Index 0 will contain the line graphic for Winline 1, etc.
         * @private
         * @type {PIXI.Sprite[]}
         */
        this._winLineImgs = [];

        /**
         * Group containing the Winline graphics.
         * @private
         * @type {PIXI.Container}
         */
        this._linesContainer = new PIXI.Container();
        this.addChild(this._linesContainer);

        this.createWinLines();
        this.showActiveWinlineNumbers();
        this.dispatcher.on(C_GameEvent.BET_SETTINGS_CHANGED, this.showActiveWinlineNumbers, this);

        //this.showWinlinesTestingDisplay();
    };


    /**
     * @private
     */
    createWinLines()
    {
        /**
         * @type {SlotConfig}
         */
        const slotConfig = this.config;

        const layoutConfig = this.getActiveSlotLayout();

        const numReels = slotConfig.numReels;
        const winLines = slotConfig.winlines;
        const numLines = Object.keys(winLines).length;

        let reelsStartX = layoutConfig.reelsPosX + layoutConfig.reelsMarginLeft;
        let reelsStartY = layoutConfig.reelsPosY + layoutConfig.reelsMarginTop;
        let winlineStartX = reelsStartX - (0.5 * layoutConfig.reelGap);
        let winlineStartY = reelsStartY - (0.5 * layoutConfig.symGap);
        let buttonPosXLeft  = layoutConfig.winlineButPosXLeft;
        let buttonPosXRight = layoutConfig.winlineButPosXRight;
        let buttonStartY = reelsStartY - (0.5 * layoutConfig.symGap);
        let totalSymWidth  = layoutConfig.symWidth  + layoutConfig.reelGap;
        let totalSymHeight = layoutConfig.symHeight + layoutConfig.symGap;

        for (let winLineIdx = 0; winLineIdx < numLines; winLineIdx++)
        {
            let winlineId = winLineIdx + 1;
            
            /**
             * @type {GraphicalWinlineConfig}
             */
            let winlineData = winLines[winlineId];
            let winningPositions = winlineData.positions;
            let numWinSyms = winningPositions.length;

            let linePoints;
            if (winlineData.linePoints) {
                linePoints = winlineData.linePoints;
            }
            // No line points provided, create them manually from the basic
            // positions data of the winline.
            else
            {
                linePoints = [];
                linePoints.push({x:0, y:winningPositions[0]+0.5});
                for (let reelIndex = 0; reelIndex < winningPositions.length; reelIndex ++) {
                    let posOffset = winningPositions[reelIndex];
                    let x = reelIndex + 0.5;
                    let y = posOffset + 0.5;
                    linePoints.push({x,y});
                }
                linePoints.push({x:numReels, y:winningPositions[winningPositions.length-1] + 0.5});

                this.log.info(`line[${winlineId}]: generated linePoints: ${JSON.stringify(linePoints)}`);
            }
            
            let graphics = new PIXI.Graphics();
            let lineWidth = layoutConfig.winlineWidth;
            graphics.lineStyle(lineWidth, winlineData.colour);

            linePoints.forEach((linePoint, linePointIndex) => {
                let x = winlineStartX + totalSymWidth  * linePoint.x;
                let y = winlineStartY + totalSymHeight * linePoint.y;

                if (linePointIndex === 0) {
                    graphics.moveTo(x,y);
                }
                else graphics.lineTo(x,y);
            });

            this._linesContainer.addChild(graphics);
            this._winLineImgs.push(graphics);

            graphics.alpha = 0;

            // If position information was supplied for a winline button, create one.

            if (this._platform.isDesktop())
            {
                if (winlineData.btnPosY !== undefined && winlineData.btnIsLeft !== undefined)
                {
                    let buttonPosY = buttonStartY + (totalSymHeight * winlineData.btnPosY);
                    let button = this.gameFactory.getWinlineButton(winLineIdx + 1);
                    this.layout.setHeight(button, this.config.winLineButHeight);
                    button.x = winlineData.btnIsLeft? buttonPosXLeft : buttonPosXRight;
                    button.y = buttonPosY;
                    button.on(C_PointerEvt.ROLLOUT, function() {TweenMax.to(graphics, .1, {alpha:0})});
                    button.on(C_PointerEvt.OVER, function() {TweenMax.to(graphics, .1, {alpha:1})});
                    this.addChild(button);
                    this._winlineButtons.push(button);
                }
            }

        }
    };


    /**
     * Shows all winlines simultaneously, with a dark background behind them (so we can see
     * them clearly), and disables interactive input on any winline numbers. This special
     * method allows us to manually check how our winlines are lining up with each other.
     * @private
     */
    showWinlinesTestingDisplay()
    {
        // add a dark backrgound behind everything, so we can see the winlines more clearly
        let darkBg = this.assets.getSprite('popup_bg');
        this.layout.setToRectangle(darkBg, 0, 0, 100, 100);
        this.addChildAt(darkBg, 0);

        this._winLineImgs.forEach(winlineImg => {
            winlineImg.alpha = 1;
        });

        this._winlineButtons.forEach(winlineButton => {
            winlineButton.interactive = false;
        });
    };


    /**
     * Shows only those winline numbers that are active according to current bet settings.
     * Only active winline numbers will be interactive after this method is invoked.
     * @private
     */
    showActiveWinlineNumbers()
    {
        let numLines = this.model.getNumLines();

        for (let butIdx=0; butIdx < this._winlineButtons.length; butIdx++)
        {
            let active = butIdx < numLines;
            this._winlineButtons[butIdx].alpha = active;
            this._winlineButtons[butIdx].interactive = active;
        }
    };
}

export default WinLines