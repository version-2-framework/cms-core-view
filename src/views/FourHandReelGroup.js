///<reference path="../../../cms-core-logic/src/types/SlotGamePhaseResultModel.d.ts"/>
import ReelGroup from './ReelGroup';
import C_GameEvent from '../../../cms-core-logic/src/const/C_GameEvent';
import C_ViewEvent from "../const/C_ViewEvent";
import * as C_RewardGroup from '../../../cms-core-logic/src/const/C_RewardGroup';
import C_PointerEvt from '../const/C_PointerEvt';
import RngSingleton from '../../../cms-core-logic/src/maths/RngSingleton';

const NUM_HANDS = 4;

const INDEX_FOR_HAND_2 = 0;
const INDEX_FOR_HAND_3 = 1;
const INDEX_FOR_HAND_4 = 2;

// TODO: we can probably offer a config value for these settings.
const SYMBOL_ALPHA_DIMMED = 0.10;
const SYMBOL_ALPHA_NORMAL = 1;

/**
 * A Four Hand extended implementation of ReelGroup, for Four-Hand style WMG games.
 */
class FourHandReelGroup extends ReelGroup
{
    /**
     * Constructs a FourHandReelGroup.. although it doesn't really (you have to call "create" after).
     * This is a wiener convention, but due to lack of documentation by Wiener about why things were
     * designed this way, it's hard to give a justifiable reason why we have to do this everywhere
     * (there seems to be no actual purpose to it) - we just do it.
     * @param {FourHandReelGroupConfig} [fourHandReelGroupConfig]
     * Optional config for the Four Hand Reel Group, which sets custom options for how it behaves.
     * If not supplied, default config will be used - which will have the most simple config settings
     * (eg: how we expected this class to behave, before any specialist functionality was added to it)
     */
    constructor(fourHandReelGroupConfig)
    {
        super();

        /**
         * @private
         * @type {number}
         */
        this._symbolDimmedAlpha = this.config.secondaryHandSymDimmedAlpha || SYMBOL_ALPHA_DIMMED;

        /**
         * Config for the Four Hand Reel Group.
         * @private
         * @type {FourHandReelGroupConfig}
         */
        this._fourHandReelGroupConfig = fourHandReelGroupConfig ?
            fourHandReelGroupConfig :
            {
                secondaryHandSpinConfig : {
                    type : "simpleStop"
                }
            }
    }

    /**
     * @inheritDoc
     */
    create()
    {
        super.create();

        let config = this.config;
        
        let layout = this.getActiveSlotLayout();

        /**
         * Config for toggle holds sounds.
         * @private
         */
        this._toggleHoldsSounds = this.app.config.slotReelBacksConfig.toggleHoldsSounds;

        /**
         * @private
         * @type {boolean}
         */
        this._useTinySymbols = this.platform.isMobileOrTablet();

        // Create the secondary reels
        /**
         * A set of static symbol graphics for the secondary hands, organized by
         * - "progressive" secondary hand index (index 0 == hand index 1, etc)
         * - reel index
         * - sym on reel index
         * @protected
         * @type {PIXI.Sprite[][][]}
         */
        this._secondarySymbols = [];

        /**
         * All reel bgs for the secondary hands, organized first by
         * - "progressive" secondary hand index (index 0 === hand index 1, etc)
         * - reel index
         * @protected
         * @type {PIXI.Sprite[][]}
         */
        this._secondaryReelsBgs = [];
        
        let numReels = config.numReels;
        let numSymPerReel = config.numSymsPerReel;
        let secondaryHandSymPosX = layout.secondaryHandSymPosX;
        let secondaryHandSymPosY = layout.secondaryHandSymPosY;
        let symWidth  = layout.secondaryHandSymWidth;
        let symHeight = layout.secondaryHandSymHeight;
        let reelWidth = layout.secondaryHandReelWidth;
        let reelHeight = layout.secondaryHandReelHeight;
        let handWidth = numReels * reelWidth;
        let handHeight = reelHeight;

        // Configure any bespoke layer for animation
        let spinAnimConfig = this._fourHandReelGroupConfig.secondaryHandSpinConfig;
        if (spinAnimConfig.type === "staticAnim")
        {
            /**
             * @private
             * @type {PIXI.extras.AnimatedSprite[][]}
             */
            this._staticSpinAnimations = [[],[],[]];

            /**
             * @private
             * @type {PIXI.Container}
             */
            this._staticSpinAnimContainer = new PIXI.Container();

            /**
             * @private
             * @type {PIXI.Texture[]}
             */
            this._staticSpinAnimFrames = this.assets.generateFrames(
                spinAnimConfig.animPrefix,
                spinAnimConfig.padLength,
                spinAnimConfig.startFrame,
                spinAnimConfig.finalFrame);
        }
        
        for (let handIndex = 1; handIndex < NUM_HANDS; handIndex ++)
        {
            let startSymbolsForHand = this.spinModel.getFinalSymbolIds()[handIndex];

            /** @type {PIXI.Sprite[]} */
            let reelBgsForHand = [];

            /** @type {PIXI.Sprite[][]} */
            let symbolGraphicsForHand = [];

            let symStartPosX = secondaryHandSymPosX[handIndex - 1];
            let symStartPosY = secondaryHandSymPosY[handIndex - 1];

            for (let reelIndex = 0; reelIndex < numReels; reelIndex ++)
            {
                /** @type {PIXI.Sprite[]} */
                let symbolGraphicsForReel = [];

                let numSymForReel = numSymPerReel[reelIndex];
                let reelStartPosX = symStartPosX + (reelWidth * reelIndex);
                let reelStartPosY = symStartPosY;

                // Create a "reel held" bg image
                let reelBg = this.assets.getSprite("reelsBgColour");
                reelBg.handIndex = handIndex;
                reelBg.reelIndex = reelIndex;
                reelBg.x = reelStartPosX;
                reelBg.y = reelStartPosY;
                reelBg.width = reelWidth;
                reelBg.height = reelHeight;
                reelBg.buttonMode = true;
                reelBg.on(C_PointerEvt.DOWN, this.onReelBgPressed, this);
                
                this.addChild(reelBg);  // TODO: should probably go in a custom group, so we can control layering

                reelBgsForHand.push(reelBg);

                // Create the symbol graphics
                for (let symIndex = 0; symIndex < numSymForReel; symIndex ++)
                {
                    let symbolId = startSymbolsForHand[reelIndex][symIndex].id;
                    let symbolTextureId = this.getTextureIdForSymbol(symbolId);
                    let symGraphic = this.assets.getSprite(symbolTextureId);
                    symGraphic.x = reelStartPosX;
                    symGraphic.y = reelStartPosY + (symIndex * symHeight);
                    symGraphic.width = symWidth;
                    symGraphic.height = symHeight;

                    this.addChild(symGraphic);
                    symbolGraphicsForReel.push(symGraphic);
                }

                symbolGraphicsForHand.push(symbolGraphicsForReel);

                // TODO:First, let's see if this works acceptably, and then: if it does,
                // i can look at improving this functionality (eg: adding it to the libs
                // properly). For example, a StaticAnimationSpinner is probably a valid
                // approach, SO: it would be better to rework reel / spinner, to allow
                // it to work for secondary hands. This, however, presents some big
                // problems, because Reel / Spinner still have a lot of Wiener's poorly
                // written crap tangled into them (they explicitly reference the outside
                // world): tidying this up is no quick task.
                if (spinAnimConfig.type === "staticAnim")
                {
                    let staticSpinAnimForReel = new PIXI.extras.AnimatedSprite(this._staticSpinAnimFrames);

                    // All of these settings are currently hard-coded
                    staticSpinAnimForReel.loop = true;
                    staticSpinAnimForReel.animationSpeed = 1;
                    staticSpinAnimForReel.x = reelStartPosX;
                    staticSpinAnimForReel.y = reelStartPosY;
                    staticSpinAnimForReel.width = reelWidth;
                    staticSpinAnimForReel.height = reelHeight;
                    staticSpinAnimForReel.visible = false;

                    let secondaryHandLocalIndex = handIndex - 1;
                    
                    this._staticSpinAnimations[secondaryHandLocalIndex].push(staticSpinAnimForReel);

                    this._staticSpinAnimContainer.addChild(staticSpinAnimForReel);
                }
            }

            if (this._staticSpinAnimContainer) {
                this.addChild(this._staticSpinAnimContainer);
            }

            this._secondarySymbols.push(symbolGraphicsForHand);
            this._secondaryReelsBgs.push(reelBgsForHand);
        }

        

        this.dispatcher.on(C_GameEvent.ALL_REELS_STOPPED, this.updateSecondarySymbolsToCurrentSpinResult, this);
        this.dispatcher.on(C_GameEvent.SPIN_ANIMATION_STARTED, this.onSpinAnimationStarted, this);
        this.dispatcher.on(C_GameEvent.SPIN_ANIMATION_FINISHED, this.onSpinAnimationFinished, this);
        
        this.dispatcher.on(C_GameEvent.SHOW_SPIN2_HOLDS_PATTERN, this.highlightHeldReels, this);
        this.dispatcher.on(C_GameEvent.HIDE_SPIN2_HOLDS_PATTERN, this.clearAllHeldReelHighlights, this);
        this.dispatcher.on(C_GameEvent.HOLDS_PATTERN_CHANGED, this.highlightHeldReels, this);
        this.dispatcher.on(C_GameEvent.GAME_IDLE_ENDED, this.setAllSecondarySymbolsToVisible, this);
        this.dispatcher.on(C_GameEvent.ENABLE_HOLDS_INPUT, this.enableHoldsInput, this);
        this.dispatcher.on(C_GameEvent.DISABLE_HOLDS_INPUT, this.disableHoldsInput, this);
        
        this.dispatcher.on(C_ViewEvent.SINGLE_REEL_SPIN_STARTED, this.onSingleReelSpinStarted, this);
        this.dispatcher.on(C_ViewEvent.SINGLE_REEL_SPIN_STOP_COMPLETE, this.onSingleReelSpinStopComplete, this);
    };


    /**
     * Enables touch interaction for the reel-backs for all active secondary hands.
     * @private
     */
    enableHoldsInput()
    {
        this._secondaryReelsBgs.forEach((secondaryHandReelBgs, secondaryHandOrdinalIndex) => {
            let numHandsActive = this.model.getNumHands();
            let actualHandIndex = secondaryHandOrdinalIndex + 1;
            let handIsEnabled = actualHandIndex < numHandsActive;
            if (handIsEnabled) {
                secondaryHandReelBgs.forEach(reelBg => {
                    reelBg.interactive = true;
                });
            }
        });
    };


    /**
     * Disables touch interaction for the reel-backs for all secondary hands.
     * @private
     */
    disableHoldsInput()
    {
        this._secondaryReelsBgs.forEach(hand => {
            hand.forEach(reelBg => {
                reelBg.interactive = false;
            });
        });
    };


    /**
     * Callback invoked when a secondary hand reel bg is touched.
     * @private
     * @param {{target:{handIndex:number, reelIndex:number}}} touchEvent
     * Data indicating which handIndex and reelIndex need toggling. Both indices are absolute, eg: handIndex
     * 1 would refer to hand 2 (the first of the secondary hands). ReelIndex is relative to the hand.
     */
    onReelBgPressed(touchEvent)
    {
        let handIndex = touchEvent.target.handIndex;
        let reelIndex = touchEvent.target.reelIndex;

        this.log.debug(`FourHandReelGroup.onReelBgPressed(handIndex:${handIndex},reelIndex:${reelIndex})`);

        //this.spinModel.toggleReelHoldState(handIndex, reelIndex);

        // TODO: This code is basically rcirbbed from "ReelBacks.toggleReelHeld".
        // It may make more sense to move this out to "SpinSoundController", as a
        // dedicated mechanic (allowing us some more flexibility and optimization
        // in the future). As of v0.10.ish of core libs, this is basically ok
        // (Russell, @26.02.2021)
        let currHoldState = this.spinModel.getPlayerHoldsPattern().pattern[handIndex][reelIndex];
        let newHoldState = !currHoldState;

        if (this._toggleHoldsSounds)
        {
            let soundId;

            if (newHoldState)
            {
                soundId = this._toggleHoldsSounds.heldSoundsPerReel[reelIndex];
            }
            else
            {
                soundId = this._toggleHoldsSounds.unheldSoundsPerReel[reelIndex];
            }

            this.sound.playSound(soundId, false, 1);
        }

        this.spinModel.setReelHoldState(handIndex, reelIndex, newHoldState);

        this.dispatcher.emit(C_GameEvent.HOLDS_PATTERN_CHANGED);
    };


    // TODO: this is not currently being used - we need a decent hook (from base ReelGroup) to add it on to.
    /**
     * Immediately updates the symbols on all secondary reels, to show the values from the current spin
     * result that is being shown.
     * @protected
     */
    updateSecondarySymbolsToCurrentSpinResult()
    {
        console.log('FourHandReelGroup.updateSecondarySymbols()');

        let spinResult = this.getCurrentSpinResult();
        let symbols = spinResult.finalSymbolIds;
        this.setAllSecondarySymbolsTo(symbols);
    };


    /**
     * Immediately sets all of the secondary hand symbols, to show the values indicated by a given
     * Symbols Matrix.
     * @private
     * @param {SymbolsMatrix} symbolsMatrix 
     * The Symbols Matrix (set of all symbol ids) that must be shown on the secondary hands.
     */
    setAllSecondarySymbolsTo(symbolsMatrix)
    {
        for (let handIndex = 1; handIndex < NUM_HANDS; handIndex ++)
        {
            let symbolDataForHand = symbolsMatrix[handIndex];

            let symbolGraphicsForHand = this._secondarySymbols[handIndex - 1];

            for (let reelIndex = 0; reelIndex < symbolGraphicsForHand.length; reelIndex ++)
            {
                let symbolDataForReel = symbolDataForHand[reelIndex];
                let symbolGraphicsForReel = symbolGraphicsForHand[reelIndex];

                for (let symIndex = 0; symIndex < symbolGraphicsForReel.length; symIndex ++)
                {
                    let symbolId = symbolDataForReel[symIndex].id;
                    let symbolGraphic = symbolGraphicsForReel[symIndex];
                    let symbolTextureId = this.getTextureIdForSymbol(symbolId);

                    symbolGraphic.texture = this.assets.getTexture(symbolTextureId);
                }
            }
        }
    };


    // TODO: Parameters from this event are almost certainly incorrect.
    // TODO: This method is now not called (i think) since the various changes when it was originally written..
    /**
     * @private
     * @param {{isSpin2:boolean}} eventData
     */
    onSpinAnimationStarted(eventData)
    {
        this.log.debug(`FourHandReelGroup.onSpinAnimationStarted:${JSON.stringify(eventData)}`);

        // Set all symbols back to full alpha state.
        this.setAllSecondaryHandSymbolsAlpha(SYMBOL_ALPHA_NORMAL, INDEX_FOR_HAND_2);
        this.setAllSecondaryHandSymbolsAlpha(SYMBOL_ALPHA_NORMAL, INDEX_FOR_HAND_3);
        this.setAllSecondaryHandSymbolsAlpha(SYMBOL_ALPHA_NORMAL, INDEX_FOR_HAND_4);

        if (eventData.isSpin2)
        {
            this.highlightHeldReels();
        }
    };


    // TODO: Was this being used ??
    /**
     * @private
     */
    onSpinAnimationFinished()
    {
        this.clearAllHeldReelHighlights();
    };


    /**
     * Action invoked, when the "single reel spin started" event is fired. This event indicates that a
     * single reel on the primary hand has started spinning - it is fired exactly at the moment that the
     * reel is requested to start spinning, and will come before any "infinite spin fully started"
     * event is fired. The event packet indicates the index of the reel that has started. On this action,
     * for all secondary hands, we hide all symbols on the reel in question (this is the secondary hand
     * equivalent of "spinning").
     * @private
     * @param {{reelIndex:number}} eventData
     */
    onSingleReelSpinStarted(eventData)
    {
        let reelIndex = eventData.reelIndex;

        this.log.debug(`FourHandReelGroup.onSingleReelSpinStarted(reelIndex:${reelIndex})`)

        this._secondarySymbols.forEach(handSymbols => {
            handSymbols[reelIndex].forEach(symbol => {
                symbol.visible = false;
            });
        });

        if (this._staticSpinAnimations)
        {
            this._staticSpinAnimations.forEach(staticSpinAnimsForHand => {
                let numFrames = this._staticSpinAnimFrames.length;
                let randomStartFrame = RngSingleton.getRandom(numFrames);
                let staticSpinAnim = staticSpinAnimsForHand[reelIndex];
                staticSpinAnim.visible = true;
                staticSpinAnim.gotoAndPlay(randomStartFrame);
            });
        }
    };


    /**
     * Action invoked, when a "single reel spin stop" has completed (as opposed to the "spin stop"
     * having started). It is fired by ReelGroup, exactly at the moment that the reel has come to a rest,
     * and the event packet tells us the reelIndex (from the primary hand) of the reel that just stopped
     * spinning. At this point, we can show the secondary hand symbols, for all corresponding reels.
     * @private
     * @param {{reelIndex:number}} eventData
     */
    onSingleReelSpinStopComplete(eventData)
    {
        let spinResult = this.getCurrentSpinResult();
        let reelIndex = eventData.reelIndex;

        this._secondarySymbols.forEach((handSymbols, progressiveSecondaryHandIndex) => {
            let handIndex = progressiveSecondaryHandIndex + 1;
            let symbolDataForHand = spinResult.finalSymbolIds[handIndex];
            let symbolDataForReel = symbolDataForHand[reelIndex];

            handSymbols[reelIndex].forEach((symbolGraphic, symbolIndex) => {
                let symbolData = symbolDataForReel[symbolIndex];
                symbolGraphic.visible = true;
                symbolGraphic.texture = this.assets.getTexture(`sym${symbolData.id}`);
            });
        });

        if (this._staticSpinAnimations)
        {
            this._staticSpinAnimations.forEach(staticSpinAnimsForHand => {
                let staticSpinAnim = staticSpinAnimsForHand[reelIndex];
                staticSpinAnim.visible = false;
                staticSpinAnim.stop();
            });
        }
    };


    /**
     * Specialized version of the "Quick Symbol Win" presentation, which handles Four Hand symbol
     * win presentations.
     * @protected
     * @override
     * @inheritDoc
     * @param {MultiHandSymbolWinPresentation} symbolWinPresentation
     * This is deliberately a different parameter type, to the one accepted by the base class method:
     * in cases where we use FourHandReelGroup, we will make sure that MultiHandSymbolWinPresentation
     * is the type we have.
     */
    showQuickSymbolWinPresentation(symbolWinPresentation)
    {
        this.log.debug(`FourHandReelGroup.showQuickWinSymbolWinPresentation(${JSON.stringify(symbolWinPresentation)})`);

        let mainHandWin = symbolWinPresentation.winPerHand[0];
        
        if (mainHandWin) {
            this.highlightSymbolWin(mainHandWin, false, true);
        }
        else
        {
            this.fadeAllSymbols(); // on the main hand
        }

        // Now, show any wins on the secondary hands.
        for (let handIndex = 1; handIndex < 4; handIndex ++)
        {
            let secondaryHandIndex = handIndex - 1;
            let winForHand = symbolWinPresentation.winPerHand[handIndex];
            if (winForHand)
            {
                this.highlightSecondaryHandSymbolWin(winForHand, secondaryHandIndex);
            }
            else
            {
                this.setAllSecondaryHandSymbolsAlpha(this._symbolDimmedAlpha, secondaryHandIndex);
            }
        }

        this.recordWinningResultValuesToModel(symbolWinPresentation);
    };


    /**
     * @protected
     * @over
     * @param {MultiHandSymbolWinPresentation} symbolWinPresentation 
     */
    showIdleSymbolWin(symbolWinPresentation)
    {
        if (symbolWinPresentation.winPerHand[0])
        {
            let showSymbolAnimations = true;
            let fadeNonWinningSymbols = true;
            this.highlightSymbolWin(symbolWinPresentation.winPerHand[0], showSymbolAnimations, fadeNonWinningSymbols);
        }
        else
        {
            this.fadeAllSymbols(); // on the main hand
        }

        // Now, show any wins on the secondary hands
        // Now, show any wins on the secondary hands.
        for (let handIndex = 1; handIndex < 4; handIndex ++)
        {
            let secondaryHandIndex = handIndex - 1;
            let winForHand = symbolWinPresentation.winPerHand[handIndex];
            if (winForHand)
            {
                this.highlightSecondaryHandSymbolWin(winForHand, secondaryHandIndex);
            }
            else
            {
                this.setAllSecondaryHandSymbolsAlpha(this._symbolDimmedAlpha, secondaryHandIndex);
            }
        }
    };


    /**
     * Extended version of the method to highlight big win symbols - we call through to the
     * super class method, all we need to do here is use it as a trigger to also highlight
     * our secondary hand symbols.
     * @protected
     * @override
     * @inheritDoc
     * @param {boolean[][][]} symbolMap 
     * @param {boolean} fadeOtherSymbols 
     */
    showBigWinStateOnSymbols(symbolMap, fadeOtherSymbols)
    {
        for (let handIndex = 1; handIndex < 4; handIndex ++) {
            let secondaryHandIndex = handIndex - 1;
            let mapForHand = symbolMap[handIndex];
            mapForHand.forEach((mapForReel, reelIndex) => {
                mapForReel.forEach((posIsWinning, posIndex) => {
                    let alpha = posIsWinning? SYMBOL_ALPHA_NORMAL : this._symbolDimmedAlpha;
                    this._secondarySymbols[secondaryHandIndex][reelIndex][posIndex].alpha = alpha;
                });
            });
        }

        super.showBigWinStateOnSymbols(symbolMap, fadeOtherSymbols);
    };


    /**
     * Highlights a Symbol Win on a secondary hand. Non-winning symbols are dimmed.
     * @private
     * @param {SymbolWin} win
     * A SymbolWin to highlight.
     * @param {number} secondaryHandIndex
     */
    highlightSecondaryHandSymbolWin(win, secondaryHandIndex)
    {
        console.log(`FourHandReelGroup.highlightSecondaryHandSymbolWin(win:${JSON.stringify(win)},secondaryHandIndex:${secondaryHandIndex})`);

        win.winningPositionsMap.forEach((winningPosMapForReel, reelIndex) => {
            winningPosMapForReel.forEach((posIsWinning, posIndex) => {
                let alpha = posIsWinning? SYMBOL_ALPHA_NORMAL : this._symbolDimmedAlpha;
                this._secondarySymbols[secondaryHandIndex][reelIndex][posIndex].alpha = alpha;
            });
        });
    };


    /**
     * Sets all secondary hand symbols to fully visible.
     * @private
     */
    setAllSecondarySymbolsToVisible()
    {
        this.setAllSecondaryHandSymbolsAlpha(1, 0);
        this.setAllSecondaryHandSymbolsAlpha(1, 1);
        this.setAllSecondaryHandSymbolsAlpha(1, 2);
    };


    /**
     * Immediately applies a single alpha value, to all secondary hand symbols.
     * @private
     * @param {number} alphaValue
     * @param {number} secondaryHandIndex
     */
    setAllSecondaryHandSymbolsAlpha(alphaValue, secondaryHandIndex)
    {
        this.log.debug(`FourHandReelGroup.setAllSecondaryHandSymbolsAlpha(to:${alphaValue},forSecondaryHand:${secondaryHandIndex})`);

        let symbolsForHand = this._secondarySymbols[secondaryHandIndex];
        for (let reelIndex = 0; reelIndex < symbolsForHand.length; reelIndex ++) {
            let symbolsForReel = symbolsForHand[reelIndex];
            for (let posIndex = 0; posIndex < symbolsForReel.length; posIndex ++) {
                symbolsForReel[posIndex].alpha = alphaValue;
            }
        }
    };


    /**
     * Immediately changes the visual status of the secondary hand symbols, to reflect which reels are
     * held, and which are not.
     * @private
     */
    highlightHeldReels()
    {
        let holdsPattern = this.spinModel.getPlayerHoldsPattern();

        this.log.debug(`FourHandReelGroup.highlightHeldReels: ${JSON.stringify(holdsPattern)}`);

        this._secondaryReelsBgs.forEach((reelBgsForHand, progressiveHandIndex) =>
        {
            let handIndex = progressiveHandIndex + 1;

            reelBgsForHand.forEach((reelBg, reelIndex) => {
                let reelIsHeld = holdsPattern.pattern[handIndex][reelIndex];
                reelBg.texture = this.assets.getTexture(reelIsHeld? "reelsBgHeldColour" : "reelsBgColour");
            });
        });
    };


    /**
     * Immediately clears any Held Reel highlights which may currently be shown.
     * @private
     */
    clearAllHeldReelHighlights()
    {
        this.log.debug('FoundHandReelsGroup.clearAllHeldReelHighlights()');

        this._secondaryReelsBgs.forEach(reelBgsForHand => {
            reelBgsForHand.forEach((reelBg, reelIndex) => {
                reelBg.texture = this.assets.getTexture("reelsBgColour");
            });
        });
    };


    /**
     * @protected
     * @override
     * @inheritdoc
     */
    abortInfiniteSpinAnimation()
    {
        // Call through to base method - this is only being used as a hook to
        // add extra functionality, we are certainly not messing with the base
        // functionality 
        super.abortInfiniteSpinAnimation();

        // Kill any static spin animations
        if (this._staticSpinAnimations) {
            this._staticSpinAnimations.forEach(staticSpinAnimsForHand => {
                staticSpinAnimsForHand.forEach(staticSpinAnim => {
                    staticSpinAnim.visible = false;
                    staticSpinAnim.stop();
                });
            }); 
        }

        // Show all secondary hand symbols again
        this._secondarySymbols.forEach(symbolsOnHand => {
            symbolsOnHand.forEach(symbolsOnReel => {
                symbolsOnReel.forEach(symbol => {
                    symbol.visible = true;
                });
            });
        });
    }


    /**
     * Returns the appropriate texture id to use for symbols on the secondary hands, for a given symbol id.
     * @private
     * @param {number} symbolId 
     * @return {string}
     */
    getTextureIdForSymbol(symbolId)
    {
        return this._useTinySymbols? `sym${symbolId}_micro` : `sym${symbolId}`;
    };
}

export default FourHandReelGroup;