import C_PointerEvt from "../const/C_PointerEvt";
import BaseView from "./BaseView";

const State =
{
    SHOW_INDIVIDUAL_SYMBOLS : 1,
    SHOW_INDIVIDUAL_REELS : 2
}

const MIN_STATE = 1;
const MAX_STATE = 2;

// TODO:There is a good argument to start expanding this into a more usable overall debug
// GUI: could be extremely usefull as we progress.

/**
 * A special debug visual component, which shows assorted bounded areas of the reels, allowing you to
 * understand the effect that your visual configuration has. This uses area data provided by Reel
 * Position Util, which can be considered the ultimate source of this data.
 * 
 * On-screen buttons are shown, allowing you to toggle between different modes:
 * - show symbol areas
 * - show individual reels areas
 * - show overall hand "reels" areas
 */
class ReelDebugLayer extends BaseView
{
    constructor()
    {
        super();

        let numHands = this.config.numHands;
        let numReels = this.config.numReels;
        let numSymPerReel = this.config.numSymsPerReel;

        /**
         * @private
         * @type {PIXI.DisplayObject[]}
         */
        this._visualStates = [];


        //------------------------------------------------------------------------------
        // Let's draw all symbol areas, for all hands.
        //------------------------------------------------------------------------------
        /**
         * @private
         * @type {PIXI.Graphics}
         */
        this._symbolsGraphic = new PIXI.Graphics();
        this._symbolsGraphic.beginFill(0xFFFF00, 0.5);

        for (let handIndex = 0; handIndex < numHands; handIndex ++)
        {
            for (let reelIndex = 0; reelIndex < numReels; reelIndex ++)
            {
                let numSymOnReel = numSymPerReel[reelIndex];

                for (let symIndex = 0; symIndex < numSymOnReel; symIndex ++)
                {
                    let symbolBounds = this.reelPositionUtil.getBoundsOfSymbolInScreenSpaceForHand(handIndex, reelIndex, symIndex);

                    this._symbolsGraphic.drawRect(symbolBounds.x, symbolBounds.y, symbolBounds.width, symbolBounds.height);
                }
            }
        }

        this._symbolsGraphic.endFill();

        this.addChild(this._symbolsGraphic);

        this._visualStates.push(this._symbolsGraphic);

        //------------------------------------------------------------------------------
        // Draw individual reels, for all hands
        //------------------------------------------------------------------------------
        /**
         * @private
         * @type {PIXI.Graphics}
         */
        this._individualReelsGraphic = new PIXI.Graphics();
        this._individualReelsGraphic.beginFill(0xFFFFFF, 0.5);

        for (let handIndex = 0; handIndex < numHands; handIndex ++)
        {
            for (let reelIndex = 0; reelIndex < numReels; reelIndex ++)
            {
                let boundsForReel = this.reelPositionUtil.getBoundsOfReelInScreenSpaceForHand(handIndex, reelIndex);

                this._individualReelsGraphic.drawRect(boundsForReel.x, boundsForReel.y, boundsForReel.width, boundsForReel.height);
            }
        }

        this._individualReelsGraphic.endFill();
        
        this.addChild(this._individualReelsGraphic);

        this._visualStates.push(this._individualReelsGraphic);

        //------------------------------------------------------------------------------
        // Finally, add a gui input, to select our choice
        //------------------------------------------------------------------------------
        /**
         * @private
         * @type {PIXI.Container}
         */
        this._guiContainer = new PIXI.Container();
        this.addChild(this._guiContainer);

        let guiBg = new PIXI.Graphics();
        guiBg.beginFill(0x000000);
        guiBg.drawRect(0, 0, this.layout.relWidth(100), this.layout.relHeight(4));
        guiBg.endFill();

        let btnLeft = new PIXI.Graphics();
        btnLeft.beginFill(0xFFFFFF);
        btnLeft.drawPolygon([
            new PIXI.Point(this.layout.relWidth(3), this.layout.relHeight(0)),
            new PIXI.Point(this.layout.relWidth(3), this.layout.relHeight(3)),
            new PIXI.Point(this.layout.relWidth(0), this.layout.relHeight(1.5))
        ]);
        btnLeft.endFill();
        btnLeft.x = this.layout.relWidth(37);
        btnLeft.y = this.layout.relHeight(0.5);
        btnLeft.interactive = true;
        btnLeft.on(C_PointerEvt.DOWN, this.cycleStateLeft, this);

        let btnRight = new PIXI.Graphics();
        btnRight.beginFill(0xFFFFFF);
        btnRight.drawPolygon([
            new PIXI.Point(this.layout.relWidth(0), this.layout.relHeight(0)),
            new PIXI.Point(this.layout.relWidth(0), this.layout.relHeight(3)),
            new PIXI.Point(this.layout.relWidth(3), this.layout.relHeight(1.5))
        ]);
        btnRight.endFill();
        btnRight.x = this.layout.relWidth(60);
        btnRight.y = this.layout.relHeight(0.5);
        btnRight.interactive = true;
        btnRight.on(C_PointerEvt.DOWN, this.cycleStateRight, this);

        /**
         * @private
         * @type {PIXI.Text}
         */
        this._stateText = new PIXI.Text("", {
            fontFamily:"Arial",
            fill : 0xFFFFFF,
            fontSize : this.layout.relHeight(3)
        });
        this._stateText.anchor.set(0.5, 0.5);
        this._stateText.x = this.layout.relWidth(50);
        this._stateText.y = this.layout.relHeight(2);

        this._guiContainer.addChild(guiBg);
        this._guiContainer.addChild(btnLeft);
        this._guiContainer.addChild(btnRight);
        this._guiContainer.addChild(this._stateText);

        this.setState(State.SHOW_INDIVIDUAL_SYMBOLS);
    };


    /**
     * @private
     */
    cycleStateLeft()
    {
        if (this._currState > MIN_STATE) {
            this._currState -= 1;
        }
        else this._currState = MAX_STATE;

        this.updateState();
    };


    /**
     * @private
     */
    cycleStateRight()
    {
        if (this._currState < MAX_STATE) {
            this._currState += 1;
        }
        else this._currState = MIN_STATE;

        this.updateState();
    };


    /**
     * @private
     * @param {number} state 
     */
    setState(state)
    {
        this._currState = state;
        this.updateState();
    };


    /**
     * @private
     */
    updateState()
    {
        this._visualStates.forEach(visualState => {
            visualState.visible = false;
        });

        if (this._currState === State.SHOW_INDIVIDUAL_SYMBOLS) {
            this._symbolsGraphic.visible = true;
            this._stateText.text = "Symbols";
        }
        else
        if (this._currState === State.SHOW_INDIVIDUAL_REELS) {
            this._individualReelsGraphic.visible = true;
            this._stateText.text = "individual reels";
        }
    }
}

export default ReelDebugLayer;