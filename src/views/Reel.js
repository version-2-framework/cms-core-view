import C_GameEvent from "../../../cms-core-logic/src/const/C_GameEvent";
import { getGameViewLogger } from "../../../cms-core-logic/src/logging/LogManager";
import C_ViewEvent from "../const/C_ViewEvent";
import * as ArrayUtils from "../../../cms-core-logic/src/utils/ArrayUtils";
import C_ReelEvents from "../const/C_ReelEvents";
import getActiveSlotLayout from "../../../cms-core-logic/src/utils/getActiveSlotLayout";

// For Reel to be a useful abstraction, it still needs a lot of refactoring, but its getting there.
/**
 * Abstraction for a single reel (a vertical slice of symbols).
 * It's not a visual component (container) , but serves as a logical entity in the game structure
 * @implements ReelView
 */
class Reel extends PIXI.utils.EventEmitter
{
    // TODO: ReelGroup does not sound like a property that the Reel needs to know about
    /**
     * 
     * @param {*} reelGroup 
     * @param {PIXI.Container} symbolContainer 
     * A container, into which new symbols are added.
     * @param {PIXI.Container} spinnerOverlayContainer
     * @param {number} reelIndex 
     */
    constructor(reelGroup, symbolContainer, spinnerOverlayContainer, reelIndex)
    {
        super();

        // TODO: Document all of these
        /**
         * @private
         */
        this._app = PIXI.app;

        /**
         * @private
         */
        this._config = this._app.config;

        /**
         * The active Slot Layout Config.
         * @private
         * @type {SlotLayoutConfig}
         */
        this._slotLayout = getActiveSlotLayout(this._app);

        /**
         * @private
         */
        this._assets = this._app.assets;

        /**
         * @private
         * @type {GameFactoryApi}
         */
        this._gameFactory = this._app.gameFactory;

        // TODO: If model access is not needed, then we will not reference it.
        /**
         * @private
         */
        this._model = this._app.model;

        // TODO: This is currently implicitly public - PIDSpinner accesses it repeatedly.
        // This is a tangled web which must be unweaved.
        this.reelGroup = reelGroup;

        /**
         * The container that new symbols are added into.
         * @private
         * @type {PIXI.Container}
         */
        this._symContainer = symbolContainer;

        /**
         * @private
         * @type {PIXI.Container}
         */
        this._overlayContainer = spinnerOverlayContainer;

        /**
         * @private
         */
        this.log = getGameViewLogger();

        // TODO: OK, now i understand this. Its definitely private (or should be) ?
        // However, i confirm that its being accessed all over the place. In addition,
        // "yOrigin" implies to me.. the y position of the reel. yOffset would actually
        // be a more accurate description of how it is being used. So it needs renaming,
        // documenting better, and cleaning up.
        /**
         * @public
         * @type {number}
         * @readonly
         */
        this.yOrigin = this._slotLayout.reelYOrigins[reelIndex];

        /**
         * Indicates the total "step" of a single symbol (height plus any additional gap). This is not clearly
         * defined - naturally, Wiener didn't document it - and its an awful name for something which is basically
         * a public constant. Is it always measured from the inner margin of the reels? There just isnt any
         * indication.
         * @public
         * @type {number}
         * @inheritDoc
         */
        this.symStep = this._slotLayout.symHeight + this._slotLayout.symGap;

        /**
         * The index of this reel, within its hand.
         * @public
         * @inheritDoc
         * @readonly
         */
        this.reelIndex = reelIndex;

        // TODO: Spinner reads variables from Reel when it is created, but... reel must know its Spinner,
        // in order to access some data (eg: how many symbols above / below the reel are needed). However,
        // the potential poor ordering (both trying to access data from each other) is currently working,
        // everything that is needed is initialized WHEN its needed: but this could do with further
        // de-tanglging, and this comment serves as a warning to the future, there is some mess here that
        // could bite us later.
        /**
         * @private
         * @type {Spinner}
         */
        this._spinner = this._gameFactory.getSpinner(this, reelIndex);

        /**
         * @public
         * @inheritDoc
         * @type {SymbolView[]}
         */
        this.symbols = [];

        /**
         * Tracks the number of symbols above the visible area.
         * @private
         * @readonly
         * @type {number}
         */
        this._numSymAboveVisibleArea = this._spinner.numSymAboveVisibleArea;

        /**
         * Tracks the number of symbols below the visible area.
         * @private
         * @readonly
         * @type {number}
         */
        this._numSymBelowVisibleArea = this._spinner.numSymBelowVisibleArea;
        
        /**
         * Tracks the total number of visible symbols on this reel.
         * @public
         * @readonly
         * @type {number}
         */
        this.numSymVisible = this._config.numSymsPerReel[reelIndex];
    };


    /**
     * @public
     * @inheritDoc
     */
    getActiveSlotLayout()
    {
        return this._slotLayout;
    };


    /**
     * @public
     * @param {SymbolConfig[]} symbolIds
     * The set of symbol ids that should be shown on the reel initially.
     */
    create(symbolIds)
    {
        const config = this._config;
        const layout = this._slotLayout;
        
        // The spinner used, tells us how many additional symbols (above / below the reels)
        // are required.
        
        
        let numSymbolsVisibleForReel = this.numSymVisible;
        let numSymbolsTotal = this.numSymVisible + this._numSymAboveVisibleArea + this._numSymBelowVisibleArea;

        let maskWidth = layout.symWidth + (layout.reelMaskExpand * 2);
        
        let maskHeight = layout.reelsMarginTop + (numSymbolsVisibleForReel * layout.symHeight) +
            ((numSymbolsVisibleForReel - 1) * layout.symGap) + layout.reelsMarginBottom;

        // Ideally, reels would be told where to be positioned, and not laid out this way.
        // When we want to have a primary set of reels, with an irregular gap - imagine
        // 3 reels on the left, 3 on the right, a big gap in the middle - then this will
        // not work. I don't want to just add in "middleReelGap" or some config property
        // like that - (hmm, or do i...) instead, we need some better way of handling this.
        // This reads far too much like the reel knowing how to position itself, and it
        // shouldn't really be doing that at all.
        let reelPosX = layout.reelsMarginLeft + this.reelIndex * (layout.symWidth + layout.reelGap) + layout.symWidth * .5;
        
        // I still do not fully understand what is being done with masking - it seems to be applied to
        // individual symbols (a single mask), and then removed. I need to read on PIXI's masking conventions,
        // and then try and better understand what Wiener are doing with it, and document it fully.
        
        let mask = new PIXI.Graphics();
        mask.beginFill(0xff00ff, .5);
        mask.drawRect(0, 0, maskWidth, maskHeight);
        mask.endFill();
        mask.x = reelPosX - (layout.symWidth * 0.5) - layout.reelMaskExpand;
        mask.y = this.yOrigin;
        mask.visible = false;
        this._symContainer.addChildAt(mask, 0);

        // NOTE:
        // This is an implicit public property (not documented by wiener, but its accessed in PIDSpinner)
        // PIDSpinner accesses it, and then applies it to individual Symbols. I have absolutely no idea
        // why this is being done, but it seems that we mask symbols, and not the symbols container for
        // the reel (Russell 06.01.2021)
        this.maskGraphics = mask;

        let symbolTotalHeight = layout.symHeight + layout.symGap;

        // This is a big hint that "reelYOrigin" is actually a "reelYOffset"
        let reelYOrigin = layout.reelYOrigins[this.reelIndex];

        let symbolsVisibleStartPosY = reelYOrigin + layout.reelsMarginTop;
        let symbolsStartPosY = symbolsVisibleStartPosY - (symbolTotalHeight * this._numSymAboveVisibleArea);
        
        for (let symIdx = 0; symIdx < numSymbolsTotal; symIdx++)
        {
            
            let symbolPosY = symbolsStartPosY + (symbolTotalHeight * symIdx) + (layout.symHeight * 0.5);
            let symbolGraphic = this._gameFactory.getSymbol();
            symbolGraphic.create();
            symbolGraphic.x = reelPosX;
            symbolGraphic.y = symbolPosY;
            symbolGraphic.setPosIndex(symIdx);
            symbolGraphic.setReelIndex(this.reelIndex);
            symbolGraphic.showAsNormalSymbol();
            this._symContainer.addChild(symbolGraphic);
            

            // Initially, i wasnt sure if this was something that Reel should be doing. It seems like
            // Spinner generally handles this. However, the "isFinal" property, i haven't yet been able
            // to properly document or understand its usage (and why it needs to exist, or if it really
            // does).
            // Also, this calculation is .. not correct? Weirdly, it seems to produce the
            // opposite value of what you would expect: if the symbol index is anywhere before the final
            // (bottom) symbol, then isFinal... is set to true. I don't understand the "larger than
            // 0" check either (presumably, thats because index 0 is.. right, its the hidden top symbol??)
            // If so, then its wrong, and i still dont understand why that would be correct.
            symbolGraphic.isFinal = symIdx > 0 && symIdx < numSymbolsTotal - 1;

            this.symbols.push(symbolGraphic);

            this.log.debug(`Reel[${this.reelIndex}].sym[${symIdx}].y = ${symbolGraphic.y}`);
        }

        /**
         * Caches the resting y positions of all symbols.
         * @private
         * @type {number[]}
         */
        this._symbolIdleYPositions = [];
        for (let symIndex = 0; symIndex < numSymbolsTotal; symIndex++) {
            this._symbolIdleYPositions.push(this.symbols[symIndex].y);
        }

        // TODO: I think this is not yet correct, it must take the additional padding area
        // into account (as this area counts as visible)
        /**
         * The visible height of the reel.
         * @private
         * @type {number}
         */
        this._visibleHeight = numSymbolsVisibleForReel * this.symStep;

        /**
         * The y position for top-wrapping. Consider that symbols are center anchored, this is half
         * the way through the top symbol position: its the value above which, the symbol should be
         * wrapped to bottom (if we were moving upwards)
         * @private
         * @type {number}
         */
        this._topWrapPosY = symbolsVisibleStartPosY - ((0.5 + this._numSymAboveVisibleArea) * symbolTotalHeight);

        /**
         * The y position for bottom-wrapping. Consider that symbols are center anchored, this is half
         * the way through the bottom symbol position: its the value above which, the symbol should be
         * wrapped to top (if we were moving downwards)
         * @private
         * @type {number}
         */
        this._bottomWrapPosY = symbolsVisibleStartPosY + this._visibleHeight + ((0.5 + this._numSymBelowVisibleArea) * symbolTotalHeight);

        // interestingly
        // - "reelBottom" is apparently public (its used by PIDSpinner). Its wrong, or at least
        //   wrong by my understanding of the name of the variable. Potentially, the name is just
        //   confusing. It should mean the bottom y position of the reel ?? The last visible y
        //   position?? - but apparently it is just the total height of the symbols (well, its not:
        //   if that is what it was meant to be, then it's calculated incorrectly). I am fairly
        //   sure that it IS the bottom y position of the reels, though.
        // - "reelHeight" is wrong (its also not used anywhere)
        // TODO: OK, we can now work this out correctly, using the actual local values which tell
        // us how many symbols above / below the visible area that we have.
        this.reelBottom = (numSymbolsTotal - 1) * this.symStep;
        this.reelHeight = (numSymbolsTotal - 1) * this.symStep;

        // This also is not necessarily a detail of of "Reel", as not all spinners will be interested in it.
        // Also.... I don't fully understand when its used. It certainly needs reverse engineering and
        // documenting.
        this.symbolWrapDistance = numSymbolsTotal * this.symStep;

        this.setAllSymbolIds(symbolIds);
        this.symbols.forEach(symbol => symbol.showAsNormalSymbol());
        this.unmaskSymbols();
    };


    /**
     * @public
     * @inheritDoc
     * @return {PIXI.Container}
     */
    get overlayContainer() {
        return this._overlayContainer;
    }


    /**
     * @public
     * @inheritDoc
     */
    get visibleHeight() {
        return this._visibleHeight;
    };


    /**
     * @public
     * @inheritDoc
     */
    get symbolIdleYPositions() {
        return this._symbolIdleYPositions;
    };


    /**
     * @public
     * @inheritDoc
     */
    get topWrapPosY() {
        return this._topWrapPosY;
    };


    /**
     * @public
     * @inheritDoc
     */
    get bottomWrapPosY() {
        return this._bottomWrapPosY;
    };


    /**
     * @public
     * @inheritDoc
     * @param {SpinPhaseType} spinPhaseType
     */
    startInfiniteSpin(spinPhaseType)
    {
        this._spinner.startInfiniteSpin(spinPhaseType);
    };


    /**
     * @public
     * @inheritDoc
     * @param {SymbolConfig[]} symbols
     * @param {number} spinType
     * @param {boolean[]} positionIsSticky
     * @param {SpinPhaseType} spinPhaseType
     */
    spinToSymbols(symbols, spinType, positionIsSticky, spinPhaseType)
    {
        this._spinner.spinToSymbols(symbols, spinType, positionIsSticky, spinPhaseType);
    };


    /**
     * @public
     * @inheritDoc
     */
    abortSpin()
    {
        this._spinner.abortSpin();

        // TODO: Reset all symbol ids ?? I think this isn't part of the api of this method..
    };


    // Apparently this is public, because its called by PIDSpinner
    // Unfortunately, its not clear to me precisely what its doing
    // "unmaskSymbols".. which apparently can also mean.. applying
    // a mask to the symbols? Originally, it was also hiding wins
    // on the symbols, changing various symbol state.
    /**
     * @public
     */
    unmaskSymbols()
    {
        // TODO: This method could be replaced with the "symbol iterator" call,
        // to set symbols outside of the visible area to masked.

        let numSyms = this.symbols.length;
        for (let symIdx = 0; symIdx < numSyms; symIdx++)
        {
            let symbolGraphic = this.symbols[symIdx];
            
            // TODO: This is no longer an acceptable part of public usage of symbolview.
            // It needs removing, but i also need to find out what it was doing
            symbolGraphic.isFinal = false;
            symbolGraphic.tallSymbolActive = false;

            // This statement is semantically incorrect, because it assumed 1 hidden sym
            // above the visible area, and one below, which was never a correct assumption.
            // However, it inadvertantly doesn't cause a problem, because the mask itself
            // should allow those symbols to be visible.
            // Most problematically, this method is called "unmaskSymbols", but it configures
            // the mask, so it actually does the exact opposite of what the method name
            // implies, which was REALLY confusing me!!
            if (symIdx === 0 || symIdx === (numSyms - 1) || this._app.config.disableUnmasking)
            {
                symbolGraphic.alpha = 1;
                symbolGraphic.mask = this.maskGraphics;
            }
        }
    };


    /**
     * @public
     * @inheritDoc
     */
    sortSymbolsByY()
    {
        this.symbols.sort((s1, s2) => s1.y - s2.y);

        this.symbols.forEach((symbol,symbolPosIndex) => {
            symbol.setPosIndex(symbolPosIndex);
        })
    };


    /**
     * @public
     * @inheritDoc
     * @param {SymbolConfig[]} symbolIds 
     */
    setAllSymbolIds(symbolIds)
    {
        // TODO: The "app.utils.getRandomSymId" isn't really a useful method for doing this.
        // It needs replacing with a dedicated component, which can feed in symbols, and uses
        // our custom randomness (so we can seed it in the same way as other stuff)
        this.iterateSymbols((symbolGraphic, isInVisibleArea, visibleSymIndex) => {
            let symbolId = isInVisibleArea? symbolIds[visibleSymIndex].id : this._app.utils.getRandomSymId();
            symbolGraphic.setId(symbolId);
        });
    };


    /**
     * @public
     * @inheritDoc
     */
    snapAllSymbolsToClosestYPositions()
    {
        let topVisibleSymIndex = this._spinner.numSymAboveVisibleArea;
        let topVisibleSymPosY = this._symbolIdleYPositions[topVisibleSymIndex];
        let totalNumSymbols = this.symbols.length;

        // Check all symbol graphics, and find the symbol closest to the
        // top visible symbol position
        let bestDeltaY = Math.abs(this.symbols[0].y - topVisibleSymPosY);
        let indexOfSymbolGraphicToPlaceInTopVisiblePosition = 0;
        for (let potentialSymIndex = 0; potentialSymIndex < totalNumSymbols; potentialSymIndex ++)
        {
            let symbolGraphicPosY = this.symbols[potentialSymIndex].y;
            let deltaY = Math.abs(symbolGraphicPosY - topVisibleSymPosY);

            if (deltaY < bestDeltaY)
            {
                bestDeltaY = deltaY;
                indexOfSymbolGraphicToPlaceInTopVisiblePosition = potentialSymIndex;
            }
        }

        // Now we have the best possible candidate, we will assume that all
        // of the symbols are in some rough top to bottom order.
        for (let i = 0; i < totalNumSymbols; i ++)
        {
            let targetSymPosIndex = (i + this._spinner.numSymAboveVisibleArea) % totalNumSymbols;
            let symGraphicIndex = (indexOfSymbolGraphicToPlaceInTopVisiblePosition + i) % totalNumSymbols;
            let symGraphic = this.symbols[symGraphicIndex];
            symGraphic.y = this._symbolIdleYPositions[targetSymPosIndex];
        }

        this.sortSymbolsByY();
    };


    /**
     * @public
     * @inheritDoc
     */
    resetAllSymbolPositions()
    {
        this.symbols.forEach((symGraphic, symIndex) => {
            symGraphic.y = this._symbolIdleYPositions[symIndex];
        });
    };


    /**
     * @public
     * @inheritDoc
     */
    showIdleState()
    {
        this.log.debug(`Reel[${this.reelIndex}].showIdleState()`);

        this.iterateSymbols(symbolGraphic => symbolGraphic.showIdleState());
    };


    /**
     * @public
     * @inheritDoc
     */
    showHeldState()
    {
        this.log.debug(`Reel[${this.reelIndex}].showHeldState()`);

        this.iterateSymbols(symbolGraphic => symbolGraphic.showHeldState());
    };


    /**
     * @public
     * @inheritDoc
     */
    showSpinningState()
    {
        this.log.debug(`Reel[${this.reelIndex}].showSpinningState()`);

        this.iterateSymbols(symbolGraphic => symbolGraphic.showSpinningState());
    };


    /**
     * @public
     * @inheritDoc
     * @see {ReelView#showBigWinState}
     * @param {boolean[]} symIsWinning 
     * @param {boolean} fadeNonWinningSymbols 
     */
    showBigWinState(symIsWinning, fadeNonWinningSymbols)
    {
        let remappedSymIsWinning = this.getRemappedSymIsWinning(symIsWinning);

        this.log.debug(`Reel[${this.reelIndex}].showBigWinState(symIsWinning:[${symIsWinning}],remapped:[${remappedSymIsWinning}])`);

        this.iterateSymbols((symbolGraphic, isInVisibleArea, visibleSymIndex) => {
            let isWinningSym = isInVisibleArea && remappedSymIsWinning[visibleSymIndex];

            if (isWinningSym) {
                symbolGraphic.showBigWinState();
            }
            else
            if (fadeNonWinningSymbols) {
                symbolGraphic.showNonWinState();
            }
        });
    };


    /**
     * @public
     * @inheritDoc
     * @see {ReelView#showQuickWinState}
     * @param {boolean[]} symIsWinning 
     * @param {boolean} fadeNonWinningSymbols 
     */
    showQuickWinState(symIsWinning, fadeNonWinningSymbols)
    {
        let remappedSymIsWinning = this.getRemappedSymIsWinning(symIsWinning);

        this.log.debug(`Reel[${this.reelIndex}].showBigWinState(symIsWinning:[${symIsWinning}],remapped:[${remappedSymIsWinning}])`);

        this.iterateSymbols((symbolGraphic, isInVisibleArea, visibleSymIndex) => {
            let isWinningSym = isInVisibleArea && remappedSymIsWinning[visibleSymIndex];

            if (isWinningSym) {
                symbolGraphic.showQuickWinState();
            }
            else
            if (fadeNonWinningSymbols) {
                symbolGraphic.showNonWinState();
            }
        });
    };


    /**
     * @public
     * @inheritDoc
     * @see {ReelView#showQuickWinState}
     * @param {boolean[]} symIsWinning 
     * @param {boolean} fadeNonWinningSymbols 
     */
    showIdleWinState(symIsWinning, fadeNonWinningSymbols)
    {
        let remappedSymIsWinning = this.getRemappedSymIsWinning(symIsWinning);

        this.log.debug(`Reel[${this.reelIndex}].showIdleWinState(symIsWinning:[${symIsWinning}],remapped:[${remappedSymIsWinning}])`);

        this.iterateSymbols((symbolGraphic, isInVisibleArea, visibleSymIndex) => {
            let isWinningSym = isInVisibleArea && remappedSymIsWinning[visibleSymIndex];

            if (isWinningSym) {
                symbolGraphic.showIdleWinState();
            }
            else
            if (fadeNonWinningSymbols) {
                symbolGraphic.showNonWinState();
            }
        });
    };


    /**
     * Takes a map if which visible symbols are winning, and returns a "translated" map, which
     * indicates the actual symbols which need to be told to be highlighted. When we have tall
     * symbols showing, then the symbol we need to invoke a win animation / state change on, is
     * not always in the same symbol position index as the symbol that is originally winning
     * (eg: the winning index might be the top symbol of a tall symbol, but its the bottom symbol
     * that we need to actually invoke the animation on). If not tall symbols are defined for
     * the reel, then this method will just return the input data. This method takes the actual
     * current TallSymbol state of each symbol into account, in pre-defined ways. For example:
     * - "involved in tall" is understood as a hidden state, so we need to find the symbol that
     *   is in the primary "is in tall symbol" state
     * @private
     * @param {boolean[]} symIsWinning 
     * @return {boolean[]}
     */
    getRemappedSymIsWinning(symIsWinning)
    {
        let tallSymbolsData = this._spinner.tallSymbolsData;
        if (tallSymbolsData && tallSymbolsData.length > 0)
        {
            if (symIsWinning.indexOf(true) === -1) {
                return symIsWinning;
            }

            let remappedSymIsWinning = symIsWinning.slice();

            // For every tall symbol that we find, is any position involved in it winning ?
            tallSymbolsData.forEach((tallSymbol, tallSymbolIndex) => {
                let tallSymbolInvolvedInWin = false;

                // First, check if any position within this tall symbol is winning.
                for (let posIndex = tallSymbol.startSymIndex; posIndex <= tallSymbol.finalSymIndex; posIndex ++) {
                    if (symIsWinning[posIndex]) {
                        tallSymbolInvolvedInWin = true;
                        break;
                    }
                }

                if (tallSymbolInvolvedInWin) {
                    for (let posIndex = tallSymbol.startSymIndex; posIndex <= tallSymbol.finalSymIndex; posIndex ++) {
                        let posIndexWithinReel = posIndex + this._numSymAboveVisibleArea;
                        let symbolGraphic = this.symbols[posIndexWithinReel];

                        if (symbolGraphic.isInvolvedInTallSymbol()) {
                            remappedSymIsWinning[posIndex] = false;
                        }
                        else
                        if (symbolGraphic.isTallSymbol()) {
                            remappedSymIsWinning[posIndex] = true;
                        }
                    }
                }
            });

            return remappedSymIsWinning;
        }
        else
        {
            return symIsWinning;
        }
    };


    /**
     * @public
     * @inheritDoc
     * @param {boolean} anchorAtBottom
     * @param {TallSymbolsData[]} tallSymbolsData
     */
    setAnyTallSymbolsToTallOrInvolved(anchorAtBottom, tallSymbolsData)
    {
        let symbolSetToTall = ArrayUtils.createArray(this.numSymVisible, false);

        this.log.debug(`Reel[${this.reelIndex}].setAnyTallSymbolsToTallOrInvolved(anchorAtBottom:${anchorAtBottom})`);

        if (tallSymbolsData && tallSymbolsData.length > 0)
        {
            tallSymbolsData.forEach(tallSymbol =>
            {
                this.log.debug(`Reel[${this.reelIndex}]: ${JSON.stringify(tallSymbol)}`);

                let tallStartIndex = tallSymbol.startSymIndex;
                let tallFinalIndex = tallSymbol.finalSymIndex;
                let numSymInTall = tallSymbol.numSymbols;
                let targetTallPosIndex = anchorAtBottom? tallFinalIndex : tallStartIndex;

                for (let visiblePosIndex = tallStartIndex; visiblePosIndex <= tallFinalIndex; visiblePosIndex ++)
                {
                    let posIndexWithinReel = visiblePosIndex + this._numSymAboveVisibleArea;
                    let symbolGraphic = this.symbols[posIndexWithinReel];
                    let setToTall = visiblePosIndex === targetTallPosIndex;

                    if (setToTall) {
                        symbolGraphic.showAsTallSymbol(numSymInTall, anchorAtBottom);
                    }
                    else symbolGraphic.showAsInvolvedInTallSymbol();

                    symbolSetToTall[visiblePosIndex] = true;
                }
            });
        }

        // Set any remaining "non-tall" symbols to normal state
        symbolSetToTall.forEach((isSet, visiblePosIndex) => {
            if (!isSet) {
                let posIndexWithinReel = visiblePosIndex + this._numSymAboveVisibleArea;
                this.symbols[posIndexWithinReel].showAsNormalSymbol();
            }
        });
    };


    /**
     * Returns an array "map" of all visible symbols for this reel, indicating which tall symbol
     * group they are involved in. This is an alternate translation of any "tallSymbolsData" that
     * is returned by the spinner. The array returned is guaranteed to have the same length as the
     * number of visible symbols on the reel. Each value is a number. A value of 0 indicates "not
     * involved in any tall symbol". A value greater than 0, means "involved in a tall symbol": the
     * actual value tells you the progressive id of the tall symbol, relative to the reel.
     * 
     * EG: suppose we had a reel with 10 visible symbols. There are 2 tall symbols (the id of the
     * symbol shown for each tall symbol does not matter here). A map with the returned value of
     * [0,1,1,1,1,0,0,2,2,0] tells us that from visible indices 1 to 4, we have 1 tall symbol (which
     * is 4 tall), and from visible symbols with idices 8 to 9, we have a second tall symbol (which
     * is 2 tall). The same "progressive id" is attached to all symbol positions involved in the
     * same tall symbol.
     * 
     * This representation of tall symbols data, is useful when we want to quickly iterate over the
     * positions of a reel, and pick out groups of tall symbols.
     * @private
     * @return {number[]}
     */
    getTallSymbolsGroupMap()
    {
        let numSymOnReel = this.numSymVisible;
        let tallSymMap = ArrayUtils.createArray(numSymOnReel, 0);

        let tallSymbolsData = this._spinner.tallSymbolsData;
        if (tallSymbolsData) {
            tallSymbolsData.forEach((tallSymbol, tallSymbolIndex) => {
                let progressiveTallSymId = tallSymbolIndex + 1;
                tallSymMap.fill(progressiveTallSymId, tallSymbol.startSymIndex, tallSymbol.finalSymIndex + 1);
            });
        }

        return tallSymMap;
    };


    /**
     * @public
     * @inheritDoc
     */
    showFadeState()
    {
        this.iterateSymbols(symbolGraphic => symbolGraphic.showNonWinState());
    };


    /**
     * @public
     * @inheritDoc
     * @param {(symbol:SymbolView,isInVisibleArea:boolean,visibleSymIndex?:number)=>void} iterator 
     */
    iterateSymbols(iterator)
    {
        let startVisibleIndex = 0 + this._numSymAboveVisibleArea;
        let finalVisibleIndex = startVisibleIndex + this.numSymVisible - 1;

        this.symbols.forEach((symbolGraphic, symbolIndex) => {
            let visibleSymIndex = null;
            let isInVisibleArea = symbolIndex >= startVisibleIndex && symbolIndex <= finalVisibleIndex;
            if (isInVisibleArea) {
                visibleSymIndex = symbolIndex - this._numSymAboveVisibleArea;
            }
            iterator(symbolGraphic, isInVisibleArea, visibleSymIndex);
        });
    };


    /**
     * @public
     * @inheritDoc
     */
    fireInfiniteSpinStartedEvent()
    {
        this.emit(C_ReelEvents.INFINITE_SPIN_STARTED, this.reelIndex);
    };


    /**
     * @public
     * @inheritDoc
     */
    fireReelSettlingEvent()
    {
        this.emit(C_ReelEvents.REEL_SETTLING, this.reelIndex);
    }


    /**
     * @public
     * @inheritDoc
     */
    fireReelStoppedEvent()
    {
        this.emit(C_ReelEvents.REEL_STOPPED, this.reelIndex);
    };


    /**
     * @public
     * @inheritDoc
     */
    triggerReelStopSound()
    {
        this._app.dispatcher.emit(C_GameEvent.PLAY_REEL_STOP_SOUND, this.reelIndex);
    };


    /**
     * @public
     * @inheritDoc
     * @param {number} value
     */
    set speedScaleFactor(value)
    {
        this._spinner.speedScaleFactor = value;
    };


    /**
     * @public
     * @inheritDoc
     * @return {number}
     */
    get speedScaleFactor()
    {
        return this._spinner.speedScaleFactor;
    };
}

export default Reel