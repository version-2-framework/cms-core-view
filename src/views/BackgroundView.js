import BaseView from "./BaseView";

class BackgroundView extends BaseView
{


    constructor()
    {
        super();

        this.addChild(this.assets.getSprite('mainBG'));
        this.layout.propHeightScale(this, 100);
    }



}

export default BackgroundView