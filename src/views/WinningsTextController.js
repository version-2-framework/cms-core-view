import { getGameViewLogger } from "../../../cms-core-logic/src/logging/LogManager";

const log = getGameViewLogger();

/**
 * A dedicated component for controlling text shown on a winnings field. This is not a view, but
 * a tool for modifying a view in real time. When you create an instance of this, you assign it a
 * configuration (which defines how it goes about performing a count up animation), and a target
 * upon which it works.
 * 
 * The target can be a PIXI.TextField (or anything which exposes a "set text" property, which is
 * the only part of the PIXI.TextField api that is used), OR it can be a method, which will be
 * invoked whenever text should be updated. When the target is a view, the controller does nothing
 * else to manipulate the view - it is just something that needs to be updated at set points. The
 * Config object provides callbacks for "start" / "end" / "afterTextUpdate" methods, which allow
 * you to maniuplate any graphical objcets on set hooks. This leaves the controller as a bare
 * bones mechanic for implementing the count-up, with no real opinion on how the actual view part
 * should work.
 * 
 * The controller maintains internal state, about any count up animation that is in progress.
 * It can be interrupted at any point (meaning the target being manipulated will be immediately
 * set to final value). It guarantees to hit the correct final value if left to its own devices.
 * Actual mechanic for counting up values is confoigurable - you can choose a preset routine, or
 * create a custom one. Speed at which updates are performed can also be set (it does not have to
 * be every visual update of the browser).
 */
export class WinningsTextController
{
    /**
     * 
     * @param {WinningsTextControllerTarget} target
     * The textfield being controlled.
     * @param {WinningsTextControllerConfig} config
     */
    constructor(target, config)
    {
        /**
         * Cached data object, representing any tween that is currently in progress, but also holding
         * meta-data about it (final value to show, and a method to kill the tween)
         * @private
         * @type {{ tween:gsap.TweenMax, tweenTarget:Object, killTween:() => void, finalValue:number }}
         */
        this._updateWinningsTween = null;

        /**
         * The method which will set text on our target. We check if the passed target is a function (in
         * which case it is assigned directly here). If target is an object (eg: has a "set text" field,
         * like a PIXI.TextField) then we assign a method to update it here.
         * @private
         * @type {(text:string) => void}
         */
        this._setTextMethod = null;

        if (typeof target === "function") {
            this._setTextMethod = target;
        }
        else
        {
            this._setTextMethod = textString => {
                target.text = textString;
            }
        }

        this.setConfig(config)
    };


    /**
     * @public
     * @param {WinningsTextControllerConfig} config 
     */
    setConfig(config)
    {
        this.killAnyCountUpAnimation();

        /**
         * Current value shown.
         * @private
         * @type {number}
         */
        this._currentValue = config.initialValue;

        /**
         * The method which will convert current value to show, into a usefull text string.
         * @private
         * @type {WinningsTextControllerValueStringProvider}
         */
        this._getTextString = config.getTextString ? config.getTextString : (currVal) => currVal.toString();

        /**
         * The method which will calculate the increment to increase by on each update.
         * @private
         * @type {WinningsTextControllerIncrementCalculator}
         */
        this._getIncrement = this.getIncrementCalculator(config.incrementConfig);

        /**
         * Speed of the update animation, in the range 0 to 1, where 1 indicates "max number of updates per
         * second", and 0.5 would indicate "half of max number of updates per second". This value is
         * guaranteed to be >= 0 and <= 1: we either use a default, or a validated version of the value that
         * is passed in through config.
         * @private@type {number}
         */
        this._updateSpeed = config.updateSpeed >= 0? Math.min(1, Math.max(0, config.updateSpeed)) : 1;

        /**
         * @private
         */
        this._onCountUpStart = config.onCountUpStart;

        /**
         * @private
         */
        this._onCountUpFinish = config.onCountUpFinish;

        /**
         * @private
         */
        this._onUpdate = config.onUpdate;
    };


    /**
     * Starts an animation on the winnings field, counting up from currently cached "correct" value,
     * adding on the amount provided. This is the same as "setToNewValueWithAnimation", except that
     * it allows you to pass in the amount to add on, rather than an absolute final value. As with
     * the other methods, existing animations will be cleared: the "current value" that the animation
     * will start from, is the correct last known value that the field was supposed to be counting
     * up to.
     * @public
     * @param {number} amountToAdd
     */
    addToValueWithAnimation(amountToAdd)
    {
        let newValue = this._currentValue + amountToAdd;

        this.killAnyCountUpAnimation();
        this.startCountUpAnimation(newValue);
    };


    /**
     * Shows a new value on the winnings field, using an asynchronous counting up animation.
     * This animation can be interrupted, by
     * - calling this method again (this method automatically interrupts any existing animations)
     * - calling "setToNewValueImmediately"
     * - calling ""
     * This method counts up from the currently known "final" winnings value, to the new target
     * value.
     * @public
     * @param {number} finalValue
     * The new, final value to show on the winnings field.
     */
    setToNewValueWithAnimation(finalValue)
    {
        this.killAnyCountUpAnimation();
        this.startCountUpAnimation(finalValue);
    };


    /**
     * Internal method, which starts a count up sequence going.
     * @private
     * @param {number} finalValue 
     */
    startCountUpAnimation(finalValue)
    {
        let tweenTarget = {};
        let startValue = this._currentValue;
        let currValue = startValue;
        let totalDeltaValue = finalValue - startValue;
        let amountStillToAdd = totalDeltaValue;

        this._currentValue = finalValue;

        log.debug(`WinningsTextController.startCountUpAnimation(startVal:${startValue},finalVal:${finalValue})`);

        let setToFinalValue = () =>
        {
            let finalValueString = this._getTextString(finalValue);

            log.debug(`WinningsTextController.setToFinalValue(finalValue:${finalValueString})`);

            this._setTextMethod(finalValueString);
        }

        /**
         * Function which will kill the existing tween, and immediately show the correct final value
         * on the text field.
         */
        let killTween = () =>
        {
            tween.kill();
            setToFinalValue();
            if (this._onCountUpFinish) {
                this._onCountUpFinish(finalValue);
            }
        }

        let numTicksBetweenUpdate = Math.round(1 / this._updateSpeed);
        let numUpdatesPerSecond = 60 * this._updateSpeed;

        // Create the actual tween
        let numTicksSinceLastChange = 0;
        let deltaValuePerUpdate = this._getIncrement(totalDeltaValue, numUpdatesPerSecond);
        let tween = new TimelineMax();

        if (this._onCountUpStart) {
            this._onCountUpStart(startValue);
        }

        tween.to(
            tweenTarget,
            99999, // we can safely say that this many seconds is far more than we need...
            {
                onUpdate : () =>
                {
                    numTicksSinceLastChange += 1;

                    if (numTicksSinceLastChange === numTicksBetweenUpdate)
                    {
                        if (amountStillToAdd > 0)
                        {
                            amountStillToAdd -= deltaValuePerUpdate;
                            currValue = Math.min(finalValue, currValue + deltaValuePerUpdate);

                            let displayValue = Math.round(currValue);

                            this._setTextMethod(this._getTextString(displayValue));

                            if (this._onUpdate) {
                                this._onUpdate(this._currentValue);
                            }
                        }
                        else
                        {
                            // This will end any existing tween sequence, and clear references to it.
                            killTween();
                            this._updateWinningsTween = null;
                        }

                        numTicksSinceLastChange = 0;
                    }
                },

                onComplete: setToFinalValue
            });

        this._updateWinningsTween = 
        {
            tween, tweenTarget, killTween, finalValue
        };
    };


    /**
     * Refreshes the Winnings Text Field. This is equivalent to calling "setToNewValueImmediately",
     * but passing the current target value to show. For cases where you might have some special logic
     * going on in a custom "getTextString" method (passed to the WinningsTextController via the config
     * object), this could be useful : you might have changed some external parameter, which affects
     * the text string generated, and you want to redraw the text field immediately.
     * @public
     */
    refresh()
    {
        this.setToNewValueImmediately(this._currentValue);
    };


    /**
     * Sets the winnings field to show a new value immediately, with no animated transition. Any
     * existing animated transition will be immediately killed.
     * @public
     * @param {number} newValue 
     */
    setToNewValueImmediately(newValue)
    {
        this.killAnyCountUpAnimation();
        
        let newTextValue = this._getTextString(newValue);
        this._setTextMethod(newTextValue);
        
        log.debug(`WinningsTextController.setToNewValueImmediately: ${newTextValue}`);
    };


    /**
     * Immediately kills any count-up animation, and shows the final value that the count up was
     * aiming towards.
     * @public
     */
    killAnyCountUpAnimation()
    {
        if (this._updateWinningsTween) {
            log.debug('WinningsTextController.killAnyCountUpAnimation()');

            this._updateWinningsTween.killTween();
            this._updateWinningsTween = null;
        }
    };


    /**
     * Factory method, which returns an increment calculator, that will let us determine the increment
     * for an animation
     * @private
     * @param {WinningsTextControllerIncrementConfig} config
     * @return {WinningsTextControllerIncrementCalculator}
     */
    getIncrementCalculator(config)
    {
        /** @type {WinningsTextControllerIncrementCalculator} */
        let calculator;

        // In simple mode, we literally just use a fixed value.
        if (!config)
        {
            // TODO: Return default routine..

            // let deltaValue = 2 * numUpdatesBetweenChange;

            calculator = (totalWinningsDelta, numUpdatesPerSecond) => {
                let numUpdatesBetweenChange = 60 / numUpdatesPerSecond;
                return 2 * numUpdatesBetweenChange;
            }
        }
        else
        if (config.type === "simple") {
            calculator = () => config.increment;
        }
        // in fixed speed mode, we have a speed per second, and we calculate increment dynamically,
        // according to update rate of the animation.
        else
        if (config.type === "fixedSpeed") {
            calculator = (totalWinningsDelta, numUpdatesPerSecond) => {
                return config.incrementPerSecond / numUpdatesPerSecond;
            }
        }
        // Check length of value that we are increasing by, and return a fixed value (per update):
        // the more chars in the amount we increase by, the larger the increment per step.
        else
        if (config.type === "auto") {
            // This is based on a routine originally implemented by Igor @ Wiener. The basic
            // calculation is good, but fails when there are fewer than 100 chars (starts to
            // try and increase by values less than 1). All we have to do is manually check
            // for a few cases, and for any larger value, we use the dynamic calculation.
            calculator = (totalWinningsDelta) => {
                if (totalWinningsDelta < 10) {
                    return 1;
                }
                else
                if (totalWinningsDelta < 20) {
                    return 2;
                }
                else
                if (totalWinningsDelta < 100) {
                    return 5;
                }
                // This routine works fine for any value that has 3 or more chars
                else {
                    return Math.pow(10, totalWinningsDelta.toString().length - 2) * .5;
                }
            }
        }
        else
        if (config.type === "custom") {
            calculator = config.method;
        }
        
        return calculator;
    }
}