import { getGameViewLogger } from "../../../cms-core-logic/src/logging/LogManager";

const State =
{
    HIDDEN : 0,
    SHOWING : 1,
    HIDING : 2
}

/**
 * @implements {LoadingSpinnerView}
 */
export class DefaultLoadingSpinner extends PIXI.Container
{
    constructor()
    {
        super();

        let assets = PIXI.app.assets;

        /**
         * @private
         */
        this._layout = PIXI.app.layout;

        /**
         * Tracks the internal visual state of the Loading Spinner.
         * @private
         * @type {number}
         */
        this._state = State.HIDDEN;

        /**
         * @private
         */
        this._log = getGameViewLogger();

        /**
         * @private
         * @type {TweenMax}
         */
        this._fadeInTween = null;

        /**
         * @private
         * @type {TweenMax}
         */
        this._fadeOutTween = null;

        /**
         * @private
         * @type {TweenMax}
         */
        this._spinningTween = null;

        /**
         * A spinner icon, which we can show while we are waiting for the game session to be closed.
         * @private
         * @type {PIXI.DisplayObject}
         */
        this._loadingCircle = assets.getSprite('wmg_loadingIcon_30degrees');
        this._loadingCircle.anchor.set(0.5, 0.5);
        this._loadingCircle.x = this._layout.relWidth(50);
        this._loadingCircle.y = this._layout.relHeight(50);
        this._layout.propHeightScale(this._loadingCircle, 20);
        this.addChild(this._loadingCircle);
    };


    /**
     * @public
     * @inheritDoc
     */
    showLoadingSpinner()
    {
        if (this._fadeOutTween) {
            this._fadeOutTween.kill();
            this._fadeOutTween = null;
        }

        if (this._fadeInTween === null)
        {
            this._fadeInTween = TweenMax.fromTo(this._loadingCircle, 0.50, { alpha:0 }, { alpha:1 });
        }

        if (this._spinningTween === null)
        {
            this._spinningTween = TweenMax.to(this._loadingCircle, 2, { rotation:Math.PI, repeat:-1, ease:"Linear.easeNone" });
        }
    };


    /**
     * @public
     * @inheritDoc
     */
    hideLoadingSpinner()
    {
        if (this._fadeInTween) {
            this._fadeInTween.kill();
            this._fadeInTween = null;
        }

        if (this._fadeOutTween === null)
        {
            let onComplete = () =>
            {
                this._spinningTween.kill();
                this._spinningTween = null;
            }

            this._fadeOutTween = TweenMax.to(this._loadingCircle, 0.25, { alpha:0, onComplete });
        }
    };
}