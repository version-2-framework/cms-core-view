///<reference path="../../../cms-core-logic/src/types/SlotGamePhaseResultModel.d.ts"/>

import * as LogManager from "../../../cms-core-logic/src/logging/LogManager";
import { KeyboardManager } from "../input/KeyboardManager";
import Assets from "../utils/Assets";
import Layout from "../utils/Layout";
import { ReelPositionUtil } from "./ReelPositionUtil";
import getActiveSlotLayout from "../../../cms-core-logic/src/utils/getActiveSlotLayout";
import { ExitClientUtil } from "../../../cms-core-logic/src/utils/ExitClientUtil";

/**
 * BaseView - abstraction for a display object container with utility methods.
 * Automatically destroyed when removed from the scene.
 */
class BaseView extends PIXI.Container
{

    constructor()
    {
        super();
        this.once('removed', this.destroy, this);
        this.app = PIXI.app;

        /**
         * @private
         */
        this._tweensInProgress = [];

        /**
         * @private
         */
        this._eventsBeingListenedFor = [];

        this.inject();
    };


    /**
     * @private
     */
    inject()
    {
        this.log = LogManager.getGameViewLogger();

        this._layout = this.app.layout;
        this._sound = this.app.sound;
        this._config = this.app.config;
        this._viewManager = this.app.viewManager;

        /**
         * @private
         * @type {KeyboardManager}
         */
        this._keyboardManager = this.app.keyboardManager;

        /**
         * @private
         * @type {Assets}
         */
        this._assets = this.app.assets;

        /**
         * @type {BuildInfoApi}
         * @private
         */
        this._buildInfo = this.app.buildInfo;

        /**
         * @type {BusinessConfig}
         */
        this._businessConfig = this.app.businessConfig;

        /**
         * @protected
         * @type {Model}
         */
        this._model = this.app.model;

        /**
         * @protected
         * @type {PlatformApi}
         */
        this._platform = this.app.platform;

        /**
         * @protected
         * @type {PIXI.utils.EventEmitter}
         */
        this._dispatcher = this.app.dispatcher;

        // TODO: These 2 fields must be removed. Access to them is not required.
        this._controller = this.app.controller;

        /**
         * @protected
         * @type {LocaleApi}
         */
        this._locale = this.app.locale;
        this._localData = this.app.localData;

        /**
         * @protected
         * @type {GameFactoryApi}
         */
        this._gameFactory = this.app.gameFactory;

        /**
         * @protected
         * @type {SpinPhaseModel}
         */
        this._spinModel = this.app.spinModel;

        /**
         * @protected
         * @type {dragonBones.PixiFactory}
         */
        this._dragonBones = this.app.dragonBones;

        /**
         * @protected
         */
        this._openLinkUtil = this.app.openLinkUtil;

        /**
         * @private
         * @type {ExitClientUtil}
         */
        this._exitClientUtil = this.app.exitClientUtil;

        /**
         * @private
         * @type {SlotSpinSoundController}
        */
       this._spinSoundController = this.app.spinSoundController;

       /**
        * @private
        * @type {ReelPositionUtil}
        */
       this._reelPositionUtil = this.app.reelPositionUtil;
    };


    // View injection of potentially needed dependencies. Also used to improve auto-complete

    /**
     * @protected
     * @return {BuildInfoApi}
     */
    get buildInfo() {
        return this._buildInfo;
    }

    /**
     * @protected
     * @return {KeyboardManager}
     */
    get keyboardManager() {
        return this._keyboardManager;
    }

    /**
     * @returns {Layout}
     */
    get layout() {
        return this._layout;
    };

    
    /**
     * Returns access to the Game's Sound Controller instance.
     * @returns {SoundControllerApi}
     */
    get sound() {
        return this._sound;
    }

    /** @returns {GameConfig|*} */ get config() { return this._config; }
    /** @returns {ViewManager|*} */ get viewManager() { return this._viewManager; }

    /** @returns {Assets} */
    get assets() { return this._assets; }

    /** @returns {Model} */
    get model() {
        return this._model;
    }

    /**
     * @return {PlatformApi}
     */
    get platform() {
        return this._platform;
    }

    /** @returns {PIXI.utils.EventEmitter} */
    get dispatcher() {
        return this._dispatcher;
    }


    // TODO: This is redundant, but it may not be sfe to deprecate it yet
    /**
     * @public
     * @returns {GameController}
     */
    get controller() {
        return this._controller;
    }
    
    /**
     * @public
     * @final
     * @returns {LocaleApi}
     */
    get locale() {
        return this._locale;
    }

    /**
     * @public
     * @final
     * @returns {LocalData}
     */
    get localData() {
        return this._localData;
    }

    /**
     * @public
     * @final
     * @returns {GameFactoryApi}
     */
    get gameFactory() {
        return this._gameFactory;
    }

    /**
     * @public
     * @final
     * @returns {dragonBones.PixiFactory}
     */
    get dragonBones() {
        return this._dragonBones;
    }

    /**
     * @protected
     * @final
     * @returns {SpinPhaseModel}
     */
    get spinModel() {
        return this._spinModel;
    }

    /**
     * @protected
     * @final
     * @return {SlotSpinSoundController}
     */
    get spinSoundController() {
        return this._spinSoundController;
    };

    /**
     * @protected
     * @final
     * @return {ExitClientUtil}
     */
    get exitClientUtil() {
        return this._exitClientUtil;
    };


    /**
     * Add a listener which is automatically disposed when the view is disposed
     * @protected
     * @param {EventEmitter} emitter
     * @param {string} eventType
     * @param {function} callback
     * @param [scope]
     */
    addListener(emitter, eventType, callback, scope)
    {
        this._eventsBeingListenedFor.push({emitter:emitter, eventType:eventType, callback:callback, scope:scope});
        emitter.on(eventType, callback, scope);
    };


    /**
     * Not fully implemented.
     * The idea is to keep track of all delayed calls so they can be killed when the view is destroyed.
     * @protected
     * @param delay
     * @param callback
     * @param [args]
     * @param [scope]
     * @returns {TweenMax}
     */
    addDelayedCall(delay, callback, args, scope)
    {
        return TweenMax.delayedCall(delay, callback, args, scope);
    };


    /**
     * Returns the current active gui layout configuration
     * @protected
     * @return {GuiLayoutConfig}
     */
    getActiveGuiLayout()
    {
        // TODO: Expand this to support other layout types..
        if (this._platform.isMobileOrTablet()) {
            return this._config.uiLayoutMobile;
        }
        else return this._config.uiLayoutDesktop;
    };


    /**
     * Returns the active Slot Layout Config.
     * @protected
     * @final
     * @return {SlotLayoutConfig}
     */
    getActiveSlotLayout()
    {
        return getActiveSlotLayout(this.app);
    };


    /**
     * @protected
     * @return {ReelPositionUtil}
     */
    get reelPositionUtil() {
        return this._reelPositionUtil;
    };


    /**
     * Convenience function, to remove this view class from its parent. Because anything extending
     * from BaseView destroys itself when its removed, this also has the effect of destroying this
     * instance.
     * @public
     */
    removeFromParent()
    {
        if (this.parent)
        {
            this.parent.removeChild(this);
        }
    };


    /**
     * Dispose of the object, called automatically when removed from stage
     * @public
     * @inheritDoc
     */
    destroy()
    {
        const numTweens = this._tweensInProgress.length;
        for (let tweenIdx = 0; tweenIdx < numTweens; tweenIdx++)
        {
            let t = this._tweensInProgress[tweenIdx];
            t.kill();
        }

        for (let evtIdx = 0; evtIdx < this._eventsBeingListenedFor.length; evtIdx++)
        {
            let evtData = this._eventsBeingListenedFor[evtIdx];
            let emitter = evtData.emitter;
            emitter.removeListener(evtData.eventType, evtData.callback, evtData.scope);
        }

        this.removeChildren();
        this.removeAllListeners();

        super.destroy({children:true});
    };
}

export default BaseView;