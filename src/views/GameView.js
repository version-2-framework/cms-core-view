import noop from "../../../cms-core-logic/src/utils/noop";
import C_ViewEvent from "../const/C_ViewEvent";
import { shakeDisplayObjectSimple } from "../utils/ShakeUtil";
import BaseView from "./BaseView";
import ReelDebugLayer from "./ReelDebugLayer";

/**
 * Game View contains elements which relate to a Slot Game (if you want to make a Roulette
 * game, you would probably want to pick an alternate implementation). It is slot game
 * specific (it should really have been called SlotGameView) - but this is a legacy detail
 * now.
 */
class GameView extends BaseView
{
    // TODO: Let's tidy up and document some code here.
    constructor()
    {
        super();

        let guiLayoutData = this.getActiveGuiLayout();
        let slotLayoutData = this.getActiveSlotLayout();

        /**
        /**
         * Main background for the game.
         * @private
         * @type {PIXI.DisplayObject}
         */
        this._bg = this.gameFactory.getBackgroundView();

        /**
         * Background layer available to Win Presentatiom View (but shown above BG)
         * @private
         * @type {PIXI.Container}
         */
        this._winPresentationBackground = new PIXI.Container();

        /**
         * Container for all "reels-space" components.
         * @private
         * @type {PIXI.Container}
         */
        this._reelsContainer = new PIXI.Container();

        /**
         * The main ReelGroup, showing reels, symbols, spin animations, etc.
         * @private
         * @type {PIXI.DisplayObject}
         */
        this._reelGroup = this.gameFactory.getReelGroup();
        this._reelGroup.create();

        if (this.config.showReelsBg)
        {
            let reelsBg = this.assets.getSprite('reels_bg');
            reelsBg.x = this.config.reelsBgPosX;
            reelsBg.y = this.config.reelsBgPosY;
            reelsBg.width  = this.config.reelsBgWidth;
            reelsBg.height = this.config.reelsBgHeight;

            this._reelsContainer.addChild(reelsBg);
        }
        
        if (this.gameFactory.getReelBacks)
        {
            /**
             * Optional background layer to the reels.
             * @private
             * @type {PIXI.DisplayObject}
             */
            this._reelBacks = this.gameFactory.getReelBacks();
            this._reelsContainer.addChild(this._reelBacks); // TODO: reorganize order
            this._reelBacks.create(this._reelGroup);  // I still want to remove the dependency here - i am pretty sure its unnecessary (it MUST be)
        }

        this._reelsContainer.addChild(this._reelGroup);

        if (this.gameFactory.getReelFrame)
        {
            /**
             * Optional foreground layer to the reels.
             * @private
             * @type {PIXI.DisplayObject}
             */
            this._reelFrame = this.gameFactory.getReelFrame();

            this._reelsContainer.addChild(this._reelFrame);
        }

        /**
         * Container used for foreground elements of the Win Presentation layer.
         * @private
         * @type {PIXI.Container}
         */
        this._winPresentationForeground = new PIXI.Container();
        
        /**
         * @private
         * @type {Object}
         */
        this._winPresentationView = this.gameFactory.getWinPresentationView(
            this._winPresentationForeground, this._winPresentationBackground);

        if (this.config.winlines != null)
        {
            /**
             * Optional winlines view, which shows winline number buttons / winlines to the player.
             * @private
             * @type {PIXI.DisplayObject}
             */
            this._winlinesView = this.gameFactory.getWinlinesView();

            this._reelsContainer.addChild(this._winlinesView);
        }
        
        if (this.config.isTwoSpin) {
            let spinIndicator = this.gameFactory.getSpinIndicator();
            if (spinIndicator)
            {
                /**
                 * Optional Spin Indicator. Used on two spin games.
                 * @private
                 * @type {PIXI.DisplayObject}
                 */
                this._spinIndicator = spinIndicator;

                this._reelsContainer.addChild(spinIndicator);
            }
        }
        
        // Organize main display-list elements
        this.addChild(this._bg);
        this.addChild(this._winPresentationBackground);
        this.addChild(this._reelsContainer);
        this.addChild(this._winPresentationForeground);

        // Perform layout
        let reelsPos = guiLayoutData.reelsPos;
        let targetReelsHeight = this.layout.relHeight(reelsPos.height);
        let actualReelsHeight = slotLayoutData.reelsHeight;
        let reelsScale = targetReelsHeight / actualReelsHeight;
        this._reelsContainer.scale.set(reelsScale);
        this._reelsContainer.x = this.layout.relWidth(reelsPos.x)  - (slotLayoutData.reelsAnchorPosX * reelsScale);
        this._reelsContainer.y = this.layout.relHeight(reelsPos.y) - (slotLayoutData.reelsAnchorPosY * reelsScale);

        //--------------------------------------------------------------------------------------------------
        // Setup any required shaking effect for reel container (for when spins stop). We do this here,
        // because the shake must be applied to the whole ReelContainer (which GameView has access to)
        //--------------------------------------------------------------------------------------------------
        /** @type {SlotSpinShakeConfig} */
        let shakeConfig = this.config.spinConfig.shake;
        if (shakeConfig && shakeConfig.type !== "none")
        {
            this.log.debug('GameView: setting up a shake effect for the reels');

            let target = this._reelsContainer;
            let shakeReelContainer;

            if (shakeConfig.type === "simple") {
                shakeReelContainer = () => shakeDisplayObjectSimple(target, shakeConfig);
            }
            else
            {
                shakeReelContainer = noop;
            }

            /**
             * Returns the correct view event to bind to, for shake triggering policies that are tied
             * to a specific reel spin.
             * @param {ReelSpinTriggerEvent} reelSpinStepTrigger
             * @return {string}
             */
            let getTriggerEvent = reelSpinStepTrigger => {
                if (reelSpinStepTrigger === "reelStopped") {
                    return C_ViewEvent.SINGLE_REEL_SPIN_STOP_COMPLETE;
                }
                else
                if (reelSpinStepTrigger === "infiniteSpinStart") {
                    return C_ViewEvent.SINGLE_REEL_SPIN_STARTED;
                }
                else
                if (reelSpinStepTrigger === "infiniteSpinEnd") {
                    return C_ViewEvent.SINGLE_REEL_SPIN_STOP_STARTED;
                }
            }

            if (shakeConfig.trigger === "onAllReelsStopped")
            {
                this.dispatcher.on(C_ViewEvent.ALL_REELS_STOPPED, shakeReelContainer);
            }
            else
            if (shakeConfig.trigger === "onReel")
            {
                let triggerReelIndex = shakeConfig.triggerReelIndex;

                /** @param {SingleReelSpinEventData} reelEventData */
                let onSpinEvent = reelEventData => {
                    if (reelEventData.reelIndex === triggerReelIndex) {
                        shakeReelContainer();
                    }
                }

                this.dispatcher.on(getTriggerEvent(shakeConfig.bindToReelEvent), onSpinEvent);
            }
            else
            if (shakeConfig.trigger === "onFinalReel")
            {
                /** @param {SingleReelSpinEventData} reelEventData */
                let onSpinEvent = reelEventData => {
                    if (reelEventData.reelIndex + 1 === reelEventData.numReelsInvolvedInSpin) {
                        shakeReelContainer();
                    }
                }

                this.dispatcher.on(getTriggerEvent(shakeConfig.bindToReelEvent), onSpinEvent);
            }
            else
            if (shakeConfig.trigger === "onReelProgressive")
            {
                let triggerProgressiveReelIndex = shakeConfig.triggerProgressiveReelIndex;

                /** @param {SingleReelSpinEventData} reelEventData */
                let onSpinEvent = reelEventData => {
                    if (reelEventData.reelProgressiveIndex === triggerProgressiveReelIndex) {
                        shakeReelContainer();
                    }
                }

                this.dispatcher.on(getTriggerEvent(shakeConfig.bindToReelEvent), onSpinEvent);
            }
        }
        
        //--------------------------------------------------------------------------------------------------
        // OPTIONAL DEBUG FEATURES
        // TODO: It would be good if these could be enabled with a special build flag.
        //--------------------------------------------------------------------------------------------------
        /*
        this.addChild(new ReelDebugLayer());
        */
    };


    /**
     * Returns access to the reels container, which contains everything related to reels. If you need
     * to perform some animation on it in custom sub-classes (for example, a custom shaking method),
     * then access it via this method.
     * @protected
     * @final
     * @return {PIXI.Container}
     */
    get reelsContainer() {
        return this._reelsContainer;
    }
}

export default GameView