import BaseView from "./BaseView";
import Button from "../ui/Button";
import C_PointerEvt from "../const/C_PointerEvt";
import C_Layout from "../const/C_Layout";


/**
 * Base class for windows which has built-in functionality for sizing and positioning of its elements.
 */
class BaseWindow extends BaseView
{


    constructor(modal)
    {
        super();
        this.modal = modal == undefined ? true : modal;
        this.windowLayout = C_Layout.VERTICAL;
        this.currentTabIdx = 0;
        this.leftPadding = this.rightPadding = 2;
        this.topPadding = this.bottomPadding = 8;
        this.vGap = 0;
        this.shadeAlpha = .6;
    }


    /**
     * Execute creation template which allows different parts of creation
     * to be modified as needed in subclasses
     * @public
     */
    create()
    {
        this.init();
        this.drawContent();
        this.finishDrawing();
        this.drawAfterSizing();
        this.fadein();
    }


    /**
     * @protected
     */
    init()
    {
        if (this.modal)
        {
            let bgShade = this.assets.getSprite("bg_shade");
            bgShade.anchor.set(.5,.5);
            this.layout.setToRectangle(bgShade, 0, 0, 100, 100);
            this.addChild(bgShade);
            bgShade.interactive = true;
            bgShade.alpha = this.shadeAlpha;
            this.bgShade = bgShade;
        }

        this.container = new PIXI.Container();
        this.addChild(this.container);

        this.rows = [];
    }


    createRow()
    {
        this.currentRow = new PIXI.Container();
        this.rows.push(this.currentRow);
        this.container.addChild(this.currentRow);
    }


    /**
     * Used in subclasses to draw the content of window
     * @abstract
     */
    drawContent()
    {

    }


    /**
     * Finalize creation - apply layout and position elements
     * @protected
     */
    finishDrawing()
    {
        this.applyLayout();
        this.calculateBgSize();
        this.drawBackground();
        this.x = this.layout.relWidth(50);
        this.y = this.layout.relHeight(50);
    }


    /**
     * Apply layoot to window components
     */
    applyLayout()
    {
        let totalHeight = 0;

        if (this.windowLayout == C_Layout.VERTICAL)
        {
            for (let rowIdx = 0; rowIdx < this.rows.length; rowIdx++)
            {
                totalHeight += this.rows[rowIdx].height;
            }

            totalHeight += (this.vGap * (this.rows.length - 1));
        }

        let yOrigin = -.5 * totalHeight;

        let yPos = 0;
        for (let rowIdx = 0; rowIdx < this.rows.length; rowIdx++)
        {
            let row = this.rows[rowIdx];

            row.x = row.width * -.5;

            if (this.windowLayout == C_Layout.VERTICAL)
            {
                row.y = yOrigin + yPos;
                yPos += (row.height + this.vGap);
            }
        }
    }


    calculateBgSize()
    {
        this.winBgWidth = this.container.width + this.layout.relWidth(this.leftPadding)+ this.layout.relWidth(this.rightPadding);
        this.winBgHeight = this.container.height + this.layout.relWidth(this.topPadding)+ this.layout.relWidth(this.bottomPadding);
    }


    drawBackground()
    {
        this.drawSolidBackground();
    }


    drawSolidBackground()
    {
        this.winBg = new PIXI.Graphics();
        this.winBg.lineStyle(8, this.config.winBgLineColor, 1);
        this.winBg.beginFill(this.config.winBgFillColor, 1);
        this.winBg.drawRect(0, 0, this.winBgWidth, this.winBgHeight);
        this.winBg.endFill();
        this.addChildAt(this.winBg, this.getChildIndex(this.container));
        this.layout.horizontalAlignCenter(this.winBg, -50);
        this.layout.verticalAlignMiddle(this.winBg, -50);
    }


    drawAfterSizing()
    {
        this.drawCloseBut();
    }


    drawCloseBut()
    {
        this.closeBtn = new Button('closeBut', 'closeBut_over');
        this.layout.propHeightScale(this.closeBtn, 7);
        this.layout.horizontalAlignRight(this.closeBtn, 3, this.winBg);
        this.layout.verticalAlignTop(this.closeBtn, 2, this.winBg);
        this.addChild(this.closeBtn);
        this.closeBtn.on(C_PointerEvt.DOWN, this.close, this);
    }


    fadein()
    {
        TweenMax.from(this, .15, {alpha:0});
        TweenMax.from(this.scale, .3, {x:0, y:0, ease:Back.easeOut});
    }


    removePivot()
    {
        if (this.bgShade)
        {
            this.bgShade.x += this.layout.relWidth(50);
            this.bgShade.y += this.layout.relHeight(50);
        }

        if (this.container)
        {
            this.container.x += this.layout.relWidth(50);
            this.container.y += this.layout.relHeight(50);
        }

        if (this.winBg)
        {
            this.winBg.x += this.layout.relWidth(50);
            this.winBg.y += this.layout.relHeight(50);
        }

        if (this.closeBtn)
        {
            this.closeBtn.x += this.layout.relWidth(50);
            this.closeBtn.y += this.layout.relHeight(50);
        }
    }


    close()
    {
        this.app.stage.removeListener(C_PointerEvt.DOWN, this.close, this);
        TweenMax.to(this, .2, {alpha:0, onComplete:this.removeFromParent, onCompleteScope:this, onCompleteParams:[]});
    }


}


export default BaseWindow