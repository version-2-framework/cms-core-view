import {BaseMobileGameButton} from "./BaseMobileGameButton";
import Button from "../ui/Button";
import C_PointerEvt from "../const/C_PointerEvt";
import C_GameEvent from "../../../cms-core-logic/src/const/C_GameEvent";

/**
 * Simple, default implementation of the Mobile Autoplay Button
 */
class AutoplayButtonMobile extends BaseMobileGameButton
{
    constructor()
    {
        super();
        
        /**
         * @private
         * @type {boolean}
         */
        this._autoplayInProgress = false;

        /**
         * @private
         * @type {boolean}
         */
        this._guiButtonsEnabled = false;

        /**
         * Button that will open the Autoplay menu. Shown as long as no Autoplay session is in progress.
         * @private
         */
        this._autoplayMenuBtn = new Button('autoBtnMobile', 'autoBtnMobileOver', 'autoBtnMobileOver', 'autoBtnMobileDisabled');
        this._autoplayMenuBtn.on(C_PointerEvt.DOWN, this.onOpenAutoplayMenuPressed, this);
        this.addChild(this._autoplayMenuBtn);

        /**
         * Button that will end the current Autoplay session. Shown when an Autoplay session is in progress.
         * @private
         */
        this._stopAutoplayBtn = new Button('stopAutoBtnMobile', 'stopAutoBtnMobileOver', 'stopAutoBtnMobileOver', 'stopAutoBtnMobileDisabled');
        this._stopAutoplayBtn.x = this._autoplayMenuBtn.x;
        this._stopAutoplayBtn.y = this._autoplayMenuBtn.y;
        this._stopAutoplayBtn.on(C_PointerEvt.DOWN, this.onStopAutoplayPressed, this);
        this.layout.propHeightScale(this._stopAutoplayBtn, 100, this._autoplayMenuBtn);
        this.addChild(this._stopAutoplayBtn);

        let buttonSoundId = this.app.config.uiSounds.mobile.autoplayButtonSound;
        if (buttonSoundId)
        {
            let buttonVolume = this.app.config.uiSounds.mobile.autoplayButtonVolume;

            this.log.debug(`AutoplayButtonMobile: setting button sound to ${buttonSoundId}`);

            this._autoplayMenuBtn.setClickSound(buttonSoundId, buttonVolume);
            this._stopAutoplayBtn.setClickSound(buttonSoundId, buttonVolume);
        }
        else
        {
            this.log.debug('AutoplayButtonMobile: no button sound available, button will not make sound when pressed');

            this._autoplayMenuBtn.setClickSound(null);
            this._stopAutoplayBtn.setClickSound(null);
        }

        this.dispatcher.on(C_GameEvent.AUTOPLAY_STARTED, this.onAutoplayStarted, this);
        this.dispatcher.on(C_GameEvent.AUTOPLAY_ENDED, this.onAutoplayEnded, this);

        // If we are showing the "stop" button, then we do not want to disable the autoplay
        // button only if it is currently in "autoplay" state (and not "stop" state)
        this.dispatcher.on(C_GameEvent.ENABLE_GUI_BUTTONS, this.onGuiButtonsEnabled, this);
        this.dispatcher.on(C_GameEvent.DISABLE_GUI_BUTTONS, this.onGuiButtonsDisabled, this);

        this.updateVisualState();
    };


    /**
     * @private
     */
    onAutoplayStarted()
    {
        this._autoplayInProgress = true;
        this.updateVisualState();
    };


    /**
     * @private
     */
    onAutoplayEnded()
    {
        this._autoplayInProgress = false;
        this.updateVisualState();
    };


    /**
     * @private
     */
    onGuiButtonsEnabled()
    {
        this._guiButtonsEnabled = true;
        this.updateVisualState();
    };


    /**
     * @private
     */
    onGuiButtonsDisabled()
    {
        this._guiButtonsEnabled = false;
        this.updateVisualState();
    };


    /**
     * @private
     */
    onOpenAutoplayMenuPressed()
    {
        this.dispatcher.emit(C_GameEvent.BUTTON_TOGGLE_AUTOPLAY_MENU_PRESSED);
    };


    /**
     * @private
     */
    onStopAutoplayPressed()
    {
        this.dispatcher.emit(C_GameEvent.BUTTON_STOP_AUTOPLAY_PRESSED);
    };


    /**
     * @private
     */
    updateVisualState()
    {
        this.showCorrectButtonMode();

        if (this._autoplayInProgress) {
            this.setEnabled(true);
        }

        if (this._autoplayInProgress === false) {
            this.setEnabled(this._guiButtonsEnabled);
        }
    };


    /**
     * Selects the most appropriate button mode to show.
     * @private
     */
    showCorrectButtonMode()
    {
        this._autoplayMenuBtn.visible = this._autoplayInProgress === false;
        this._autoplayMenuBtn.setEnabled(this._autoplayInProgress === false);

        this._stopAutoplayBtn.visible = this._autoplayInProgress;
        this._stopAutoplayBtn.setEnabled(this._autoplayInProgress);
    };
}

export default AutoplayButtonMobile;