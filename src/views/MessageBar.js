import BaseView from "./BaseView";
import C_GameEvent from "../../../cms-core-logic/src/const/C_GameEvent";

/**
 * Maximum width of the whole textfield, owing to texture limits.
 */
const MAX_TEXT_TEXTURE_WIDTH = 2048;


/**
 * A MessageBar component, which shows a message for the game.
 * When FreeSpins or Autoplay are in progress, the MessageBar tracks this info, and displays
 * special stats messages for these stats. Otherwise, it falls back to the last message that
 * it was explicitly told to show.
 */
class MessageBar extends BaseView
{
    constructor()
    {
        super();

        this.name = "MessageBar";
        
        // TODO: This is a cached message - we often override it when showing special
        // states (eg: autoplay, freespins, etc). Document this a bit better.
        /**
         * Current message to be shown.
         * @private
         * @type {TextConfig}
         */
        this._msgData = null;
    }


    // TODO: Apparently this is public, but i don't understand why this component
    // cannot just create itselt when the constructor is called.
    /**
     * @public
     */
    create()
    {
        //this.createAreaIndicator();

        this.createBackground();
        this.createTextField();
    };


    /**
     * Creates a highlighted area, which indicates the total designated area of the Message Bar.
     * Usable in debug modes, when we want to see how our GUI components are laying out.
     * @private
     */
    createAreaIndicator()
    {
        let layout = this.getActiveGuiLayout().messageBar;
        
        let width = this.layout.relWidth(layout.width);
        let height = this.layout.relHeight(layout.height);
        let posX = this.layout.relWidth(50 + layout.centerOffset) - (width * 0.5);
        let posY = this.layout.relHeight(100 - layout.bottomOffset) - height;

        let areaGraphic = new PIXI.Graphics();
        areaGraphic.beginFill(0xFFFF00, 0.5);
        areaGraphic.drawRect(posX, posY, width, height);
        areaGraphic.endFill();

        this.addChild(areaGraphic);
    };


    /**
     * Creates any backrgound element required for the message bar.
     * @private
     */
    createBackground()
    {
        let style = this.config.uiStyle.messageBar;
        let layout = this.getActiveGuiLayout().messageBar;

        if (style.useBackground)
        {
            let bgConfig =
            {
                w: layout.width,
                h: layout.height,
                bgColor : style.bgColor,
                bgAlpha : style.bgAlpha,
                lineThickness : style.lineThickness,
                lineColor : style.lineColor,
                lineAlpha : style.lineAlpha
            };
            
            /**
             * The background to the Message Bar (may be not always be created).
             * @private
             * @type {PIXI.Graphics}
             */
            this._bg = this.gameFactory.getRoundedRect(bgConfig);

            this.layout.horizontalAlignCenter(this._bg, layout.centerOffset);
            this.layout.verticalAlignBottom(this._bg, layout.bottomOffset);

            this.addChild(this._bg);
        }
    };


    /**
     * Creates the textfield for the message bar, and adds it to the display list.
     * @private
     */
    createTextField()
    {
        let layoutData = this.getActiveGuiLayout().messageBar;

        /** @type {GuiMessageBarStyle} */
        let messageBarConfig = this.config.uiStyle.messageBar;
        let showingBackground = messageBarConfig.useBackground;
        let proportionalTextHeight = showingBackground? layoutData.height * 0.8 : layoutData.height;
        let proportionalTextWidth  = showingBackground? layoutData.width  * 0.9 : layoutData.width;

        /**
         * Maximum width (in pixels) that the message should consume on-screen.
         * @private
         * @type {number}
         */
        this._maxProportionalTextWidth = proportionalTextWidth;

        /**
         * Maximum height (in pixels) that the message should consume on-screen.
         * @private
         * @type {number}
         */
        this._maxProportionalTextHeight = proportionalTextHeight;

        // On mobile devices with large resolution screens, we need to limit the potential
        // total resolution of the messagebar - because it could exceed the texture limit
        // for the device!! There is no simple way to achieve this with PIXI's standard
        // Text Renderer, and at the moment, we don't want to be writing a new text renderer
        // for this purpose, so we impose a few hacks: these hacks cannot explicitly guarantee
        // that we will not try and create a messageBar textfield texture that is too large,
        // but combined with sensible text strings for the messagebar (eg: nothing that is too
        // monstrously huge), word-wrapping (at a width that, within the local pixel space of
        // the messagebar, is very wide), we can reach the point where texture too large is
        // unlikely to happen. The main effect is to pick a scaling factor: on very low res
        // devices, we can allow the text to be high resolution. On larger resolution devices,
        // we severely limit its resolution.
        // The current fix is inelegant, and may break in future conditions - basically, if
        // the game area has a high resolution, then we use "half size" text on the font,
        // and hope this will be enough ! In addition, because we use wordWrap, the font will
        // not overflow (although it will become un-renderable)
        let fontSizeScalingFactor = this.platform.isDesktop()? 1 : 0.25;

        let textScaleFactor = messageBarConfig.textScaleFactor || 1;

        /**
         * The target scale for the textfield. When repositioning and resizing the textfield after
         * the text has changed, we first set it to this scale (which should produce "unity" size).
         * If its then too large for the available width, we can shrink it down
         * @private
         * @type {number}
         */
        this._targetTextScale = 1 / fontSizeScalingFactor;

        // TODO: Change to calculating stroke size as a proportion of font size (eg: stroke size
        // is always relative, rather than absolute, which it currently is - which is a bit weird)
        let fontSize = this.layout.relHeight(proportionalTextHeight * textScaleFactor) * fontSizeScalingFactor;

        /**
         * @type {PIXI.TextStyle}
         */
        let textStyle =
        {
            align : 'center',
            fontFamily : messageBarConfig.font,
            fontWeight : messageBarConfig.fontWeight,
            fontSize : fontSize,
            fill : messageBarConfig.fontColor,
            stroke : messageBarConfig.strokeColor,
            strokeThickness : this.layout.relHeight(messageBarConfig.strokeSize) * fontSizeScalingFactor,
            miterLimit : messageBarConfig.miterLimit,
            wordWrap : true,
            wordWrapWidth : MAX_TEXT_TEXTURE_WIDTH //Math.min(this.layout.relWidth(proportionalTextWidth), MAX_TEXT_WIDTH)
        }

        /**
         * The textfield used to show the message to the player.
         * @private
         * @type {PIXI.Text}
         */
        this._textField = new PIXI.Text(" ", textStyle);
        this._textField.name = "message";
        this._textField.anchor.set(.5);
        this.addChild(this._textField);

        

        this.updateTextSizeAndPosition();

        this.dispatcher.on(C_GameEvent.SET_MESSAGE, this.setMessage, this);
        this.dispatcher.on(C_GameEvent.CLEAR_MESSAGE, this.clearMessage, this);
    };


    /**
     * @private
     * @param {TextConfig} msgData 
     */
    setMessage(msgData)
    {
        this._msgData = msgData; // TODO: choose a better local var name..
        this.updateMessageShown();
    };


    /**
     * Clears the MessageBar of all text.
     * @private
     */
    clearMessage()
    {
        this._msgData = null;
        this.updateMessageShown();
    };


    /**
     * @private
     * @return {string}
     */
    getCurrMessageText()
    {
        let newMessage;

        if (this._msgData)
        {
            let localizationId = this._msgData.localizationId;
            let localizationMods = this._msgData.localizationMods;

            newMessage = this.locale.getString(localizationId, localizationMods);
        }
        else
        {
            newMessage = " ";
        }

        return newMessage;
    };


    /**
     * @private
     */
    updateMessageShown()
    {
        let newMessage = this.getCurrMessageText();
        this._textField.text = newMessage;
        this.updateTextSizeAndPosition();
        this.log.debug(`MessageBar: set text to ${newMessage}`);
    };


    /**
     * @private
     */
    updateTextSizeAndPosition()
    {
        let layoutData = this.getActiveGuiLayout().messageBar;
        let maxPropTextWidth  = this._maxProportionalTextWidth;
        let maxPropTextHeight = this._maxProportionalTextHeight;
        let maxTextWidth = this.layout.relWidth(maxPropTextWidth);
        let maxTextHeight = this.layout.relHeight(maxPropTextHeight);

        let rawTextWidth = this._textField.width / this._textField.scale.x;
        let rawTextHeight = this._textField.height / this._textField.scale.y;

        this._textField.scale.set(this._targetTextScale);
        
        this._textField.x = this.layout.relWidth(50 + layoutData.centerOffset);
        this._textField.y = this.layout.relHeight(100 - layoutData.bottomOffset - (layoutData.height / 2));

        // Get real width and height of component.
        this.log.debug(`MessageBar.updateTextSizeAndPosition: rawWidth:${rawTextWidth},currHeight:${rawTextHeight})`);
        this.log.debug(`MessageBar.updateTextSizeAndPosition: currWidth:${this._textField.width},currHeight:${this._textField.height})`);
        this.log.debug(`MessageBar.updateTextSizeAndPosition: maxWidth:${maxTextWidth}, maxHeight:${maxTextHeight}`);
        
        if (this._textField.width > maxTextWidth) {
            this.log.debug(`textField.width = ${this._textField.width}, which is larger than ${maxTextWidth}`);
            this.layout.propWidthScale(this._textField, maxPropTextWidth);
        }

        /*
        if (this._textField.height > maxTextHeight) {
            this.log.debug(`textField.height = ${this._textField.height}, which is larger than ${maxTextHeight}`);
            this.layout.propHeightScale(this._textField, maxPropTextHeight);
        }
        */
        
        this.log.debug(`MessageBar.updateTextSizeAndPosition: new dimensions = {width:${this._textField.width}, height:${this._textField.height}}`);
    };
}

export default MessageBar