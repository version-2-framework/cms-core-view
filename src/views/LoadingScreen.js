import BaseView from "./BaseView";
import C_PointerEvt from "../const/C_PointerEvt";
import C_GameEvent from "../../../cms-core-logic/src/const/C_GameEvent";
import C_ViewEvent from "../../../cms-core-view/src/const/C_ViewEvent";
import { LoadingScreenViewEvent, SGDigitalLoadingScreenViewModel } from "../../../cms-core-logic/src/controllers/LoadingScreenViewModel";
import { LoadingScreenViewModel } from "../../../cms-core-logic/src/controllers/LoadingScreenViewModel";
import { Gen1LoadingScreenViewModel } from "../../../cms-core-logic/src/controllers/LoadingScreenViewModel";
import { Gen2LoadingScreenViewModel } from "../../../cms-core-logic/src/controllers/LoadingScreenViewModel";
import Button from "../ui/Button";
import DefaultLoadingRoutine from "../integration/DefaultLoadingRoutine";
import * as C_ClientEnvironmentValue from "../../../cms-core-logic/src/const/C_ClientEnvironmentValues"; 

/**
 * Proportion of screen height reserved for the Game Mode buttons.
 */
const MODE_BUTTON_REL_HEIGHT = 15;

/**
 * Relative position of the vertical center of active actions (eg: "game mode buttons", messages to
 * indicate that some transitive action is in progress)
 */
const ACTIVE_ACTION_REL_POS_Y = 80;

/**
 * Relative height that content in the "action" area should be.
 */
const ACTIVE_ACTION_REL_HEIGHT = 10;

/**
 * Displayed while the game is loading.
 * Can be customized per game.
 */
class LoadingScreen extends BaseView
{
    constructor()
    {
        super();

        /**
         * @private
         * @type {PIXI.Loader}
         */
        this.pixiLoader = this.app.bootLoader;

        /**
         * @private
         */
        this.loadingScreenModel = this.createLoadingScreenViewModel(this.app);
        
        // TODO: This should probably get a more generic name
        /**
         * @private
         * @type {PIXI.DisplayObject[]}
         */
        this.responsibleGamingLogos = [];
        
        // Setup standard text style for messages
        /** @type {GuiLoadingScreenTextStyle} */
        let textStyleConfig = this.app.config.loadingScreenTextStyle;
        
        /**
         * Text style for the loading screen messages.
         * @private
         * @type {Partial<PIXI.TextStyle>}
         */
        this.txtStyleMsg =
        {
            fontFamily : this.config.gameSpecificFont,
            fontWeight: textStyleConfig.fontWeight,
            fontSize: this.layout.relHeight(10),
            fill: textStyleConfig.fill,
            strokeThickness : this.layout.relHeight(textStyleConfig.strokeSize),
            stroke : textStyleConfig.strokeColor
        };

        this.addListener(this._dispatcher, LoadingScreenViewEvent.ENTER_SELECT_SERVER_URL, this.showUrlSelectionOverlay, this);
        this.addListener(this._dispatcher, LoadingScreenViewEvent.EXIT_SELECT_SERVER_URL, this.hideUrlSelection, this);
        this.addListener(this._dispatcher, LoadingScreenViewEvent.ENTER_ADVERT_STATE, this.enterAdvert, this);
        this.addListener(this._dispatcher, LoadingScreenViewEvent.EXIT_ADVERT_STATE, this.exitAdvert, this);
        this.addListener(this._dispatcher, LoadingScreenViewEvent.ENTER_SELECT_GAME_MODE, this.enterSelectGameMode, this);
        this.addListener(this._dispatcher, LoadingScreenViewEvent.EXIT_SELECT_GAME_MODE, this.exitSelectGameMode, this);
        this.addListener(this._dispatcher, LoadingScreenViewEvent.ENTER_STARTING_SESSION, this.showStartingGameMsg, this);
        this.addListener(this._dispatcher, LoadingScreenViewEvent.EXIT_STARTING_SESSION, this.hideStartingGameMsg, this);
        this.addListener(this._dispatcher, LoadingScreenViewEvent.ENTER_QUICK_LOAD_MODE,this.enterQuickLoadScreen, this);
        this.addListener(this._dispatcher, C_GameEvent.HIDE_LOADING_SCREEN, this.closeLoadingScreen, this);
    };


    /**
     * Creates the required intsance of Loading Screen View Model. This could be done through the normal
     * Game Factory method, however: the version loaded will never change from game to game, and Loading
     * Screen View already has a strong dependency on imports from the LoadingScreenViewModel module.
     * There are a limited, permanent set of View Models available: the one picked is based on the target
     * chosen (eg: V1 platform, V2 platform, and possibly some other use cases). Adding this into the
     * GameFactory class, gives the impression we might want to change this on a per game basis, and we
     * do not (the list of possible implementations to pick, and the way that we pick them, will be
     * standard across all game clients).
     * @private
     * @param {Dependencies} dependencies 
     * @return {LoadingScreenViewModel}
     */
    createLoadingScreenViewModel(dependencies) {
        let viewModel;
        if (BUILD_TARGET === "v1") {
            viewModel = new Gen1LoadingScreenViewModel(dependencies)
        }
        else
        if (BUILD_TARGET === "v2") {
            if (BUILD_PLATFORM === "sgdigital") {
                viewModel = new SGDigitalLoadingScreenViewModel(dependencies);
            }
            else viewModel = new Gen2LoadingScreenViewModel(dependencies);
        }
        return viewModel;
    };


    /**
     * Creates a new sprite, using a preloader texture
     * @protected
     * @param {string} textureId
     * @return {PIXI.Sprite}
     */
    createPreloaderSprite(textureId)
    {
        return new PIXI.Sprite(this.getPreloaderTexture(textureId));
    }


    /**
     * Fetches a Texture from the Preloader spritesheet.
     * @protected
     * @param {string} textureId 
     * @return {PIXI.Texture}
     */
    getPreloaderTexture(textureId)
    {
        return this.pixiLoader.resources.preloader.textures[textureId];
    };


    // TODO: We may want to make this method name more abstract in the future: "responsible
    // gaming" is an Italian specific name. The fact that we may want to show a bunch of
    // similar assets for other countries, however, is a useful feature.
    /**
     * Fetches a Texture from the responsible gaming spritesheet.
     * @private
     * @param {string} textureId 
     * @return {PIXI.Texture}
     */
    getResponsibleGamingTexture(textureId) {
        let licenseeAssets = this.pixiLoader.resources.licenseeAssets;
        let countryAssets = this.pixiLoader.resources.countryAssets;

        let licenseeTextures = licenseeAssets && licenseeAssets.textures? licenseeAssets.textures : [];
        let countryTextures = countryAssets && countryAssets.textures? countryAssets.textures : [];

        let texture = countryTextures[textureId] || licenseeTextures[textureId];

        if (!texture) {
            this.log.warn(`LoadingScreenView.getResponsibleGamingTexture: no texture with id ${textureId} available`);
        }

        return texture;
    };


    /**
     * @todo: Why is this public? And what does it do, that the constructor could not do ?
     * @public
     */
    create()
    {
        this.createBg();
        this.createLoadBar();
        this.createWmgLogo();
        this.createContent();
        
        // TODO: This is absolutely optional
        this.createResponsibleGamingLogos(); // 

        // Begin loading assets.
        // TODO: Consider if we want to use LoadingRoutine abstractly, or if it exists solely
        // because we need to reuse that code in more than one place.
        let loadingRoutine = new DefaultLoadingRoutine(this.app);
        loadingRoutine.loadAssets(
            progress => this.handleProgress(progress),
            () => this.preloadComplete(),
            () => this.handleLoadFailure());
    };


    /**
     * @private
     * @param {number} progress
     * A value in the range of 0 to 100 inclusive.
     */
    handleProgress(progress)
    {
        this.log.debug(`Loading... ${progress.toFixed(0)}%`);
        this.dispatcher.emit(C_GameEvent.LOAD_PROGRESS, progress);
        this.setProgress(progress);
    };


    /**
     * Action invoked when loading main game assets has failed. This is a tricky scenario,
     * because we may be missing assets required for game localization, or to draw anything
     * on-screen (although if we have a dynamically drawn dialog type available, then we
     * should be ok to access it somehow).
     * @private
     */
    handleLoadFailure()
    {
        this.log.error(`Loading assets has failed`);

        // TODO: Handle failure better than this, though for now, this is ok.

        window.alert(`Failed to load all game assets`);
    };


    // TODO: Document this method properly
    /**
     * @private
     * @private {number}
     * A value in the range of 0 to 100 inclusive.
     */
    setProgress(progress)
    {
        // TODO: There are some magic numbers in the constants here - and i don't like them,
        // because i don't what they are referring to. This needs tidying up.
        let barWidthScaleFactor = 0.07 + 0.86 * (progress / 100);
        
        TweenMax.to(this._loadbar, 0.4, {width: this._blackBar.width * barWidthScaleFactor, ease: Power4.easeIn, overwrite: 1});
    };


    // TODO: Document this method better, I am only 50% sure of what it is really doing right now
    /**
     * @private
     */
    preloadComplete()
    {
        TweenMax.to(
            this._loadbar,
            0.4,
            {
                width: this._blackBar.width,
                ease: Power4.easeIn,
                overwrite: 1,
                onComplete: () => this.loadingScreenModel.actionOnAssetsLoaded()
            });

        this.app.assets.onResourcesLoaded();
    };


    /**
     * Creates the main background for the loading screen.
     * @private
     */
    createBg()
    {
        this.bg = new PIXI.Sprite(this.getPreloaderTexture("bg"));
        this.layout.propHeightScale(this.bg, 100);
        this.addChild(this.bg);
    };


    /**
     * Creates a graphic showing safe content area (useful for debugging, when you are creating
     * custom content for a custom loading screen). Only call this when running in debug mode,
     * get rid of it for you release builds!
     * @protected
     */
    drawSafeContentArea()
    {
        let safeArea = new PIXI.Graphics();
        safeArea.beginFill(0x000000, 0.25);
        safeArea.drawRect(0, 0, this.layout.relWidth(100), this.layout.relHeight(70));
        safeArea.endFill();

        this.addChildAt(safeArea, 1);
    }


    /**
     * Creates the primary content for the Loading Screen. Override this method in an extended
     * class, if you want to show something customized on the loading screen. Note, that the
     * Loading Screen View needs to show other messages and states at certain points, so cannot
     * guarantee to show your customized content for the entire duration of the Loading Screen's
     * lifetime.
     * @protected
     */
    createContent()
    {
        this.createDefaultContent();
    };


    // TODO : Generate better guidelines on what this method should do, and better info about
    // when it gets called.
    /**
     * Clears the primary content of the Loading Screen view. If you created custom content in an
     * extended Loading Screen View, then use this method to implement a short animated transition,
     * which will clear it away.
     * @protected
     */
    clearContent()
    {
        this.clearDefaultContent();
    }


    /**
     * Creates the standard set of content for the Loading Screen - Game Logo & Customizable
     * Hints data.
     * @private
     */
    createDefaultContent()
    {
        this.log.debug('LoadingScreenView.createDefaultContent()');

        this.createGameLogo();
        this.createHints();
    };


    // TODO: Implement this method, to animate away the default loading screen view content.
    /**
     * @private
     */
    clearDefaultContent()
    {

    }


    // TODO: Nope, this needs refactoring for sure. THe name is wrong, for a start.
    /**
     * @private
     */
    createGameLogo()
    {
        let logoTextureId = this.config.loadingScreenGameLogo;

        this.log.debug(`LoadingScreenView.createGameLogo(texture:${logoTextureId})`);

        let logo = new PIXI.Sprite(this.getPreloaderTexture(logoTextureId));
        this.addChild(logo);
        this.layout.propHeightScale(logo, this.config.preloaderLogoHeight);
        this.layout.horizontalAlignCenter(logo, 0, this.bg);
        this.layout.verticalAlignTop(logo, 5, this.bg);
        TweenMax.from(logo, .8, {
            alpha: 0, y: logo.y - this.layout.relHeight(23, this.bg),
            ease: Cubic.easeOut
        });
    };


    // TODO: NOPE, not a good approach. The name is wrong (it creates, and then performs
    // a very specific animation on, the game logo). We cannot be implementing this in
    // this manner.
    /**
     * Creates - and shows appearing - a WMG logo, positioned inside the "create game type"
     * text area (this should only be seen, before we show select game type buttons)
     * @private
     */
    createWmgLogo()
    {
        /**
         * WMG logo, shown in the "select game type" area, 
         * @private
         * @type {PIXI.Sprite}
         */
        this._wmgLogo = new PIXI.Sprite(this.getPreloaderTexture("logo_wmg_white"));
        this._wmgLogo.anchor.set(0.5, 0.5);
        this._wmgLogo.x = this.layout.relWidth(50);
        this._wmgLogo.y = this.layout.relHeight(ACTIVE_ACTION_REL_POS_Y);
        this.layout.propHeightScale(this._wmgLogo, ACTIVE_ACTION_REL_HEIGHT);

        this.addChild(this._wmgLogo);

        let finalWidth = this._wmgLogo.width;
        let finalHeight = this._wmgLogo.height;
        let startWidth = finalWidth * 1.2;
        let startHeight = finalHeight * 1.2; 

        TweenMax.fromTo(
            this._wmgLogo,
            0.5,
            { alpha:0, width:startWidth, height:startHeight },
            { alpha:1, width:finalWidth, height:finalHeight });
    };


    // TODO: Tidy up the naming of the loading bar components, and give them some documentation
    /**
     * Creates the loading bar graphical component.
     * @private
     */
    createLoadBar()
    {
        /**
         * @private
         * @type {PIXI.Graphics}
         */
        this._blackBar = new PIXI.Graphics();
        this._blackBar.beginFill(0, .7);
        this._blackBar.drawRect(0, 0, this.layout.relWidth(100), this.layout.relHeight(10, this.bg));
        this._blackBar.endFill();
        this._blackBar.cacheAsBitmap = true;
        this.layout.verticalAlignBottom(this._blackBar, 0, this.bg);
        this.addChild(this._blackBar);

        let loadBarBg = new PIXI.Graphics();
        loadBarBg.beginFill(0x7d7a7a, .8);
        loadBarBg.drawRect(0, 0, this._blackBar.width, this.layout.relHeight(6, this._blackBar));
        loadBarBg.endFill();
        loadBarBg.cacheAsBitmap = true;
        loadBarBg.y = this._blackBar.y - loadBarBg.height;
        this.addChild(loadBarBg);

        /**
         * @private
         * @type {PIXI.Graphics}
         */
        this._loadbar = new PIXI.Graphics();
        this._loadbar.beginFill(0x00fff6, 1);
        this._loadbar.drawRect(0, 0, loadBarBg.width, this.layout.relHeight(100, loadBarBg));
        this._loadbar.endFill();
        this._loadbar.cacheAsBitmap = true;
        this._loadbar.x = loadBarBg.x;
        this._loadbar.y = loadBarBg.y;
        this._loadbar.width = 10;
        this.addChild(this._loadbar);

        let gameVersionText = this.locale.getString("T_splash_clientInfo", {
            "[GAME_VERSION]" : this.buildInfo.getBuildVersion(),
            "[GAME_STATIC_VERSION]":this.buildInfo.getGameStaticVersion()
        });

        let gameVersionTextField = new PIXI.Text(gameVersionText, {
            fontFamily : "Arial",
            fontSize : Math.round(this.layout.relHeight(2)),
            fontWeight : "normal",
            fill : 0xFFFFFF,
            stroke:0x000000,
            strokeThickness: Math.round(this.layout.relHeight(1)),
            lineJoin:"miter",
            miterLimit:2
        });
        gameVersionTextField.anchor.set(0.5, 0.5);
        gameVersionTextField.x = this.layout.relWidth(50);
        //gameVersionTextField.y = this.layout.relHeight(98);
        gameVersionTextField.y = this._loadbar.y - this.layout.relHeight(2);
        this.addChild(gameVersionTextField);
    };


    // TODO: For future, non-italian use cases, this functionality will probably want
    // renaming or modifying, so it describes something a bit more generic.
    /**
     * Creates the set of Responsible Gaming logos to show.
     * @private
     */
    createResponsibleGamingLogos()
    {
        let logoConfigs = this.loadingScreenModel.getResponsibleGamingLinks();
        let logoContainer = new PIXI.Container();

        this.log.debug(`LoadingScreenView: creating ResponsibleGamingLogos: ${JSON.stringify(logoConfigs)}`);

        this.addChild(logoContainer);
        let logoSpacing = this.layout.relWidth(2, this._blackBar);

        for (let logoIdx = 0; logoIdx < logoConfigs.length; logoIdx++)
        {
            let logoConfig = logoConfigs[logoIdx];
            let logoTextureId = logoConfig.buttonTexture;
            let logoOpenLinkAction = logoConfig.action;
            
            let logo = new PIXI.Sprite(this.getResponsibleGamingTexture(logoTextureId));
            this.layout.propHeightScale(logo, 75, this._blackBar);
            logoContainer.addChild(logo);

            if (logoIdx > 0)
            {
                logo.x = logoContainer.children[logoIdx - 1].x + logoContainer.children[logoIdx - 1].width + logoSpacing;
            }

            if (logoOpenLinkAction) {
                logo.interactive = true;
                logo.buttonMode = true;

                // Must be on UP for iOS to work for opening external links
                logo.on(C_PointerEvt.UP, () => {
                    this._openLinkUtil.openLink(logoOpenLinkAction);
                });
            }

            TweenMax.from(logo, .8, {delay: 0.3 * logoIdx, alpha: 0, ease: "cubic.out"});
        }

        this.layout.horizontalAlignCenter(logoContainer, 0, this._blackBar);
        this.layout.verticalAlignMiddle(logoContainer, 0, this._blackBar);
    };


    // TODO: This should be "showGameInfoItems" - the name "hints" is poor
    /**
     * Creates the standard hint graphic components.
     * @private
     */
    createHints()
    {
        /** @type {LoadingScreenHintConfig[]} */
        let hintConfigs = this.app.config.loadingScreenHintConfigs;
        if (!hintConfigs) {
            return;
        }
        
        let hintsContainer = new PIXI.Container();
        hintsContainer.name = 'hintsContainer';

        this.addChild(hintsContainer);

        /**
         * The container for the hints graphics.
         * @private
         */
        this._hintsContainer = hintsContainer;

        /**
         * Tracks whether the hints container is currently shown (or whether we have attempted to hide it)
         * @private
         * @type {boolean}
         */
        this._hintsAreShown = true;

        let gameSpecificFont = this.config.gameSpecificFont;

        hintConfigs.forEach((hintConfig, hintIndex) =>
        {
            let hint = new PIXI.Container();

            let textStyle =
            {
                dropShadow: hintConfig.dropShadow,
                dropShadowAlpha: hintConfig.dropShadowAlpha,
                dropShadowColor: hintConfig.dropShadowColor || 0x000000,
                wordWrap: true,
                breakWords: true
            };

            let hintImg = new PIXI.Sprite(this.getPreloaderTexture(`img_${hintIndex + 1}`));
            hintImg.anchor.set(.5, .5);
            this.layout.propHeightScale(hintImg, hintConfig.img.height, this.bg);
            this.layout.horizontalAlignLeft(hintImg, hintConfig.img.x, this.bg);
            this.layout.verticalAlignTop(hintImg, hintConfig.img.y, this.bg);
            hint.addChild(hintImg);
            TweenMax.from(hintImg.scale, .4, {x: 0, y: 0, delay: .4 + hintIndex * .5, ease: "back.out"});
            TweenMax.from(hintImg, .25, {alpha: 0, delay: .5 + hintIndex * .5});

            let text = this.locale.getString(`preloader_info_${hintIndex + 1}`);
            let hintTF = this.assets.getTF(text, this.layout.relHeight(hintConfig.text.size, this.bg),
                gameSpecificFont, hintConfig.text.colors, null, this.layout.relWidth(hintConfig.text.wrapWidth), textStyle);
            hintTF.anchor.set(.5, 0);
            this.assets.addTFStroke(hintTF, hintConfig.text.strokeData);
            this.layout.horizontalAlignLeft(hintTF, hintConfig.text.x, this.bg);
            this.layout.verticalAlignTop(hintTF, hintConfig.text.y, this.bg);


            hint.addChild(hintTF);

            hintsContainer.addChild(hint);
            TweenMax.from(hintTF, .25, {alpha: 0, delay: .5 + hintIndex * .5});
        });
    };


    /**
     * Shows the url selection screen, as an overlay to the loading screen. This will only be invoked, on
     * internal builds (where we want a choice of urls to be available for testing with).
     * @private
     */
    showUrlSelectionOverlay() {
        this.log.debug('LoadingScreenView.showUrlSelectionOverlay()');

        this.urlSelectionView = this.gameFactory.getUrlSelectionScreen();
        this.addChild(this.urlSelectionView);
    };


    /**
     * Hides the url selection screen.
     * @private
     */
    hideUrlSelection() {
        this.log.debug('LoadingScreenView.hideUrlSelection()');

        if (this.urlSelectionView) {
            this.urlSelectionView.destroy();
            this.urlSelectionView = null;
        }
    };


    //--------------------------------------------------------------------------------------------------
    // Game Advert related functionality
    //--------------------------------------------------------------------------------------------------
    /**
     * @private
     */
    enterAdvert()
    {
        this.log.debug('LoadingScreenView.enterAdvert()');

        // TODO: Let's make the Advert appear, with a small animated transition.
        // 1) We need to determine the actual area that the advert will occupy. Adverts should
        //    probably be designed to be full screen - so i think, showing the advert as full
        //    screen is fine.
        // 2) We could do with more than 1 mechanism for pickint the advert
        //    - fetch from url (we should know this is being done, fetch it remotely, show some
        //      kind of spinner like in Kingdom Rush, then show the advert)
        //    - images loaded at runtime (we will not be hard coding this in game config, but we
        //      could have some kind of separate json config, with extra non-critical data, such
        //      as this).

        // NOTE: The advert functionality is not yet complete: this is only a sketch. The advertImage
        // texture will probably not be found, and in practise, we may need to download an image and
        // show it. It could even be simpler to show such a downloaded image in the html layer.

        let advertImage = this.assets.getSprite('advertImage');

        // We set the advert to be full-screen width, and anchor it to the screen center.
        // This means that we can perform a simple animated transition, where it grows out
        // from the screen center (if we want to).
        advertImage.anchor.set(0.5, 0.5);
        advertImage.x = this.layout.relWidth(50);
        advertImage.y = this.layout.relHeight(50);
        advertImage.width = this.layout.relWidth(100);
        advertImage.height = this.layout.relHeight(100);

        TweenMax.fromTo(
            advertImage,
            0.2,
            {
                alpha : 0
            },
            {
                alpha : 1
            }
        );
    };


    /**
     * @private
     */
    exitAdvert()
    {
        this.log.debug('LoadingScreenView.exitAdvert()');
    };


    //--------------------------------------------------------------------------------------------------
    // Select Game Mode related functionality
    //--------------------------------------------------------------------------------------------------
    /**
     * Shows the Session Type (Game Mode) selection buttons.
     * @private
     */
    enterSelectGameMode()
    {
        this.log.debug('LoadingScreenView.showGameModes()');

        if (this._model.isAndroid() && this.fullScreenPromptEnabled()) {
            this._dispatcher.emit(C_ViewEvent.CREATE_FULLSCREEN_PROMPT);
        }

        TweenMax.killTweensOf(this._loadbar);

        let startSessionButtonConfigs = this.loadingScreenModel.getStartSessionButtonConfigs();

        /**
         * Container, with Game Mode selection buttons.
         * @private
         */
        this.gameModeBtnContainer = new PIXI.Container();
        this.gameModeBtnContainer.name = 'btnContainer';

        let textStyle =
        {
            dropShadow: true,
            dropShadowAlpha: .5,
            dropShadowColor: '#000000',
        };
        
        // Calculate size and padding between buttons. padding is tricky - we want to ensure
        // all buttons fit in a single horizontal row. If we assign a screen width percentage
        // for padding, this could fail if we ever needed lots of buttons (although its highly
        // unlikely there will ever be more than 2 buttons here). So, for now, we assign a
        // percentage of the actual target max width for the button, as the padding between
        // each button horizontally.
        let numStartSessionButtons = startSessionButtonConfigs.length;
        let maxButtonWidth = this.layout.relWidth(96) / numStartSessionButtons; // leave a little edge padding
        let maxButtonHeight = this.layout.relHeight(12);
        let buttonStartX = this.layout.relWidth(2); // leave 2% edge padding at left and right
        let buttonWidth = maxButtonWidth * 0.9;
        let buttonEdgePad = maxButtonWidth * 0.05;

        let gameModeBtns = [];
        let gameModeBtnTargetScale = 1;
        for (let btnIdx = 0; btnIdx < startSessionButtonConfigs.length; btnIdx++)
        {
            let buttonText = this.locale.getString(startSessionButtonConfigs[btnIdx].localizationId);

            let gameModeBtn = this.assets.getTF(
                buttonText,
                this.layout.relHeight(MODE_BUTTON_REL_HEIGHT),
                this.config.gameSpecificFont,
                this.config.gameModeBtnColours, null, null, textStyle);

            this.assets.addTFStroke(gameModeBtn, this.config.gameModeBtnStrokeColour);
            
            gameModeBtn.anchor.set(0.5, 0.5);
            gameModeBtn.buttonMode = true;
            gameModeBtn.x = buttonStartX + ((btnIdx + 0.5) * maxButtonWidth);
            gameModeBtn.interactive = true;
            gameModeBtn.on(C_PointerEvt.OVER, ()=> {gameModeBtn.style.fill = this.config.gameModeBtnHoverColour;});
            gameModeBtn.on(C_PointerEvt.ROLLOUT, ()=>{gameModeBtn.style.fill = this.config.gameModeBtnColours});
            gameModeBtn.on(C_PointerEvt.DOWN, startSessionButtonConfigs[btnIdx].action, startSessionButtonConfigs[btnIdx]);
            this.gameModeBtnContainer.addChild(gameModeBtn);

            // if the text, when set to target height, is too wide for the available area,
            // then calculate the scale value that would be required to brign the text into
            // the required width. We then cache the smallest scale value : we will scale
            // ALL button texts to match this scale at the end (so we don't end up with any
            // that look weird because they have a different size to the others). If no
            // button goes wider than the target scale, then we can just leave them at the
            // intended scale (where their height fills the assigned height area)
            gameModeBtns.push(gameModeBtn);
            if (gameModeBtn.width > buttonWidth) {
                let scaleDifference = buttonWidth / gameModeBtn.width;
                gameModeBtnTargetScale = Math.min(gameModeBtnTargetScale, scaleDifference);
            }

            // If the selected scale would be make the text taller than it is allowed to be,
            // then update target scale to a value that also satisfies height requirements
            if ((gameModeBtn.height * gameModeBtnTargetScale) > maxButtonHeight) {
                let scaleDifference = maxButtonHeight / (gameModeBtn.height * gameModeBtnTargetScale);
                gameModeBtnTargetScale = Math.min(gameModeBtnTargetScale, scaleDifference);
            }
        }

        // Finally, make sure all buttons are scaled correctly
        gameModeBtns.forEach(gameModeBtn => {
            gameModeBtn.scale.set(gameModeBtnTargetScale);
        });

        this.addChild(this.gameModeBtnContainer);
        this.gameModeBtnContainer.y = this.layout.relHeight(ACTIVE_ACTION_REL_POS_Y);

        TweenMax.to(this._wmgLogo, .5, {alpha: 0});
        TweenMax.from(this.gameModeBtnContainer, .5, {delay:0.3, alpha: 0});
    };


    /**
     * Hides the Session Type (Game Mode) selection buttons.
     * @private
     */
    exitSelectGameMode()
    {
        this.log.debug('LoadingScreenView.hideGameModes()');

        // this.hideHintsContainer();

        if (this.gameModeBtnContainer) {
            let container = this.gameModeBtnContainer;
            container.interactive = false;
            this.gameModeBtnContainer = null;
            TweenMax.to(
                container,
                0.2,
                {
                    alpha:0,
                    onComplete : () => {
                        container.destroy();
                    }
                });
        }
    };

    //--------------------------------------------------------------------------------------------------
    // Display message for quick load screen
    //--------------------------------------------------------------------------------------------------
    enterQuickLoadScreen() {
        if (this._model.isAndroid() && this.fullScreenPromptEnabled()) {
            this._dispatcher.emit(C_ViewEvent.CREATE_FULLSCREEN_PROMPT);
        }

        let sessionTypeLabelConfig = this.loadingScreenModel.getSessionTypeText()[0];

       
        let sessionTypeLabelText = new PIXI.Text(this.locale.getString(sessionTypeLabelConfig.localizationId), this.txtStyleMsg);
        sessionTypeLabelText.anchor.set(0.5, 0.5);
        sessionTypeLabelText.x = this.layout.relWidth(50);
        sessionTypeLabelText.y = this.layout.relHeight(ACTIVE_ACTION_REL_POS_Y);
        sessionTypeLabelText.alpha = 0;

        this.addChild(sessionTypeLabelText);


        let timeline = new TimelineMax();
        timeline.to(this._wmgLogo,0.5,{alpha:0});
        timeline.from(sessionTypeLabelText,1,{alpha:1});
        timeline.call(()=>{
            sessionTypeLabelConfig.action();
        });
        

    }

    /**
     * Hides the hints container (if its visible or present)
     * @private
     */
    /*
    hideHintsContainer() {
        this.log.debug('LoadingScreenView.hideHintsContainer()');

        this._hintsAreShown = false;

        if (this._hintsContainer) {
            TweenMax.killTweensOf(this.hints)
            TweenMax.to(this._hintsContainer, .7, {alpha: 0});
        }
    };
    */

    /**
     * Shows the "starting game" message to the player.
     * @private
     */
    showStartingGameMsg()
    {
        // TODO: I think we could make this a bit neater? EG: an exit action, would
        // let us tidy up the flow of how this works, and make it less ugly when we
        // want to customize the behaviour !
        /*
        if (this._hintsAreShown) {
            this.hideHintsContainer();
        }
        */
        
        let startingGameMsg = this.locale.getString("T_splash_startingGame");

        this.log.debug(`LoadingScreenView.showStartingGameMsg(${startingGameMsg})`);

        /**
         * Text shown to the player, when we are attempting to start a game session.
         * This message simply 
         * @private
         * @type {PIXI.Text}
         */
        this._startingGameTxt = new PIXI.Text(startingGameMsg, this.txtStyleMsg);
        this._startingGameTxt.anchor.set(0.5, 0.5);
        this._startingGameTxt.x = this.layout.relWidth(50);
        this._startingGameTxt.y = this.layout.relHeight(ACTIVE_ACTION_REL_POS_Y);

        let startWidth = this._startingGameTxt.width;
        let startHeight = this._startingGameTxt.height;
        let finalWidth = startWidth * 1.02;
        let finalHeight = startHeight * 1.02;

        TweenMax.fromTo
        (
            this._startingGameTxt,
            0.5,
            { width:startWidth, height:startHeight },
            { width:finalWidth, height:finalHeight, yoyo:true, repeat:-1 }
        );
        
        this.addChild(this._startingGameTxt);
    };


    /**
     * Hides the "starting game" message.
     * @private
     */
    hideStartingGameMsg()
    {
        this.log.debug('LoadingScreenView.hideStartingGameMsg()');

        if (this._startingGameTxt) {
            TweenMax.killTweensOf(this._startingGameTxt);
            this._startingGameTxt.destroy();
            this._startingGameTxt = null;
        }
    };


    /**
     * Closes and destroys the loading screen view.
     * @private
     */
    closeLoadingScreen()
    {
        this.log.debug('LoadingScreenView.closeLoadingScreen()');
        this.destroy();
    }   

    /**
     * Returns a boolean value indicating whether or not the full-screen prompt has been enabled. This is based 
     * on the value set for the client environment parameter in the url.
     * @private
     * @return {boolean}
     */
    fullScreenPromptEnabled()
    {
        let clientEnvironmentValue = this.model.getClientEnvironmentValue();

        if (clientEnvironmentValue === C_ClientEnvironmentValue.MOBILE_APP)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}

export default LoadingScreen