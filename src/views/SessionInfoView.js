import BaseView from './BaseView';
import C_SessionType from '../../../cms-core-logic/src/const/C_SessionType';
import C_GameEvent from '../../../cms-core-logic/src/const/C_GameEvent';
import C_PointerEvt from '../const/C_PointerEvt';

// TODO: the mechanism used for sizing this component perhaps needs improving. Currently, this
// is passed a rectangle (determined by whichever view component is using it, eg: the UI), and
// it draws itself into that area. Although this could be written directly into the UI, it might
// get reused, it *definitely* will have expanding functionality.
class SessionInfoView extends BaseView
{
    /**
     * @param {SessionInfoSlotConfig} sessionInfoConfig
     * @param {PIXI.Rectangle} area
     * The area to be occupied by the SessionInfoView: it handles its own positioning and sizing
     * (the positioning should be static for its life-time). This rectangle is expected to be
     * defined in "percentage of screen space" values.
     * NOTE (Russell W, 23.07.2020): I am not quite sure why I wrote this component this way:
     * it definitely needs to know its maximum width and height, as it has to clamp itself to a
     * specific area. It wouldnt need to position itself, if we guaranteed that it was center
     * anchored (so any other graphics class using this would understand that this is a part of
     * its api). It can be tidied up at some point.
     */
    constructor(sessionInfoConfig, area)
    {
        super();

        /**
         * @private
         * @type {SessionInfoSlotConfig}
         */
        this._sessionInfoConfig = sessionInfoConfig;

        /**
         * @private
         * @type {PIXI.Rectangle}
         */
        this._area = area;

        /**
         * @private
         * @type {PIXI.DisplayObject}
         */
        this._activeContent = null;

        /**
         * Method to be called, whenever we need to update the active realplay session info text. We don't
         * know exactly how the update works (this is determined when the text is created) : we only know
         * that now is the right time to do it. Hence we cache the method that does the dirty work, into
         * this field.
         * @private
         * @type {() => void}
         */
        this._updateRealplayInfo = null;

        if (this._model.getSessionType() === C_SessionType.FREE_PLAY) {
            let logo = this.assets.getSprite(this.config.uiStyle.freeplayLogoTexture);
            this.addChild(logo);
            this._activeContent = logo;
            this.layout.fitAndCenterDisplayObjectInProportionalArea(this._activeContent, this._area);
        }
        else
        {
            if (this._model.isSessionInProgress()) {
                this.createRealplayView();
            }
            else this._updateRealplayInfo = () => {};

            // We never remove these event listeners, because this component is intended to permanently
            // exist (at the moment). This could get changed later, if we need to destroy / recreate
            // the GUI, which contains this component.
            this._dispatcher.on(C_GameEvent.SESSION_OPENED, this.createRealplayView, this);
            this._dispatcher.on(C_GameEvent.SESSION_CLOSED, this.destroyRealplayView, this);
        }
    }


    /**
     * Creates content for the Realplay view. Only invoked when a Session has been opened
     * (because we need to know the country, in order to know the data to show).
     * @private
     */
    createRealplayView()
    {
        this.log.debug("SessionInfoView.createReaplayView()");

        let area = this._area;

        let bg = null;

        let maxWidth = this.layout.relWidth(area.width);
        let maxHeight = this.layout.relHeight(area.height);
            
        // In realplay mode, if an openLink action is configured, we must create a touch area
        // that will use it.
        // TODO: The openLink functionality is implicitly for realplay (merkur want this), and
        // perhaps we need a smarter configuration. But that's for another day.
        if (this._sessionInfoConfig.openLink)
        {
            this.log.debug('SessionInfoView: needs to open a link when pressed');

            let openLinkConfig = this._sessionInfoConfig.openLink;
            let openLinkUtil = this.app.openLinkUtil;

            bg = new PIXI.Graphics();
            bg.beginFill(0x4477FF, 0);
            bg.drawRect(this.layout.relWidth(area.x), this.layout.relHeight(area.y), this.layout.relWidth(area.width), this.layout.relHeight(area.height));
            bg.endFill();
            bg.interactive = true;
            bg.buttonMode = true;
            bg.on(C_PointerEvt.UP, () => {
                this.log.debug('SessionInfoView.onPressed, open link');
                openLinkUtil.openLink(openLinkConfig);
            });

            this.addChild(bg);
        }
        else this.log.debug("SessionInfoView: doesn't need to open any link when pressed");

        let country = this._model.getSessionInfo().country;
        if (country === "ITA")
        {
            /** @type {SessionInfoItaly} */
            let sessionInfo = this._model.getSessionInfo();

            /** @type {PIXI.TextStyleOptions} */
            let textConfig = {
                fontFamily : "Arial",
                fontWeight : "normal",
                fill : 0xFFFFFF,
                fontSize : Math.round(this.layout.relHeight(area.height * 0.48)),
                lineHeight : Math.round(this.layout.relHeight(area.height * 0.5)),
                align : "center"
            };

            let textField = new PIXI.Text("Sample Text\nSampleText", textConfig);
            textField.anchor.set(0.5, 0.5);
            textField.x = this.layout.relX(area.left + (area.width * 0.5));
            textField.y = this.layout.relY(area.top + (area.height * 0.5));

            this.addChild(textField);
            this._activeContent = textField;

            let updateItalianInfoText = () => {
                this.log.debug("SessionInfoView.updateItalianInfoText()");

                let sessionIdText = this.locale.getString("T_sessionDetails_sessionId", {"[SESSION_ID]":sessionInfo.aamsSessionId});
                let ticketIdText = this.locale.getString("T_sessionDetails_ticketId", {"[TICKET_ID]":sessionInfo.aamsTicketId});
                let fullText = `${sessionIdText}\n${ticketIdText}`;
                
                textField.text = fullText;

                let srcWidth = textField.width / textField.scale.x;
                let srcHeight = textField.height / textField.scale.y;
                let newScale = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);

                textField.scale.set(newScale, newScale);
            }

            updateItalianInfoText();

            this._updateRealplayInfo = updateItalianInfoText;
            this._dispatcher.on(C_GameEvent.SESSION_CHANGED, this.updateRealplayView, this);
        }
    };


    /**
     * @private
     */
    updateRealplayView()
    {
        this.log.debug(`SessionInfoView.updateRealplayInfo()`);

        if (this._updateRealplayInfo) {
            this._updateRealplayInfo();
        }
    };


    /**
     * @private
     */
    destroyRealplayView()
    {
        this.log.debug("SessionInfoView.destroyRealplayView()");
        
        if (this._activeContent) {
            this.removeChild(this._activeContent);
            this._activeContent.destroy({ destroyChildren:true }); // destroy children as well
            this._activeContent = null;

            this._updateRealplayInfo = null;

            this._dispatcher.off(C_GameEvent.SESSION_CHANGED, this.updateRealplayView, this);
        }
    };
};

export default SessionInfoView;