import { getGameViewLogger } from "../../../cms-core-logic/src/logging/LogManager";

export class BaseMobileGameButton extends PIXI.Container
{
    constructor()
    {
        super();

        this.app = PIXI.app;

        /**
         * @protected
         */
        this.assets = this.app.assets;

        /**
         * @protected
         */
        this.layout = this.app.layout;

        /**
         * @protected
         */
        this.dispatcher = this.app.dispatcher;
        
        /**
         * @protected
         * @readonly
         */
        this.log = getGameViewLogger();
    };


    /**
     * Updates the overall enabled visual state for the mobile button. Mobile game buttons
     * will hide themselves when completely disabled. This will use a short animated transition
     * to show / hide the button (by changing its opacity). The animation will only be triggered
     * if the enabledState of the button has actually changed.
     * @protected
     * @final
     * @param {boolean} enabledState 
     */
    setEnabled(enabledState)
    {
        if (enabledState !== this._enabledState) {
            this._enabledState = enabledState;

            if (this._enabledTween) {
                this._enabledTween.kill();
                this._enabledTween = null;
            }

            if (enabledState) {
                this._enabledTween = TweenMax.fromTo(this, 0.3, { alpha:0 }, { alpha:1 });
            }
            else
            {
                this._enabledTween = TweenMax.fromTo(this, 0.3, { alpha:1 }, { alpha: 0 });
            }
        }
    };
}