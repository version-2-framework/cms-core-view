import BaseUI from "../ui/BaseUI";
import Button from "../ui/Button";
import C_PointerEvt from "../const/C_PointerEvt";
import C_TooltipOrientation from "../const/C_TooltipOrientation";
import C_GameEvent from "../../../cms-core-logic/src/const/C_GameEvent";
import C_TooltipPosition from "../const/C_TooltipPosition";
import * as DefaultKeyCodes from "../input/DefaultKeyCodes";
import * as KeyPhase from "../input/KeyPhase";

// TODO: This class needs to support some basic mechanism of localization. Currently, we show
// a single set of textures with standard ids. The correct approach, is probably to do something
// like the following:
// 1) have 2 sets of possible textures for Bet and Autoplay buttons (English and Icon based)
// 2) Provide icon based as a minimum
// 3) Be able to switch between the 2
// 4) Provide an option, which means we can only load up icon based if required
/**
 * Simple, default implementation of the desktop Autoplay Button
 */
class AutoplayButtonDesktop extends BaseUI
{
    constructor()
    {
        super();
        
        /**
         * Tracks whether the "open autoplay menu" button is enabled (it can be shown, but
         * not enabled).
         * @private
         * @type {boolean}
         */
        this._guiButtonsEnabled = false;

        /**
         * Indicates if the button is currently showing the "stop autoplay" visual state.
         * @private
         * @type {boolean}
         */
        this._isInStopAutoplayState = false;

        /**
         * Button that will open the Autoplay menu. Shown as long as no Autoplay session is in progress.
         * @private
         */
        this._autoplayMenuBtn = new Button('autoBtnDesktop', 'autoBtnDesktopOver', 'autoBtnDesktop', 'autoBtnDesktopDisabled');
        this._autoplayMenuBtn.on(C_PointerEvt.DOWN, this.onOpenAutoplayMenuPressed, this);
        this.addChild(this._autoplayMenuBtn);

        /**
         * Button that will end the current Autoplay session. Shown when an Autoplay session is in progress.
         * @private
         */
        this._stopAutoplayBtn = new Button('stopBtnDesktop', 'stopBtnDesktopOver');
        this.layout.propHeightScale(this._stopAutoplayBtn, 100, this._autoplayMenuBtn);
        this._stopAutoplayBtn.x = this._autoplayMenuBtn.x;
        this._stopAutoplayBtn.y = this._autoplayMenuBtn.y;
        this._stopAutoplayBtn.on(C_PointerEvt.DOWN, this.onStopAutoplayPressed, this);
        this.addChild(this._stopAutoplayBtn);

        // By default, assume buttons are center-bottom of the screen
        this.setTooltipOrientation(C_TooltipOrientation.VERTICAL, C_TooltipPosition.ABOVE);

        let buttonSoundId = this.app.config.uiSounds.desktop.autoplayButtonSound;
        if (buttonSoundId)
        {
            let buttonVolume = this.app.config.uiSounds.desktop.autoplayButtonVolume;

            this._autoplayMenuBtn.setClickSound(buttonSoundId, buttonVolume);
            this._stopAutoplayBtn.setClickSound(buttonSoundId, buttonVolume);

            this.log.debug(`AutoplayButtonDesktop: setting button sound to ${buttonSoundId}`);
        }
        else
        {
            this.log.debug('AutoplayButtonDesktop: no button sound available, button will not make sound when pressed');

            this._autoplayMenuBtn.setClickSound(null);
            this._stopAutoplayBtn.setClickSound(null);
        }

        // Listen out for the keyboard press
        this.app.keyboardManager.addKeyAction(DefaultKeyCodes.TOGGLE_AUTOPLAY_MENU, KeyPhase.PRESSED, this.onKeyPressed, this);

        this.dispatcher.on(C_GameEvent.AUTOPLAY_STARTED, this.onAutoplayStarted, this);
        this.dispatcher.on(C_GameEvent.AUTOPLAY_ENDED, this.onAutoplayEnded, this);
        
        // If we are showing the "stop" button, then we do not want to disable the autoplay
        // button only if it is currently in "autoplay" state (and not "stop" state)
        this.dispatcher.on(C_GameEvent.ENABLE_GUI_BUTTONS, this.onGuiButtonsEnabled, this);
        this.dispatcher.on(C_GameEvent.DISABLE_GUI_BUTTONS, this.onGuiButtonsDisabled, this);

        // Initially, the button should be disabled, until explicitly ordered to be enabled
        this.setToOpenAutoplayMenuState();
        this._autoplayMenuBtn.setEnabled(false);
        this._stopAutoplayBtn.setEnabled(false);
    };


    /**
     * In the case of this method, it effectively configures the tooltip from scratch
     * @public
     * @inheritDoc
     * @param {TooltipOrientation} orientation 
     * @param {TooltipPosition} position 
     */
    setTooltipOrientation(orientation=null, position=null)
    {
        let autoplayMenuTooltipText = this.app.locale.getString("T_tooltip_openAutoplayMenu");
        let stopAutoplayTooltipText = this.app.locale.getString("T_tooltip_stopAutoplay");
 
        this._autoplayMenuBtn.setToolTipProperties(autoplayMenuTooltipText, orientation, position);
        this._stopAutoplayBtn.setToolTipProperties(stopAutoplayTooltipText, orientation, position);
    };


    /**
     * @private
     */
    onAutoplayStarted()
    {
        this._stopAutoplayBtn.setEnabled(true);
        this.setToStopButtonState();
    };


    /**
     * @private
     */
    onAutoplayEnded()
    {
        this._stopAutoplayBtn.setEnabled(false);
        this.setToOpenAutoplayMenuState();
    };


    /**
     * @private
     */
    onGuiButtonsEnabled()
    {
        this._guiButtonsEnabled = true;
        this._autoplayMenuBtn.setEnabled(true);
    };


    /**
     * @private
     */
    onGuiButtonsDisabled()
    {
        this._guiButtonsEnabled = false;
        this._autoplayMenuBtn.setEnabled(false);
    };


    /**
     * Sets the button into the "open autoplay menu" visual state.
     * @private
     */
    setToOpenAutoplayMenuState()
    {
        this._isInStopAutoplayState = false;
        this._autoplayMenuBtn.visible = true;
        this._stopAutoplayBtn.visible = false;
    };


    /**
     * Sets the button in to the "stop autoplay" visual state.
     * @private
     */
    setToStopButtonState()
    {
        this._isInStopAutoplayState = true;
        this._autoplayMenuBtn.visible = false;
        this._stopAutoplayBtn.visible = true;
    };


    /**
     * Action invoked when the keyboard key that is the "Autoplay button" is pressed: we invoke the
     * appropriate action (either "Open Autoplay Menu", or "Stop Autoplay").
     * @private
     */
    onKeyPressed()
    {
        if (this._isInStopAutoplayState)
        {
            this.onStopAutoplayPressed();      
        }
        else
        {
            this.onOpenAutoplayMenuPressed();
        }
    };


    /**
     * Action, which requests to open the Autoplay Settings Menu.
     * @private
     */
    onOpenAutoplayMenuPressed()
    {   
        this.dispatcher.emit(C_GameEvent.BUTTON_TOGGLE_AUTOPLAY_MENU_PRESSED);
    };


    /**
     * Action, which requests to cancel any Autoplay Session that is in progress.
     * @private
     */
    onStopAutoplayPressed()
    {
        this.dispatcher.emit(C_GameEvent.BUTTON_STOP_AUTOPLAY_PRESSED);
    };
}

export default AutoplayButtonDesktop;