/**
 * Desktop specific UI.
 */
import Button from "../ui/Button";
import C_PointerEvt from "../const/C_PointerEvt";
import BaseView from "./BaseView";
import C_GameEvent from "../../../cms-core-logic/src/const/C_GameEvent";
import SessionInfoView from './SessionInfoView';
import { shareOnFacebook, shareOnTwitter } from "../../../cms-core-logic/src/utils/SocialMediaUtil";
import C_TooltipOrientation from "../const/C_TooltipOrientation";
import C_TooltipPosition from "../const/C_TooltipPosition";
import * as DefaultKeyCodes from "../input/DefaultKeyCodes";
import * as KeyPhase from "../input/KeyPhase";

class UIDesktop extends BaseView
{
    constructor()
    {
        super();

        this.name = "GuiDesktop";

        /**
         * Configurable part of BusinessConfig : specifies changes for GUI between licensees (certain actions
         * for buttons, whether certain buttons are available for that licensee, etc).
         * @private
         * @type {DesktopGuiConfig}
         */
        this._guiConfig = this._businessConfig.desktopGui;

        if (!this._guiConfig) {
            this.log.error(`UIDesktop: GuiConfig is not defined! UI will fall back to basic state`);
        }

        /**
         * @todo: Document properly
         * @private
         */
        this._guiButtons = [];

        this.defineLayout();

        let guiConfig = this._guiConfig;

        let placeMessageBarAboveGameButtons = this.app.config.uiStyle.desktopPlaceMessageBarAboveGameButtons;

        if (!placeMessageBarAboveGameButtons) {
            this.createMessageBar();
        }
        
        if (guiConfig && guiConfig.openMenuButton && guiConfig.openMenuButton.isEnabled) {
            this.createOpenMenuBtn();
        }

        if (guiConfig && guiConfig.topLeftInfoSlot) {
            let infoSlotConfig = guiConfig.topLeftInfoSlot;
            let infoSlotArea = this._topLeftInfoSlotArea;
            this.createInfoSlotComponent(infoSlotConfig, infoSlotArea);
        }

        this.createGameLogo();
        
        if (guiConfig && guiConfig.topRightInfoSlot) {
            let infoSlotConfig = guiConfig.topRightInfoSlot;
            let infoSlotArea = this._topRightInfoSlotArea;
            this.createInfoSlotComponent(infoSlotConfig, infoSlotArea);
        }

        if (guiConfig && guiConfig.closeSessionButton && guiConfig.closeSessionButton.isEnabled) {
            this.createCloseSessionBtn();
        }
        
        this.getValidatedGameButtonsZOrder().forEach(gameButtonId =>
        {
            if (gameButtonId === "auto") {
                this.createAutoplayButton();
            }
            else
            if (gameButtonId === "bet") {
                this.createBetButton();
            }
            else
            if (gameButtonId === "spin") {
                this.createSpinButton();
            }
        });

        if (placeMessageBarAboveGameButtons) {
            this.createMessageBar();
        }

        this.createUiTexts();

        if (guiConfig)
        {
            if (guiConfig.fullScreenButton && guiConfig.fullScreenButton.isEnabled) {
                this.createFullScreenBtn();
            }

            if (guiConfig.toggleSoundButton && guiConfig.toggleSoundButton.isEnabled) {
                this.createSoundBtn();
            }

            if (guiConfig.socialMediaButtons && guiConfig.socialMediaButtons.isEnabled) {
                this.createSocialMediaButtons();
            }

            if (guiConfig.addCreditButton && guiConfig.addCreditButton.isEnabled) {
                this.createAddCreditBtn();
            }
        }

        if (false)
        {
            this.log.debug('UiDesktop: add layout overlay');

            this.addChild(this.getLayoutOverlay());
        }

        // Initially disable input, until we are explicitly told to enable it
        this.setInputEnabledTo(false);
        
        this.dispatcher.on(C_GameEvent.ENABLE_GUI_BUTTONS, () => this.setInputEnabledTo(true));
        this.dispatcher.on(C_GameEvent.DISABLE_GUI_BUTTONS, () => this.setInputEnabledTo(false));
    };


    /**
     * Returns the target game buttons order. This will validate the config data - if the config data
     * is no good, this will simply return standard ordering.
     * @private
     * @return string[]
     */
    getValidatedGameButtonsZOrder()
    {
        let gameButtonsOrder = this.app.config.uiStyle.desktopGameButtonsZOrder;
        if (gameButtonsOrder.length === 3 &&
            gameButtonsOrder.indexOf("auto") > -1 &&
            gameButtonsOrder.indexOf("bet") > -1 &&
            gameButtonsOrder.indexOf("spin") > -1)
        {
            return gameButtonsOrder;
        }
        else
        {
            this.log.warn(`gameButtonsOrder ${gameButtonsOrder} is not valid. Using default values`);
            return ["auto", "bet", "spin"];
        }
    };


    /**
     * Configures the general layout areas of the desktop GUI.
     * @protected
     */
    defineLayout()
    {
        /**
         * The on-screen area that the Menu button must fit within. This is defined in terms
         * of percentage of screen space.
         * @private
         * @type {PIXI.Rectangle}
         */
        this._menuBtnArea = new PIXI.Rectangle(2, 2, 5, 5);

        /**
         * The on-screen area that any Top Left Info Slot Component must fit within. This is
         * defined in terms of percentage of screen space.
         * @private
         * @type {PIXI.Rectangle}
         */
        this._topLeftInfoSlotArea = new PIXI.Rectangle(7, 2.5, 23, 4);

        /**
         * The on-screen area that the Game Logo must fit within. This is defined in terms of
         * percentage of screen space.
         * @private
         * @type {PIXI.Rectangle}
         */
        this._gameLogoArea = new PIXI.Rectangle(30, 1, 40, 8);

        /**
         * The on-screen area that any Top Right Info Slot Component must fit within. This is
         * defined in terms of percentage of screen space.
         * @private
         * @type {PIXI.Rectangle}
         */
        this._topRightInfoSlotArea = new PIXI.Rectangle(70, 2.5, 23, 4);

        /**
         * The on-screen area that the Close Session button must fit within.
         * @private
         * @type {PIXI.Rectangle}
         */
        this._closeBtnArea = new PIXI.Rectangle(93, 2, 5, 5);

        // TODO: Add in "side button" areas ?
        /**
         * @private
         * @type {PIXI.Rectangle}
         */
        this._leftButtonsArea = new PIXI.Rectangle(2, 10, 5, 70);

        /**
         * @private
         * @type {PIXI.Rectangle}
         */
        this._rightButtonsArea = new PIXI.Rectangle(93, 10, 5, 70);

        /**
         * The vertical padding between items (buttons) within the left or right areas.
         * This is a proportional value.
         * @private
         */
        this._sideItemsPadding = 2;
    };


    /**
     * Creates and returns a display object, defining the available layout areas of the GUI, drawn
     * using semi-transparent boxes. This gives us a hint about where stuff can go on screen, because
     * it shows the available bounds for different components.
     * @private
     * @return {PIXI.DisplayObject}
     */
    getLayoutOverlay()
    {
        let rectsToDraw =
        [
            this._menuBtnArea,
            this._topLeftInfoSlotArea,
            this._gameLogoArea,
            this._topRightInfoSlotArea,
            this._closeBtnArea,
            this._leftButtonsArea,
            this._rightButtonsArea
        ];

        let overlay = new PIXI.Graphics();
        overlay.beginFill(0xffff00, 0.25); //0.5);
        overlay.lineStyle(4, 0xaa4400, 1, 0.5);

        rectsToDraw.forEach(rect => overlay.drawRect(
            this.layout.relX(rect.x),
            this.layout.relY(rect.y),
            this.layout.relX(rect.width),
            this.layout.relY(rect.height)
        ));

        overlay.endFill();
        
        return overlay;
    };
    

    /**
     * Creates the Menu button in the top left of the screen.
     * @private
     */
    createOpenMenuBtn()
    {
        let menuBtn = new Button('menuBtn', 'menuBtn_over', 'menuBtn_over', 'menuBtn_disabled');
        this.layout.fitAndCenterDisplayObjectInProportionalArea(menuBtn, this._menuBtnArea);

        menuBtn.name = "GuiDesktop.MenuBtn";
        menuBtn.on(C_PointerEvt.DOWN, this.onMenuBtnPressed, this);
        menuBtn.setToolTipProperties(this.app.locale.getString("T_tooltip_openMenu"), C_TooltipOrientation.VERTICAL, C_TooltipPosition.BELOW);
        this.addChild(menuBtn);

        this.keyboardManager.addKeyActionOnPressed(DefaultKeyCodes.TOGGLE_GAME_MENU, this.onMenuBtnPressed, this);

        if (this.app.config.uiSounds.desktop.guiButtonSound)
        {
            let buttonSoundId = this.app.config.uiSounds.desktop.guiButtonSound;
            let buttonVolume = this.app.config.uiSounds.desktop.guiButtonVolume;

            this.log.debug(`OpenMenuButtonDesktop: setting sound to ${buttonSoundId}`);

            menuBtn.setClickSound(buttonSoundId, buttonVolume);
        }
        else
        {
            this.log.debug('OpenMenuButtonDesktop: no button sound available, button will not make sound when pressed');

            menuBtn.setClickSound(null);
        }

        this._guiButtons.push(menuBtn);

        /**
         * Button for opening the game menu. Not necessarily created - please check before accessing.
         * @private
         * @type {Button}
         */
        this._menuBtn = menuBtn;
    };


    /**
     * @private
     */
    onMenuBtnPressed()
    {
        this.log.debug("OpenMenuButtonDesktop: pressed");

        this.dispatcher.emit(C_GameEvent.BUTTON_TOGGLE_GAME_MENU_PRESSED);
    };


    /**
     * Creates the Close Button in the top right of the screen.
     * @private
     */
    createCloseSessionBtn()
    {
        let closeBtn = new Button('closeBtn', 'closeBtn_over', 'closeBtn_over', 'closeBtn_disabled');
        closeBtn.name = "GuiDesktop.CloseBtn";
        closeBtn.on(C_PointerEvt.DOWN, this.onCloseButtonPressed, this);
        closeBtn.setToolTipProperties(this.app.locale.getString("T_tooltip_closeSession"), C_TooltipOrientation.VERTICAL, C_TooltipPosition.BELOW);

        this.keyboardManager.addKeyActionOnPressed(DefaultKeyCodes.CLOSE_GAME_SESSION, this.onCloseButtonPressed, this);

        this.layout.fitAndCenterDisplayObjectInProportionalArea(closeBtn, this._closeBtnArea);

        if (this.app.config.uiSounds.desktop.guiButtonSound)
        {
            let buttonSoundId = this.app.config.uiSounds.desktop.guiButtonSound;
            let buttonVolume = this.app.config.uiSounds.desktop.guiButtonVolume;

            this.log.debug(`CloseSessionButtonDesktop: setting sound to ${buttonSoundId}`);

            closeBtn.setClickSound(buttonSoundId, buttonVolume);
        }
        else
        {
            this.log.debug('CloseSessionButtonDesktop: no button sound available, button will not make sound when pressed');
            
            closeBtn.setClickSound(null);
        }

        this.addChild(closeBtn);
        this._guiButtons.push(closeBtn);

        /**
         * Button for closing the game client (or the current session). Not necessarily created - please check
         * before accessing.
         * @private
         * @type {Button}
         */
        this._closeBtn = closeBtn;
    };


    /**
     * Action invoked when the Close Session button is pressed, or the Keyboard shortcut is pressed.
     * @private
     */
    onCloseButtonPressed()
    {
        this.log.debug("CloseSessionButtonDesktop: pressed");

        this.dispatcher.emit(C_GameEvent.BUTTON_CLOSE_SESSION_PRESSED);
    };


    /**
     * Creates the Autoplay Settings Button, adds it to the display list, and positions it.
     * @private
     */
    createAutoplayButton()
    {
        let autoplayButton = this.gameFactory.getAutoplayButtonDesktop();
        let posData = this.config.uiLayoutDesktop.autoplayButtonPos;

        this.layout.propHeightScale(autoplayButton, posData.height);
        autoplayButton.x = this.layout.relWidth(posData.x)  - (autoplayButton.width  * 0.5);
        autoplayButton.y = this.layout.relHeight(posData.y) - (autoplayButton.height * 0.5);

        autoplayButton.name = "GuiDesktop.AutoplayBtn";
        
        this.addChild(autoplayButton);

        /**
         * Button for changing autoplay / opening the autoplay menu.
         * @private
         * @type {PIXI.DisplayObject}
         */
        this._autoplayButton = autoplayButton;
    };


    /**
     * Creates the Spin Button, adds it to the display list, and positions it.
     * @private
     */
    createSpinButton()
    {
        let spinButton = this.gameFactory.getSpinButtonDesktop();
        let posData = this.config.uiLayoutDesktop.spinButtonPos;

        this.layout.propHeightScale(spinButton, posData.height);
        spinButton.x = this.layout.relWidth(posData.x)  - (spinButton.width  * 0.5);
        spinButton.y = this.layout.relHeight(posData.y) - (spinButton.height * 0.5);

        spinButton.name = "GuiDesktop.SpinBtn";

        this.addChild(spinButton);

        /**
         * Button for triggering a spin.
         * @private
         * @type {PIXI.DisplayObject}
         */
        this._spinButton = spinButton;
    };


    /**
     * Creates the Bet Settings Button, adds it to the display list, and positions it.
     * @private
     */
    createBetButton()
    {
        let betButton = this.gameFactory.getBetButtonDesktop();
        let posData = this.config.uiLayoutDesktop.betButtonPos;

        this.layout.propHeightScale(betButton, posData.height);
        betButton.x = this.layout.relWidth(posData.x)  - (betButton.width  * 0.5);
        betButton.y = this.layout.relHeight(posData.y) - (betButton.height * 0.5);

        betButton.name = "GuiDesktop.BetBtn";

        this.addChild(betButton);

        /**
         * Button for changing bet / opening the bet menu.
         * @private
         * @type {PIXI.DisplayObject}
         */
        this._betButton = betButton;
    };


    /**
     * @private
     */
    createUiTexts()
    {
        let uiTexts = this.gameFactory.getUITexts();
        this.addChild(uiTexts);
        uiTexts.create();   // there is no obvious reason why this method exists.. Wiener couldn't be bothered to document it
        uiTexts.name = "GuiDesktop.InfoBar";
    };


    /**
     * @private
     */
    createMessageBar()
    {
        let messageBar = this.gameFactory.getMessageBar();
        messageBar.create();   // there is no obvious reason why this method exists.. Wiener couldn't be bothered to document it
        messageBar.name = "GuiDesktop.MessageBar";
        this.addChild(messageBar);
    };


    /**
     * Creates the Game Logo, which appears in the top center of the screen.
     * @private
     */
    createGameLogo()
    {
        let targetHeight = this.layout.relHeight(this.config.uiLayoutDesktop.gameLogoHeight);
        let targetCenterPosX = this.layout.relWidth(this.config.uiLayoutDesktop.gameLogoCenterPosX);
        let targetCenterPosY = this.layout.relHeight(this.config.uiLayoutDesktop.gameLogoCenterPosY);

        /**
         * @private
         */
        this._gameLogo = this._gameFactory.getGameLogo(targetCenterPosX, targetCenterPosY, targetHeight);

        this.addChild(this._gameLogo);
    };


    // TODO: This looks highly redundant
    /**
     * @private
     * @param {*} b 
     * @param {*} text 
     */
    createButtonLabel(b, text)
    {
        let btnTextSize = this.layout.relHeight(this.config.desktopButLabelsSize);

        let butTF = this.assets.getTF(text, btnTextSize,
            this.config.desktopButLabelsFont, this.config.desktopButLabelsColor, 1.1);
        butTF.anchor.set(.5, .5);
        this.layout.horizontalAlignCenter(butTF, 0, b);
        this.layout.verticalAlignMiddle(butTF, 0, b);

        this.addChild(butTF);

        return butTF;
    };


    /**
     * Creates an Info Slot Component, using a given config and a target area.
     * @private
     * @param {DesktopGuiInfoSlotConfig} infoSlotConfig 
     * @param {*} infoSlotArea 
     */
    createInfoSlotComponent(infoSlotConfig, infoSlotArea)
    {
        if (infoSlotConfig.type === "responsibleGamingLinks")
        {
            this.log.debug('DesktopGui.createInfoSlotComponent: creating Responsible Gaming Links');
            this.createResponsibleGamingLinks(infoSlotConfig, infoSlotArea);
        }
        else
        if (infoSlotConfig.type === "sessionInfo")
        {
            this.log.debug('DesktopGui.createInfoSlotComponent: creating Session Info');
            this.createSessionInfoView(infoSlotConfig, infoSlotArea);
        }
        else
        {
            this.log.debug('DesktopGui.createInfoSlotComponent: nothing to create');
        }
    };


    // TODO: For future, non-italian use cases, this functionality will probably want
    // renaming or modifying, so it describes something a bit more generic.
    /**
     * Creates the Responsible Gaming links component.
     * @param {ResponsibleGamingLinksInfoSlotConfig} config
     * @param {PIXI.Rectangle} infoSlotArea
     * @private
     */
    createResponsibleGamingLinks(config, infoSlotArea)
    {
        let linksContainer = new PIXI.Container();
        let logoConfigs = this._businessConfig.responsibleGamingLinks;

        this.log.debug(`createResponsibleGamingLinks: ${JSON.stringify(logoConfigs)}`);

        for (let logoIdx = 0; logoIdx < logoConfigs.length; logoIdx++)
        {
            let logoConfig = logoConfigs[logoIdx];
            let logoTextureId = logoConfig.buttonTexture;
            let logoOpenLinkAction = logoConfig.action;
            let logo = this.assets.getSprite(logoTextureId);
            this.layout.propHeightScale(logo, 100, infoSlotArea);
            linksContainer.addChild(logo);

            if (logoIdx > 0) {
                logo.x = linksContainer.children[logoIdx - 1].x + linksContainer.children[logoIdx - 1].width + linksContainer.children[0].width * 0.1;
            }

            if (logoOpenLinkAction) {
                logo.interactive = true;
                logo.buttonMode = true;

                // Must be on UP for iOS to work for opening external links
                logo.on(C_PointerEvt.UP, () => {
                    this._openLinkUtil.openLink(logoOpenLinkAction);
                });
            }
        }
        
        this.layout.fitAndCenterDisplayObjectInProportionalArea(linksContainer, infoSlotArea);

        this.addChild(linksContainer);

        linksContainer.name = "GuiDesktop.ResponsibleGamingLinks";
    };


    /**
     * Creates the Full Screen button, which sits in the bottom right corner of the view.
     * @private
     */
    createFullScreenBtn()
    {
        let fullScreenBtn = new Button('fullScreenBtn', 'fullScreenBtn_over', 'fullScreenBtn_over', 'fullScreenBtn');

        this.layout.propHeightScale(fullScreenBtn, 5);
        this.layout.verticalAlignBottom(fullScreenBtn, this.config.uiStyle.uiStatsBg.h + 6);
        this.layout.horizontalAlignRight(fullScreenBtn, 2);
        fullScreenBtn.setToolTipProperties(this.app.locale.getString("T_tooltip_toggleFullScreen"), C_TooltipOrientation.VERTICAL, C_TooltipPosition.ABOVE);
        
        // TODO: We need to listen out for f11 fullscreen toggles
        fullScreenBtn.on(C_PointerEvt.DOWN, () => {
            this.log.debug("FullScreenButtonDesktop: pressed");
            this.app.toggleFullScreen();
        });

        fullScreenBtn.name = "GuiDesktop.FullScreenBtn";

        if (this.app.config.uiSounds.desktop.guiButtonSound)
        {
            let buttonSoundId = this.app.config.uiSounds.desktop.guiButtonSound;
            let buttonVolume = this.app.config.uiSounds.desktop.guiButtonVolume;

            this.log.debug(`FullScreenButtonDesktop: setting sound to ${buttonSoundId}`);

            fullScreenBtn.setClickSound(buttonSoundId, buttonVolume);
        }
        else
        {
            this.log.debug('FullScreenButtonDesktop: no button sound available, button will not make sound when pressed');

            fullScreenBtn.setClickSound(null);
        }

        this.addChild(fullScreenBtn);

        /**
         * Button for toggling the fullscreen status of the game. Not necessarily created - please
         * check before accessing it.
         * @private
         * @type {Button}
         */
        this._fullScreenBtn = fullScreenBtn;
    };


    /**
     * Creates the Add Credit button which sits in the bottom left corner of the view.
     * @private
     */
    createAddCreditBtn()
    {
        /**
         * Button which will trigger an Add Credit action. Not always enabled, and no necessarily
         * created - please check before accessing it.
         * @private
         * @type {Button}
         */
        this._addCreditBtn = new Button('addCreditBtn', 'addCreditBtn_over', 'addCreditBtn_over', 'addCreditBtn_disabled');
        this._addCreditBtn.name = "GuiDesktop.AddCreditBtn";

        this.layout.propHeightScale(this._addCreditBtn, 5);
        this.layout.verticalAlignBottom(this._addCreditBtn, this.config.uiStyle.uiStatsBg.h + 6);
        this.layout.horizontalAlignLeft(this._addCreditBtn, 2);
        this._addCreditBtn.on(C_PointerEvt.DOWN, this.onAddCreditPressed, this);

        this.keyboardManager.addKeyActionOnPressed(DefaultKeyCodes.ADD_CREDIT, this.onAddCreditPressed, this);

        if (this.app.config.uiSounds.desktop.guiButtonSound)
        {
            let buttonSoundId = this.app.config.uiSounds.desktop.guiButtonSound;
            let buttonVolume = this.app.config.uiSounds.desktop.guiButtonVolume;

            this.log.debug(`AddCreditButtonDesktop: setting sound to ${buttonSoundId}`);

            this._addCreditBtn.setClickSound(buttonSoundId, buttonVolume);
        }
        else
        {
            this.log.debug('AddCreditButtonDesktop: no button sound available, button will not make sound when pressed');

            this._addCreditBtn.setClickSound(null);
        }

        this._addCreditBtn.setToolTipProperties(this.app.locale.getString("T_tooltip_addCredit"), C_TooltipOrientation.VERTICAL, C_TooltipPosition.ABOVE);
        this.addChild(this._addCreditBtn);
    };


    /**
     * @private
     */
    onAddCreditPressed()
    {
        this.log.debug("AddCreditButtonDesktop: pressed");

        this.dispatcher.emit(C_GameEvent.BUTTON_ADD_CREDIT_PRESSED);
    };


    /**
     * Creates the toggle sound button.
     * @private
     */
    createSoundBtn()
    {
        /**
         * Button that will toggle the status of Sound (on or off). Not necessarily created, so check
         * if its null before accessing it.
         * @private
         * @type {Button}
         */
        this._soundBtn = new Button('soundOnBtn', 'soundOnBtn','soundOnBtn', 'soundOnBtn');
        this._soundBtn.name = "GuiDesktop.SoundBtn";

        this.layout.fitAndCenterDisplayObjectInProportionalArea(this._soundBtn, this._leftButtonsArea);

        this._soundBtn.on(C_PointerEvt.DOWN, this.onToggleSoundPressed, this);

        this.app.keyboardManager.addKeyAction(DefaultKeyCodes.TOGGLE_SOUND, KeyPhase.PRESSED, this.onToggleSoundPressed, this);

        this.setSoundBtnStatus();

        this._soundBtn.setToolTipProperties(this.app.locale.getString("T_tooltip_toggleSound"), C_TooltipOrientation.HORIZONTAL, C_TooltipPosition.RIGHT);

        this.addChild(this._soundBtn);

        this.dispatcher.on(C_GameEvent.SOUND_TOGGLED, this.setSoundBtnStatus, this);
    };


    /**
     * @private
     */
    onToggleSoundPressed()
    {
        this.log.debug("ToggleSoundButtonDesktop: pressed");

        this.sound.toggleSound();
        this.setSoundBtnStatus();
        this.dispatcher.emit(C_GameEvent.BUTTON_TOGGLE_SOUND_PRESSED);
    };


    /**
     * Updates the visual state of the Toggle Sound button, to whatever is appropriate (considering
     * the current enabled state of sound overall)
     * @private
     */
    setSoundBtnStatus()
    {
        let skin = this.sound.getSoundEnabled() ? 'soundOnBtn' : 'soundOffBtn';
        this._soundBtn.setSkins(skin,skin,skin,skin);
    };


    /**
     * Creates the Social Media buttons, and adds them to the display list.
     * @private
     */
    createSocialMediaButtons()
    {
        this.log.debug('GuiDesktop.createSocialMediaButtons()');
        
        let twitterButton = new Button('btnTwitter');
        twitterButton.name = "TwitterBtn";

        let facebookButton = new Button('btnFacebook');
        facebookButton.name = "FacebookBtn";
        
        let buttonsGroup = new PIXI.Container();
        buttonsGroup.name = "GuiDesktop.SocialMediaButtons";
        buttonsGroup.addChild(twitterButton);
        buttonsGroup.addChild(facebookButton);

        twitterButton.setToolTipProperties(this.app.locale.getString("T_tooltip_shareOnTwitter"), C_TooltipOrientation.HORIZONTAL, C_TooltipPosition.LEFT);
        facebookButton.setToolTipProperties(this.app.locale.getString("T_tooltip_shareOnFacebook"), C_TooltipOrientation.HORIZONTAL, C_TooltipPosition.LEFT);

        twitterButton.on(C_PointerEvt.DOWN, this.onButtonTwitterPressed, this);
        facebookButton.on(C_PointerEvt.DOWN, this.onButtonFacebookPressed, this);

        this.app.keyboardManager.addKeyAction(DefaultKeyCodes.SHARE_ON_FACEBOOK, KeyPhase.PRESSED, this.onButtonFacebookPressed, this);
        this.app.keyboardManager.addKeyAction(DefaultKeyCodes.SHARE_ON_TWITTER, KeyPhase.PRESSED, this.onButtonTwitterPressed, this);

        this.layout.setWidth(twitterButton, this.layout.relWidth(this._rightButtonsArea.width));
        this.layout.setWidth(facebookButton, this.layout.relWidth(this._rightButtonsArea.width));

        twitterButton.y = 0;
        facebookButton.y = this.layout.relHeight(this._sideItemsPadding) + twitterButton.height;

        this.addChild(buttonsGroup);
        this.layout.fitAndCenterDisplayObjectInProportionalArea(buttonsGroup, this._rightButtonsArea);
    };


    /**
     * @private
     * @return {string}
     */
    getSocialMediaMessage()
    {
        let gameName = this._config.gameName;
        let winnings = this._model.getPlayerWinnings();

        if (winnings > 0) {
            return this.locale.getString(
                "T_socialMediaMessage_winnings",
                { "[GAME_NAME]":gameName, "[WINNINGS]":winnings });
        }
        else
        {
            return this.locale.getString("T_socialMediaMessage_default", { "[GAME_NAME]":gameName });
        }
    }


    /**
     * @private
     */
    onButtonTwitterPressed()
    {
        let socialMediaConfig = this._businessConfig.desktopGui.socialMediaButtons;
        let message = this.getSocialMediaMessage();
        this.log.debug(`twitter button pressed, message = ${message}`);
        shareOnTwitter(socialMediaConfig.url_twitter, message);
    };


    /**
     * @private
     */
    onButtonFacebookPressed()
    {
        let socialMediaConfig = this._businessConfig.desktopGui.socialMediaButtons;
        let message = this.getSocialMediaMessage();
        this.log.debug(`facebook button pressed, message = ${message}`);
        shareOnFacebook(socialMediaConfig.url_facebook, message);
    };


    /**
     * Creates the Session Info View component that sits in the top-right of the desktop viewport.
     * For Freeplay games, this simply shows a graphic indicating that you are playing in Freeplay
     * mode. For Italian games, it shows Aams Session & Ticket Ids. For other future countries, it
     * may be required to show different info.
     * 
     * In addition, some licensees now require us to open an external link when this area is pressed.
     * @protected
     * @param {SessionInfoSlotConfig} sessionInfoConfig
     * @param {PIXI.Rectangle} sessionInfoArea
     */
    createSessionInfoView(sessionInfoConfig, sessionInfoArea)
    {
        // TODO: Its an open question about how this component should work. Should it
        // a) resize itself (this would be better, because it updates itself)
        // b) be resized here.
        // If its A, the code needs moving there (ideally the "fitInBox" method would be a util,
        // on the layout class - note that the layout class really needs properly documenting, as
        // it seems poorly designed at the moment
        let sessionInfoView = new SessionInfoView(sessionInfoConfig, sessionInfoArea);
        sessionInfoView.name = "GuiDesktop.SessionInfoView";
        
        this.addChild(sessionInfoView);
    }


    /**
     * Enables or disables button input. If a value of true is passed, not all buttons may actually be enabled:
     * the ability of certain inputs to be enabled may depend on other factors (eg: the add credit button may
     * be disabled if rules regarding the maximum amount of credit that can be added are imposed). Passing a
     * value of true simply means "it is time for the UI to enable buttons".
     * @private
     * @param {boolean} enabled 
     */
    setInputEnabledTo(enabled)
    {
        if (this._addCreditBtn) {
            let addCreditButtonEnabled = this._model.canAddMoreCreditToSession() && enabled;
            this._addCreditBtn.setEnabled(addCreditButtonEnabled);
        }

        for (let btnIdx = 0; btnIdx < this._guiButtons.length; btnIdx++)
        {
            this._guiButtons[btnIdx].setEnabled(enabled);
        }
    }
}

export default UIDesktop