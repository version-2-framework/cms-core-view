import BaseView from "./BaseView";
import Button from "../ui/Button";
import DropShadowFilter from "../filters/DropShadowFilter";



/**
 * Used for freespin intro and outro popups
 */
class PromptView extends BaseView
{


    constructor(data, animIn = null, animOut = null)
    {
        super();

        this.animOut = animOut;
        this.buttons = {};

        for (let i = 0; i < data.length; i++)
        {
            if (data[i].type === "img")
            {
                this.addImage(Object.assign({}, data[i]));
            }
            else if (data[i].type === "btn")
            {
                this.addButton(Object.assign({}, data[i]));
            }
            else if (data[i].type.indexOf("text") !== -1)
            {
                this.addTextField(Object.assign({}, data[i]));
            }
        }

        if (animIn)
        {
            animIn(this);
        }
    }


    create()
    {
        // Do not remove - needed for polymorphism
    }


    addImage(data)
    {
        let img = this.assets.getSprite(data.src);

        if (data.height)
        {
            this.layout.setHeightAndPos(img, data.height, data.x || 0, data.y || 0);
        }
        else
        {
            this.layout.setPos(img, data.x || 0, data.y || 0);
        }

        delete data.height;
        delete data.x;
        delete data.y;

        Object.assign(img, data);

        this.addChild(img);
    }


    addTextField(data)
    {
        let string;
        if (data.type.indexOf("spins") !== -1)
        {
            string = this.model.getFreespinsNum();
        }
        else if (data.type.indexOf("multiplier") !== -1)
        {
            string = this.model.getFreespinMultiplier();
        }
        else if (data.type.indexOf("win") !== -1)
        {
            string = this.model.formatCurrency(this.model.getFreespinWinnings(), true, false);
        }
        else if (data.type.indexOf("json") !== -1)
        {
            string = this.locale.getString(data.json);
        }
        else
        {
            string = data.text;
        }

        data.style = Object.assign({}, data.style);
        let dropShadowFilter;
        if (data.style && data.style.dropShadow === "filter")
        {
            delete data.style.dropShadow;
            dropShadowFilter = new DropShadowFilter();

            if (data.style.dropShadowAngle)
            {
                dropShadowFilter.rotation = data.style.dropShadowAngle;
                delete data.style.dropShadowAngle;
            }
            if (data.style.dropShadowBlur)
            {
                dropShadowFilter.blur = data.style.dropShadowBlur;
                delete data.style.dropShadowBlur;
            }
            if (data.style.dropShadowColor)
            {
                dropShadowFilter.color = data.style.dropShadowColor;
                delete data.style.dropShadowColor;
            }
            if (data.style.dropShadowDistance)
            {
                dropShadowFilter.distance = data.style.dropShadowDistance;
                delete data.style.dropShadowDistance;
            }
            if (data.style.dropShadowAlpha)
            {
                dropShadowFilter.alpha = data.style.dropShadowAlpha;
                delete data.style.dropShadowAlpha;
            }
            else
            {
                dropShadowFilter.alpha = 1;
            }
        }

        let txt = this.assets.getTF(string, this.layout.relHeight(data.size), data.font, data.color, data.lineHeight, data.wrapWidth, data.style);

        Object.assign(txt, data)

        this.layout.setPos(txt, data.bounds.x, data.bounds.y);
        txt.fieldWidth = data.bounds.width;
        txt.fieldHeight = data.bounds.height;

        if (data.style.anchor)
        {
            txt.anchor.set(data.style.anchor[0], data.style.anchor[1]);
            let _xPos = data.bounds.x + (data.bounds.width * (1 - data.style.anchor[0]) );
            let _yPos = data.bounds.y + (data.bounds.height * (data.style.anchor[1]) );
            this.layout.setPos(txt, _xPos, _yPos);
        }

        if (data.style.sizeGroup)
        {
            txt.setSizeGroup(data.style.sizeGroup);
        }


        if (txt.style.strokeThickness)
        {
            txt.style.strokeThickness *= txt.style.fontSize;
        }
        if (dropShadowFilter)
        {
            dropShadowFilter.distance *= txt.style.fontSize;
            dropShadowFilter.blur *= txt.style.fontSize;
            txt.filters = [dropShadowFilter];
        }
        else if (txt.style.dropShadow)
        {
            dropShadowFilter.distance *= txt.style.fontSize;
            dropShadowFilter.blur *= txt.style.fontSize;
        }

        this.addChild(txt);
    }


    addButton(data)
    {
        if (!data.id)
        {
            let n = 1;
            let id = "default"
            while (this.buttons[id])
            {
                id = "default" + n;
            }
            data.id = id;
        }

        let btn = new Button(data.up, data.over, data.down, data.off);

        if (data.bounds)
        {
            this.layout.putInRect(btn, data.bounds, this);
        }
        else
        {
            this.layout.setHeightAndPos(btn, data.height, data.x, data.y);
        }

        this.addChild(btn);

        this.buttons[data.id] = btn;
    }

    /**
     * Sets action for the button
     * @param id - id of button
     * @param listener - listener on which to trigger action... usually C_PointerEvt.DOWN
     * @param action - action to execute on listener triggered
     */
    addButtonAction(id, listener, action)
    {
        if (this.buttons[id])
        {
            this.buttons[id].on(listener, action);
        }
    }


    removeButtonActions()
    {
        for (let prop in this.buttons)
        {
            this.buttons[prop].removeAllListeners();
        }
    }


    close()
    {
        this.removeButtonActions();
        if (this.animOut)
        {
            this.animOut(this, this.removeFromParent.bind(this));
        }
        else
        {
            this.removeFromParent();
        }
    }


}

export default PromptView