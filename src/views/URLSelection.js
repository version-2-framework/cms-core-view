
import BaseView from "./BaseView";
import C_PointerEvt from "../const/C_PointerEvt";
import {V1_URLS, V2_URLS} from "../const/C_TestUrls";
import * as Comms from '../../../cms-core-logic/src/controllers/Comms';
import C_GameEvent from "../../../cms-core-logic/src/const/C_GameEvent";
import ScrollContainer from "../ui/ScrollContainer";

/**
 * View used on non-production versions of the game: allows you to select a specific url to play the
 * game against.
 */
class URLSelection extends BaseView
{
    constructor()
    {
        super();

        this.log.debug('URLSelectionView:constructor()');
        
		let headHeightRatio = 0.1;
        let footHeightRatio = 0.15;
        
        let allServerButtonsContainer = new PIXI.Container();

        /**
         * Background to the view's header area.
         * @private
         * @type {PIXI.mesh.NineSlicePlane}
         */
        this._headerBg = this.assets.getS9Sprite('wmg_contentBox_blue');
		this._headerBg.width  = this.layout.relWidth(100);
        this._headerBg.height = this.layout.relHeight(headHeightRatio * 100);
        this._headerBg.interactive = true; 
        
        /**
         * Background to the whole view.
         * @private
         * @type {PIXI.mesh.NineSlicePlane}
         */
        this._screenBg = this.assets.getS9Sprite('wmg_contentBox_white');
        this._screenBg.width = this.layout.relWidth(100);
        this._screenBg.height = this.layout.relHeight(100-((headHeightRatio+footHeightRatio)*100));
        this._screenBg.y = this._headerBg.height;
        this._screenBg.interactive = true;

        /**
         * Background to the view's header area.
         * @private
         * @type {PIXI.Sprite}
         */
        this._footerBg = this.assets.getSprite('cashDeskGreyBottom');
		this._footerBg.width  = this.layout.relWidth(100);
		this._footerBg.height = this.layout.relHeight(100-((footHeightRatio)*100));
		this._footerBg.y = this._headerBg.height+this._screenBg.height;
        this._footerBg.interactive = true; // block lower clicks
        
        let confirmBtn = this.assets.getS9Sprite('wmg_btn');
        confirmBtn.width  = this.layout.relWidth(30);
        confirmBtn.height = this._footerBg.height * 0.17;
        confirmBtn.x = (this._footerBg.width*0.5)-(confirmBtn.width/2);
        confirmBtn.y = this._footerBg.y+(this._footerBg.height * 0.01);
        confirmBtn.interactive = true;
        
        let dynamicFontSize = confirmBtn.height * 0.2;
        let font = "Arial";
        let colour = "#000000"; 
        
        let confirmBtnTxt = this.assets.getTF('Select Url', dynamicFontSize, font, colour, null, null, null);
        let xPos = confirmBtn.x+((confirmBtn.width - confirmBtnTxt.width)/2);
        let yPos = confirmBtn.y+((confirmBtn.height - confirmBtnTxt.height)/2);

        confirmBtnTxt.x = xPos;
        confirmBtnTxt.y = yPos;

        let titleTxt = this.assets.getTF('Select Url', (this._headerBg.height * 0.85), font, "#FFFFFF", null, null, null);
        let titleTxtXPos = this._headerBg.x+((this._headerBg.width - titleTxt.width)/2);
        let titleTxtYPos = this._headerBg.y+((this._headerBg.height - titleTxt.height)/2);

        titleTxt.x = titleTxtXPos;
        titleTxt.y = titleTxtYPos;

        confirmBtnTxt.x = xPos;
        confirmBtnTxt.y = yPos;

        /**
         * Defines the on-screen area for the scroller
         * @private
         */
        this._scrollArea =
        {
            x: 0,
            y: this._headerBg.height,
            width: this.layout.relWidth(100),
            height: this._footerBg.y - this._screenBg.y
        };

        let numSelectionButtonsPerPage = this.platform.isDesktop() ? 6 : 4;
        let buttonHeight = this._scrollArea.height / numSelectionButtonsPerPage;
		let fontSize = Math.round(buttonHeight * 0.25);
		let txtNameY = buttonHeight * 0.1;
        let txtUrlY = buttonHeight * 0.5;
        
        /**
         * Ordered list of all buttons for selecting a specific endpoint.
         * @private
         * @type {PIXI.DisplayObject[]}
         */
        this._selectEndpointButtons = [];
        
        let serverConfigs = this.getServerUrls();
        
        // Create an array of buttons, each one representing a Server endpoint.
		for (let i = 0; i < serverConfigs.length; i ++)
		{
            let serverConfig = serverConfigs[i];

            this.log.debug(`UrlSelection.create(): add testUrls[${i}]:{name:${serverConfig.name},url:${serverConfig.url}}`);

            let selectServer = () => this.selectServerByIndex(i);

            let buttonBg = this.assets.getS9Sprite('wmg_btn');
            buttonBg.width = this.layout.relWidth(100);
            buttonBg.height = buttonHeight;
            buttonBg.interactive = true;
            buttonBg.on(C_PointerEvt.DOWN, selectServer, this);

            let serverIdTextField = this.assets.getTF('Select Url', fontSize, 'Arial', '#0393fd', null, null, null);
            serverIdTextField.x = buttonBg.x + (this.layout.relWidth(100) * 0.05);
            serverIdTextField.y = buttonBg.y + txtNameY;
            serverIdTextField.text = serverConfig.name;
            
            this.log.debug(`UrlSelection.create: text item ${i} is ${serverIdTextField}, item.text = ${serverIdTextField.text}`);

            let txtUrl = this.assets.getTF('Select Url', fontSize, 'Arial', '#444444', null, null, null);
            txtUrl.x = serverIdTextField.x;
            txtUrl.y = buttonBg.y + txtUrlY;
            txtUrl.text = serverConfig.url;

            let buttonContainer = new PIXI.Container();
            buttonContainer.addChild(buttonBg);
            buttonContainer.addChild(serverIdTextField);
            buttonContainer.addChild(txtUrl);
            buttonContainer.y = i * buttonBg.height;

            allServerButtonsContainer.addChild(buttonContainer);

            this._selectEndpointButtons.push(buttonBg);

            // We use onInputUp here specifically - why? It's a hack. The url selection view is shown over the
            // top of the loading screen view. The confirm button usually ends up being shown above the responsible
            // gaming links. The responsible gaming links use onInputUp - because on mobile, that's often required
            // in order to actually open an external link. When the confirm button here uses "onInputDown", for some
            // reason with pixi, the up phase ends up triggering the responsible gaming buttons below immediately
            // after. This is contrary to what i expect - but changing the event used here was the simplest and
            // quickest fix! Russell, 06.02.2020
            confirmBtn.on(C_PointerEvt.UP, this.actionOnConfirmEndpointPressed, this);
        }

        /**
         * Create a scroll hit area, so we can move around the 
         * @private
         * @type {ScrollContainer}
         */
        this._scrollContainer = new ScrollContainer({
            posX : this._scrollArea.x,
            posY : this._scrollArea.y,
            width : this._scrollArea.width,
            height : this._scrollArea.height
        }, allServerButtonsContainer);

        this.addChild(this._screenBg);
        this.addChild(this._scrollContainer);
        this.addChild(this._headerBg);
        this.addChild(this._footerBg);
        this.addChild(confirmBtn);
        this.addChild(confirmBtnTxt);
        this.addChild(titleTxt);

        /**
         * Cached reference to the complete list of available server configs, from which we are selecting.
         * @private
         * @type {TestServerConfig[]}
         */
        this._serverConfigs = serverConfigs;

        /**
         * The index (within the list of avialable server configs) of the server config that has been selected.
         * This also, implicitly, the index of the button which needs to be shown visually as selected.
         * @private
         * @type {number}
         */
        this._selectedServerIndex = 0;

        // Ensure first option is selected by default.
        this.selectServerByIndex(0);
    };


    /**
	 * Fired when the "confirm" button is pressed, selects the current activePointer
	 * url, and proceeds to next application state (main menu).
	 * @private
	 */
	actionOnConfirmEndpointPressed()
	{
        let selectedServerIndex = this._selectedServerIndex;
        let selectedServerConfig = this._serverConfigs[selectedServerIndex];

        this.log.debug(`URLSelection.actionOnConfirmEndpointPressed(${JSON.stringify(selectedServerConfig)})`);

        Comms.setInternalTargetUrl(selectedServerConfig.url);

        // TODO: This event dispatch is a bit confusingly named
        this._dispatcher.emit(C_GameEvent.HIDE_URL_SELECTION_VIEW);

        this.removeURLSelectionScreen();
    };
    

    /**
	 * Callback executed when a server option button is pressed. Updates
	 * selected option to the one that was pressed. Sets BG of selected button
	 * to full alpha, sets bgs of non-selected options to partially transparent.
     * @private
     * @param {number} serverIndex
     * The index of the server endpoint being selected.
	 */
    selectServerByIndex(serverIndex)
	{
        this._selectedServerIndex = serverIndex;

        this.log.debug(`UrlSelection.selectServerOption(index:${serverIndex})`);

        // Now, we need to highlight the appropriate button
        this._selectEndpointButtons.forEach((button, buttonIndex) => {
            button.alpha = buttonIndex === serverIndex? 1.0 : 0.2;
        })
    };


    /**
     * @private
     */
    removeURLSelectionScreen() {
        this.removeFromParent();
    };


    /**
     * Returns the set of test urls which should be used. This currently depends on the build target
     * which is baked into the build.
     * @private
     * @return {TestServerConfig[]}
     */
    getServerUrls() {

        /**
         * We dynamically generate a localhost config, and add that as the first item in our list.
         * @type {TestServerConfig[]}
         */
        let standardServerConfigs =
        [{
            name:"Localhost connection - Test Server App",
            url: `http://${location.hostname}:5000/gameservices`
        }];

        /** @type {TestServerConfig[]} */
        let additionalServerConfigs;

        if (this._businessConfig.urlSelectionView.customTestUrls) {
            additionalServerConfigs = this._businessConfig.urlSelectionView.customTestUrls;
        }
        else
        {
            if (BUILD_TARGET === "v1") {
                additionalServerConfigs = V1_URLS;
            }
            else
            if (BUILD_TARGET === "v2") {
                additionalServerConfigs = V2_URLS;
            }
        }

        return standardServerConfigs.concat(additionalServerConfigs);
    };
}
export default URLSelection