import C_GameEvent from "../../../cms-core-logic/src/const/C_GameEvent";
import BaseView from "./BaseView";
import C_PointerEvt from "../const/C_PointerEvt";
import Button from "../ui/Button";

/**
 * Mobile specific UI. Operates differently based on the orientation of the device - landscape or portrait.
 */
class UIMobile extends BaseView
{
    constructor()
    {
        super();

        this.name = "GuiMobile";

        /**
         * @private
         * @type {MobileGuiConfig}
         */
        this._guiConfig = this._businessConfig.mobileGui;

        if (!this._guiConfig) {
            this.log.error(`UIMobile: GuiConfig is not defined! UI will fall back to basic state`);
        }
        
        /**
         * Safe area for the game logo.
         * @private
         * @type {PIXI.Rectangle}
         */
        this._gameLogoArea = new PIXI.Rectangle(30, 0, 40, 8);

        /**
         * List of all buttons added to the GUI.
         * @private
         */
        this._guiButtons = [];

        this.createUiTexts();
        this.createMessageBar();
        this.createGameLogo();

        if (this._guiConfig)
        {
            let openMenuButtonConfig = this._guiConfig.openMenuButton;
            if (openMenuButtonConfig && openMenuButtonConfig.isEnabled) {
                this.createMenuBtn();
            }

            let closeSessionButtonConfig = this._guiConfig.closeSessionButton;
            if (closeSessionButtonConfig && closeSessionButtonConfig.isEnabled) {
                this.createCloseBtn();
            }
        }

        this.createSpinButton();
        this.createBetButton();
        this.createAutoplayButton();

        this.dispatcher.on(C_GameEvent.DISABLE_GUI_BUTTONS, () => this.setGuiButtonsEnabledTo(false));
        this.dispatcher.on(C_GameEvent.ENABLE_GUI_BUTTONS, () => this.setGuiButtonsEnabledTo(true));
    };

        
    /**
     * Creates the game logo
     * @private
     */
    createGameLogo()
    {
        let targetHeight = this.layout.relHeight(this.config.uiLayoutMobile.gameLogoHeight);
        let targetCenterPosX = this.layout.relWidth(this.config.uiLayoutMobile.gameLogoCenterPosX);
        let targetCenterPosY = this.layout.relHeight(this.config.uiLayoutMobile.gameLogoCenterPosY);

        /**
         * @private
         */
        this._gameLogo = this._gameFactory.getGameLogo(targetCenterPosX, targetCenterPosY, targetHeight);

        this.addChild(this._gameLogo);
    };


    /**
     * Creates the UiTexts component, and adds it to the display list
     * @private
     */
    createUiTexts()
    {
        let uiTexts = this.gameFactory.getUITexts();
        this.addChild(uiTexts);

        // TODO: "create" is not a documented method, it doesn't really need to exist either
        // (or there is no documentation to explain why it does) : would like this tidied up
        uiTexts.create();
        uiTexts.name = "GuiMobile.InfoBar";
    };


    /**
     * Creates the MessageBar component, and adds it to the display list
     * @private
     */
    createMessageBar()
    {
        let messageBar = this.gameFactory.getMessageBar();
        messageBar.create();
        messageBar.name = "GuiMobile.MessageBar";

        // TODO: "create" is not a documented method, it doesn't really need to exist either
        // (or there is no documentation to explain why it does) : would like this tidied up
        this.addChild(messageBar);
    };


    /**
     * Creates the Menu button in the top left of the screen.
     * @private
     */
    createMenuBtn()
    {
        let posY = this.getActiveGuiLayout().uiStats.height;

        let menuBtn = new Button('menuBtn', 'menuBtn_over', 'menuBtn_over', 'menuBtn_disabled');
        this.layout.propHeightScale(menuBtn, 8);
        this.layout.horizontalAlignLeft(menuBtn, .5);
        this.layout.verticalAlignBottom(menuBtn, posY + 0.5);

        menuBtn.name = "GuiMobile.MenuBtn";
        menuBtn.on(C_PointerEvt.DOWN, () => this.onMenuButtonPressed());
        
        this.addChild(menuBtn);

        if (this.app.config.uiSounds.mobile.guiButtonSound)
        {
            let buttonSoundId = this.app.config.uiSounds.mobile.guiButtonSound;
            let buttonVolume = this.app.config.uiSounds.mobile.guiButtonVolume;

            this.log.debug(`OpenMenuButtonMobile: setting sound to ${buttonSoundId}`);

            menuBtn.setClickSound(buttonSoundId, buttonVolume);
        }
        else
        {
            this.log.debug('OpenMenuButtonMobile: no button sound available, button will not make sound when pressed');

            menuBtn.setClickSound(null);
        }

        this._guiButtons.push(menuBtn);
    };


    /**
     * Action invoked when the menu button is pressed.
     * @private
     */
    onMenuButtonPressed()
    {
        this.log.debug('OpenMenuButtonMobile: Pressed');

        this.dispatcher.emit(C_GameEvent.BUTTON_TOGGLE_GAME_MENU_PRESSED);
    };


    /**
     * Creates the Close Button in the top right of the screen.
     * @private
     */
    createCloseBtn()
    {
        let posY = this.getActiveGuiLayout().uiStats.height;

        let closeBtn = new Button('closeBtn', 'closeBtn_over', 'closeBtn_over', 'closeBtn_disabled');
        this.layout.propHeightScale(closeBtn, 8);
        this.layout.horizontalAlignRight(closeBtn, .5);
        this.layout.verticalAlignBottom(closeBtn, posY + 0.5);

        if (this.app.config.uiSounds.mobile.guiButtonSound)
        {
            let buttonSoundId = this.app.config.uiSounds.mobile.guiButtonSound;
            let buttonVolume = this.app.config.uiSounds.mobile.guiButtonVolume;

            this.log.debug(`CloseSessionButtonMobile: setting sound to ${buttonSoundId}`);

            closeBtn.setClickSound(buttonSoundId, buttonVolume);
        }
        else
        {
            this.log.debug('CloseSessionButtonMobile: no button sound available, button will not make sound when pressed');
            
            closeBtn.setClickSound(null);
        }

        closeBtn.name = "GuiMobile.CloseBtn";
        closeBtn.on(C_PointerEvt.DOWN, () => this.onCloseButtonPressed());
        this.addChild(closeBtn);
        this._guiButtons.push(closeBtn);
    };


    /**
     * Action invoked when the Close button is pressed. This method will only be invoked if a Close Session
     * button has been created in the GUI (which is not done if the Close Session button is not configured).
     * @private
     */
    onCloseButtonPressed()
    {
        this.log.debug('CloseSessionButtonMobile: Pressed');

        this.dispatcher.emit(C_GameEvent.BUTTON_CLOSE_SESSION_PRESSED);
    };


    /**
     * @private
     */
    createSpinButton()
    {
        let spinBtn = this.gameFactory.getSpinButtonMobile();
        let posData = this.config.uiLayoutMobile.spinButtonPos;

        // TODO: relying on reported size is sub-optimal.
        spinBtn.scale.set(this.layout.relHeight(posData.height) / spinBtn.height);
        spinBtn.x = this.layout.relWidth(posData.x)  - (spinBtn.width * 0.5);
        spinBtn.y = this.layout.relHeight(posData.y) - (spinBtn.height * 0.5);

        spinBtn.name = "GuiMobile.SpinBtn";

        this.addChild(spinBtn);
    };


    /**
     * @private
     */
    createAutoplayButton()
    {
        let autoBtn = this.gameFactory.getAutoplayButtonMobile();
        let posData = this.config.uiLayoutMobile.autoplayButtonPos;

        // TODO: relying on reported size is sub-optimal.
        autoBtn.scale.set(this.layout.relHeight(posData.height) / autoBtn.height);
        autoBtn.x = this.layout.relWidth(posData.x)  - (autoBtn.width * 0.5);
        autoBtn.y = this.layout.relHeight(posData.y) - (autoBtn.height * 0.5);

        autoBtn.name = "GuiMobile.AutoplayBtn";

        this.addChild(autoBtn);
    };


    /**
     * @private
     */
    createBetButton()
    {
        let betBtn = this.gameFactory.getBetButtonMobile();
        let posData = this.config.uiLayoutMobile.betButtonPos;

        // TODO: relying on reported size is sub-optimal.
        betBtn.scale.set(this.layout.relHeight(posData.height) / betBtn.height);
        betBtn.x = this.layout.relWidth(posData.x)  - (betBtn.width * 0.5);
        betBtn.y = this.layout.relHeight(posData.y) - (betBtn.height * 0.5);

        betBtn.name = "GuiMobile.BetBtn";
        
        this.addChild(betBtn);
    };


    /**
     * Enables or disables button input for the main GUI buttons (menu, close session, bet and autoplay).
     * If a value of true is passed, not all buttons may actually be enabled: the ability of certain inputs
     * to be enabled may depend on other factors. Passing a value of true simply means "it is time for the
     * UI to enable these buttons (if valid)".
     * @private
     * @param {boolean} enabled
     */
    setGuiButtonsEnabledTo(enabled)
    {
        for (let btnIdx = 0; btnIdx < this._guiButtons.length; btnIdx++)
        {
            this._guiButtons[btnIdx].setEnabled(enabled);
            let alpha = enabled ? 1 : 0;
            let button = this._guiButtons[btnIdx];

            TweenMax.killTweensOf(button);
            TweenMax.to(button, 0.3, {alpha:alpha});
        }
    };
}

export default UIMobile