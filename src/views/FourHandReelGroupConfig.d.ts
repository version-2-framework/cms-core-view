/**
 * Configuration settings for the Four Hand Reel Group class.
 */
interface FourHandReelGroupConfig
{
    /**
     * Configuration for reel stop animation on the secondary hands - which currently
     * do not use "spinners", but instead work in a cruder way.
     */
    secondaryHandSpinConfig : FourHandSecondaryReelAnimationConfig;
}

/**
 * Configuration for how the secondary hands will stop spinning.
 */
type FourHandSecondaryReelAnimationConfig =
    FourHandSecondaryReelSimpleStopAnimationConfig |
    FourHandSecondaryReelStaticSpinAnimationConfig;

/**
 * Config which indicates that secondary hand reels should stop in a very simply way -
 * 
 */
interface FourHandSecondaryReelSimpleStopAnimationConfig
{
    type : "simpleStop";
}

/**
 * Spins a secondary hand reel, via a simple static animation.
 */
interface FourHandSecondaryReelStaticSpinAnimationConfig
{
    type : "staticAnim";

    /**
     * Sets the string animation prefix used for frames, eg : "myFrame_"
     */
    animPrefix : string;

    /**
     * Numerical id of start frame
     */
    startFrame : number;

    /**
     * Numerical id of final frame.
     */
    finalFrame : number;

    /**
     * Padding length for animation frame ids
     */
    padLength : number;
}