/**
 * Scene manager.
 * Uses the abstraction of a view - a display object container (BaseView) which has many components inside.
 */
import C_GameEvent from "../../../cms-core-logic/src/const/C_GameEvent";
import MainView from "./MainView";
import C_ViewEvent from "../const/C_ViewEvent";
import FullScreenPrompt from "../ui/FullScreenPrompt";


class ViewManager
{
    constructor()
    {
        /**
         * @private
         */
        this._app = PIXI.app;

        // TODO: Not convined this serves any purpose within this context
        /**
         * @private
         * @type {PIXI.Container}
         */
        this._mainContainer = new PIXI.Container();

        this._app.stage.addChild(this._mainContainer);

        /**
         * @private
         * @type {EventEmitter}
         */
        this._dispatcher = this._app.dispatcher;

        /**
         * @private
         * @type {GameFactory}
         */
        this._gameFactory = this._app.gameFactory;

        /**
         * @private
         */
        this._loadingScreen = null;

        /**
         * @private
         */
        this._mainView = null;

        /**
         * @private
         */
        this._fullScreenPrompt = null;

        this._dispatcher.on(C_GameEvent.SHOW_LOADING_SCREEN, this.onEnterLoadScreenState, this);
        this._dispatcher.on(C_GameEvent.LOADING_SCREEN_COMPLETE, this.onExitLoadScreenState, this);
        this._dispatcher.once(C_ViewEvent.CREATE_FULLSCREEN_PROMPT, this.createFullScreenPrompt, this);
        this._dispatcher.once(C_GameEvent.ALL_GAME_ASSETS_AVAILABLE, this.onGameAssetsAvailable, this);
    };


    /**
     * Creates the loading screen, and shows it.
     * @private
     */
    onEnterLoadScreenState()
    {
        this._loadingScreen = this._gameFactory.getLoadingScreenView();
        this._loadingScreen.create();
        this._mainContainer.addChild(this._loadingScreen);
    };


    /**
     * Creates the "swipe for full screen" animated graphic.
     * @private
     */
    createFullScreenPrompt()
    {
        this._fullScreenPrompt = new FullScreenPrompt();
        this._app.stage.addChild(this._fullScreenPrompt);
    };


    /**
     * Callback executed when we exit loading screen state.
     * @private
     */
    onExitLoadScreenState()
    {
        this._app.stage.removeChild(this._loadingScreen);
        this._loadingScreen = null;

        this._mainView = this._gameFactory.getMainView();
        this._mainContainer.addChild(this._mainView);
    };


    /**
     * @private
     */
    onGameAssetsAvailable() {
        // Once game assets are available, we are able to show a dialog view.
        this._dispatcher.on(C_GameEvent.SHOW_DIALOG_VIEW, this.showDialog, this);
    };


    /**
     * @private
     * @param {DialogViewModel} dialogViewModel
     */
    showDialog(dialogViewModel) {
        let dialogView = this._gameFactory.getDialog(dialogViewModel);
        this._mainContainer.addChild(dialogView);
    };


    //--------------------------------------------------------------------------------------------------
    // Utility methods
    //--------------------------------------------------------------------------------------------------
    /**
     * "Diagnostic method"
     * @private
     */
    runDiag()
    {
        console.log(`Num children : ${this.getNumChildren(this._app.stage)}`);
    }


    // TODO: This method includes the parent in the number (its also recursive, which might
    // not work well for large display lists). We are not currently using it, but it could do
    // with getting tidied up at some point.
    /**
     * Diagnostic method
     * @private
     * @param {PIXI.Container} parent
     * @returns {number}
     */
    getNumChildren(parent)
    {
        let numChildren = 1;
        if (parent.children)
        {
            for (let childIdx = 0; childIdx < parent.children.length; childIdx++)
            {
                numChildren += this.getNumChildren(parent.children[childIdx]);
            }
        }
        return numChildren;
    };
}


export default ViewManager;