class PrecacheView extends PIXI.Sprite
{

    constructor(width, height)
    {
        super();
        this.renderable = false;
        this.texturesArray = [];
        this.maxWidth = width;
        this.maxHeight = height;
    }


    add(texture)
    {
        this.texturesArray.push(texture);
    }


    next()
    {
        this.texture = this.texturesArray.pop();
        this.width = this.maxWidth;
        this.height = this.maxHeight;
        this.renderable = true;
    }


    get completed()
    {
        return this.texturesArray.length === 0;
    }


    renderWebGL(renderer)
    {
        super.renderWebGL(renderer);
        if (this.texturesArray.length > 0)
        {
            this.next();
        }
        else
        {
            this.renderable = false;
            this.emit("COMPLETE");
        }
    }

}

export default PrecacheView