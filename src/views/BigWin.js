import BaseView from "./BaseView";
import C_GameEvent from "../../../cms-core-logic/src/const/C_GameEvent";
import C_PointerEvt from "../const/C_PointerEvt";
import C_BigWinViewEvent from "../const/C_BigWinViewEvent";


/**
 * Big Win overlay
 */
class BigWin extends BaseView
{


    constructor()
    {
        super();
    }


    /**
     * @public
     */
    create()
    {
        this.bgShade = this.assets.getSprite("bg_shade");
        this.layout.setToRectangle(this.bgShade, 0, 0, 100, 100);
        this.bgShade.alpha = .5;
        this.bgShade.interactive = true;
        this.bgShade.once(C_PointerEvt.DOWN, this.endBigWin, this);
        this.addChild(this.bgShade);

        this.title = this.assets.getTF("BIG WIN\n" + this.model.getWinnings(),
            this.layout.relHeight(10), this.config.bigWinFont, "#FFBF20");
        this.title.anchor.set(.5, .5);
        this.layout.horizontalAlignCenter(this.title);
        this.layout.verticalAlignMiddle(this.title);
        this.addChild(this.title);

        let duration = this.config.bigWinDuration || 1; 
        TweenMax.delayedCall(duration, () => this.endBigWin());
    }


    /**
     * @private
     */
    endBigWin()
    {
        this.emit(C_BigWinViewEvent.COMPLETE);
        this.destroy();
        this.removeFromParent();
    };
}

export default BigWin;