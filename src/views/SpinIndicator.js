import BaseView from "./BaseView";
import C_GameEvent from "../../../cms-core-logic/src/const/C_GameEvent";
import C_SpinPhase from "../../../cms-core-logic/src/const/C_SpinPhase";

class SpinIndicator extends BaseView
{
    constructor()
    {
        super();

        /**
         * @private
         * @type {SpinPhaseType}
         */
        this._spinPhase = null;

        /**
         * @type {SpinIndicatorConfig}
         */
        let spinIndicatorConfig = this.platform.isDesktop() ? this.config.spinIndicatorDesktop : this.config.spinIndicatorMobile;

        /**
         * @private
         * @type {SpinIndicatorConfig}
         */
        this._spinIndicatorConfig = spinIndicatorConfig;

        /**
         * The acutal spin indicator icon.
         * @private
         * @type {PIXI.Sprite}
         */
        this._spinIndicator = this.assets.getSprite("spin1_indicator");
        this._spinIndicator.anchor.set(0.5);
        this._spinIndicator.x = spinIndicatorConfig.posX;
        this._spinIndicator.y = spinIndicatorConfig.posY;
        this._spinIndicator.alpha = 0;

        this.layout.setHeight(this._spinIndicator, spinIndicatorConfig.height);

        this.addChild(this._spinIndicator);

        this.dispatcher.on(C_GameEvent.SHOW_SPIN_INDICATOR, this.showSpinIndicator, this);
        this.dispatcher.on(C_GameEvent.HIDE_SPIN_INDICATOR, this.hideSpinIndicator, this);
    };


    /**
     * @private
     * @param {SpinPhaseType} newSpinPhase 
     */
    showSpinIndicator(newSpinPhase) {
        let textureId = null;
        let isTwoSpin = this.config.isTwoSpin;

        if (isTwoSpin && newSpinPhase === C_SpinPhase.SPIN_1) {
            textureId = "spin1_indicator";
        }
        else
        if (isTwoSpin && newSpinPhase === C_SpinPhase.SPIN_2) {
            textureId = "spin2_indicator";
        }
        else
        if (newSpinPhase === C_SpinPhase.FREESPIN) {
            textureId = "freespin_indicator";
        }

        let texture = textureId? this.assets.getTexture(textureId) : null;

        if (texture) {
            if (this._spinPhase !== newSpinPhase) {
                this._spinPhase = newSpinPhase;
                this.showNewSpinIndicator(texture);
            }
            // if we are told to show the spin indicator, but we already were showing
            // the correct spin indicator for the current spin state, then we don't need
            // to do anything: we simply keep the indicator visible on screen
        }
        // if no texture id assigned, clear the indicator. There is either an error
        // (bad spin state), or maybe this is a single spin game (so we skip showing the
        // spin1 / spin2 indicators)
        else
        {
            this.killAnyExistingTweens();
            this._spinIndicator.visible = false;
        }
    };


    /**
     * @private
     */
    hideSpinIndicator() {
        if (this._spinPhase !== null) {
            this._spinPhase = null;
            this.killAnyExistingTweens();
            TweenMax.to(this._spinIndicator, 0.1, {alpha:0, overwrite:1});
        }
    };


    /**
     * @private
     */
    killAnyExistingTweens()
    {
        TweenMax.killTweensOf(this._spinIndicator);
        TweenMax.killTweensOf(this._spinIndicator.scale);
    };
    

    /**
     * @private
     * @param {PIXI.Texture} texture 
     */
    showNewSpinIndicator(texture)
    {
        this.killAnyExistingTweens();
        
        this._spinIndicator.texture = texture;
        this._spinIndicator.alpha = 1;

        let spinIndicatorConfig = this._spinIndicatorConfig;

        // Make the indicator appear with animated transition
        this.layout.setHeight(this._spinIndicator, spinIndicatorConfig.height);
        
        let startScale = this._spinIndicator.scale.x * 1.75;

        TweenMax.from(this._spinIndicator.scale, 0.4, {x:startScale, y:startScale, overwrite:1});
        TweenMax.from(this._spinIndicator, 0.1, {alpha:0, overwrite:1});
    };
}

export default SpinIndicator