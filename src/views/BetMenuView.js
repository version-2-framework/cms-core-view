import BaseView from "./BaseView";
import C_PointerEvt from "../const/C_PointerEvt";
import Button from "../ui/Button";
import C_GameEvent from "../../../cms-core-logic/src/const/C_GameEvent";
import ScrollingSelector from "../ui/ScrollingSelector";
import {Events as ScrollingSelectorEvents} from "../ui/ScrollingSelector";

const HEADER_HEIGHT = 10;
const FOOTER_HEIGHT = 14;

const TEXTURE_ID_BTN_DECREMENT = "selectorArrow";
const TEXTURE_ID_BTN_INCREMENT = "selectorArrow";

const TEXT_DROPSHADOW_ANGLE = Math.PI/180 * 60;
const TEXT_DROPSHADOW_ALPHA = 0.75;
const TEXT_DROPSHADOW_COLOUR = 0x000000;
const TEXT_FILL_WHITE = [ 0xffffff, 0xdddddd ];
const TEXT_FILL_BLUE = [ 0x66b8fe, 0x008afe ];

class BetMenuView extends BaseView
{
    constructor()
    {
        super();

        /**
         * Configuration for all possible bet settings. This drives what we see on the screen.
         * @private
         * @type {BetSettingsConfig}
         */
        this._betConfig = this._config.betConfig;

        /**
         * Indicates if the bet selector title text has been enabled
         * @private
         * @type {boolean}
         */
        this._betSelectorTitleTxtEnabled = true; 

        if (this._betConfig.removeBetSelectorTitleText !== undefined)
        {
            this._betSelectorTitleTxtEnabled = this._betConfig.removeBetSelectorTitleText ? false : true;
        }

        let bg = this.assets.getSprite('popup_bg');
        this.layout.setToRectangle(bg, 0, 0, 100, 100);
        bg.alpha = 0.9;
        bg.interactive = true;
        this.addChild(bg);

        /**
         * @private
         * @type {PIXI.Text}
         */
        this._headerText = new PIXI.Text(
            this.locale.getString('T_betMenu_title'),
            {
                fontFamily : "Arial",
                fontSize : Math.round(this.layout.relHeight(8)),
                fill : TEXT_FILL_BLUE,
                dropShadow : true,
                dropShadowAlpha : TEXT_DROPSHADOW_ALPHA,
                dropShadowAngle: TEXT_DROPSHADOW_ANGLE,
                dropShadowColor : TEXT_DROPSHADOW_COLOUR,
                dropShadowDistance : Math.round(this.layout.relHeight(0.8))
            });
        this._headerText.anchor.set(0.5, 0);

        this.layout.horizontalAlignCenter(this._headerText, 0, bg);
        this.layout.verticalAlignTop(this._headerText, 2, bg);
        this.addChild(this._headerText);

        // TODO: need to dynamically calculate a couple of constants, to ease the layout pains.

        // Dynamically pre-calculate some general layout values. Because the content of the BetMenu
        // changes from game to game, there are several possible configurations.
        let settingsStartPosY = this.layout.relHeight(10);
        let settingTitleHeight = Math.round(this.layout.relHeight(5));
        let settingTitleDropshadowDistance = Math.round(settingTitleHeight/10);
        let settingSelectorWidth = this.layout.relWidth(80);
        let settingSelectorHeight = this.layout.relHeight(10);
        let settingFooterHeight = Math.round(this.layout.relHeight(5))
        let settingFooterDropshadowDistance = Math.round(settingFooterHeight/10);
        let settingsMaxContentHeight = this.layout.relHeight(100 - HEADER_HEIGHT - FOOTER_HEIGHT);
        let settingSelectorBtnWidth = this.layout.relWidth(3);

        /** @type {PIXI.TextStyleOptions} */
        let settingTitleTextStyle =
        {
            fontFamily : "Arial",
            fontSize : settingTitleHeight,
            fill : TEXT_FILL_BLUE,
            dropShadow: true,
            dropShadowAlpha: TEXT_DROPSHADOW_ALPHA,
            dropShadowAngle: TEXT_DROPSHADOW_ANGLE,
            dropShadowColor: TEXT_DROPSHADOW_COLOUR,
            dropShadowDistance: settingTitleDropshadowDistance
        }

        /** @type {ScrollingSelectorDimensions} */
        let selectorDimensions = { width:settingSelectorWidth, height:settingSelectorHeight, useExactWidth:false };

        // Now we can calculate the height of the individual setting sections
        let heightStakePerHand = settingTitleHeight + settingSelectorHeight + settingFooterHeight;
        let heightNumHands = settingTitleHeight + settingSelectorHeight;
        let heightNumLines = settingTitleHeight + settingSelectorHeight;
        let heightBetPerLine = settingTitleHeight + settingSelectorHeight;
        let heightSpecialWagerMultiplier = settingTitleHeight + settingSelectorHeight;

        // Determine which items should be added to the Bet Menu. For each possible
        // bet setting, we only add a selector if there is a config for that setting,
        // and there is more than 1 available value to select.
        let addStakePerHandSelector = false;
        let addBetPerLineSelector = false;
        let addNumLinesSelector = false;
        let addNumHandsSelector = false;
        let addSpecialWagerMultiplierSelector = false;

        if (this._betConfig.stakePerHand) {
            let stakePerHandConfig = this._betConfig.stakePerHand;
            if (stakePerHandConfig.length > 1) {
                addStakePerHandSelector = true;
            }
        }
        else
        {
            if (this._betConfig.numLines) {
                let numLinesConfig = this._betConfig.numLines;
                if ((numLinesConfig.isEnumerated && numLinesConfig.values.length > 1) ||
                    (numLinesConfig.isEnumerated == false && numLinesConfig.maxValue > numLinesConfig.minValue)) {
                    addNumLinesSelector = true;
                }
            }

            if (this._betConfig.betPerLine) {
                let betPerLineConfig = this._betConfig.betPerLine;
                if ((betPerLineConfig.isEnumerated && betPerLineConfig.values.length > 1) ||
                    (betPerLineConfig.isEnumerated == false && betPerLineConfig.maxValue > betPerLineConfig.minValue)) {
                    addBetPerLineSelector = true;
                }
            }
        }

        if (this._betConfig.numHands) {
            let numHandsConfig = this._betConfig.numHands;
            if ((numHandsConfig.isEnumerated && numHandsConfig.values.length > 1) ||
                (numHandsConfig.isEnumerated == false && numHandsConfig.maxValue > numHandsConfig.minValue)) {
                addNumHandsSelector = true;
            }
        }

        if (this._betConfig.specialWagerMultiplier) {
            let specialWagerMultiplier = this._betConfig.specialWagerMultiplier;
            if ((specialWagerMultiplier.isEnumerated && specialWagerMultiplier.values.length > 1) ||
                (specialWagerMultiplier.isEnumerated === false && specialWagerMultiplier.maxValue > specialWagerMultiplier.minValue) ||
                (specialWagerMultiplier.isBinarySetting && specialWagerMultiplier.onValue > 1)) {
                    addSpecialWagerMultiplierSelector = true;
                }
        }


        // Calculate the total height of all settings (excluding any padding)
        let numSettingsActive = 0;
        let settingsTotalHeight = 0;
        if (addStakePerHandSelector) {
            settingsTotalHeight += heightStakePerHand;
            numSettingsActive += 1;
        }
        else {
            if (addBetPerLineSelector) {
                settingsTotalHeight += heightBetPerLine;
                numSettingsActive += 1;
            }

            if (addNumLinesSelector) {
                settingsTotalHeight += heightNumLines;
                numSettingsActive += 1;
            }
        }
        if (addNumHandsSelector) {
            settingsTotalHeight += heightNumHands;
            numSettingsActive += 1;
        }
        if (addSpecialWagerMultiplierSelector) {
            settingsTotalHeight += heightSpecialWagerMultiplier
            numSettingsActive += 1;
        }

        // Now determine padding required, and positions for each setting
        let settingsPadY = (settingsMaxContentHeight - settingsTotalHeight) / (numSettingsActive + 1);
        let settingsCurrPosY = settingsStartPosY + settingsPadY;

        // StakePerHand means that BetPerLine & NumLines are linked
        // In this mode, we will show special information to indicate
        // this (a slider to chose StakePerHand, extra info to indicate
        // the associated values of BetPerLine & NumLines). Also note:
        // if "NumHands" is not an available BetSetting, then we should
        // print a different title for StakePerHand (its semantically
        // correct to call it StakePerHand, but it's more user friendly
        // to label it as "Stake"
        if (addStakePerHandSelector)
        {
            let titleText = this.locale.getString(this._betConfig.numHands? "T_betMenu_stakePerHand" : "T_betMenu_stake");
            let titleField = new PIXI.Text(titleText, settingTitleTextStyle);

            let stakePerHandOptions = (() => {
                let options = [];
                this._betConfig.stakePerHand.forEach(stakePerHand => {
                    options.push(stakePerHand.numLines * stakePerHand.betPerLine);
                })
                return options;
            })();

            let currStakePerHandValue = this._model.getBetPerHand();
            let currStakePerHandIndex = stakePerHandOptions.indexOf(currStakePerHandValue);
            if (currStakePerHandIndex < 0) {
                currStakePerHandIndex = 0;
            }

            let stakePerHandSelector = new ScrollingSelector(selectorDimensions, stakePerHandOptions, currStakePerHandIndex);
            let btnDecrementStakePerHand = this.assets.getSprite(TEXTURE_ID_BTN_DECREMENT);
            let btnIncrementStakePerHand = this.assets.getSprite(TEXTURE_ID_BTN_INCREMENT);
            btnIncrementStakePerHand.scale.x *= -1;

            let stakePerHandBreakdownText = new PIXI.Text("", {
                fontFamily : "arial",
                fontWeight : "normal",
                fontSize : settingFooterHeight,
                fill : TEXT_FILL_WHITE,
                dropShadow: true,
                dropShadowAlpha: TEXT_DROPSHADOW_ALPHA,
                dropShadowAngle: TEXT_DROPSHADOW_ANGLE,
                dropShadowColor: TEXT_DROPSHADOW_COLOUR,
                dropShadowDistance: settingFooterDropshadowDistance
            });
            stakePerHandBreakdownText.anchor.set(0.5, 0);

            /**
             * Updates the breakdown text shown beneath the Stake per Hand selector. This text tells
             * the player the values of Stake per Line / Num Lines associated with the Stake per Hand
             * setting.
             */
            let updateStakePerHandBreakdown = () => {
                let stakePerHand = this._betConfig.stakePerHand[stakePerHandSelector.getSelectedValueIndex()];
                stakePerHandBreakdownText.text = this.locale.getString("T_betMenu_stakePerHand_breakdown", {
                    "[NUM_LINES]" : stakePerHand.numLines,
                    "[STAKE_PER_LINE]" : stakePerHand.betPerLine
                });
            };

            this.layout.setWidth(btnDecrementStakePerHand, settingSelectorBtnWidth);
            this.layout.setWidth(btnIncrementStakePerHand, settingSelectorBtnWidth);

            titleField.x = this.layout.relWidth(50) - (0.5 * titleField.width);
            stakePerHandSelector.x = this.layout.relWidth(50) - (stakePerHandSelector.getVisibleWidth() * 0.5);
            stakePerHandBreakdownText.x = this.layout.relWidth(50);
            btnDecrementStakePerHand.x = stakePerHandSelector.x - btnDecrementStakePerHand.width - this.layout.relWidth(2);
            btnIncrementStakePerHand.x = stakePerHandSelector.x + stakePerHandSelector.getVisibleWidth() + this.layout.relWidth(2) + btnIncrementStakePerHand.width;

            titleField.y = settingsCurrPosY;
            stakePerHandSelector.y = titleField.y + settingTitleHeight;
            stakePerHandBreakdownText.y = stakePerHandSelector.y + settingSelectorHeight;
            this.layout.verticalAlignMiddle(btnDecrementStakePerHand, 0, stakePerHandSelector);
            this.layout.verticalAlignMiddle(btnIncrementStakePerHand, 0, stakePerHandSelector);

            // Update running y position
            settingsCurrPosY += heightStakePerHand + settingsPadY;

            if (this._betSelectorTitleTxtEnabled)
            {
                this.addChild(titleField);
            }   
            this.addChild(stakePerHandSelector);
            this.addChild(stakePerHandBreakdownText);
            this.addChild(btnDecrementStakePerHand);
            this.addChild(btnIncrementStakePerHand);

            updateStakePerHandBreakdown();

            stakePerHandSelector.on(ScrollingSelectorEvents.SELECTED_VALUE_CHANGED, () => {
                let selectedIndex = stakePerHandSelector.getSelectedValueIndex();
                let stakePerHand = stakePerHandOptions[selectedIndex];
                
                this._model.setNumLines(this._betConfig.stakePerHand[selectedIndex].numLines);
                this._model.setBetPerLine(this._betConfig.stakePerHand[selectedIndex].betPerLine);
                updateStakePerHandBreakdown();
            });

            btnDecrementStakePerHand.interactive = true;
            btnDecrementStakePerHand.on(C_PointerEvt.DOWN, ()=>stakePerHandSelector.selectPrevOptionValue());

            btnIncrementStakePerHand.interactive = true;
            btnIncrementStakePerHand.on(C_PointerEvt.DOWN, ()=>stakePerHandSelector.selectNextOptionValue());
        }
        // Otherwise, we might have separate values available for
        // StakePerLaine & NumLines. So, configured independent
        // controls for the both.
        else
        {
            if (addBetPerLineSelector)
            {
                let titleText = this.locale.getString("T_betMenu_stakePerLine");
                let titleField = new PIXI.Text(titleText, settingTitleTextStyle);

                let betPerLineOptions = this.getPossibleValuesFor(this._betConfig.betPerLine);

                let currBetPerLineValue = this._model.getBetPerLine();
                let currBetPerLineIndex = betPerLineOptions.indexOf(currBetPerLineValue);
                if (currBetPerLineIndex < 0) {
                    currBetPerLineIndex = 0;
                }

                let betPerLineSelector = new ScrollingSelector(selectorDimensions, betPerLineOptions, currBetPerLineIndex);
                let btnDecrementBetPerLine = this.assets.getSprite(TEXTURE_ID_BTN_DECREMENT);
                let btnIncrementBetPerLine = this.assets.getSprite(TEXTURE_ID_BTN_INCREMENT);
                btnIncrementBetPerLine.scale.x *= -1;

                this.layout.setWidth(btnDecrementBetPerLine, settingSelectorBtnWidth);
                this.layout.setWidth(btnIncrementBetPerLine, settingSelectorBtnWidth);

                titleField.x = this.layout.relWidth(50) - (titleField.width * 0.5);
                betPerLineSelector.x = this.layout.relWidth(50) - (betPerLineSelector.getVisibleWidth() * 0.5);
                btnDecrementBetPerLine.x = betPerLineSelector.x - btnDecrementBetPerLine.width - this.layout.relWidth(2);
                btnIncrementBetPerLine.x = betPerLineSelector.x + betPerLineSelector.getVisibleWidth() + this.layout.relWidth(2) + btnIncrementBetPerLine.width;

                titleField.y = settingsCurrPosY;
                betPerLineSelector.y = titleField.y + settingTitleHeight;
                this.layout.verticalAlignMiddle(btnDecrementBetPerLine, 0, betPerLineSelector);
                this.layout.verticalAlignMiddle(btnIncrementBetPerLine, 0, betPerLineSelector);

                // Update running y position
                settingsCurrPosY += heightBetPerLine + settingsPadY;

                if (this._betSelectorTitleTxtEnabled)
                {
                    this.addChild(titleField);
                }  
                this.addChild(betPerLineSelector);
                this.addChild(btnDecrementBetPerLine);
                this.addChild(btnIncrementBetPerLine);

                betPerLineSelector.on(ScrollingSelectorEvents.SELECTED_VALUE_CHANGED, () => {
                    let selectedIndex = betPerLineSelector.getSelectedValueIndex();
                    let betPerLine = betPerLineOptions[selectedIndex];
                    this._model.setBetPerLine(betPerLine);
                });

                btnDecrementBetPerLine.interactive = true;
                btnDecrementBetPerLine.on(C_PointerEvt.DOWN, ()=>betPerLineSelector.selectPrevOptionValue());

                btnIncrementBetPerLine.interactive = true;
                btnIncrementBetPerLine.on(C_PointerEvt.DOWN, ()=>betPerLineSelector.selectNextOptionValue());
            }

            if (addNumLinesSelector)
            {
                let posY = this.layout.relHeight(40);

                let titleText = this.locale.getString("T_betMenu_numLines");
                let titleField = new PIXI.Text(titleText, settingTitleTextStyle);

                let numLinesOptions = this.getPossibleValuesFor(this._betConfig.numLines);

                let currNumLinesValue = this._model.getNumLines();
                let currNumLinesIndex = numLinesOptions.indexOf(currNumLinesValue);
                if (currNumLinesIndex < 0) {
                    currNumLinesIndex = 0;
                }
                
                let numLinesSelector = new ScrollingSelector(selectorDimensions, numLinesOptions, currNumLinesIndex);
                let btnDecrementNumLines = this.assets.getSprite(TEXTURE_ID_BTN_DECREMENT);
                let btnIncrementNumLines = this.assets.getSprite(TEXTURE_ID_BTN_INCREMENT);
                btnIncrementNumLines.scale.x *= -1;

                this.layout.setWidth(btnDecrementNumLines, settingSelectorBtnWidth);
                this.layout.setWidth(btnIncrementNumLines, settingSelectorBtnWidth);

                titleField.x = this.layout.relWidth(50) - (titleField.width * 0.5);
                numLinesSelector.x = this.layout.relWidth(50) - (numLinesSelector.getVisibleWidth() * 0.5);
                btnDecrementNumLines.x = numLinesSelector.x - btnDecrementNumLines.width - this.layout.relWidth(2);
                btnIncrementNumLines.x = numLinesSelector.x + numLinesSelector.getVisibleWidth() + this.layout.relWidth(2) + btnIncrementNumLines.width;

                titleField.y = settingsCurrPosY;
                numLinesSelector.y = titleField.y + settingTitleHeight;
                this.layout.verticalAlignMiddle(btnDecrementNumLines, 0, numLinesSelector);
                this.layout.verticalAlignMiddle(btnIncrementNumLines, 0, numLinesSelector);

                // Update running y position
                settingsCurrPosY += heightNumLines + settingsPadY;

                if (this._betSelectorTitleTxtEnabled)
                {
                    this.addChild(titleField);
                }  
                this.addChild(numLinesSelector);
                this.addChild(btnDecrementNumLines);
                this.addChild(btnIncrementNumLines);

                numLinesSelector.on(ScrollingSelectorEvents.SELECTED_VALUE_CHANGED, () => {
                    let selectedIndex = numLinesSelector.getSelectedValueIndex();
                    let numLines = numLinesOptions[selectedIndex];
                    this._model.setNumLines(numLines);
                });

                btnDecrementNumLines.interactive = true;
                btnDecrementNumLines.on(C_PointerEvt.DOWN, ()=>numLinesSelector.selectPrevOptionValue());

                btnIncrementNumLines.interactive = true;
                btnIncrementNumLines.on(C_PointerEvt.DOWN, ()=>numLinesSelector.selectNextOptionValue());
            }
        }

        // In addition, we *may* let the player select a Number of Hands. Most games use a single
        // hand - some might support more than 1 (4 is a common value!)
        if (addNumHandsSelector)
        {
            this.log.debug(`BetMenu: add NumHands option. Settings = ${JSON.stringify(this._betConfig.numHands)}`);

            // TODO: position the text better vertically
            let titleText = this.locale.getString("T_betMenu_numHands");
            let titleField = new PIXI.Text(titleText, settingTitleTextStyle);

            let numHandsOptions = this.getPossibleValuesFor(this._betConfig.numHands);

            let currNumHandsValue = this._model.getNumHands();
            let currNumHandsIndex = numHandsOptions.indexOf(currNumHandsValue);
            if (currNumHandsIndex < 0) {
                currNumHandsIndex = 0;
            }

            let numHandsSelector = new ScrollingSelector(selectorDimensions, numHandsOptions, currNumHandsIndex);
            let btnDecrementNumHands = this.assets.getSprite(TEXTURE_ID_BTN_DECREMENT);
            let btnIncrementNumHands = this.assets.getSprite(TEXTURE_ID_BTN_INCREMENT);
            btnIncrementNumHands.scale.x *= -1;

            this.layout.setWidth(btnDecrementNumHands, settingSelectorBtnWidth);
            this.layout.setWidth(btnIncrementNumHands, settingSelectorBtnWidth);

            titleField.x = this.layout.relWidth(50) - (titleField.width * 0.5);
            numHandsSelector.x = this.layout.relWidth(50) - (numHandsSelector.getVisibleWidth() * 0.5);
            btnDecrementNumHands.x = numHandsSelector.x - btnDecrementNumHands.width - this.layout.relWidth(2);
            btnIncrementNumHands.x = numHandsSelector.x + numHandsSelector.getVisibleWidth() + this.layout.relWidth(2) + btnIncrementNumHands.width;

            titleField.y = settingsCurrPosY;
            numHandsSelector.y = titleField.y + settingTitleHeight;
            this.layout.verticalAlignMiddle(btnDecrementNumHands, 0, numHandsSelector);
            this.layout.verticalAlignMiddle(btnIncrementNumHands, 0, numHandsSelector);

            // Update running y position
            settingsCurrPosY += heightNumHands + settingsPadY;

            if (this._betSelectorTitleTxtEnabled)
            {
                this.addChild(titleField);
            }  
            this.addChild(numHandsSelector);
            this.addChild(btnDecrementNumHands);
            this.addChild(btnIncrementNumHands);

            numHandsSelector.on(ScrollingSelectorEvents.SELECTED_VALUE_CHANGED, () => {
                let selectedIndex = numHandsSelector.getSelectedValueIndex();
                let numHands = numHandsOptions[selectedIndex];
                this._model.setNumHands(numHands);
            });

            btnDecrementNumHands.interactive = true;
            btnDecrementNumHands.on(C_PointerEvt.DOWN, ()=>numHandsSelector.selectPrevOptionValue());

            btnIncrementNumHands.interactive = true;
            btnIncrementNumHands.on(C_PointerEvt.DOWN, ()=>numHandsSelector.selectNextOptionValue());
        }

        if (addSpecialWagerMultiplierSelector)
        {
            let isBooleanOption = this._betConfig.specialWagerMultiplier.isBinarySetting;
            
            let specialWagerMultiplierValues = this.getPossibleValuesFor(this._betConfig.specialWagerMultiplier);
            
            // The actual values we present to the player (could be real vlaues, could be on / off)
            let specialWagerMultiplierOptions = specialWagerMultiplierValues;
            if (isBooleanOption) {
                specialWagerMultiplierOptions = [
                    this.locale.getString("T_betMenu_SettingDisabled"),
                    this.locale.getString("T_betMenu_SettingEnabled")
                ]
            }

            this.log.debug(`BetMenu: add SpecialWagerMultiplier option. Settings = ${JSON.stringify(this._betConfig.specialWagerMultiplier)}`);

            // TODO: position the text better vertically
            let titleText = this.locale.getString("T_betMenu_specialWagerMultiplier");
            let titleField = new PIXI.Text(titleText, settingTitleTextStyle);

            let currSpecialWagerValue = this._model.getSpecialWagerMultiplier();
            let currSpecialWagerIndex = specialWagerMultiplierValues.indexOf(currSpecialWagerValue);
            if (currSpecialWagerIndex < 0) {
                currSpecialWagerIndex = 0;
            }

            let specialWagerSelector = new ScrollingSelector(selectorDimensions, specialWagerMultiplierOptions, currSpecialWagerIndex);
            let btnDecrementSpecialWager = this.assets.getSprite(TEXTURE_ID_BTN_DECREMENT);
            let btnIncrementSpecialWager = this.assets.getSprite(TEXTURE_ID_BTN_INCREMENT);
            btnIncrementSpecialWager.scale.x *= -1;

            this.layout.setWidth(btnDecrementSpecialWager, settingSelectorBtnWidth);
            this.layout.setWidth(btnIncrementSpecialWager, settingSelectorBtnWidth);

            titleField.x = this.layout.relWidth(50) - (titleField.width * 0.5);
            specialWagerSelector.x = this.layout.relWidth(50) - (specialWagerSelector.getVisibleWidth() * 0.5);
            btnDecrementSpecialWager.x = specialWagerSelector.x - btnDecrementSpecialWager.width - this.layout.relWidth(2);
            btnIncrementSpecialWager.x = specialWagerSelector.x + specialWagerSelector.getVisibleWidth() + this.layout.relWidth(2) + btnIncrementSpecialWager.width;

            titleField.y = settingsCurrPosY;
            specialWagerSelector.y = titleField.y + settingTitleHeight;
            this.layout.verticalAlignMiddle(btnDecrementSpecialWager, 0, specialWagerSelector);
            this.layout.verticalAlignMiddle(btnIncrementSpecialWager, 0, specialWagerSelector);

            // Update running y position
            settingsCurrPosY += heightNumHands + settingsPadY;

            if (this._betSelectorTitleTxtEnabled)
            {
                this.addChild(titleField);
            } 
            this.addChild(specialWagerSelector);
            this.addChild(btnDecrementSpecialWager);
            this.addChild(btnIncrementSpecialWager);

            specialWagerSelector.on(ScrollingSelectorEvents.SELECTED_VALUE_CHANGED, () => {
                let selectedIndex = specialWagerSelector.getSelectedValueIndex();
                let specialWagerMultiplier = specialWagerMultiplierValues[selectedIndex];
                this._model.setSpecialWagerMultiplier(specialWagerMultiplier);
            });

            btnDecrementSpecialWager.interactive = true;
            btnDecrementSpecialWager.on(C_PointerEvt.DOWN, ()=>specialWagerSelector.selectPrevOptionValue());

            btnIncrementSpecialWager.interactive = true;
            btnIncrementSpecialWager.on(C_PointerEvt.DOWN, ()=>specialWagerSelector.selectNextOptionValue());
        }

        this.createStandardGuiBtns();

        // Finally, listen for model's "total bet updated" message, and
        // update the title text at the top of the BetMenu (which indicates
        // current total stake for a game)
        this._dispatcher.on(C_GameEvent.BET_SETTINGS_CHANGED, this.updateTotalBetText, this);
        this._dispatcher.on(C_GameEvent.HIDE_BET_MENU, this.closeBetMenu, this);

        this.updateTotalBetText();
    };


    /**
     * @private
     * @param {ScrollingSelectorDimensions} selectorDimensions
     * @param {number[]} allBetSettingValues
     * @return {PIXI.Container}
     */
    /*
    getNewSelector(selectorDimensions, allBetSettingValues, titleFieldLocalizationId)
    {
        let allBetSettingValues = this.getPossibleValuesFor(this._betConfig.specialWagerMultiplier);

        let container = new PIXI.Container();
        
        // TODO: position the text better vertically
        let titleText = this.locale.getString("T_betMenu_specialWagerMultiplier");
        let titleField = new PIXI.Text(titleText, {
            fontFamily : "arial",
            fontSize : settingTitleHeight,
            fontWeight : "normal",
            fill : 0x008afe
        });

            let currSpecialWagerValue = this._model.getNumHands();
            let currSpecialWagerIndex = allBetSettingValues.indexOf(currSpecialWagerValue);
            if (currSpecialWagerIndex < 0) {
                currSpecialWagerIndex = 0;
            }

            let selector = new ScrollingSelector(selectorDimensions, allBetSettingValues, currSpecialWagerIndex);
            let btnDecrementSetting = this.assets.getSprite(TEXTURE_ID_BTN_DECREMENT);
            let btnIncrementSpecialWager = this.assets.getSprite(TEXTURE_ID_BTN_INCREMENT);
            btnIncrementSpecialWager.scale.x *= -1;

            this.layout.setWidth(btnDecrementSetting, settingSelectorBtnWidth);
            this.layout.setWidth(btnIncrementSpecialWager, settingSelectorBtnWidth);

            titleField.x = this.layout.relWidth(50) - (titleField.width * 0.5);
            selector.x = this.layout.relWidth(50) - (selector.getVisibleWidth() * 0.5);
            btnDecrementSetting.x = selector.x - btnDecrementSetting.width - this.layout.relWidth(2);
            btnIncrementSpecialWager.x = selector.x + selector.getVisibleWidth() + this.layout.relWidth(2) + btnIncrementSpecialWager.width;

            titleField.y = 0;
            selector.y = titleField.y + 0;
            this.layout.verticalAlignMiddle(btnDecrementSetting, 0, selector);
            this.layout.verticalAlignMiddle(btnIncrementSpecialWager, 0, selector);

            container.addChild(titleField);
            container.addChild(selector);
            container.addChild(btnDecrementSetting);
            container.addChild(btnIncrementSpecialWager);

            selector.on(ScrollingSelectorEvents.SELECTED_VALUE_CHANGED, () => {
                let selectedIndex = selector.getSelectedValueIndex();
                let specialWagerMultiplier = allBetSettingValues[selectedIndex];
                this._model.setSpecialWagerMultiplier(specialWagerMultiplier);
            });

            btnDecrementSetting.interactive = true;
            btnDecrementSetting.on(C_PointerEvt.DOWN, () => selector.selectPrevOptionValue());

            btnIncrementSpecialWager.interactive = true;
            btnIncrementSpecialWager.on(C_PointerEvt.DOWN, () => selector.selectNextOptionValue());
    }
    */



    /**
     * @private
     * @param {BetSetting} betSetting
     * @return {number[]}
     */
    getPossibleValuesFor(betSetting)
    {
        let possibleValues;

        if (betSetting.isEnumerated)
        {
            possibleValues = betSetting.values;
        }
        else
        {
            possibleValues = [];
            for (let value = betSetting.minValue; value <= betSetting.maxValue; value ++) {
                possibleValues.push(value);
            }
        }

        return possibleValues;
    }


    /**
     * @private
     */
    createStandardGuiBtns()
    {
        let closeMenuBtn = new Button("selectorBtn", "selectorBtn_over");
        this.layout.propHeightScale(closeMenuBtn, 9, this);
        this.layout.horizontalAlignCenter(closeMenuBtn, 0);
        this.layout.verticalAlignBottom(closeMenuBtn, 2);

        // TODO: i think this should just fire "close_bet_menu_pressed", and let BetMenu state order
        // the view to be removed / destroyed (same for AUtoplay menu). This way, we win this action
        // for free when we need to enter "error" state within the game.
        closeMenuBtn.once(C_PointerEvt.DOWN, () => {
            this.dispatcher.emit(C_GameEvent.BUTTON_CLOSE_BET_MENU_PRESSED);
            this.closeBetMenu();
        });

        let closeMenuTxt = new PIXI.Text(this.locale.getString('T_button_ok'), {
            fontFamily : "Arial",
            fontSize : Math.round(closeMenuBtn.height * 0.8),
            fill : 0xFFFFFF
        });
        this.layout.limitSize(closeMenuTxt, 80, 70, closeMenuBtn);
        this.layout.horizontalAlignCenter(closeMenuTxt, 0, closeMenuBtn);
        this.layout.verticalAlignMiddle(closeMenuTxt, 0, closeMenuBtn);
        this.addChild(closeMenuBtn);
        this.addChild(closeMenuTxt);
    };


    /**
     * Updates the text shown for Total Stake value at the head of the menu.
     * @private
     */
    updateTotalBetText()
    {
        this._headerText.text = this.locale.getString('T_betMenu_totalStake', {
            "[TOTAL_STAKE]" : this._model.formatCurrency(this._model.getTotalBet())
        });
    };


    /**
     * Closes the Bet Menu.
     * @private
     */
    closeBetMenu()
    {
        this.log.info('BetMenu.closeBetMenu()');

        this.removeFromParent();
    };


    /**
     * @inheritDoc
     */
    destroy()
    {
        super.destroy();
        this._dispatcher.off(C_GameEvent.BET_SETTINGS_CHANGED, this.updateTotalBetText, this);
        this._dispatcher.off(C_GameEvent.HIDE_BET_MENU, this.closeBetMenu, this);
    };

}
export default BetMenuView