import BaseView from "./BaseView";
import VisualUtil from "../utils/VisualUtil";
import * as C_SymbolFadeType from "../const/C_SymbolFadeType";
import * as LogManager from "../../../cms-core-logic/src/logging/LogManager";

/**
 * Enum for the Primary visual states that a Symbol can be in.
 */
const VisualState =
{
    /**
     * The Symbol is currently shown as idle.
     */
    IDLE : 0,

    /**
     * The symbol is currently being shown as held.
     */
    HELD : 1,

    /**
     * The Symbol is currently shown as spinning.
     */
    SPINNING : 2,

    /**
     * The Symbol is currently shown as involved in a Big Win presentation.
     */
    INVOLVED_IN_BIG_WIN : 3,

    /**
     * The Symbol is currently shown as involved in an Idle Win presentaiton.
     */
    INVOLVED_IN_IDLE_WIN : 4,

    /**
     * The Symbol is currently shown as involved in a Quick Win presentation (that is
     * not a Big Win)
     */
    INVOLVED_IN_QUICK_WIN : 5,

    /**
     * The Symbol is "not involved in a win" (but other symbols are): this is a special
     * visual state, in which the symbol is often shown as faded (so that it no longer
     * is emphasised on the screen)
     */
    NOT_INVOLVED_IN_WIN : 6
};

/**
 * Enum to indicate the Tall Symbol state of the symbol.
 */
const TallSymState =
{
    NORMAL : 0,
    TALL : 1,
    INVOLVED_IN_TALL : 2,
    PARTIALLY_TALL : 3
};

/**
 * Default configuration to use for a specific symbol, when no custom configuration has been
 * provided. The symbol will be shown at standard scale, with a "shine" win animation, and
 * that is about it!
 * @type {SymbolViewConfig}
 */
const DEFAULT_SYMBOL_VIEW_CONFIG =
{
    scale : 100,
    winAnimation :
    {
        type : "shine",
        loop : true
    }
};

/**
 * Default id for an empty symbol texture: we should always have one of these loaded, at a size
 * basically comparable to a standard symbol. This allows us to create the sprites required,
 * without actually showing anything meaningful.
 */
const EMPTY_SYM_TEXTURE_ID = 'empty_sym';

/**
 * Abstraction for a single symbol.
 * A reel contains several of those.
 * @implements SymbolView
 */
class Symbol extends BaseView
{

    constructor()
    {
        super();

        // TODO: Why is this here, and where does the "removed" event come from?
        //Igor: I am not really sure as well. Could be safe to remove
        this.removeListener('removed', this.destroy, this);

        /**
         * The master configuration for all symbol animations (includes default animation
         * settings, as well as specific settings for individual symbol ids).
         * @private
         * @type {SymbolsConfiguration}
         */
        this._symbolsConfig = this.config.symbolsConfig;

        /**
         * Currently symbol id being shown on this symbol.
         * @private
         * @type {number}
         */
        this._symbolId = 0;

        /**
         * Specific configuration, for the symbol currently being shown. Once a symbol id has been
         * set, we guarantee to set this to some kind of usable value (even if no symbol config data
         * has been specified, we will fall back to a simple, default configuration for how to show
         * the symbol).
         * @private
         * @type {SymbolViewConfig}
         */
        this._currSymbolConfig = null;

        /**
         * Specific configuration, the "1 tall" animation of the symbol currently being shown.
         * Once a symbol id has been set, we guarantee to set this to some kind of usable value
         * (even if no animation data is specified, we will fall back to a default "shine" effect
         * animation)
         * @private
         * @type {SymbolAnimationConfig}
         */
        this._symbolAnimConfig = null;

        /**
         * Currently active win animation config, for Tall Symbol: this will always be null if no
         * Tall Symbol state is active (and will only ever be non-null if a Tall Symbol is currently
         * being shown). It cannot be initialized when the symbol is initially set to a certain id,
         * because we do not yet know how many symbols tall any Tall Symbol state will be.
         * @private
         * @type {SymbolAnimationConfig}
         */
        this._symbolTallAnimConfig = null;
        
        /**
         * Overall configuration options for the current symbol id, when it is in "tall symbol" mode.
         * If this field is null, then the current symbol id doesn't support being a tall symbol.
         * @private
         * @type {{ [numSymInTallSym:number]:SymbolViewConfig } | null}
         */
        this._symbolTallConfig = null;

        /**
         * Configuration for the standard symbol background.
         * @private
         * @type {SymbolComponentConfig}
         */
        this._symbolBgConfig = null;

        /**
         * Configuration for any symbol held background that needs showing for the current symbol id.
         * This can either be a custom configuration for the symbol, a generic configuration (used by
         * default for all symbols), or set to null (in which case, no held background should be shown
         * for the current symbol id). This is updated everytime that symbol id is updated.
         * @private
         * @type {SymbolHeldComponentConfig}
         */
        this._symbolHeldBgConfig = null;

        /**
         * Configuration for any symbol win background that needs showing for the current symbol id.
         * This can either be a custom configuration for the symbol, a generic configuration (used
         * by default for all symbols), or set to null (in which case, no win background should be
         * shown for the current symbol id). This is updated everytime that symbol id is updated.
         * @private
         * @type {SymbolWinComponentConfig}
         */
        this._symbolWinBgConfig = null;

        /**
         * Configuration for any symbol win foreground that needs showing for the current symbol id.
         * This can either be a custom configuration for the symbol, a generic configuration (used
         * by default for all symbols), or set to null (in which case, no foreground should be shown
         * for the current symbol). This is updated everytime that symbol id is updated.
         * @private
         * @type {SymbolWinComponentConfig}
         */
        this._symbolWinFgConfig = null;

        /**
         * Lookup table of all enter / exit actions, for all visual states.
         * @private
         */
        this._visualStates =
        {
            [VisualState.IDLE] :
            {
                enter: () => this.enterIdleVisualState(),
                exit: () => this.exitIdleVisualState()
            },
            [VisualState.HELD] :
            {
                enter: () => this.enterHeldVisualState(),
                exit: () => this.exitHeldVisualState()
            },
            [VisualState.SPINNING] :
            {
                enter: () => this.enterSpinningVisualState(),
                exit: () => this.exitSpinningVisualState()
            },
            [VisualState.INVOLVED_IN_BIG_WIN] :
            {
                enter : () => this.enterInvolvedInBigWinVisualState(),
                exit : () => this.exitInvolvedInBigWinVisualState()
            },
            [VisualState.NOT_INVOLVED_IN_WIN] :
            {
                enter: () => this.enterNotInvolvedInWinVisualState(),
                exit: () => this.exitNotInvolvedInWinVisualState()
            },
            [VisualState.INVOLVED_IN_IDLE_WIN] :
            {
                enter: () => this.enterInvolvedInWinWithAnimationVisualState(),
                exit: () => this.exitInvolvedInWinWithAnimationVisualState()
            },
            [VisualState.INVOLVED_IN_QUICK_WIN] :
            {
                enter: () => this.enterQuickWinState(),
                exit: () => this.exitQuickWinState()
            }
        };

        /**
         * Tracks the primary visual state of the symbol right now
         * @private
         */
        this._currVisualState = VisualState.IDLE;

        /**
         * The previous visual state (before the change to current visual state)
         * @private
         */
        this._prevVisualState = null;

        /**
         * Tracks the "Tall Symbol" state of this symbol.
         * @private
         */
        this._tallSymbolState = TallSymState.NORMAL;

        /**
         * For debug purposes, tracks the number of symbols currently being shown for Tall
         * Symbol State (if it's active). This represents the actual number of vertical symbol
         * cells, that our Symbol visually occupies. It does not necessarily correspond to the
         * sise of the tall symbol sprite picked to show. For example: we may show a symbol as
         * 2 tall (in which case, this field will indicate 2), but are actually using the 3
         * tall sprite (and cropping it with a mask). To check how many symbols are currently
         * shown in our actual tall symbol asset, please see "numSymInTallSymAsset" field,
         * (which sometimes is going to be the same value as this field, of course)
         * @private
         * @type {number}
         */
        this._numSymInTallSymVisually = 0;

        /**
         * Tracks the number of symbols in any current tall symbol asset which is being shown,
         * which may or may not be the same as the number of symbols that the tall symbol is
         * being shown visually as occupying. For example: we may have 4 symbols on a reel,
         * and we can show our symbol as 1 symbol cell high (normal), or in a tall state (which
         * would be [2,3 or 4] cells high). We might only have assets available for the 4 tall
         * state : in which case, the value of this field would be 4, even if we were showing
         * the symbol as occupying 2 or 3 consecutive vertical cells.
         * @private
         * @type {number}
         */
        this._numSymInTallSymAsset = 0;

        /**
         * For debug purposes, tracks if the tall symbol being shown is currently cropped.
         * @private
         * @type {boolean}
         */
        this._tallSymIsCropped = false;

        /**
         * Tracks the current position index of this symbol. This is for debug purposes only
         * (so that we can show the posIndex when showing debug info text, but also to print
         * it as part of any detailed debug log statements).
         * @private
         * @type {number}
         */
        this._posIndex = 0;

        /**
         * Tracks the current reel index of this symbol (not ever expected to change in the
         * symbol's lifetime!!). This is purely for debug purposes (it will only be added to
         * log statements, and if we enabled symbol log statements, our logs become massive:
         * so, we really need each symbol to announce exactly which reel and position it is
         * currently in, in order to get any useful information).
         */
        this._reelIndex = 0;

        // Override logger: the logging generated by SymbolView is waay too much to leave in,
        // unless you are doing very specific debugging.
        // TODO: When the new "dynamic debug" features are available, we should be able to
        // explicitly turn symbol logging on / off using them.
        this.log = LogManager.getSpinViewLogger();
    };


    // Russells note:
    // Apparently this method is public, but i have no idea why it would need to be,
    // or why this code block cannot just be executed by the constructor.
    /**
     * @public
     */
    create()
    {
        // TODO: Both of these could also have their x / y positions configured, so the rectangles
        // fully represent the bounds of the symbol.
        /**
         * Defines the standard dimensions of the symbol.
         * @private
         * @type {PIXI.Rectangle}
         */
        this._symbolNormalRect = new PIXI.Rectangle(0, 0, this.config.symWidth, this.config.symHeight);

        /**
         * Defines the dimensions of the symbol, when it is a tall symbol.
         * @private
         * @type {PIXI.Rectangle}
         */
        this._symbolTallRect = new PIXI.Rectangle(0, 0, 0, 0);

        // TODO: THese various "isRequired" checks maybe should be handled differently ?

        let isBackgroundRequired = this._symbolsConfig.defaultBackground? true : false;
        
        // TODO: Should the contract for this be, that it is visible permanently ?
        if (isBackgroundRequired)
        {
            /**
             * Background to be used when the symbol is idle.
             * @private
             * @type {PIXI.extras.AnimatedSprite}
             */
            this._symbolBackground = new PIXI.extras.AnimatedSprite([this.assets.getTexture(EMPTY_SYM_TEXTURE_ID)]);
            this._symbolBackground.visible = false;
            this.addChild(this._symbolBackground);
        }

        let isHeldBackgroundRequired = this._symbolsConfig.defaultHeldBackground? true : false;

        if (isHeldBackgroundRequired)
        {
            /**
             * Background to be used when the symbol (or reel that the symbol is on) is held.
             * @private
             * @type {PIXI.extras.AnimatedSprite}
             */
            this._symbolHeldBackground = new PIXI.extras.AnimatedSprite([this.assets.getTexture(EMPTY_SYM_TEXTURE_ID)]);
            this._symbolHeldBackground.visible = false;
            this._symbolHeldBackground.alpha = 0;
            this.addChild(this._symbolHeldBackground);
        }

        let isWinBackgroundRequired = true; // TODO

        if (isWinBackgroundRequired)
        {
            /**
             * Background win component layer. This can be used to display some kind of "symbol background"
             * effect during a win, which is available for either (or both) of the win states - "quick win"
             * (with no animation) or "idle win" (with symbol animation).
             * @private
             * @type {PIXI.extras.AnimatedSprite}
             */
            this._symbolWinBackground = new PIXI.extras.AnimatedSprite([this.assets.getTexture(EMPTY_SYM_TEXTURE_ID)]);
            this._symbolWinBackground.visible = false;
            this.addChild(this._symbolWinBackground);
        }

        /**
         * The main static symbol graphic, shown when the symbol is stationary.
         * @private
         * @type {PIXI.Sprite}
         */
        this._symbolNormalStatic = this.assets.getSprite(EMPTY_SYM_TEXTURE_ID);
        this._symbolNormalStatic.filters = [];

        this.addChild(this._symbolNormalStatic);
        this.layout.propHeightScale(this._symbolNormalStatic, this._symbolsConfig.defaultScale, this._symbolNormalRect);

        if (this._symbolsConfig.useBlur)
        {
            let blurLayerScale = this._symbolsConfig.defaultScale * this._symbolsConfig.blurVerticalScale;

            /**
             * The blur layer to be shown above the symbol, when the reels are spinning. Note: if blurring
             * is not enabled for this game client, then this field will be null or undefined (so you may
             * need to check if this instance exists)
             * @private
             * @type {PIXI.Sprite | null}
             */
            this._symbolNormalBlur = this.assets.getSprite(EMPTY_SYM_TEXTURE_ID);
            this._symbolNormalBlur.anchor.set(.5, .5);
            this._symbolNormalBlur.alpha = 0;
            this.addChild(this._symbolNormalBlur);
            this.layout.propHeightScale(this._symbolNormalBlur, blurLayerScale, this._symbolNormalRect);
        }

        // TODO: what is "uniform"? I can see a filter is being set up, but i don't understand
        // the actual set up code.
        const uniforms = {position: {type: "f", value: 0}, intensity: {type: "f", value: 1}};

        const shaderCode = `
            precision mediump float;

            varying vec2 vTextureCoord;
            uniform sampler2D uSampler;
        
            uniform float position;
            uniform float intensity;
        
            void main(void) 
            {	
                vec4 texSample = texture2D(uSampler, vTextureCoord);
        
                float tmp = vTextureCoord.x + vTextureCoord.y - position;	
            
                tmp = intensity * exp (tmp * tmp / -0.01) * texSample.w;
            
                vec4 scaledComp = texSample * tmp;	
                texSample = texSample + scaledComp;
            
                gl_FragColor = texSample;
            }
        `;

        /**
         * @private
         * @type {PIXI.Filter}
         */
        this._shineFilter = new PIXI.Filter('',shaderCode,uniforms);

        // If tall symbols are enabled, then add the tall symbol layer.
        if (this._symbolsConfig.tallSymPerSymbol)
        {
            /**
             * Static Tall Symbol layer. By providing a separate layer for tall symbols, we can perform
             * a cross-fade to get rid of the tall symbol (when starting a new spin). NOTE: If tall
             * symbols are not in any way enabled for the game client, then this field will be null or
             * undefined, so you may need to check if it exists.
             * @private
             * @type {PIXI.Sprite | null}
             */
            this._symbolTallStatic = this.assets.getSprite(EMPTY_SYM_TEXTURE_ID);
            this._symbolTallStatic.visible = false;
            this.addChild(this._symbolTallStatic);
        }

        /**
         * The animation for the symbol, generally shown during some kind of winnings presentation.
         * @private
         * @type {PIXI.extras.AnimatedSprite}
         */
        this._symbolAnim = new PIXI.extras.AnimatedSprite([this.assets.getTexture(EMPTY_SYM_TEXTURE_ID)]);
        this._symbolAnim.anchor.set(.5, .5);
        this._symbolAnim.visible = false;
        this._symbolAnim.loop = false;
        this.addChild(this._symbolAnim); 
        this.layout.propHeightScale(this._symbolAnim, this._symbolsConfig.defaultAnimScale, this._symbolNormalRect);

        /**
         * Reference to a Skeletal symbol animation instance, which may currently exist.
         * @private
         */
        this._symbolAnimSkeletal = null;

        let isWinForegroundRequired = true; // TODO

        if (isWinForegroundRequired)
        {
            /**
             * Foreground win component layer. This can be used to display some kind of "symbol frame"
             * overlay, which is available for either (or both) of the win states - "quick win" (with
             * no animation) or "idle win" (with symbol animation).
             * @private
             * @type {PIXI.extras.AnimatedSprite}
             */
            this._symbolWinForeground = new PIXI.extras.AnimatedSprite([this.assets.getTexture(EMPTY_SYM_TEXTURE_ID)]);
            this._symbolWinForeground.visible = false;
            this.addChild(this._symbolWinForeground);
        }

        /**
         * Cached index - within the Symbol - at which symbol animation must be displayed.
         * @private
         * @type {number}
         */
        this._symbolAnimDisplayIndex = this.getChildIndex(this._symbolAnim);

        /**
         * Flag to indicate if we need to reinvoke config animations for "normal" win animation. We use
         * a lazy approach to configuring our win animations - we don't do it automatically when symbol
         * id changes, we do it only if we are showing an actual win animation (eg: it's required). But,
         * we could enter and exit the win animation state multiple times while symbol id is the same
         * (eg: we are in idle state, and are cycling through a list of symbol animations): in this case,
         * we want to avoid invoking the setup of win animation repeatedly when not required (because in
         * some cases, the expense of doing this is non-trivial, eg: when we are using DragonBones symbols,
         * we are basically creating a new armature each time we setup the win animation). Therefore, we
         * can use this flag as a dirty check: when symbol id changes, we indicate that the normal win
         * animation will need setting up when it is time to use it (and when we set up the win animation
         * again, we set this flag back to false)
         * @private
         * @type {boolean}
         */

        /**
         * Flag to indicate if we need to fully configure the skeletal win animation when we enter any
         * animated state that uses it. We use lazy initialization for win animations (including the
         * skeletal win animations, which are expensive to create), so that they are only configured
         * when required. However, the most appropriate moment that this is required, is also a moment
         * that gets invoked repeatedly in cases of idle win sequences (where we may loop through a
         * series of animations), so we use this dirty flag to check if the skeletal win animation needs
         * creating again.
         * @private
         * @type {boolean}
         */
        this._skeletalWinAnimationRequiresSetup = true;
    };


    //--------------------------------------------------------------------------------------------------
    // showState methods
    //--------------------------------------------------------------------------------------------------
    // We have a number of basic visual states for the symbol, which correspond to the context in which
    // it is being used:
    // - idle
    // - spinning (shown when any kind of spin animation is in progress)
    // - involvedInWin
    // - notInvolvedInWin (this is arguably a special case of involvedInWin, but we use a separate
    //   state, and a separate public method, to achieve it)
    //--------------------------------------------------------------------------------------------------
    /**
     * @public
     * @inheritDoc
     */
    showIdleState()
    {
        this.log.debug(`Symbol[r:${this._reelIndex},p:${this._posIndex}].showIdleState()`);

        this.setVisualStateTo(VisualState.IDLE);
    };


    /**
     * @public
     * @inheritDoc
     */
    showHeldState()
    {
        this.log.debug(`Symbol[r:${this._reelIndex},p:${this._posIndex}].showHeldState()`);

        this.setVisualStateTo(VisualState.HELD);
    };


    /**
     * @public
     * @inheritDoc
     */
    showSpinningState()
    {
        this.log.debug(`Symbol[r:${this._reelIndex},p:${this._posIndex}].showSpinningState()`);

        this.setVisualStateTo(VisualState.SPINNING);
    };


    /**
     * @public
     * @inheritDoc
     */
    showBigWinState()
    {
        this.log.debug(`Symbol[r:${this._reelIndex},p:${this._posIndex}].showBigWinState()`);

        this.setVisualStateTo(VisualState.INVOLVED_IN_BIG_WIN);
    };


    /**
     * @public
     * @inheritDoc
     */
    showQuickWinState()
    {
        this.log.debug(`Symbol[r:${this._reelIndex},p:${this._posIndex}].showQuickWinState()`);

        this.setVisualStateTo(VisualState.INVOLVED_IN_QUICK_WIN);
    };


    /**
     * @public
     * @inheritDoc
     */
    showIdleWinState()
    {
        this.log.debug(`Symbol[r:${this._reelIndex},p:${this._posIndex}].showIdleWinState()`);

        this.setVisualStateTo(VisualState.INVOLVED_IN_IDLE_WIN);
    };


    /**
     * @public
     * @inheritDoc
     */
    showNonWinState()
    {
        this.log.debug(`Symbol[r:${this._reelIndex},p:${this._posIndex}].showNonWinState()`);

        this.setVisualStateTo(VisualState.NOT_INVOLVED_IN_WIN);
    };


    /**
     * Changes the visual state of the symbol, invoking any animated transitions required
     * to make it look nice. If the visual state passed is the same as the one that we are
     * currently in, no transition will be invoked. Otherwise, this is a basic state machine:
     * we call an exit action for whatever state we were previously in, and an enter action
     * for the new visual state. Whilst this seems convoluted initially (compared to simply
     * invoking functionality on the public "showIdle" / "showSpinning" methods), this proved
     * to be a better way of cleaning up after ourselves when we are no longer doing something
     * (the code reads more simply now).
     * @private
     * @param {number} newVisualState
     * The id of the new visual state to change to.
     */
    setVisualStateTo(newVisualState)
    {
        if (newVisualState === this._currVisualState) {
            return;
        }

        let prevVisualState = this._currVisualState;

        this.log.debug(`Symbol[r:${this._reelIndex},p:${this._posIndex}]: changing VisualState from ${prevVisualState} to ${newVisualState}`);

        this._currVisualState = newVisualState;
        this._prevVisualState = prevVisualState;
        
        this._visualStates[prevVisualState].exit();
        this._visualStates[newVisualState].enter();
    };


    /**
     * Retriggers the enter action of the current visual state. This is useful to invoke when
     * changing the Tall Symbol state of the Symbol: retriggering the enter action means that
     * we don't have to handle which layers are visible inside the tall symbol methods, we can
     * use the standard visual state methods to do it for us (and it also means we can correctly
     * drop into a visual state when we switch between tall and non-tall symbol modes).
     * @private
     */
    retriggerCurrentVisualStateEnterAction() {
        this._visualStates[this._currVisualState].enter();
    };


    /**
     * Enter action for the "Idle" visual state.
     * @private
     */
    enterIdleVisualState()
    {
        this.log.debug(`Symbol[r:${this._reelIndex},p:${this._posIndex}].enterIdleVisualState()`);

        // TODO: If a tall symbol is active, this needs to behave differently
        if (this._tallSymbolState === TallSymState.TALL) {
            this._symbolNormalStatic.visible = false;

            if (this._symbolNormalBlur) {
                this._symbolNormalBlur.visible = false;
            }

            this._symbolAnim.visible = false;
            this._symbolTallStatic.visible = true;
        }
        else
        if (this._tallSymbolState === TallSymState.INVOLVED_IN_TALL) {
            this._symbolNormalStatic.visible = false;
            
            if (this._symbolNormalBlur) {
                this._symbolNormalBlur.visible = false;
            }
            
            this._symbolAnim.visible = false;
            this._symbolTallStatic.visible = false;
        }
        else
        {
            this._symbolNormalStatic.visible = true;
            this._symbolNormalStatic.alpha = 1;
            
            if (this._symbolNormalBlur) {
                this._symbolNormalBlur.visible = false;
            }
            
            this._symbolAnim.visible = false;

            // Tall symbol doesn't always exist in this case
            if (this._symbolTallStatic) {
                this._symbolTallStatic.visible = false;
            }
        }

        this.updateDebugInfoText();
    };


    /**
     * Exit action for the "Idle" Visual State
     * @private
     */
    exitIdleVisualState()
    {
        this.log.debug(`Symbol[r:${this._reelIndex},p:${this._posIndex}].exitIdleVisualState()`);

        // Nothing to do, at present
    };


    /**
     * Enter action for the Held visual state.
     * @private
     */
    enterHeldVisualState()
    {
        this.log.debug(`Symbol[r:${this._reelIndex},p:${this._posIndex}].enterHeldVisualState()`);

        // TODO: Look at conditional logic, to see if we can do anything special here, in order
        // to avoid doing what we don't want..

        // TODO: If a tall symbol is active, this needs to behave differently
        if (this._tallSymbolState === TallSymState.TALL) {
            this._symbolNormalStatic.visible = false;

            if (this._symbolNormalBlur) {
                this._symbolNormalBlur.visible = false;
            }

            this._symbolAnim.visible = false;
            this._symbolTallStatic.visible = true;
        }
        else
        if (this._tallSymbolState === TallSymState.INVOLVED_IN_TALL) {
            this._symbolNormalStatic.visible = false;
            
            if (this._symbolNormalBlur) {
                this._symbolNormalBlur.visible = false;
            }
            
            this._symbolAnim.visible = false;
            this._symbolTallStatic.visible = false;
        }
        else
        {
            this._symbolNormalStatic.visible = true;
            this._symbolNormalStatic.alpha = 1;
            
            if (this._symbolNormalBlur) {
                this._symbolNormalBlur.visible = false;
            }
            
            this._symbolAnim.visible = false;

            // Tall symbol doesn't always exist in this case
            if (this._symbolTallStatic) {
                this._symbolTallStatic.visible = false;
            }

            // TODO: This COULD be pushed outside of this block ?
            // Check for held background
            let heldBackgroundConfig = this._symbolHeldBgConfig;
            if (heldBackgroundConfig && this._symbolHeldBackground) {
                this.showSymbolComponent(this._symbolHeldBackground, heldBackgroundConfig);
            }
        }

        this.updateDebugInfoText();
    };


    /**
     * Exit action for the Held visual state.
     * @private
     */
    exitHeldVisualState()
    {
        this.log.debug(`Symbol[r:${this._reelIndex},p:${this._posIndex}].exitHeldVisualState()`);

        if (this._tallSymbolState === TallSymState.NORMAL)
        {
            let heldBackgroundConfig = this._symbolHeldBgConfig;
            if (heldBackgroundConfig && this._symbolHeldBackground) {
                this.hideSymbolComponent(this._symbolHeldBackground, heldBackgroundConfig);
            }
        }
    };


    /**
     * Enter action for the "Spinning" visual state.
     * @private
     */
    enterSpinningVisualState()
    {
        this.log.debug(`Symbol[r:${this._reelIndex},p:${this._posIndex}].enterSpinningVisualState()`);

        // TODO: "If a tall symbol is active, this needs to behave differently"
        // As on 06.10.2020, not actually sure what was meant by this comment.
        if (this._tallSymbolState === TallSymState.TALL) {
            this._symbolNormalStatic.visible = false;

            if (this._symbolNormalBlur) {
                this._symbolNormalBlur.visible = false;
            }

            this._symbolAnim.visible = false;
            this._symbolTallStatic.visible = true;
        }
        else
        if (this._tallSymbolState === TallSymState.INVOLVED_IN_TALL) {
            this._symbolNormalStatic.visible = false;
            
            if (this._symbolNormalBlur) {
                this._symbolNormalBlur.visible = false;
            }

            this._symbolAnim.visible = false;
            this._symbolTallStatic.visible = false;
        }
        else
        {
            // Symbol is being shown in "normal" state. We simply have to ensure that
            // the normal visual layer is visible, and the blur layer is visible (as
            // we might be showing a blur effect during the animation)

            this._symbolNormalStatic.visible = true;

            if (this._symbolNormalBlur) {
                this._symbolNormalBlur.visible = true;
                this._symbolNormalBlur.alpha = 0; // reset blur to 0, before any blur effect applied
            }

            this._symbolAnim.visible = false;

            // Tall symbol doesn't always exist in this case.
            if (this._symbolTallStatic) {
                this._symbolTallStatic.visible = false;
            }
        }

        this.updateDebugInfoText();
    };


    /**
     * Exit action for the "Spinning" Visual State
     * @private
     */
    exitSpinningVisualState()
    {
        this.log.debug(`Symbol[r:${this._reelIndex},p:${this._posIndex}].exitSpinningVisualState()`);

        // Nothing to do, at present
    };


    /**
     * Enter action for the "Involved In Win - With Animation" Visual State.
     * @private
     */
    enterInvolvedInWinWithAnimationVisualState()
    {
        this.log.debug(`Symbol[r:${this._reelIndex},p:${this._posIndex}].enterInvolvedInWinWithAnimationVisualState()`);

        if (this._tallSymbolState === TallSymState.TALL)
        {
            this._symbolNormalStatic.visible = false;

            if (this._symbolNormalBlur) {
                this._symbolNormalBlur.visible = false;
            }

            // We actually do not know whether the "tall static" or "tall animated"
            // layers should be shown, without examining what is specified in the
            // configured animation. So, we delegate this task to the methods which
            // setup & play the animation.
            this.setupTallWinAnimation();
            this.playTallWinAnimation();
        }
        else
        if (this._tallSymbolState === TallSymState.INVOLVED_IN_TALL)
        {
            this._symbolNormalStatic.visible = false;
            this._symbolAnim.visible = false;
            
            if (this._symbolNormalBlur) {
                this._symbolNormalBlur.visible = false;
            }

            this._symbolTallStatic.visible = false;
        }
        // TODO: This branch is catch all, and includes "partially tall", which...
        // may not be correct? (If it is correct, some comments here explaining why
        // it works like this, would be apreciated!!)
        else
        {
            // Symbol is in "normal" (1 tall) animated state
            if (this._symbolNormalBlur) {
                this._symbolNormalBlur.visible = false;
            }

            // Tall symbol doesn't always exist in this case.
            if (this._symbolTallStatic) {
                this._symbolTallStatic.visible = false;
            }

            this.setupNormalWinAnimation();
            this.playNormalWinAnimation();

            let winBackgroundConfig = this._symbolWinBgConfig;
            if (winBackgroundConfig && winBackgroundConfig.useInIdleWin && this._symbolWinBackground) {
                this.showSymbolComponent(this._symbolWinBackground, winBackgroundConfig);
            }

            let winForegroundConfig = this._symbolWinFgConfig;
            if (winForegroundConfig && winForegroundConfig.useInIdleWin && this._symbolWinForeground) {
                this.showSymbolComponent(this._symbolWinForeground, winForegroundConfig);
            }
        }

        this.updateDebugInfoText();
    };


    /**
     * Exit action for the "Involved In Win - With Animation" Visual State
     * 
     * We need to clean up any animations that were in progress, hide the win frame if one
     * was shown, etc.
     * @private
     */
    exitInvolvedInWinWithAnimationVisualState()
    {
        this.log.debug(`Symbol[r:${this._reelIndex},p:${this._posIndex}].exitInvolvedInWinWithAnimationVisualState()`);

        // TODO: This block can change. With the introduction of an optional "static"
        // tall symbol state, its now possible to handle this differently (eg: the
        // same set of logic will apply to both normal and tall symbols)
        if (this._tallSymbolState === TallSymState.TALL)
        {
            // Because we currently use symbolAnim for tallSym, we need to simply stop
            // the animation, and go to its first frame - for now (i will introduce a
            // separate "tall symbol" visual layer to resolve other issues that relate
            // to tall symbol resolution)
            this._symbolAnim.gotoAndStop(0);
            
            if (this._symbolAnimSkeletal) {
                this._symbolAnimSkeletal.children[0].animation.stop();
                this._symbolAnimSkeletal.visible = false;
            }

        }
        // TODO: This else logic is catch all, and that may not be the correct solution
        // (it may require some further modification)
        else
        {
            // Not a tall sym.. are all conditions covered correctly here ??
            this._symbolAnim.stop();
            this._symbolAnim.visible = false; // probably not required

            TweenMax.killTweensOf(this._symbolAnim);

            if (this._symbolAnimSkeletal) {
                this._symbolAnimSkeletal.children[0].animation.stop();
                this._symbolAnimSkeletal.visible = false;
            }

            // We could fade the animated symbol away ? This could look nice. However, to do
            // this, requires a little thought about state transitions. If any enter state
            // action calls something on the symbolAnim layer, then it will likely destroy
            // whatever animation we were trying to perform.

            if (this._tallSymbolState === TallSymState.NORMAL)
            {
                let winBackgroundConfig = this._symbolWinBgConfig;
                if (winBackgroundConfig && winBackgroundConfig.useInQuickWin && this._symbolWinBackground) {
                    this.hideSymbolComponent(this._symbolWinBackground, winBackgroundConfig);
                }

                let winForegroundConfig = this._symbolWinFgConfig;
                if (winForegroundConfig && winForegroundConfig.useInQuickWin && this._symbolWinForeground) {
                    this.hideSymbolComponent(this._symbolWinForeground, winForegroundConfig);
                }
            }
        }

        // TODO: Not 100% sure what all of this is up to, it needs some comments to explain
        // it
        // Kill any "shine effect" tweens on the symbols
        TweenMax.killTweensOf(this._shineFilter.uniforms);

        if (this._symbolNormalStatic.filters.indexOf(this._shineFilter) !== -1)
        {
            const hideTime = .2;
            TweenMax.to(this._shineFilter.uniforms, hideTime, { intensity: 0, onComplete: () => this.removeFilters() });
        }
    };


    /**
     * @private
     */
    enterInvolvedInBigWinVisualState()
    {
        this.log.debug(`Symbol[r:${this._reelIndex},p:${this._posIndex}].enterInvolvedInBigWinVisualState()`);

        if (this._tallSymbolState === TallSymState.TALL)
        {
            // This works correctly
            this._symbolNormalStatic.visible = false;
            this._symbolAnim.visible = false;
            
            if (this._symbolNormalBlur) {
                this._symbolNormalBlur.visible = false;
            }

            this._symbolTallStatic.visible = true;

            this._symbolAnim.gotoAndStop(0);
        }
        else
        if (this._tallSymbolState === TallSymState.INVOLVED_IN_TALL)
        {
            this._symbolNormalStatic.visible = false;
            this._symbolAnim.visible = false;

            if (this._symbolNormalBlur) {
                this._symbolNormalBlur.visible = false;
            }

            this._symbolTallStatic.visible = false;
        }
        else
        if (this._tallSymbolState === TallSymState.NORMAL)
        {
            // Symbol is in "normal" state
            this._symbolAnim.visible = false;
            this._symbolNormalStatic.visible = true;
            
            if (this._symbolNormalBlur) {
                this._symbolNormalBlur.visible = false;
            }

            // Tall symbol doesn't always exist in this case.
            if (this._symbolTallStatic) {
                this._symbolTallStatic.visible = false;
            }
            
            let winBackgroundConfig = this._symbolWinBgConfig;
            if (winBackgroundConfig && winBackgroundConfig.useInBigWin && this._symbolWinBackground) {
                this.showSymbolComponent(this._symbolWinBackground, winBackgroundConfig);
            }

            let winForegroundConfig = this._symbolWinFgConfig;
            if (winForegroundConfig && winForegroundConfig.useInBigWin && this._symbolWinForeground) {
                this.showSymbolComponent(this._symbolWinForeground, winForegroundConfig);
            }
        }

        this.updateDebugInfoText();
    };


    /**
     * @private
     */
    exitInvolvedInBigWinVisualState()
    {
        this.log.debug(`Symbol[r:${this._reelIndex},p:${this._posIndex}].exitInvolvedInBigWinVisualState()`);

        if (this._tallSymbolState === TallSymState.NORMAL)
        {
            let winBackgroundConfig = this._symbolWinBgConfig;
            if (winBackgroundConfig && winBackgroundConfig.useInBigWin && this._symbolWinBackground) {
                this.hideSymbolComponent(this._symbolWinBackground, winBackgroundConfig);
            }

            let winForegroundConfig = this._symbolWinFgConfig;
            if (winForegroundConfig && winForegroundConfig.useInBigWin && this._symbolWinForeground) {
                this.hideSymbolComponent(this._symbolWinForeground, winForegroundConfig);
            }
        }
    };

    /**
     * Enter quick win state
     */
    enterQuickWinState()
    {
        if(this._tallSymbolState === TallSymState.NORMAL && this._symbolWinlineAnimConfig)
        {
            this._symbolNormalStatic.visible = false;

             // Symbol is in "normal" (1 tall) animated state
             if (this._symbolNormalBlur) {
                this._symbolNormalBlur.visible = false;
            }

            // Tall symbol doesn't always exist in this case.
            if (this._symbolTallStatic) {
                this._symbolTallStatic.visible = false;
            }

            this.setupWinlineAnimation();
            this.playWinlineSymbolAnimation();

            let winBackgroundConfig = this._symbolWinBgConfig;
            if (winBackgroundConfig && winBackgroundConfig.useInQuickWin && this._symbolWinBackground) {
                this.showSymbolComponent(this._symbolWinBackground, winBackgroundConfig);
            }

            let winForegroundConfig = this._symbolWinFgConfig;
            if (winForegroundConfig && winForegroundConfig.useInQuickWin && this._symbolWinForeground) {
                this.showSymbolComponent(this._symbolWinForeground, winForegroundConfig);
            }
        }
        else
        {
            this.enterInvolvedInWinNoAnimationVisualState();
        }
        
    }


    /**
     * Exit quick win state
     */
    exitQuickWinState()
    {
        if(this._tallSymbolState === TallSymState.NORMAL && this._symbolWinlineAnimConfig)
        {
            this._symbolNormalStatic.visible = true;

             // Symbol is in "normal" (1 tall) animated state
             if (this._symbolNormalBlur) {
                this._symbolNormalBlur.visible = true;
            }

            // Tall symbol doesn't always exist in this case.
            if (this._symbolTallStatic) {
                this._symbolTallStatic.visible = true;
            }
        }

        this.exitInvolvedInWinNoAnimationVisualState();
    }

    /**
     * Enter action for the "Involved In Win - No Animation" Visual State.
     * @private
     */
    enterInvolvedInWinNoAnimationVisualState()
    {
        this.log.debug(`Symbol[r:${this._reelIndex},p:${this._posIndex}].enterInvolvedInWinNoAnimationVisualState()`);
        
        if (this._tallSymbolState === TallSymState.TALL)
        {
            // This works correctly
            this._symbolNormalStatic.visible = false;
            this._symbolAnim.visible = false;
            
            if (this._symbolNormalBlur) {
                this._symbolNormalBlur.visible = false;
            }

            this._symbolTallStatic.visible = true;

            this._symbolAnim.gotoAndStop(0);
        }
        else
        if (this._tallSymbolState === TallSymState.INVOLVED_IN_TALL)
        {
            this._symbolNormalStatic.visible = false;
            this._symbolAnim.visible = false;

            if (this._symbolNormalBlur) {
                this._symbolNormalBlur.visible = false;
            }

            this._symbolTallStatic.visible = false;
        }
        else
        if (this._tallSymbolState === TallSymState.NORMAL)
        {
            // Symbol is in "normal" state
            this._symbolAnim.visible = false;
            this._symbolNormalStatic.visible = true;
            
            if (this._symbolNormalBlur) {
                this._symbolNormalBlur.visible = false;
            }

            // Tall symbol doesn't always exist in this case.
            if (this._symbolTallStatic) {
                this._symbolTallStatic.visible = false;
            }
            
            let winBackgroundConfig = this._symbolWinBgConfig;
            if (winBackgroundConfig && winBackgroundConfig.useInQuickWin && this._symbolWinBackground) {
                this.showSymbolComponent(this._symbolWinBackground, winBackgroundConfig);
            }

            let winForegroundConfig = this._symbolWinFgConfig;
            if (winForegroundConfig && winForegroundConfig.useInQuickWin && this._symbolWinForeground) {
                this.showSymbolComponent(this._symbolWinForeground, winForegroundConfig);
            }
        }

        this.updateDebugInfoText();
    };


    /**
     * Exit action for the "Involved In Win - No Animation" Visual State
     * @private
     */
    exitInvolvedInWinNoAnimationVisualState()
    {
        this.log.debug(`Symbol[r:${this._reelIndex},p:${this._posIndex}].exitInvolvedInWinWithAnimationVisualState()`);

        if (this._tallSymbolState === TallSymState.NORMAL)
        {
            let winBackgroundConfig = this._symbolWinBgConfig;
            if (winBackgroundConfig && winBackgroundConfig.useInQuickWin && this._symbolWinBackground) {
                this.hideSymbolComponent(this._symbolWinBackground, winBackgroundConfig);
            }

            let winForegroundConfig = this._symbolWinFgConfig;
            if (winForegroundConfig && winForegroundConfig.useInQuickWin && this._symbolWinForeground) {
                this.hideSymbolComponent(this._symbolWinForeground, winForegroundConfig);
            }
        }
    };


    /**
     * Enter action for the "Not Involved In Win" visual state.
     * @private
     */
    enterNotInvolvedInWinVisualState()
    {
        this.log.debug(`Symbol[r:${this._reelIndex},p:${this._posIndex}].enterNotInvolvedInWinVisualState()`);

        if (this._tallSymbolState === TallSymState.TALL)
        {
            this._symbolNormalStatic.visible = false;
            
            if (this._symbolNormalBlur) {
                this._symbolNormalBlur.visible = false;
            }

            this._symbolTallStatic.visible = true;
            this._symbolAnim.visible = false;
        }
        else
        if (this._tallSymbolState === TallSymState.INVOLVED_IN_TALL)
        {
            this._symbolNormalStatic.visible = false;
            
            if (this._symbolNormalBlur) {
                this._symbolNormalBlur.visible = false;
            }
            
            this._symbolTallStatic.visible = false;
            this._symbolAnim.visible = false;
        }
        else
        if (this._tallSymbolState === TallSymState.NORMAL)
        {
            this._symbolNormalStatic.visible = true;
            
            if (this._symbolNormalBlur) {
                this._symbolNormalBlur.visible = false;
            }
            
            this._symbolAnim.visible = false;

            // Tall symbol doesn't always exist in this case.
            if (this._symbolTallStatic) {
                this._symbolTallStatic.visible = false;
            }
        }
        else
        if (this._tallSymbolState === TallSymState.PARTIALLY_TALL)
        {
            // TODO
        }
        
        this.setFadeState(true);

        this.updateDebugInfoText();
    };


    /**
     * Exit action for the "Not Involved In Win" Visual State.
     * 
     * When we are in "not involved in win" state, we show some kind of fade effect on our the main
     * "normal" symbol layer (or the tall symbol layer). When we exit this state, we must clear the
     * fade effect.
     * @private
     */
    exitNotInvolvedInWinVisualState()
    {
        this.log.debug(`Symbol[r:${this._reelIndex},p:${this._posIndex}].exitNotInvolvedInWinVisualState()`);

        this.setFadeState(false);
    };


    //--------------------------------------------------------------------------------------------------
    // 
    //--------------------------------------------------------------------------------------------------
    /**
     * Sets the blur level of the symbol, during a Spin. To be called by the Spinner implementation,
     * which will decide if to show blurring / how much blurring needs to be shown, and when. The
     * level parameter sets the amount of blur to show (its a value in the range 0 to 1). This same
     * method is also used to clear any blur effect (simply call setBlurLevel(0)).
     * @public
     * @param {number} level 
     * The blur level to set, where 0 == no blur, and 1 == maximum blur.
     */
    setBlurLevel(level)
    {
        if (this._symbolsConfig.useBlur)
        {
            // This statement is odd - its how Wiener originally wrote it, and I do not know why
            // - if you pass 0, its Math.min(1, 2 - 0), eg:1
            // - if you pass 1, its Math.min(1, 2 - 2), eg:0
            // But why the "2 - level * 2" part?
            // Why not just "1 - level" ?
            let inverseLevel = Math.min(1, (2 - level * 2));

            this._symbolNormalStatic.alpha = inverseLevel;
            this._symbolNormalBlur.alpha = level;
        }
    };


    /**
     * Sets the id of this Symbol graphic. The symbol's visual state will be updated, based on this
     * new symbol id.
     * @public
     * @param {number} id 
     * The id of the symbol that should be shown within this Symbol View.
     */
    setId(id)
    {
        this._skeletalWinAnimationRequiresSetup = true;

        // Cache symbol id, as well as symbol specific configuration data.
        // If no symbol specific config data exists, then default to using
        // the default data. In addition, we cache the most appropriate
        // win foreground / background configs here (these could still end
        // up being null), allowing for quicker checking later on, when
        // they are required.
        this._symbolId = id;
        this._currSymbolConfig = this._symbolsConfig.perSymbol[id] || DEFAULT_SYMBOL_VIEW_CONFIG;
        this._symbolAnimConfig = this._currSymbolConfig.winAnimation || DEFAULT_SYMBOL_VIEW_CONFIG.winAnimation;
        this._symbolTallConfig = this._symbolsConfig.tallSymPerSymbol? (this._symbolsConfig.tallSymPerSymbol[id] || null) : null;
        this._symbolBgConfig = this._currSymbolConfig.background || this._symbolsConfig.defaultBackground;
        this._symbolHeldBgConfig = this._currSymbolConfig.heldBackground || this._symbolsConfig.defaultHeldBackground;
        this._symbolWinBgConfig = this._currSymbolConfig.winBackground || this._symbolsConfig.defaultWinBackground;
        this._symbolWinFgConfig = this._currSymbolConfig.winForeground || this._symbolsConfig.defaultWinForeground;
        this._symbolWinlineAnimConfig = this._currSymbolConfig.winlineAnimation || null;
        
        let anchor = this._currSymbolConfig.anchor;
        let anchorX = anchor ? anchor.x : .5;
        let anchorY = anchor ? anchor.y : .5;
        let scale = this._currSymbolConfig.scale > 0? this._currSymbolConfig.scale : 100;

        this._symbolNormalStatic.texture = this.assets.getTexture(`sym${id}`);

        if (this._symbolsConfig.useBlur)
        {
            let blurLayerScale = scale * this._symbolsConfig.blurVerticalScale;
            this._symbolNormalBlur.texture = this.assets.getTexture(`sym${id}_blur`);
            this._symbolNormalBlur.anchor.set(anchorX, anchorY);
            this.layout.propHeightScale(this._symbolNormalBlur, blurLayerScale, this._symbolNormalRect);
        }

        this._symbolNormalStatic.anchor.set(anchorX, anchorY);
        this.layout.propHeightScale(this._symbolNormalStatic, scale, this._symbolNormalRect);

        // TODO: This was the logic Igor added, but really it needs deprecating (as we already have api to do
        // what he added). need to confirm which recent games use this, and modify them when the lib changes.
        //When there is custom scale for specific symbols, this will apply it, otherwise the rest will be the default scale
        if (this._symbolsConfig.customSymbolsScale)
        {
            for(let symId = 0; symId < this._symbolsConfig.customSymbolsScale.length; symId++)
            {
                if(id === this._symbolsConfig.customSymbolsScale[symId].symId)
                {
                    this.layout.propHeightScale(this._symbolNormalStatic, this._symbolsConfig.customSymbolsScale[symId].scale, this._symbolNormalRect);
                }
            }
        }


        // I wonder if this logic got borked somewhere? Apparently, we are setting a custom scale... for the anim data.
        // But that scale should clearly be applied only on the animation, not on the regular symbol. Did this get borked
        // on this branch, or is it undone somewhere else ??
        /*
        if (this.config.animData[id] && this.config.animData[id].customScale)
        {
            this.layout.propHeightScale(this._symbolNormalStatic, this.config.animData[id].customScale, this._scaleRect);
        }
        else
        {
            this.layout.propHeightScale(this._symbolNormalStatic, this.config.symScale, this._scaleRect);
        }
        */

        // Clear any Skeletal animations, if they exist.
        if (this._symbolAnimSkeletal) {
            this.log.debug(`Symbol[r:${this._reelIndex},p:${this._posIndex}].setId: clearing old Skeletal animation away`);

            this.removeChild(this._symbolAnimSkeletal);

            let armature = this._symbolAnimSkeletal.children[0];
            
            this._symbolAnimSkeletal.removeChildren();
            this._symbolAnimSkeletal = null;
            
            armature.dispose();
        }

        this.updateDebugInfoText();
    };


    /**
     * Returns the id of the Symbol being shown by this Symbol View.
     * @public
     * @return {number}
     */
    getId()
    {
        return this._symbolId;
    };


    /**
     * @public
     * @inheritDoc
     * @param {number} posIndex 
     */
    setPosIndex(posIndex)
    {
        this._posIndex = posIndex;
        this.updateDebugInfoText();
    };


    /**
     * @public
     * @inheritDoc
     * @param {number} reelIndex 
     */
    setReelIndex(reelIndex)
    {
        this._reelIndex = reelIndex;
    };


    //--------------------------------------------------------------------------------------------------
    // Tall Symbol methods
    //--------------------------------------------------------------------------------------------------
    // These methods are called externally, when we need to show the Symbol in one of serveral Tall
    // Symbol related modes
    // - NORMAL: its a normal symbol, and we see it
    // - TALL: This symbol is shown in Tall mode
    // - INVOLVED_IN_TALL: The symbol position would be involved in a Tall Symbol, but we actually do
    //   not want the symbol to be seen - because a separate symbol next to it, is set as Tall. Instead
    //   of just hiding the symbol, we use INVOLVED_IN_TALL as a special semantic marker. Who knows if
    //   we will need to do something slightly different in that method in the future ?
    // - PARTIALLY_TALL: This is a special reserved state. In future, suppose we had a 3 tall symbol:
    //   instead of setting one symbol to TALL mode (and the other 2 to INVOLVED_IN_TALL), we might set
    //   all 3 to PARTIALLY_TALL, so each of the 3 symbols would show a section of the tall symbol. That
    //   would work well for cases like Collapsing Symbols where we might want to show tall symbols (and
    //   then remove some of them).
    // 
    // So, we have methods to set the "TallSymbolState" of this symbol. These are parrallel to visual
    // state. The enter actions for the various Visual States will check the current Tall Symbol State,
    // and show the most appropriate details for each visual state. When we change Tall Symbol State, we
    // also re-invoke the enter action for the current Visual State, so for example:
    // - VisualState == INVOLVED_IN_WIN_WITH_ANIMATION (we are showing a win animation)
    // - TallSymState == TALL
    // If we decided to switch TallSymState to NORMAL, we would retrigger the enter action for
    // INVOLVED_IN_WIN_WITH_ANIMATION VisualState (this isn't the best edge case to explain why this is
    // good, but other parallel transitions might be very useful in the future)
    //--------------------------------------------------------------------------------------------------
    /**
     * @public
     * @inheritDoc
     */
    showAsNormalSymbol()
    {
        this.log.debug(`Symbol[r:${this._reelIndex},p:${this._posIndex}].showAsNormalSymbol()`);

        this._tallSymbolState = TallSymState.NORMAL;

        this.zOrder = undefined;

        if (this._tallSymbolMask) {
            this.removeChild(this._tallSymbolMask);
            this._tallSymbolMask.destroy(true);
            this._symbolAnim.mask = null;
            this._symbolTallStatic.mask = null;
            this._tallSymbolMask = null;
        }

        // Ensure that standard symbol bg is visible (if required)
        if (this._symbolBgConfig && this._symbolBackground) {
            this.showSymbolComponent(this._symbolBackground, this._symbolBgConfig);
        }

        // TODO: Its not a problem for this to be here, but really, position must be set
        // in "setupAnimation"
        this._symbolAnim.y = 0;

        this.retriggerCurrentVisualStateEnterAction();
    };


    /**
     * @public
     * @inheritDoc
     * @return {boolean}
     */
    isNormalSymbol() {
        return this._tallSymbolState === TallSymState.NORMAL;
    };


    /**
     * @public
     * @inheritDoc
     * @see {@link SymbolView#showAsInvolvedInTallSymbol}
     */
    showAsInvolvedInTallSymbol()
    {
        this.log.debug(`Symbol[r:${this._reelIndex},p:${this._posIndex}].showAsInvolvedInTallSymbol()`);

        this._tallSymbolState = TallSymState.INVOLVED_IN_TALL;

        // Nothing to do here: the retrigger visual state entry method will take care
        // of showing / hiding the approprate layers. This state is basically syntactic
        // sugar for most cases.
        this.retriggerCurrentVisualStateEnterAction(); 
    };


    /**
     * @public
     * @inheritDoc
     * @return {boolean}
     */
    isInvolvedInTallSymbol() {
        return this._tallSymbolState === TallSymState.INVOLVED_IN_TALL;
    };


    /**
     * @public
     * @inheritDoc
     * @see {@link SymbolView#showAsPartiallyTallSymbol}
     * @param {number} numPosInTallSymbol
     * @param {number} indexWithinTallSymbol
     */
    showAsPartiallyTallSymbol(numPosInTallSymbol, indexWithinTallSymbol)
    {
        this.log.debug(`[r:${this._reelIndex},p:${this._posIndex}].showAsPartiallyTallSymbol(numPosInTallSymbol:${numPosInTallSymbol},index:${indexWithinTallSymbol})`);

        this._tallSymbolState = TallSymState.PARTIALLY_TALL;

        this.retriggerCurrentVisualStateEnterAction();

        // TODO: This mode should be implemented for the standard SymbolView
    };


    /**
     * @public
     * @inheritDoc
     * @return {boolean}
     */
    isPartiallyTallSymbol() {
        return this._tallSymbolState === TallSymState.PARTIALLY_TALL;
    };


    // TODO: The data object which configures Tall Symbol animations, is the same as the one
    // which configures normal win animations. In principle, we can add support for WinFrame
    // and WinBackground in a customized way for tall symbol: once these are added, then the
    // basic functionality is actually complete. (When this task is done, all of this code
    // should get a good internal tidying up: the metaphors about how you configure normal /
    // tall win animations are a bit mixed).
    /**
     * @public
     * @inheritDoc
     * @param {number} numSymInTallSymbol
     * The number of symbols tall that the symbol should be shown, in its tall wild state.
     * @param {boolean} anchorAtBottom 
     * If true, tall symbol goes "upwards" from the bottom symbol (eg: this symbol is the
     * bottom symbol of the tall symbol). If false, tall symbol goes "downwards" (eg: this
     * symbol is the top symbol of the tall symbol).
     */
    showAsTallSymbol(numSymInTallSymbol, anchorAtBottom)
    {
        this.log.debug(`Symbol[r:${this._reelIndex},p:${this._posIndex}].showAsTallSymbol(anchorAtBottom:${anchorAtBottom}, numSymInTallSymbol:${numSymInTallSymbol})`);

        let configForTallSymbol = this._symbolTallConfig;
        let tallSymMaxNumSym = this._symbolsConfig.tallSymMaxNumSym;
        let numSymInTallSymAsset;
        
        // Use an animation that is the exact height that we are targeting, if
        // such an animation exists.
        if (numSymInTallSymbol in configForTallSymbol) {
            numSymInTallSymAsset = numSymInTallSymbol;
            
            this.log.debug(`Symbol[r:${this._reelIndex},p:${this._posIndex}].showAsTallSymbol: use data for ${numSymInTallSymbol} sym tall`);
        }
        // If such an animation doesn't exist, grab the default tallest animation.
        // We will use cropping on it, to shrink it down to the required size.
        else
        {   
            numSymInTallSymAsset = tallSymMaxNumSym;
            
            this.log.debug(`Symbol[r:${this._reelIndex},p:${this._posIndex}].showAsTallSymbol: use full size data and add crop`);
        }

        let staticTextureId = `sym${this._symbolId}_tall${numSymInTallSymAsset}`;
        let configForRequiredNumSym = configForTallSymbol[numSymInTallSymAsset];

       let cropToSymbolWidth = configForRequiredNumSym.tallSymbolCropBySymbolWidth !== undefined ? configForRequiredNumSym.tallSymbolCropBySymbolWidth : true;

        let fullTallSymbolsIsNotShown = numSymInTallSymAsset !== numSymInTallSymbol;

        let offSetPosition = configForRequiredNumSym.partialTallSymbolOffsetPosition !== undefined && fullTallSymbolsIsNotShown ? configForRequiredNumSym.partialTallSymbolOffsetPosition : {x:0, y:0};

        let animOffSetPosition = configForRequiredNumSym.winAnimation.fullTallAnimOffsetPosition !== undefined && !fullTallSymbolsIsNotShown ? configForRequiredNumSym.winAnimation.fullTallAnimOffsetPosition : offSetPosition;

        // Determine the target height of the symbol, taking symGap into account (if we
        // have a symGap value greater than 0, then tall symbol assets should be designed
        // to take this into account).
        let shouldCropSymbol = fullTallSymbolsIsNotShown;
        let symWidth = this.app.config.symWidth;
        let symHeight = this.app.config.symHeight;
        let symGap = this.app.config.symGap;
        let reelGap = this.app.config.reelGap;
        let tallSymAssetHeight = (symHeight * numSymInTallSymAsset) + (symGap * (numSymInTallSymAsset - 1));
        let tallSymVisibleHeight = (symHeight * numSymInTallSymbol) + (symGap * (numSymInTallSymbol - 1));

        // Cache some usefull local state
        this._tallSymbolState = TallSymState.TALL;
        this._numSymInTallSymVisually = numSymInTallSymbol;
        this._numSymInTallSymAsset = numSymInTallSymAsset;
        this._tallSymIsCropped = shouldCropSymbol;
        
        // First, we can configure the static symbol easily.
        this._symbolTallStatic.texture = this.assets.getTexture(staticTextureId);

        // Now, configure animations. This is like the "normal" animation: we may have an animation,
        // we may not. We may also support "shine" modes.

        /** @type {SymbolAnimationConfig} */
        let winAnimConfig = null;
        if (configForRequiredNumSym.winAnimation) {
            winAnimConfig = configForRequiredNumSym.winAnimation;
        }
        else
        {
            // Default config is basically a shine effect
            winAnimConfig = DEFAULT_SYMBOL_VIEW_CONFIG.winAnimation;
        }

        this._symbolTallAnimConfig = winAnimConfig;

        let tallSymAnchorX = winAnimConfig.anchor? winAnimConfig.anchor.x : 0.5;
        let tallSymAnchorY = winAnimConfig.anchor? winAnimConfig.anchor.y : 0.5;

        // Russell (20.04.2020)
        // This property seems to be used by ReelGroup, to reorganize the symbols, ensuring that the
        // tall symbol appears above others. NOTE: This is basically a public property. This needs
        // changing though:
        // 1) The property, what it is for and how it is used, must be documented
        // 2) The implementation is wrong. setting zOrder to 2 (eg: its index 2, where zOrder of 0
        //    is the lowest) is incorrect: it assumes that the reel has 3 symbols (this will not
        //    always be true), and it also assumes that there will only be 1 tall symbol on the reel,
        //    or 1 symbol that must be shown above others. Really, the zOrdering should be handled by
        //    ReelGroup, which has the responsibility for this (Symbol should know as little as possible
        //    about the context in which it is displayed). Alternatively, ReelView knows how many symbols
        //    there are on the reel, and is probably the appropriat component to perform z-ordering.
        this.zOrder = 2;

        
        if (fullTallSymbolsIsNotShown)
        {
            // When cropping, we must use a custom mechanism for determing y anchor position. When we
            // are anchoring at the top, this is simple (it's the same as if we were not cropping).
            // When anchoring at the bottom, we need to take into account the fact that our raw asset
            // is taller than the target height (so we are calculating a proportional vertical anchor
            // within the space of the actual height of the asset, but must calculate the correct
            // visual value for the visible height of the cropped asset).
            if (anchorAtBottom)
            {
                tallSymAnchorY = (tallSymVisibleHeight - (symHeight * 0.5)) / tallSymAssetHeight;
            }
            else
            {
                tallSymAnchorY = (symHeight * 0.5) / tallSymAssetHeight;
            }
        }
        else
        {
            // Not cropping, therefore we must correctly work out the anchor, based
            // on how tall the symbol is
            // - if sym is 2 tall, and we anchor "at top", that is 0.25
            // - if sym is 2 tall, and we anchor "at bottom", that is 0.75
            // - if sym is 3 tall, and we anchor "at top", that is 0.167
            // - if sym is 3 tall, and we anchor "at bottom", that is 0.833
            // However, we also have "symGap" to take into account. It's easiest
            // then to work out anchor, based on target height of the tall symbol,
            // and the position at which we know it's vertical anchor must be.

            if (anchorAtBottom)
            {
                tallSymAnchorY = 1 - ((symHeight * 0.5) / tallSymAssetHeight);
            }
            else
            {
                tallSymAnchorY = (symHeight * 0.5) / tallSymAssetHeight;
            }
        }
        
        //------------------------------------------------------------------------------
        // Update the scale of the symbol to the correct new height. This matches
        // the symbol's height to the actual expected height of the asset (which
        // may be different from the target height that the symbol should be seen
        // at, if cropping is being applied).
        //------------------------------------------------------------------------------
        // Reset tall symbol assets to default scale
        this._symbolTallStatic.scale.set(1,1);
        this._symbolAnim.scale.set(1,1);

        // Now, we can proportionately scale them, in an accurate way. We could set width /
        // height independently to exact values, but we may want to support "custom scaling",
        // where we adjust the scale of one asset (eg: the animation) to a percentage of its
        // correct size, in which case, it's simpler to just reset scale, then proportionately
        // size the assets, like this.
        this.layout.setHeight(this._symbolTallStatic, tallSymAssetHeight);
        this.layout.setHeight(this._symbolAnim, tallSymAssetHeight);

        // Update anchor and y positions
        // TODO: Support "custom anchoring", although it needs a bit of working out, exactly
        // how this would be implemented: presumably, custom anchor would treat (0.5,0.5) as
        // the center of the whole tall symbol area, rather than a specific symbol which is
        // a member of the tall symbol: but, it's exactly these details that must be fleshed
        // out before it can be handled properly.
        this._symbolTallStatic.anchor.set(tallSymAnchorX, tallSymAnchorY);
        this._symbolTallStatic.x = offSetPosition.x;
        this._symbolTallStatic.y = offSetPosition.y;

        this._symbolAnim.anchor.set(tallSymAnchorX, tallSymAnchorY);


        this._symbolAnim.x = animOffSetPosition.x;
        this._symbolAnim.y =  animOffSetPosition.y;

        this.log.debug(`Symbol[r:${this._reelIndex},p:${this._posIndex}].showAsTallSymbol(anchor:(x:${this._symbolAnim.anchor.x},y:${this._symbolAnim.anchor.y}))`);
        this.log.debug(`Symbol[r:${this._reelIndex},p:${this._posIndex}].showAsTallSymbol(y:${this._symbolAnim.y})`);
        
        // TODO:
        // Masking could perform poorly on webgl, especially if we consider future use cases
        // of this: imagine a 6 * 4 game, with lots of tall symbols potentially appearing
        // (eg: we could have 2 tall symbols on the same reel in this simple example, for a
        // maximum of 12 tall symbols). That's 12 masks within the reels, just to show the
        // symbols correctly (not counting any other masks involved in the game). There MUST
        // be a better way of handling this?
        if (shouldCropSymbol)
        {
           
            let maskTargetWidth = cropToSymbolWidth ? symWidth : symWidth + (reelGap * 2);
            let maskTargetHeight = tallSymVisibleHeight;
            let maskTargetPosX = cropToSymbolWidth ?  -symWidth * 0.5 : -symWidth * 0.5 - reelGap;
            let maskTargetPosY = 0 - (tallSymAnchorY * tallSymAssetHeight);
    
            let mask = new PIXI.Graphics();
            mask.beginFill(0xffffff, .5);
            mask.drawRect(maskTargetPosX, maskTargetPosY, maskTargetWidth, maskTargetHeight);
            mask.endFill();
                
            this._symbolAnim.mask = mask;
            this._symbolTallStatic.mask = mask;
    
            /**
             * Cached reference to a tall symbol mask.
             * @private
             * @type {PIXI.Graphics}
            */
            this._tallSymbolMask = mask;
                
            this.addChild(mask);     
        }
        
        // Setup size of tall symbol rect independently. It is not necessarily the size of
        // the mask (if a mask exists), as the symbol can be a larger area: its the area that
        // we size the symbol to
        this._symbolTallRect.width = this._symbolTallStatic.width;
        this._symbolTallRect.height = this._symbolTallStatic.height;
        this._symbolTallAnchorX = tallSymAnchorX;
        this._symbolTallAnchorY = tallSymAnchorY;

        // TODO: We might need to explicitly set the "reconfigure skeletal symbol" feature here ?
        // Swapping symbolId is where it is currently done, and this is probably sufficient. But,
        // are there some edge cases here where that is not the case?

        this.retriggerCurrentVisualStateEnterAction();
    };


    /**
     * @public
     * @inheritDoc
     * @return {boolean}
     */
    isTallSymbol() {
        return this._tallSymbolState === TallSymState.TALL;
    };


    /**
     * @public
     * @inheritDoc
     * @param {boolean} shouldShow 
     */
    showDebugInfo(shouldShow)
    {
        if (shouldShow && !this._debugInfoText)
        {
            /**
             * Text field which shows debug info about a symbol.
             * @private
             * @type {PIXI.Text}
             */
            this._debugInfoText = new PIXI.Text("", {
                fontFamily : "Arial",
                fill : 0xffffff,
                fontSize : Math.round(this._symbolNormalRect.height * 0.10),
                stroke : 0x000000,
                strokeThickness : Math.round(this._symbolNormalRect.height * 0.05),
                align : "center"
            });
            this._debugInfoText.anchor.set(0.5,0.5);
            this._debugInfoText.x = 0;
            this._debugInfoText.y = 0;
            this.addChild(this._debugInfoText);
        }
        else
        if (!shouldShow && this._debugInfoText)
        {
            this._debugInfoText.parent.removeChild(this._debugInfoText);
            this._debugInfoText.destroy();
            this._debugInfoText = null;
        }
    };


    /**
     * Updates any debug info text (but only if its currently shown) - the method is safe
     * to call at any time.
     * @private
     */
    updateDebugInfoText()
    {
        if (this._debugInfoText)
        {
            let textString = 
                `Sym:${this._symbolId},posIndex:${this._posIndex}\nstate:${this._currVisualState},tallSym:${this._tallSymbolState}`;

            if (this._tallSymbolState === TallSymState.TALL) {
                textString += `\nnumTallSym:${this._numSymInTallSymVisually}\ncropped:${this._tallSymIsCropped}`;
            }

            this._debugInfoText.text = textString;
        }
    };


    //--------------------------------------------------------------------------------------------------
    // Internal animation utility methods
    //--------------------------------------------------------------------------------------------------
    // TODO: Document this section better - there are still references (in my comments) to
    // "i dont know what this does", and it refers to the shine filters (which are applied
    // for the "shine the symbol win animation", used when there isn't a frame based win
    // animation that is specified)
    
    /**
     * Sets up the win animation to show for this symbol, when it is in "NORMAL" state (eg: its not
     * a Tall Symbol, which has a different visual layer for showing win animations). Internally,
     * this method tracks some local state ("dirty" flags)
     * @private
     */
    setupNormalWinAnimation()
    {
        this.setupWinAnimation(false);
    };


    /**
     * "Sets up" the Tall Win animation. This actually does very little: tall win animations are
     * currently configured when the symbol is first set to tall symbol state. This method simply
     * picks which layers need to be visible (if animation is not being used, but other filter
     * visual effects are used instead, then we use "static" layer as visible, instead of "anim"
     * layer). The method primarily exists to match the current naming conventions for "normal" state.
     * This convention can be refactored in future updates, to make this module more readbale.
     * @private
     */
    setupTallWinAnimation()
    {
        this.setupWinAnimation(true);
    };

    /**
     * Sets up the winline symbol animation. 
     */
    setupWinlineAnimation()
    {
        this.log.debug(`Symbol[r:${this._reelIndex},p:${this._posIndex}].setupWinlineAnimation()`);

        let symbolId = this._symbolId;
        let winAnimData = this._symbolWinlineAnimConfig;
        let symbolRect = this._symbolNormalRect;
        let symbolStatic = this._symbolNormalStatic;
        let symbolScale = winAnimData.scale || this._symbolsConfig.defaultAnimScale;

        let animAnchorPosX = 0.5;
        let animAnchorPosY = 0.5;

        this._symbolAnim.onComplete = null;
        this._symbolAnim.alpha = 1;

        let animTextures = this.assets.getWinlineSymAnimTextures(symbolId);

        this._symbolAnim.textures = animTextures;
        this._symbolAnim.loop = winAnimData.loop;
        this._symbolAnim.animationSpeed = winAnimData.speed || animTextures.length / 60;
        this._symbolAnim.visible = true;
        this._symbolAnim.anchor.set(animAnchorPosX, animAnchorPosY);

        this.layout.propHeightScale(this._symbolAnim, symbolScale, symbolRect);

        this.log.debug(`SCALE: scale:${symbolScale},rect:${JSON.stringify(symbolRect)},newHeight:${this._symbolAnim.height}`);

    }


    /**
     * @private
     * @param {boolean} useTallSymbolForWinAnimation
     */
    setupWinAnimation(useTallSymbolForWinAnimation)
    {
        this.log.debug(`Symbol[r:${this._reelIndex},p:${this._posIndex}].setupWinAnimation(useTallSymForAnim:${useTallSymbolForWinAnimation})`);

        let isNotFullTallSymbol = useTallSymbolForWinAnimation && this._numSymInTallSymVisually < this._numSymInTallSymAsset;

        let symbolId = this._symbolId;
        let winAnimData = useTallSymbolForWinAnimation? this._symbolTallAnimConfig : this._symbolAnimConfig;
        let symbolRect = useTallSymbolForWinAnimation? this._symbolTallRect : this._symbolNormalRect;
        let symbolStatic = useTallSymbolForWinAnimation? this._symbolTallStatic : this._symbolNormalStatic;
        let symbolScale = winAnimData.scale || this._symbolsConfig.defaultAnimScale;

        let scalePartialTallAnim = true;

        if (useTallSymbolForWinAnimation && this._numSymInTallSymAsset > 0)
        {
            scalePartialTallAnim = this._symbolTallConfig[this._numSymInTallSymAsset].partialTallSymbolScaleAnim !== undefined ? this._symbolTallConfig[this._numSymInTallSymAsset].partialTallSymbolScaleAnim : true;
        }

        if (isNotFullTallSymbol && !scalePartialTallAnim)
        {
            symbolScale = this._symbolsConfig.defaultAnimScale;
        }

        let mask = (useTallSymbolForWinAnimation && this._tallSymbolMask)? this._tallSymbolMask : null;

        // TODO: Custom anchors are probably neither desired, nor supportable at present.
        // However... now i need to check if anyone is using them (they shouldn't be)
        /*
        let animAnchor = winAnimData.anchor;
        let animAnchorPosX = animAnchor? animAnchor.x : 0.5;
        let animAnchorPosY = animAnchor? animAnchor.y : 0.5;
        */

        let animAnchorPosX = useTallSymbolForWinAnimation? this._symbolTallAnchorX : 0.5;
        let animAnchorPosY = useTallSymbolForWinAnimation? this._symbolTallAnchorY : 0.5;

        this._symbolAnim.onComplete = null;
        this._symbolAnim.alpha = 1;

        if (winAnimData.type === "animated")
        {
            let animTextures =  useTallSymbolForWinAnimation?
                this.assets.getTallSymAnimTextures(symbolId, this._numSymInTallSymAsset) :
                this.assets.getSymAnimTextures(symbolId);

            this._symbolAnim.textures = animTextures;
            this._symbolAnim.loop = winAnimData.loop;
            this._symbolAnim.animationSpeed = winAnimData.speed || animTextures.length / 60;
            this._symbolAnim.visible = true;
            this._symbolAnim.anchor.set(animAnchorPosX, animAnchorPosY);

            // "mask" can be null, so this could either be setting or clearing the mask, as required
            this._symbolAnim.mask = mask;
            
            this._symbolNormalStatic.visible = false;
            
            if (this._symbolTallStatic) {
                this._symbolTallStatic.visible = false;
            }
        }
        else
        if (winAnimData.type === "shine")
        {
            symbolStatic.filters = [this._shineFilter];
            symbolStatic.visible = true;
            symbolStatic.mask = mask;

            this._shineFilter.uniforms.position = 0;
            this._shineFilter.uniforms.intensity = 0;
        }
        // Igor committed this to dev branch, but reported to me that it's not actually production
        // ready. It needs finalizing into a usable format asap.
        else
        if (winAnimData.type === "skeletal")
        {
            if (this._skeletalWinAnimationRequiresSetup)
            {
                this._skeletalWinAnimationRequiresSetup = false;

                let armatureName = winAnimData.armatureName;
                let dragonBonesDataName = winAnimData.dragonBonesDataName;

                let dbArmature = this.dragonBones.buildArmatureDisplay(armatureName, dragonBonesDataName);
                let dbContainer = this.layout.createDragonBonesContainer(dbArmature);

                // To position the DragoneBones animation, we actually need to know how big it is meant to
                // be, as it's inside a container (for reasons that are not really well explained in Wiener's
                // code, but it seems Dragonbones Pixi uses unexpected scaling).
                let targetHeight = symbolScale/100 * symbolRect.height;
                let scaleRatio = targetHeight / dbContainer.height;
                let targetWidth = dbContainer.width * scaleRatio;

                // Our DragonBones animation seems to be top-left anchored (and its placed inside a container,
                // which has no anchor): therefore, we must position it manually, remembering that {x:0,y:0}
                // is our "center point"
                dbContainer.x = -(targetWidth  * animAnchorPosX);
                dbContainer.y = -(targetHeight * animAnchorPosY);
                this.layout.propHeightScale(dbContainer, symbolScale, symbolRect);

                this._symbolAnimSkeletal = dbContainer;

                // It's always a new armature, so we need to add it to the display-list
                this.addChildAt(this._symbolAnimSkeletal, this._symbolAnimDisplayIndex);
            }

            // "mask" can be null, so this could either be setting or clearing the mask, as required
            this._symbolAnimSkeletal.mask = mask;

            this._symbolAnimSkeletal.visible = true;
            this._symbolAnim.visible = false;
            this._symbolNormalStatic.visible = false;

            if (this._symbolTallStatic) {
                this._symbolTallStatic.visible = false; // TODO: redundant to call here? Or required ?
            }
        }
        
        this.layout.propHeightScale(this._symbolAnim, symbolScale, symbolRect);

        this.log.debug(`SCALE: scale:${symbolScale},rect:${JSON.stringify(symbolRect)},newHeight:${this._symbolAnim.height}`);
    }
    

    // TODO: "This method needs better documentation, yes it plays a win animation,
    // but there is apparently quite a few things involved in doing that. If its
    // changing local state of SymbolView, then we need to explain what and why."
    // - these are old comments (pre september 2020), but they still hold true,
    //   the method DOES need fully explaining
    /**
     * Starts the Symbol View's animation going.
     * @private
     */
    playNormalWinAnimation()
    {
        let winAnimData = this._symbolAnimConfig;

        this.playSpecificWinAnimation(winAnimData, this._symbolNormalStatic);
    };


    /**
     * Plays the Tall Symbol Win Animation: should only be called when a Tall Symbol state is active.
     * If no Tall Symbol state is shown, then this method will have no effect.
     * @private
     */
    playTallWinAnimation()
    {
        let winAnimData = this._symbolTallAnimConfig;

        if (!winAnimData) {
            return;
        }

        this.playSpecificWinAnimation(winAnimData, this._symbolTallStatic);
    };


    /**
     * Plays the winline symbol animation
     * @private
     */
    playWinlineSymbolAnimation()
    {
        let winAnimData = this._symbolWinlineAnimConfig;

        this.playSpecificWinAnimation(winAnimData, this._symbolNormalStatic);
    }



    // TODO: Document the parameters for this method! Looks like "staticTarget" is for cases
    // where we are using filter type animations (on the static layer of the symbol), which
    // of course could be normal sized static symbol / tall static symbol. If it was only
    // those 2, then we might configure the parameters differently - needs designing and
    // then thinking through
    /**
     * Plays the appropriate animation for the Symbol Anim layer, using the given anim config.
     * Can be called for either normal or tall symbol animation states.
     * @private
     * @param {SymbolAnimationConfig} winAnimData
     * @param {PIXI.Sprite} staticTarget
     */
    playSpecificWinAnimation(winAnimData, staticTarget)
    {
        // TODO: If visibility on anim related layers is being set here, then we probably
        // need more specific configurations.

        if (winAnimData.type === "animated")
        {
            this._symbolAnim.gotoAndPlay(0);
        }
        else
        if (winAnimData.type === "shine")
        {
            TweenMax.to(this._shineFilter.uniforms, 1.5, { position: 1, yoyo: true, repeat: winAnimData.loop ? -1 : 3 });
            TweenMax.to(this._shineFilter.uniforms, 0.2, { intensity: 2 });
            
            if (!winAnimData.loop)
            {
                TweenMax.to(this._shineFilter.uniforms, .2, {intensity: 0, delay: 6});
            }

            // Proposed "bouncing scale" animation effect: needs to be properly revertable before
            // this can actually be deployed as a usable feature. To be revertable, some of this
            // code needs to get tidied up!!
            /*
            if (winAnimData.bounceScale > 0)
            {
                let startScaleX = staticTarget.scale.x;
                let startScaleY = staticTarget.scale.y;
                let finalScaleX = startScaleX * winAnimData.bounceScale;
                let finalScaleY = startScaleY * winAnimData.bounceScale;

                TweenMax.fromTo(staticTarget.scale, 1.5,
                    { x:startScaleX, y:startScaleY },
                    { x:finalScaleX, y:finalScaleY });
            }
            */
        }
        else
        if (winAnimData.type === "skeletal")
        {
            // TODO: We could potentially support the playing of multiple animations.
            // In practise, this needs some further designing, regarding exactly what
            // it is that we are trying to achieve by doing that
            this._symbolAnimSkeletal.children[0].animation.play();
        }
        /*
        else
        if (winAnimData.type === "fade")
        {
            // TODO: The original logic actually used "half of win animation time" as the time of this
            // anim (it wasn't configurable in a further way), but we now have 2 or more potential win anim
            // times ("quick", "idle", "big win"), so that setup is no longer appropriate.
            TweenMax.from(this._symbolAnim, 2.0, { alpha:0, yoyo:true, repeat:winAnimData.loop? -1: 1 });
        }
        */
    };


    /**
     * Sets the symbol into a faded or non-faded state. Uses an animated transition. This does
     * no internal checking of whether any fade is already applied: it only guarantees to clear
     * any existing fade animations. Therefore, it should only be called (internally within this
     * class) when it is semantically correct to do so. The method checks for the appropriate
     * target for fading, based on whether the symbol is currently showing a tall symbol or not
     * (it makes assumptions that the TallSymbol static layer, or the NormalSymbol stat layer,
     * will be shown in each case).
     * @private
     * @param {boolean} symbolShouldBeFaded 
     * Indicates whether the symbol should be faded or not (true == should be faded, false == should
     * be shown in "normal" fade state, no fade effect applied).
     */
    setFadeState(symbolShouldBeFaded)
    {
        this.log.debug(`Symbol[r:${this._reelIndex},p:${this._posIndex}].setFadeState(to:${symbolShouldBeFaded})`);

        let fadeConfig = this._symbolsConfig.symbolFade;
        if (fadeConfig.type === "NONE") {
            return;
        }
        
        let foregroundTarget = this._symbolNormalStatic;
        if (this._tallSymbolState === TallSymState.TALL) {
            foregroundTarget = this._symbolTallStatic;
        }
        
        switch (fadeConfig.type)
        {
            case C_SymbolFadeType.TINT:
            {
                let targetTintColour = symbolShouldBeFaded ? fadeConfig.tintColour : 0xffffff;
                let tintDuration = fadeConfig.duration >= 0? fadeConfig.duration : 0.2;
                
                // Currently, v4 onwards of pixi do not support tints applied directly to a container
                // (although its technically perfectly possible). There may be plugins which handle it,
                // but for now, we simply apply the tint manually on all required layers
                VisualUtil.tintTween(foregroundTarget, targetTintColour, tintDuration);

                if (fadeConfig.applyToBackground && this._symbolBackground) {
                    VisualUtil.tintTween(this._symbolBackground, targetTintColour, tintDuration);
                }

                break;
            }

            case C_SymbolFadeType.ALPHA:
            {
                let target = fadeConfig.applyToBackground? this : foregroundTarget;

                let targetAlpha = symbolShouldBeFaded ? fadeConfig.alpha : 1;
                let alphaDuration = fadeConfig.duration >= 0? fadeConfig.duration : 0.1;
                TweenMax.to(target, alphaDuration, {alpha:targetAlpha, overwrite:1});
                break;
            }
        }
    };


    /**
     * Shows a specific symbol component. Multiple symbol components may exist, but all use the same
     * basic configuration data for determing how the symbol component looks (and how it is scaled),
     * so we have a single method which accepts a target display object, and the visual config data.
     * @private
     * @param {PIXI.extras.AnimatedSprite} symbolComponent
     * The target display object representing the symbol component.
     * @param {SymbolComponentConfig} symbolComponentConfig 
     */
    showSymbolComponent(symbolComponent, symbolComponentConfig)
    {
        // Ensure any existing tweens for this symbol component are killed.
        TweenMax.killTweensOf(symbolComponent);

        if (symbolComponentConfig.type === "invisible")
        {
            symbolComponent.visible = false;
            return;
        }

        symbolComponent.visible = true;

        // Set initial positioning
        // TODO: Support the custom anchor property, decide actually how it is meant to work!!
        // Wiener added something like this originally, so it still exists, but they didn't
        // document how it was actually supposed to work.
        symbolComponent.anchor.set(0.5, 0.5);
        symbolComponent.x = 0;
        symbolComponent.y = 0;        

        if (symbolComponentConfig.type === "static")
        {
            symbolComponent.textures = [this.assets.getTexture(symbolComponentConfig.assetName)];
            symbolComponent.gotoAndStop(0);
            symbolComponent.loop = false;
        }
        else
        if (symbolComponentConfig.type === "animated")
        {
            let prefix = symbolComponentConfig.prefix;
            let padLen = symbolComponentConfig.padLen;
            let startFrame = symbolComponentConfig.startFrame;
            let finalFrame = symbolComponentConfig.finalFrame;
            let textures = this.assets.generateFrames(prefix, padLen, startFrame, finalFrame);
            let numFrames = textures.length;
            let animSpeed = symbolComponentConfig.speed > 0? symbolComponentConfig.speed : numFrames / 60;
            
            symbolComponent.textures = textures;
            symbolComponent.loop = symbolComponentConfig.loop;
            symbolComponent.animationSpeed = animSpeed;
            symbolComponent.gotoAndPlay(0);
        }

        // Set the scale of the win component to the appropriate value.
        // For a PIXI.animatedSprite, this must be done AFTER any textures
        // are updated (as apparently this causes the animatedSprite to
        // change it's internal scaling)
        let targetScale = 100;
        if (symbolComponentConfig.scale > 0) {
            targetScale = symbolComponentConfig.scale;
        }

        // TODO: With some tweaks, we can also use this on tall symbols.
        this.layout.propHeightScale(symbolComponent, targetScale, this._symbolNormalRect);
        
        // Perform any fade in animation required.
        let useFadeIn = symbolComponentConfig.fadeInTime > 0;
        if (useFadeIn)
        {
            let deltaAlpha = 1 - symbolComponent.alpha;
            let fadeInDuration = deltaAlpha * symbolComponentConfig.fadeInTime;
            TweenMax.to( symbolComponent, fadeInDuration, {alpha:1}, "Sine.easeOut");
        }
        else
        {
            symbolComponent.alpha = 1;
        }
    };


    /**
     * Hides a specific symbol component. Multiple symbol components may exist, but all use the same
     * basic configuration data, so we have a single method which accepts a target display object, and
     * the visual config data.
     * @private
     * @param {PIXI.extras.AnimatedSprite} symbolComponent 
     * @param {SymbolComponentConfig} symbolComponentConfig 
     */
    hideSymbolComponent(symbolComponent, symbolComponentConfig)
    {
        TweenMax.killTweensOf(symbolComponent);

        let useFadeOut = symbolComponentConfig.fadeOutTime > 0;
        if (useFadeOut)
        {
            let alphaDelta = Math.abs(0 - symbolComponent.alpha);
            let fadeOutDuration = alphaDelta * symbolComponentConfig.fadeOutTime;
            
            TweenMax.to(
                symbolComponent,
                fadeOutDuration,
                {
                    alpha:0,
                    onComplete : () => {
                        symbolComponent.visible = false;
                        symbolComponent.stop();
                    }
                },
                "Sine.easeIn");
        }
        else
        {
            symbolComponent.visible = false;
        }
    };


    // TODO: Expand the method name, so that it becomes a bit clearer.
    /**
     * Removes all filters applied to symbols.
     * @private
     */
    removeFilters()
    {
        this._symbolNormalStatic.filters = [];
    };
};

export default Symbol