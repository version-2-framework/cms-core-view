import BaseView from "./BaseView";
import C_GameEvent from "../../../cms-core-logic/src/const/C_GameEvent";
import C_PointerEvt from "../const/C_PointerEvt";
import Button from "../ui/Button";

/**
 * "UITexts" is the bottom infobar, which displays information about 
 */
class UITexts extends BaseView
{
    constructor()
    {
        super();
    };


    /**
     * This appears to be public, but its totally undocumented by Wiener why its public, why
     * we expect it to be called externally to this class.
     * @public
     */
    create()
    {
        this.createBackground();
        this.configure();
        this.createTextFields();
        this.createClock();
    };


    /**
     * @private
     */
    configure()
    {
        let titleY = 97.5;
        let valueY = 97.5;

        let uiStyle = this.config.uiStyle;
        let textFont = uiStyle.uiFont;
        let textSize = 3;

        let titleColor = uiStyle.uiStats.titleColor;
        let valueColor = uiStyle.uiStats.valueColor || titleColor;
        let clockColor = uiStyle.uiStats.clockColor;

        let sizeLimits = {w:10, h:4};

        this.clockTxtProps = {
            h: textSize,
            font: textFont,
            colour: clockColor
        };

        this.textFieldsData =
        [
            // Coins title/value
            {
                h:textSize, anchor:{x:0, y:0}, color:titleColor,
                font:textFont, sizeLimits:sizeLimits, translateId:"T_infobar_wallet"
            },
            {
                isValue: true, h:textSize, anchor:{x:0, y:0}, color:valueColor,
                font:textFont, sizeLimits:sizeLimits,
                update: () =>
                {
                    let returnValue = "";
                    if (this.model.isSessionInProgress()) {
                        let rawWallet = this.model.getPlayerWallet();
                        let validatedWallet = this.getValidatedValueFor(rawWallet);
                        if (validatedWallet != null) {
                            returnValue = this.model.formatCurrency(validatedWallet);
                        }
                    }
                    return returnValue;
                },
                updateEvents:[
                    C_GameEvent.SESSION_OPENED,
                    C_GameEvent.SESSION_CLOSED,
                    C_GameEvent.WALLET_CHANGED,
                    C_GameEvent.GAME_FINISHED
                ]
            },

            // Total stake title/value
            {
                h:textSize, anchor:{x:0, y:0}, color:titleColor,
                font:textFont, sizeLimits:sizeLimits,
                translateId:"T_infobar_totalBet",

            },
            {
                isValue: true, h:textSize, anchor:{x:0, y:0},color:valueColor,
                font:textFont, sizeLimits:sizeLimits,
                update:() =>
                {
                    let returnValue = "";
                    if (this._model.isSessionInProgress()) {
                        let rawTotalBet = this.model.getTotalBet();
                        let validatedTotalBet = this.getValidatedValueFor(rawTotalBet);
                        if (validatedTotalBet != null) {
                            returnValue = this.model.formatCurrency(validatedTotalBet);
                        }
                    }
                    return returnValue;
                },
                updateEvents:[
                    C_GameEvent.SESSION_OPENED,
                    C_GameEvent.SESSION_CHANGED,
                    C_GameEvent.BET_SETTINGS_CHANGED
                ]
            },

            // Winnings title/value
            {
                h:textSize, anchor:{x:0, y:0},color:titleColor,
                font:textFont, sizeLimits:sizeLimits,
                translateId:"T_infobar_winnings"
            },
            {
                isValue: true, h:textSize, anchor:{x:0, y:0}, color:valueColor,
                font:textFont, sizeLimits:sizeLimits,
                update:() =>
                {
                    let returnValue = "";
                    if (this.model.isSessionInProgress()) {
                        let validatedValue = this.getValidatedValueFor(this.model.getPlayerWinnings());
                        if (validatedValue != null) {
                            returnValue = validatedValue.toString();
                        }
                    }
                    return returnValue;
                },
                updateEvents:[
                    C_GameEvent.SESSION_OPENED,
                    C_GameEvent.SESSION_CHANGED,
                    C_GameEvent.WINNINGS_CHANGED
                ]
            }
        ];

        // If Superbet supported for the game client, add the Superbet field
        let addSuperbet = this._config.permanentlyShowSuperbetField;
        if (addSuperbet) {
            this.textFieldsData.push({
                h:textSize, anchor:{x:0,y:0}, color:titleColor,
                font:textFont, sizeLimits:sizeLimits, translateId:"T_infobar_superbet"
            });

            this.textFieldsData.push({
                isValue:true, h:textSize, anchor:{x:0,y:0}, color:valueColor,
                font:textFont, sizeLimits:sizeLimits,
                update:() =>
                {
                    let returnValue = "";
                    if (this.model.isSessionInProgress()) {
                        let validatedValue = this.getValidatedValueFor(this.model.getPlayerSuperbet());
                        if (validatedValue != null) {
                            returnValue = validatedValue.toString();
                        }
                    }
                    return returnValue;
                },
                updateEvents:[
                    C_GameEvent.SESSION_OPENED,
                    C_GameEvent.SESSION_CHANGED,
                    C_GameEvent.SUPERBET_CHANGED
                ]
            });
        }
    }


    /**
     * Takes a raw value from model, determines the numerical value that should be shown: this will handle error
     * cases (where the rawValue might be undefined, null, less than 0, eg: unusable) gracefully, by returning an
     * appropriate value to display in the error case.
     * @private
     * @param {*} rawValue 
     * @returns {number|null}
     * Null if the input is unusable (in this scenario, the infobar should render a blank string for the value: we
     * are likely to be in an error case). 0 if the value was less than 0 (its still an error case, but wrapping to
     * 0 is the determined outcome for this scenario.
     */
    getValidatedValueFor(rawValue)
    {
        if (rawValue === null || Number.isNaN(rawValue)) {
            return null;
        }
        else
        if (rawValue < 0) {
            return 0;
        }
        else return rawValue;
    }

    /**
     * Creates the backrgound component of the ui bar, and adds it to the display list.
     * @private
     */
    createBackground()
    {
        let styleConfig = this.config.uiStyle.uiStatsBg;
        let layoutConfig = this.getActiveGuiLayout().uiStats;

        /**
         * The background behind the whole of the ui bar.
         * @private
         */
        this.uiBarBg = new PIXI.Graphics();
        this.uiBarBg.beginFill(styleConfig.bgColor);
        this.uiBarBg.drawRect(0, 0, this.layout.relWidth(layoutConfig.width), this.layout.relHeight(layoutConfig.height));
        this.uiBarBg.endFill();
        
        this.layout.horizontalAlignCenter(this.uiBarBg, layoutConfig.centerOffset);
        this.layout.verticalAlignBottom(this.uiBarBg, layoutConfig.bottomOffset);
        this.addChild(this.uiBarBg);
    };


    /**
     * Creates all text fields on the ui bar, and adds them to the display list.
     * @private
     */
    createTextFields()
    {
        this.textFields = [];
        let tempContainer = new PIXI.Container();
        let bringNextContainer = false;
        let containerCounter = 0;

        for (let tfIdx = 0; tfIdx < this.textFieldsData.length; tfIdx++)
        {
            let tfData = this.textFieldsData[tfIdx];

            let text = tfData.translateId ? this.locale.getString(tfData.translateId) : "";
            //Added padding = 0. Style property to disable the global setting. I can't figure out why adding padding > 0 ruins the layout
            //This is safe and backwards compatible
            let textField = this.assets.getTF(text, this.layout.relHeight(tfData.h), tfData.font, tfData.color, null, null, null, 0);
            textField.anchor.set(tfData.anchor.x, tfData.anchor.y);

            if (tfData.isValue)
            {
                textField.x = this.textFields[tfIdx - 1].x + this.textFields[tfIdx - 1].width * 1.03;
                bringNextContainer = true;
            }

            textField.x += textField.width * .5;

            textField.update = tfData.update;
            textField.sizeLimits = tfData.sizeLimits;

            tempContainer.addChild(textField);

            if (bringNextContainer)
            {
                bringNextContainer = false;

                this.layout.propHeightScale(tempContainer, 85, this.uiBarBg);
                this.layout.horizontalAlignCenter(tempContainer, this.config.uiStyle.statsOffsets[containerCounter], this.uiBarBg);
                tempContainer.x -= tempContainer.width * .5;
                this.layout.verticalAlignMiddle(tempContainer, 2, this.uiBarBg);

                this.addChild(tempContainer);
                tempContainer = new PIXI.Container();
                containerCounter++;
            }

            if (tfData.updateEvents)
            {
                for (let evtIdx=0; evtIdx < tfData.updateEvents.length; evtIdx++)
                {
                    this.dispatcher.on(tfData.updateEvents[evtIdx], this.update.bind(this));
                }
            }

            this.textFields.push(textField);
        }

        this.update();
    };


    /**
     * Creates the clock text, and adds it to the display list.
     * @private
     */
    createClock()
    {
        this.clockTF = this.assets.getTF('', this.layout.relHeight(this.clockTxtProps.h), this.clockTxtProps.font, this.clockTxtProps.colour);
        this.layout.propHeightScale(this.clockTF, 85, this.uiBarBg);
        this.layout.horizontalAlignLeft(this.clockTF, 1.5, this.uiBarBg);
        this.layout.verticalAlignMiddle(this.clockTF, 0, this.uiBarBg);
        this.addChild(this.clockTF);

        /**
         * Reference to the tween which updates our clock text.
         * @private
         */
        this.clockUpdateTween = TweenMax.to({}, 1, {repeat: -1, onUpdate: this.updateClockText.bind(this)});
    };


    /**
     * Returns current time as a formatted string, ready to be shown on the clock text.
     * @private
     * @return {string}
     */
    getTimeString()
    {
        return new Date().toLocaleTimeString("en-GB", {hour: '2-digit', minute:'2-digit'});
    };


    /**
     * Updates the text on the clock field.
     * @private
     */
    updateClockText()
    {
        this.clockTF.text = this.getTimeString();
    };


    // What is "update"? Why is it here, and what does it do ???
    // What does it do that "updateMethod" does not do ? Why would the code be written in this way?
    // Why is there no documentation?
    /**
     * @private
     */
    update()
    {
        for (let tfIdx=0; tfIdx < this.textFields.length; tfIdx++)
        {
            let tf = this.textFields[tfIdx];
            if (tf.update)
            {
                let newValue = tf.update();
                tf.text = newValue;
                if (tf.sizeLimits)
                {
                    this.layout.limitSize(tf, tf.sizeLimits.w, tf.sizeLimits.h);
                }
            }
        }
    };


    /**
     * @public
     * @override
     * @inheritDoc
     */
    destroy()
    {
        super.destroy();

        if (this.clockUpdateTween) {
            this.clockUpdateTween.kill();
            this.clockUpdateTween = null;
        }
    }
}

export default UITexts