import BaseUI from "../ui/BaseUI";
import Button from "../ui/Button";
import C_PointerEvt from "../const/C_PointerEvt";
import C_TooltipOrientation from "../const/C_TooltipOrientation";
import C_GameEvent from "../../../cms-core-logic/src/const/C_GameEvent";
import C_TooltipPosition from "../const/C_TooltipPosition";
import * as DefaultKeyCodes from "../input/DefaultKeyCodes";
import * as KeyPhase from "../input/KeyPhase";

// TODO: This class needs to support some basic mechanism of localization. Currently, we show
// a single set of textures with standard ids. The correct approach, is probably to do something
// like the following:
// 1) have 2 sets of possible textures for Bet and Autoplay buttons (English and Icon based)
// 2) Provide icon based as a minimum
// 3) Be able to switch between the 2
// 4) Provide an option, which means we can only load up icon based if required
/**
 * Simple, default implementation of the Desktop Bet Button
 * @implements TooltipEnabledGuiButton
 */
class BetButtonDesktop extends BaseUI
{
    constructor()
    {
        super();

        /**
         * @private
         */
        this._button = new Button('betBtnDesktop', 'betBtnDesktopOver', 'betBtnDesktop', 'betBtnDesktopDisabled');
        this._button.on(C_PointerEvt.DOWN, this.onButDown, this);

        // By default, assume buttons are center-bottom of the screen
        this.setTooltipOrientation(C_TooltipOrientation.VERTICAL, C_TooltipPosition.ABOVE);

        let buttonSoundId = this.app.config.uiSounds.desktop.betButtonSound;
        if (buttonSoundId)
        {
            let buttonVolume = this.app.config.uiSounds.desktop.betButtonVolume;

            this.log.debug(`BetButtonDesktop: setting button sound to ${buttonSoundId}`);

            this._button.setClickSound(buttonSoundId, buttonVolume);
        }
        else
        {
            this.log.debug('BetButtonDesktop: no button sound available, button will not make sound when pressed');

            this._button.setClickSound(null);
        }

        // When B is pressed on a keyboard, this is equivalent to pressing the bet button.
        this.keyboardManager.addKeyAction(DefaultKeyCodes.TOGGLE_BET_MENU, KeyPhase.PRESSED, this.onButDown, this);
        
        this.addChild(this._button);

        this.dispatcher.on(C_GameEvent.ENABLE_GUI_BUTTONS, this.enableButton, this);
        this.dispatcher.on(C_GameEvent.DISABLE_GUI_BUTTONS, this.disableButton, this);

        // Initially, the button should be disabled, until explicitly ordered to be enabled
        this._button.setEnabled(false);
    };


    /**
     * In the case of this method, it effectively configures the tooltip from scratch
     * @public
     * @inheritDoc
     * @param {TooltipOrientation} orientation 
     * @param {TooltipPosition} position 
     */
    setTooltipOrientation(orientation=null, position=null)
    {
        let tooltipText = this.app.locale.getString("T_tooltip_openBetMenu");

        this._button.setToolTipProperties(tooltipText, orientation, position);
    };


    /**
     * @private
     */
    enableButton()
    {
        this._button.setEnabled(true);
    };


    /**
     * @private
     */
    disableButton()
    {
        this._button.setEnabled(false);
    };


    /**
     * Fired when the button is pressed.
     * @private
     */
    onButDown()
    {
        this.app.dispatcher.emit(C_GameEvent.BUTTON_TOGGLE_BET_MENU_PRESSED);
    };
}

export default BetButtonDesktop