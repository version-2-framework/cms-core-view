import BaseUI from "../ui/BaseUI";
import Button from "../ui/Button";
import C_PointerEvt from "../const/C_PointerEvt";
import C_TooltipOrientation from "../const/C_TooltipOrientation";
import C_GameEvent from "../../../cms-core-logic/src/const/C_GameEvent";
import C_TooltipPosition from "../const/C_TooltipPosition";
import * as DefaultKeyCodes from "../input/DefaultKeyCodes";
import * as KeyPhase from "../input/KeyPhase";
import C_ViewEvent from "../const/C_ViewEvent";

/**
 * Simple, default implementation of the Desktop Spin Button. Supports the AutoSpin timer animation,
 * for Two Spin games.
 */
class SpinButtonDesktop extends BaseUI
{
    constructor()
    {
        super();

        /**
         * Indicates if the action of starting a Spin is currently allowed.
         * @private
         * @type {boolean}
         */
        this._spinActionIsEnabled = false;

        /**
         * Indicates if the button should operate in "Skip Spin Win" mode.
         * @private
         * @type {boolean}
         */
        this._skipSpinWinActionIsEnabled = false;

        /**
         * @private
         * @type {Button}
         */
        this._button = new Button('spinBtnDesktop', 'spinBtnDesktopOver', 'spinBtnDesktop', 'spinBtnDesktopDisabled');
        this._button.on(C_PointerEvt.DOWN, this.onButDown, this);

        // By default, assume buttons are center-bottom of the screen
        this.setTooltipOrientation(C_TooltipOrientation.VERTICAL, C_TooltipPosition.ABOVE);
        
        this.addChild(this._button);

        let buttonSoundId = this.app.config.uiSounds.desktop.spinButtonSound;
        if (buttonSoundId)
        {
            let buttonVolume = this.app.config.uiSounds.desktop.spinButtonVolume;

            this.log.debug(`SpinButtonDesktop: setting button sound to ${buttonSoundId}`);

            this._button.setClickSound(buttonSoundId, buttonVolume)   
        }
        else
        {
            this.log.debug('SpinButtonDesktop: no button sound available, button will not make sound when pressed');

            this._button.setClickSound(null);
        }

        this.app.keyboardManager.addKeyAction(DefaultKeyCodes.EXECUTE_SPIN_ACTION, KeyPhase.PRESSED, this.onButDown, this);

        // By default, we just listen to whether the Spin button should be enabled or disabled
        this.dispatcher.on(C_GameEvent.DISABLE_SPIN_BUTTON, this.onSpinActionDisabled, this);
        this.dispatcher.on(C_GameEvent.ENABLE_SPIN_BUTTON, this.onSpinActionEnabled, this);

        this.dispatcher.on(C_GameEvent.ENABLE_SKIP_SPIN_WIN_PRESENTATION, this.onSkipSpinWinEnabled, this);
        this.dispatcher.on(C_GameEvent.DISABLE_SKIP_SPIN_WIN_PRESENTATION, this.onSkipSpinWinDisabled, this);

        // Initially, the button should be disabled, until explicitly ordered to be enabled
        this._button.setEnabled(false);

        // For TwoSpin games, we also need to support the "auto-spin" timer, which is shown for Spin 2 idle state
        let isTwoSpinGame = this.app.config.isTwoSpin;
        if (isTwoSpinGame)
        {
            let spinTimerTextureAvailable = this.assets.getTexture('spinBtnDesktopTimer') !== null;
            let spinTimerTextureId = spinTimerTextureAvailable? 'spinBtnDesktopTimer' : 'spinBtnDesktopOver';

            if (!spinTimerTextureAvailable) {
                this.log.debug('spinBtnDesktopTimer texture not available, use default tinted spin button texture');
            }

            /**
             * "AutoSpin timer" layer, shown above the button. This is a highlight effect, which we perform an
             * animated tween on. The player sees the normal button, then a fill (moving from left to right),
             * where the button fills up with a brighter colour. If the texture width id "spinBtnDesktopTimer"
             * is available, this will be used: this is expected behaviour. As an emergency fallback, if it is
             * not available, a white tinted version of the standard "spinBtnDesktopOver" texture will be used.
             * However, without more sophisticated configuration, this is unlikely to look good.
             * @private
             * @type {PIXI.Sprite}
             */
            this._autoSpinLayer = this.assets.getSprite(spinTimerTextureId);
            this._autoSpinLayer.x = this._button.x;
            this._autoSpinLayer.y = this._button.y;
            this._autoSpinLayer.width = this._button.width;
            this._autoSpinLayer.height = this._button.height;
            this._autoSpinLayer.visible = false;

            // Only tint, if we are using the emergency fallback texture
            if (!spinTimerTextureAvailable) {
                this._autoSpinLayer.tint = 0xFFFFFF;
            }

            /**
             * Mask used on the auto-spin layer. To show the timer, we show an animation where we simply tween
             * the width of this mask
             * @private
             * @type {PIXI.Graphics}
             */
            this._autoSpinMask = new PIXI.Graphics();
            this._autoSpinMask.beginFill(0x000000);
            this._autoSpinMask.drawRect(this._button.x, this._button.y, this._button.width, this._button.height);
            this._autoSpinMask.endFill();

            this._autoSpinLayer.mask = this._autoSpinMask;
            
            this.addChild(this._autoSpinLayer);
            this.addChild(this._autoSpinMask);
            
            /**
             * Full width of the AutoSpin mask - this is the width that the mask should be set to, when the timer
             * reaches its end ()
             * @private
             * @type {number}
             */
            this._autoSpinMaskTargetWidth = this._button.width;

            // Listen out for the main instructions to show / hide the autospin timer effect
            this.dispatcher.on(C_GameEvent.SHOW_AUTOSPIN_TIMER, this.showAutoSpinTimer, this);
            this.dispatcher.on(C_GameEvent.HIDE_AUTOSPIN_TIMER, this.hideAutoSpinTimer, this);
        }
    };


    /**
     * In the case of this method, it effectively configures the tooltip from scratch
     * @public
     * @inheritDoc
     * @param {TooltipOrientation} orientation 
     * @param {TooltipPosition} position 
     */
    setTooltipOrientation(orientation=null, position=null)
    {
        let tooltipText = this.app.locale.getString("T_tooltip_startSpin1");
 
        this._button.setToolTipProperties(tooltipText, orientation, position);
    };


    /**
     * @private
     */
    updateEnabledState()
    {
        if (this._skipSpinWinActionIsEnabled || this._spinActionIsEnabled) {
            this._button.setEnabled(true);
        }
        else this._button.setEnabled(false);
    };


    /**
     * @private
     */
    onSpinActionEnabled()
    {
        this._spinActionIsEnabled = true;
        this.updateEnabledState();
    };


    /**
     * @private
     */
    onSpinActionDisabled()
    {
        this._spinActionIsEnabled = false;
        this.updateEnabledState();
    };


    /**
     * @private
     */
    onSkipSpinWinEnabled()
    {
        this._skipSpinWinActionIsEnabled = true;
        this.updateEnabledState();
    };


    /**
     * @private
     */
    onSkipSpinWinDisabled()
    {
        this._skipSpinWinActionIsEnabled = false;
        this.updateEnabledState();
    };

    
    /**
     * Fired when the button is pressed.
     * @private
     */
    onButDown()
    {
        if (this._skipSpinWinActionIsEnabled)
        {
            this.dispatcher.emit(C_GameEvent.SKIP_TO_END_OF_SLOT_WIN_PRESENTATION);
        }
        else
        {
            this.dispatcher.emit(C_GameEvent.BUTTON_SPIN_PRESSED);
        }
    };


    /**
     * Shows the auto-spin timer effect, for a given number of seconds.
     * @private
     * @param {number} numSecondsToShowTimerFor
     * The number of seconds that the timer should be shown for.
     */
    showAutoSpinTimer(numSecondsToShowTimerFor)
    {
        this.log.debug(`SpinButtonDesktop.showAutoSpinTimer for ${numSecondsToShowTimerFor} seconds`);

        this._autoSpinLayer.visible = true;

        this.killExistingAutoSpinAnimation();

        /**
         * Tween instance, for the tween which animates the mask open.
         * @private
         * @type {TweenMax}
         */
        this.autoSpinTimerTween = TweenMax.fromTo(
            this._autoSpinMask,
            numSecondsToShowTimerFor,
            { width:0 },
            { width:this._autoSpinMaskTargetWidth, ease:"Linear.easeInOut" });
    };


    /**
     * Hides the auto-spin timer effect.
     * @private
     */
    hideAutoSpinTimer()
    {
        this.log.debug(`SpinButtonDesktop.hideAutoSpinTimer()`);

        this.killExistingAutoSpinAnimation();
        this._autoSpinLayer.visible = false;
    };


    /**
     * Kills any existing autospin animation. Safe to call at any time, as it checks if the animation is on-goign
     * @private
     */
    killExistingAutoSpinAnimation()
    {
        if (this.autoSpinTimerTween) {
            this.autoSpinTimerTween.kill();
            this.autoSpinTimerTween = null;
        }
    };
}

export default SpinButtonDesktop