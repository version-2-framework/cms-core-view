
import BaseView from "./BaseView";
import C_PointerEvt from "../const/C_PointerEvt";
import { CashierViewEvents, CashierViewModel } from "../../../cms-core-logic/src/controllers/CashierViewModel";
import Button from "../ui/Button";
import C_GameEvent from "../../../cms-core-logic/src/const/C_GameEvent";
import C_Snd from "../const/C_Snd";

const CONTENT_GROUP_DIPPED_ALPHA = 0.1;

class Cashier extends BaseView
{
    constructor()
    {
        super();

        /**
         * @private
         * @type {CashierViewModel}
         */
        this._cashierModel = new CashierViewModel(this.app);

        //--------------------------------------------------------------------------------------------------
        // Setup stuff to do with layout.
        //--------------------------------------------------------------------------------------------------
        /**
         * The height of the header.
         * @private
         * @type {number}
         */
        this._headerHeight = Math.round(this.layout.relHeight(10));

        /**
         * This property was added by Igor - no documentation, so not sure what it does yet.
         * @private
         * @type {boolean}
         */
        this._stopUpdate = true;

        /**
         * Height (in pixels) of the footer. This will be calculated differently for different
         * layouts (eg: mobile, desktop)
         * @private
         */
        this._footerHeight = 0;

        /**
         * Width (in pixels) of a button in the footer.
         * @private
         * @type {number}
         */
        this._footerBtnWidth = 0;

        if (this._platform.isMobileOrTablet())
        {
            this._footerHeight = Math.round(this.layout.relHeight(16));
            this._footerBtnWidth = Math.round(this.layout.relWidth(40));
        }
        else
        {
            this._footerHeight = Math.round(this.layout.relHeight(12));
            this._footerBtnWidth = Math.round(this.layout.relWidth(30));
        }

        /**
         * @private
         * @type {number}
         */
        this._footerBtnHeight = Math.round(this._footerHeight * 0.9);

        /**
         * Max width that text on a footer button may be.
         * @private
         * @type {number}
         */
        this._footerBtnTxtWidth = this._footerBtnWidth * 0.9;

        /**
         * Max height that text on a footer button may be.
         * @private
         * @type {number}
         */
        this._footerBtnTxtHeight = this._footerBtnHeight * 0.9;

        /**
         * Total allowed height of all content (the area between header and footer)
         * @private
         * @type {number}
         */
        this._contentHeight = this.layout.relHeight(100) - this._headerHeight - this._footerHeight;

        /**
         * Minimum horizontal distance that must be applied as padding between items in the header.
         * @private
         * @type {number}
         */
        this._headerItemPadX = this.layout.relWidth(1);

        /**
         * Maximimum width (in pixels) that the Game logo may occupy, within the header.
         * @private
         * @type {number}
         */
        this._maxHeaderLogoWidth = this.layout.relWidth(15);
        
        /**
         * Maximum width (in pixels) that the "Cashier" text may occupy, within the header.
         * @private
         * @type {number}
         */
        this._maxHeaderTitleWidth = this.layout.relWidth(100) - (2 * this._maxHeaderLogoWidth) - (4 * this._headerItemPadX);
        
        /**
         * Maximum height (in pixels) that any item within the header may occupy.
         * @private
         * @type {number}
         */
        this._maxHeaderItemHeight = this._headerHeight * 0.85;

        /**
         * Height (in pixels) of text within the header.
         * @private
         * @type {number}
         */
        this._headerTxtSize = this._maxHeaderItemHeight;

        this.drawCashier();

        this.dispatcher.on(C_GameEvent.HIDE_CASHIER_VIEW, this.closeCashier, this);
    }


    /**
     * Draws the Cashier View.
     * @private
     */
    drawCashier()
    {
        let isAddCreditCashier = this._cashierModel.isAddCreditCashier();

        this.log.debug(`drawCashier(isAddCredit:${isAddCreditCashier})`);

        /**
         * Background graphic for the header.
         * @private
         * @type {PIXI.mesh.NineSlicePlane}
         */
        this._headerBg = this.assets.getS9Sprite('wmg_contentBox_blue');
        this._headerBg.width = this.layout.relWidth(100);
        this._headerBg.height = this._headerHeight;

        /**
         * Background graphic for the footer.
         * @private
         * @type {PIXI.Sprite}
         */
        this._footerBg = this.assets.getSprite('cashDeskGreyBottom');
        this._footerBg.width = this._headerBg.width;
        this._footerBg.height = this._footerHeight;
        this.layout.verticalAlignBottom(this._footerBg);

        let topRightItem;

        if (this._cashierModel.isExitClientButtonAvailable())
        {
            /**
             * Button (in top right) for exiting the game client.
             * @private
             * @type {Button}
             */
            this._exitClientBtn = new Button('closeBtn', 'closeBtn_over', 'closeBtn_over', 'closeBtn_disabled');

            //this.layout.propHeightScale(this._exitClientBtn, 100, this._headerBg);
            
            this.layout.setHeight(this._exitClientBtn, this._maxHeaderItemHeight);
            
            this._exitClientBtn.x = this.layout.relWidth(100) - this._headerItemPadX - this._exitClientBtn.width;
            this._exitClientBtn.y = this._headerBg.y + this._headerBg.height * .5 - this._exitClientBtn.height * .5;
            this._exitClientBtn.on(C_PointerEvt.DOWN, () => this._cashierModel.actionOnExitClientButtonPressed());
            
            topRightItem = this._exitClientBtn;
        }
        else
        if (this._cashierModel.isCloseButtonAvailable())
        {
            /**
             * Close button for the cashier - shown in top right corner of screen. This is optional.
             * @private
             * @type {Button}
             */
            this._closeBtn = new Button('wmg_iconX');
            this.layout.propHeightScale(this._closeBtn, 100, this._headerBg);
            
            this._closeBtn.x = this.layout.relWidth(100) - this._headerItemPadX - this._closeBtn.width;
            this._closeBtn.y = this._headerBg.y + this._headerBg.height * .5 - this._closeBtn.height * .5;
            this._closeBtn.on(C_PointerEvt.DOWN, this._cashierModel.actionOnCloseCashierPressed, this._cashierModel);
            
            topRightItem = this._closeBtn;
        }
        else
        {
            let wmgLogo = this.assets.getSprite('logo_wmg_white');
            const maxRect = new PIXI.Rectangle(0, 0, 100, this._headerHeight);
            this.layout.propHeightScale(wmgLogo, 90, maxRect);

            wmgLogo.anchor.set(.5, .5);
            wmgLogo.x = this.layout.relWidth(100) - this._headerItemPadX - (wmgLogo.width * 0.5);
            wmgLogo.y = this._headerBg.y + (0.5 * this._headerHeight);

            topRightItem = wmgLogo;
        }

        let bg = this.assets.getS9Sprite('wmg_contentBox_white');
        bg.width = this.layout.relWidth(100);
        bg.height = this.layout.relHeight(100);
        bg.interactive = true;

        let headerTxt = new PIXI.Text(
            this.locale.getString('T_cashier_title'),
            {
                fontFamily : "Arial",
                fontSize : this._headerTxtSize,
                fill : 0xFFFFFF
            });
        headerTxt.anchor.set(.5,.5);
        this.layout.horizontalAlignCenter(headerTxt, 0);
        headerTxt.y = this._headerBg.y + (.5 * this._headerBg.height);

        /**
         * Group containing the main content - anything between the header and footer (not including backgrounds)
         * @private
         * @type {PIXI.Container}
         */
        this._contentGroup = new PIXI.Container();

        this.addChild(bg);
        this.addChild(this._footerBg);
        this.addChild(this._headerBg);
        this.addChild(topRightItem);
        this.addChild(headerTxt);
        this.addChild(this._contentGroup);

        //----------------------------------------------------------
        // Let's calculate some layout variables.
        //----------------------------------------------------------
        // The general principle used for laying out this section,
        // is as follows:
        // 1) We calculate a bunch of dimensions, based on dividing
        // up the content area into 3 sectors, each with their own
        // proportions. We determine the y positions, and height,
        // allotted to each sector.
        // 2) We create the content for each sector. Text items
        // are anchored centrally (both vertically, and horizontally).
        // They are then positioned at the absoloute center of the
        // area that is allotted to them, and assigned maxWidth /
        // maxHeight properties (which denote the limites of their
        // available areas). When the text changes (for a number
        // value change, or a locale change), we can then reset their
        // scale to intended text size, and ensure that they fit
        // within the maxWidth / maxHeight areas allowed to them.
        //----------------------------------------------------------
        let dynamicFontSize;
        const numSectors = 3;
        let contentPadY = this.layout.relHeight(1);
        let contentStartY = this._headerBg.height;
        let contentAreaHeight = this.layout.relHeight(100) - this._headerHeight - this._footerBg.height;
        let maxContentHeight = contentAreaHeight - ((numSectors + 1) * contentPadY);
        let sector1Height = maxContentHeight * 0.3;
        let sector2Height = maxContentHeight * 0.3;
        let sector3Height = maxContentHeight * 0.4;
        let sector1posY = contentStartY + contentPadY;
        let sector2posY = sector1posY + sector1Height + contentPadY;
        let sector3posY = sector2posY + sector2Height + contentPadY;
        let textMaxWidth = this.layout.relWidth(80);

        // Calculate font sizes for content area.
        if (this._platform.isDesktop())
        {
            dynamicFontSize = Math.round(maxContentHeight / 14);
        }
        else
        {
            // For mobile, there are 3 sectors. At MAX, you can assume each sector
            // has a piece of small text (set to dynamicFontSize) and a large bit
            // of text (2 * dynamicFontSize), and we add another sector of height
            // as padding - so to achieve dynamicFontSize, we divide by 12. It's
            // a rough measure, but it has worked quite well.
            dynamicFontSize = Math.round(maxContentHeight / 12);
        }

        //----------------------------------------------------------
        // Set up fonts.
        //----------------------------------------------------------
        /**
         * Text style to be applied to any "blue" text field.
         * @private
         * @type {PIXI.TextStyleOptions}
         */
        this._txtStyleBlue =
        {
            fontFamily: "Arial",
            fontSize : Math.round(dynamicFontSize * 1.5),
            fontWeight: 'bold',
            fill: "#0393fd",
            align: "center"
        };

        /**
         * Text style to be applied to any "grey" text field.
         * @private
         * @type {PIXI.TextStyleOptions}
         */
        this._txtStyleGrey = {
            fontFamily: "Arial",
            fontSize : dynamicFontSize,
            fill: "#3c3c3c",
            align: "center"
        };

        /**
         * Text style to be applied to any "grey italic" text field.
         * @private
         * @type {PIXI.TextStyleOptions}
         */
        this._txtStyleGreyItalic =
        {
            fontSize : dynamicFontSize,
            fontFamily: "Arial",
            fontStyle: 'italic',
            fill: "#3c3c3c"
        };

        /**
         * Text style for the main "amount to transfer" number.
         * @private
         * @type {PIXI.TextStyleOptions}
         */
        this._txtStyleAmountNumber =
        {
            fontFamily: 'Arial',
            fontSize: Math.round(dynamicFontSize * 2),
            fill:'#000'
        };

        /**
         * Text style to be applied to footer buttons.
         * @private
         * @type {PIXI.TextStyleOptions}
         */
        this._txtStyleBtns =
        {
            fontFamily: "Arial",
            fontSize : dynamicFontSize,
            fill: "#000000",
            fontWeight: "bold"
        };

        //----------------------------------------------------------
        // Create the text material
        // todo: add alternate desktop layout for sectors 1 / 2.
        // I kept getting confused with the change of flow that the
        // smallCashDesk / largeCashDesk functions added, so i have
        // removed it. I may add it back later..
        //----------------------------------------------------------

        //----------------------------------------------------------
        // Sector 1
        // account balance title and text
        //----------------------------------------------------------

        let maxTransferTitleString = this.locale.getString('T_cashier_maxTransferAmount');

        /**
         * The title field for the Max Transfer section. Max Transfer Amount indicates the
         * maximum amount the player may transfer right now (its NOT account balance, but
         * represents the amount they may actually bring over).
         * @private
         * @type {PIXI.Text}
         */
        this._maxTransferTitleTxt = new PIXI.Text(maxTransferTitleString, this._txtStyleGrey);

        this._maxTransferTitleTxt.anchor.set(0.5, 1);
        this._maxTransferTitleTxt.maxWidth = textMaxWidth;
        this._maxTransferTitleTxt.maxHeight = sector1Height * 0.4;
        this.layout.horizontalAlignCenter(this._maxTransferTitleTxt, 0);
        this._maxTransferTitleTxt.y = sector1posY + this._maxTransferTitleTxt.maxHeight;

        let maxTransferValueString = this._model.formatCurrency(this._cashierModel.getMaxTransferAmount());

        /**
         * The value field for the Max Transfer section. Max Transfer Amount indicates the
         * maximum amount the player may transfer right now (its NOT account balance, but
         * represents the amount they may actually bring over).
         * @private
         * @type {PIXI.Text}
         */
        this._maxTransferValueTxt = new PIXI.Text(maxTransferValueString, this._txtStyleBlue);
        this._maxTransferValueTxt.anchor.set(0.5, 0);
        this._maxTransferValueTxt.maxWidth = textMaxWidth;
        this._maxTransferValueTxt.maxHeight = sector1Height - this._maxTransferTitleTxt.maxHeight;
        this.layout.horizontalAlignCenter(this._maxTransferValueTxt, 0);
        this._maxTransferValueTxt.y = sector1posY + this._maxTransferTitleTxt.maxHeight;

        //----------------------------------------------------------
        // Sector 2
        // transfer amount title and text.
        //----------------------------------------------------------
        let transferAmountString = this.locale.getString('T_cashier_amountToTransfer');

        /**
         * @private
         * @type {PIXI.Text}
         */
        this._transferAmountTxt = new PIXI.Text(transferAmountString, this._txtStyleGrey);
        this._transferAmountTxt.anchor.set(0.5, 1);
        this._transferAmountTxt.maxWidth = textMaxWidth;
        this._transferAmountTxt.maxHeight = sector2Height * 0.4;
        this.layout.horizontalAlignCenter(this._transferAmountTxt, 0);
        this._transferAmountTxt.y = sector2posY + this._transferAmountTxt.maxHeight;

        let transferNumberString = this._model.formatCurrency(this._cashierModel.getMaxTransferAmount());

        /**
         * @private
         * @type {PIXI.Text}
         */
        this._transferNumberTxt = new PIXI.Text(transferNumberString, this._txtStyleAmountNumber);
        this._transferNumberTxt.name = "TransferNumber_Text";
        this._transferNumberTxt.anchor.set(0.5, 0);
        this._transferNumberTxt.maxWidth = textMaxWidth;
        this._transferNumberTxt.maxHeight = sector2Height - this._transferAmountTxt.maxHeight;
        this.layout.horizontalAlignCenter(this._transferNumberTxt, 0);
        this._transferNumberTxt.y = sector2posY + this._transferAmountTxt.maxHeight;


        //----------------------------------------------------------
        // Sector 3
        // The slider!
        //----------------------------------------------------------
        // Min button for slider.
        let sliderBtnHeight = sector3Height * 0.5;
        let sliderPadX = this.layout.relWidth(5);


        /**
         * Background area for the credit slider. This slider sits between the
         * min and max transfer buttons.
         * @private
         * @type {PIXI.Sprite}
         */
        this._sliderBg = this.assets.getSprite('sliderBg');
        this._sliderBg.anchor.set(0.5);
        this.layout.horizontalAlignCenter(this._sliderBg);
        this._sliderBg.y = sector3posY + (0.5 * sliderBtnHeight);
        this._sliderBg.width = this.layout.relWidth(65);
        this._sliderBg.height = sliderBtnHeight * 0.4;

        /**
         * "Cached position of slider.
         * @private
         */
        this._sliderPositions = {left : this._sliderBg.x - this._sliderBg.width * .5, right : this._sliderBg.x + this._sliderBg.width * .5};

        /**
         * This button sits above the slider, giving the player a touch target to aim for.
         * @private
         * @type {PIXI.Sprite}
         */
        this._sliderBtn = this.assets.getSprite('sliderBtn');
        this._sliderBtn.x = this._sliderPositions.right;
        this._sliderBtn.y = this._sliderBg.y;
        this._sliderBtn.scale.set(this._sliderBg.height * 1.6 / this._sliderBtn.height);
        this._sliderBtn.anchor.set(0.5);
        this._sliderBtn
            .on(C_PointerEvt.DOWN, this.startDrag, this)
            .on(C_PointerEvt.UP, this.stopDrag, this)
            .on(C_PointerEvt.UPOUTSIDE, this.stopDrag, this);

        /**
         * Button for setting transfer amount to min.
         * @private
         * @type {PIXI.Sprite}
         */
        this._minBtn = this.assets.getSprite('sliderMinBtn');
        this._minBtn.on(C_PointerEvt.DOWN, ()=>
        {
            TweenMax.to(this._sliderBtn, 0.5, {x: this._sliderPositions.left});
            this.sound.playSound(C_Snd.BTN_DOWN);
            this._cashierModel.setTransferAmountToMin();
            this.syncValueFields();
        });

        this.layout.propHeightScale(this._minBtn, 10);
        this._minBtn.anchor.set(0, 1);
        this._minBtn.x = sliderPadX;
        this._minBtn.y = sector3posY + sliderBtnHeight * 0.85;

        /**
         * Button for setting transfer amount to max.
         * @private
         * @type {PIXI.Sprite}
         */
        this._maxBtn = this.assets.getSprite('sliderMaxBtn');
        this._maxBtn.on(C_PointerEvt.DOWN, ()=>
        {
            TweenMax.to(this._sliderBtn, 0.5, {x: this._sliderPositions.right});
            this.sound.playSound(C_Snd.BTN_DOWN);
            this._cashierModel.setTransferAmountToMax();
            this.syncValueFields();
        });
        this._maxBtn.width  = this._minBtn.width;
        this._maxBtn.height = this._minBtn.height;
        this._maxBtn.anchor.set(0, 1);
        this._maxBtn.x = this.layout.relWidth(100) - this._maxBtn.width - sliderPadX;
        this._maxBtn.y = this._minBtn.y;

        /**
         * The maxSessionTransfer field appears under the slider, and indicates if a limit
         * exists for the total amount of credit which may be transferred to any single
         * session. We are not guaranteed to know if such a limit exists, until we get balance
         * data, so we create the field always: on a resync to balanceData, we can decide
         * whether to show it and update its text
         * @private
         * @type {PIXI.Text}
         */
        this._maxSessionTransferTxt = new PIXI.Text("", this._txtStyleGreyItalic);
        this._maxSessionTransferTxt.anchor.set(0.5, 0);
        this.layout.horizontalAlignCenter(this._maxSessionTransferTxt);
        this._maxSessionTransferTxt.y = sector3posY + sliderBtnHeight;

        /**
         * Confirm button: the user presses this to initate the credit transfer.
         * @private
         * @type {PIXI.mesh.NineSlicePlane}
         */
        this._confirmBtn = this.assets.getS9Sprite('wmg_btn');
        this._confirmBtn.width  = this._footerBtnWidth;
        this._confirmBtn.height = this._footerBtnHeight;
        this._confirmBtn.y = this._footerBg.y + (this._footerBg.height * 0.5) - this._footerBtnHeight * 0.5;
        this._confirmBtn.on(C_PointerEvt.DOWN, () => {
            this.sound.playSound(C_Snd.BTN_DOWN);
            this._cashierModel.transferCredit();
        });

        this.layout.horizontalAlignCenter(this._confirmBtn);
        
        /**
         * Text shown on the Confirm button.
         * @private
         * @type {PIXI.Text}
         */
        this._confirmBtnTxt = new PIXI.Text(this.locale.getString('T_button_confirm'), this._txtStyleBtns);
        this.layout.horizontalAlignCenter(this._confirmBtnTxt, 0, this._confirmBtn);
        this.layout.verticalAlignMiddle(this._confirmBtnTxt, 0, this._confirmBtn);

        // this.confirmBtnTxt.localizationId = 'buttonConfirm';
        this._confirmBtnTxt.maxWidth  = this._footerBtnTxtWidth;
        this._confirmBtnTxt.maxHeight = this._footerBtnTxtHeight;

        //----------------------------------------------------------
        // Arrange the content.
        //----------------------------------------------------------
        this._contentGroup.addChild(this._maxTransferTitleTxt);
        this._contentGroup.addChild(this._maxTransferValueTxt);
        this._contentGroup.addChild(this._transferAmountTxt);
        this._contentGroup.addChild(this._transferNumberTxt);
        this._contentGroup.addChild(this._sliderBg);
        this._contentGroup.addChild(this._sliderBtn);
        this._contentGroup.addChild(this._minBtn);
        this._contentGroup.addChild(this._maxBtn);
        this._contentGroup.addChild(this._maxSessionTransferTxt);
        this._contentGroup.addChild(this._confirmBtn);
        this._contentGroup.addChild(this._confirmBtnTxt);

        this._contentGroup.alpha = CONTENT_GROUP_DIPPED_ALPHA;
        this._sliderBg.on(C_PointerEvt.DOWN, this.onSliderTouch, this);

        //----------------------------------------------------------
        // Do some final state synchronization.
        //----------------------------------------------------------
        this.syncValueFields();
        this.disableInput();

        // Finally, bind to the key state event listeners.
        this._dispatcher.on(CashierViewEvents.ENTER_STATE_FETCHING_BALANCE, this.showWaitingForBalance, this);
        this._dispatcher.on(CashierViewEvents.EXIT_STATE_FETCHING_BALANCE, this.clearWaitingForBalance, this);
        this._dispatcher.on(CashierViewEvents.ENTER_STATE_SELECT_TRANSFER_AMOUNT, this.enterSelectAmountState, this);
        this._dispatcher.on(CashierViewEvents.EXIT_STATE_SELECT_TRANSFER_AMOUNT, this.exitSelectAmountState, this);
        this._dispatcher.on(CashierViewEvents.ENTER_STATE_TRANSFERRING_CREDIT, this.showTransferringCredit, this);
        this._dispatcher.on(CashierViewEvents.EXIT_STATE_TRANSFERRING_CREDIT, this.clearTransferringCredit, this);

        this._cashierModel.actionOnViewShown();
    };


    /**
     * Closes, and destroys, the Cashier View.
     * @private
     */
    closeCashier()
    {
        this.removeFromParent()

        this._dispatcher.off(CashierViewEvents.ENTER_STATE_FETCHING_BALANCE, this.showWaitingForBalance, this);
        this._dispatcher.off(CashierViewEvents.EXIT_STATE_FETCHING_BALANCE, this.clearWaitingForBalance, this);
        this._dispatcher.off(CashierViewEvents.ENTER_STATE_SELECT_TRANSFER_AMOUNT, this.enterSelectAmountState, this);
        this._dispatcher.off(CashierViewEvents.EXIT_STATE_SELECT_TRANSFER_AMOUNT, this.exitSelectAmountState, this);
        this._dispatcher.off(CashierViewEvents.ENTER_STATE_TRANSFERRING_CREDIT, this.showTransferringCredit, this);
        this._dispatcher.off(CashierViewEvents.EXIT_STATE_TRANSFERRING_CREDIT, this.clearTransferringCredit, this);
    };


    /**
     * Initializes the slider, ready for input.
     * @private
     */
    initializeSlider()
    {
        let numberOfSliderValues = (this._cashierModel.getMaxTransferAmount() - this._cashierModel.getMinTransferAmount()) / this._cashierModel.getMinTransferIncrement();
        this.sizeOfSliderSnap = this._sliderBg.width / numberOfSliderValues;
    };


    /**
     * Enables all input actions (buttons and sliders): these are only available for a limited time in the Cashier
     * @private
     */
    enableInput()
    {
        this.log.debug("enableInput()");

        this._sliderBg.interactive = true;
        this._sliderBtn.interactive = true;
        this._minBtn.interactive = true;
        this._maxBtn.interactive = true;
        this._confirmBtn.interactive = true;

        if (this._closeBtn) {
            this._closeBtn.interactive = true;
            this._closeBtn.alpha = 1;
        }
    };


    /**
     * Disables all input actions (buttons and sliders): these are only available for a limited time in the Cashier
     * @private
     */
    disableInput()
    {
        this.log.debug("disableInput()");

        this._sliderBg.interactive = false;
        this._sliderBtn.interactive = false;
        this._minBtn.interactive = false;
        this._maxBtn.interactive = false;
        this._confirmBtn.interactive = false;
        if (this._closeBtn) {
            this._closeBtn.interactive = false;
            this._closeBtn.alpha = 0.2;
        }
    };


    /**
     * Shows the "Waiting For Balance" visual overlay for the cashier.
     * @private
     */
    showWaitingForBalance()
    {
        this.log.debug('showWaitingForBalance()');

        /**
         * Text message shown to the player, while we are fetching balance from the server.
         * @private
         * @type {PIXI.Text}
         */
        this._fetchBalanceTxt = new PIXI.Text(
            this.locale.getString('T_cashier_retrievingBalance'),
            {
                fontFamily : "Arial",
                fontSize : this._headerTxtSize,
                fill : '#000'
            });

        this._fetchBalanceTxt.anchor.set(.5, .5);
        this.layout.horizontalAlignCenter(this._fetchBalanceTxt, 0);
        this.layout.verticalAlignMiddle(this._fetchBalanceTxt, 0);

        this.addChild(this._fetchBalanceTxt);

        TweenMax.fromTo(this._fetchBalanceTxt, 1, {alpha:0.5}, {alpha:1,yoyo:true,repeat:-1});
    };


    /**
     * Clears the Waiting for balance visual state.
     * @private
     */
    clearWaitingForBalance()
    {
        this.log.debug('clearWaitingForBalance()');

        if (this._fetchBalanceTxt)
        {
            TweenMax.killTweensOf(this._fetchBalanceTxt);
            TweenMax.to(this._fetchBalanceTxt, 0.4, { delay:0.4, alpha:0, onComplete:() => {
                this.removeChild(this._fetchBalanceTxt);
                this._fetchBalanceTxt = null;
            }});
        }
    };


    /**
     * Shows the "Transferring credit to session" visual overlay for the Cashier.
     * @private
     */
    showTransferringCredit()
    {
        this.log.debug('showTransferringCredit()');

        TweenMax.to(this._contentGroup, 0.2, { alpha:CONTENT_GROUP_DIPPED_ALPHA });
        
        /**
         * Text message shown to the player, while a credit transfer operation is taking place.
         * @private
         * @type {PIXI.Text}
         */
        this._transferringTxt = new PIXI.Text(
            this.locale.getString('T_cashier_transferringToAccount'),
            {
                fontFamily : "Arial",
                fontSize : this._headerTxtSize,
                fill : '#000'
            });

        this._transferringTxt.anchor.set(.5, .5);

        this.layout.horizontalAlignCenter(this._transferringTxt, 0);
        this.layout.verticalAlignMiddle(this._transferringTxt, 0);

        this.addChild(this._transferringTxt);

        TweenMax.fromTo(this._transferringTxt, 1, {alpha:0.5}, {alpha:1,yoyo:true,repeat:-1});
    };


    /**
     * Clears the "Transferring Credit to Session" visual overlay.
     * @private
     */
    clearTransferringCredit()
    {
        this.log.debug('clearTransferringCredit()');

        if (this._transferringTxt)
        {
            TweenMax.killTweensOf(this._transferringTxt);
            TweenMax.to(this._transferringTxt, 0.8, { delay:1, alpha:0, onComplete:() => {
                this.removeChild(this._transferringTxt);
                this._transferringTxt = null;
            }});
        }
    };
    

    /**
     * Shows the Cashier in the "Select value" visual state.
     * @private
     */
    enterSelectAmountState()
    {
        this.log.debug("enterSelectAmountState()");

        this.initializeSlider();
        this.syncValueFields();
        this.enableInput();

        TweenMax.to(
            this._contentGroup,
            0.8,
            {
                delay: 0.5,
                alpha: 1
            });
    };


    /**
     * @private
     */
    exitSelectAmountState()
    {
        this.log.debug("exitSelectAmountState()");

        this.disableInput();
    };


    // TODO: Document method event params properly
    /**
     * Fired when the slider is touched.
     * @private
     * @param {*} evtData 
     */
    onSliderTouch(evtData)
    {
        let x = Math.floor(evtData.data.getLocalPosition(this).x);
        this._sliderBtn.x = x;
        this.setAmountFromSliderPos(x);
        this.sound.playSound(C_Snd.BTN_DOWN);
    };


    // TODO: Document this method properly
    /**
     * @private
     * @param {*} event 
     */
    startDrag(event)
    {
        event.stopPropagation();
        this.startDragPt = this._contentGroup.toLocal(event.data.global);
        event.currentTarget.on(C_PointerEvt.MOVE, this.updatePos, this);

        this.startPts = [];
    }


    // TODO: Document this method properly
    /**
     * @private
     * @param {*} event 
     */
    stopDrag(event)
    {
        event.stopPropagation();
        event.currentTarget.removeListener(C_PointerEvt.MOVE, this.updatePos, this);

        // this.snapClosestBox();
    };


    // TODO: Document this method properly
    /**
     * @private
     * @param {*} event 
     */
    updatePos(event)
    {
        event.stopPropagation();
        let currDragPt = event.data.global;

        let deltaX = this._contentGroup.toLocal(currDragPt).x - this.startDragPt.x;

        if(!this._stopUpdate)
        {
            this._sliderBtn.x = this.startDragPt.x + deltaX;
        }

        this.checkBounds(this._contentGroup.toLocal(currDragPt).x);

        this.setAmountFromSliderPos(this._sliderBtn.x);
    };


    // TODO: Document this method properly
    /**
     * @private
     * @param {*} deltaX 
     */
    checkBounds(deltaX) {

        let maxX = this._sliderBg.x + this._sliderBg.width * 0.5;
        let minX = this._sliderBg.x - this._sliderBg.width * 0.5;

        if (deltaX > maxX)
        {
            this._stopUpdate = true;
            this._sliderBtn.x = maxX;
        }
        else if (deltaX < minX)
        {
            this._stopUpdate = true;
            this._sliderBtn.x = minX;
        }
        else
        {
            this._stopUpdate = false;
        }

    };


    // TODO: Document this method properly
    /**
     * @private
     * @param {*} posX 
     */
    setAmountFromSliderPos(posX)
    {
        // 1) Calculate distance that slider button center is from slider left pos.
        let sliderPosition = posX - this._sliderPositions.left;

        // Determine how many increments have been stepped.
        let numIncrements = Math.round(sliderPosition / this.sizeOfSliderSnap);
        let newAmount = this._cashierModel.getMinTransferAmount() + (numIncrements * this._cashierModel.getMinTransferIncrement());

        // todo: This will snap the slider btn into exact increment positions.
        // This might feel awfull on mobile (especially if player doesn't have much
        // money to transfer, in which case the jumps will be very large). Work out
        // if I want to leave this feature in, or remove it.
        this._sliderBtn.x = this._sliderPositions.left + (numIncrements * this.sizeOfSliderSnap);
        this._cashierModel.setTransferAmount(newAmount);
        this.syncValueFields();
    };


    /**
     * Updates the big "amount to transfer" number field, syncing it to the latest value
     * from the CashDeskController. Refreshes the field's layout to ensure the text does
     * not overflow its allowed area.
     * @private
     */
    syncValueFields()
    {
        this.log.debug("CashierView.syncValueFields()");

        if (this._maxSessionTransferTxt)
        {
            let maxTransferPerSession = this._cashierModel.getMaxTransferPerSession();

            if (maxTransferPerSession !== Number.POSITIVE_INFINITY)
            {
                let formattedCurrency = this._model.formatCurrency(maxTransferPerSession);
                let amountByLawMsg = this.locale.getString('T_cashier_maxSessionTransfer', {"[SESSION_TRANSFER_LIMIT]" : formattedCurrency});

                this._maxSessionTransferTxt.text = amountByLawMsg;
                this._maxSessionTransferTxt.scale.set(1);
                this._maxSessionTransferTxt.visible = true;
            }
            else this._maxSessionTransferTxt.visible = false;
        }

        if (this._transferNumberTxt)
        {
            this._transferNumberTxt.text = this._cashierModel.getTransferAmountString();
            this._transferNumberTxt.scale.set(1);
            if (this._transferNumberTxt.width > this._transferNumberTxt.maxWidth ||
                this._transferNumberTxt.height > this._transferNumberTxt.maxHeight)
            {
                this.layout.limitSize(this._transferNumberTxt, this._transferNumberTxt.maxWidth, this._transferNumberTxt.maxHeight);
            }
        }

        if (this._maxTransferValueTxt)
        {
            this._maxTransferValueTxt.text = this._model.formatCurrency(this._cashierModel.getMaxTransferAmount());
            this._maxTransferValueTxt.scale.set(1);
            if (this._maxTransferValueTxt.width > this._maxTransferValueTxt.maxWidth ||
                this._maxTransferValueTxt.height > this._maxTransferValueTxt.maxHeight)
            {
                this.layout.limitSize(this._maxTransferValueTxt, this._maxTransferValueTxt.maxWidth, this._maxTransferValueTxt.maxHeight);

            }
        }
    };
}

export default Cashier