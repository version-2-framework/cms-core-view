import { getGameViewLogger } from "../../../cms-core-logic/src/logging/LogManager";
import getActiveSlotLayout from "../../../cms-core-logic/src/utils/getActiveSlotLayout";
import Layout from "../utils/Layout";

const log = getGameViewLogger();

export class ReelPositionUtil
{
    constructor()
    {
        let app = PIXI.app;
        let config = app.config;

        /**
         * @type {Layout}
         */
        let layout = app.layout;

        /**
         * @type {PlatformApi}
         */
        let platform = app.platform;

        /**
         * Array of all symbol areas, divided by hand, then reel, and then by position within
         * the reel. All coordinates are in reel-group space (they will need to be translated
         * in order to get them in screen space).
         * @private
         * @type {PIXI.Rectangle[][][]}
         */
        this._symbolBounds = [];
        
        /**
         * Array of the bounds of each hand / reel, in screen space.
         * @private
         * @type {PIXI.Rectangle[][]}
         */
        this._reelBounds = [];
        
        // Let's assign rectangles for all positions, within reel space.
        let numHands = config.numHands;
        let numReels = config.numReels;
        let numSymPerReel = config.numSymsPerReel;

        /** @type {GuiLayoutConfig} */
        let activeGuiLayout = config.uiLayoutDesktop;
        if (platform.isMobileOrTablet()) {
            activeGuiLayout = config.uiLayoutMobile;
        }

        let slotLocalLayout = getActiveSlotLayout(app);

        let slotAreaWithinGui = activeGuiLayout.reelsPos;

        log.debug(`ReelPositionUtil: layoutData = ${JSON.stringify(slotAreaWithinGui)}`);

        let targetReelsHeight = layout.relHeight(slotAreaWithinGui.height);
        let actualReelsHeight = slotLocalLayout.reelsHeight;
        let reelsScale = targetReelsHeight / actualReelsHeight;

        log.debug(`ReelPositionUtil: targetReelsHeight = ${targetReelsHeight}`);
        log.debug(`ReelPositionUtil: actualReelsHeight = ${actualReelsHeight}`);
        log.debug(`ReelPositionUtil: reelsScale = ${reelsScale}`);

        let reelsLocalStartX = slotLocalLayout.reelsPosX;
        let reelsLocalStartY = slotLocalLayout.reelsPosY;
        let reelsMarginLeft = slotLocalLayout.reelsMarginLeft;
        let reelsMarginRight = slotLocalLayout.reelsMarginRight;
        let reelsMarginTop = slotLocalLayout.reelsMarginTop;
        let reelsMarginBottom = slotLocalLayout.reelsMarginBottom;
        let symWidth = slotLocalLayout.symWidth;
        let symHeight = slotLocalLayout.symHeight;
        let symGap = slotLocalLayout.symGap;
        let reelGap = slotLocalLayout.reelGap;

        let maxNumSymOnAnyReel = Math.max(...numSymPerReel);

        /**
         * Bounds of all reels together, divided by hand (guaranteed to have at least the main hand in it)
         * @private
         * @type {PIXI.Rectangle[]}
         */
        this._reelsBounds = 
        [
            new PIXI.Rectangle(
                reelsLocalStartX,
                reelsLocalStartY,
                reelsMarginLeft + reelsMarginRight + (symWidth * numReels) + (reelGap * (numReels - 1)),
                reelsMarginTop + reelsMarginBottom + (symHeight * maxNumSymOnAnyReel) + (symGap * (maxNumSymOnAnyReel - 1))
            )
        ];

        log.info(`ReelPositionUtil: reelsBounds = ${JSON.stringify(this._reelsBounds)}`);

        // Calculate symbol and reel positions for primary hand
        let symbolRectsForMainHand = [];
        let reelBoundsForMainHand = [];

        for (let reelIndex = 0; reelIndex < numReels; reelIndex ++)
        {
            let symbolRectsForReel = [];
            let numSymOnReel = numSymPerReel[reelIndex];

            let reelStartX = reelsLocalStartX + reelsMarginLeft + (symWidth * reelIndex) + (reelIndex >= 1? reelGap * reelIndex : 0);
            let reelStartY = reelsLocalStartY + reelsMarginTop;
            let reelWidth = symWidth;
            let reelHeight = (symHeight * numSymOnReel) + (symGap * (numSymOnReel - 1));

            for (let posIndex = 0; posIndex < numSymOnReel; posIndex ++)
            {
                let symStartX = reelStartX;
                let symStartY = reelStartY + (symHeight * posIndex) + (posIndex >= 1? symGap * posIndex : 0);
                
                log.debug(`ReelPositionUtil:sym(r:${reelIndex},p:${posIndex}) = symStartX:${symStartX},symStartY:${symStartY}`);

                let rectangle = new PIXI.Rectangle(symStartX, symStartY, symWidth, symHeight);
                symbolRectsForReel.push(rectangle);
            }

            symbolRectsForMainHand.push(symbolRectsForReel);

            // Also, generate the bounds of the whole reel.
            reelBoundsForMainHand.push(new PIXI.Rectangle(reelStartX, reelStartY, reelWidth, reelHeight));
        }

        this._symbolBounds.push(symbolRectsForMainHand);
        this._reelBounds.push(reelBoundsForMainHand);

        // Calculate symbol and reel positions for any secondary hands
        if (numHands > 1)
        {
            let symbolWidth = slotLocalLayout.secondaryHandSymWidth;
            let symbolHeight = slotLocalLayout.secondaryHandSymHeight;
            
            for (let handIndex = 1; handIndex < numHands; handIndex ++)
            {
                let handProgressiveIndex = handIndex - 1;
                
                let symbolRectsForHand = [];
                let reelRectsForHand = [];

                let handSymStartPosX = slotLocalLayout.secondaryHandSymPosX[handProgressiveIndex];
                let handSymStartPosY = slotLocalLayout.secondaryHandSymPosY[handProgressiveIndex];
                let handWidth = symbolWidth * numReels;
                let handHeight = symbolHeight * maxNumSymOnAnyReel;

                for (let reelIndex = 0; reelIndex < numReels; reelIndex ++)
                {
                    let symbolRectsForReel = [];
                    let numSymOnReel = numSymPerReel[reelIndex];

                    let reelStartPosX = handSymStartPosX + (reelIndex * symbolWidth);
                    let reelStartPosY = handSymStartPosY;
                    let reelWidth = symbolWidth;
                    let reelHeight = symbolHeight * numSymOnReel;

                    for (let posIndex = 0; posIndex < numSymOnReel; posIndex ++)
                    {
                        let symStartX = reelStartPosX;
                        let symStartY = reelStartPosY + (posIndex * symbolHeight);

                        symbolRectsForReel.push(
                            new PIXI.Rectangle(symStartX, symStartY, symbolWidth, symbolHeight));
                    }

                    reelRectsForHand.push(new PIXI.Rectangle(reelStartPosX, reelStartPosY, reelWidth, reelHeight));
                    symbolRectsForHand.push(symbolRectsForReel);
                }

                this._symbolBounds.push(symbolRectsForHand);
                this._reelBounds.push(reelRectsForHand);

                // Now, create the total bounds for this secondary hand
                this._reelsBounds.push(new PIXI.Rectangle(
                    handSymStartPosX, handSymStartPosY, handWidth, handHeight));
            }
        }

        /**
         * Scale applied to reels group, to get it to correct size in screen-space.
         * @private
         * @type {number}
         */
        this._reelsScale = reelsScale;

        /**
         * Left x position of the reels group, in screen space.
         * @private
         * @type {number}
         */
        this._reelsContainerPosX = layout.relWidth(slotAreaWithinGui.x) - (reelsScale * slotLocalLayout.reelsAnchorPosX);

        /**
         * Top y position of the reels group, in screen space.
         * @private
         * @type {number}
         */
        this._reelsContainerPosY = layout.relHeight(slotAreaWithinGui.y) - (reelsScale * slotLocalLayout.reelsAnchorPosY);

        log.info(`ReelPositionUtil: reels anchor position {x:${layout.relWidth(slotAreaWithinGui.x)},y:${layout.relHeight(slotAreaWithinGui.y)}`);
        log.info(`ReelPositionUtil: reels local anchor = (x:${slotLocalLayout.reelsAnchorPosX},y:${slotLocalLayout.reelsAnchorPosY})`);
        log.info(`ReelPositionUtil: reels top-left corner = {x:${this._reelsContainerPosX},y:${this._reelsContainerPosY}}`);
        log.info(`ReelPositionUtil: reelsScale: ${this._reelsScale}`);
    };


    /**
     * Returns the central position of a given symbol on the main hand. The position
     * returned specifies x / y pixel coodinates, within the internal space of the
     * reelGroup.
     * If the values  of the reelIndex and posIndex parameters passed are out of range,
     * then a position of { x:0, y:0 } will be returned.
     * @todo: Maybe this should throw an error instead ?
     * @public
     * @param {number} reelIndex
     * The index of the reel to fetch the symbol center position for.
     * @param {number} posIndex 
     * The position index (within the reel) to fetch the symbol center position for.
     * @return {{x:number,y:number}}
     * A pair of coordinates, representing the center position of the symbol (in reel-group
     * coordinate space)
     */
    getCenterPositionOfSymbolInReelSpace(reelIndex, posIndex)
    {
        return this.getPositionWithinSymbolInReelSpace(reelIndex, posIndex, 0.5, 0.5);
    };


    /**
     * Returns an offset position of a given symbol on the main hand. The position
     * returned specifies x / y pixel coodinates, within the internal space of the
     * reelGroup. The posX and posY parameters specify the proportion of the symbol
     * that the position should be in, and each one is in the range of 0 to 1.
     * eg: posX/posY of (0,0) specifies top-left, (1,1) specifies bottom right,
     * (0.5,0.5) specifies the center: it is of course valid to use different values
     * for each of posX / posY
     * If the values  of the reelIndex and posIndex parameters passed are out of range,
     * then a position of { x:0, y:0 } will be returned.
     * @todo: Maybe this should throw an error instead ?
     * @public
     * @param {number} reelIndex
     * The index of the reel to fetch the symbol position for.
     * @param {number} posIndex 
     * The position index (within the reel) to to fetch the symbol position for.
     * @param {number} posX
     * The proportion horizontally across the symbol to return a position for, in the
     * range of 0 to 1, where 0 == fully-left and 1 == fully-right
     * @param {number} posY
     * The proportion vertically of the symbol to return a position for, in the range
     * of 0 to 1, where 0 == fully-top, and 1 == fully-bottom
     * @return {{x:number,y:number}}
     * A pair of coordinates, representing the position within the symbol requested (in
     * reel-group space)
     */
    getPositionWithinSymbolInReelSpace(reelIndex, posIndex, posX, posY)
    {
        return this.getPositionWithinSymbolInReelSpaceForHand(0, reelIndex, posIndex, posX, posY);
    };


    /**
     * Returns an offset position of a given symbol on any arbitrary hand. The position
     * returned specifies x / y pixel coodinates, within the internal space of the
     * reelGroup. The posX and posY parameters specify the proportion of the symbol
     * that the position should be in, and each one is in the range of 0 to 1.
     * eg: posX/posY of (0,0) specifies top-left, (1,1) specifies bottom right,
     * (0.5,0.5) specifies the center: it is of course valid to use different values
     * for each of posX / posY
     * If the values  of the reelIndex and posIndex parameters passed are out of range,
     * then a position of { x:0, y:0 } will be returned.
     * @todo: Maybe this should throw an error instead ?
     * @public
     * @param {number} handIndex
     * The index of the hand to fetch bounds for.
     * @param {number} reelIndex
     * The index of the reel to fetch the symbol position for.
     * @param {number} posIndex 
     * The position index (within the reel) to to fetch the symbol position for.
     * @param {number} posX
     * The proportion horizontally across the symbol to return a position for, in the
     * range of 0 to 1, where 0 == fully-left and 1 == fully-right
     * @param {number} posY
     * The proportion vertically of the symbol to return a position for, in the range
     * of 0 to 1, where 0 == fully-top, and 1 == fully-bottom
     * @return {{x:number,y:number}}
     * A pair of coordinates, representing the position within the symbol requested (in
     * reel-group space)
     */
    getPositionWithinSymbolInReelSpaceForHand(handIndex, reelIndex, posIndex, posX, posY)
    {
        let position = { x:0, y:0 };

        if (reelIndex < this._symbolBounds.length &&
            posIndex < this._symbolBounds[reelIndex].length)
        {
            let symbolRect = this._symbolBounds[handIndex][reelIndex][posIndex];
            position.x = symbolRect.left + (symbolRect.width * posX);
            position.y = symbolRect.top + (symbolRect.height * posY);
        }

        return position;
    }


    /**
     * @public
     * @param {number} reelIndex
     * The index of the reel to fetch the symbol center position for.
     * @param {number} posIndex 
     * The position of the reel to fetch the symbol center position for.
     * @return {{x:number,y:number}}
     */
    getCenterPositionOfSymbolInScreenSpace(reelIndex, posIndex)
    {
        let position = this.getCenterPositionOfSymbolInReelSpace(reelIndex, posIndex);

        this.changePosToScreenSpace(position);

        return position;
    };


    // TODO: Add proportional method here as well


    /**
     * Returns bounds of any given symbol on the primary hand, in reel space.
     * @public
     * @param {number} reelIndex
     * The index of the reel to fetch the symbol center position for.
     * @param {number} posIndex 
     * The position of the reel to fetch the symbol center position for.
     * @return {Bounds}
     */
    getBoundsOfSymbolInReelSpace(reelIndex, posIndex)
    {
        return this.getBoundsOfSymbolInReelSpaceForHand(0, reelIndex, posIndex);
    };


    /**
     * Returns bounds of any given symbol, on any given hand, in reel space.
     * @public
     * @param {number} reelIndex
     * The index of the reel to fetch the symbol center position for.
     * @param {number} posIndex 
     * The position of the reel to fetch the symbol center position for.
     * @return {Bounds}
     */
    getBoundsOfSymbolInReelSpaceForHand(handIndex, reelIndex, posIndex)
    {
        /** @type {Bounds} */
        let bounds = { x:0, y:0, centerX:0, centerY:0, width:0, height:0 };

        if (handIndex < this._symbolBounds.length &&
            reelIndex < this._symbolBounds[handIndex].length &&
            posIndex < this._symbolBounds[handIndex][reelIndex].length)
        {
            let symbolRect = this._symbolBounds[handIndex][reelIndex][posIndex];

            bounds.x = symbolRect.left;
            bounds.y = symbolRect.top;
            bounds.centerX = symbolRect.left + (0.5 * symbolRect.width);
            bounds.centerY = symbolRect.top + (0.5 * symbolRect.height);
            bounds.width = symbolRect.width;
            bounds.height = symbolRect.height;
        }

        return bounds;
    };


    /**
     * Returns bounds of any given symbol on the primary hand, in screen space.
     * @public
     * @param {number} reelIndex
     * The index of the reel to fetch the symbol center position for.
     * @param {number} posIndex 
     * The position of the reel to fetch the symbol center position for.
     * @return {Bounds}
     */
    getBoundsOfSymbolInScreenSpace(reelIndex, posIndex)
    {
        return this.getBoundsOfSymbolInScreenSpaceForHand(0, reelIndex, posIndex)
    };


    /**
     * Returns bounds of any given symbol, on any given hand, in screen space.
     * @public
     * @param {number} handIndex
     * @param {number} reelIndex
     * The index of the reel to fetch the symbol center position for.
     * @param {number} posIndex 
     * The position of the reel to fetch the symbol center position for.
     * @return {Bounds}
     */
    getBoundsOfSymbolInScreenSpaceForHand(handIndex, reelIndex, posIndex)
    {
        let bounds = this.getBoundsOfSymbolInReelSpaceForHand(handIndex, reelIndex, posIndex);

        this.changeBoundsToScreenSpace(bounds);

        return bounds;
    };


    /**
     * Returns the bounds of all reels for the primary hand, in reel space. This doesn't include
     * the reels-frame: it is equivalent to the total bounds of all of the individual reels.
     * @public
     * @return {Bounds}
     */
    getBoundsOfReelsInReelSpace()
    {
        return this.getBoundsOfReelsInReelSpaceForHand(0);
    };


    /**
     * Returns the bounds of all reels for any given hand, in reel space. This doesn't include
     * the reels-frame: it is equivalent to the total bounds of all of the individual reels.
     * @public
     * @param {number} handIndex
     * @return {Bounds}
     */
    getBoundsOfReelsInReelSpaceForHand(handIndex)
    {
        let reelsBoundsForHand = this._reelsBounds[handIndex];

        /** @type {Bounds} */
        let bounds =
        {
            x: reelsBoundsForHand.left,
            y: reelsBoundsForHand.top,
            centerX: reelsBoundsForHand.left + (reelsBoundsForHand.width * 0.5),
            centerY: reelsBoundsForHand.top  + (reelsBoundsForHand.height * 0.5),
            width:  reelsBoundsForHand.width,
            height: reelsBoundsForHand.height
        };

        return bounds;
    };


    /**
     * Returns the bounds of all reels for the primary hand, in screen space. This doesn't include
     * the reels-frame: it is equivalent to the total bounds of all of the individual reels.
     * @public
     * @return {Bounds}
     */
    getBoundsOfReelsInScreenSpace()
    {
        return this.getBoundsOfReelsInScreenSpaceForHand(0);
    };


    /**
     * Returns the bounds of all reels for any given hand, in screen space. This doesn't include
     * the reels-frame: it is equivalent to the total bounds of all of the individual reels.
     * @public
     * @param {number} handIndex
     * @return {Bounds}
     */
    getBoundsOfReelsInScreenSpaceForHand(handIndex)
    {
        let bounds = this.getBoundsOfReelsInReelSpaceForHand(handIndex);

        this.changeBoundsToScreenSpace(bounds);
        
        return bounds;
    };


    /**
     * Returns bounds of any given reel from the primary hand, in reel space.
     * @public
     * @param {number} reelIndex 
     * @return {Bounds}
     */
    getBoundsOfReelInReelSpace(reelIndex)
    {
        return this.getBoundsOfReelInReelSpaceForHand(0, reelIndex);
    };
    

    /**
     * Returns bounds of any given reel, for any given hand, in reel space.
     * @public
     * @param {number} handIndex
     * @param {number} reelIndex 
     * @return {Bounds}
     */
    getBoundsOfReelInReelSpaceForHand(handIndex, reelIndex)
    {
        /** @type {Bounds} */
        let bounds = { x:0, y:0, centerX:0, centerY:0, width:0, height:0 };

        if (handIndex < this._reelBounds.length &&
            reelIndex < this._reelBounds[handIndex].length)
        {
            let reelRect = this._reelBounds[handIndex][reelIndex];
            bounds.x = reelRect.left;
            bounds.y = reelRect.top;
            bounds.centerX = reelRect.left + (reelRect.width * 0.5);
            bounds.centerY = reelRect.top + (reelRect.height * 0.5);
            bounds.width = reelRect.width;
            bounds.height = reelRect.height;
        }

        return bounds;
    };


    /**
     * Returns bounds of any given reel from the primary hand, in screen space.
     * @public
     * @param {number} reelIndex 
     * @return {Bounds}
     */
    getBoundsOfReelInScreenSpace(reelIndex)
    {
        return this.getBoundsOfReelInScreenSpaceForHand(0, reelIndex);
    };


    /**
     * Returns bounds of any given reel, from any given hand, in screen space.
     * @public
     * @param {number} handIndex
     * @param {number} reelIndex 
     * @return {Bounds}
     */
    getBoundsOfReelInScreenSpaceForHand(handIndex, reelIndex)
    {
        let bounds = this.getBoundsOfReelInReelSpaceForHand(handIndex, reelIndex);

        this.changeBoundsToScreenSpace(bounds);

        return bounds;
    };


    /**
     * @private
     * @param {{x:number,y:number}} pos 
     */
    changePosToScreenSpace(pos)
    {
        pos.x = this._reelsContainerPosX + (pos.x * this._reelsScale);
        pos.y = this._reelsContainerPosY + (pos.y * this._reelsScale);
    };


    /**
     * @private
     * @param {Bounds} bounds 
     */
    changeBoundsToScreenSpace(bounds)
    {
        bounds.x = this._reelsContainerPosX + (bounds.x * this._reelsScale);
        bounds.y = this._reelsContainerPosY + (bounds.y * this._reelsScale);
        bounds.centerX = this._reelsContainerPosX + (bounds.centerX * this._reelsScale);
        bounds.centerY = this._reelsContainerPosY + (bounds.centerY * this._reelsScale);
        bounds.width = bounds.width * this._reelsScale;
        bounds.height = bounds.height * this._reelsScale;
    };
}