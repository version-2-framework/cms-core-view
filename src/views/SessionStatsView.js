import BaseView from "./BaseView";
import {SessionStatsViewModel, SessionStatsViewEvents} from "../../../cms-core-logic/src/controllers/SessionStatsViewModel";
import C_PointerEvt from "../const/C_PointerEvt";
import C_GameEvent from "../../../cms-core-logic/src/const/C_GameEvent";
import Button from "../ui/Button";

/**
 * SessionStatsView
 * Responsible for displaying the data upon closing a session.
 * Relies on SessionStatsViewModel
 */
class SessionStatsView extends BaseView
{
    constructor()
    {
        super();

        // TODO: No longer needed
        this.spinTweener = {};

        this.container = new PIXI.Container();
        this.addChild(this.container);
        this.container.alpha = 0;

        let isMobileOrTablet = this._platform.isMobileOrTablet();

        /**
         * Calculated height of the header area, in pixels.
         * @private
         * @type {number}
         */
        this.headerHeight = Math.round(this.layout.relHeight(10));

        /**
         * The height (in pixels) of the footer.
         * @private
         * @type {number}
         */
        this.footerHeight = Math.round(this.layout.relHeight(isMobileOrTablet? 16 : 12));

        /**
         * The width (in pixels) of a single footer button.
         * @private
         * @type {number}
         */
        this.footerBtnWidth = Math.round(this.layout.relWidth(isMobileOrTablet? 40 : 30));

        /**
         * Calculated height (in pixels) of a footer button.
         * @private
         * @type {number}
         */
        this.footerBtnHeight = Math.round(this.footerHeight * 0.9);

        /**
         * Calculated max with (in pixels) of footer text.
         * @private
         * @type {number}
         */
        this.footerBtnTxtWidth = this.footerBtnWidth * 0.9;

        /**
         * Calculated max height (in pixels) of footer text.
         * @private
         * @type {number}
         */
        this.footerBtnTxtHeight = this.footerBtnHeight * 0.9;

        /**
         * Vertical padding which must appear between footer / header / anything in between
         * @private
         * @type {number}
         */
        this.verticalPadY = this.layout.relHeight(2);

        /**
         * Game logo, displayed in the title area.
         * @private
         * @type {PIXI.DisplayObject}
         */
        this.gameLogo = this.assets.getSprite(this.app.config.sessionGameLogo);
        this.gameLogo.anchor.set(0.5, 0.5);
        this.gameLogo.x = this.layout.relWidth(50);
        this.gameLogo.y = this.headerHeight * 0.5;
        this.layout.setHeight(this.gameLogo, this.headerHeight * 0.9);
        if (this.gameLogo.width > this.layout.relWidth(50)) {
            this.layout.setWidth(this.gameLogo, this.layout.relWidth(50));
        }

        /**
         * Header background
         * @private
         * @type {PIXI.DisplayObject}
         */
        this.headerBg = this.assets.getS9Sprite('wmg_contentBox_blue');
        this.headerBg.width = this.layout.relWidth(100);
        this.headerBg.height = this.headerHeight;

        /**
         * Backround to the footer buttons area.
         * @private
         * @type {PIXI.DisplayObject}
         */
        this.footerBg = this.assets.getSprite('cashDeskGreyBottom');
        this.footerBg.width = this.layout.relWidth(100);
        this.footerBg.height = this.footerHeight;
        this.layout.verticalAlignBottom(this.footerBg);

        /**
         * WMG Logo, displayed in the top right of the screen.
         * @private
         * @type {PIXI.DisplayObject}
         */
        this.wmgLogo = this.assets.getSprite('logo_wmg_white');
        this.wmgLogo.anchor.set(1, 0.5);
        this.wmgLogo.x = this.layout.relWidth(98);
        this.wmgLogo.y = this.headerHeight * 0.5;
        this.layout.setHeight(this.wmgLogo, this.headerHeight * 0.7);
        if (this.wmgLogo.width > this.layout.relWidth(18)) {
            this.layout.setWidth(this.wmgLogo, this.layout.relWidth(18));
        }

        /**
         * The background to the whole of the Session Stats view.
         * @private
         * @type {PIXi.DisplayObject}
         */
        this.bg = this.assets.getS9Sprite('wmg_contentBox_white');
        this.bg.width = this.layout.relWidth(100);
        this.bg.height = this.layout.relHeight(100);
        this.bg.interactive = true;


        this.txtStyleBtns = { font:Math.round(this.footerBtnHeight * 0.8) + "px Arial", fill:"#2f2e2e", fontWeight:"bold"};

        
        let closingSessionText = this.locale.getString("T_sessionStats_closingSession");

        // TODO: I want to replace this instantiation code, with the standard pixi api
        /**
         * Text message, telling the player that a game session is being closed.
         * @private
         * @type {PIXI.Text}
         */
        this.closingSessionTxt = this.assets.getTF(closingSessionText, null, null, null, null, null, this.txtStyleBtns);
        this.closingSessionTxt.anchor.set(0.5, 0);
        this.closingSessionTxt.x = this.layout.relWidth(50);
        this.closingSessionTxt.y = this.layout.relHeight(40);
        this.closingSessionTxt.alpha = 0;

        /**
         * A spinner icon, which we can show while we are waiting for the game session to be closed.
         * @private
         * @type {PIXI.DisplayObject}
         */
        this.loadingCircle = this.assets.getSprite('wmg_loadingIcon_30degrees');
        this.loadingCircle.alpha = 0;
        this.loadingCircle.anchor.set(0.5, 0.5);
        this.loadingCircle.x = this.layout.relWidth(50);
        this.loadingCircle.y = this.layout.relHeight(60);
        this.layout.propHeightScale(this.loadingCircle, 16);

        /**
         * Used to contain all session stats, shown in the middle of the screen.
         * @private
         * @type {PIXI.Container}
         */
        this.contentGroup = new PIXI.Container();

        /**
         * Used to contain all footer buttons, shown on the bottom of the screen.
         * @private
         * @type {PIXI.Container}
         */
        this.footerBtnGroup = new PIXI.Container();

        this.container.addChild(this.bg);
        this.container.addChild(this.closingSessionTxt);
        this.container.addChild(this.loadingCircle);
        this.container.addChild(this.headerBg);
        this.container.addChild(this.footerBg);
        this.container.addChild(this.gameLogo);
        this.container.addChild(this.wmgLogo);
        this.container.addChild(this.contentGroup);
        this.container.addChild(this.footerBtnGroup);

        this.container.pivot.x = this.container.width * 0.5;
        this.container.pivot.y = this.container.height * 0.5;

        this.container.x += this.container.width * 0.5;
        this.container.y += this.container.height * 0.5;

        this._dispatcher.on(SessionStatsViewEvents.ENTER_STATE_CLOSING_SESSION, this.showClosingSessionState, this);
        this._dispatcher.on(SessionStatsViewEvents.EXIT_STATE_CLOSING_SESSION, this.endClosingSessionState, this);
        this._dispatcher.on(SessionStatsViewEvents.ENTER_STATE_CLOSE_SESSION_FAILED, this.showCloseSessionFailedState, this);
        this._dispatcher.on(SessionStatsViewEvents.EXIT_STATE_CLOSE_SESSION_FAILED, this.endCloseSessionFailedState, this);
        this._dispatcher.on(SessionStatsViewEvents.ENTER_STATE_SESSION_CLOSED, this.showSessionClosedState, this);
        this._dispatcher.on(SessionStatsViewEvents.EXIT_STATE_SESSION_CLOSED, this.endSessionClosedState, this);
        this._dispatcher.on(C_GameEvent.HIDE_SESSION_STATS_VIEW, this.closeSessionStatsView, this);

        this.showViewAppearing();

        /**
         * @private
         * @type {DialogController}
         */
        this._dialogController = this.app.dialogController;

        /**
         * @private
         * @type {SessionStatsViewModel}
         */
        this.sessionStatsModel = new SessionStatsViewModel(this.app);
    };


    /**
     * @private
     */
    closeSessionStatsView()
    {
        this.log.info('SessionStatsView.closeSessionStatsView()');

        this.removeFromParent();

        this._dispatcher.off(SessionStatsViewEvents.ENTER_STATE_CLOSING_SESSION, this.showClosingSessionState, this);
        this._dispatcher.off(SessionStatsViewEvents.EXIT_STATE_CLOSING_SESSION, this.endClosingSessionState, this);
        this._dispatcher.off(SessionStatsViewEvents.ENTER_STATE_CLOSE_SESSION_FAILED, this.showCloseSessionFailedState, this);
        this._dispatcher.off(SessionStatsViewEvents.EXIT_STATE_CLOSE_SESSION_FAILED, this.endCloseSessionFailedState, this);
        this._dispatcher.off(SessionStatsViewEvents.ENTER_STATE_SESSION_CLOSED, this.showSessionClosedState, this);
        this._dispatcher.off(SessionStatsViewEvents.EXIT_STATE_SESSION_CLOSED, this.endSessionClosedState, this);
        this._dispatcher.off(C_GameEvent.HIDE_SESSION_STATS_VIEW, this.closeSessionStatsView, this);
    };


    /**
     * Adds buttons to the footer. The list of buttons to create, comes from SessionStatsViewModel,
     * which may make some dynamic choices about what should be shown at individual steps. No
     * animation ios triggered by this method.
     * @private
     */    
    createFooterButtons()
    {
        let buttonConfigs = this.sessionStatsModel.getFooterButtonConfigs();

        if (buttonConfigs.length === 0)
        {
            this.log.debug("SessionStatsView: no footer buttons to show");
            return;
        }

        this.log.debug(`SessionStatsView: got ${buttonConfigs.length} buttons to create`);
        
        buttonConfigs.forEach((buttonConfig, buttonIndex) =>
        {
            let btn = new Button('error_button');
            this.layout.propHeightScale(btn, 85, this.footerBg);
            btn.x = (btn.width * 1.5) * buttonIndex;
            btn.on(C_PointerEvt.DOWN, buttonConfig.action, buttonConfig);
                
            // TODO: Replace this with standard pixi text creation api usage
            let labelText = this.locale.getString(buttonConfig.localizationId);
            let label = this.assets.getTF(labelText, null, null, null, null, null, this.txtStyleBtns);
                
            btn.addLabelTF(label);

            this.footerBtnGroup.addChild(btn);
        });

        this.layout.horizontalAlignCenter(this.footerBtnGroup, 0, this.footerBg);
        this.layout.verticalAlignMiddle(this.footerBtnGroup, 0, this.footerBg);
    };


    /**
     * Plays an initial animation of the Session Stats View appearing. This has no effect on any other
     * state based animations, and should always be played when the default Session Stats View implementation
     * appears on screen.
     * @private
     */
    showViewAppearing()
    {
        this.log.debug(`SessionStatsView.showViewAppearing()`);

        TweenMax.to(this.container, 0.5, {alpha:1});
        TweenMax.fromTo(this.container.scale, 0.5, {x:0, y:0}, {x:1, y:1, ease:Power4.easeOut });
    };


    /**
     * Shows the "Closing Session" visual state
     * @private
     */
    showClosingSessionState()
    {
        this.log.debug(`SessionStatsView.showClosingSessionState()`);

        
        let timeline = new TimelineMax();
        timeline.addLabel("start");
        timeline.to(this.closingSessionTxt, 0.7, {alpha: 1, yoyo:true, repeat: -1, repeatDelay: 0.1, ease:Power4.easeOut}, "start");
        timeline.to(this.loadingCircle, 0.7, {alpha: 1, ease:Power4.easeOut}, "start");
        timeline.fromTo(this.loadingCircle.scale, 0.7, { x:0, y:0 }, { x:this.loadingCircle.scale.x, y:this.loadingCircle.scale.y, ease:Power4.easeOut }, "start");
        timeline.to(this.loadingCircle, 1, { rotation:Math.PI, repeat:-1, ease:"Linear.easeNone" }, "start");

        /**
         * @private
         * @type {TimelineMax}
         */
        this._showClosingSessionTimeline = timeline;
    };


    /**
     * Ends the "Closing Session" visual state.
     * @private
     */
    endClosingSessionState()
    {
        this.log.debug(`SessionStatsView.endClosingSessionState()`);

        // Kill any existing timeline that was showing the "closing session state", as this is
        // an indefinite animation (keeps looping until we stop it)
        if (this._showClosingSessionTimeline) {
            this._showClosingSessionTimeline.kill();
            this._showClosingSessionTimeline = null;
        }

        let fadeOutDuration = 0.2;

        // Quickly fade out the "closing session" content
        // TODO: This content could be in its own group, which we fade in / out
        let timeline = new TimelineMax();
        timeline.addLabel("start");
        timeline.to(this.loadingCircle, fadeOutDuration, { alpha:0 }, "start");
        timeline.to(this.closingSessionTxt, fadeOutDuration, { alpha:0 }, "start");
    };


    /**
     * @private
     */
    showCloseSessionFailedState()
    {
        this.log.debug(`SessionStatsView.showCloseSessionFailedState()`);

        // Here, the decision of whether or not to use a dialog view, is a feature of
        // the view's visualization. This is not how its been done elsewher (eg: on the
        // CashierView), but perhaps it should be?

        this._dialogController.showDialogs([{
            type : "multiChoice",
            header : { localizationId : "T_dialog_SessionClosedFailed_Title" },
            message : { localizationId : "T_dialog_SessionClosedFailed_Content" },
            options : [{ localizationId : "T_button_tryAgain" }]
        }],
        () => this.sessionStatsModel.retryCloseSession());
    };


    /**
     * @private
     */
    endCloseSessionFailedState()
    {
        this.log.debug('SessionStatsView.endCloseSessionFailedState()');
    };


    /**
     * @private
     */
    showSessionClosedState()
    {
        this.log.debug('SessionStatsView.showSessionClosedState()');

        // Create footer buttons and text only now - maybe we only get the full info from
        // the ViewModel at this moment, once the session is definitely closed.
        this.createFooterButtons();
        this.createSessionStatsContentText();

        let animDuration = 0.7
        let timeline = new TimelineMax();
        timeline.addLabel("start");
        timeline.fromTo(this.contentGroup, animDuration, { alpha:0 }, { alpha:1 }, "start")
        timeline.fromTo(this.footerBtnGroup, animDuration, { alpha:0 }, { alpha:1 }, `start+=${animDuration*0.5}`);
    };


    /**
     * @private
     */
    endSessionClosedState()
    {
        this.log.debug('SessionStatsView.endSessionClosedState()');
    };


    /**
     * Creates all of the "session stats text", which appears once the session is closed. No animations are
     * triggered by this method.,
     * @private
     */
    createSessionStatsContentText()
    {
        let sessionStats = this.sessionStatsModel.getSessionStats();
        let statsContainer = this.contentGroup;

        sessionStats.forEach((sessionStat, sessionStatIndex) =>
        {
            let sessionStatTextValue = this.locale.getString(sessionStat.localizationId, sessionStat.localizationMods);

            // TODO: Replace this with standard pixi text creation api methods
            let sessionStatTextField = this.assets.getTF(sessionStatTextValue, null, null, null, null, null, this.txtStyleBtns);
            sessionStatTextField.y = (sessionStatTextField.height * 2) * sessionStatIndex;
            statsContainer.addChild(sessionStatTextField);
        });

        let sessionStatsStartY = this.headerHeight + this.verticalPadY;
        let sessionStatsFinalY = this.layout.relHeight(100) - this.headerHeight - this.verticalPadY;
        let sessionStatsHeight = sessionStatsFinalY - sessionStatsStartY;

        // Position the stats container in the middle of the general stats area (as long as it
        // fits between the header and the footer, we are good)
        statsContainer.x = this.layout.relWidth(50) - (statsContainer.width * 0.5);
        statsContainer.y = sessionStatsStartY + (0.5 * sessionStatsHeight) - (statsContainer.height * 0.5);

        // Ensre that the stats text isn't larger than max allowed height
        if (statsContainer.height > sessionStatsHeight) {
            this.layout.setHeight(statsContainer, sessionStatsHeight);
        }
    };
}

export default SessionStatsView;