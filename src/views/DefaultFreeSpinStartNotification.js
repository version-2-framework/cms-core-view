import C_GameEvent from "../../../cms-core-logic/src/const/C_GameEvent";
import C_PointerEvt from "../const/C_PointerEvt";
import BaseView from "./BaseView";

// TODO: Default class wants to actually be usable!
class DefaultFreeSpinStartNotification extends BaseView
{
    constructor()
    {
        super();

        /**
         * A dark BG layer
         * @private
         * @type {PIXI.Graphics}
         */
        this._bg = new PIXI.Graphics();
        this._bg.beginFill(0x000000, 0.7);
        this._bg.drawRect(0, 0, this.layout.relWidth(100), this.layout.relHeight(100));
        this._bg.endFill();

        /**
         * @private
         * @type {PIXI.Graphics}
         */
        this._buttonOK = new PIXI.Graphics();
        this._buttonOK.beginFill(0xffffff);
        this._buttonOK.drawCircle(this.layout.relWidth(50), this.layout.relHeight(50), this.layout.relHeight(10));
        this._buttonOK.endFill();
        this._buttonOK.interactive = true;
        this._buttonOK.on(C_PointerEvt.UP, this.startFreeSpins, this);

        this.addChild(this._bg);
        this.addChild(this._buttonOK);

        this.dispatcher.on(C_GameEvent.HIDE_FREESPIN_START_NOTIFICATION, this.hideNotification, this);
    };


    /**
     * @private
     */
    startFreeSpins()
    {
        this.log.info('DefaultFreeSpinNotification.startFreeSpins()');

        this.removeFromParent();
        this.dispatcher.emit(C_GameEvent.FREESPIN_START_NOTIFICATION_CLOSED);
    };


    /**
     * @private
     */
    hideNotification()
    {
        this.removeFromParent();
    };


    /**
     * @public
     * @inheritDoc
     */
    destroy()
    {
        this.dispatcher.off(C_GameEvent.HIDE_FREESPIN_START_NOTIFICATION, this.hideNotification, this);

        super.destroy();
    };
}

export default DefaultFreeSpinStartNotification;