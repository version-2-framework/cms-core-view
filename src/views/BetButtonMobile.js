import {BaseMobileGameButton} from "./BaseMobileGameButton";
import Button from "../ui/Button";
import C_PointerEvt from "../const/C_PointerEvt";
import C_GameEvent from "../../../cms-core-logic/src/const/C_GameEvent";

/**
 * Simple, default implementation of the Mobile Bet Button
 */
class BetButtonMobile extends BaseMobileGameButton
{
    constructor()
    {
        super();

        /**
         * @private
         */
        this._button = new Button('betBtnMobile', 'betBtnMobileOver', 'betBtnMobileOver', 'betBtnMobileDisabled');
        this._button.on(C_PointerEvt.DOWN, this.onButDown, this);
        this.addChild(this._button);

        let buttonSoundId = this.app.config.uiSounds.mobile.betButtonSound;
        if (buttonSoundId)
        {
            let buttonVolume = this.app.config.uiSounds.mobile.betButtonVolume;

            this.log.debug(`AutoplayButtonMobile: setting button sound to ${buttonSoundId}`);
            
            this._button.setClickSound(buttonSoundId, buttonVolume);
        }
        else
        {
            this.log.debug('BetButtonMobile: no button sound available, button will not make sound when pressed');

            this._button.setClickSound(null);
        }

        this.dispatcher.on(C_GameEvent.ENABLE_GUI_BUTTONS, this.enableButton, this);
        this.dispatcher.on(C_GameEvent.DISABLE_GUI_BUTTONS, this.disableButton, this);
    };


    /**
     * @private
     */
    enableButton()
    {
        this.setEnabled(true);
        this._button.setEnabled(true);
    };


    /**
     * @private
     */
    disableButton()
    {
        this.setEnabled(false);
        this._button.setEnabled(false);
    };


    /**
     * Fired when the button is pressed.
     * @private
     */
    onButDown()
    {
        this.dispatcher.emit(C_GameEvent.BUTTON_TOGGLE_BET_MENU_PRESSED);
    };
}

export default BetButtonMobile;