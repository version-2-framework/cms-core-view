
import BaseView from "./BaseView";
import WinValue from "./WinValue";
import * as C_RewardGroup from "../../../cms-core-logic/src/const/C_RewardGroup";
import C_BonusViewEvent from "../const/C_BonusViewEvent";
import C_TotalWinViewEvent from "../../../cms-core-view/src/const/C_TotalWinViewEvent";
import C_Snd from "../const/C_Snd";

const DEFAULT_TITLE_REL_HEIGHT = 10;
const DEFAULT_TITLE_REL_WIDTH = 60;
const DEFAULT_TITLE_REL_POS_Y_NORMAL = 35; //40;
const DEFAULT_TITLE_REL_POS_Y_MULTIPLIER = 30;

const DEFAULT_MULTIPLIER_REL_POS_Y = 45;
const DEFAULT_MULTIPLIER_REL_WIDTH = 60;
const DEFAULT_MULTIPLIER_REL_HEIGHT = 10;

const DEFAULT_WIN_VAL_REL_WIDTH = 50;
const DEFAULT_WIN_VAL_REL_HEIGHT = 20;
const DEFAULT_WIN_VAL_REL_POS_Y_NORMAL = 55;
const DEFAULT_WIN_VAL_REL_POS_Y_MULTIPLIER = 65;

/**
 * @type {BonusTotalWinAnimationConfig}
 */
const DEFAULT_BONUS_TOTAL_WIN_CONFIG =
{
    numberCharPrefix : "wpanim_",
    useWinValue : true
}

/**
 * Default implementation of the Total Winnings Popup - used during slot phases
 * (Spin1 / Spin2 / Freespins)
 */
class BonusTotalWinningsPopUp extends BaseView
{
    constructor()
    {
        super();

        /**
         * @type {BonusTotalWinAnimationConfig}
         */
        this._totalWinConfig = this.config.totalWinningsBonusConfig ?
            this.config.totalWinningsBonusConfig : DEFAULT_BONUS_TOTAL_WIN_CONFIG;
    };


    /**
     * @public
     * @inheritDoc
     * @param {*} rewardGroup 
     * @param {*} rewardValue 
     * @param {*} multiplier 
     */
    setValue(rewardGroup, rewardValue, multiplier=false)
    {
        /**
         * The Reward Group for the win.
         * @private
         * @type {number}
         */
        this._rewardGroup = rewardGroup;

        /**
         * The reward value for the win.
         * @private
         * @type {number}
         */
        this._rewardValue = rewardValue;

        /**
         * Indicates if a multiplier needs to be shown.
         * @private
         * @type {number | false}
         */
        this._multiplier = multiplier;
    };


    // TODO: Its not clear to me why the create method exists - why can't the contsructor
    // just handle this? Why do we also pass a parameter here, when we also have "setvalue"?
    /**
     * @inheritDoc
     * @public
     * @param {number} rewardGroup
     * @param {number} rewardValue
     * @param {number | false} multiplier
     */
    create(rewardGroup=undefined, rewardValue=undefined, multiplier=undefined)
    {
        let winValueGraphic = null;

        if (rewardGroup !== undefined) {
            this._rewardGroup = rewardGroup;
        }

        if (rewardValue !== undefined) {
            this._rewardValue = rewardValue;
        }

        if (multiplier !== undefined) {
            this._multiplier = multiplier;
        }
        
        this.log.debug(`BonusTotalWinningsPopup.create: rewardGroup:${this._rewardGroup},rewardValue:${this._rewardValue},multiplier:${this._multiplier}`);

        let titleString = null;
        if (this._rewardGroup === C_RewardGroup.CREDIT) {
            titleString = this.app.locale.getString("T_Total_Winnings_PopUp");
        }
        else
        if (this._rewardGroup === C_RewardGroup.SUPERBET) {
            titleString = this.app.locale.getString("T_Total_Superbet_PopUp");
        }
        else
        if (this._rewardGroup === C_RewardGroup.FREESPIN) {
            titleString = this.app.locale.getString("T_Total_FreeSpins_PopUp")
        }

        const charPrefix = this._totalWinConfig.numberCharPrefix ?
            this._totalWinConfig.numberCharPrefix : 'wpanim_';

        const titleRelHeight = this._totalWinConfig.titleRelativeHeight > 0?
            this._totalWinConfig.titleRelativeHeight : DEFAULT_TITLE_REL_HEIGHT;

        const titleRelWidth = this._totalWinConfig.titleRelativeWidth > 0?
            this._totalWinConfig.titleRelativeWidth : DEFAULT_TITLE_REL_WIDTH;

        const titleRelPosY = this._totalWinConfig.titleRelativePosY > 0?
            this._totalWinConfig.titleRelativePosY : DEFAULT_TITLE_REL_POS_Y_NORMAL;

        const titleMultiplierRelPosY = this._totalWinConfig.titleMultiplierRelativePosY > 0?
            this._totalWinConfig.titleMultiplierRelativePosY : DEFAULT_TITLE_REL_POS_Y_MULTIPLIER;

        const winValRelPosY = this._totalWinConfig.winValueRelativePosY > 0?
            this._totalWinConfig.winValueRelativePosY : DEFAULT_WIN_VAL_REL_POS_Y_NORMAL;

        const winValMultiplierRelPosY = this._totalWinConfig.winValueMultiplierRelativePosY > 0?
            this._totalWinConfig.winValueMultiplierRelativePosY : DEFAULT_WIN_VAL_REL_POS_Y_MULTIPLIER;

        const winValRelWidth = this._totalWinConfig.winValueRelativeWidth > 0?
            this._totalWinConfig.winValueRelativeWidth : DEFAULT_WIN_VAL_REL_WIDTH;

        const winValRelHeight = this._totalWinConfig.winValueRelativeHeight > 0?
            this._totalWinConfig.winValueRelativeHeight : DEFAULT_WIN_VAL_REL_HEIGHT;

        const multiplierRelPosY = this._totalWinConfig.multiplierRelPosY > 0?
            this._totalWinConfig.multiplierRelPosY : DEFAULT_MULTIPLIER_REL_POS_Y;
            
        const multiplierRelHeight = this._totalWinConfig.multiplierRelHeight > 0?
            this._totalWinConfig.multiplierRelHeight : DEFAULT_MULTIPLIER_REL_HEIGHT;

        const multiplierRelWidth = this._totalWinConfig.multiplierRelWidth > 0?
            this._totalWinConfig.multiplierRelWidth : DEFAULT_MULTIPLIER_REL_WIDTH;

        /**
         * Creates and returns a new winvalue graphic, ready for use.
         * @private
         * @param {number} rewardValue 
         * @param {number} duration
         * @return {PIXI.DisplayObject} // TODO: An actual api for the win graphic would be better..
         */
        let createWinValueGraphic = (rewardValue, duration, inMultiplierPos=false) =>
        {
            winValueGraphic = this.gameFactory.getWinValue(rewardValue, duration);

            winValueGraphic.create();
            this.layout.setHeightOrWidthToMax(
                winValueGraphic,
                this.layout.relWidth(winValRelWidth),
                this.layout.relHeight(winValRelHeight));
            
            this.layout.horizontalAlignCenter(winValueGraphic, 0);
            winValueGraphic.y = this.layout.relHeight((inMultiplierPos? winValMultiplierRelPosY : winValRelPosY) - (winValRelHeight * 0.5));
            
            this.log.debug(`BonusTotalWinningsPopup.createWinValueGraphic: inMultiplierPos ${inMultiplierPos}`);
            this.log.debug(`BonusTotalWinningsPopup.createWinValueGraphic: posY = ${winValueGraphic.y}`);
            
            return winValueGraphic;
        };

        /** @type {SlotWinningsTextStyleConfig} */
        let titleTextStyleConfig = this.app.config.totalWinningsTextStyle;
        
        let titleTextHeight = this.layout.relHeight(titleRelHeight);
        let titleTextPadding = titleTextStyleConfig.useSizesRelativeToText?
            titleTextHeight * titleTextStyleConfig.padding / 100 : this.layout.relHeight(titleTextStyleConfig.padding);
        
        let titleTextStyle = {
            fontFamily : this.config.gameSpecificFont,
            fontSize : titleTextHeight,
            fill : titleTextStyleConfig.fill,
            fontWeight : titleTextStyleConfig.fontWeight,
            padding: titleTextPadding
        };

        if (titleTextStyleConfig.useStroke) {
            // This logic to determine strokeSize is convoluted, BUT - up to v0.7 of the core libs,
            // stroke size was determined proportionately from screen size. Right now, we are trying
            // to not break backwards compatibility on all games. So we have a flag to switch modes
            // (by default, its "relative to screen height")
            let strokeThickness;
            if (titleTextStyleConfig.useSizesRelativeToText) {
                strokeThickness = Math.max(1, Math.round(titleTextStyleConfig.strokeSize / 100 * titleTextHeight));
            }
            else
            {
                strokeThickness = this.layout.relHeight(titleTextStyleConfig.strokeSize);
            }

            titleTextStyle.stroke = titleTextStyleConfig.strokeColor;
            titleTextStyle.strokeThickness = strokeThickness;
            titleTextStyle.lineJoin = titleTextStyleConfig.lineJoin;
            titleTextStyle.miterLimit = titleTextStyleConfig.miterLimit;
        }

        if (titleTextStyleConfig.useDropShadow) {
            // As for stroking, we also support the same 2 modes for dropShadowDistance / Blur
            let dropShadowDistance, dropShadowBlur;

            if (titleTextStyleConfig.useSizesRelativeToText) {
                dropShadowDistance = Math.max(1, Math.round(titleTextStyleConfig.dropShadowDistance / 100 * titleTextHeight));
                dropShadowBlur = Math.max(1, Math.round(titleTextStyleConfig.dropShadowBlur / 100 * titleTextHeight));
            }
            else
            {
                dropShadowDistance = this.layout.relHeight(titleTextStyleConfig.dropShadowDistance);
                dropShadowBlur = this.layout.relHeight(titleTextStyleConfig.dropShadowBlur);
            }

            titleTextStyle.dropShadow = true;
            titleTextStyle.dropShadowColor = titleTextStyleConfig.dropShadowColor;
            titleTextStyle.dropShadowAlpha = titleTextStyleConfig.dropShadowAlpha;
            titleTextStyle.dropShadowDistance = dropShadowDistance;
            titleTextStyle.dropShadowBlur = dropShadowBlur;
        }

        this.log.debug(`BonusTotalWinningsPopup: textStyle = ${JSON.stringify(titleTextStyle)}`);
        
        let titleTF = new PIXI.Text(titleString, titleTextStyle);
        this.addChild(titleTF);
        titleTF.anchor.set(0.5);
        titleTF.x = this.layout.relWidth(50);
        titleTF.y = this.layout.relHeight(titleRelPosY);
        if (titleTF.width > this.layout.relWidth(titleRelWidth)) {
            this.layout.propWidthScale(titleTF, titleRelWidth);
        }
        
        let initialValueDuration = this._multiplier === 1? 3 : 2;
        
        let multiplierValueDuration = this._totalWinConfig.multiplierDuration >= 0?
            this._totalWinConfig.multiplierDuration : 1.2;

        let multiplierFinalValueDuration = this._totalWinConfig.finalMultiplierDuration >= 0?
            this._totalWinConfig.finalMultiplierDuration : multiplierValueDuration * 2;

        let multiplierTfFadeOutDuration = Math.min(0.3, multiplierFinalValueDuration * 0.5);

        let initialRewardValue = this._multiplier? this._rewardValue / this._multiplier : this._rewardValue;

        this._timeline = new TimelineMax();
        this._timeline.addLabel("start");
        this._timeline.fromTo(this, 0.1, { alpha:0 }, { alpha:1 }, "start");

        if (this._totalWinConfig.totalWinStartSound) {
            let soundConfig = this._totalWinConfig.totalWinStartSound;
            let soundId = soundConfig.soundId;
            let soundVolume = soundConfig.soundVolume;
            this._timeline.call(() => {
                this.sound.playSound(soundId, false, soundVolume);
            }, null, null, "start");
        }

        if (this._totalWinConfig.useWinValue)
        {
            let winValueGraphic = createWinValueGraphic(initialRewardValue, initialValueDuration);
            this.addChild(winValueGraphic);
            winValueGraphic.playAnimation();
        }
        else
        {
            /**
             * Container for all currently active win value chars.
             * @private
             */
            this._winValueCharsContainer = new PIXI.Container();
            this.addChild(this._winValueCharsContainer);

            winValueGraphic = this._winValueCharsContainer;

            /**
             * All cached win value chars.
             * @private
             * @type {PIXI.Sprite[]}
             */
            this._winValueChars = [];

            let totalNumWinValueCharsRequired = this._rewardValue.toString().length;
            let winCharTimeline = new TimelineMax();
            let winCharAppearTotalDuration = initialValueDuration * 0.5;
            let singleWinCharAppearDuration = 2 * winCharAppearTotalDuration / (totalNumWinValueCharsRequired + 1);
            let singleWinCharAppearDelay = winCharAppearTotalDuration / (totalNumWinValueCharsRequired + 1);

            for (let i = 0; i < totalNumWinValueCharsRequired; i ++)
            {
                let charSprite = this.assets.getSprite(`${charPrefix}0`);
                this._winValueChars.push(charSprite);
            }

            // Let's add the initial animation of win chars appearing.

            let currPosX = 0;

            initialRewardValue.toString().split("").forEach((charId, charIndex) => {
                let charSprite = this._winValueChars[charIndex];
                charSprite.anchor.set(0.5, 0.5);
                charSprite.texture = this.assets.getTexture(`${charPrefix}${charId}`);
                charSprite.x = (charSprite.width * 0.5) + currPosX;

                currPosX += charSprite.width;

                this._winValueCharsContainer.addChild(charSprite);
            });

            this._winValueCharsContainer.children.forEach(winCharSprite => {
                winCharSprite.x -= currPosX * 0.5;
            });

            this._winValueCharsContainer.x = this.layout.relWidth(50);
            this._winValueCharsContainer.y = this.layout.relHeight(winValRelPosY);

            // Ensure win value fits in bounds as required
            this.layout.setHeightOrWidthToMax(
                this._winValueCharsContainer,
                this.layout.relWidth(winValRelWidth),
                this.layout.relHeight(winValRelHeight));

            // Build the timeline
            this._winValueCharsContainer.children.forEach((winCharSprite, winCharIndex) => {
                let finalWidth = winCharSprite.width;
                let finalHeight = winCharSprite.height;
                let startWidth = finalWidth * 1.5;
                let startHeight = finalHeight * 1.5;
                
                winCharTimeline.fromTo(
                    winCharSprite,
                    singleWinCharAppearDuration,
                    { width:startWidth, height:startHeight, alpha:0 },
                    { width:finalWidth, height:finalHeight, alpha:1 },
                    winCharIndex * singleWinCharAppearDelay);
            });

            // Fade out at end
            winCharTimeline.to(
                this._winValueCharsContainer,
                singleWinCharAppearDuration,
                { alpha:0 },
                initialValueDuration - singleWinCharAppearDuration);

            this._timeline.add(winCharTimeline, "start");
        }

        // Play the optional sound.
        if (this._totalWinConfig.soundId) {
            let soundId = this._totalWinConfig.soundId;
            let soundVolume = this._totalWinConfig.soundVolume >= 0? this._totalWinConfig.soundVolume : 1.0;
            this._timeline.call(() => this.sound.playSound(soundId, false, soundVolume), null, "start");
        }

        if (this._multiplier)
        {
            this.log.debug(`BonusTotalWinningsPopup: use multiplier animation`);

            let targetPosY = this.layout.relHeight(winValMultiplierRelPosY);
            if (this._totalWinConfig.useWinValue) {
                targetPosY -= this.layout.relHeight(winValRelHeight) * 0.5;
                this.log.debug(`BonusTotalWinningsPopup: targetPosY for WinValue after slide = ${targetPosY}`);
            }

            // Move apart the title and win value text
            let slideApartTime = initialValueDuration * 0.5;
            let slideApartDelay = initialValueDuration - slideApartTime;
            this._timeline.to(titleTF, slideApartTime, { y:this.layout.relHeight(titleMultiplierRelPosY) }, `start+=${slideApartDelay}`);
            this._timeline.to(winValueGraphic, slideApartTime, {
                y: targetPosY
            }, `start+=${slideApartDelay}`);

            /** @type {SlotWinningsTextStyleConfig} */
            let multiplierTextStyleConfig = this.config.totalWinningsMultiplierTextStyle;

            let multiplierTextHeight = this.layout.relHeight(multiplierRelHeight);
            let multiplierTextPadding = multiplierTextStyleConfig.useSizesRelativeToText?
                titleTextHeight * multiplierTextStyleConfig.padding / 100 : this.layout.relHeight(multiplierTextStyleConfig.padding);

            
            let multiplierTextStyle = {
                fontFamily : this.config.gameSpecificFont,
                fontSize : multiplierTextHeight,
                fill : multiplierTextStyleConfig.fill,
                fontWeight : multiplierTextStyleConfig.fontWeight,
                padding: multiplierTextPadding
            };
    
            if (multiplierTextStyleConfig.useStroke) {
                // This logic to determine strokeSize is convoluted, BUT - up to v0.7 of the core libs,
                // stroke size was determined proportionately from screen size. Right now, we are trying
                // to not break backwards compatibility on all games. So we have a flag to switch modes
                // (by default, its "relative to screen height")
                let strokeThickness;
                if (multiplierTextStyleConfig.useSizesRelativeToText) {
                    strokeThickness = Math.max(1, Math.round(multiplierTextStyleConfig.strokeSize / 100 * titleTextHeight));
                }
                else
                {
                    strokeThickness = this.layout.relHeight(multiplierTextStyleConfig.strokeSize);
                }

                multiplierTextStyle.stroke = multiplierTextStyleConfig.strokeColor;
                multiplierTextStyle.strokeThickness = strokeThickness;
                multiplierTextStyle.lineJoin = multiplierTextStyleConfig.lineJoin;
                multiplierTextStyle.miterLimit = multiplierTextStyleConfig.miterLimit;
            }
    
            if (multiplierTextStyleConfig.useDropShadow) {
                // As for stroking, we also support the same 2 modes for dropShadowDistance / Blur
                let dropShadowDistance, dropShadowBlur;

                if (multiplierTextStyleConfig.useSizesRelativeToText) {
                    dropShadowDistance = Math.max(1, Math.round(multiplierTextStyleConfig.dropShadowDistance / 100 * titleTextHeight));
                    dropShadowBlur = Math.max(1, Math.round(multiplierTextStyleConfig.dropShadowBlur / 100 * titleTextHeight));
                }
                else
                {
                    dropShadowDistance = this.layout.relHeight(multiplierTextStyleConfig.dropShadowDistance);
                    dropShadowBlur = this.layout.relHeight(multiplierTextStyleConfig.dropShadowBlur);
                }

                multiplierTextStyle.dropShadow = true;
                multiplierTextStyle.dropShadowColor = multiplierTextStyleConfig.dropShadowColor;
                multiplierTextStyle.dropShadowAlpha = multiplierTextStyleConfig.dropShadowAlpha;
                multiplierTextStyle.dropShadowDistance = dropShadowDistance;
                multiplierTextStyle.dropShadowBlur = dropShadowBlur;
            }

            this.log.debug(`BonusTotalWinningsPopup: multiplierTextStyle = ${JSON.stringify(multiplierTextStyle)}`);

            let multiplierText = this.locale.getString('T_Total_BonusMultiplier_PopUp', {"[MULTIPLIER]": this._multiplier});
            let multiplierTF = new PIXI.Text(multiplierText, multiplierTextStyle);
            multiplierTF.anchor.set(0.5, 0.5);
            multiplierTF.alpha = 0;
            multiplierTF.x = this.layout.relWidth(50);
            multiplierTF.y = this.layout.relHeight(multiplierRelPosY);
            if (multiplierTF.width > this.layout.relWidth(multiplierRelWidth)) {
                this.layout.propWidthScale(multiplierTF, multiplierRelWidth);
            }
            this.addChild(multiplierTF);

            // TODO: The present implementation is copied from what Igor had originally done,
            // but its not a future proof implementation, because we are using a fixed time
            // constant for total length, and fixed local time constants for delays between
            // each multiplier level up. It may not be a safe assumption that we will always
            // have a total bonus multiplier of 5x - for some future games, its 100% possible
            // that we will have a multiplier value more like 100x. So, we will need to add
            // some logic that can handle this more gracefully - one simple way, would be,
            // - if multiplier is from x1 to x5, use currently timings and logic
            // - if multiplier is from x6 to x10, using current logic but faster timings
            // - if multiplier is above x10, use a "single step" sequence, but use timings
            //   to make it look nice
            this._timeline.fromTo(
                multiplierTF, 0.3,
                { alpha:0, width:multiplierTF.width * 1.5, height:multiplierTF.height * 1.5 },
                { alpha:1, width:multiplierTF.width,       height:multiplierTF.height },
                `start+=${slideApartDelay}`);
            
            // If we are given a multiplier, but its only x1, then we will not be able to show
            // any kind of "increase the value" sequence (but we are still showing a "x1" multiplier
            // win). In this scenario, we simply need to insert a delay, so the sequence doesn't end
            // too quickly.
            if (this._multiplier === 1)
            {
                // By adding in the fade out on the Multiplier TextField, we effectively create
                // the required pause. In the case of this animation, we measure it from the start
                // of the timeline (and not from the end of the slide), as we have had to arbitrarily
                // extend the initial value duration
                let multiplierFadeOutOffset = initialValueDuration - multiplierTfFadeOutDuration;
                this._timeline.to(multiplierTF, multiplierTfFadeOutDuration, { alpha:0 }, `start+=${multiplierFadeOutOffset}`);
            }
            else
            {
                this._timeline.addLabel("startMultiplying", "+=0.1");

                let currDelay = 0;

                // Add the sequence of multiplied win values
                for (let multiplierIndex = 1; multiplierIndex < this._multiplier; multiplierIndex ++)
                {
                    let currMultiplierValue = multiplierIndex + 1;
                    let currWinValue = initialRewardValue * currMultiplierValue;
                    let isFinalMultiplier = currMultiplierValue === this._multiplier;
                    let duration = isFinalMultiplier? multiplierFinalValueDuration : multiplierValueDuration;
                    let currLabelId = `multiplier${multiplierIndex}`;

                    this.log.debug(`queueing next multiplier anim (multiplier:${currMultiplierValue},winValue:${currWinValue},duration:${duration})`);

                    this._timeline.addLabel(currLabelId, `startMultiplying+=${currDelay}`);

                    currDelay += duration;

                    if (this._totalWinConfig.useWinValue)
                    {
                        this._timeline.call(() =>
                        {
                            // Clear the existing win value graphic
                            this.removeChild(winValueGraphic);
                            winValueGraphic.destroy();

                            // Create the new win value graphic for the new "multiplied" win value
                            winValueGraphic = createWinValueGraphic(currWinValue, duration, true);
                            this.addChild(winValueGraphic);
                            winValueGraphic.playAnimation();

                            if (this._totalWinConfig.multiplierUpSound)
                            {
                                let multiplierSoundConfig = this._totalWinConfig.multiplierUpSound;
                                let soundId = multiplierSoundConfig.soundId;
                                let soundVolume = multiplierSoundConfig.soundVolume;
                                this.sound.playSound(soundId, false, soundVolume);
                            }
                        }, null, null, currLabelId);
                    }
                    else
                    {
                        this._timeline.call(() => {
                            this._winValueCharsContainer.removeChildren();

                            let currPosX = 0;

                            currWinValue.toString().split("").forEach((charId, charIndex) => {
                                let winCharSprite = this._winValueChars[charIndex];
                                winCharSprite.texture = this.assets.getTexture(`${charPrefix}${charId}`);
                                winCharSprite.anchor.set(0.5, 0.5);
                                winCharSprite.x = (winCharSprite.width * 0.5) + currPosX;

                                currPosX += winCharSprite.width;

                                this._winValueCharsContainer.addChild(winCharSprite);
                            });

                            // Center chars around (0,0)
                            this._winValueCharsContainer.children.forEach(charSprite => {
                                charSprite.x -= currPosX * 0.5;
                            });

                            this._winValueCharsContainer.alpha = 1;
                            this._winValueCharsContainer.x = this.layout.relWidth(50);
                            this._winValueCharsContainer.y = this.layout.relHeight(winValMultiplierRelPosY);

                            // Ensure win value fits in bounds as required
                            this.layout.setHeightOrWidthToMax(
                                this._winValueCharsContainer,
                                this.layout.relWidth(winValRelWidth),
                                this.layout.relHeight(winValRelHeight));

                            // Build a timeline
                            let winCharTimeline = new TimelineMax();
                            let totalNumWinValueCharsRequired = currWinValue.toString().length;
                            let winCharAppearTotalDuration = duration * 0.5;
                            let singleWinCharAppearDuration = 2 * winCharAppearTotalDuration / (totalNumWinValueCharsRequired + 1);
                            let singleWinCharAppearDelay = winCharAppearTotalDuration / (totalNumWinValueCharsRequired + 1);
                            
                            if (this._totalWinConfig.multiplierUpSound)
                            {
                                let multiplierSoundConfig = this._totalWinConfig.multiplierUpSound;
                                let soundId = multiplierSoundConfig.soundId;
                                let soundVolume = multiplierSoundConfig.soundVolume;

                                winCharTimeline.call(() => this.sound.playSound(soundId, false, soundVolume));
                            }
                            
                            // Make all chars appear
                            this._winValueCharsContainer.children.forEach((winCharSprite, winCharIndex) => {
                                let finalWidth = winCharSprite.width;
                                let finalHeight = winCharSprite.height;
                                let startWidth = winCharSprite.width * 1.5;
                                let startHeight = winCharSprite.height * 1.5;

                                winCharSprite.alpha = 0;
                                winCharSprite.width = startWidth;
                                winCharSprite.height = startHeight;
                                
                                winCharTimeline.to(
                                    winCharSprite,
                                    singleWinCharAppearDuration,
                                    { width:finalWidth, height:finalHeight, alpha:1 },
                                    winCharIndex * singleWinCharAppearDelay);
                            });

                            // Fade out at end
                            winCharTimeline.to(
                                this._winValueCharsContainer,
                                singleWinCharAppearDuration,
                                { alpha:0 },
                                duration - singleWinCharAppearDuration);

                            // Add new timeline at appropriate point in main timeline
                            this._timeline.add(winCharTimeline, currLabelId);

                        }, null, null, currLabelId);
                    }

                    // Insert dummy tween, to pad out the length of the sequence
                    this._timeline.to({}, duration, {});
                }

                // Fade out the multiplier text. We do this shortly before the last multiplier value has
                // finished showing - and the last multiplier uses a special, extra long duration. We
                // can simply fade out a quarter of the way from the end.
                //let multiplierFadeOutDuration = multiplierFinalValueDuration * 0.25;
                //this._timeline.to(multiplierTF, multiplierFadeOutDuration, { alpha:0 }, `-=${multiplierFadeOutDuration}`);

                this._timeline.to(multiplierTF, multiplierTfFadeOutDuration, { alpha:0 }, `-=${multiplierTfFadeOutDuration}`);
            }
        }
        else
        {
            this.log.debug(`BonusTotalWinningsPopup: no multiplier animation to show`);

            // Insert a pause, making sure the overall animation is long enough
            this._timeline.set({}, {}, initialValueDuration);
        }
        
        if (this._totalWinConfig.totalWinEndSound)
        {
            let soundId = this._totalWinConfig.totalWinEndSound.soundId;
            let soundVolume = this._totalWinConfig.totalWinEndSound.soundVolume;
            this._timeline.call(() => this.sound.playSound(soundId, false, soundVolume));
        }
        
        this._timeline.to(this, 0.25, { alpha:0 }); // fade out at end
        this._timeline.call(() => this.emit(C_TotalWinViewEvent.COMPLETE));
        this._timeline.call(() => this._dispatcher.emit(C_BonusViewEvent.TOTAL_WINNINGS_COMPLETE));
        this._timeline.call(() => this.remove());
    };


    


    /**
     * @private
     */
    remove()
    {
        this.log.debug('TotalWinningsPopup.remove()');

        TweenMax.killTweensOf(this);
        
        if (this._timeline) {
            this._timeline.kill();
            this._timeline = null;
        }

        super.destroy();
    };
}

export default BonusTotalWinningsPopUp