/**
 * Desktop specific UI.
 */
import Button from "../ui/Button";
import C_PointerEvt from "../const/C_PointerEvt";
import BaseView from "./BaseView";
import C_GameEvent from "../../../cms-core-logic/src/const/C_GameEvent";
import SessionInfoView from './SessionInfoView';
import C_TooltipOrientation from "../const/C_TooltipOrientation";
import C_TooltipPosition from "../const/C_TooltipPosition";
import * as DefaultKeyCodes from "../input/DefaultKeyCodes";

class UIDesktopMinimal extends BaseView
{
    constructor()
    {
        super();

        this.name = "GuiDesktop";

        /**
         * @private
         * @type {DesktopGuiConfig}
         */
        this._guiConfig = this._businessConfig.desktopGui;

        if (!this._guiConfig) {
            this.log.error(`UIDesktop: GuiConfig is not defined! UI will fall back to basic state`);
        }

        /**
         * @private
         * @type {GuiLayoutConfig}
         */
        this._uiLayout = this.config.uiLayoutDesktop;

        /**
         * @todo: Document properly
         * @private
         */
        this._guiButtons = [];

        this.defineLayout();

        let guiConfig = this._guiConfig;

        let placeMessageBarAboveGameButtons = this.app.config.uiStyle.desktopPlaceMessageBarAboveGameButtons;

        if (!placeMessageBarAboveGameButtons) {
            this.createMessageBar();
        }

        if (guiConfig && guiConfig.topLeftInfoSlot) {
            let infoSlotConfig = guiConfig.topLeftInfoSlot;
            let infoSlotArea = this._topLeftInfoSlotArea;
            this.createInfoSlotComponent(infoSlotConfig, infoSlotArea);
        }

        this.createGameLogo();
        
        if (guiConfig && guiConfig.topRightInfoSlot) {
            let infoSlotConfig = guiConfig.topRightInfoSlot;
            let infoSlotArea = this._topRightInfoSlotArea;
            this.createInfoSlotComponent(infoSlotConfig, infoSlotArea);
        }

        this.getValidatedGameButtonsZOrder().forEach(gameButtonId =>
        {
            if (gameButtonId === "auto") {
                this.createAutoplayButton();
            }
            else
            if (gameButtonId === "bet") {
                this.createBetButton();
            }
            else
            if (gameButtonId === "spin") {
                this.createSpinButton();
            }
        });

        if (placeMessageBarAboveGameButtons) {
            this.createMessageBar();
        }

        this.createInfoBarBackground();
        this.createGameInfoTexts();

        if (guiConfig)
        {
            // left footer buttons first. These are [ Menu, AddCredit, Sound ].
            // Not all may exist, due to config from licensee.

            let leftSlotIndex = 0;

            if (guiConfig.openMenuButton && guiConfig.openMenuButton.isEnabled) {
                let rect = this._leftFooterButtonSlots[leftSlotIndex];
                this.createOpenMenuBtn(rect);
                leftSlotIndex += 1;
            }

            if (guiConfig.toggleSoundButton && guiConfig.toggleSoundButton.isEnabled) {
                let rect = this._leftFooterButtonSlots[leftSlotIndex];
                this.createSoundBtn(rect);
                leftSlotIndex += 1;
            }

            if (guiConfig.addCreditButton && guiConfig.addCreditButton.isEnabled) {
                let rect = this._leftFooterButtonSlots[leftSlotIndex];
                this.createAddCreditBtn(rect);
                leftSlotIndex += 1;
            }

            // TODO: I think.. text rendering can be handled by a different class, but,
            // there is too much crossover now between text and this. So, i am just
            // going to implement the text handling part here.

            // Right Footer Buttons, these are (in order from right to left) [ Close, FullScreen ]
            // Not all may exist, due to config from licensee.
            let rightSlotIndex = 0;

            if (guiConfig.closeSessionButton && guiConfig.closeSessionButton.isEnabled) {
                let rect = this._rightFooterButtonSlots[rightSlotIndex];
                this.createCloseSessionBtn(rect);
                rightSlotIndex += 1;
            }

            if (guiConfig.fullScreenButton && guiConfig.fullScreenButton.isEnabled) {
                let rect = this._rightFooterButtonSlots[rightSlotIndex];
                this.createFullScreenBtn(rect);
                rightSlotIndex += 1;
            }
        }

        if (false)
        {
            this.log.debug('UiDesktop: add layout overlay');

            this.addChild(this.getLayoutOverlay());
        }

        // Initially disable input, until we are explicitly told to enable it
        this.setInputEnabledTo(false);
        
        this.dispatcher.on(C_GameEvent.ENABLE_GUI_BUTTONS, () => this.setInputEnabledTo(true));
        this.dispatcher.on(C_GameEvent.DISABLE_GUI_BUTTONS, () => this.setInputEnabledTo(false));
    };


    /**
     * Returns the target game buttons order. This will validate the config data - if the config data
     * is no good, this will simply return standard ordering.
     * @private
     * @return string[]
     */
    getValidatedGameButtonsZOrder()
    {
        let gameButtonsOrder = this.app.config.uiStyle.desktopGameButtonsZOrder;
        if (gameButtonsOrder.length === 3 &&
            gameButtonsOrder.indexOf("auto") > -1 &&
            gameButtonsOrder.indexOf("bet") > -1 &&
            gameButtonsOrder.indexOf("spin") > -1)
        {
            return gameButtonsOrder;
        }
        else
        {
            this.log.warn(`gameButtonsOrder ${gameButtonsOrder} is not valid. Using default values`);
            return ["auto", "bet", "spin"];
        }
    };


    /**
     * Configures the general layout areas of the desktop GUI.
     * @protected
     */
    defineLayout()
    {
        /**
         * @private
         * @type {PIXI.Rectangle}
         */
        this._infoBarArea = new PIXI.Rectangle(0, 96, 100, 4);

        /**
         * The on-screen area that any Top Left Info Slot Component must fit within. This is
         * defined in terms of percentage of screen space.
         * @private
         * @type {PIXI.Rectangle}
         */
        this._topLeftInfoSlotArea = new PIXI.Rectangle(1, 1, 28, 4);

        /**
         * The on-screen area that the Game Logo must fit within. This is defined in terms of
         * percentage of screen space.
         * @private
         * @type {PIXI.Rectangle}
         */
        this._gameLogoArea = new PIXI.Rectangle(30, 1, 40, 8);

        /**
         * The on-screen area that any Top Right Info Slot Component must fit within. This is
         * defined in terms of percentage of screen space.
         * @private
         * @type {PIXI.Rectangle}
         */
        this._topRightInfoSlotArea = new PIXI.Rectangle(71, 1, 28, 4);

        /**
         * @private
         * @type {PIXI.Rectangle}
         */
        this._infoTextArea = new PIXI.Rectangle(8, 96.5, 84, 3);

        /**
         * @private
         * @type {PIXI.Rectangle[]}
         */
        this._leftFooterButtonSlots = 
        [
            new PIXI.Rectangle(0.5, 96.4, 2.5, 3.2),
            new PIXI.Rectangle(3.0, 96.4, 2.5, 3.2),
            new PIXI.Rectangle(5.5, 96.4, 2.5, 3.2)
        ];

        /**
         * @private
         * @type {PIXI.Rectangle[]}
         */
        this._rightFooterButtonSlots = 
        [
            new PIXI.Rectangle(97.0, 96.4, 2.5, 3.2), // start at right edge
            new PIXI.Rectangle(94.5, 96.4, 2.5, 3.2),
            new PIXI.Rectangle(92.0, 96.4, 2.5, 3.2)
        ];
    };


    /**
     * Creates and returns a display object, defining the available layout areas of the GUI, drawn
     * using semi-transparent boxes. This gives us a hint about where stuff can go on screen, because
     * it shows the available bounds for different components.
     * @private
     * @return {PIXI.DisplayObject}
     */
    getLayoutOverlay()
    {
        let rectsToDraw =
        [
            this._topLeftInfoSlotArea,
            this._gameLogoArea,
            this._topRightInfoSlotArea,
            this._infoTextArea,
            this._leftFooterButtonSlots[0],
            this._leftFooterButtonSlots[1],
            this._leftFooterButtonSlots[2],
            this._rightFooterButtonSlots[0],
            this._rightFooterButtonSlots[1],
            this._rightFooterButtonSlots[2]
        ];

        let overlay = new PIXI.Graphics();
        overlay.beginFill(0xffff00, 0.25); //0.5);
        overlay.lineStyle(4, 0xaa4400, 1, 0.5);

        rectsToDraw.forEach(rect => overlay.drawRect(
            this.layout.relX(rect.x),
            this.layout.relY(rect.y),
            this.layout.relX(rect.width),
            this.layout.relY(rect.height)
        ));

        overlay.endFill();
        
        return overlay;
    };
    

    /**
     * Creates the Autoplay Settings Button, adds it to the display list, and positions it.
     * @private
     */
    createAutoplayButton()
    {
        this.log.debug('UIDesktopMinimal.createAutoplayButton()');

        let autoplayButton = this.gameFactory.getAutoplayButtonDesktop();
        let posData = this._uiLayout.autoplayButtonPos;

        // We assume Autoplay Button will be top-left anchored
        this.layout.propHeightScale(autoplayButton, posData.height);
        autoplayButton.x = this.layout.relWidth(posData.x)  - (autoplayButton.width  * 0.5);
        autoplayButton.y = this.layout.relHeight(posData.y) - (autoplayButton.height * 0.5);
        autoplayButton.setTooltipOrientation(posData.tooltipOrientation, posData.tooltipPosition);
        autoplayButton.name = "GuiDesktop.AutoplayBtn";
        
        this.addChild(autoplayButton);

        /**
         * Button for changing autoplay / opening the autoplay menu.
         * @private
         * @type {PIXI.DisplayObject}
         */
        this._autoplayButton = autoplayButton;
    };


    /**
     * Creates the Spin Button, adds it to the display list, and positions it.
     * @private
     */
    createSpinButton()
    {
        this.log.debug('UIDesktopMinimal.createSpinButton()');

        let spinButton = this.gameFactory.getSpinButtonDesktop();
        let posData = this._uiLayout.spinButtonPos;

        // We assume Spin Button will be top-left anchored
        this.layout.propHeightScale(spinButton, posData.height);
        spinButton.x = this.layout.relWidth(posData.x)  - (spinButton.width  * 0.5);
        spinButton.y = this.layout.relHeight(posData.y) - (spinButton.height * 0.5);
        spinButton.setTooltipOrientation(posData.tooltipOrientation, posData.tooltipPosition);

        spinButton.name = "GuiDesktop.SpinBtn";

        this.addChild(spinButton);

        /**
         * Button for triggering a spin.
         * @private
         * @type {PIXI.DisplayObject}
         */
        this._spinButton = spinButton;
    };


    /**
     * Creates the Bet Settings Button, adds it to the display list, and positions it.
     * @private
     */
    createBetButton()
    {
        this.log.debug('UIDesktopMinimal.createBetButton()');

        let betButton = this.gameFactory.getBetButtonDesktop();
        let posData = this._uiLayout.betButtonPos;

        // We assume Bet Button will be top-left anchored
        this.layout.propHeightScale(betButton, posData.height);
        betButton.x = this.layout.relWidth(posData.x)  - (betButton.width  * 0.5);
        betButton.y = this.layout.relHeight(posData.y) - (betButton.height * 0.5);
        betButton.setTooltipOrientation(posData.tooltipOrientation, posData.tooltipPosition);

        betButton.name = "GuiDesktop.BetBtn";

        this.addChild(betButton);

        /**
         * Button for changing bet / opening the bet menu.
         * @private
         * @type {PIXI.DisplayObject}
         */
        this._betButton = betButton;
    };


    /**
     * Creates the backrgound component of the ui bar, and adds it to the display list.
     * @private
     */
    createInfoBarBackground()
    {
        let styleConfig = this.config.uiStyle.uiStatsBg;
        
        /**
         * The background behind the whole of the ui bar.
         * @private
         */
        this._infobarBg = new PIXI.Graphics();
        this._infobarBg.beginFill(styleConfig.bgColor);
        this._infobarBg.drawRect(
            this.layout.relWidth(this._infoBarArea.x),
            this.layout.relHeight(this._infoBarArea.y),
            this.layout.relWidth(this._infoBarArea.width),
            this.layout.relHeight(this._infoBarArea.height)
        );
        this._infobarBg.endFill();
        
        this.addChild(this._infobarBg);
    };


    // TODO: This will need a lot of tidying up, and some decisions being made...
    /**
     * Creates the text component of the info bar.
     * @private
     */
    createGameInfoTexts()
    {
        let model = this._model;

        let uiStyle = this.config.uiStyle;
        let useDualColours = "valueColor" in uiStyle.uiStats;

        let uiTextConfigs = [{
            type : "wallet",
            getTextValue : () => model.isSessionInProgress()? model.formatCurrency(model.getPlayerWallet()) : "",
            updateEvents : [
                C_GameEvent.SESSION_OPENED,
                C_GameEvent.SESSION_CLOSED,
                C_GameEvent.WALLET_CHANGED,
                C_GameEvent.GAME_FINISHED
            ]
        },
        {
            type : "totalBet",
            getTextValue : () => model.isSessionInProgress()? model.formatCurrency(model.getTotalBet()) : "",
            updateEvents:[
                C_GameEvent.SESSION_OPENED,
                C_GameEvent.SESSION_CHANGED,
                C_GameEvent.BET_SETTINGS_CHANGED
            ]
        },
        {
            type : "winnings",
            //getTextValue : () => model.isSessionInProgress()? model.getPlayerSuperbet().toString() : "",
            getTextValue : () => model.isSessionInProgress()? model.getPlayerWinnings().toString() : "",
            updateEvents:[
                C_GameEvent.SESSION_OPENED,
                C_GameEvent.SESSION_CHANGED,
                C_GameEvent.WINNINGS_CHANGED
            ]
        }];

        let textAreaStartX = this.layout.relWidth(this._infoTextArea.x);
        let textAreaWidth = this.layout.relWidth(this._infoTextArea.width);
        let textAreaHeight = Math.round(this.layout.relHeight(this._infoTextArea.height));
        let textPosY = this.layout.relHeight(this._infoTextArea.y + (this._infoTextArea.height * 0.5));
        let numTextItems = uiTextConfigs.length;
        let singleTextItemWidth = textAreaWidth / numTextItems;

        /**
         * @private
         */
        this._textFieldContainer = new PIXI.Container();
        this.addChild(this._textFieldContainer);

        if (useDualColours)
        {
            let titleTextStyle = 
            {
                fontFamily : uiStyle.uiFont,
                fill : uiStyle.uiStats.titleColor,
                fontSize : textAreaHeight
            };

            let valueTextStyle = 
            {
                fontFamily : uiStyle.uiFont,
                fill : uiStyle.uiStats.valueColor,
                fontSize : textAreaHeight
            };

            let textPadX = textAreaHeight * 0.25;

            uiTextConfigs.forEach((uiTextConfig, textIndex) =>
            {
                let textCenterPosX = textAreaStartX + (singleTextItemWidth * (textIndex + 0.5));
                let getTextValue = uiTextConfig.getTextValue;

                let titleField = new PIXI.Text(this.locale.getString(`T_infobar_${uiTextConfig.type}`), titleTextStyle);
                titleField.anchor.set(0,0.5);
                titleField.y = textPosY;

                let valueField = new PIXI.Text(getTextValue(), valueTextStyle);
                valueField.anchor.set(0,0.5);
                valueField.y = textPosY;

                let updateTextItem = () =>
                {
                    valueField.text = getTextValue();

                    let halfWidth = (titleField.width + valueField.width) / 2;
                    titleField.x = textCenterPosX - halfWidth - textPadX;
                    valueField.x = titleField.x + titleField.width + textPadX;
                }

                uiTextConfig.updateEvents.forEach(eventId => {
                    this.dispatcher.addListener(eventId, updateTextItem);
                });

                this._textFieldContainer.addChild(titleField);
                this._textFieldContainer.addChild(valueField);

                updateTextItem();
            });
        }
        else
        {
            /** @type {PIXI.TextStyleOptions} */
            let uiTextStyle =
            {
                fontFamily : uiStyle.uiFont,
                fill : uiStyle.uiStats.titleColor,
                fontSize : textAreaHeight
            };

            // TODO: Force always single field, or both colours possible ?
            // It might be neat to support both options.
            uiTextConfigs.forEach((uiTextConfig, textIndex) =>
            {
                let getTextValue = uiTextConfig.getTextValue;

                let getFullString = () => this.locale.getString(
                    `T_infobar_${uiTextConfig.type}_withValue`,
                    { "[VALUE]":getTextValue() });

                let textField = new PIXI.Text(getFullString(), uiTextStyle);
                textField.anchor.set(0.5, 0.5);
                textField.x = textAreaStartX + (singleTextItemWidth * (textIndex + 0.5));
                textField.y = textPosY;

                let updateTextItem = () => textField.text = getFullString();

                uiTextConfig.updateEvents.forEach(eventId => {
                    this.dispatcher.addListener(eventId, updateTextItem);
                });

                this._textFieldContainer.addChild(textField);
            });
        }
    };


    /**
     * @private
     */
    createMessageBar()
    {
        let messageBar = this.gameFactory.getMessageBar();
        messageBar.create();   // there is no obvious reason why this method exists.. Wiener couldn't be bothered to document it
        messageBar.name = "GuiDesktop.MessageBar";
        this.addChild(messageBar);
    };


    /**
     * Creates the Game Logo, which appears in the top center of the screen.
     * @private
     */
    createGameLogo()
    {
        let targetHeight = this.layout.relHeight(this.config.uiLayoutDesktop.gameLogoHeight);
        let targetCenterPosX = this.layout.relWidth(this.config.uiLayoutDesktop.gameLogoCenterPosX);
        let targetCenterPosY = this.layout.relHeight(this.config.uiLayoutDesktop.gameLogoCenterPosY);

        /**
         * Game logo, shown normally at top-center of the screen (but can be positioned dynamically)
         * @private
         */
        this._gameLogo = this._gameFactory.getGameLogo(targetCenterPosX, targetCenterPosY, targetHeight);

        this.addChild(this._gameLogo);
    };


    /**
     * Creates an Info Slot Component, using a given config and a target area.
     * @private
     * @param {DesktopGuiInfoSlotConfig} infoSlotConfig 
     * @param {*} infoSlotArea 
     */
    createInfoSlotComponent(infoSlotConfig, infoSlotArea)
    {
        if (infoSlotConfig.type === "responsibleGamingLinks")
        {
            this.log.debug('DesktopGui.createInfoSlotComponent: creating Responsible Gaming Links');
            this.createResponsibleGamingLinks(infoSlotConfig, infoSlotArea);
        }
        else
        if (infoSlotConfig.type === "sessionInfo")
        {
            this.log.debug('DesktopGui.createInfoSlotComponent: creating Session Info');
            this.createSessionInfoView(infoSlotConfig, infoSlotArea);
        }
        else
        {
            this.log.debug('DesktopGui.createInfoSlotComponent: nothing to create');
        }
    };


    // TODO: For future, non-italian use cases, this functionality will probably want
    // renaming or modifying, so it describes something a bit more generic.
    /**
     * Creates the Responsible Gaming links component.
     * @param {ResponsibleGamingLinksInfoSlotConfig} config
     * @param {PIXI.Rectangle} infoSlotArea
     * @private
     */
    createResponsibleGamingLinks(config, infoSlotArea)
    {
        let linksContainer = new PIXI.Container();
        let logoConfigs = this._businessConfig.responsibleGamingLinks;

        this.log.debug(`createResponsibleGamingLinks: ${JSON.stringify(logoConfigs)}`);

        for (let logoIdx = 0; logoIdx < logoConfigs.length; logoIdx++)
        {
            let logoConfig = logoConfigs[logoIdx];
            let logoTextureId = logoConfig.buttonTexture;
            let logoOpenLinkAction = logoConfig.action;
            let logo = this.assets.getSprite(logoTextureId);
            this.layout.propHeightScale(logo, 100, infoSlotArea);
            linksContainer.addChild(logo);

            if (logoIdx > 0) {
                logo.x = linksContainer.children[logoIdx - 1].x + linksContainer.children[logoIdx - 1].width + linksContainer.children[0].width * 0.1;
            }

            if (logoOpenLinkAction) {
                logo.interactive = true;
                logo.buttonMode = true;

                // Must be on UP for iOS to work for opening external links
                logo.on(C_PointerEvt.UP, () => {
                    this._openLinkUtil.openLink(logoOpenLinkAction);
                });
            }
        }
        
        this.layout.fitAndCenterDisplayObjectInProportionalArea(linksContainer, infoSlotArea);

        this.addChild(linksContainer);

        linksContainer.name = "GuiDesktop.ResponsibleGamingLinks";
    };


    /**
     * @private
     * @param {Button} button 
     */
    configureDefaultButtonSound(button)
    {
        if (this.config.uiSounds.desktop.guiButtonSound)
        {
            let buttonSoundId = this.config.uiSounds.desktop.guiButtonSound;
            let buttonVolume = this.config.uiSounds.desktop.guiButtonVolume;

            button.setClickSound(buttonSoundId, buttonVolume);
        }
        else button.setClickSound(null);
    };


    //--------------------------------------------------------------------------------------------------
    // 
    //--------------------------------------------------------------------------------------------------
    /**
     * Creates the Menu button in the top left of the screen.
     * @private
     * @param {PIXI.Rectangle} menuBtnArea
     * Area into which the button must be placed - the area will be defined as percentages of screen area.
     */
    createOpenMenuBtn(menuBtnArea)
    { 
        let tooltipText = this.locale.getString("T_tooltip_openMenu");
 
        let menuBtn = new Button('minGui_menuBtn', 'minGui_menuBtn_over', 'minGui_menuBtn_over', 'minGui_menuBtn_disabled');
        this.layout.fitAndCenterDisplayObjectInProportionalArea(menuBtn, menuBtnArea);
 
        menuBtn.name = "GuiDesktop.MenuBtn";
        menuBtn.on(C_PointerEvt.DOWN, this.onMenuBtnPressed, this);
        menuBtn.setToolTipProperties(tooltipText, C_TooltipOrientation.VERTICAL, C_TooltipPosition.ABOVE);

        this.addChild(menuBtn);
 
        this.keyboardManager.addKeyActionOnPressed(DefaultKeyCodes.TOGGLE_GAME_MENU, this.onMenuBtnPressed, this);

        this.configureDefaultButtonSound(menuBtn);
 
        this._guiButtons.push(menuBtn);
 
        /**
         * Button for opening the game menu. Not necessarily created - please check before accessing.
         * @private
         * @type {Button}
         */
        this._menuBtn = menuBtn;
    };
 
 
    /**
     * Action invoked when the Menu button is pressed.
     * @private
     */
    onMenuBtnPressed()
    {
        this.dispatcher.emit(C_GameEvent.BUTTON_TOGGLE_GAME_MENU_PRESSED);
    };
 
 
    /**
     * Creates the Close Button in the top right of the screen.
     * @private
     * @param {PIXI.Rectangle} closeBtnArea
     * Area into which the button must be placed - the area will be defined as percentages of screen area.
     */
    createCloseSessionBtn(closeBtnArea)
    {
        let tooltipText = this.locale.getString("T_tooltip_closeSession");
 
        let closeBtn = new Button('minGui_closeBtn', 'minGui_closeBtn_over', 'minGui_closeBtn_over', 'minGui_closeBtn_disabled');
        closeBtn.name = "GuiDesktop.CloseBtn";
        closeBtn.on(C_PointerEvt.DOWN, this.onCloseButtonPressed, this);
        closeBtn.setToolTipProperties(tooltipText, C_TooltipOrientation.VERTICAL, C_TooltipPosition.ABOVE);
 
        this.keyboardManager.addKeyActionOnPressed(DefaultKeyCodes.CLOSE_GAME_SESSION, this.onCloseButtonPressed, this);
        this.layout.fitAndCenterDisplayObjectInProportionalArea(closeBtn, closeBtnArea);
 
        this.configureDefaultButtonSound(closeBtn);
 
        this.addChild(closeBtn);
        this._guiButtons.push(closeBtn);
 
        /**
         * Button for closing the game client (or the current session). Not necessarily created - please check
         * before accessing.
         * @private
         * @type {Button}
         */
        this._closeBtn = closeBtn;
    };
 
 
    /**
     * Action invoked when the Close Session button is pressed, or the Keyboard shortcut is pressed.
     * @private
     */
    onCloseButtonPressed()
    {
        this.dispatcher.emit(C_GameEvent.BUTTON_CLOSE_SESSION_PRESSED);
    };


    /**
     * Creates the Full Screen button, which sits in the bottom right corner of the view.
     * @private
     * @param {PIXI.Rectangle} fullScreenBtnArea
     * Area into which the button must be placed - the area will be defined as percentages of screen area.
     */
    createFullScreenBtn(fullScreenBtnArea)
    {
        let tooltipText = this.locale.getString("T_tooltip_toggleFullScreen");

        let fullScreenBtn = new Button('minGui_fullScreenBtn', 'minGui_fullScreenBtn_over', 'minGui_fullScreenBtn_over', 'minGui_fullScreenBtn');
        fullScreenBtn.on(C_PointerEvt.DOWN, () => this.app.toggleFullScreen());
        fullScreenBtn.name = "GuiDesktop.FullScreenBtn";
        fullScreenBtn.setToolTipProperties(tooltipText, C_TooltipOrientation.VERTICAL, C_TooltipPosition.ABOVE);

        this.layout.fitAndCenterDisplayObjectInProportionalArea(fullScreenBtn, fullScreenBtnArea);
        
        // TODO: We need to listen out for f11 fullscreen toggles
        
        this.configureDefaultButtonSound(fullScreenBtn);

        this.addChild(fullScreenBtn);

        /**
         * Button for toggling the fullscreen status of the game. Not necessarily created - please
         * check before accessing it.
         * @private
         * @type {Button}
         */
        this._fullScreenBtn = fullScreenBtn;
    };


    /**
     * Creates the Add Credit button which sits in the bottom left corner of the view.
     * @private
     * @param {PIXI.Rectangle} addCreditBtnArea
     * Area into which the button must be placed - the area will be defined as percentages of screen area.
     */
    createAddCreditBtn(addCreditBtnArea)
    {
        let tooltipText = this.locale.getString("T_tooltip_addCredit");

        let addCreditBtn = new Button('minGui_addCreditBtn', 'minGui_addCreditBtn_over', 'minGui_addCreditBtn_over', 'minGui_addCreditBtn_disabled');
        addCreditBtn.name = "GuiDesktop.AddCreditBtn";
        addCreditBtn.on(C_PointerEvt.DOWN, this.onAddCreditPressed, this);
        addCreditBtn.setToolTipProperties(tooltipText, C_TooltipOrientation.VERTICAL, C_TooltipPosition.ABOVE);

        this.layout.fitAndCenterDisplayObjectInProportionalArea(addCreditBtn, addCreditBtnArea);

        this.keyboardManager.addKeyActionOnPressed(DefaultKeyCodes.ADD_CREDIT, this.onAddCreditPressed, this);

        this.configureDefaultButtonSound(addCreditBtn);

        this.addChild(addCreditBtn);

        /**
         * Button which will trigger an Add Credit action. Not always enabled, and no necessarily
         * created - please check before accessing it.
         * @private
         * @type {Button}
         */
        this._addCreditBtn = addCreditBtn;
    };


    /**
     * @private
     */
    onAddCreditPressed()
    {
        this.dispatcher.emit(C_GameEvent.BUTTON_ADD_CREDIT_PRESSED);
    };


    /**
     * Creates the toggle sound button.
     * @private
     * @param {PIXI.Rectangle} soundBtnArea
     * Area into which the button must be placed - the area will be defined as percentages of screen area.
     */
    createSoundBtn(soundBtnArea)
    {
        let tooltipText = this.app.locale.getString("T_tooltip_toggleSound");

        let soundBtn = new Button('minGui_soundOnBtn', 'minGui_soundOnBtn','minGui_soundOnBtn', 'minGui_soundOnBtn');
        soundBtn.name = "GuiDesktop.SoundBtn";
        soundBtn.on(C_PointerEvt.DOWN, this.onToggleSoundPressed, this);
        soundBtn.setToolTipProperties(tooltipText, C_TooltipOrientation.VERTICAL, C_TooltipPosition.ABOVE);

        this.layout.fitAndCenterDisplayObjectInProportionalArea(soundBtn, soundBtnArea);

        this.keyboardManager.addKeyActionOnPressed(DefaultKeyCodes.TOGGLE_SOUND, this.onToggleSoundPressed, this);

        this.addChild(soundBtn);

        this.dispatcher.on(C_GameEvent.SOUND_TOGGLED, this.setSoundBtnStatus, this);

        /**
         * Button that will toggle the status of Sound (on or off). Not necessarily created, so check
         * if its null before accessing it.
         * @private
         * @type {Button}
         */
        this._soundBtn = soundBtn;

        this.setSoundBtnStatus();
    };


    /**
     * @private
     */
    onToggleSoundPressed()
    {
        this.sound.toggleSound();
        this.setSoundBtnStatus();
        this.dispatcher.emit(C_GameEvent.BUTTON_TOGGLE_SOUND_PRESSED);
    };


    /**
     * Updates the visual state of the Toggle Sound button, to whatever is appropriate (considering
     * the current enabled state of sound overall)
     * @private
     */
    setSoundBtnStatus()
    {
        let skin = this.sound.getSoundEnabled() ? 'minGui_soundOnBtn' : 'minGui_soundOffBtn';
        this._soundBtn.setSkins(skin,skin,skin,skin);
    };


    /**
     * Creates the Session Info View component that sits in the top-right of the desktop viewport.
     * For Freeplay games, this simply shows a graphic indicating that you are playing in Freeplay
     * mode. For Italian games, it shows Aams Session & Ticket Ids. For other future countries, it
     * may be required to show different info.
     * 
     * In addition, some licensees now require us to open an external link when this area is pressed.
     * @protected
     * @param {SessionInfoSlotConfig} sessionInfoConfig
     * @param {PIXI.Rectangle} sessionInfoArea
     * Area into which the Session Info component must be placed (the area will be defined in percentages
     * of screen area)
     */
    createSessionInfoView(sessionInfoConfig, sessionInfoArea)
    {
        // TODO: Its an open question about how this component should work. Should it
        // a) resize itself (this would be better, because it updates itself)
        // b) be resized here.
        // If its A, the code needs moving there (ideally the "fitInBox" method would be a util,
        // on the layout class - note that the layout class really needs properly documenting, as
        // it seems poorly designed at the moment
        let sessionInfoView = new SessionInfoView(sessionInfoConfig, sessionInfoArea);
        sessionInfoView.name = "GuiDesktop.SessionInfoView";
        
        this.addChild(sessionInfoView);
    }


    /**
     * Enables or disables button input. If a value of true is passed, not all buttons may actually be enabled:
     * the ability of certain inputs to be enabled may depend on other factors (eg: the add credit button may
     * be disabled if rules regarding the maximum amount of credit that can be added are imposed). Passing a
     * value of true simply means "it is time for the UI to enable buttons".
     * @private
     * @param {boolean} enabled 
     */
    setInputEnabledTo(enabled)
    {
        if (this._addCreditBtn) {
            let addCreditButtonEnabled = this._model.canAddMoreCreditToSession() && enabled;
            this._addCreditBtn.setEnabled(addCreditButtonEnabled);
        }

        for (let btnIdx = 0; btnIdx < this._guiButtons.length; btnIdx++)
        {
            this._guiButtons[btnIdx].setEnabled(enabled);
        }
    }
}

export default UIDesktopMinimal;