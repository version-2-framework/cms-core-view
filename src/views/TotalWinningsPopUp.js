
import BaseView from "./BaseView";
import WinValue from "./WinValue";
import * as C_RewardGroup from "../../../cms-core-logic/src/const/C_RewardGroup";
import C_Snd from "../const/C_Snd";
import C_TotalWinViewEvent from "../const/C_TotalWinViewEvent";

// Proportional position and size values to use, when the animation is not
// being centered in the area of the reels.
const DEFAULT_TITLE_REL_HEIGHT = 10;
const DEFAULT_TITLE_REL_WIDTH = 60;
const DEFAULT_TITLE_REL_POS_Y = 35;

const DEFAULT_WIN_VAL_REL_WIDTH = 80;
const DEFAULT_WIN_VAL_REL_HEIGHT = 20;
const DEFAULT_WIN_VAL_REL_POS_Y = 55;

const DEFAULT_DURATION_SECONDS = 2.0;

/**
 * @type {SlotTotalWinAnimationConfig}
 */
const DEFAULT_TOTAL_WIN_CONFIG = 
{
    centerInReels : false,
    numberCharPrefix : 'wpanim_',
    useWinValue : true
};

/**
 * Default implementation of the Total Winnings Popup - used during slot phases
 * (Spin1 / Spin2 / Freespins)
 * @implements {TotalWinView}
 */
class TotalWinningsPopUp extends BaseView
{
    constructor()
    {
        super();

        /**
         * @type {SlotTotalWinAnimationConfig}
         */
        this._totalWinConfig = this.config.totalWinningsConfig ?
            this.config.totalWinningsConfig : DEFAULT_TOTAL_WIN_CONFIG;

        this.log.debug(`TotalWinningsPopup: config = ${JSON.stringify(this._totalWinConfig)}`);
    };


    /**
     * 
     * @public
     * @inheritDoc
     * @param {number} rewardGroup
     * @param {number} rewardValue
     */
    create(rewardGroup, rewardValue)
    {
        let duration = this._totalWinConfig.duration > 0? 
            this._totalWinConfig.duration : DEFAULT_DURATION_SECONDS;
        
        /**
         * The Reward Group for the win.
         * @private
         * @type {number}
         */
        this._rewardGroup = rewardGroup;

        /**
         * The reward value for the win.
         * @private
         * @type {number}
         */
        this._rewardValue = rewardValue;

        // Build a timeline for the animation
        this._timeline = new TimelineMax();
        this._timeline.addLabel("start");

        this.log.debug(`TotalWinningsPopup.create: rewardGroup:${this._rewardGroup},rewardValue:${this._rewardValue}`);

        let titleString = null;

        if (this._rewardGroup === C_RewardGroup.CREDIT) {
            titleString = this.locale.getString("T_Total_Winnings_PopUp");
        }
        else
        if (this._rewardGroup === C_RewardGroup.SUPERBET) {
            titleString = this.locale.getString("T_Total_Superbet_PopUp");
        }
        else
        if (this._rewardGroup === C_RewardGroup.FREESPIN) {
            titleString = this.locale.getString("T_Total_FreeSpins_PopUp")
        }

        // If centering within the reels, fetch the bounds of the reels.
        /** @type {Bounds} */
        let bounds;
        if (this._totalWinConfig.centerInReels) {
            bounds = this.reelPositionUtil.getBoundsOfReelsInScreenSpace();
        }
        else
        {
            bounds = 
            {
                x : 0,
                y : 0,
                width : this.layout.relWidth(100),
                height : this.layout.relHeight(100),
                centerX : this.layout.relWidth(50),
                centerY : this.layout.relHeight(50)
            }
        };

        const titleRelHeight = this._totalWinConfig.titleRelativeHeight > 0?
            this._totalWinConfig.titleRelativeHeight : DEFAULT_TITLE_REL_HEIGHT;

        const titleRelWidth = this._totalWinConfig.titleRelativeWidth > 0?
            this._totalWinConfig.titleRelativeWidth : DEFAULT_TITLE_REL_WIDTH;

        const titleRelPosY = this._totalWinConfig.titleRelativePosY > 0?
            this._totalWinConfig.titleRelativePosY : DEFAULT_TITLE_REL_POS_Y;

        const winValRelPosY = this._totalWinConfig.winValueRelativePosY > 0?
            this._totalWinConfig.winValueRelativePosY : DEFAULT_WIN_VAL_REL_POS_Y;

        const winValRelWidth = this._totalWinConfig.winValueRelativeWidth > 0?
            this._totalWinConfig.winValueRelativeWidth : DEFAULT_WIN_VAL_REL_WIDTH;

        const winValRelHeight = this._totalWinConfig.winValueRelativeHeight > 0?
            this._totalWinConfig.winValueRelativeHeight : DEFAULT_WIN_VAL_REL_HEIGHT;


        this.log.debug(`TotalWinningsPopup: bounds = ${JSON.stringify(bounds)}`);
        this.log.debug(`TotalWinningsPopup: titleRelHeight = ${titleRelHeight}`);
        this.log.debug(`TotalWinningsPopup: titleRelWidth = ${titleRelWidth}`);
        this.log.debug(`TotalWinningsPopup: titleRelPosY = ${titleRelPosY}`);
        this.log.debug(`TotalWinningsPopup: winValRelHeight = ${winValRelHeight}`);
        this.log.debug(`TotalWinningsPopup: winValRelWidth = ${winValRelWidth}`);
        this.log.debug(`TotalWinningsPopup: winValRelPosY = ${winValRelPosY}`);

        /** @type {SlotWinningsTextStyleConfig} */
        let textStyleConfig = this.config.totalWinningsTextStyle;
        
        let titleTextHeight = Math.round(bounds.height * titleRelHeight / 100);
        let titleTextPadding = textStyleConfig.useSizesRelativeToText?
            titleTextHeight * textStyleConfig.padding / 100 : this.layout.relHeight(textStyleConfig.padding);

        this.log.debug(`TotalWinningsPopup: titleTextHeight = ${titleTextHeight}`);

        
        let titleTextStyle = {
            fontFamily : this.config.gameSpecificFont,
            fontSize : titleTextHeight,
            fill : textStyleConfig.fill,
            fontWeight : textStyleConfig.fontWeight,
            padding : titleTextPadding
        };

        if (textStyleConfig.useStroke) {
            // This logic to determine strokeSize is convoluted, BUT - up to v0.7 of the core libs,
            // stroke size was determined proportionately from screen size. Right now, we are trying
            // to not break backwards compatibility on all games. So we have a flag to switch modes
            // (by default, its "relative to screen height")
            let strokeThickness;
            if (textStyleConfig.useSizesRelativeToText) {
                strokeThickness = Math.max(1, Math.round(textStyleConfig.strokeSize / 100 * titleTextHeight));
            }
            else
            {
                strokeThickness = this.layout.relHeight(textStyleConfig.strokeSize);
            }

            titleTextStyle.stroke = textStyleConfig.strokeColor;
            titleTextStyle.strokeThickness = strokeThickness;
            titleTextStyle.lineJoin = textStyleConfig.lineJoin;
            titleTextStyle.miterLimit = textStyleConfig.miterLimit;
        }

        if (textStyleConfig.useDropShadow) {
            // As for stroking, we also support the same 2 modes for dropShadowDistance / Blur
            let dropShadowDistance, dropShadowBlur;

            if (textStyleConfig.useSizesRelativeToText) {
                dropShadowDistance = Math.max(1, Math.round(textStyleConfig.dropShadowDistance / 100 * titleTextHeight));
                dropShadowBlur = Math.max(1, Math.round(textStyleConfig.dropShadowBlur / 100 * titleTextHeight));
            }
            else
            {
                dropShadowDistance = this.layout.relHeight(textStyleConfig.dropShadowDistance);
                dropShadowBlur = this.layout.relHeight(textStyleConfig.dropShadowBlur);
            }

            titleTextStyle.dropShadow = true;
            titleTextStyle.dropShadowColor = textStyleConfig.dropShadowColor;
            titleTextStyle.dropShadowAlpha = textStyleConfig.dropShadowAlpha;
            titleTextStyle.dropShadowDistance = dropShadowDistance;
            titleTextStyle.dropShadowBlur = dropShadowBlur;
        }

        this.log.debug(`TotalWinningsPopup: titleTextStyle = ${JSON.stringify(titleTextStyle)}`);
        
        let titleTF = new PIXI.Text(titleString, titleTextStyle);

        // Now, size and position the animation (either relative to reels,
        // or relative to screen space, according to the TotalWinAnimation
        // Config setting for "centerInReels")
        this.addChild(titleTF);
        titleTF.anchor.set(0.5);
        titleTF.x = bounds.centerX;
        titleTF.y = this.layout.relY(titleRelPosY, bounds);
        if (titleTF.width > this.layout.relWidth(titleRelWidth, bounds)) {
            this.layout.propWidthScale(titleTF, titleRelWidth, bounds);
        }
        
        // This is basically a legacy mode - Wiener originally used the WinValue in
        // this animation, which is understandable if it was important to reuse a
        // custom animation. However, in most cases, it is neither desirable, nor
        // flexible enough to adopt this approach, so there is a configurable choice
        // about using WinValue, or generating prize letters from scratch.
        if (this._totalWinConfig.useWinValue) 
        {
            let winVal = this.gameFactory.getWinValue(rewardValue, duration);
            this.addChild(winVal);
            winVal.name = 'winVal';
            winVal.create();
            this.layout.propHeightScale(winVal, winValRelHeight, bounds);
            if (winVal.width > this.layout.relWidth(winValRelWidth, bounds)) {
                this.layout.propWidthScale(winVal, winValRelWidth, bounds);
            }

            this.layout.horizontalAlignCenter(winVal, 0, bounds);
            winVal.y = this.layout.relHeight(winValRelPosY - (winValRelHeight * 0.5), bounds);
            winVal.playAnimation();
        }
        else
        {
            // Manually create prize chars, and animate them.
            let prizeCharContainer = new PIXI.Container();
            let prizeCharPrefix = this._totalWinConfig.numberCharPrefix ?
                this._totalWinConfig.numberCharPrefix : "wpanim_";

            /** @type {PIXI.Sprite[]} */
            let prizeChars = [];

            let currPosX = 0;

            rewardValue.toString().split("").forEach(winValueChar => {
                let prizeCharAssetId = `${prizeCharPrefix}${winValueChar}`;
                let prizeCharSprite = this.assets.getSprite(prizeCharAssetId);
                prizeCharSprite.anchor.set(0.5, 0.5);
                prizeCharSprite.x = currPosX + (prizeCharSprite.width * 0.5);

                currPosX += prizeCharSprite.width;
                
                prizeCharContainer.addChild(prizeCharSprite);
                prizeChars.push(prizeCharSprite);
            });

            // Move everything to the left by a half of the total width of all prize chars
            // This ensures that we are centered around (0,0) in prizeCharContainer
            prizeChars.forEach(prizeChar => {
                prizeChar.x -= (currPosX * 0.5);
            });

            // Scale and position the prizeChar container
            this.layout.propHeightScale(prizeCharContainer, winValRelHeight, bounds);
            if (prizeCharContainer.width > this.layout.relWidth(winValRelWidth, bounds)) {
                this.layout.propWidthScale(prizeCharContainer, winValRelWidth, bounds);
            }
            prizeCharContainer.x = bounds.centerX;
            prizeCharContainer.y = bounds.y + (bounds.height * winValRelPosY / 100);

            let numCharsTotal = prizeChars.length;
            let totalCharsAppearDuration = duration * 0.4;
            let singleCharAppearDuration = 2 * totalCharsAppearDuration / (numCharsTotal + 1);
            let singleCharAppearDelay = totalCharsAppearDuration / (numCharsTotal + 1);

            let prizeCharTimeline = new TimelineMax();
            
            prizeChars.forEach((prizeChar, prizeCharIndex) => {
                let finalWidth = prizeChar.width;
                let finalHeight = prizeChar.height;
                let startWidth = finalWidth * 1.4;
                let startHeight = finalHeight * 1.4;

                prizeCharTimeline.fromTo(
                    prizeChar,
                    singleCharAppearDuration,
                    { width:startWidth, height:startHeight, alpha:0 },
                    { width:finalWidth, height:finalHeight, alpha:1 },
                    singleCharAppearDelay * prizeCharIndex);
            });

            this._timeline.add(prizeCharTimeline, "start");

            this.addChild(prizeCharContainer);
        }

        // TODO: This is the legacy sound - played at end of "coins fade in" timeline.
        // I need to be sure exactly when it falls, so the config documentation (which
        // describes the legacy behaviour) can work a little better.
        if (this._totalWinConfig.soundId) {
            let soundId = this._totalWinConfig.soundId;
            let soundVolume = this._totalWinConfig.soundVolume >= 0? this._totalWinConfig.soundVolume : 1.0;
            this._timeline.call(() => this.sound.playSound(soundId, false, soundVolume));
        }

        this._timeline.addLabel("fadeOut", `start+=${duration-0.25}`);

        this._timeline.fromTo(this, 0.1, { alpha:0 }, { alpha:1 }, "start");
        this._timeline.to(this, 0.25, { alpha:0 }, "fadeOut");
        this._timeline.call(() => this.endAnimation());

        if (this._totalWinConfig.totalWinStartSound) {
            let soundConfig = this._totalWinConfig.totalWinStartSound;
            let soundId = soundConfig.soundId;
            let soundVolume = soundConfig.soundVolume;
            this._timeline.call(() => {
                this.sound.playSound(soundId, false, soundVolume);
            }, null, null, "start");
        }

        if (this._totalWinConfig.totalWinEndSound) {
            let soundConfig = this._totalWinConfig.totalWinEndSound;
            let soundId = soundConfig.soundId;
            let soundVolume = soundConfig.soundVolume;
            this._timeline.call(() => {
                this.sound.playSound(soundId, false, soundVolume);
            }, null, null, "fadeOut");
        }
    };


    /**
     * @private
     */
    endAnimation()
    {
        this.log.debug('TotalWinningsPopup.endAnimation()');

        if (this._timeline) {
            this._timeline.kill();
            this._timeline = null;
        }

        this.emit(C_TotalWinViewEvent.COMPLETE);

        this.removeFromParent();
    };
}

export default TotalWinningsPopUp