import BaseView from "./BaseView";
import {HistoryTab} from "../ui/menu/HistoryTab";
import {SettingsTab} from "../ui/menu/SettingsTab";
import {RulesTab} from "../ui/menu/RulesTab";
import ScrollContainer from "../ui/ScrollContainer";
import C_PointerEvt from "../const/C_PointerEvt";
import C_GameEvent from "../../../cms-core-logic/src/const/C_GameEvent";
import C_Snd from "../const/C_Snd";
import * as C_ClientEnvironmentValue from "../../../cms-core-logic/src/const/C_ClientEnvironmentValues"; 

const RULES_TAB = {
    id : "rules",
    headerTextId : "T_menu_RulesTitle",
    selectedIconId : "MenuIcon_Rules_Selected",
    normalIconId : "MenuIcon_Rules_Normal",
    graphic : RulesTab
}

const HISTORY_TAB = {
    id : "history",
    headerTextId : "T_menu_HistoryTitle",
    selectedIconId : "MenuIcon_History_Selected",
    normalIconId : "MenuIcon_History_Normal",
    graphic : HistoryTab
}

const SETTINGS_TAB = {
    id : "settings",
    headerTextId : "T_menu_SettingsTitle",
    selectedIconId : "MenuIcon_Settings_Selected",
    normalIconId : "MenuIcon_Settings_Normal",
    graphic : SettingsTab
}

/**
 * @typedef MenuTabConfig
 * @property {string} id
 * @property {string} headerTextId
 * @property {string} selectedIconId
 * @property {string} normalIconId
 * @property {(width:number, height:number) => void} graphic
 */

/**
 * An overlay view which contains several sub-menus
 * Rules
 * Paytable
 * Music/sound settings
 */
class MenuView extends BaseView
{
    constructor()
    {
        super();

        let desktopGuiMenuTabConfig = this._businessConfig.desktopGui.menuViewTabsConfig;

        let mobileGuiMenuTabConfig = this._businessConfig.mobileGui.menuViewTabsConfig;


        /**
         * Caches the value of the client environment variable which 
         * is set via the url parameters
         * @private
         * @type {number}
         */
        this._clientEnvironmentValue = this.model.getClientEnvironmentValue();


        /**
         * @type {MenuViewTabConfig}
         */
        this._menuConfig = this._platform.isDesktop() ? desktopGuiMenuTabConfig : mobileGuiMenuTabConfig;


        /**
         * The config for the currently open tab.
         * @private
         * @type {MenuTabConfig}
         */
        this._currentTabConfig = null;

        /**
         * A list of all tab configs that are configured for this game.
         * @private
         * @type {MenuTabConfig[]}
         */
        this._allTabConfigs = this.getTabConfigs();

        //--------------------------------------------------------------------------------------------------
        // Decide some static layout
        //--------------------------------------------------------------------------------------------------
        /**
         * Amount of padding to use between items.
         * @private
         * @type {number}
         */
        this._padding = this.layout.relHeight(1);

        /**
         * Starting x position of the header background area.
         * @private
         * @type {number}
         */
        this._headerPosX = this.layout.relX(0);

        /**
         * Starting y position of the header background area.
         * @private
         * @type {number}
         */
        this._headerPosY = this.layout.relY(0);

        /**
         * Defines the width of the header area.
         * @private
         * @type {number}
         */
        this._headerWidth = this.layout.relWidth(100);

        /**
         * Defines the height of the header area.
         * @private
         * @type {number}
         */
        this._headerHeight = this.layout.relHeight(10);	// or 5 ?

        /**
         * Max allowed width of the text (tab title) in the header.
         * @private
         * @type {number}
         */
        this._headerTextMaxWidth = this._headerWidth * 0.6;

        /**
         * Defines the height of the footer area (where tab buttons are placed)
         * @private
         * @type {number}
         */
        this._footerHeight = this.layout.relHeight(10);

        /**
         * Defines the width of the footer area (where tab buttons are placed).
         * @type {number}
         */
        this._footerWidth = this.layout.relWidth(100);

        /**
         * Start x (left-most) position of the footer area.
         * @private
         * @type {number}
         */
        this._footerPosX = this.layout.relX(0);

        /**
         * Start y (top-most) position of the footer area.
         * @private
         * @type {number}
         */
        this._footerPosY = this.layout.relHeight(100) - this._footerHeight;

        /**
         * Additional vertical padding: scroller height could just be the vertical gap between header
         * and footer, but instead we reduce it by 2 * scrollerPadY (and add this padding to the top
         * as well).
         * @private
         * @type {number}
         */
        this._scrollerPadY = this.layout.relHeight(2);

        /**
         * Defines the height of the content area (in which the active tab content is shown).
         * @private
         * @type {number}
         */
        this._scrollerHeight = this.layout.relHeight(100) - this._headerHeight - this._footerHeight - (2 * this._scrollerPadY);

        /**
         * Defines the width of the content area (in which the active tab content is shown). We aim
         * for a maximum width to height ratio here (if we are ever drawing the menu onto super wide
         * devices, we do not want the content area to be super wide, because the actual content could
         * be hard to read in this scenario).
         * @private
         * @type {number}
         */
        this._scrollerWidth = Math.min(this.layout.relWidth(90), this._scrollerHeight * 2);

        /**
         * Start (left-most) x position of the content area.
         * @private
         * @type {number}
         */
        this._scrollerStartX = (this.layout.relWidth(100) - this._scrollerWidth) * 0.5;

        /**
         * Start (top-most) y position of the content area.
         * @private
         * @type {number}
         */
        this._scrollerStartY = this._headerHeight + this._scrollerPadY;

        /**
         * The start x position for tab buttons (on mobile, we reserve some space for the
         * "home" button)
         * @private
         * @type {number}
         */
        this.tabButtonsStartX = this._platform.isMobileOrTablet()? this.layout.relWidth(10) : 0;

        this.createGraphics();

        this.dispatcher.addListener(C_GameEvent.HIDE_MENU_VIEW, this.closeMenu, this);
    };


    /**
     * Creates all graphical content for the Menu.
     * @private
     */
    createGraphics()
    {
        let isMobileOrTablet = this._platform.isMobileOrTablet();

        //--------------------------------------------------------------------------------------------------
        // Create all of the graphical content
        //--------------------------------------------------------------------------------------------------
        /**
         * The header text, shown in the header area. Describes the current tab that is open.
         * @private
         * @type {PIXI.Text}
         */
        this._headerText = this.assets.getTF('', Math.round(this._headerHeight * 0.7), "Arial", this.config.gameMenuStyle.headerTextColour);
        this._headerText.anchor.set(0.5);
        this._headerText.x = this._headerPosX + (this._headerWidth * 0.5);
        this._headerText.y = this._headerPosY + (this._headerHeight * 0.5);

        let colourBg = this.config.gameMenuStyle.bgColour;
        let colourHeader = this.config.gameMenuStyle.headerFooterColour;

        /**
         * The background to the whole of the menu view. This is set to input enabled, so
         * that it can absorb any misplaced screen touches (stop anything below getting
         * triggered).
         * @private
         */
        this._bgMain = new PIXI.Graphics();
        this._bgMain.beginFill(colourBg);
        this._bgMain.drawRect(0, 0, this.layout.relWidth(100), this.layout.relHeight(100));
        this._bgMain.endFill();
        /*
        this.bgMain = this.assets.getSprite("menu_main_bg");
        this.bgMain.x = this.layout.relX(0);
        this.bgMain.y = this.layout.relY(0);
        this.bgMain.width = this.layout.relWidth(100);
        this.bgMain.height = this.layout.relHeight(100);
        */
        this._bgMain.interactive = true;

        

        /**
         * The background to the header area of the menu.
         * @private
         */
        this._bgHeader = new PIXI.Graphics();
        this._bgHeader.beginFill(colourHeader);
        this._bgHeader.drawRect(this._headerPosX, this._headerPosY, this.layout.relWidth(100), this._headerHeight);
        this._bgHeader.endFill();
        /*
        this.bgHeader = this.assets.getSprite("menu_footer_bg");
        this.bgHeader.x = this.headerPosX;
        this.bgHeader.y = this.headerPosY;
        this.bgHeader.width = this.layout.relWidth(100);
        this.bgHeader.height = this.headerHeight;
        */
        
        /**
         * The background to the footer (tab bar) component of the menu view.
         * @private
         */
        this._bgFooter = new PIXI.Graphics();
        this._bgFooter.beginFill(colourHeader);
        this._bgFooter.drawRect(this.layout.relX(0), this.layout.relHeight(100) - this._footerHeight, this.layout.relWidth(100), this._footerHeight);
        this._bgFooter.endFill();
        /*
        this.bgFooter = this.assets.getSprite("menu_footer_bg");
        this.bgFooter.x = this.layout.relX(0);
        this.bgFooter.y = this.layout.relHeight(100) - this.footerHeight;
        this.bgFooter.width = this.layout.relWidth(100);
        this.bgFooter.height = this.footerHeight;
        */

        /**
         * The return button dispatches the "close menu" event. The GameController should
         * catch this event, and request that the menu is closed. On desktop, we place this
         * button in the top left of the screen (which is where the menu button is in the
         * main GUI). On mobile, we place it in the bottom left, in the tabs area (because
         * on mobile, we place the Menu button at the bottom left of the main GUI): on iOS
         * we basically can no longer use the top 10% of the screen, due to the fact that
         * touching here will cause full-screen mode to exit.
         * @private
         */
        this._buttonReturn = this.assets.getSprite("MenuIcon_Return");
        this._buttonReturn.anchor.set(0.5);
        this.layout.setHeight(this._buttonReturn, 0.9 * (isMobileOrTablet? this._footerHeight : this._headerHeight));
        this._buttonReturn.x = isMobileOrTablet? this._footerPosX + (this._footerHeight * 0.5) : this._headerPosX + (this._headerHeight * 0.5);
        this._buttonReturn.y = isMobileOrTablet? this._footerPosY + (this._footerHeight * 0.5) : this._headerPosY + (this._headerHeight * 0.5);
        this._buttonReturn.interactive = true;
        this._buttonReturn.on(C_PointerEvt.DOWN, () => {
            this.sound.playSound(C_Snd.BTN_DOWN);
            this.requestMenuClose();
        });

        /**
         * All tab buttons are added to this group.
         * @private
         * @type {PIXI.Container}
         */
        this._buttonsGroup = new PIXI.Container();

        //--------------------------------------------------------------------------------------------------
        // Create all of the bottom tab buttons.
        //--------------------------------------------------------------------------------------------------
        this.buttons = {};

        let numTabs = this._allTabConfigs.length;
        let numTabsIsEven = numTabs % 2 === 1;
        let widthAvailableForTabs = this._footerWidth - this.tabButtonsStartX;
        let maxWidthPerTab = widthAvailableForTabs / numTabs;
        let maxBtnH = this._footerHeight * 0.9;

        this._allTabConfigs.forEach((tabConfig, index) =>
        {
            let buttonIcon = this.assets.getSprite(tabConfig.normalIconId);
            buttonIcon.name = `TabIcon.${tabConfig.id}`;
            buttonIcon.anchor.set(0.5);
            buttonIcon.x = this.tabButtonsStartX + (maxWidthPerTab * (index + 0.5));
            buttonIcon.y = this._footerPosY + (this._footerHeight * 0.5);
            buttonIcon.interactive = true;
            buttonIcon.on(C_PointerEvt.DOWN, () => {
                this.sound.playSound(C_Snd.BTN_DOWN);
                this.setCurrentTab(tabConfig);
            });

            this.buttons[tabConfig.id] = { button:buttonIcon, config:tabConfig };

            this.layout.propHeightScale(buttonIcon, 90, this._bgFooter);

            this._buttonsGroup.addChild(buttonIcon);
        });

        //--------------------------------------------------------------------------------------------------
        // Content group contains the scrollable area..
        //--------------------------------------------------------------------------------------------------
        /**
         * Group which contains the scroller (used to move the scrollable content of the menu)
         * @private
         * @type {PIXI.Container}
         */
        this._scrollGroup = new PIXI.Container();

        /**
         * The view associated with the currently open tab.
         * @private
         */
        this._currentTabView = null;

        //--------------------------------------------------------------------------------------------------
        // Order the display list
        //--------------------------------------------------------------------------------------------------
        this.addChild(this._bgMain);
        this.addChild(this._scrollGroup);
        this.addChild(this._bgHeader);
        this.addChild(this._bgFooter);
        this.addChild(this._buttonsGroup);
        this.addChild(this._buttonReturn);
        this.addChild(this._headerText);

        //--------------------------------------------------------------------------------------------------
        // Set some initial state
        //--------------------------------------------------------------------------------------------------
        this._currentTabConfig = this._allTabConfigs[0];
        this.setCurrentTab(this._allTabConfigs[0]);
    }


    /**
     * Updates the currently active tab, based on a config object which specifies the new tab
     * to show.
     * @private
     * @param {MenuTabConfig} tabConfig
     * Configuration for the new tab to load.
     */
    setCurrentTab(tabConfig)
    {
        this.log.debug(`[MenuView].setCurrentTab(${tabConfig.id})`);

        this._scrollGroup.removeChildren();

        let activeTab = this.buttons[tabConfig.id];

        this._currentTabConfig = tabConfig;

        if (this._currentTabView) {
            this._currentTabView.destroy();
            this._currentTabView = null;
        }

        // highlight the selected tab, and set
        // all others to the unhilighted state
        for (let key in this.buttons)
        {
            this.buttons[key].button.texture = this.assets.getTexture(this.buttons[key].config.normalIconId);
        }

        activeTab.button.texture = this.assets.getTexture(tabConfig.selectedIconId);

        this._headerText.text = this.locale.getString(tabConfig.headerTextId);
        this._currentTabView = new tabConfig.graphic(this._scrollerWidth, this._scrollerHeight);

        // TODO: There is no need to re-create scroll container each time
        let scrollContainer = new ScrollContainer({
            posX : this._scrollerStartX,
            posY : this._scrollerStartY,
            width : this._scrollerWidth,
            height : this._scrollerHeight,
            scrollbarBgColour : this.config.gameMenuStyle.scrollbarBgColour,
            scrollbarBtnColour : this.config.gameMenuStyle.scrollbarBtnColour,
            useMask : true,
            fitContent : true
        }, this._currentTabView);

        this._scrollGroup.addChild(scrollContainer);
    };

    /**
     * Gets all the default tab configs required for this game.
     * @protected
     * @return {MenuTabConfig[]}
     */
    getDefaultTabConfigs()
    {
        let configs =
        [
            RULES_TAB,
            HISTORY_TAB,
            SETTINGS_TAB
        ];

        return configs;
    };


    /**
     * Gets the tab configs based on settings in the business config for the licensee
     * @protected
     * @return {MenuTabConfig[]}
     */
     getCustomTabConfigs()
     {
        let configs = [];


        if (this._menuConfig.rulesTab.isEnabled)
        {
            configs.push(RULES_TAB);
        }

        if (this._menuConfig.historyTab.isEnabled)
        {
            configs.push(HISTORY_TAB);
        }

        if (this._menuConfig.settingsTab.isEnabled)
        {
            configs.push(SETTINGS_TAB);
        }

        if (configs.length > 0)
        {
            return configs;
        } else {
            return this.getDefaultTabConfigs();
        }
        


     }

      /**
     * Gets the tab configs for when the client environment parameter has been set to mobile app
     * .
     * @protected
     * @return {MenuTabConfig[]}
     */
    getMobileAppTabConfigs()
    {
        let configs =
        [
            RULES_TAB,
            SETTINGS_TAB
        ];

        return configs;
    }

    /**
     * Returns the tab configs required for the game
     * @returns {MenuTabConfig[]}
     */
    getTabConfigs() 
    {

        if (this.clientEnvironmentIsMobileApp())
        {
            return this.getMobileAppTabConfigs();
        }
        else if (this._menuConfig)
        {
            return this.getCustomTabConfigs();
        }
        else
        {
            return this.getDefaultTabConfigs();
        }
    }


    /**
     * Closes the Menu View, and removes it from the display list.
     * @private
     */
    closeMenu()
    {
        this.dispatcher.removeListener(C_GameEvent.HIDE_MENU_VIEW, this.closeMenu, this);
        this.removeFromParent();
    };


    /**
     * Request (to Game Controller) to close the Game Menu. Game Controller will likely execute
     * a state change (out of Menu state) if it approves this request (there are potentially contexts
     * in which Game Controller will ignore this request).
     * @private
     */
    requestMenuClose()
    {
        this.dispatcher.emit(C_GameEvent.BUTTON_CLOSE_GAME_MENU_PRESSED);
    };

    /**
     * Indicates if the client environment variable has been set to mobile app and that the client is running on 
     * a mobile device.
     * @private
     * @return {boolean}
     */
    clientEnvironmentIsMobileApp()
    {
        return this._platform.isMobileOrTablet() && this._clientEnvironmentValue === C_ClientEnvironmentValue.MOBILE_APP;
    }

}

export default MenuView