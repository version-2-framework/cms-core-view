interface WinningsTextControllerConfig
{
    initialValue : number;

    /**
     * Speed that the counter will be updated at when using animation: uses a value of "1" to
     * indicate "full speed" (this would be 60 updates per second, assuming that the game is
     * running at this speed). A value of 0.5, indicates "update at half full speed". Sometimes
     * you want to slow the updates down a bit: in particular, this saves texture updates when
     * the textField being used is a canvas / texture based one. If omitted, then the controller
     * will use the default value of 1 (eg: max number of updates per second). The change in
     * value shown on each step will be scaled (eg: if the change would normally be by a value
     * of 2 when the update is running at max speed, then at half speed, the change delta per
     * update will be 4)
     */
    updateSpeed ? : number;

    /**
     * An optional method, which returns the correct text string to show on the field, based
     * on the current winnings value to be shown. If not provided, then the field will simply
     * show the winnings value directly. If provided, then this method can be used to customize
     * the text field in any way that you like.
     * @param amountToShow
     * The amount currently being shown on the field
     * @return
     * The string to display.
     */
    getTextString ? : WinningsTextControllerValueStringProvider;

    /**
     * Configures how the value is incremented per visual update: if ommitted, a default
     * routine is used.
     */
    incrementConfig ? : WinningsTextControllerIncrementConfig;

    /**
     * An optional callback fired, whenever "start counting up" is triggered.
     * @param startValueShown
     */
    onCountUpStart ? : (startValueShown : number) => void;

    /**
     * An optional callback fired, whenever the count up finishes (eg: the target value is reached).
     * This callback will also be triggered, if the count up finishes due to a call to "kill" the
     * count up (eg: we have reached some arbitrar time, we tell the controller to finish imediately,
     * even if the target value is not yet reached: in this condition, the countUpFinish method is
     * still invoked).
     * @param finalValueShown
     */
    onCountUpFinish ? : (finalValueShown : number) => void;

    /**
     * An optional callback fired whenever the text gets updated: allows you to set scaling, adjust
     * positioning, etc.
     * @param currValueShown
     * The new value being shown on the text field.
     */
    onUpdate ? : (currValueShown : number) => void;
}

/**
 * A method which converts a winnings value, into a string to show on the field.
 */
type WinningsTextControllerValueStringProvider = (currentValue:number) => string;

/**
 * Defines a routine for calculating the increment per visual update
 * @param totalWinningsDelta
 * Total amount by which winnings must increase across the whole animation
 * @param numUpdatesPerSecond
 * @returns
 * The increment to increase by on each visual update.
 */
type WinningsTextControllerIncrementCalculator = (totalWinningsDelta:number, numUpdatesPerSecond:number) => number;

/**
 * Configuration for increment by which we update.
 */
type WinningsTextControllerIncrementConfig =
    WinningsTextControllerSimpleIncrementConfig |
    WinningsTextControllerFixedSpeedIncrementConfig |
    WinningsTextControllerAutoIncrementConfig |
    WinningsTextControllerCustomIncrementConfig;

/**
 * Simple config, provides a fixed step on each visual update. Winnings Text Controller lets
 * you set an update rate (for example: 10 updates per second, 30 updates per second). In this
 * mode, rate of updates doesn't matter: if you set an increment of 10, you will ALWAYS see
 * the value increas by 10 (unless we are on the final update, and a value less than 10 needs
 * to be added to reach the target value).
 */
interface WinningsTextControllerSimpleIncrementConfig
{
    type : "simple";

    /**
     * The fixed value of increment to update by on each step.
     */
    increment : number;
}

/**
 * Specifies a fixed amount by which winnings should increase per second. In this mode, rate
 * of visual updates are taken into account, and the delta is calculated dynamically. If you
 * have 60 updates per second, and an incrementPerSecond value of 6000, every update will have
 * an increment of 100. Whereas, if you set an update rate of 30 updates per second, then every
 * update will have an increment of 200.
 */
interface WinningsTextControllerFixedSpeedIncrementConfig
{
    type : "fixedSpeed";

    /**
     * The total amount (on average) that the winnings text should increase by in a single second.
     */
    incrementPerSecond : number;
}

/**
 * Specifies that the increment on each update should be proportional to the size of the value
 * we are increasing by (eg: the number of characters in the winnings increase): the more characters,
 * the more we increase by per second. For example:
 * - if winnings increase <    10, then increase by  1 per update
 * - if winnings increase <    20, then increase by  2 per update
 * - if winnings increase <  1000, then increase by  5 per update
 * - if winnings increase < 10000, then increase by 50 per update
 * (this is the general idea)
 * 
 * The increment in this mode, is fixed for each visual update (rate is not taken into account).
 */
interface WinningsTextControllerAutoIncrementConfig
{
    type : "auto";
}

// TODO
interface WinningsTextControllerCustomIncrementConfig
{
    type : "custom";

    /**
     * The custom method by which a delta should be calculated.
     */
    method : WinningsTextControllerIncrementCalculator;
}

/**
 * Target of a winnings text controller.
 */
type WinningsTextControllerTarget = { text:string } | { (text:string):void };