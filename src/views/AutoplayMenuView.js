import BaseView from "./BaseView";
import C_PointerEvt from "../const/C_PointerEvt";
import Button from "../ui/Button";
import C_GameEvent from "../../../cms-core-logic/src/const/C_GameEvent";
import ScrollingSelector from "../ui/ScrollingSelector";
import {Events as ScrollingSelectorEvents} from "../ui/ScrollingSelector";
import * as C_ClientConfigValue from "../../../cms-core-logic/src/const/C_ClientConfigValues";
import * as C_ClientEnvironmentValue from "../../../cms-core-logic/src/const/C_ClientEnvironmentValues"; 


const TEXT_DROPSHADOW_COLOUR = 0x000000;
const TEXT_DROPSHADOW_ALPHA = 0.75;
const TEXT_DROPSHADOW_ANGLE = Math.PI/180 * 60;
const TEXT_FILL_WHITE = [ 0xffffff, 0xdddddd ];
const TEXT_FILL_BLUE = [ 0x66b8fe, 0x008afe ];

const DEFAULT_PRODUCTION_AUTOPLAY_PRESETS = [0,5,10,50,100,500];
const DEFAULT_TESTING_AUTOPLAY_PRESETS = [0,5,10,50,100,500,5000];

/*
* @class AutoPlayMenuView.
* Allows the user to setup the settings for autoplay.
* */

class AutoplayMenuView extends BaseView
{
    constructor()
    {
        super();

        this.log.debug(`AutoplayMenu.constructor()`);

        let desktopGuiAutoplayMenuConfig = this._businessConfig.desktopGui.autoPlayMenuConfig;

        let mobileGuiAutoplayMenuConfig = this._businessConfig.mobileGui.autoPlayMenuConfig;

        /**
         * @type {AutoplayMenuConfig}
         */
        this._menuConfig = this._platform.isDesktop() ? desktopGuiAutoplayMenuConfig : mobileGuiAutoplayMenuConfig;

        /**
         * @private
         * @type {number[]}
         */
        this._autoPlayOptions = this.getAutoplayOptions();

        /**
         * Caches the value of the client environment variable which 
         * is set via the url parameters
         * @private
         * @type {number}
         */
        this._clientEnvironmentValue = this.model.getClientEnvironmentValue();

        // Clear any cached values for autoplayLossLimit / autoplayWinningsLimit
        this._model.setAutoplayMaxLosses(Number.POSITIVE_INFINITY);
        this._model.setAutoplayMaxWinnings(Number.POSITIVE_INFINITY);
        
        this._bg = this.assets.getSprite('popup_bg');
        this.layout.setToRectangle(this._bg, 0, 0, 100, 100);
        this._bg.alpha = 0.95;
        this._bg.interactive = true;
        this.addChild(this._bg);

        let balanceTextString = this.locale.getString(
            'T_autoplayMenu_currentBalance',
            {
                "[BALANCE]" : this.model.formatCurrency(this.model.getPlayerWallet())
            });

        let betTextString = this.locale.getString(
            'T_autoplayMenu_currentBet',
            {
                "[TOTAL_BET]" : this.model.formatCurrency(this.model.getTotalBet())
            });

        /** @type {PIXI.TextStyleOptions} */
        let upperIndicatorsTextStyle =
        {
            fontFamily: "Arial",
            fontSize : Math.round(this.layout.relHeight(4)),
            fill: TEXT_FILL_WHITE,
            dropShadow: true,
            dropShadowColor: TEXT_DROPSHADOW_COLOUR,
            dropShadowAlpha: TEXT_DROPSHADOW_ALPHA,
            dropShadowAngle: TEXT_DROPSHADOW_ANGLE,
            dropShadowDistance: Math.round(this.layout.relHeight(0.4)),
        };

        let balanceTextField = new PIXI.Text(balanceTextString, upperIndicatorsTextStyle);
        this.layout.horizontalAlignLeft(balanceTextField, 1.5);
        this.layout.verticalAlignTop(balanceTextField, 2);
        this.addChild(balanceTextField);

        let betTextField = new PIXI.Text(betTextString, upperIndicatorsTextStyle);
        this.layout.horizontalAlignRight(betTextField, 1.5);
        this.layout.verticalAlignTop(betTextField, 2);
        this.addChild(betTextField);

        /**
         * @private
         * @type {PIXI.Text}
         */
        this._headerText = new PIXI.Text(
            this.locale.getString("T_autoplayMenu_title"),
            {
                fontFamily : "Arial",
                fontSize : Math.round(this.layout.relHeight(8)),
                fill : TEXT_FILL_BLUE,
                dropShadow : true,
                dropShadowColor : TEXT_DROPSHADOW_COLOUR,
                dropShadowAlpha : TEXT_DROPSHADOW_ALPHA,
                dropShadowAngle: TEXT_DROPSHADOW_ANGLE,
                dropShadowDistance : Math.round(this.layout.relHeight(0.8))
            });

        this.layout.horizontalAlignCenter(this._headerText, 0, this._bg);
        this.layout.verticalAlignTop(this._headerText, 3, this._bg);

        this.addChild(this._headerText);

        


        /** 
         * @private
         * @type {PIXI.TextStyleOptions} */
        this._titleTextStyle  =
        {
            fontFamily: "Arial",
            fontSize: Math.round(this.layout.relHeight(5)),
            fill: TEXT_FILL_WHITE,
            dropShadow: true,
            dropShadowAlpha: TEXT_DROPSHADOW_ALPHA,
            dropShadowAngle: TEXT_DROPSHADOW_ANGLE,
            dropShadowColor: TEXT_DROPSHADOW_COLOUR,
            dropShadowDistance: Math.round(this.layout.relHeight(0.5))
        };


        this.createBtns();
        this.createTextInputs();
        this.createNumberSlider();

        this._dispatcher.on(C_GameEvent.HIDE_AUTOPLAY_MENU, this.closeAutoplayMenu, this);
    };


    /**
     * 
     */
    createNumberSlider() {

        const MinNumEnabledTextBoxes = 1;

        let numTextBoxesEnabled = (()=>{
            let textBoxCount = 0;

            if (this.lossLimitTextBoxesEnabled()) textBoxCount += 1;

            if (this.winLimitTextBoxEnabled()) textBoxCount += 1;

            return textBoxCount;

        })();

        let selectorDimensions = { width:this.layout.relWidth(40), height:this.layout.relHeight(10), useExactWidth:false };

        let numAutoScroller = new ScrollingSelector(selectorDimensions, this._autoPlayOptions, 0);
        this.layout.horizontalAlignCenter(numAutoScroller, 0, this._bg);

        if (numTextBoxesEnabled === 0)
        {
            this.layout.verticalAlignMiddle(numAutoScroller, 0, this._bg);
        }
        else if (numTextBoxesEnabled === MinNumEnabledTextBoxes)
        {
            numAutoScroller.y = this.layout.relHeight(30, this._bg);
        }
        else
        {
            numAutoScroller.y = this.layout.relHeight(19.5, this._bg);
        }   

        

        numAutoScroller.on(ScrollingSelectorEvents.SELECTED_VALUE_CHANGED, ()=>
        {
            let selectedIndex = numAutoScroller.getSelectedValueIndex();
            let numAutoPlays = this._autoPlayOptions[selectedIndex];
            this.model.setAutoplayNumGamesTotal(numAutoPlays);

            if (this._autoLossLimitTI)
            {
                this._autoLossLimitTI.value = this.model.getAutoplayAutoLossLimit();
            }
            
        });

        let btnDecrementStakePerHand = this.assets.getSprite("selectorArrow");
        let btnIncrementStakePerHand = this.assets.getSprite("selectorArrow");
        btnIncrementStakePerHand.scale.x *= -1;
        this.layout.setWidth(btnDecrementStakePerHand, this.layout.relWidth(3));
        this.layout.setWidth(btnIncrementStakePerHand, this.layout.relWidth(3));
        btnDecrementStakePerHand.x = numAutoScroller.x - btnDecrementStakePerHand.width - this.layout.relWidth(2);
        btnIncrementStakePerHand.x = numAutoScroller.x + numAutoScroller.getVisibleWidth() + this.layout.relWidth(2) + btnIncrementStakePerHand.width;
        this.layout.verticalAlignMiddle(btnDecrementStakePerHand, 0, numAutoScroller);
        this.layout.verticalAlignMiddle(btnIncrementStakePerHand, 0, numAutoScroller);
        btnDecrementStakePerHand.interactive = true;
        btnDecrementStakePerHand.on(C_PointerEvt.DOWN, ()=>numAutoScroller.selectPrevOptionValue());

        btnIncrementStakePerHand.interactive = true;
        btnIncrementStakePerHand.on(C_PointerEvt.DOWN, ()=>numAutoScroller.selectNextOptionValue());

        this.addChild(numAutoScroller);
        this.addChild(btnDecrementStakePerHand);
        this.addChild(btnIncrementStakePerHand);

    }


    /**
     * @private
     */
    createBtns()
    {
        let cancelButton = new Button("selectorBtn", "selectorBtn_over");
        this.layout.propHeightScale(cancelButton, 9, this);
        this.layout.horizontalAlignCenter(cancelButton, -25, this);
        this.layout.verticalAlignBottom(cancelButton, 8, this);

        cancelButton.once(C_PointerEvt.DOWN, () => {
            this.model.setAutoplayNumGamesTotal(0);
            this.dispatcher.emit(C_GameEvent.BUTTON_CLOSE_AUTOPLAY_MENU_PRESSED);
            this.closeAutoplayMenu();
        }, this);

        let cancelTextHeight = cancelButton.height * 0.8;
        let cancelText = this.assets.getTF(this.locale.getString('T_button_cancel'), cancelTextHeight, 'Arial', "#fff");
        this.layout.limitSize(cancelText, 80, 70, cancelButton);
        this.layout.horizontalAlignCenter(cancelText, 0, cancelButton);
        this.layout.verticalAlignMiddle(cancelText, 0, cancelButton);
        this.addChild(cancelButton);
        this.addChild(cancelText);

        let confirmButton = new Button("selectorBtn", "selectorBtn_over");
        this.layout.propHeightScale(confirmButton, 9, this);
        this.layout.horizontalAlignCenter(confirmButton, 25);
        this.layout.verticalAlignBottom(confirmButton, 8);

        confirmButton.once(C_PointerEvt.DOWN, () => {
            this.dispatcher.emit(C_GameEvent.BUTTON_CLOSE_AUTOPLAY_MENU_PRESSED);
            this.closeAutoplayMenu();
        });

        let confirmTextHeight = confirmButton.height * 0.8;
        let confirmText = this.assets.getTF(this.locale.getString('T_button_confirm'), confirmTextHeight, 'Arial', "#fff");
        this.layout.limitSize(confirmText, 80, 70, confirmButton);
        this.layout.horizontalAlignCenter(confirmText, 0, confirmButton);
        this.layout.verticalAlignMiddle(confirmText, 0, confirmButton);
        this.addChild(confirmButton);
        this.addChild(confirmText);
    };


    /**
     * Creates all text input components.
     * @private
     */
    createTextInputs()
    {
        
        if (this.lossLimitTextBoxesEnabled()) {
            this.createAutoLossLimitTextBox();
            this.createCustomLossLimitTextBox();
        }
       
        
        if (this.winLimitTextBoxEnabled())
        {
            this.createWinLimitTextBox();
        }
        
    };




    /**
     * @private
     */
    getTextInputBg()
    {
        let container = new PIXI.Container();
        let left = this.assets.getSprite('limitField_edge');
        left.scale.x *= -1;
        left.anchor.set(1, 0);
        let right = this.assets.getSprite('limitField_edge');
        let mid = this.assets.getSprite('limitField_middle');

        container.addChild(left);
        container.addChild(right);
        container.addChild(mid);

        mid.x = left.x + left.width;
        right.x = mid.x + mid.width;

        return container;
    };


    /**
     * @private
     * @param {*} e 
     */
    checkKeycode(e)
    {
        if (e.keyCode === 13)
        {
            this._customLossLimitTI.blur();
            this._winLimitTI.blur();
        }
    };


    /**
     * @private
     */
    setCustomLossLimit()
    {
        if (isNaN(this._customLossLimitTI.value))
        {
            this._customLossLimitTI.value = this.model.getAutoplayMaxLosses();
        }
        else
        {
            this._customLossLimitTI.value = Number(this._customLossLimitTI.value);
            this.model.setAutoplayMaxLosses(this._customLossLimitTI.value);
        }
    };


    /**
     * @private
     */
    setCustomWinLimit()
    {
        if (isNaN(this._winLimitTI.value))
        {
            this._winLimitTI.value = this.model.getAutoplayMaxWinnings();
        }
        else
        {
            this._winLimitTI.value = Number(this._winLimitTI.value);
            this.model.setAutoplayMaxWinnings(this._winLimitTI.value);
        }
    };


    /**
     * Closes the Autoplay menu.
     * @private
     */
    closeAutoplayMenu()
    {
        this.log.info('AutoplayMenu.closeAutoplayMenu()');

        this.removeFromParent();
    };


    /**
     * @private
     * @return {number[]}
     */
    getAutoplayOptions()
    {
        return this._config.autoplayPresets ||
            (this.buildInfo.getClientConfig() === C_ClientConfigValue.PRODUCTION?
                DEFAULT_PRODUCTION_AUTOPLAY_PRESETS :
                DEFAULT_TESTING_AUTOPLAY_PRESETS);
    }

    

    /**
     * @private 
     */
    createAutoLossLimitTextBox()
    {

        let tiCoords = this.winLimitTextBoxEnabled() ? {x:10,y:45,width:30,height:10} : {x:10,y:55,width:30,height:10};

        let autoLossLimitArea = new PIXI.Rectangle();
        autoLossLimitArea.x = tiCoords.x;
        autoLossLimitArea.y = tiCoords.y;
        autoLossLimitArea.width = tiCoords.width;
        autoLossLimitArea.height = tiCoords.height;

        let autoLossLimitBg = this.getTextInputBg();
        autoLossLimitBg.height = this.layout.relHeight(autoLossLimitArea.height) * 1.2;
        autoLossLimitBg.width = this.layout.relWidth(autoLossLimitArea.width) * 1.2;
        autoLossLimitBg.x = (this.layout.relX(autoLossLimitArea.x)) + (this.layout.relWidth(autoLossLimitArea.width) - autoLossLimitBg.width) * .5;
        autoLossLimitBg.y = (this.layout.relY(autoLossLimitArea.y)) + (this.layout.relHeight(autoLossLimitArea.height) - autoLossLimitBg.height) * .5;
        this.addChild(autoLossLimitBg);

        /**
         * Text input, which allows the player to set a custom loss limit.
         * @private
         */
        this._autoLossLimitTI = this.assets.createTextInput(autoLossLimitArea, 9);
        this._autoLossLimitTI.value = this.model.getAutoplayAutoLossLimit();
        this._autoLossLimitTI.disabled = true;
        this._autoLossLimitTI.style.background = 'transparent';
        this._autoLossLimitTI.style.border = 'none';
        this._autoLossLimitTI.style.color = '#cfcfcf';


        let autoLossLimitString = this.locale.getString('T_autoplayMenu_limitYourLossesTo')
        let autoLossLimitLabel = new PIXI.Text(autoLossLimitString, this._titleTextStyle);
        autoLossLimitLabel.x = this.layout.relX(autoLossLimitArea.x) + (this.layout.relWidth(autoLossLimitArea.width) * 0.5) - (autoLossLimitLabel.width * 0.5);
        autoLossLimitLabel.y = this.layout.relY(autoLossLimitArea.y) - (autoLossLimitLabel.height * 1.5);
        this.addChild(autoLossLimitLabel);
    }

    /**
     * @private
     */
    createCustomLossLimitTextBox() 
    {
        let tiCoords = this.winLimitTextBoxEnabled() ? {x:60,y:45,width:30,height:10} : {x:60,y:55,width:30,height:10};

        let customLossLimitArea = new PIXI.Rectangle();
        customLossLimitArea.x = tiCoords.x;
        customLossLimitArea.y = tiCoords.y;
        customLossLimitArea.width = tiCoords.width;
        customLossLimitArea.height = tiCoords.height;

        /**
         * Text Input for the Custom Loss limit value.
         * @private
         */
        this._customLossLimitTI = this.assets.createTextInput(customLossLimitArea, 9, { minValue:0, maxValue:1000000 });
        this._customLossLimitTI.value = this.model.getAutoplayMaxLosses();
        this._customLossLimitTI.lastValue = this._customLossLimitTI.value;
        this._customLossLimitTI.onchange = this.setCustomLossLimit.bind(this);
        this._customLossLimitTI.onfocus = this._customLossLimitTI.select;
        this._customLossLimitTI.onkeyup = this.checkKeycode.bind(this);
        this._customLossLimitTI.style.background = 'transparent';
        this._customLossLimitTI.style.border = 'none';
        this._customLossLimitTI.style.color = '#cfcfcf';
        this._customLossLimitTI.min = 0;

        

        let customLossLimitBg = this.getTextInputBg();
        customLossLimitBg.height = this.layout.relHeight(customLossLimitArea.height) * 1.1;
        customLossLimitBg.width  = this.layout.relWidth(customLossLimitArea.width) * 1.1;
        customLossLimitBg.x = (this.layout.relX(customLossLimitArea.x)) + (this.layout.relWidth(customLossLimitArea.width) - customLossLimitBg.width) * .5;
        customLossLimitBg.y = (this.layout.relY(customLossLimitArea.y)) + (this.layout.relHeight(customLossLimitArea.height) - customLossLimitBg.height) * .5;
        this.addChild(customLossLimitBg);

        let customLossLimitString = this.locale.getString('T_autoplayMenu_customLossLimit');
        let customLossLimitLabel = new PIXI.Text(customLossLimitString, this._titleTextStyle);
        customLossLimitLabel.x = this.layout.relX(customLossLimitArea.x) + (this.layout.relWidth(customLossLimitArea.width) * 0.5) - (customLossLimitLabel.width * 0.5);
        customLossLimitLabel.y = this.layout.relY(customLossLimitArea.y) - (customLossLimitLabel.height * 1.5);
        this.addChild(customLossLimitLabel);
    }

    /**
     * 
     */
    createWinLimitTextBox() 
    {
        let tiCoords = this.lossLimitTextBoxesEnabled() ? {x:30,y:70,width:40,height:10} : {x:30,y:55,width:40,height:10};
        

        let winLimitTiArea = new PIXI.Rectangle();
        winLimitTiArea.x = tiCoords.x;
        winLimitTiArea.y = tiCoords.y;
        winLimitTiArea.width = tiCoords.width;
        winLimitTiArea.height = tiCoords.height;

        /**
         * Text input for the Winnings Limit field.
         * @private
         */
        this._winLimitTI = this.assets.createTextInput(winLimitTiArea, 9, { minValue:0, maxValue:1000000 });
        this._winLimitTI.value = this.model.getAutoplayMaxWinnings();
        this._winLimitTI.lastValue = this._winLimitTI.value;
        this._winLimitTI.onchange = this.setCustomWinLimit.bind(this);
        this._winLimitTI.onfocus = this._winLimitTI.select;
        this._winLimitTI.onkeyup = this.checkKeycode.bind(this);
        this._winLimitTI.style.background = 'transparent';
        this._winLimitTI.style.border = 'none';
        this._winLimitTI.style.color = '#cfcfcf';
        this._winLimitTI.min = 0;

        let winLimitBg = this.getTextInputBg();
        winLimitBg.height = this.layout.relHeight(winLimitTiArea.height) * 1.1;
        winLimitBg.width = this.layout.relWidth(winLimitTiArea.width) * 1.1;
        winLimitBg.x = (this.layout.relX(winLimitTiArea.x)) + (this.layout.relWidth(winLimitTiArea.width) - winLimitBg.width) * .5;
        winLimitBg.y = (this.layout.relY(winLimitTiArea.y)) + (this.layout.relHeight(winLimitTiArea.height) - winLimitBg.height) * .5;
        this.addChild(winLimitBg);

        let winLimitString = this.locale.getString('T_autoplayMenu_limitYourWinningsTo')
        let winLimitLabel = new PIXI.Text(winLimitString, this._titleTextStyle);
        winLimitLabel.x = this.layout.relX(winLimitTiArea.x) + (this.layout.relWidth(winLimitTiArea.width) * 0.5) - (winLimitLabel.width * 0.5);
        winLimitLabel.y = this.layout.relY(winLimitTiArea.y) - (winLimitLabel.height * 1.5);
        this.addChild(winLimitLabel);
    }

    /**
     * Checks to see if the auto loss limit text box is enabled by default this method will 
     * return a value of true
     * @return {boolean}
     */
    lossLimitTextBoxesEnabled() {


        if (this.clientEnvironmentIsMobileApp())
        {
            return false;
        }
        else if (this._menuConfig)
        {
            return this._menuConfig.lossLimitTextBoxes.isEnabled;
        } else {
            return true;
        } 
        
    }
    

    /**
     * Checks to see if the win limit text box is enabled by default this method
     * will return a value of true
     * @private
     * @return {boolean}
     */
    winLimitTextBoxEnabled() {

        if (this.clientEnvironmentIsMobileApp())
        {
            return false;
        }
        else if (this._menuConfig)
        {
            return this._menuConfig.winningsLimitTextBox.isEnabled;
        } else {
            return true;
        }

    }

    /**
     * Indicates if the client environment variable has been set to mobile app and that the client is running on 
     * a mobile device.
     * @private
     * @return {boolean}
     */
    clientEnvironmentIsMobileApp()
    {
        return this._platform.isMobileOrTablet() && this._clientEnvironmentValue === C_ClientEnvironmentValue.MOBILE_APP;
    }


    /**
     * @override
     * @inheritDoc
     */
    destroy()
    {
        
        if (this._autoLossLimitTI)
        {
            document.body.removeChild(this._autoLossLimitTI);
            this._autoLossLimitTI.destroy();
        }

        if (this._customLossLimitTI)
        {
            document.body.removeChild(this._customLossLimitTI);
            this._customLossLimitTI.destroy();
        }
        
        if (this._winLimitTI)
        {
            document.body.removeChild(this._winLimitTI);
            this._winLimitTI.destroy();
        }
        

        this._dispatcher.off(C_GameEvent.HIDE_AUTOPLAY_MENU, this.closeAutoplayMenu, this);
        
        super.destroy();
    }

}
export default AutoplayMenuView