import BaseView from "./BaseView";
import C_ViewEvent from "../const/C_ViewEvent";
import C_GameEvent from "../../../cms-core-logic/src/const/C_GameEvent";
import { getGameViewLogger } from "../../../cms-core-logic/src/logging/LogManager";
import Layout from "../utils/Layout";
import { ReelPositionUtil } from "./ReelPositionUtil";
import Assets from "../utils/Assets";
import { SpinModelUtils } from "../../../cms-core-logic/src/model/SpinModelUtils";

// TODO: THe default implementation needs tidying up, and probably needs sticky symbols
// support adding in.
/**
 * Win Presentation Overlay is a dedicated view component, which sits above the ReelGroup.
 * It is used to show customized sequences for the game that pertain to the symbols : Reel
 * Group will report certain hook events, and the Win Presentation Overlay can show a variable
 * length sequence (if required for the game client), before reporting back to Reel Group that
 * the sequence is completed.
 * 
 * The 2 views are closely coupled, with the coupling handled via messages sent between the 2
 * views. However, neither need to know about the instance or api of the other. By default,
 * Win Presentation Overlay doesn't really need to do anything, and a game still will work
 * (the win presentations may be very simple and standard, however).
 * 
 * The approach of using a separate class was chosen, because Win Presentation Overlay may
 * need to do a lot of graphical work, and extending from standard Reel Group did not seem
 * the correct approach to handle it (or make it simple).
 */
class BaseWinPresentationView
{
    /**
     * @public
     * @param {PIXI.Container} foreground 
     * A container, placed above the reels (and any reels frame), into which we may draw custom
     * graphics for our win presentation hooks.
     * @param {PIXI.Container} background
     * A container, placed below the reels (but above any background view), into which we may draw
     * custom graphics for our win presentation hooks.
     */
    constructor(foreground, background)
    {
        let app = PIXI.app;

        /**
         * @protected
         * @type {GameFactoryApi}
         */
        this._gameFactory = app.gameFactory;

        /**
         * @protected
         * @type {Model}
         */
        this._model = app.model;

        /**
         * @type {SpinPhaseModel}
         * @protected
         */
        this._spinModel = app.spinModel;

        /**
         * @private
         * @protected
         * @final
         * @type {PIXI.Container}
         */
        this._foreground = foreground;

        /**
         * @protected
         * @final
         * @type {PIXI.Container}
         */
        this._background = background;
        
        /**
         * @type {SpinModelUtils}
         */
        this._spinUtils = app.spinModelUtils;

        /**
         * @protected
         * @type {EventEmitter}
         */
        this._emitter = app.dispatcher;

        /**
         * @protected
         */
        this._log = getGameViewLogger();

        /**
         * @protected
         * @type {Layout}
         */
        this._layout = app.layout;
        
        /**
         * @protected
         * @type {ReelPositionUtil}
         */
        this._reelPositionUtil = app.reelPositionUtil;

        /**
         * @protected
         * @type {Assets}
         */
        this._assets = app.assets;

        // Set up event listeners
        // This first bunch are the "hook" listeners - WinPresentationView receives (from ReelGroup) a request
        // to show a custom sequence, and it reports back when its done (if no sequence is shown, Win Presentation
        // Overlay can report back instantly that the sequence is completed)
        this._emitter.on(C_ViewEvent.SHOW_PRE_SPIN_ROUND_PRESENTATION, this.showPreSpinRoundPresentation, this);
        this._emitter.on(C_ViewEvent.SHOW_PRE_SPIN_START_PRESENTATION, this.showPreSpinStartPresentation, this);
        this._emitter.on(C_ViewEvent.SHOW_PRE_SPIN_STOP_PRESENTATION, this.showPreSpinStopPresentation, this);
        this._emitter.on(C_ViewEvent.SHOW_POST_SPIN_STOP_PRESENTATION, this.showPostSpinStopPresentation, this);
        this._emitter.on(C_ViewEvent.SHOW_POST_SPIN_WINS_PRESENTATION, this.showPostSpinWinsPresentation, this);
        this._emitter.on(C_ViewEvent.SHOW_POST_SPIN_RESULT_PRESENTATION, this.showPostSpinResultPresentation, this);
        this._emitter.on(C_ViewEvent.SHOW_POST_SPIN_ROUND_PRESENTATION, this.showPostSpinRoundPresentation, this);

        // Then, there are simple "notification" events sent by ReelGroup - we do not have to respond to these
        // (as they are not hooks for an asynchronous, variable sequence), but we can bind to them if we need to
        // update anything on these events.
        this._emitter.on(C_ViewEvent.SHOW_REELS_IDLE, this.showIdle, this);
        this._emitter.on(C_ViewEvent.SINGLE_REEL_SPIN_STARTED, this.onSingleReelSpinStarted, this);
        this._emitter.on(C_ViewEvent.SINGLE_REEL_SPIN_SETTLING, this.onSingleReelSpinSettling, this);
        this._emitter.on(C_ViewEvent.SINGLE_REEL_SPIN_STOP_COMPLETE, this.onSingleReelSpinStopComplete, this);
        this._emitter.on(C_ViewEvent.SPIN_ROUND_PRESENTATION_STARTED, this.onSpinRoundPresentationStarted, this);
        this._emitter.on(C_ViewEvent.SPIN_RESULT_PRESENTATION_STARTED, this.onSpinResultPresentationStarted, this);
        this._emitter.on(C_ViewEvent.SPIN_RESULT_PRESENTATION_COMPLETE, this.onSpinResultPresentationComplete, this);
        this._emitter.on(C_ViewEvent.SPIN_ROUND_PRESENTATION_COMPLETE, this.onSpinRoundPresentationComplete, this);
    };


    /**
     * Returns the foreground layer: a Container which is placed above the reels (but below any GUI).
     * You can draw arbitrary graphics into this. If you need to draw something above the GUI, you
     * are probably better off adding a custom container to the stage directly.
     * @protected
     * @final
     * @return {PIXI.Container}
     */
    get foreground() {
        return this._foreground;
    };


    /**
     * Returns the background layer: a Container which is placed below the reels (but above the main
     * game background). You can draw arbitrary graphics into this.
     * @protected
     * @final
     * @return {PIXI.Container}
     */
    get background() {
        return this._background;
    };


    //--------------------------------------------------------------------------------------------------
    // The following are the standard time-variant hook sequences. We don't know if our custom Win
    // Presentation Overlay for a game client wants to show these seuqences, or how long they will
    // take. Default behaviour, is that ReelGroup will request the sequence to be shown (that's the
    // "hook"), and the WinPresentationView will inform ReelGroup via an event that the sequence
    // has been shown (its fine to do this immediately, without showing any sequence, if none is
    // required).
    //--------------------------------------------------------------------------------------------------
    // TODO: Pre spin phase hook as well ?

    /**
     * A hook for showing a custom Pre Spin Round Presentation. A Spin Round may consist of 1 or more
     * Spin Results.
     * 
     * All Spin-like phases have at least 1 Spin Round.
     * - Single Spin, Spin1 and Spin2 phases all consist of exactly 1 Spin Round. So, for thses phases,
     *   the Pre Spin Round presentation will be triggered exactly once: it happens before any real
     *   action is taken on-screen for the spin phase.
     * - FreeSpin phase constists of 1 or more Spin Rounds (1 FreeSpin round is also referred to as one
     *   FreeSpin). So, for a FreeSpin phase, the Pre-Spin Round presentation is triggered the same
     *   number of times as there are FreeSpins (once before each FreeSpin starts).
     * 
     * Override this method to implement a custom Pre Spin Round Presentation. Once your custom presentation
     * completed, call "notifyPreSpinRoundPresentationComplete".
     * 
     * EXAMPLE USE CASE:
     * For Free Spin Rounds, we want to make a character perform some animation before each Free Spin Round
     * begins: we can trigger this here.
     * @protected
     * @see notifyPreSpinRoundPresentationComplete
     * @param {SpinRoundResultPresentation} spinRoundPresentation
     * Presentation model for the Spin Round that is about to be shown.
     * @param {SpinPhaseType} spinPhase
     * The spin phase type currently being shown
     * @param {SpinPhasePresentationStateModel} spinPresentationState
     * Data describing current transitory "state" of the spin presentation.
     */
    showPreSpinRoundPresentation(spinRoundPresentation, spinPhase, spinPresentationState) {
        this._log.debug('WinPresentationView.showPreSpinRoundPresentation: nothing to show by default');
        this.notifyPreSpinRoundPresentationComplete();
    };


    /**
     * Dispatches an event notification that the Pre Spin Round Presentation has completed. This method
     * must not be overridden.
     * @protected
     * @final
     */
    notifyPreSpinRoundPresentationComplete() {
        this._log.debug('WinPresentationView.notifyPreSpinRoundPresentationComplete()');
        this._emitter.emit(C_ViewEvent.PRE_SPIN_ROUND_PRESENTATION_COMPLETE);
    };


    /**
     * A hook for showing a custom Pre Spin Start Presentation. This presentation is shown before any new
     * Spin Result presentation starts. All Spin Rounds consist of at least 1 Spin Result: for a single
     * Spin Round with N spin results, this presentation will be triggered N times. For the very first
     * Spin Result of a Spin Round, this presentation is triggered after the Pre Spin Round Presentation.
     * 
     * Override this method in order to trigger a custom Pre Spin Start presentation. Once you custom
     * presentation has completed, call "notifyPreSpinStartPresentationComplete".
     * 
     * EXAMPLE USE CASE:
     * The pre-spin start presentation is where we would choose to show Sticky Symbols (for the next spin)
     * if we were making a game like Jack Hammer (ie: we pre-highlight the symbols before the actual spin
     * commences).
     * @protected
     * @see notifyPreSpinStartPresentationComplete
     * @param {SpinResultPresentation} spinPresentation
     * Presentation model for the Spin Result that is about to be shown.
     * @param {SpinPhaseType} spinPhase
     * The spin phase type currently being shown
     * @param {SpinPhasePresentationStateModel} spinPresentationState
     * Data describing current transitory "state" of the spin presentation.
     */
    showPreSpinStartPresentation(spinPresentation, spinPhase, spinPresentationState) {
        this._log.debug('WinPresentationView.showPreSpinStartPresentation: nothing to show (by default)');

        this.notifyPreSpinStartPresentationComplete();
    };


    /**
     * Dispatches an event notification that the Pre Spin Start Presentation has completed. This method
     * must not be overridden.
     * @protected
     * @final
     */
    notifyPreSpinStartPresentationComplete() {
        this._log.debug('WinPresentationView.notifyPreSpinStartPresentationComplete()');
        this._emitter.emit(C_ViewEvent.PRE_SPIN_START_PRESENTATION_COMPLETE);
    };


    /**
     * A hook for showing a "pre-spin-stop" presentation. This presentation is shown before a set of
     * spinning reels can be brought to a halt: only once the presentation has completed, can the
     * reels be stopped on their final symbols.
     * 
     * For a Spin Round with N spin results, this presentation will be triggered N times: once for each
     * set of spinning reels. It will only be triggered once all reels are fully spinning, and we know
     * the outcome of the Spin Result.
     * 
     * Override this method to trigger a custom presentation: once your custom presentaiton has completed,
     * call "notifyPreSpinStopPresentationComplete" in order to trigger the next step (which is the reels
     * stopping).
     * 
     * EXAMPLE USE CASES:
     * 1) We want to place some symbols in an overlay (above the reels), and for the player to see these
     *    symbols BEFORE the reels stop spinning: for example, some sticky wilds are awarded, and we want
     *    to visually place these above each reel (perhaps using an animated sequence), and only once all
     *    new sticky wilds are placed, will we allow the reels to come to a halt.
     * 2) We want to show some Special Wins before the reels stop spinning. (There is now rule about when
     *    special wins must be shown - it will tend to be game client specific, how we want to visualize
     *    them). For example, we may get a multiplier from special win, and we want to visualize this
     *    whilst the reels are spinning.
     * @protected
     * @see notifyPreSpinStopPresentationComplete
     * @param {SpinResultPresentation} spinPresentation
     * Presentation model for the Spin PResult that is in progress (and for which the reels are about to
     * come to a halt). This provides information on symbols that will be shown when the reels stop spinning,
     * regular symbol wins and special wins.
     * @param {SpinPhaseType} spinPhase
     * The spin phase type currently being shown
     * @param {SpinPhasePresentationStateModel} spinPresentationState
     * Data describing current transitory "state" of the spin presentation.
     */
    showPreSpinStopPresentation(spinPresentation, spinPhase, spinPresentationState) {
        this._log.debug('WinPresentationView.showPreSpinStopPresentation: nothing to show (by default)');

        this.notifyPreSpinStopPresentationComplete(spinPresentation);
    };


    /**
     * Dispatches an event notification that the Pre Spin Stop presentation has completed. This method must
     * not be overridden.
     * @protected
     * @final
     * @param spinPresentation
     */
    notifyPreSpinStopPresentationComplete(spinPresentation) {
        this._log.debug('WinPresentationView.notifyPreSpinStopPresentationComplete()');
        this._emitter.emit(C_ViewEvent.PRE_SPIN_STOP_PRESENTATION_COMPLETE, spinPresentation);
    };


    /**
     * A hook for showing a "post-spin-stop" presentation. This presentation is shown immediately after a
     * set of spinning reels has been brought to a halt (and before any standard Win Presentations are shown).
     * Only once this presentation has completed, can the standard win presentation be shown. The presentation
     * is shown regardless of whether there are any wins or not. For a Spin Round with N spin results, this
     * presentation will be shown N times.
     * 
     * Override this method, in order to trigger a custom presentation: once your custom presentation has
     * completed, call "notifyPostSpinStopPresentationComplete", in order to allow the standard sequence of
     * ReelGroup actions to continue (a standard Win Presentation sequence may be triggered next, if there
     * are wins to show).
     * 
     * EXAMPLE USE CASES:
     * 1) Because standard win presentation comes next, its possible to use this to trigger actions before
     *    the win presentation (you simply have to check the data object passed for the spin result to see
     *    if there is anything you want to respond to)
     * 
     * @protected
     * @see notifyPostSpinStopPresentationComplete
     * @param {SpinResultPresentation} spinPresentation 
     * Presentation model for the Spin Result that is currently being shown (and for which thne reels have
     * just come to a halt).
     * @param {SpinPhaseType} spinPhase
     * The spin phase type currently being shown
     * @param {SpinPhasePresentationStateModel} spinPresentationState
     * Data describing current transitory "state" of the spin presentation.
     */
    showPostSpinStopPresentation(spinPresentation, spinPhase, spinPresentationState) {
        this._log.debug('WinPresentationView.showPostSpinStopPresentation: nothing to show (by default)');
        this.notifyPostSpinStopPresentationComplete();  
    };


    /**
     * Dispatches an event notification that the Post Spin Stop presentation has completed. This method must
     * not be overridden.
     * @protected
     * @final
     */
    notifyPostSpinStopPresentationComplete() {
        this._log.debug('WinPresentationView.notifyPostSpinStopPresentationComplete()');
        this._emitter.emit(C_ViewEvent.POST_SPIN_STOP_PRESENTATION_COMPLETE);
    };


    /**
     * A hook for showing a "post-spin-wins" presentation. This presentation is triggered immediately after
     * a standard win presentation sequence has completed: it will ONLY be triggered if a win presentation
     * was actually shown.
     * 
     * For a Spin Round with N spin results (and P winning spin results), this presentation will be triggered
     * P times.
     * 
     * Override this method in order to create or trigger a custom presentation. Once you custom presentation
     * is completed, call "notifyPostSpinWinsPresentationComplete" in order to inform ReelGroup to continue.
     * @potected
     * @see notifyPostSpinWinsPresentationComplete
     * @param {SpinResultPresentation} spinPresentation
     * Presentation model for the Spin Result currently in progress (and whose wins have just been shown).
     * @param {SpinPhaseType} spinPhase
     * The spin phase type currently being shown
     * @param {SpinPhasePresentationStateModel} spinPresentationState
     * Data describing current transitory "state" of the spin presentation.
     */
    showPostSpinWinsPresentation(spinPresentation, spinPhase, spinPresentationState) {
        this._log.debug('WinPresentationView.showPostSpinWinsPresentation: nothing to show (by default)');
        this.notifyPostSpinWinsPresentationComplete();
    };


    /**
     * Dispatches an event notification that the Post Spin Wins Presentation has completed. This method must
     * not be overridden.
     * @protected
     * @final
     */
    notifyPostSpinWinsPresentationComplete() {
        this._log.debug('WinPresentationView.notifyPostSpinWinsPresentationComplete()');
        this._emitter.emit(C_ViewEvent.POST_SPIN_WINS_PRESENTATION_COMPLETE);
    };


    /**
     * A hook for showing a "post-spin-result" presentation. This presentation is triggered as the very last
     * action of a Spin Result: it is always triggered, regardless of whether any wins were present in the
     * spin (if wins were present, then "postSpinWinsPresentation" comes first).
     * 
     * For a Spin Round with N spin results, this presentation will be triggered exactly N times.
     * 
     * Override this method in order to create or trigger a custom presentation. Once your presentation is
     * completed, call "notifyPostSpinResultPresentationComplete" in order to inform ReelGroup to continue.
     * @protected
     * @see notifyPostSpinResultPresentationComplete
     * @param {SpinResultPresentation} spinPresentation
     * Presentation model for the Spin Result that has just completed.
     * @param {SpinPhaseType} spinPhase
     * The spin phase type currently being shown
     * @param {SpinPhasePresentationStateModel} spinPresentationState
     * Data describing current transitory "state" of the spin presentation.
     */
    showPostSpinResultPresentation(spinPresentation, spinPhase, spinPresentationState)
    {
        this._log.debug('WinPresentationView.showPostSpinResultPresentation: nothing to show (by default)');
        this.notifyPostSpinResultPresentationComplete();
    };


    /**
     * Dispatches an event notification that the Post Spin Result Presentation has completed. This method must
     * not be overridden.
     * @protected
     * @final
     */
    notifyPostSpinResultPresentationComplete()
    {
        this._log.debug('WinPresentationView.notifyPostSpinResultPresentationComplete()');
        this._emitter.emit(C_ViewEvent.POST_SPIN_RESULT_PRESENTATION_COMPLETE);
    };


    /**
     * A hook for showing a "Post Spin Round" presentation. All Spin-like phases consist of at least 1 Spin
     * Round, and each Spin Round may consist of 1 or more Spin Results.
     * - Single Spin, Spin1 and Spin2 phases consist of exactly 1 Spin Round
     * - FreeSpin Phases consist of 1 or more "FreeSpin" Rounds (a FreeSpinRound is synonimous with a Spin
     *   Round: it is also referred to as simply a FreeSpin)
     * 
     * This presentation is shown after all other sub-components of the Spin Round have been shown: it is the
     * very final customizable presentation.
     * - For a Single Spin, Spin1 or Spin2 phase, this will be called exactly once.
     * - For a FreeSpin phase with N FreeSpins, this presentation will be called exactly N times.
     * 
     * Override this hook to create a custom presentation: once your custom presentation is completed, call
     * "notifyPostSpinRoundPresentationComplete", in order to inform the outside world that it is safe to
     * continue.
     * 
     * EXAMPLE USE CASES:
     * 1) For a FreeSpin round with very high winnings, we can show a final animated sequence on any custom
     *    characters in this presentation.
     * 
     * @protected
     * @see notifyPostSpinRoundPresentationComplete
     * @param {SpinRoundResultPresentation} roundPresentation
     * Presentation model for the Spin Round that has just completed.
     * @param {SpinPhaseType} spinPhase
     * The spin phase type currently being shown
     * @param {SpinPhasePresentationStateModel} spinPresentationState
     * Data describing current transitory "state" of the spin presentation.
     */
    showPostSpinRoundPresentation(roundPresentation, spinPhase, spinPresentationState) {
        this._log.debug('WinPresentationView.showPostSpinRoundPresentation: nothing to show (by default)');
        this.notifyPostSpinRoundPresentationComplete();
    };


    /**
     * Dispatches an event notification that the Post Spin Round presentation has completed. This method must
     * not be overridden.
     * @protected
     * @final
     */
    notifyPostSpinRoundPresentationComplete() {
        this._log.debug('WinPresentationView.notifyPostSpinRoundPresentationComplete()');
        this._emitter.emit(C_ViewEvent.POST_SPIN_ROUND_PRESENTATION_COMPLETE);
    };


    // TODO: Post spin phase hook as well ?


    //--------------------------------------------------------------------------------------------------
    // The following are all simple, overridable hook actions for one off events. You don't trigger
    // async actions that you need to complete before something else in these: you just do something
    // simple when an event happens (the methods are provided only for simple convenience). Many of
    // these are most useful for doing a single, immediate action of tidy up (eg: reset some symbols
    // to some state without animation)
    //--------------------------------------------------------------------------------------------------
    /**
     * Method called when IDLE is triggered.
     * @protected
     */
    showIdle() {
        this._log.debug('WinPresentationView.showIdle()');
    };


    /**
     * Method called when a single reel spin has started.
     * @protected
     * @param {SingleReelSpinEventData} reelSpinEventData
     * Data describing the reel that has just started spinning.
     */
    onSingleReelSpinStarted(reelSpinEventData) {
        this._log.debug('WinPresentationView.onSingleReelSpinStarted()');
    };


    /**
     * Method called when a single reel spin is settling.
     * @protected
     * @param {SingleReelSpinEventData} reelSpinEventData 
     * Data describing the reel that is settling.
     */
    onSingleReelSpinSettling(reelSpinEventData) {
        this._log.debug('WinPresentationView.onSingleReelSpinSettling()');
    };


    /**
     * Method called when a single reel spin has finished.
     * @protected
     * @param {SingleReelSpinEventData} reelSpinEventData 
     * Data describing the reel that has just stopped spinning.
     */
    onSingleReelSpinStopComplete(reelSpinEventData) {
        this._log.debug('WinPresentationView.onSingleReelSpinStopComplete()');
    };


    /**
     * Hook for triggering one off events when a Spin Round presentation has started. Override this
     * method to trigger a custom action. This is for one-off events (you do not need to dispatch
     * any event to indicate your custom action is completed). To carry out an action which must
     * complete BEFORE a Spin Round starts, please override "showPreSpinRoundPresentation" instead.
     * @protected
     */
    onSpinRoundPresentationStarted() {
        this._log.debug('WinPresentationView.onSpinRoundPresentationStarted()');
    };


    /**
     * Hook for triggering one off events when a Spin Result presentation has started. Override this
     * method to trigger a custom action. This is for one-off events (you do not need to dispatch an
     * event to indicate your custom action is completed). To carry out an asynchronous event, please
     * override showPreSpinResultPresentation instead.
     * @protected
     * @param {SpinResultPresentation} [spinResult]
     * Data description of the spin result presentation that has started. May not be available, if
     * we are asynchronously fetching results from the server.
     */
    onSpinResultPresentationStarted(spinResult) {
        this._log.debug('WinPresentationView.onSpinResultPresentationStarted()');
    };


    /**
     * Hook for triggering one off events one a Spin Result presentation has completed. Override this
     * method to trigger a custom action. This is for one-off events (you do not need to dispatch an
     * event to indicate your custom action is completed). To carry out an asynchronous event, please
     * override showPostSpinResultPresentation instead.
     * @protected
     */
    onSpinResultPresentationComplete() {
        this._log.debug('WinPresentationView.onSpinResultPresentationComplete()');
    };


    /**
     * Hook for triggering one off events one a Spin Round presentation has completed. Override this
     * method to trigger a custom action. This is for one-off events (you do not need to dispatch an
     * event to indicate your custom action is completed). To carry out an asynchronous event, please
     * override showPostSpinRoundPresentation instead.
     * @protected
     */
    onSpinRoundPresentationComplete() {
        this._log.debug('WinPresentationView.onSpinRoundPresentationComplete()');
    };


    /**
     * Hook for triggering one off events one a Spin Phase presentation has completed. Override this
     * method to trigger a custom action. This is for one-off events (you do not need to dispatch an
     * event to indicate your custom action is completed).
     * 
     * EXAMPLE USE CASE:
     * Performs any tidy up when the Spin Phase Presentation is completed.
     * @protected
     */
    onSpinPhasePresentationComplete() {
        this._log.debug('WinPresentationView.onSpinPhasePresentationComplete()');
    };
};

export default BaseWinPresentationView;