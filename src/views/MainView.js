import BaseView from "./BaseView";
import C_GameEvent from "../../../cms-core-logic/src/const/C_GameEvent";
import C_PointerEvt from "../const/C_PointerEvt";

/**
 * Main view contains all views needed for the main slot game
 * Game View - the slot game itself
 * UI - user interface
 * MenuView - the settings menu, for performance reasons it's only created once
 * and then it's visibility is toggled on/off
 * Bonus View
 */
class MainView extends BaseView
{
    constructor()
    {
        super();

        /**
         * Main container, which all content (with the exception of Bonus View) is added to. This
         * has been done, to allow us to execute pans from Slot Game to Bonus View (which we
         * occasionally need for a game client). This also gives us the option of making main
         * content non-visible when a bonus is in progress, which could improve general rendering
         * performance of the game.
         * @private
         * @type {PIXI.Container}
         */
        this._mainContainer = new PIXI.Container();
        this.addChild(this._mainContainer);

        /**
         * @private
         */
        this._gameView = this.gameFactory.getSlotGameView();
        this._mainContainer.addChild(this._gameView);

        /**
         * @private
         */
        this._ui = this.gameFactory.getUI();
        this._mainContainer.addChild(this._ui);

        /**
         * A generic Loading Spinner that can be shown or hidden with a standard GameEvent call.
         * @private
         * @type {LoadingSpinnerView}
         */
        this._loadingSpinner = this.gameFactory.getLoadingSpinner();

        /**
         * Tracks whether the loading spinner should currently be shown.
         * @private
         * @type {boolean}
         */
        this._loadingSpinnerIsShown = false;

        //Various listeners for displaying features


        // RUSSELL'S NOTE: These listeners are coming from "BonusController"
        this.dispatcher.on(C_GameEvent.SHOW_BONUS_VISUALIZATION, this.createBonusView, this);
        this.dispatcher.on(C_GameEvent.BONUS_VISUALIZATION_COMPLETE, this.destroyBonusView, this);
        this.dispatcher.on(C_GameEvent.SHOW_MENU, this.showMenu, this);
        this.dispatcher.on(C_GameEvent.SHOW_SESSION_STATS, this.showSessionStats, this);
        this.dispatcher.on(C_GameEvent.SHOW_CASHIER, this.showCashier, this);
        this.dispatcher.on(C_GameEvent.SHOW_AUTOPLAY_MENU, this.showAutoPlayMenu, this);
        this.dispatcher.on(C_GameEvent.SHOW_BET_MENU, this.showBetMenu, this);
        this.dispatcher.on(C_GameEvent.SHOW_FREESPIN_START_NOTIFICATION, this.showFreeSpinStartNotification, this);
        this.dispatcher.on(C_GameEvent.SHOW_LOADING_SPINNER, this.showLoadingSpinner, this);
        this.dispatcher.on(C_GameEvent.HIDE_LOADING_SPINNER, this.hideLoadingSpinner, this);

        this.playMainBackgroundMusic();
    };


    /**
     * Plays any required "main game" backround music.
     * @private
     */
    playMainBackgroundMusic()
    {
        this.log.info(`MainView.playMainBackgroundMusic()`)

        // Trigger the main game's background music playing (if required)
        /**
         * @type {MainGameBgMusicConfig}
         */
        let mainGameBgMusicConfig = this.config.music.mainGameBgMusic;
        if (mainGameBgMusicConfig)
        {
            if (mainGameBgMusicConfig.type === "looping") {
                let musicVolume = mainGameBgMusicConfig.volume;
                this.log.info(`MainView: starting looping background music (id:${mainGameBgMusicConfig.assetName},volume:${musicVolume})`);
                this.sound.startMainGameMusic(mainGameBgMusicConfig.assetName, musicVolume);
            }
            else
            {
                this.log.info('MainView: no background music to play for main game');
            }
        }
        else this.log.info('MainView: no background music config specified, will not play music for main game');
    }


    /**
     * Adds the Menu View to the Main View and displays it
     * @private
     */
    showMenu() {
        this._mainContainer.addChild(this.gameFactory.getMenuView());
    };


    /**
     * Adds the Session Stats View to the Main View and displays it.
     * @private
     */
    showSessionStats() {
        this._mainContainer.addChild(this.gameFactory.getSessionStats());
    };


    /**
     * Adds the Cashier View to the Main View and displays it
     * @private
     */
    showCashier() {
        this._mainContainer.addChild(this.gameFactory.getCashier());
    };


    /**
     * Adds the Auto Play Menu View to the Main View and displays it
     * @private
     */
    showAutoPlayMenu() {
        this._mainContainer.addChild(this.gameFactory.getAutoplayMenu());
    };


    /**
     * Adds the Bet Menu View to the Main View and displays it.
     * @private
     */
    showBetMenu() {
        this._mainContainer.addChild(this.gameFactory.getBetMenu());
    };


    /**
     * Shows the FreeSpin Start notification to the player.
     * @private
     */
    showFreeSpinStartNotification() {
        this.addChild(this.gameFactory.getFreeSpinStartNotification());
    };


    /**
     * Create bonus view, in response to the Show Bonus Visualization event fired by
     * Game Controller.
     * @private
     * @param {boolean} isRestored
     * The GameController will report if the Bonus is a restored Bonus. If it is, it
     * should be shown without an animated transition (crossfade).
     */
    createBonusView(isRestored=false)
    {
        if (!this.bonusView)
        {
            this.log.debug(`MainView.createBonusView()`);

            let bonusClass = this.gameFactory.getBonusViewClass();
            
            /**
             * Reference to the current, active instance of the Bonus View.
             * @protected
             */
            this.bonusView = new bonusClass();
            this.bonusView.interactive = true;
            this.addChild(this.bonusView);
            this.bonusView.x = this._gameView.x;
            this.bonusView.y = this._gameView.y;
        }
    };


    /**
     * Destroys the bonus view : invoked once the Bonus Phase is complete (as indicated by the Bonus Visualization
     * Complete Game Event). Override this method only in special circumstances (eg: if you do not want the BonusView
     * to be automatically removed from the display list)
     * @protected
     */
    destroyBonusView()
    {
        if (this.bonusView)
        {
            this.removeChild(this.bonusView);
            //this.bonusView.destroy(); // if BonusView inherits BaseView, it calls this itself.. this potentially causes problems.
            this.bonusView = null;

            // Ensure that main background music plays again after the bonus
            this.sound.restoreMainGameMusic();
        }
    };


    /**
     * Shows the "slow comms" / "loading" spinner. Many views have an in-built equivalent to this,
     * but this can be invoked generally (eg: by GameController or AppController) to show a spinner
     * that appears over the main view.
     * @private
     */
    showLoadingSpinner()
    {
        if (this._loadingSpinnerIsShown) {
            return;
        }

        this.log.debug('MainView.showLoadingSpinner()');

        this._loadingSpinnerIsShown = true;
        this._loadingSpinner.showLoadingSpinner();
        this.addChild(this._loadingSpinner);
    };


    /**
     * Hides the "slow comms" / "loading" spinner.
     * @private
     */
    hideLoadingSpinner()
    {
        if (this._loadingSpinnerIsShown === false) {
            return;
        }

        this.log.debug('MainView.hideLoadingSpinner()');

        this._loadingSpinnerIsShown = false;
        this._loadingSpinner.hideLoadingSpinner();
        this.removeChild(this._loadingSpinner);
    };
}

export default MainView
