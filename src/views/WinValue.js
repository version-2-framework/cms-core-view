import {getGameViewLogger} from "../../../cms-core-logic/src/logging/LogManager";

const log = getGameViewLogger();

/**
 * An animated text used in various places to display winnings. This is a simple
 * animation of some numbers appearing: we never show any currency indicators, or
 * anything like that. The graphics used to draw the numbers are actually indivudal
 * textures (we don't use a text field), allowing us to both stylize them nicely
 * for the game client skin, and to show individual tween animations on each letter
 * appearing.
 * 
 * This class does not explicitly display a credit value, or antything like that:
 * it simply shows a number, with a short animation. No event is fired when the
 * animation completes: instead, you pass in the duration the animation should last
 * for.
 * 
 * @implements WinValue
 */
class WinValue extends PIXI.Container
{

    /**
     * Creates a new Win Value instance. The animation will not be autoplayed - instead
     * a call to "playAnimation must be made, when you want the animation to trigger.
     * @param {number} value 
     * The value to show. This should be a simple numerical value. There are no limits on
     * how large this value can be, but a minimum value of 1 is expected for correct
     * functionality.
     * @param {number} duration 
     * The exact duration (in seconds) that the win should last for.
     */
    constructor(value, duration)
    {
        super();

        /**
         * The text rendering of the value.
         * @private
         * @type {string}
         */
        this._valueString = String(value);

        /**
         * The duration, in seconds, that the win animation should last for.
         * @private
         * @type {number}
         */
        this._duration = duration;
    };


    // Russell's note: apparently this method is public, but wiener didn't write any documentation
    // for it, and i cannot understand why on earth it has to be called externally the class. There
    // may be a good reason - but because wiener have not written a word of documentation in the
    // code base, i have absolutely no idea what their intentions were. Even the method name ("create"
    // is rather vague.
    /**
     * @public
     */
    create()
    {
        let xPos = 0;

        for (let letterIdx=0; letterIdx < this._valueString.length; letterIdx++)
        {
            let suffix = this._valueString.charAt(letterIdx);

            // Russell's note: i am aware that this contradicts what was written in the class header,
            // but that is because i have only just written the class header - no contract about
            // how this was meant to work previously existed. This bit needs deprecating, or the class
            // description needs adjusting appropriately.
            if (suffix == ".")
            {
                suffix = "dot";
            }

            let letterSprite = PIXI.app.assets.getSprite(`wpanim_${suffix}`);
            letterSprite.anchor.set(.5,.5);
            letterSprite.x = xPos;
            letterSprite.x += letterSprite.width * .5;
            letterSprite.y += letterSprite.height * .5;
            this.addChild(letterSprite);
            xPos += letterSprite.width;
        }
    }


    // Russell's note: apparently this is a publc method too, that needs to be called externally
    // but naturally, wiener didn't write a single word of documentation to record this, so there
    // is no explanation from them on *why* they thought it was better for it to be a public
    // method, or under what circumstances or when it should be called.
    /**
     * Starts the animation playing: invoke this when you need the animation to start.
     * @public
     */
    playAnimation()
    {
        log.debug(`WinValue.playAnimation(value:${this._valueString},duration:${this._duration})`);

        let singleCharTweenDuration = 0.2;
        let fadeOutDuration = 0.15;

        // if requested duration is too short, skip any animation, just let the letter's show.
        if (singleCharTweenDuration >= this._duration) {
            TweenMax.delayedCall(this._duration, this.parent.removeChild, [this], this.parent);
        }
        // Otherwise, if requested duration is long enough, show an animation of all of the
        // numers appearing sequentially.
        else
        {
            // Work out the delay between chars: we will use an ideal value if there is enough time.
            // If not, we simply use the maximum possible delay between chars that allows us to make
            // all chars appear within duration.
            let numCharacters = this.children.length;
            let maxDelayBetweenChars = (this._duration - singleCharTweenDuration) / numCharacters;
            let idealDelayBetweenChars = 0.06;
            let delayBetweenChars = Math.min(maxDelayBetweenChars, idealDelayBetweenChars);

            log.debug(`numChars = ${numCharacters}`);
            log.debug(`delayBetweenChars = ${delayBetweenChars}`);

            this.children.forEach((characterSprite, letterSpriteIndex) => {
                TweenMax.from(
                    characterSprite.scale,
                    singleCharTweenDuration,
                    {x:0, y:0, delay:letterSpriteIndex * delayBetweenChars, ease:Back.easeOut}
                );
            });

            // make the animation fade out, a short time before it completes.
            TweenMax.to(this, .15, {alpha:0, delay:this._duration - fadeOutDuration});
            TweenMax.delayedCall(this._duration, this.parent.removeChild, [this], this.parent);
        }
    };


    /**
     * @public
     */
    destroy()
    {
        super.destroy();
    };
}

export default WinValue