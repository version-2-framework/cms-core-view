/**
 * Provides custom easing functions for TweenMax.
 */
class CustomEase
{
    constructor()
    {

        this.DoubleBounce = function(p)
        {
            if (p < .7)
            {
                return p * p * 2.0408;
            }
            else if (p < 1)
            {
                return 2.222 * p * p - 3.778 * p + 2.556;
            }
            return 1;
        };


        this.TripleBounce = function(p)
        {
            if (p < .7)
            {
                return p * p *2.0408;
            }
            else if (p < .9)
            {
                return 8 * p * p - 12.8 * p + 6.040;
            }
            else if (p < 1)
            {
                return 8 * p * p - 15.2 * p + 8.2;
            }
            return 1;
        };

    }
}

export default (new CustomEase);
