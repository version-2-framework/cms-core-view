// TODO: This module can be named better - its not really "DefaultLoadingRoutine", its
// "load the game assets in the default way". Its not yet 100% clear if we will ever
// need to customize the set of assets being loaded - i think we will, but this could
// be handled with json configuration files which relate to the assets. The use cases
// are not yet fully developed.

import { getAppLogger } from "../../../cms-core-logic/src/logging/LogManager";

const log = getAppLogger();

/**
 * Defines the standard routine to load a bunch of assets.
 */
class DefaultLoadingRoutine
{
    constructor(app)
    {
        this._app = app;
    };


    // TODO: The failure callback needs to specify information about why loading has failed
    // (eg: the list of assets that failed, data about why they failed). We could also handle
    // different assets differently - sounds, for example, could be optional (and failure in
    // the sounds wouldn't cause us to abort the app launching procedure)
    /**
     * @public
     * @param {(progress:number) => void} onProgress 
     * A callback to be executed whenever the loading sequence progresses. A progress value will be passed
     * to the callback - this is a value in the range of 0 to 100 (normally expected to be a floating point
     * value)
     * @param {() => void} onComplete
     * A callback that will be executed when all assets have been loaded succesfully.
     * @param {() => void} onFailure
     * A callback that will be executed when an asset load has failed for some reason.
     * TODO: Document the use case of this better.
     */
    loadAssets(onProgress, onComplete, onFailure)
    {
        let version = this._app.version;
        let atlases = this._app.bootLoader.resources.atlases.data.paths;
        let loader = PIXI.loader;
        let loadHasFailed = false;


        for (let atlasIndex = 0; atlasIndex < atlases.length; atlasIndex++)
        {
            let regExp = /([^\/\\&\?]+)\.\w{3,4}/g;
            let assetPath = atlases[atlasIndex];

            let assetName = regExp.exec(assetPath)[1];
            let assetFullPath = `${assetPath}?version=${version}`;
            
            log.debug(`DefaultLoadingRoutine: loading texture asset ${assetName} from ${assetFullPath}`);
            
            loader.add(assetName, assetFullPath);
        }

        let dynamicAssetPaths = this._app.bootLoader.resources.dynamicAssets.data.paths;

        log.debug(`DefaultLoadingRoutine: loading ${dynamicAssetPaths.length} dynamicAssets`);
        log.debug(`DefaultLoadingRoutine: dynamicAssetPaths = ${dynamicAssetPaths}`);

        for (let assetIndex = 0; assetIndex < dynamicAssetPaths.length; assetIndex++)
        {
            let regExp = /([^\/\\&\?]+\.\w{3,4})/g;
            let assetPath = dynamicAssetPaths[assetIndex];

            let assetName = regExp.exec(assetPath)[1];
            let assetFullPath = `${assetPath}?version=${version}`;

            log.debug(`DefaultLoadingRoutine: loading dynamicAsset ${assetName} from ${assetFullPath}`);

            loader.add(assetName, assetFullPath, {loadType:"XHR", xhrType:"blob"});
        }

        // Load up dragon bones assets
        let dragonBonesAssetConfigs = this._app.bootLoader.resources.atlases.data.dragonBones;
        if (dragonBonesAssetConfigs)
        {
            dragonBonesAssetConfigs.forEach(assetConfig =>
            {
                let assetTypeSuffixes = 
                [
                    { suffix:"",     extension:".png"  },
                    { suffix:"_tex", extension:".json" },
                    { suffix:"_ske", extension:".json" }
                ];

                assetTypeSuffixes.forEach(assetTypeSuffix =>
                {
                    let assetName = `${assetConfig.name}${assetTypeSuffix.suffix}`;
                    let assetPath = `${assetConfig.path}${assetConfig.name}${assetTypeSuffix.suffix}${assetTypeSuffix.extension}`;
                    let assetFullPath = `${assetPath}?version=${version}`;

                    log.debug(`DefaultLoadingRoutine: loading dragonsBones asset ${assetName} from ${assetFullPath}`);

                    loader.add(assetName, assetFullPath);
                });
            });
        }


        // Load the menu config
        // TODO: We want the option of overriding this, based on BusinessConfig
        // (eg: certain licensees can get a custom json file loaded, if we REALLY
        // need to do this)
        let rulesConfigFileName = 'rulesConfig';
        loader.add('rulesConfig', `game-static/assets/rules/${rulesConfigFileName}.json`);

        loader.onLoad.add(loader => {
            onProgress(loader.progress);
        });

        loader.onError.add((errMsg, loader, resource) => {
            log.error(`DefaultLoadingRoutine: asset load has failed: ${errMsg}`);
            loadHasFailed = true;
        });

        loader.load(() => {
            log.debug(`DefaultLoadingRoutine: loading sequence has completed. all assets loaded OK? ${!loadHasFailed}`);

            if (loadHasFailed)
            {
                onFailure();
            }
            else onComplete();
        });
    }
}

export default DefaultLoadingRoutine;