/**
 * @type {{ [id:string]:ShakeInProgressProperties }}
 */
let shakesInProgress = {};

/**
 * A global counter, which caches an id for new shakes.
 */
let shakeIdCounter = 0;


/**
 * Shakes a display object, using a given configuration.
 * @param {PIXI.DisplayObject} targetDisplayObject
 * Target display object that should be shaken.
 * @param {SimpleShakeConfig} config
 * Configures the shake animation.
 */
export function shakeDisplayObjectSimple (targetDisplayObject, config)
{
    let newShakeId = `key${shakeIdCounter}`;

    // Check if object already has shake applied , if so the previous one is killed

    let targetHasShakeInProgress = false;
    
    // Start x/y positions for the shake. If the current target has no shake in progress,
    // then we use current x/y positions of the display object for these values. If a
    // shake was already in progress, we want to return to the correct start x/y positions
    // after our new shake is completed, so we will use the existing cached startX/y pos
    // for the shake that is in progress (instead of current x/y positions of the display
    // object) - means that when our new shake is completed, we should return out target
    // to rest in its correct positions.
    let shakeStartPosX = 0;
    let shakeStartPosY = 0;

    // If the target display object already has a shake in progress, kill that shake:
    // we only want 1 shake animation in progress at a time.
    for (let shakeId in shakesInProgress)
    {
        if (shakesInProgress[shakeId].target == targetDisplayObject)
        {
            let shakeInProgress = shakesInProgress[shakeId];
            
            shakeInProgress.tween.kill();

            // I think this block is now redundant
            /*
            // TODO: Cannot 100% get my head around why this is done yet
            targetDisplayObject.x = shakeInProgress.startPosX;
            targetDisplayObject.y = shakeInProgress.startPosY;
            */

            shakeStartPosX = shakeInProgress.startPosX;
            shakeStartPosY = shakeInProgress.startPosY;

            delete shakesInProgress[shakeId];

            targetHasShakeInProgress = true;
        }
    }

    // If no shake in progress, we will just use current x / y positions as the
    // start point of the shake.
    if (!targetHasShakeInProgress)
    {
        shakeStartPosX = targetDisplayObject.x;
        shakeStartPosY = targetDisplayObject.y;
    }

    // Configure the tween which will execute the actual shake (the tween actually just
    // calls an update function which shakes the displayObject: the tween does not directly
    // change anything on the target displayObject)
    let tween = TweenMax.to(
        newShakeId,
        config.duration,
        {
            onUpdate: updateShake,
            onUpdateParams: [newShakeId],
            onComplete: finishShake,
            onCompleteParams: [newShakeId],
            delay: config.delay
        });

    // Cache properties of the new shake.
    shakesInProgress[newShakeId] =
    {
        target : targetDisplayObject,
        startPosX: shakeStartPosX,
        startPosY: shakeStartPosY,
        currAmplitudeX : config.xAmplitude,
        currAmplitudeY : config.yAmplitude,
        amplitudeMultiplier : config.amplitudeMultiplier,
        tween: tween
    };

    // This is weird - if a shake is in progress, make sure that the display object
    // initially has its original position. However, we already explicitly overrode
    // its position in this scenario - before the tween was applied - meaning the
    // tween is from required starting position, and to required end position
    // This has the effect of momentarily setting start position to what it already
    // was. You'd think this was to prevent a jump, but the animation - i think that
    // the tween will go from current position. So this logic is a bit tortured and
    // pointless ? EXCEPT that the shake is not done by TweenMax directly, it is done
    // manually.. also, the shake doesnt seem to take current x position into account
    // at all, it uses startPosX/posY that we define - above -
    /*
    if (targetHasShakeInProgress)
    {
        targetDisplayObject.x = currShakePosX;
        targetDisplayObject.y = currShakePosY;
    }
    */

    shakeIdCounter ++;
};


/**
 * Updates a single shake.
 * @param {number} shakeId
 */
function updateShake(shakeId)
{
    let shakeProperties = shakesInProgress[shakeId];
    let targetDisplayObject = shakeProperties.target;
    let shakeStartPosX = shakeProperties.startPosX;
    let shakeStartPosY = shakeProperties.startPosY;
    let shakeAmplitudeX = shakeProperties.currAmplitudeX;
    let shakeAmplitudeY = shakeProperties.currAmplitudeY;

    // Update position of display object based on current shake amplitudes
    targetDisplayObject.x = shakeStartPosX - shakeAmplitudeX + (Math.random() * shakeAmplitudeX * 2);
    targetDisplayObject.y = shakeStartPosY - shakeAmplitudeY + (Math.random() * shakeAmplitudeY * 2);

    // Update shake amplitudes
    shakeProperties.currAmplitudeX *= shakeProperties.amplitudeMultiplier;
    shakeProperties.currAmplitudeY *= shakeProperties.amplitudeMultiplier;
};


/**
 * Finishes a shake in progress, by resetting the target display object back to its start
 * position, and deleting cached references to the shake.
 * @param {number} shakeId
 */
function finishShake(shakeId)
{   
    let shakeProperties = shakesInProgress[shakeId];
    let targetDisplayObject = shakeProperties.target;

    // Once all shakes are complete, reset display object position to its starting position.
    targetDisplayObject.x = shakeProperties.startPosX;
    targetDisplayObject.y = shakeProperties.startPosY;

    delete shakesInProgress[shakeId];
};