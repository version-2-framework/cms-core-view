import C_GameEvent from "../../../cms-core-logic/src/const/C_GameEvent";
import * as LogManager from "../../../cms-core-logic/src/logging/LogManager";

const log = LogManager.getGameViewLogger();

/**
 * Used to position and size graphical elements. The basic (default) coordinate system is rect.
 */
class Layout
{
    /**
     * The Layout class creates two positioning rectangles
     * <p> rect (0,0,w,h) - this is default if no other positioning object
     * is passed as a parameter to a positioning/sizing method </p>
     * <p> portraitRect - a rectangle, whose width is the narrower side of the display
     * </pre>
     * @param {number} width
     * "the width of the default positioning rect"
     * @param {number} height
     * "The height of the default positioning rect"
     */
    constructor(width, height)
    {
        this.app = PIXI.app;

        /**
         * @private
         * @type {PIXI.DisplayObject}
         */
        this._stage = this.app.stage;

        /**
         * @private
         * @type {number}
         */
        this._startW = width;

        /**
         * @private
         * @type {number}
         */
        this._startH = height;

        /**
         * The dimensions of the game clientm, in pixels.
         * @private
         * @type {PIXI.Rectangle}
         */
        this._rect = new PIXI.Rectangle(0, 0, width, height);

        /**
         * Alias for horizontalAlignLeft
         * @public
         */
        this.hAlignLeft = this.horizontalAlignLeft;

        /**
         * Alias for horizontalAlignCenter
         * @public
         */
        this.hAlignCent = this.horizontalAlignCenter;

        /**
         * Alias for horizontalAlignRight
         * @public
         */
        this.hAlignRight = this.horizontalAlignRight;

        /**
         * Alias for verticalAlignBottom
         * @public
         */
        this.vAlignBot = this.verticalAlignBottom;

        /**
         * Alias for verticalAlignMiddle
         * @public
         */
        this.vAlignMid = this.verticalAlignMiddle;

        /**
         * Alias for verticalAlignTop
         * @public
         */
        this.vAlignTop = this.verticalAlignTop;
    };


    /**
     * Is the device in landscape mode ?
     * @public
     * @return {boolean}
     */
    isLandscape()
    {
        return this.landscape;
    };


    /**
     * Returns the x position of the game view, within html coordinate space.
     * @public
     * @return {number}
     */
    getGamePosX()
    {
        return (window.innerWidth - this.getGameWidth()) * 0.5;
    };


    /**
     * @public
     * @return {number}
     */
    getGamePosY()
    {
        //return 0;
        return (window.innerHeight - this.getGameHeight()) * 0.5;
    };


    /**
     * @public
     * @return {number}
     */
    getGameWidth()
    {
        // TODO: This property is clearly incorrectly named
        // Unfortunately, "deviceWidth" is not a documented property,
        // so to solve this, we need to reverse engineer all places
        // in which it may be used as though it was a public field ..

        return this.deviceWidth;
    };


    /**
     * @public
     * @return {number}
     */
    getGameHeight()
    {
        // TODO: This property is clearly incorrectly named
        // Unfortunately, "deviceHeight" is not a documented property,
        // so to solve this, we need to reverse engineer all places
        // in which it may be used as though it was a public field ..

        return this.deviceHeight;
    };


    /**
     * Execute recalculations needed when screen is resized.
     * @public
     * @param width {number} width
     * @param height {number} height
     */
    onResize(width, height)
    {
        log.debug(`Layout.onResize(newWidth:${width},newHeight:${height})`);

        this.deviceWidth = width;
        this.deviceHeight = height;

        this.landscape = width > height;

        if (this.landscape)
        {
            // TODO: Why is this called portrait rect, when the game is in landscape mode?
            // I think this property is just incorrectly named.
            this.portraitRect =  new PIXI.Rectangle(0, 0, this._startW, this._startW * this._startW / this._startH);
        }
        else
        {
            this.portraitRect =  new PIXI.Rectangle(0, 0, this._startW, this._startW * height/width);
        }

        if (this.app.dispatcher)
        {
            this.app.dispatcher.emit(C_GameEvent.RESIZE);
        }

        // this.stage.filterArea = new PIXI.Rectangle
        // (
        //     Math.floor(this.app.stage.x),
        //     Math.floor(this.app.stage.y),
        //     Math.floor(1920 * this.app.stage.scale.x),
        //     Math.floor(1080 * this.app.stage.scale.y)
        // );
    }


    // TODO: It looks highly unlikely that this method is correctly implemented,
    // as it references "portraitRect", which - semantically - seems completely
    // inappropriate. I doubt this is being used anywhere though.
    /**
     * @public
     * @param {PIXI.DisplayObject} object 
     * @return {boolean}
     */
    isInGameBounds(object)
    {
        return object.x.toFixed(2) >= this.portraitRect.x.toFixed(2) &&
               object.y.toFixed(2) <= this.portraitRect.x.toFixed(2) &&
               object.y.toFixed(2) >= this.portraitRect.y.toFixed(2) &&
               object.y.toFixed(2) <= this.portraitRect.y.toFixed(2);
    };


    /**
     * Set position of a target object relative to a source
     * @public
     * @param target - display object
     * @param x - x position (percent of source object)
     * @param y - y position (percent of source object)
     * @param src - either a display object or a rectangle
     */
    setPos(target, x, y, src)
    {
        this.horizontalAlignLeft(target, x, src);
        this.verticalAlignTop(target, y, src);
    }


    /**
     * Set both size and position of a target object relatively to a source rectangle. If no source
     * rectangle is provided, then screen-space will be used as the source rectangle instead.
     * @public
     * @param targetDisplayObject
     * The display object to position and scale.
     * @param {number} height
     * The height to set the display obect to, as a percentage of either screen-height, or of the
     * total height of the optional "sourceRectangle". A value of 100 represents "100% of height".
     * @param {number} x
     * X position (within either screen-space or sourceRectangle) to position the target display
     * object at. This is a percentage value, where 100 == 100%.
     * @param {number} y
     * Y position (within either screen-space or sourceRectangle) to position the target display
     * object at. This is a percentage value, where 100 == 100%.
     * @param {{x:number,y:number,width:number,height:number}} [sourceRectangle]
     * An optional area, to use as the basis for scaling and positioning. This can be either a display
     * object, or a rectangle. If not provided, then the screen's dimensions will be used instead.
     */
    setHeightAndPos(targetDisplayObject, height, x, y, sourceRectangle)
    {
        this.propHeightScale(targetDisplayObject, height, sourceRectangle);
        this.setPos(targetDisplayObject, x, y, sourceRectangle);
    };


    /**
     * Calculates and returns a horizontal position, relative to the local coordinate space of a
     * given rectangle. If no "sourceRectangle" parameter is provided, then screen-space is used
     * instead. For example, pass a value of 50 to calculate an x position 50% of the way across
     * the selected sourceRectangle: if the sourceRectangle has an x position of 400, and a width
     * of 1000, then the returned value will be (400 + (1000 * 0.5)), eg: 900 (exactly half way
     * across the sourceRectangle).
     * @public
     * @param {number} proportionalX
     * The proportional x position (within coordinate space of either sourceRectangle or screen
     * space) to calculate. This is a perctentage based value, where 100 == 100%.
     * @param {{x:number,y:number,width:number,height:number}} [sourceRectangle]
     * Optional source rectangle: if provided, the relative x position will be calculated according
     * to the position and width of this rectangle.
     * @return {number}
     */
    relX(proportionalX, sourceRectangle=undefined)
    {
        if (sourceRectangle === undefined)
        {
            sourceRectangle = this._rect;
        }

        return sourceRectangle.x + (proportionalX * sourceRectangle.width / 100);
    };


    /**
     * Calculates and returns a vertical position, relative to the local coordinate space of a
     * given rectangle. If no "sourceRectangle" parameter is provided, then screen-space is used
     * instead. For example, pass a value of 50 to calculate a y position 50% of the way down
     * the selected sourceRectangle: if the sourceRectangle has an y position of 400, and a height
     * of 1000, then the returned value will be (400 + (1000 * 0.5)), eg: 900 (exactly half way
     * down the sourceRectangle).
     * @public
     * @param {number} proportionalY
     * The proportional x position (within coordinate space of either sourceRectangle or screen
     * space) to calculate. This is a perctentage based value, where 100 == 100%.
     * @param {{x:number,y:number,width:number,height:number}} [sourceRectangle]
     * Optional source rectangle: if provided, the relative x position will be calculated according
     * to the position and width of this rectangle.
     * @return {number}
     */
    relY(proportionalY, sourceRectangle=undefined)
    {
        if (sourceRectangle === undefined)
        {
            sourceRectangle = this._rect;
        }

        return sourceRectangle.y + (proportionalY * sourceRectangle.height / 100);
    };


    /**
     * Calculates a width (in pixels), as a percentage of the total width of a target rectangle. If no
     * target rectangle is passed, then the method falls back to using total width of the screen. EG:
     * "layout.relWidth(100)" returns the total width of the screen, "layout.relWidth(50)" returns half
     * the width of the screen, and so on.
     * @public
     * @param {number} percentageWidth
     * A percentage value (where 100 == 100%), representing the percentage of the target rectangle's
     * width that you want to calculate.
     * @param {{width:number}} [sourceRectangle]
     * Either a display object or a rectangle: alternatively, omit this parameter, and the value returned
     * will be calculated relative to screen dimensions.
     * @returns {number}
     * The pixel width that was calculated.
     */
    relWidth(percentageWidth, sourceRectangle=undefined)
    {
        if (sourceRectangle === undefined)
        {
            sourceRectangle = this._rect;
        }

        return percentageWidth * sourceRectangle.width  / 100;
    };


    /**
     * Calculates a height (in pixels), as a percentage of the total height of a target rectangle. If no
     * target rectangle is passed, then the method falls back to using total height of the screen. EG:
     * "layout.relHeight(100)" returns the total height of the screen, "layout.relHeight(50)" returns half
     * the height of the screen, and so on.
     * @public
     * @param {number} percentageHeight
     * A percentage value (where 100 == 100%), representing the percentage of the target rectangle's
     * height that you want to calculate.
     * @param {{height:number}} [sourceRectangle]
     * Either a display object or a rectangle: alternatively, omit this parameter, and the value returned
     * will be calculated relative to screen dimensions.
     * @returns {number}
     * The pixel height that was calculated.
     */
    relHeight(percentageHeight, sourceRectangle=undefined)
    {
        if (sourceRectangle === undefined)
        {
            sourceRectangle = this._rect;
        }

        return percentageHeight * sourceRectangle.height / 100;
    };


    /**
     * Scales a target, preserving its aspect relative to a source. The target's size will be changed,
     * so that its new width matches a calculated value (and the height is relatively scaled to match
     * the width). The new width will be a percentage of a target rectangle's width (or a percentage of
     * the width of the screen, if no target rectangle is provided).
     * @public
     * @param target
     * Display object instance whose size is to be adjusted.
     * @param {number} scale
     * A percentage value (where 100 == 100%), representing the percentage of the target rectangle's
     * width that you want to set the target's width (and relative scale) to.
     * @param {{width:number}} [sourceRectangle]
     * Display object or rectangle
     */
    propWidthScale(target, scale, sourceRectangle=undefined)
    {
        if (sourceRectangle === undefined)
        {
            sourceRectangle = this._rect;
        }

        let requiredScale = sourceRectangle.width * scale/100 / target.width * target.scale.x;
        target.scale.x = requiredScale;
        target.scale.y = requiredScale;
    };


    /**
     * Scales a target, preserving its aspect relative to a source. The target's size will be changed,
     * so that its new height matches a calculated value (and the width is relatively scaled to match
     * the height). The new height will be a percentage of a target rectangle's height (or a percentage
     * of the height of the screen, if no target rectangle is provided).
     * @public
     * @param target
     * Display object instance whose size is to be adjusted.
     * @param {number} scale
     * A percentage value (where 100 == 100%), representing the percentage of the target rectangle's
     * height that you want to set the target's width (and relative scale) to.
     * @param {{height:number}} [sourceRectangle]
     * Display object or rectangle
     */
    propHeightScale(target, scale, sourceRectangle=undefined)
    {
        if (sourceRectangle === undefined)
        {
            sourceRectangle = this._rect;
        }

        let requiredScale = sourceRectangle.height * scale/100 / target.height * target.scale.y;
        target.scale.x = requiredScale;
        target.scale.y = requiredScale;
    };


    /**
     * Horizontally aligns a target relative to a source rectangle's left position. If no source
     * rectangle is provided, then screen-space is used instead. An offset parameter can be used,
     * to push the target display object a certain distance to the right. For example, if the source
     * rectangle has an x position of 100, and a width of 1000, and a value of 20 is passed, then the
     * target display object will be positioned at (100 + (1000 * 0.02)) - 120 (exactly 20% across the
     * sourceRectangle, from the left). Please note, this method does not really take into account true
     * left bounds of the sourceRectangle, it only uses its x position (so if "x" and "left" are not
     * the same, the positioning uses x)
     * @public
     * @param {} targetDisplayObject
     * Target display object to horizontally align.
     * @param {number} offsetFromLeft
     * The proportional distance (as a perctange value, where 100 == 100%) to offset the display
     * object to the right, from the x position of sourceRectangle.
     * @param {{x:number,y:number,width:number,height:number}} [sourceRectangle]
     * A rectangle, or display object, to use as the area in which targetDisplayObject is positioned.
     */
    horizontalAlignLeft(targetDisplayObject, offsetFromLeft=0, sourceRectangle=undefined)
    {
        if (sourceRectangle === undefined)
        {
            sourceRectangle = this._rect;
        }
        targetDisplayObject.x = sourceRectangle.x + (sourceRectangle.width * offsetFromLeft / 100);
    };


    /**
     * Horizontally aligns a target relative to a source rectangle's rightt position. If no source
     * rectangle is provided, then screen-space is used instead. An offset parameter can be used,
     * to push the target display object a certain distance to the right. For example, if the source
     * rectangle has a y position of 100, and a width of 1000, and a value of 20 is passed, then the
     * target display object will be positioned at (100 + (1000 * 0.98)) - 9900 (exactly 20% from the
     * right edge of sourceRectangle). Please note, this method does not really take into account true
     * right bounds of the sourceRectangle, it only uses its x position (so if "x" and "right" are not
     * the same, the positioning uses x)
     * @public
     * @param {} targetDisplayObject
     * Target display object to horizontally align.
     * @param {number} offsetFromRight
     * The proportional distance (as a perctange value, where 100 == 100%) to offset the display
     * object to the left, from the x position of sourceRectangle.
     * @param {{x:number,y:number,width:number,height:number}} [sourceRectangle]
     * A rectangle, or display object, to use as the area in which targetDisplayObject is positioned.
     */
    horizontalAlignRight(targetDisplayObject, offsetFromRight=0, sourceRectangle=undefined)
    {
        if (sourceRectangle === undefined)
        {
            sourceRectangle = this._rect;
        }
        targetDisplayObject.x = sourceRectangle.x + sourceRectangle.width * (100 - offsetFromRight) / 100 - targetDisplayObject.width;
    };


    /**
     * Horizontally align center a target relative to a source
     * @public
     * @param target - display object
     * @param distance - distance in percent
     * @param src - display object or rectangle
     */
    horizontalAlignCenter(target, distance=0, src=undefined)
    {
        if (src === undefined)
        {
            src = this._rect;
        }
        else if (src == target.parent)
        {
            let width = src.width/src.scale.x;
            let height = src.height/src.scale.y;
            let x = src.anchor !== undefined ? src.anchor.x * width : 0;
            let y = src.anchor !== undefined ? src.anchor.y * height : 0;
            src = new PIXI.Rectangle(x, y, width, height);
        }

        let taX = target.anchor ? target.anchor.x : 0;
        let saX = src.anchor ? src.anchor.x * 100 : 0;

        target.x = src.x + src.width * (50 + distance - saX)/100 - target.width * (.5 - taX) ;
    }


    /**
     * Vertically align top a target relative to a source
     * @public
     * @param target - display object
     * @param distance - distance in percent
     * @param src - display object or rectangle
     */
    verticalAlignTop(target, distance=0, src=undefined)
    {
        if (src === undefined)
        {
            src = this._rect;
        }
        target.y = src.y + src.height * (distance)/100;
    }


    /**
     * Vertically align middle a target relative to a source
     * @public
     * @param target - display object
     * @param distance - distance in percent
     * @param src - display object or rectangle
     */
    verticalAlignMiddle(target, distance=0, src=undefined)
    {
        if (src === undefined)
        {
            src = this._rect;
        }
        else if (src === target.parent && src.anchor)
        {
            src = new PIXI.Rectangle(0, 0, src.width/src.scale.x, src.height/src.scale.y);
        }
        
        let aY = target.anchor ? target.anchor.y : 0;

        target.y = src.y + src.height * (50 + distance)/100 - target.height * (.5 - aY);
    }


    /**
     * Vertically align bottom a target relative to a source
     * @public
     * @param target - display object
     * @param distance - distance in percent
     * @param src - display object or rectangle
     */
    verticalAlignBottom(target, distance=0, src=undefined)
    {
        if (src === undefined)
        {
            src = this._rect;
        }
        target.y = src.y + src.height * (100-distance)/100 - target.height;
    }


    /**
     * Return the coordinates of a global point as percentage of the layout rectangle
     * @public
     * @param globalPt - global point
     * @return {PIXI.Point}
     */
    getRelCoords(globalPt)
    {
        let relX = globalPt.x * 100 / this.deviceWidth;
        let relY = globalPt.y * 100 / this.deviceHeight;

        let rect = this.landscape ? this._rect : this.portraitRect;

        let ptX = relX/100 * rect.width;
        let ptY = relY/100 * rect.height;

        return new PIXI.Point(ptX, ptY);
    }


    /**
     * Apply layout to children of a container spacing them equally on the vertical
     * @public
     * @param {PIXI.Container} container
     * @param {number} vStep
     * "the distance between the children"
     */
    applyVerticalLayout(container, vStep)
    {
        let numChildren = container.children.length;
        let totalHeight = 0;
        let rect = new PIXI.Rectangle(0,0,container.width, container.height);
        for (let childIdx=0; childIdx<numChildren; childIdx++)
        {
            let child = container.getChildAt(childIdx);
            this.horizontalAlignCenter(child, 0, rect);
            child.y = totalHeight;
            totalHeight += (child.height + vStep);
        }
    }


    /**
     * Apply layout to children of a container spacing them equally on the horizontal
     * @public
     * @param {PIXI.Container} container
     * @param {number} hStep
     * "The distance between the children"
     */
    applyHorizontalLayout(container, hStep)
    {
        let numChildren = container.children.length;
        let totalWidth = 0;
        let rect = new PIXI.Rectangle(0, 0, container.width, container.height);
        for (let childIdx=0; childIdx < numChildren; childIdx++)
        {
            let child = container.getChildAt(childIdx);
            child.x = totalWidth;
            this.verticalAlignMiddle(child, 0, rect);
            totalWidth += (child.width + hStep);
        }
    }


    /**
     * Set size and position of an object using a rectangle as coordinate system.
     * Aspect is not preserved
     * @public
     * @param object - object to position and size
     * @param x - x coord
     * @param y - y coord
     * @param w - width
     * @param h - height
     * @param [rect] - source rectangle
     */
    setToRectangle(object, x, y, w, h, rect=undefined)
    {
        rect = rect === undefined ? this._rect : rect;

        object.x = rect.x + x/100 * rect.width;
        object.y = rect.y + y/100 * rect.height;
        object.width = w/100 * rect.width;
        object.height = h/100 * rect.height;
    };


    /**
     * Fit an object inside a rectangle
     * @public
     * @param object - object to fit
     * @param {PIXI.Rectangle} rect
     * For example Rectangle(0,0,100,100)
     * means the object will be fit to the full bounds of the source. (0,50,100,50) - bottom half,, etc.
     * @param {} [src]
     * "source object"
     */
    putInRect(object, rect, src)
    {
        let width, height;
        if (src == object.parent)
        {
            width = Math.abs(src.width/src.scale.x);
            height = Math.abs(src.height/src.scale.y);
            let anchor;
            if (src.anchor)
            {
                anchor = {x: src.anchor.x, y: src.anchor.y};
            }
            else
            {
                anchor = {x: 0, y: 0};
            }
            src = new PIXI.Rectangle(0, 0, width, height);
            src.anchor = anchor;
        }
        else
        {
            width = Math.abs(src.width);
            height = Math.abs(src.height);
        }

        let modRect = new PIXI.Rectangle();
        modRect.x = src.x + rect.x / 100 * width;
        modRect.y = src.y + rect.y / 100 * height;

        if (src.anchor)
        {
            modRect.x -= width * src.anchor.x;
            modRect.y -= height * src.anchor.y;
        }

        modRect.width = width * rect.width / 100;
        modRect.height = height * rect.height / 100;

        if (object.scale)
        {
            object.scale.set(1);
        }

        if (object.width / object.height > modRect.width / modRect.height)
        {
            let scale = modRect.width / object.width;
            object.scale.set(scale, scale);
            object.x = modRect.x;
            object.y = modRect.y + (modRect.height - object.height) *.5;
        }
        else
        {
            let scale = modRect.height / object.height;
            object.scale.set(scale, scale);
            object.x = modRect.x + (modRect.width - object.width) *.5;
            object.y = modRect.y;
        }

        if (object.anchor)
        {
            object.x += object.anchor.x * object.width;
            object.y += object.anchor.y * object.height;

        }
    }


    /**
     * Limit the size of an object. Preserves aspect
     * @public
     * @param object
     * @param {number} width
     * "max width"
     * @param {number} height
     * "max height"
     * @param {PIXI.Rectangle} [rect]
     * Optional source rect to use for coord system
     */
    limitSize(object, width, height, rect=undefined)
    {
        if (rect === undefined)
        {
            rect = this._rect;
        }

        if (object.scale)
        {
            object.scale.set(1);
        }

        let scale = Math.min(1, Math.min(this.relWidth(width, rect) / object.width , this.relHeight(height, rect) / object.height));
        object.scale.set(scale, scale);

    };


    /**
     * Sets the width of a display object, to a specific value in pixels. Height will be proportionately
     * scaled, to match the new width value.
     * 
     * NOTE: This method assumes that existing width / height are already correctly proportional: if they
     * have been changed from their "correct" asset sizes, this may not be the method that you want to use.
     * @public
     * @param {{width:number,height:number}} targetDisplayObject
     * The display object to set width for.
     * @param {number} targetWidth
     * The width (in pixels) to set the display object's width to.
     */
    setWidth(targetDisplayObject, targetWidth)
    {
        targetDisplayObject.height = targetWidth / targetDisplayObject.width * targetDisplayObject.height;
        targetDisplayObject.width = targetWidth;
    };


    /**
     * Sets the height of a display object, to a specific value in pixels. Width will be proportionately
     * scaled, to match the new height value.
     * 
     * NOTE: This method assumes that existing width / height are already correctly proportional: if they
     * have been changed from their "correct" asset sizes, this may not be the method that you want to use.
     * @public
     * @param {{width:number,height:number}} targetDisplayObject
     * The display object to set height for.
     * @param {number} targetHeight
     * The height (in pixels) to set the display object's height to.
     */
    setHeight(targetDisplayObject, targetHeight)
    {
        targetDisplayObject.width = targetHeight / targetDisplayObject.height * targetDisplayObject.width;
        targetDisplayObject.height = targetHeight;
    };


    /**
     * Sets the width or height of a display object, to a specific value in pixels. Proportion of the
     * display object will be preserved. The display object will either be set to the value of maxWidth
     * (as long as height does not exceed the maxHeight), or to the value of maxHeight. Width is used
     * as first priority.
     * 
     * NOTE: This method assumes that existing width / height are already correctly proportional: if they
     * have been changed from their "correct" asset sizes, this may not be the method that you want to use.
     * @public
     * @param {{width:number,height:number}} targetDisplayObject
     * @param {number} maxWidth
     * The max width (in pixels) to set the display object's width to.
     * @param {number} maxHeight
     * The max height (in pixels) to set the display object's height to.
     */
    setWidthOrHeightToMax(targetDisplayObject, maxWidth, maxHeight) {
        this.setWidth(targetDisplayObject, maxWidth);
        if (targetDisplayObject.height > maxHeight) {
            this.setHeight(targetDisplayObject, maxHeight);
        }
    };


    /**
     * Sets the width or height of a display object, to a specific value in pixels. Proportion of the
     * display object will be preserved. The display object will either be set to the value of maxWidth
     * (as long as height does not exceed the maxHeight), or to the value of maxHeight. Height is used
     * as first priority.
     * 
     * NOTE: This method assumes that existing width / height are already correctly proportional: if they
     * have been changed from their "correct" asset sizes, this may not be the method that you want to use.
     * @public
     * @param {{width:number,height:number}} targetDisplayObject
     * @param {number} maxWidth
     * The max width (in pixels) to set the display object's width to.
     * @param {number} maxHeight
     * The max height (in pixels) to set the display object's height to.
     */
    setHeightOrWidthToMax(targetDisplayObject, maxWidth, maxHeight) {
        this.setHeight(targetDisplayObject, maxHeight);
        if (targetDisplayObject.width > maxWidth) {
            this.setWidth(targetDisplayObject, maxWidth);    
        }
    };


    /**
     * "Fixes dimensions of Dragon Bones object so it can be handled as any other container
     * returns PIXI.Container with the armature inside". This appears to be required, because by default,
     * Dragonbones (or perhaps it is the dragonbones lib we use) sets anchor as bottom right of the
     * armature. The default call to this method, adjusts the positioning of the armature, so that it is
     * top left anchored (as is default for pixi display objects). 2 optional parameters, allow you to
     * specify an anchor position, in the standard way.
     * @public
     * @param {} armature
     * A dragonBones armature
     * @param {number} [anchorX]
     * Specifices horizontal anchor - in the range 0 to 1. Value of 0 == left anchored, 1 == right
     * anchored, 0.5 == center anchored. Defaults to 0 (left anchored)
     * @param {number} [anchorY]
     * Specifices vertical anchor - in the range 0 to 1. Value of 0 == top anchored, 1 == bottom
     * anchored, 0.5 == center anchored. Defaults to 0 (top anchored)
     * @return {PIXI.Container}
     */
    createDragonBonesContainer(armature, anchorX=0, anchorY=0)
    {
        let cont = new PIXI.Container();
        cont.addChild(armature);

        /** @type {PIXI.Rectangle} */
        let localRect = armature.getLocalBounds();
        armature.x = -localRect.x - (localRect.width * anchorX);
        armature.y = -localRect.y - (localRect.height * anchorY);

        return cont;
    };


    /**
     * Fits a display object into an area. The Display object passed will first be scaled to fit the
     * area, maintaining its proportion (but being as large as possible). It will then be centered
     * both vertically and horizontally within that area. The rectangle that it is fitted to must be
     * specified as a percentage (eg: the rectangle represents a proportional area)
     * @public
     * @param {*} displayObject 
     * The Display Object to fit & center in an area. It will be manually scaled, so that it becomes
     * the largest size possible that fits in the area, and will be horizontally and vertically
     * centered in that area. Its pivot will not be adjusted.
     * @param {{x:number,y:number,width:number,height:number}} area
     * The area to fit in to. This is a proportional rectangle, where (0,0,100,100) would mean "the
     * whole of screen space". Each value is a percentage (in the range 0 to 100), and represents 
     * the percentage of that dimension, relative to src.
     * @param {{x:number,y:number,width:number,height:number}} [src]
     * An optional source rectangle: the "area" parameter will be treated as a proportion of this.
     * If no parameter is passed, then the method defaults to using the full dimensions of the screen.
     */
    fitAndCenterDisplayObjectInProportionalArea(displayObject, area, src)
    {
        let startX = this.relX(area.x, src);
        let startY = this.relY(area.y, src);
        let maxWidth = this.relWidth(area.width, src);
        let maxHeight = this.relHeight(area.height, src);

        let srcWidth = displayObject.width / displayObject.scale.x;
        let srcHeight = displayObject.height / displayObject.scale.y;
        let possNewScaleW = maxWidth / srcWidth;
        let possNewScaleH = maxHeight / srcHeight;
        let newScaleActual = Math.min(possNewScaleW, possNewScaleH);
        let displayObjectFinalW = srcWidth * newScaleActual;
        let displayObjectFinalH = srcHeight * newScaleActual;
        let displayObjectFinalPosX = startX + ((maxWidth - displayObjectFinalW) * 0.5);
        let displayObjectFinalPosY = startY + ((maxHeight - displayObjectFinalH) * 0.5);

        displayObject.scale.x = newScaleActual;
        displayObject.scale.y = newScaleActual;
        displayObject.x = displayObjectFinalPosX;
        displayObject.y = displayObjectFinalPosY;
    }
}

export default Layout;