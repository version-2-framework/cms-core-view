import TextField from "../ui/TextField";
import C_GameEvent from "../../../cms-core-logic/src/const/C_GameEvent";

import * as LogManager from "../../../cms-core-logic/src/logging/LogManager";

const log = LogManager.getGameViewLogger();


/**
 * Assets factory.
 * Factory class for creation of Sprites, Text etc.
 */
class Assets
{

    constructor()
    {
        this.app = PIXI.app;

        /**
         * A map of the animation textures for "normal" symbol state, registered by symbol id.
         * @private
         * @type {{ [symbolId:number]:PIXI.Texture[] }}
         */
        this._symAnimTextures = {};

        /**
         * A map of the animation textures for the winline symbol animations, registed by symbol id.
         * @private
         * @type {{ [symbolId:number]:PIXI.Texture[] }} 
         */
        this._symWinlineAnimTextures = {};

        /**
         * A map of tall symbol ids and animation textures with specific numbers of symbols.
         * @private
         * @type {{ [symbolId:number]:{ [numSym:number]:PIXI.Texture[] } }}
         */
        this._tallSymAnimTextures = {};

        // TODO: Document correctly ("padding" is a poor name i think)
        /**
         * @private
         * @type {number}
         */
        this._padding = this.app.layout.relWidth(this.app.config.tfPadding);
    }


    // TODO: Public, private, why is it used, etc
    /**
     * @public
     */
    onResourcesLoaded()
    {
        this.resources = PIXI.loader.resources;
        this.validateLoadedAssets();
        this.initializeSymbolAnimations();
        this.loadArmatures();
    };


    /**
     * Executes a validation process on loaded assets.
     * @private
     */
    validateLoadedAssets()
    {
        let emptyAssetKeys = [];

        for (let assetKey in this.resources)
        {
            if (!this.resources[assetKey]) {
                emptyAssetKeys.push(assetKey);
            }
        }

        if (emptyAssetKeys.length > 0)
        {
            log.warn(`Assets.validateLoadedAssets: found ${emptyAssetKeys.length} empty asset keys`);
            log.warn(emptyAssetKeys.toString());
        }
        else
        {
            log.info(`Assets.validateLoadedAssets: no empty asset keys found`);
        }
    };


    /**
     * Loads up any dragon bones armatures that were configured for the game.
     * @private
     */
    loadArmatures()
    {
        let dragonBonesAssetConfigs = this.app.bootLoader.resources.atlases.data.dragonBones;
        if (dragonBonesAssetConfigs)
        {
            dragonBonesAssetConfigs.forEach(assetConfig =>
            {
                let assetRootName = assetConfig.name;

                this.app.dragonBones.parseDragonBonesData(PIXI.loader.resources[`${assetRootName}_ske`].data);
                this.app.dragonBones.parseTextureAtlasData(PIXI.loader.resources[`${assetRootName}_tex`].data, PIXI.loader.resources[assetRootName].texture);
            });
        }
    };


    /**
     * Return the array of animation textures for a symbol with specified id.
     * @public
     * @param symId {number}
     * The id of the symbol
     * @return {PIXI.Texture[]}
     * Ordered sequence of PIXI.Textures
     */
    getSymAnimTextures(symId)
    {
        return this._symAnimTextures[symId];
    };


    /**
     * Returns the array of animation textures for a Tall Symbol with specified id, and a
     * specific number of symbols in the Tall Symbol art asset.
     * @public
     * @param {number} symbolId 
     * @param {number} numSymInTallSymAsset
     * @return {PIXI.Texture[]}
     * Ordered sequence of PIXI.Textures.
     */
    getTallSymAnimTextures(symbolId, numSymInTallSymAsset)
    {
        return this._tallSymAnimTextures[symbolId][numSymInTallSymAsset];
    };

    /**
     * Return the array of winline animation textures for a symbol with specified id.
     * @public
     * @param symId {number}
     * The id of the symbol
     * @return {PIXI.Texture[]}
     * Ordered sequence of PIXI.Textures
     */
    getWinlineSymAnimTextures(symId)
    {
        return this._symWinlineAnimTextures[symId];
    }


    // TODO: This approach is potentially prone to failure, if we ever get assets with
    // overlapping names. In an ideal world, we would have textures pre-sorted into their
    // own asset bucket (this is how Starling's AssetManager handled things). PIXI's
    // resourceLoader is very basic (i think their developers even constantly warn of this,
    // and generally advise that you should use your own implementation if you want to do
    // anything more sophisticated). The ability to pre-sort loaded assets as textures,
    // sounds, general json files, would help make the loading / asset validation process
    // more robust, and potentially a little quicker to grab textures at runtime as well.
    /**
     * Return a texture with specified id. The id is based on the file name of the image file.
     * The texture atlases are searched consecutively until the image is found
     * The file suffix (.png) is added automatically;
     * Returns null if texture is not found
     * @public
     * @param textureId {string}
     * @return {PIXI.Texture | null}
     */
    getTexture(textureId)
    {
        let texture = null;

        for (let resourceName in this.resources)
        {
            let resource = this.resources[resourceName];

            if (resource)
            {
                // Should be a texture atlas. Unless it's a json with property
                // "textures", in which case, this will fail.
                if (resource.textures)
                {
                    texture = resource.textures[textureId];
                }
                // Seems to be an actual asset with the same name as our textureId.
                // Unless we loaded some other sound or config file with a matching
                // name to our target texture, in which case this will fail.
                else
                if (resourceName == textureId)
                {
                    texture = resource.texture;
                }

                if (texture != undefined)
                {
                    break;
                }
            }
        }
        
        if (!texture)
        {
            texture = null;
            log.error(`Could not find texture : ${textureId}`);
        }

        return texture;
    };


    /**
     * Get a Sprite which uses texture with a specified id
     * @public
     * @param {string} textureId
     * @returns {PIXI.Sprite}
     */
    getSprite(textureId)
    {
        return new PIXI.Sprite(this.getTexture(textureId));
    };
    

    /**
     * Factory method for creating text fields.
     * @public
     * @param text {string}
     * The initial text to be displayed on the text field.
     * @param size {number}
     * Size of the font (in pixels)
     * @param font {string}
     * @param color {string | number | string[] | number[]}
     * The colour that the text should be - alternatively, supply an array of values, in order to
     * get a gradient effect on the text.
     * @param lineHeight {number}
     * Height of a single line, in pixels.
     * @param wrapWidth {number}
     * Text wrap width (in pixels)
     * @param additionalStyle {object}
     * Additional style variables - TODO: Actually document what this means
     * @param padding {Number} padding around the text that sometimes causes visual cutoffs. Increase the number to decrease the cutting
     * It will use the value of configurable object (this.padding) in the constructor - unless it is overridden
     * @returns {PIXI.Text}
     */
    getTF(text, size, font = null, color = null, lineHeight = null, wrapWidth = null, additionalStyle = null, padding = this._padding)
    {
        font = font == null ? this.app.config.defaultFont : font;
        color = color == null ? this.app.config.defaultFontColor : color;
        size = Math.round(size);

        let styleVars = {fontFamily: font, fontSize: size, fill: color, align: 'center'};

        for (let stylePropName in additionalStyle)
        {
            styleVars[stylePropName] = additionalStyle[stylePropName];
        }

        if (lineHeight != null)
        {
            styleVars.lineHeight = Math.round(size * lineHeight);
        }

        if (wrapWidth != null)
        {
            styleVars.wordWrap = true;
            styleVars.wordWrapWidth = wrapWidth;
        }

        styleVars.padding = padding;

        return new TextField(text,  new PIXI.TextStyle(styleVars));
    };


    /**
     * Initialize the assets controller. This method actually fetches symbol animations data, and
     * pre-generates textures (and additional meta-data objects) for each symbol animation.
     * 
     * @todo:
     * A PIXI plugin method ("renderer.plugins.prepare.upload") is called: presumably this means
     * that the animations are in some way "pre-loaded", although it isn't clear if this is true,
     * or why this would be required. Fully explaining this is an important part of documenting why
     * this init method needs to exist at all, and what it is actually doing.
     * @private
     */
    initializeSymbolAnimations()
    {
        const numSyms = this.app.config.symbolIds.length;
        
        /** @type {SymbolsConfiguration} */
        const symbolsConfig = this.app.config.symbolsConfig;
        
        for (let symIdx = 0; symIdx < numSyms; symIdx++)
        {
            let symFrames = null;
            let symId = this.app.config.symbolIds[symIdx];
            let symData = symbolsConfig.perSymbol[symId];

            if (symData)
            {
                let symAnimData = symData.winAnimation;
                
                if (symAnimData && symAnimData.type === "animated")
                {
                    let startFrame = symAnimData.startFrame;
                    let finalFrame = symAnimData.finalFrame;
                    let padLen = symAnimData.padLen;
                    symFrames = this.generateFrames(`sym${symId}_anim_`, padLen, startFrame, finalFrame);
                }


                // If not animated, fetch the default symbol frame instead
                else
                {
                    symFrames = [this.getTexture(`sym${symId}`)];
                }

                // TODO: Restore (and document!!) the fade effect, here.

                this._symAnimTextures[symId] = symFrames;
                let anim = new PIXI.extras.AnimatedSprite(symFrames);
                this.app.renderer.plugins.prepare.upload(anim);


                let symWinlineData = symData.winlineAnimation;

                if (symWinlineData && symWinlineData.type === "animated")
                {
                    let startFrame = symWinlineData.startFrame;
                    let finalFrame = symWinlineData.finalFrame;
                    let padLen = symWinlineData.padLen;
                    let winlineSymFrames = this.generateFrames(symWinlineData.prefix, padLen, startFrame, finalFrame);
                    this._symWinlineAnimTextures[symId] = winlineSymFrames;
                    let anim = new PIXI.extras.AnimatedSprite(winlineSymFrames);
                    this.app.renderer.plugins.prepare.upload(anim);
                }

                // TODO: Restore the fade effect, when i understand it !!
                /*
                if (symAnimData && (symAnimData.finalFrame || symAnimData.fade))
                {
                    let frames;
                    if (symAnimData.numFrames)
                    {
                        let startFrame = symAnimData.startFrame || 1;
                        let finalFrame = symAnimData.numFrames;
                        let padLen = symAnimData.padLen;
                        frames = this.generateFrames(`sym${symId}_anim_`, padLen, startFrame, finalFrame);
                    }
                    else if (symAnimData.fade)
                    {
                        frames = [this.getTexture(`sym${symId}_win`)];
                    }

                    this.symAnimTextures[symId] = frames;
                    let anim = new PIXI.extras.AnimatedSprite(frames);
                    this.app.renderer.plugins.prepare.upload(anim);
                }
                */
            }
            else
            {
                log.debug(`Assets: no perSymbolData defined for sym ${symId}`);
            }

            if (symFrames === null)
            {
                log.debug(`Assets: no anim data defined for sym ${symId}, generating default static frame`);

                symFrames = [this.getTexture(`sym${symId}`)];
            }
        }

        // Prepare animations for all tall symbols (whether wild or non-wild, 2, 3, 4, or infinity
        // symbols tall - its all handled in this one parsing block)
        if (symbolsConfig.tallSymPerSymbol)
        {
            let tallSymPerSymbol = symbolsConfig.tallSymPerSymbol;
            
            for (let tallSymbolId in tallSymPerSymbol)
            {
                let dataForCurrSymbol = tallSymPerSymbol[tallSymbolId];

                /** @type {{ [symbolId:number]:PIXI.Texture[] }} */
                let outputDataForCurrSymbol = {};

                // The keys of this object, represent the "number of symbols tall" for which
                // we have an explicit configuration available.
                for (let numSymKey in dataForCurrSymbol)
                {
                    if (dataForCurrSymbol[numSymKey].winAnimation)
                    {
                        let animData = dataForCurrSymbol[numSymKey].winAnimation;
                        if (animData.type === "animated")
                        {
                            let startFrame = animData.startFrame;
                            let finalFrame = animData.finalFrame;
                            let padLength  = animData.padLen;
                            let frameName  = `sym${tallSymbolId}_anim_tall${numSymKey}_`;
                            let frames = this.generateFrames(frameName, padLength, startFrame, finalFrame);

                            // TODO: This may no longer be the desirable way to access the symbol
                            // animation data for tall symbols. How I whish wiener had bothered
                            // to document what the "normal symbol" code was doing, so i could
                            // understand if its worth reproducing the same mechanics here.
                            outputDataForCurrSymbol[numSymKey] = frames;
                        }
                    }
                }

                this._tallSymAnimTextures[tallSymbolId] = outputDataForCurrSymbol;
            }
        }
    };


    /**
     * Generates an array of textures.
     * @public
     * @param prefix {string}
     * Texture prefix
     * @param padLen {number}
     * Number of symbols of padding of the numeric part (eg: if your original frame
     * name has a '001' for its number, then you need to pass a value of 3).
     * @param startNum {number}
     * The frame id of the first texture of the sequence (eg: if your first frame is
     * 'myFrame-001', then pass a value of 1).
     * @param endNum {number}
     * The frame id of the last texture of the sequence (eg: if your last frame is
     * 'myFrame-060', then pass a value of 60).
     * @return {PIXI.Texture[]}
     * Ordered array of PIXI.Textures, constituting the frame sequence for the animation.
     */
    generateFrames(prefix, padLen, startNum, endNum)
    {
        let frames = [];
        let frameNames = this.generateFrameNames(prefix, padLen, startNum, endNum);

        for (let frameIdx = 0; frameIdx < frameNames.length; frameIdx++)
        {
            frames.push(this.getTexture(frameNames[frameIdx]));
        }

        return frames;
    };


    /**
     * Generates an array of textures for an animation, from a list of frame ids.
     * @pubic
     * @param prefix {string}
     * Texture prefix
     * @param padLen {number}
     * Number of symbols of padding of the numeric part (eg: if your original frame
     * name has a '001' for its number, then you need to pass a value of 3).
     * @param {number[]} frameIds
     * Ordered list of frame ids.
     * @return {PIXI.Texture[]}
     * Ordered array of PIXI.Textures, constituting the frame sequence for the animation.
     */
    generateFramesForSequence(prefix, padLen, frameIds)
    {
        let frames = [];

        frameIds.forEach(frameId =>
        {
            let textureName = this.generateFrameName(prefix, padLen, frameId);
            let texture = this.getTexture(textureName);
            frames.push(texture);
        });

        return frames;
    };


    /**
     * Generates the names of frames - helper for generateFrames
     * @public
     * @param prefix {string}
     * Texture prefix
     * @param padLen {number}
     * number of symbols of padding of the numeric part (eg: if your original frame
     * name has a '001' for its number, then you need to pass a value of 3).
     * @param startNum {number}
     * The frame id of the first frame of the sequence (eg: if your first frame is
     * 'myFrame-001', then pass a value of 1).
     * @param endNum {number}
     * The frame id of the last texture of the sequence (eg: if your last frame is
     * 'myFrame-060', then pass a value of 60).
     * @return {string[]} Ordered sequence of frame names.
     */
    generateFrameNames(prefix, padLen, startNum, endNum)
    {
        let frameNames = [];
        for (let frameIdx = startNum; frameIdx <= endNum; frameIdx++)
        {
            frameNames.push(this.generateFrameName(prefix, padLen, frameIdx));
        }

        return frameNames;
    };


    /**
     * @private
     * @param prefix {string}
     * Texture prefix
     * @param padLen {number}
     * number of symbols of padding of the numeric part (eg: if your original frame
     * name has a '001' for its number, then you need to pass a value of 3).
     * @param {number} frameNum
     * Number of the frame
     * @return {string}
     * Name of the frame asset
     */
    generateFrameName(prefix, padLen, frameNum)
    {
        let frameString = String(frameNum);
        while (frameString.length < padLen)
        {
            frameString = '0' + frameString;
        }
        return `${prefix}${frameString}`;
    };


    // TODO: Document the strokeData parameter properly
    /**
     * Add stroke to a text field
     * @param {PIXI.Text} textField
     * The Pixi Text object to add a stroke to.
     * @param strokeData
     */
    addTFStroke(textField, strokeData)
    {
        if (strokeData)
        {
            textField.style.stroke = strokeData.strokeColor;
            let size = this.app.layout.relHeight(strokeData.strokeSize);
            textField.style.strokeThickness = size;
            textField.style.miterLimit  = 1;
        }
    };


    /**
     * Get scale nine sprite with a cut defined by its id
     * @public
     * @param {string} textureId
     */
    getS9Sprite(textureId)
    {
        let texture = this.getTexture(textureId);
        let rect = this.getS9Rect(textureId);
        return new PIXI.mesh.NineSlicePlane(texture, rect.x, rect.y, rect.width, rect.height);
    };


    /**
     * @public
     * @param {string} textureId 
     */
    getS9Rect(textureId)
    {
        let r;

        if (this.app.config.s9rects.hasOwnProperty(textureId))
        {
            let prop = this.app.config.s9rects[textureId];
            r = new PIXI.Rectangle(prop.x, prop.y, prop.width, prop.height);
        }
        else
        {
            log.error(`s9 rect ${textureId} is not defined. Check the s9Rects definitions.`);
        }

        return r;
    };


    /**
     * @typedef TextInputConfig
     * @property {number} [minValue]
     * @property {number} [maxValue]
     */


    // TODO: This might have to implement different behaviour for mobile
    /**
     * Creates a Html TextInput, which is set to a given area. The TextInput will reposition
     * itself into the target rect when the game resizes: because this involves adding non
     * standard event listeners, when you want to get rid of the TextInput, you must call
     * TextInput.destroy() (a custom method) in order to clear up these event listeners (or
     * else a memory leak will occur). This route was chosen, instead of allowing The Text
     * Input to listen out for its own "removed from DOM" event (because this second way of
     * doing it could be confusing if a user wanted to cache references to a textfield, add
     * or remove them from the dom at various points). The explicit destroy call was decided
     * to be a better fit.
     * 
     * No visual style is applied to the TextInput - the user of this method is free to apply
     * the visual styles to the object returned, using standard css.style.
     * @public
     * @param textInputArea {PIXI.Rectangle}
     * The area that the text input should sit in. The values here are all expressed as
     * percentages of screen dimensions (eg: x == 10, means "10% from the screen left").
     * @param relFontSize {number}
     * The relative size of the font for the text input.
     * @param {TextInputConfig} [textInputConfig]
     * Allows us to configure optional min and max values for the TextInput, which will be
     * enforced.
     * @returns {Element}
     */
    createTextInput(textInputArea, relFontSize, textInputConfig=null)
    {
        let input = document.createElement("input");
        input.type = "number";

        if (textInputConfig) {
            if (textInputConfig.minValue) {
                input.min = textInputConfig.minValue.toString();
            }

            if (textInputConfig.maxValue) {
                input.max = textInputConfig.maxValue.toString();
            }

            input.oninput = () => {
                let newValue = parseInt(input.value);

                if (textInputConfig.minValue && newValue < textInputConfig.minValue) {
                    log.debug(`user entered ${newValue}, which is too low: set value to ${textInputConfig.minValue}`);
                    input.value = textInputConfig.minValue.toString();
                }
                else
                if (textInputConfig.maxValue && newValue > textInputConfig.maxValue) {
                    log.debug(`user entered ${newValue}, which is too high: set value to ${textInputConfig.maxValue}`);
                    input.value = textInputConfig.maxValue.toString();
                }
            }
        }

        input.onkeydown = ()=>
        {
            let e = event || window.event;  // get event object
            let key = e.keyCode || e.which; // get key cross-browser

            if (key < 48 || key > 57) { //if it is not a number ascii code
                //Prevent default action, which is inserting character
                if (e.preventDefault) e.preventDefault(); //normal browsers
                e.returnValue = false; //IE
            }
        };
        
        document.body.appendChild(input);

        log.debug(`Assets.createTextInput(textInputArea:${JSON.stringify(textInputArea)})`);

        input.resizeHandler = () =>
        {
            let gameViewWidth = this.app.layout.getGameWidth();
            let gameViewHeight = this.app.layout.getGameHeight();
            let gameViewX = this.app.layout.getGamePosX();
            let gameViewY = this.app.layout.getGamePosY();

            log.debug(`stage.scale.x = ${this.app.stage.scale.x}`);
            log.debug(`stage.scale.y = ${this.app.stage.scale.y}`);
            
            log.debug(`input.resize: gameViewX = ${gameViewX}`);
            log.debug(`input.resize: gameViewY = ${gameViewY}`);
            log.debug(`input.resize: gameViewWidth = ${gameViewWidth}`);
            log.debug(`input.resize: gameViewHeight = ${gameViewHeight}`);
            
            let textInputX = gameViewX + (textInputArea.x / 100 * gameViewWidth);
            let textInputY = gameViewY + (textInputArea.y / 100 * gameViewHeight);
            let textInputW = textInputArea.width / 100 * gameViewWidth;
            let textInputH = textInputArea.height / 100 * gameViewHeight;
            let textInputFontSize = relFontSize / 100 * gameViewHeight;

            log.debug(`input.resize: textInput.x = ${textInputX}`);
            log.debug(`input.resize: textInput.y = ${textInputY}`);
            log.debug(`input.resize: textInput.width = ${textInputW}`);
            log.debug(`input.resize: textInput.height = ${textInputH}`);
            log.debug(`input.resize: textInput.fontSize = ${textInputFontSize}`);
            
            input.style.position = 'fixed';
            input.style.left = `${textInputX}px`;
            input.style.top = `${textInputY}px`;
            input.style.width = `${textInputW}px`;
            input.style.height = `${textInputH}px`;
            input.style.fontSize = `${textInputFontSize}px`;
            input.style.textAlign = 'center';
            input.style.padding = '0px';
            input.style.margin = '0px';
        };

        // When game resizes, re-size and position the text input
        this.app.dispatcher.on(C_GameEvent.RESIZE, input.resizeHandler);

        input.resizeHandler();

        // When TextInput is destroyed, clear the resize event listener,
        // or else we will get a memory leak
        input.destroy = () => {
            log.debug(`TextInput.destroy(): clearing up event listeners`);
            this.app.dispatcher.removeListener(C_GameEvent.RESIZE, input.resizeHandler);
        }

        return input;
    };


    /**
     * Checks if an asset, under a given id, exists.
     * @public
     * @param {string} id
     * The id under which the asset should be registered.
     * @return {boolean}
    */
    hasAsset(id)
    {
        return this.resources[id]? true : false;
    };


    /**
     * Returns any (root) asset
     * @public
     * @param {string} id
     * The id under which the asset is registered.
     * @return {Object | null}
     */
    getAsset(id)
    {
        return this.resources[id].data;
    };


    /**
     * Returns the Rules Config to use
     * @public
     * @return {Object}
     */
    getRulesConfig()
    {
        if (this.resources.rulesConfig) {
            return this.resources.rulesConfig.data;
        }
        else
        {
            // TODO: If no config file loaded, generate a simple, default rules object that can configure
            // the menu (nothing fancy will be shown - only basic data)
            return null;
        }
    }
}

export default Assets;