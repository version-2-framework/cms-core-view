/**
 * Utility methods for applying visual effects (eg: tints)
 */
class VisualUtil
{
    /**
     * Tween the color tint of an object. The color value is multiplied per pixel of the texture.
     * NOTE: if duration of the tween requested is 0, this wil skip creating a new tween (and
     * apply the tint immediately instead). Existing tint tweens on the target display object, will
     * be killed whenever this method is invoked.
     * @public
     * @param targetDisplayObject
     * The display object that will be tinted.
     * @param {number} endColour
     * The final tint colour to be applied.
     * @param {number} duration
     * The duration (in seconds) over which the tint should be applied. A value of zero would
     * apply the tint immediately.
     * @param {number} repeat
     * The number of times that the tint animation should be repeated. A value of 0 means that the
     * tint will be played only a single time (values larger than 1, indicate the number of repeats
     * that should be applied). This value is only meaningful, if you use a duration larger than 0
     * (a duration of 0 means that effectively no tint animation is actually applied).
     * @param {boolean} yoyo
     * Indicates whether a repeating tint should be "yo-yoed" back and forth. If false, then any
     * repeats on the tint would be played repeatedly, always starting at the same values, and always
     * moving forward to the same endpoint. If true, then the tween starts at start colours, moves to
     * final colours, and then reverses.
     */
    static tintTween(targetDisplayObject, endColour, duration, repeat = 0, yoyo = false)
    {
        if (targetDisplayObject.tint == endColour)
        {
            return;
        }

        if (targetDisplayObject.tintTween)
        {
            TweenMax.killTweensOf(targetDisplayObject.tintTween);
        }

        if (duration === 0)
        {
            targetDisplayObject.tint = endColour;
        }
        else
        {
            let finalColourR = endColour & 0xff0000 >> 16;
            let finalColourG = endColour & 0x00ff00 >> 8;
            let finalColourB = endColour & 0x0000ff;

            targetDisplayObject.tintTween =
            {
                r: ((targetDisplayObject.tint & 0xff0000) >> 16),
                g: ((targetDisplayObject.tint & 0x00ff00) >> 8),
                b: (targetDisplayObject.tint & 0x0000ff)
            };

            TweenMax.to(
                targetDisplayObject.tintTween,
                duration,
                {
                    r: finalColourR,
                    g: finalColourG,
                    b: finalColourB,
                    repeat: repeat,
                    yoyo: yoyo,
                    onUpdate: VisualUtil.setTint,
                    onUpdateParams: [targetDisplayObject],
                    onUpdateScope: VisualUtil,
                    onComplete: VisualUtil.endTintTween,
                    onCompleteParams: [targetDisplayObject],
                    onCompleteScope: VisualUtil
                });
        }
    };


    /**
     * @private
     * @param targetDisplayObject
     * The display object upon which the tint is being applied.
     */
    static setTint (targetDisplayObject)
    {
        let c = targetDisplayObject.tintTween;
        targetDisplayObject.tint = (c.r << 16) | (c.g << 8) | c.b;
    };


    /**
     * @private
     * @param targetDisplayObject
     * The display object upon which the tint is being applied (and for which a tint tween must
     * be ended)
     */
    static endTintTween (targetDisplayObject)
    {
        delete targetDisplayObject.tintTween;
    };
}

export default VisualUtil