/**
 * Mathematical utility functions.
 */
class MathUtil
{


    /**
     * Return integer between min and max
     * @public
     * @param min
     * @param max
     * @returns {*}
     */
    static getRandomInt(min, max)
    {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    /*
    * Returns random Boolean
    * @public
    * */
    static getRandomBool()
    {
        return Boolean(Math.round(Math.random()));
    }

    /**
     * Returns minVal if value < minVal , maxVal if value > maxVal, unchanged value otherwise
     * @public
     * @param value
     * @param minVal
     * @param maxVal
     * @returns {number}
     */
    static clamp(value, minVal, maxVal)
    {
        return (Math.max(minVal, Math.min(value, maxVal)));
    }


    /**
     * Calculates the mean value of an array of numbers
     * @public
     * @param a {Array} - array of numbers
     * @returns {number}
     */
    static average(a)
    {
        let sum = 0;
        for (let i = 0; i < a.length; i++)
        {
            sum += a[i];
        }
        return sum/a.length;
    }


}

export default MathUtil