/*
* Particle Emitter
* @class
* @extends PIXI.Container
*
* Example config file:
* {
                textures: [textures],

                config:
                    {
                        "alpha": {"start": 1, "end": 1},
                        "scale": {"start": 1, "end": 1, "minimumScaleMultiplier": 1},
                        "color": {"start": "#ffffff", "end": "#ffffff"},
                        "speed": {"start": 500, "end": 500, "minimumSpeedMultiplier": 1},
                        "acceleration": {"x": 0, "y": 0},
                        "maxSpeed": 0,
                        "startRotation": {"min": -280, "max": -250},
                        "noRotation": false,
                        "rotationSpeed": {"min": 0, "max": 50},
                        "lifetime": {"min": 3.5, "max": 4},
                        "blendMode": "normal",
                        "frequency": 0.016,
                        "emitterLifetime": -1,
                        "maxParticles": 500,
                        "pos": {"x": 0, "y": 0},
                        "addAtBack": false,
                        "spawnType": "rect",
                        "spawnRect": {"x": 0, "y": 0, "w": 0, "h": 0}
                    },

                particleConstructor: PIXI.particles.Particle
            };
*
* */
import BaseView from "../views/BaseView";

export default class ParticleEmitter extends BaseView
{

    /**
     * Constructs a new Particle Emitter.
     * @param {ParticleEmitterConfig} config
     * Configuration for the emitter
     * @param {boolean} emitFromStart
     * Indicates if the emitter should start emitting immediately at the moment of construction.
     * When false, will only start when "start" (or another method) is invoked.
    */
    constructor(config, emitFromStart=true)
    {
        super();

        // TODO: This container seems completely pointless, considering that ParticleEmitter
        // extends from BaseView - so it is ALREADY a container. I think we can safely dump
        // this.
        let container = new PIXI.Container();

        // Create a new emitter
        /**
         * @private
         */
        this.emitter = new PIXI.particles.Emitter
        (

            // The PIXI.Container to put the emitter in
            // if using blend modes, it's important to put this
            // on top of a bitmap, and not use the root stage Container
            container,

            // The collection of particle images to use
            config.textures,

            // Emitter configuration, edit this to change the look of the emitter
            config.config
        );
        
        // If a particleType is explicitly set, then we are using one of the newer, explicit
        // configs, and we can infer particle constructor explicitly. Otherwise, we will check
        // for a custom field (for backwards compatibility on older games), or use a default
        // value. Note that older games may have set a particleContsructor, but in fact, the
        // libs before version 0.11 didn't use the particleConstructor field passed in.
        let particleConstructor = PIXI.particles.Particle;

        if (config.particleType)
        {
            // TODO: Path particles no filly supported yet, as i don't know how they work!
            // Russell, 18.03.2021
            if (config.particleType === "path") {
                particleConstructor = PIXI.particles.PathParticle;
            }
            else
            if (config.particleType === "animated") {
                particleConstructor = PIXI.particles.AnimatedParticle;
            }
            else
            if (config.particleType === "simple") {
                particleConstructor = PIXI.particles.Particle;
            }
        }
        else
        if (config.particleConstructor)
        {
            particleConstructor = config.particleConstructor;
        }
        else
        {
            particleConstructor = PIXI.particles.Particle;
        }

        this.emitter.particleConstructor = particleConstructor;

        this.emitter.emit = emitFromStart;

        this.addChild(container);
    }

    /**
     * Start emitting particles
     * @public
     */
    start()
    {
        this.emitter.emit = true;
    };


    /**
     * Stop emitting particles
     * @public
     */
    stop()
    {
        this.emitter.emit = false;
    };


    /**
     * Plays the particles once. An optional callback can be invoked when particles are finished.
     * @public
     * @param {() => void} [onComplete]
     */
    playOnce(onComplete)
    {
        this.emitter.playOnce(onComplete);
    };


    /**
     * @public
     * @inheritDoc
     */
    destroy()
    {
        super.destroy(true);
    };
}