/**
 * Provides translations for different languages.
 * @implements {LocaleApi}
 */
class Locale
{
    /**
     * @param {string} language 
     * @param {PIXI.Loader} loader 
     */
    constructor(language, loader)
    {
        /**
         * @private
         */
        this._app = PIXI.app;

        /**
         * Language in use in the game client.
         * @private
         * @type {string}
         */
        this._language = language;

        /**
         * @private
         */
        this._dictionaries = {};

        let commonLocalizations = loader.resources[`locale_common_${language}`].data;
        let licenseeLocalizations = loader.resources[`locale_licensee_${language}`].data;
        let gameLocalizations = loader.resources[`locale_game_${language}`].data;
        let combinedLocalizations = Object.assign({}, commonLocalizations, licenseeLocalizations, gameLocalizations);
        this._dictionaries[language] = combinedLocalizations;
    };


    /**
     * Get the translation for a string with the specified identifier. If no translation is
     * available under the localizationId passed, then the value returned will be the localization
     * id.
     * @inheritDoc
     * @param {string} localizationId
     * The string key under which the required localization is stored
     * @param {{[id:string]:string}} [localizationSubstitutions]
     * An optional set of keys to be substituted in the output localization. For example, if
     * the raw text stored under localization id is "Credit remaining : [CREDIT]", the [CREDIT] key
     * can be substituted using this parameter (eg: by passing {"[CREDIT]":"$1200.00"}). This allows
     * for dynamic substitution of values in different languages, including cases where word order
     * may change (or where the key to substitute may not be present in all available localizations).
     * @returns {string}
     */
    getString(localizationId, localizationSubstitutions = undefined)
    {
        let translation = this._dictionaries[this._language][localizationId];

        if (!translation)
        {
            translation = localizationId;
        }

        if (localizationSubstitutions)
        {
            for (let paramName in localizationSubstitutions)
            {
                translation = translation.replace(paramName, localizationSubstitutions[paramName]);
            }
        }

        return translation;
    };
}


export default Locale;