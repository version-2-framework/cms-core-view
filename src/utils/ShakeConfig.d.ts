interface ShakeInProgressProperties
{
    target : PIXI.DisplayObject;

    startPosX : number;
    
    startPosY : number;

    currAmplitudeX : number;

    currAmplitudeY : number;

    amplitudeMultiplier : number;

    tween : gsap.TweenMax;
}