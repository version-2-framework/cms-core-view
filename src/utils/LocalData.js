import {getAppLogger} from "../../../cms-core-logic/src/logging/LogManager";

const log = getAppLogger();

/**
 * Responsible for storing data locally.
 */
class LocalData
{
    constructor()
    {
        /**
         * @private
         */
        this.app = PIXI.app;

        /**
         * The string prefix, pre-pended before all keys. The "v2" prefix exists, because
         * some game clients may have legacy ids (so we use this to make it distinct).
         * @private
         * @type {string}
         */
        this._keyPrefix = `v2_${this.app.config.gameId}`;
    };


    /**
     * Generates the fully qualified key for a specific stored value.
     * @private
     * @param {string} valueId 
     * The unique string id associated with a specific stored value.
     * @return {string}
     * The fully qualified string key, under which that value will be stored.
     */
    getFullKey(valueId)
    {
        return `${this._keyPrefix}${valueId}`;
    };


    /**
     * Saves a simple value under a specific identifier. The value can be retrieved
     * using any of the primitive methods - getStringValue, getBooleanValue,
     * getIntegerValue, getFloatValue.
     * @public
     * @param {string} valueId
     * The unique string id associated with a specific stored value.
     * @param {number | string | boolean} value
     * The primitive value to store.
     */
    saveValue(valueId, value)
    {
        try
        {
            let key = this.getFullKey(valueId);

            log.info(`LocalData: saving (key:${key},value:${value})`);

            localStorage.setItem(key, value.toString());
        }
        catch (error)
        {
            log.error(`Cannot save to local storage: ${error.toString()}`);
        }
    };


    /**
     * Saves an object under a specific identifier. The value can be retrived with a
     * call to getObjectValue. This is best used for serializing simple data objects.
     * @public
     * @param {string} valueId
     * The unique string id associated with a specific stored value.
     * @param {Object} valueObject
     * The object value to store.
     */
    saveObject(valueId, valueObject)
    {
        try
        {
            let key = this.getFullKey(valueId);
            let stringValue = JSON.stringify(valueObject);

            log.info(`LocalData: saveing (key:${key}, value:${stringValue})`);

            localStorage.setItem(key, stringValue);
        }
        catch (error)
        {
            log.error(`Cannot save to local storage: ${error.toString()}`);
        }
    };


    /**
     * Retrieves a stored value, treating it as an integer number. If no value is currently
     * stored under the given key (eg: it has not been previously saved, or the user has cleared
     * their browser cache), or if the value is stored but is not a valid integer, then the value
     * of parameter defaultValue will be returned instead.
     * @public
     * @param {string} valueId
     * The key under which the integer value is stored.
     * @param {number} defaultValue
     * Default value that should be returned, in the case that it is not possible to retrieve a
     * valid integer value. NOTE: the method does not validate this parameter to check if it is
     * an integer, it only validates any stored value.
     */
    getIntegerValue(valueId, defaultValue=0)
    {
        if (!this.isLocalStorageAvailable()) {
            return defaultValue;
        }

        let key = this.getFullKey(valueId);
        let rawValue = localStorage.getItem(key);
        let integerValue = defaultValue;

        if (rawValue !== null)
        {
            integerValue = Number.parseInt(rawValue);
            if (Number.isNaN(integerValue)) {
                integerValue = defaultValue;
            }
        }

        return integerValue;
    };


    /**
     * Retrieves a stored value, treating it as a floating point number. If no value is currently
     * stored under the given key (eg: it has not been previously saved, or the user has cleared
     * their browser cache), or if the value is stored but is not a valid floating point number,
     * then the value of parameter defaultValue will be returned instead.
     * @public
     * @param {string} valueId
     * The key under which the floating point number value is stored.
     * @param {number} [defaultValue]
     * Default value that should be returned, in the case that it is not possible to retrieve a
     * valid floating point number value.
     * @returns {number}
     * The number retrieved, or the default value.
     */
    getNumberValue(valueId, defaultValue=0)
    {
        if (!this.isLocalStorageAvailable()) {
            return defaultValue;
        }

        let key = this.getFullKey(valueId);
        let rawValue = localStorage.getItem(key);
        let floatValue = defaultValue;

        if (rawValue !== null)
        {
            floatValue = Number.parseFloat(rawValue);
            if (Number.isNaN(floatValue)) {
                floatValue = defaultValue;
            }
        }

        return floatValue;
    };


    /**
     * Retrieves a stored value, treating it as a string. If no value is stored under the given key
     * (eg: it has not been previously saved, or the user has cleared their browser cache), then the
     * value of parameter defaultValue will be returned instead.
     * @public
     * @param {string} valueId
     * The key under which the floating point number value is stored.
     * @param {string} defaultValue
     * Default value to return
     */
    getStringValue(valueId, defaultValue="")
    {
        if (!this.isLocalStorageAvailable()) {
            return defaultValue;
        }

        let key = this.getFullKey(valueId);
        let stringValue = localStorage.getItem(key);

        if (stringValue === null) {
            stringValue = defaultValue;
        }

        return stringValue;
    };


    /**
     * Retrieves a stored value, treating it as a boolean. If no value is stored under the given key
     * (eg: it has not been previously saved, or the user has cleared their browser cache), or if the
     * value is stored but is not a valid boolean, then the value of parameter defaultValue will be
     * returned instead.
     * @public
     * @param {string} valueId
     * The key under which the floating point number value is stored.
     * @param {boolean} [defaultValue]
     * Default value that should be returned, in the case that it is not possible to retrieve a
     * valid boolean value.
     * @returns {boolean}
     */
    getBooleanValue(valueId, defaultValue=false)
    {
        if (!this.isLocalStorageAvailable()) {
            return defaultValue;
        }

        let key = this.getFullKey(valueId);
        let rawValue = localStorage.getItem(key);
        let booleanValue = defaultValue;

        if (rawValue !== null)
        {
            booleanValue = rawValue === "true";
        }
        
        return booleanValue;
    };


    /**
     * Retrievs a stored object (stored using saveObject). Objects are saved as json, so if the
     * value stored is not valid json (or has not been previously saved under the given valueId,
     * or the user has cleared their browser cache), then the value of defaultValue will be
     * returned instead.
     * @public
     * @param {string} valueId
     * The key under which the floating point number value is stored.
     * @param {Object} defaultValue
     * Default value that should be returned, in the case that it is not possible to retrieve a
     * valid object.
     * @return {Object}
     */
    getObject(valueId, defaultValue={})
    {
        if (!this.isLocalStorageAvailable()) {
            return defaultValue;
        }

        let key = this.getFullKey(valueId);
        let rawValue = localStorage.getItem(key);
        let objectValue = defaultValue;

        if (rawValue !== null)
        {
            try
            {
                objectValue = JSON.parse(rawValue);
            }
            catch (error)
            {
                log.warn(`LocalData.getObject: value stored under key ${valueId} was not a valid json object. Returning default instead`);

                objectValue = defaultValue;
            }
        }

        return objectValue;
    };


    /**
     * Checks if local storage is available. It might exist, but be disallowed, in which case, apparently
     * we are forced to simply try it and soak up the exception to detect if its disallowed..
     * @private
     * @return {boolean}
     */
    isLocalStorageAvailable()
    {
        let isAvailable = false;

        try
        {
            let storage = window['localStorage'];
            let x = '__storage_test__';
            
            storage.setItem(x, x);
            storage.removeItem(x);

            isAvailable = true;
        }
        catch(e)
        {
            isAvailable = false;
        }

        return isAvailable;
    };
}


export default LocalData;