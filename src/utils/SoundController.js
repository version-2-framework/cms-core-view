import C_Local from "../const/C_Local";
import {Howl, Howler} from 'howler';
import C_GameEvent from "../../../cms-core-logic/src/const/C_GameEvent";
import * as LogManager from "../../../cms-core-logic/src/logging/LogManager";

const log = LogManager.getGameViewLogger();

/**
 * The duration (in seconds) that background music should be faded back in over,
 * when music is re-enabled.
 */
const MUSIC_ENABLED_FADE_DURATION = 0.2;

/**
 * The duration (in seconds) that background music should be faded out over, when
 * music is disabled.
 */
const MUSIC_DISABLED_FADE_DURATION = 0.1;

/**
 * The default duration (in seconds) that a cross-fade between 2 pieces of music should
 * last for. NOTE: with the current implementation, the real total duration is actually
 * twice this figure (fade out takes this time, and fade in of the new piece also takes
 * this time).
 */
const DEFAULT_MUSIC_FADE_DURATION = 0.5;

/**
 * Provides methods for working with sound and music.
 * @implements {SoundControllerApi}
 */
class SoundController
{
    constructor()
    {
        /**
         * @private
         */
        this.app = PIXI.app;

        /**
         * @private
         * @readonly
         * @type {number}
         */
        this.MAX_MUSIC_VOLUME = 1;

        // Document the type of this, and what purpose it serves
        /**
         * @private
         */
        this._soundBlobs = [];

        /**
         * Indicates if sound is currently enabled.
         * @private
         * @type {boolean}
         */
        this._isSoundEnabled = false;

        // TODO: We don't support this, but we could ?
        /**
         * Indicates if music is currently enabled.
         * @private
         * @type {boolean}
         */
        //this._isMusicEnabled = this.app.localData.getBooleanValue(C_Local.MUSIC_ENABLED, true);

        /**
         * A map of all sounds (eg: sound effects) that are currently playing, against their string id.
         * @private
         * @type {{[soundId:string]:Howl}}
         */
        this._currentlyActiveSounds = {};

        /**
         * Reference to any currently playing piece of background music.
         * @private
         * @type {Howl & {id?:string}}
         */
        this._backgroundMusic = null;

        /**
         * Target volume for the current piece of background music.
         * @private
         * @type {number}
         */
        this._backgroundMusicVolume = 1;

        /**
         * A TweenMax delayed call, queueing up the new set of music to start.
         * @private
         * @type {gsap.TweenMax}
         */
        this._startMusicCall = null;

        /**
         * The maximum volume at which music may play.
         * @private
         * @type {gsap.TweenMax}
         */
        this._startMusicCall = null;

        /**
         * Global event emitter instance.
         * @private
         * @type {EventEmitter}
         */
        this._dispatcher = this.app.dispatcher;

        /**
         * The id of any piece of main game music that should be played. A value of null,
         * indicates that no main game music has been configured.
         * @private
         * @type {string}
         */
        this._mainGameMusicId = null;

        /**
         * @private
         * @type {number}
         */
        this._mainGameMusicVolume = 1;

        /**
         * The id of the last piece of music playing before the current one (null if no
         * music was previously playing)
         * @private
         * @type {string}
         */
        this._lastMusicId = null;

        /**
         * @private
         * @type {number}
         */
        this._lastMusicVolume = 1;

        /**
         * Gain of music, as adjusted in-game.
         * @private
         * @type {number}
         */
        this._musicGain = 1;

        /**
         * Preparsed set of data about all loaded audio assets. This contains basic info about the file
         * type of the audio asset (which is extracted from its name on disk). It is a map: the key is
         * the simple string id of the audio asset. The data represents its file type. The map should be
         * populated at game launch (the list of audio asset filenames being loaded is used to do this).
         * This allows us to automatically support different file-types (eg: mp3, flac, etc).
         * @private
         * @type {{[key:string]:SupportedAudioFileType}}
         */
        this._audioAssets = {};

        let soundShouldBeEnabled = this.app.localData.getBooleanValue(C_Local.SOUND_ENABLED, true);

        log.debug(`SoundController created: soundShouldBeEnabled:${soundShouldBeEnabled}`);
        
        this.setSoundEnabled(soundShouldBeEnabled);
    };


    /**
     * Preparses the "dynamic assets json" - which is where our audio files get loaded from - in order to know
     * the full list of audio files being loaded - AND their file types. This allows SoundController to generate
     * an internal map, where the key is "soundId" (the simple string name that our game code refers to sounds
     * by, eg: "MySound"), and the value is fileType extensions. So, when any code in our game tries to play a
     * sound (passing in the soundId), SoundController can automatically fetch the correct fileType, and configure
     * the sound internally as required. The user of SoundController doesn't need to pass any information about
     * the type of sound being played, which makes it simpler to swap the fileType on disk (eg: we could move a
     * game from mp3 to flac audio assets, without requiring any code changes)
     * @public
     * @param {DynamicAssetsJson} dynamicAssetsJson
     */
    preParseDynamicAssets(dynamicAssetsJson)
    {
        log.info('SoundController.preParseDynamicAssets()');

        /** @type {SupportedAudioFileType[]} */
        let supportedFileTypes = ["mp3", "flac"];

        dynamicAssetsJson.paths.forEach(path =>
        {
            if (path.indexOf("/audio/") > -1)
            {
                let assetTokens = path.split("/audio/")[1].split(".");

                let assetName = assetTokens[0];
                let assetFileType = assetTokens[1];

                if (supportedFileTypes.indexOf(assetFileType) > -1)
                {
                    log.debug(`SoundController.preParseDynamicAssets: assetName=${assetName}, assetFileType=${assetFileType}`);
                
                    this._audioAssets[assetName] = assetFileType;
                }
                else
                {
                    log.warn(`SoundController.preParseDynamicAssets: fileType ${assetFileType} for sound ${assetName} not currently supported`);
                }
            }
        });
    };


    /**
     * Toggles the active state of sound: if sound is currently enabled, a call to this will flip it to
     * disabled, and vice-versa. This method will automatically take care of saving the new state of
     * sound to local preferences.
     * @public
     */
    toggleSound()
    {
        let newSoundState = !this._isSoundEnabled;
        this.setSoundEnabled(newSoundState);
    };


    // Currently not enabled: we only allow a generic "toggle sound" method
    /**
     * Toggle music state.
     * @public
     */
    /*
    toggleMusic()
    {
        let newMusicState = !this._isMusicEnabled;
        this.setMusicEnabled(newMusicState);
    };
    */


    /**
     * @public
     * @inheritDoc
     */
    getCurrMusicData()
    {
        return { musicId : this._backgroundMusic.id, musicVolume:this._backgroundMusicVolume };
    };


    /**
     * Sets the enabled state of sound to a specific value. Automatically takes care of saving the new
     * state of "is sound enabled" to local preferences.
     * @public
     * @param {boolean} newValue 
     * The new value of "is sound enabled".
     */
    setSoundEnabled(newValue)
    {
        this._isSoundEnabled = newValue;

        this.app.localData.saveValue(C_Local.SOUND_ENABLED, this._isSoundEnabled);

        Howler.mute(!this._isSoundEnabled);

        this._dispatcher.emit(C_GameEvent.SOUND_TOGGLED, this._isSoundEnabled);

        // If we ever restore the separate "enable / disable music" option, this code wants removing
        // from this method: it should be handlded only by the "setMusicEnabled" method.
        if (this._backgroundMusic)
        {
            let targetFullVolume = this._backgroundMusicVolume * this.MAX_MUSIC_VOLUME * this._musicGain;

            if (this._isSoundEnabled)
            {
                this._backgroundMusic.fade(0, targetFullVolume, MUSIC_ENABLED_FADE_DURATION * 1000);
            }
            else
            {
                this._backgroundMusic.fade(targetFullVolume, 0, MUSIC_DISABLED_FADE_DURATION * 1000);
            }
        }

        log.info(`SoundController.setSoundEnabled(enabled:${newValue})`);
    };


    // Currently not enabled: we only allow a generic "toggle sound" method
    /**
     * Sets the enabled state of music to a specific value. Automatically takes care of saving the new
     * state of "is music enabled" to local preferences. Any background music will be faded in / out
     * with a short transition (but only if the new state of music is different to the old state).
     * @public
     * @param {boolean} newValue 
     * The new value of "is music enabled".
     */
    /*
    setMusicEnabled(newValue)
    {
        if (this._isMusicEnabled === newValue) {
            return;
        }

        this._isMusicEnabled = newValue;

        this.app.localData.saveValue(C_Local.MUSIC_ENABLED, this._isMusicEnabled);

        if (this._backgroundMusic)
        {
            if (this._isMusicEnabled)
            {
                this._backgroundMusic.fade(0, this.MAX_MUSIC_VOLUME, MUSIC_ENABLED_FADE_DURATION * 1000);
            }
            else
            {
                this._backgroundMusic.fade(this.MAX_MUSIC_VOLUME, 0, MUSIC_DISABLED_FADE_DURATION * 1000);
            }
        }

        log.info(`SoundController.setMusicEnabled(enabled:${newValue})`);
    };
    */


    /**
     * Returns the current enabled state of sound.
     * @public
     * @returns {boolean}
     * True to indicate that sound is currently enabled, false to indicate that it is disabled.
     */
    getSoundEnabled()
    {
        return this._isSoundEnabled;
    };


    // Currently not enabled: we only allow a generic "toggle sound" method
    /**
     * Returns the current enabled state of music.
     * @public
     * @returns {boolean}
     * True to indicate that music is currently enabled, false to indicate that it is disabled.
     */
    /*
    getMusicEnabled()
    {
        return this._isMusicEnabled;
    };
    */


    /**
     * Sets the gain of background music: this is NOT the volume of a specific piece of music,
     * it is the volume that all music will be played at. An individual piece of music plays at
     * a volume, which is the product of this, and of its own relative volume setting. This
     * should not be changed as part of game settings: instead, it is for cases where you do not
     * want to stop music playing during the game (eg: during a custom Big Win sequence), but
     * want to turn the volume down for a bit.
     * @public
     * @param {number} volume
     * A number, between 0 and 1.
     */
    setMusicGain(volume)
    {
        this._musicGain = volume;

        if (this._isSoundEnabled && this._backgroundMusic)
        {
            this._backgroundMusic.volume(this._backgroundMusicVolume * this.MAX_MUSIC_VOLUME * this._musicGain);
        }
    };


    /**
     * Returns the current value for Music Gain.
     * @public
     * @return {number}
     */
    getMusicGain()
    {
        return this._musicGain;
    };


    /**
     * Plays a sound with a specified id.
     * @public
     * @param {string} soundId
     * The string id of the sound file to play.
     * @param {boolean} [loop]
     * Indicates if the played sound should loop. If not specified, defaults to false (no loop).
     * @param {number} [volume]
     * Specifies the volume (a floating point number, in the range of 0 to 1) at which the sound
     * should be played. If not specified, defaults to 1 (maximum volume).
     * @param {array} [sprites]
     * TODO: Still not actually sure what this parameter is used for, or why it needs to exist.
     * @return {Howl}
     */
    playSound(soundId, loop=false, volume=1, sprites=null)
    {
        let sound = this.getSound(soundId, loop, volume, sprites);

        // TODO: Right now, this ditionary is not doing anything useful.
        // - it caches only "sounds", not music
        // - we never reference it again
        // Howler takes care of making sure individual sounds are muted / unmuted,
        // so it's also not clear what it was originally intended for. Even if we dod
        // need to keep track of sounds playing (and do something with them), this actually
        // doesn't achieve that:
        // - we cache a reference to the sound, under its id.
        // - we delete that reference once the sound stops playing
        // - its imediately obvious that if we trigger the sound more than once (which we often do),
        //   then the dictionary as it's currently structured wouldn't be any help anyway:
        //   - we could have copies of that sound still playing, but the first one ended, so we
        //     have cleared all references to it playing
        //   - if we play more than once, we only cache one copy, which wouldn't be much help for
        //     restoring or muting existing sounds anyway.
        this._currentlyActiveSounds[soundId] = sound;

        sound.on('end', () => delete this._currentlyActiveSounds[soundId]);

        log.debug(`SoundController.playSound(soundId:${soundId},volume:${volume},sprites:${sprites})`);
        
        if (sprites)
        {
            sound.play(sprites[0]);
        }
        else
        {
            sound.play();
        }

        return sound;
    };


    // TODO: If this really "gets a raw sound", then it's not clear to me why it needs parameters
    // such as loop, volume and "sprites". I think this is just an artifact of the Howler api, but
    // it would be good to improve this method's documentation to make it clear why it is this way.
    /**
     * "Get a raw sound"
     * @public
     * @param {string} soundId
     * The string id of the sound file to fetch. This should not include file type, eg: if the
     * file that you loaded was "MySound.mp3", just pass "MySound" to this parameter.
     * @param {boolean} [loop]
     * Indicates if the played sound should loop. If not specified, defaults to false (no loop).
     * @param {number} [volume]
     * Specifies the volume (a floating point number, in the range of 0 to 1) at which the sound
     * should be played. If not specified, defaults to 1 (maximum volume).
     * @param [sprites]
     * TODO: Still not actually sure what this parameter is used for, or why it needs to exist.
     * @returns {Howl | null}
     */
    getSound(soundId, loop=false, volume=1, sprites=null)
    {
        let blobUrl = this._soundBlobs[soundId];
        if (blobUrl == undefined)
        {
            let audioAssetFileType = this._audioAssets[soundId];
            if (audioAssetFileType)
            {
                let qualifiedSoundId = `${soundId}.${audioAssetFileType}`;

                let byteArray = PIXI.loader.resources[qualifiedSoundId];
                if (byteArray)
                {
                    let encoding = this.getEncodingTypeForAudioFileType(audioAssetFileType);
                    let blob = new Blob([byteArray.data], { type:encoding });
                    blobUrl = window.URL.createObjectURL(blob);
                    this._soundBlobs[soundId] = blobUrl;
                }
                else
                {
                    log.error(`SoundController: could not find data for sound with id : ${soundId}`);
                }
            }
            else
            {
                log.error(`SoundController: no fileType associated with sound ${soundId}`);
            }
        }

        let sound = new Howl({
            src:[blobUrl],
            format:["flac"],
            loop:loop,
            preload:true,
            volume:volume,
            sprite: sprites ? sprites:null
        });

        return sound;
    };


    // TODO: Document this method a bit better!
    /**
     * Returns the encoding type string for a specific filetype.
     * @private
     * @param {SupportedAudioFileType} audioFileType 
     * @return {string}
     */
    getEncodingTypeForAudioFileType(audioFileType)
    {
        let encodingString = null;

        if (audioFileType === "mp3") {
            return "audio/mpeg";
        }
        else
        if (audioFileType === "flac") {
            return "audio/flac";
        }

        return encodingString;
    };


    /**
     * Starts a piece of music playing, treating it as the main piece of background music to play.
     * The id of the music passed will be cached for future reference, so that we can restore main
     * game music, as required. In general, we only want a single call to this method during the
     * life-cycle of a game client (or no calls, if no main game music is required).
     * @public
     * @inheritDoc
     * @param {string} musicId
     * The id of the main game music to play.
     * @param {number} [volume]
     */
    startMainGameMusic(musicId, volume=1)
    {
        log.info(`SoundController.startMainGameMusic(musicId:${musicId})`);

        this._mainGameMusicId = musicId;
        this._mainGameMusicVolume = volume;

        this.playMusic(musicId, 0, volume);
    };


    /**
     * @public
     * @inheritDoc
     */
    restoreMainGameMusic()
    {
        log.info(`SoundController.restoreMainGameMusic()`);

        // TODO: These calls here should be completely redundant (and indeed, they achieve nothing)
        if (this._backgroundMusic === null || this._backgroundMusic.id !== this._mainGameMusicId)
        {
            log.debug(`SoundController.restoreMainGameMusic: background music is null? ${this._backgroundMusic === null}`);
            log.debug(`SoundController.restoreMainGameMusic: backgroundMusicId = ${this._backgroundMusic? this._backgroundMusic.id : "null"}`);

            this.playMusic(this._mainGameMusicId, 0, this._mainGameMusicVolume);
        }
        else log.debug(`SoundController.restoreMainGameMusic: already playing`);
    };


    /**
     * @public
     * @inheritDoc
     */
    restoreLastMusic()
    {
        log.info(`SoundController.restoreLastMusic(lastMusicId:${this._lastMusicId},volume:${this._lastMusicVolume}})`);

        if (this._backgroundMusic === null || this._backgroundMusic.id !== this._lastMusicId)
        {
            this.playMusic(this._lastMusicId, 0, this._lastMusicVolume);
        }
        else log.debug(`SoundController.restoreLastMusic: already playing`);
    };


    // TODO: Should this be the main "playMainBackgroundMusic" method? It would be good for
    // SoundController to track whatever is the main piece of music (so we can do a revert, eg:
    // we can stop an existing piece of music from playing, and revert back to old one).
    /**
     * Starts a piece of background music playing. The SoundController supports the notion of playing
     * a single piece of background music at a time, and this method takes care of that. If some music
     * is already playing, there will be a cross-fade transition, the duration of which is specified
     * by parameter "crossFadeDuration". The cross-fade is not quite a cross-fade: the old music is
     * actually faded out before the new music is faded in. The total duration of this transition is
     * actually double the value of crossFadeDuration (this duration specifies the length of *each
     * of the 2 parts of the cross fade*). This behaviour could be changed in the future (so that the
     * total duration is actually the value passed), but for now has been kept in order to maintain
     * backwards compatibility with any existing code which uses this api.
     * @public
     * @param {string} musicId
     * The string id of the sound file to be used as music.
     * @param {number} [crossFadeDuration]
     * The duration (in seconds) of any cross-fade, from an existing piece of background music that is
     * playing, into the new piece of background music. If not specified, then a default cross fade
     * duration will be applied. It's valid to pass a value of 0 for instant cross-fading as well.
     * @param {number} [volume]
     * The volume at which the new piece of music will be played.
     */
    playMusic(musicId, crossFadeDuration=null, volume=1)
    {
        let fadeDuration = crossFadeDuration > 0? crossFadeDuration : 0;

        log.info(`SoundController.playMusic(musicId:${musicId},fadeDuration:${fadeDuration},volume:${volume})`);

        if (this._startMusicCall)
        {
            log.debug(`SoundController.playMusic: kill existing delayed call to play music`);

            this._startMusicCall.kill();
            this._startMusicCall = null;
        }

        if (this._backgroundMusic && this._backgroundMusic.id !== musicId)
        {
            log.debug(`SoundController.playMusic: must stop old music first!`);

            this.stopMusic(fadeDuration);
        }

        if (fadeDuration > 0)
        {
            log.debug(`SoundController.playMusic: play with fade of ${fadeDuration} seconds`);

            this._startMusicCall = TweenMax.delayedCall(fadeDuration, () => {
                this.startNewMusic(musicId, fadeDuration, volume);
            });
        }
        else
        {
            log.debug(`SoundController.playMusic: play new music immediately`)

            this.startNewMusic(musicId, 0, volume);
        }
    };


    /**
     * Stops any current piece of background music from playing. This is done using a cross-fade, the
     * duration of which can be customized (alternatively, it's possible to omit the cross-fade completely).
     * Using this method, no new piece of background music will be started.
     * @public
     * @param {number} [fadeOutDurationSecs]
     * The duration over which the music will be faded out, in seconds. Omitting this parameter
     * means that a default duration will be applied. Specifying a value of 0 means that there will
     * be no cross-fade: current music will be stopped immediately.
     */
    stopMusic(fadeOutDurationSecs=null)
    {
        if (this._backgroundMusic)
        {
            let bgMusic = this._backgroundMusic;

            this._lastMusicId = this._backgroundMusic.id;
            this._lastMusicVolume = this._backgroundMusicVolume;

            this._backgroundMusic = null;

            log.debug(`SoundController.stopMusic: stopping music with id ${this._lastMusicId}`);

            if (fadeOutDurationSecs === null) {
                fadeOutDurationSecs = DEFAULT_MUSIC_FADE_DURATION;
            }

            if (fadeOutDurationSecs > 0)
            {
                log.debug(`SoundController.stopMusic: stop with fade of ${fadeOutDurationSecs}`);

                let currVolume  = bgMusic.volume();
                let fadeOutDurationMs = fadeOutDurationSecs * 1000;
                bgMusic.fade(currVolume, 0, fadeOutDurationMs);
                TweenMax.delayedCall(fadeOutDurationSecs, () => bgMusic.stop());
            }
            else
            {
                log.debug(`SoundController.stopMusic: stop immediately`);

                bgMusic.stop();
            }
        }
    };


    // TODO: Should this also accept a target volume value ?
    /**
     * Starts a new piece of background music playing. This is a purely internal method.
     * @private
     * @param {string} musicId
     * The string id of the sound file to be used as music.
     * @param {number} volume
     */
    startNewMusic(musicId, fadeInDurationSecs=0, volume=1)
    {
        let targetVolume = volume * this.MAX_MUSIC_VOLUME;
        
        let initialVolume = 0;
        if (fadeInDurationSecs === 0 && this._isSoundEnabled) {
            initialVolume = targetVolume;
        }

        log.debug(`SoundController.startNewMusic(musicId:${musicId}, initialVolume:${initialVolume})`);

        this._backgroundMusic = this.getSound(musicId, true, initialVolume);
        this._backgroundMusic.id = musicId;
        this._backgroundMusic.play();

        this._backgroundMusicVolume = volume;

        //if (this._isMusicEnabled && fadeInDurationSecs > 0)
        if (this._isSoundEnabled && fadeInDurationSecs > 0)
        {
            this._backgroundMusic.fade(0, targetVolume, fadeInDurationSecs * 1000);
        }
    };
}


export default SoundController;