import MathUtil from "./MathUtil";


class Utils
{

    constructor()
    {
        this.app = PIXI.app;
        let symbolsData = this.app.config.symbols;

        this.symbolIds = [];
        for (let key in symbolsData)
        {
            this.symbolIds.push(symbolsData[key].id);
        }
    }


    getRandomSymId()
    {
        return this.symbolIds[MathUtil.getRandomInt(0, this.symbolIds.length-1)];
    }


    /**
     *
     * @param display {PIXI.DisplayObject}
     * @param callback {Function}
     * @param args {*[]=}
     * @param context {*=}
     */
    static addedToStage(display, callback, args = null, context = null)
    {
        while (display.parent)
        {
            display = display.parent;
        }
        if (display === PIXI.app.stage)
        {
            callback.apply(context, args);
        }
        else
        {
            display.once("added", function()
            {
                Utils.addedToStage(display, callback, args, context);
            }, this);
        }
    }


}

export default Utils