//--------------------------------------------------------------------------------------------------
// General interfaces for common view components.
//--------------------------------------------------------------------------------------------------
/**
 * API for a Loading Spinner component.
 */
interface LoadingSpinnerView extends PIXI.DisplayObject
{
    /**
     * Shows the Loading Spinner's animation, which is generally expected to be some kind of
     * infinite spinning action (although in principle, it could be anything). The loading
     * spinner does not need to show anything visually until this is called: however, it will
     * normally be removed from the display list when it is not needed.
     */
    showLoadingSpinner() : void;

    /**
     * Hides the Loading Spinner's animation.
     */
    hideLoadingSpinner() : void;
}

interface TotalWinView extends PIXI.DisplayObject
{
    /**
     * Creates, and starts running, the Total Win animation.
     * @param rewardGroup 
     * @param rewardValue 
     */
    create(rewardGroup : number, rewardValue : number) : void;
}


interface BigWinView extends PIXI.DisplayObject
{
    /**
     * For some reason, for a BigWinView, you have to call "create" (the constructor won't do
     * it alone). This was a Wiener design decision, but we (CMS) don't know the reason for it
     * @param {number} [totalWonMultiplier]
     * Multiplier of TotalStake that the BigWin value represents.
     * @param {number} [amountWon]
     * Optional indication of the amount won (some Big Win animations may adapt the sequence that
     * they show, based on this value). This must be expressed as multiples of Total Stake.
     */
    create(totalWonMultiplier?:number, amountWon?:number) : void;
}

interface TooltipEnabledGuiButton extends PIXI.DisplayObject
{
    setTooltipOrientation(orientation:TooltipOrientation, position:TooltipPosition) : void;
}