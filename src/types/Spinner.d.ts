
/**
 * A spinner is anything which can spin a set of symbols.
 */
interface Spinner
{
    /**
     * Specifies the number of symbols that this Spinner implementation requires to be
     * created above the visible symbols area. The ReelView class will create the actual
     * symbols, according to this specification.
     * 
     * Each reel shows a certain number of symbols (eg: 3). Some spinner implementations
     * need more symbol graphics available than the number shown when static: if we show 3
     * symbols when static, then at certain points during a spin on a "standard" spinner,
     * we will probably be showing 2 complete symbols, and 2 half symbols. Some spinner
     * implementations will not need any padding symbols at all, and some may have very
     * custom requirements.
     */
    readonly numSymAboveVisibleArea : number;

    /**
     * Specifies the number of symbols that this Spinner implementation requires to be
     * created below the visible symbols area. The ReelView class will create the actual
     * symbols, according to this specification.
     * 
     * Each reel shows a certain number of symbols (eg: 3). Some spinner implementations
     * need more symbol graphics available than the number shown when static: if we show 3
     * symbols when static, then at certain points during a spin on a "standard" spinner,
     * we will probably be showing 2 complete symbols, and 2 half symbols. Some spinner
     * implementations will not need any padding symbols at all, and some may have very
     * custom requirements.
     */
    readonly numSymBelowVisibleArea : number;

    /**
     * Set of TallSymbols data for this Spinner. This data is generated during a spin.
     * External users of the Spinner api (eg: the Reel class), should only need to know
     * about tall symbols data when no spin animation is in progress: this is the only
     * time it is safe to access this field to retrieve the correct current values.
     * 
     * If no tall symbols are being shown on the current reel, then this list will be
     * empty. Otherwise, it specifies any tall symbols shown within the visible area for
     * the reel.
     * 
     * Not all Spinner implementations will support tall symbols.
     */
    readonly tallSymbolsData : TallSymbolsData[];

    /**
     * Starts an infinite spin on this spinner. If any spin animation is on-going, then a
     * good spinner implementation should do nothing when this method is called again. Not
     * all spinner implementations need to do anything when this method is called: (for some
     * types of spin animation, it may be simpler to do nothing, and execute the full spin
     * when "spinToSymbols" is called).
     * @param spinPhaseType
     * The type of spin phase being executed.
     * 1 == Spin1
     * 2 == Spin2
     * 3 == FreeSpin
     * So, a value of 1 will be passed for ANY SpinResult that is part of a Spin 1 phase (and
     * the same is true for other phases).
     * Not all Spinners need to respect this parameter, but for a spinner that needs to perform
     * some different simulation based on the spin phase being played, it is usefull.
     */
    startInfiniteSpin(spinPhaseType ? : SpinPhaseType) : void;

    /**
     * Orders this spinner to play a spin animation which stops on the provided set of symbols.
     * stop on the passed set of symbols. If an infinite spin is in progress, then a Spinner
     * implementation should interrupt it, to bring the reels to a halt. If no infinite spin is
     * in progress, then the Spinner implementation should start the reels spinning, and then
     * bring them to a halt on the desired symbols. For this reason, we should always be able to
     * skip the infinite spin animation, and only call "spinToSymbols", in order to visualize a
     * spin animation.
     * For some spinner implementations, it will be valid for "startInfiniteSpin" to do nothing,
     * and for the entire spin animation to be triggered by spinToSymbols (at which point, we
     * will already know the full results expected for the spin)
     * @param symbols
     * The set of symbols that the Spinner should stop on.
     * @param spinType
     * Indicates the basic type of Spin simulation. This is meta-data, normally returned by the
     * Game Engine server. The exact meaning is not currently defined in a strong way, but we
     * do have some standard values. 1 == standard spin, 2 == respin. Some maths engines may return
     * additional meta-data flags (which could be interpreted in a special way, by a custom spinner)
     * @param positionIsSticky
     * Indicates if each of the positions on the reel is held from the previous spin. This is a
     * boolean list, the length of which must be the same as the number of symbols on the reel
     * (eg: a state is supplied for each symbol position). If not passed, then a Spinner must
     * assume that no Symbols are held as sticky (this is the expected case for the 1st spin of
     * a Spin Round, where generally no symbols will be sticky, or for use cases where we want
     * a simple spin, with no concept of previous symbols).
     * 
     * This data will be interpreted differently, according to the spinner implementation being
     * used, but the data itself always comes from the "stickySymbolsMap" which is returned with
     * a spin result. If we have a spin round, with 2 spin results - if there are sticky symbols,
     * then the data is supplied on the 2nd spin. For some mechanics of spin, the positions indicated
     * as sticky are the positions which the symbol is kept in. For a mechanic like "Collapsing
     * Symbols", the sticky positions are supplied on the 2nd spin, and still refer to the positions
     * from the 1st spin which were sticky, but its possible these symbols will actually move on
     * the 2nd spin's simulation. So, different spinners will handle this data differently, but
     * by convention, the sticky symbols data always comes from the "2nd" spin result. In many
     * sticky spin simulations like Collapsing Symbols, the Spinner needs to already know the
     * previous symbols shown anyway.
     * @param spinPhaseType
     * Indicats the Spin Phase Type that this spin is for (Spin 1, Spin 2, FreeSpin). Some Spinner
     * implementations may which to execute different simulations, according to the value passed in.
     * This flag is optional: some spinner implementations may whish to completely ignore it.
     * 1 == Spin1 (SingleSpin or Spin1)
     * 2 == Spin2
     * 3 == FreeSpin
     */
    spinToSymbols(
        finalSymbols : SymbolConfig[],
        spinType ? : number,
        positionIsSticky ? : boolean[],
        spinPhaseType ? : SpinPhaseType
    ) : void;

    /**
     * Activates or de-actives "QuickStop" state. QuickStop will only affect "spinToSymbols" - if
     * enabled, then SpinToSymbols can land on the destination outcome more quickly. Not all Spinner
     * implementations are expected to support this functionality (and for those that do, it may have
     * different behaviour according to the implementation).
     */
    setQuickStopEnabled(isEnabled : boolean) : void;

    /**
     * Indicates if QuickStop is currently enabled for this Spinner.
     * @return true if quickstop is enabled, false if not.
     */
    getIsQuickStopEnabled() : boolean;

    /**
     * Aborts any spin which is currently in progress.
     */
    abortSpin() : void;
    
    // TODO: something like "getIsSpinning()" would probably also be a useful piece of public api.
    // However, considering that we have both "infiniteSpin" and "spinningToSymbols", I am not 100%
    // sure yet what the most appropriate flag checking methods would be.

    /**
     * A directly settable property, corresponding to the Speed Scale factor for any spin that is
     * in progress. A value of 1 indicates that the spin occurs at normal speed: a value of 2
     * indicates double speed, and 0.5 indicates half speed. This property is directly settable,
     * because it is designed to be tweened. Use it to add special "slow motion" effects on the
     * reel spins.
     */
    speedScaleFactor : number;
}

/**
 * Public API which all SymbolViews need to implement.
 * 
 * A SymbolView shows a Symbol on-screen, within the "ReelsGroup" layer. We may have other
 * static representations of Symbols within the "WinPresentation" layer, but as standard,
 * we have a set of SymbolView objects, attached to various reels. We spin them, we move
 * them around and re-order them as we spin, we trigger win animations on them when they
 * are static.
 * 
 * The SymbolView API is like a basic state machine, but with 2 important pieces of parallel
 * state: its important to understand these concepts, to understand how you use a SymbolView.
 * 
 * 1) "Visual State" - this is the context in which the symbol is being used. We currently
 *    have 6 available VisualStates
 *    - IDLE: Shown when the symbol is at rest, and there are no wins being shown
 *      See {@link SymbolView#showIdleState}
 *    - SPINNING: Shown when a spin is in progress
 *      See {@link SymbolView#showSpinningState}
 *    - BIG_WIN: The Symbol is shown as involved in a Big Win presentation. Big Wins are
 *      shown as part of the overall Quick Win presentatation, but are a special type of
 *      win presentation in their own (often using separately configured visual effects)
 *      See {@link SymbolView#showBigWinState}
 *    - QUICK_WIN: The Symbol is shown as involved in a Quick Win presentation. This is
 *      when we initially show each individual win (and its amount)
 *      See {@link SymbolView#showQuickWinState}
 *    - IDLE_WIN: The Symbol is shown as involved in an Idle Win presentation. This is the
 *      state in which we show any symbol animations (we loop through all Symbol Wins when
 *      the game is in IDLE state)
 *      See {@link SymbolView#showIdleWinState}
 *    - NON WINNING: Shown when the Symbol is not involved in a Win (but other symbols are)
 *      See {@link SymbolView#showNonWinState}
 * 
 * 2) "Tall Symbol State". A symbol can be shown in a number of modes which relate to Tall
 *    Symbols.
 *    - NORMAL: This is the default mode in which a Symbol is shown: it occupies exactly 1
 *      vertical space.
 *      See {@link SymbolView#showAsNormalSymbol}
 *    - TALL: Sets this Symbol to render as a TallSymbol. We can specify exactly how many
 *      symbols tall the Tall Symbol must be. This method basically expands a single symbol
 *      to being taller than normal. It will overlap with other symbols - those symbols that
 *      are overlapped, should be set to the INVOLVED_IN_TALL_SYMBOL state.
 *      See {@link SymbolView#showAsTallSymbol}
 *    - INVOLVED_IN_TALL: When a symbol is set as TALL, the symbols it overlaps should be set
 *      to INVOLVED_IN_TALL. Imagine you have 3 symbols on a reel, and you set the bottom
 *      symbol to TALL (3 tall), and it is set to expand upwards. The top 2 positions should
 *      be set to INVOLVED_IN_TALL. In this mode, a standard SymbolView implementation would be
 *      be expected to visually hide itself.
 *      See {@link SymbolView#showAsInvolvedInTallSymbol}
 *    - PARTIALLY_TALL: This is a special mode, which is an alternative to TALL / INVOLVED_IN_TALL.
 *      In this mode, we expect a SymbolView to know that it is part of a tall symbol - but to
 *      show a portion of the tall symbol. Depending on the requirements of the game graphics, this
 *      may involve masking (and may be less efficient to render), which is why we have the option
 *      of showing tall symbols in more than 1 way. In this mode, if we have 3 symbols on a reel, and
 *      want to show a 3 tall symbol, then we set all 3 symbols to INVOLVED_IN_TALL, specifying their
 *      index (from the top) within the tall symbol, and the overall size of the tall symbol. This
 *      mode may be appropriate for tall symbols on Collapsing Symbol games (where we could destroy
 *      a middle section of a tall symbol, and collapse the other 2 parts) - it would be appropritae
 *      where we had very specific requirements for how the tall symbol graphics should look.
 *      See {@link SymbolView#showAsPartiallyTallSymbol}
 * 
 * In general, a good SymbolView implementation should be able to track both sets of state
 * simultaneously, and trigger the appropriate visual transitions internally. Outside users
 * of the SymbolView API are not expected to call complicated methods on SymbolView to get
 * it to render correctly: it should be sufficient to trigger one of the basic Visual States,
 * and to change its Tall Symbol State as required. Everything else, should be handled by
 * the SymbolView implementation internally.
 * 
 * @todo: Inheriting from PIXI.DisplayObject may not be quite right for the interface.
 */
interface SymbolView extends PIXI.DisplayObject
{
    /**
     * Shows the Symbol in normal Visual State, with all visual effects cleared. This is how
     * the player would be presented with a Symbol, the first time the game is opened. Any
     * animations will be cleared, any tall symbol state shown will be removed, all visual
     * effects will be removed.
     */
    showIdleState() : void;

    /**
     * Shows the symbol in HELD visual state, which is used for two spin games.
     */
    showHeldState() : void;

    /**
     * Sets the symbol to the visual state required while a spin is in progress.
     */
    showSpinningState() : void;

    /**
     * Sets the symnbol into "Big Win" visual state.
     */
    showBigWinState() : void;

    /**
     * Sets the symbol into "Quick Win" visual state.
     */
    showQuickWinState() : void;

    /**
     * Sets the symbol into "Idle Win" visual state.
     */
    showIdleWinState() : void;

    /**
     * Sets the symbol to a non-winning visual state. When we need to highlight some combination
     * of symbols, we should call "showWinState" for any Symbol that wants to be highlighted, and
     * "showNonWinState" for any Symbol that should not be highlighted (sometimes, this will fade
     * the symbol away: othertimes, we may show a special visual state for it).
     */
    showNonWinState() : void;

    /**
     * Shows this SymbolView as a normal, "1 tall" symbol. This is the default Tall Symbol State.
     * Calling this after the SymbolView has been in any other TallSymbolState, will revert the Symbol
     * View back to the default 1 tall state.
     */
    showAsNormalSymbol() : void;

    /**
     * Indicates if this SymbolView is currently being shown as Normal symbol.
     */
    isNormalSymbol() : boolean;

    /**
     * Shows this SymbolView as a TallSymbol.
     * @param numPosInTallSymbol
     * The number of positions high that the Tall Symbol must be shown as.
     * @param anchorAtBottom 
     * This parameter comes from wieners original code, and I am still completely cluesless as to the
     * exact implications of changing it. I would like to say its meaning is 
     * "Indicates if the SymbolView should show as tall above or below its current position.", but
     * from scanning the codebase, this actually doesnt seem to be correct. This is still on the todo
     * list, unfortunately.
     */
    showAsTallSymbol(numPosInTallSymbol:number, anchorAtBottom:boolean) : void;

    /**
     * Indicates if this SymbolView is currently being shown as a Tall Symbol.
     */
    isTallSymbol() : boolean;

    /**
     * Shows this SymbolView as involved in a tall symbol. In this mode, the SymbolView will basically
     * be hidden. This is appropriate to use, when you are using "showAsTallSymbol" on another symbol:
     * call showAsInvolved in the symbols which would be overlapped by the tall symbol.
     */
    showAsInvolvedInTallSymbol() : void;

    /**
     * Indicates if this SymbolView is currently being shown as involved in a Tall Symbol.
     */
    isInvolvedInTallSymbol() : boolean;

    /**
     * Shows this symbol as a component of a Tall Symbol. If we have a 3 tall symbol, we can set
     * all 3 symbols as partially tall, telling each SymbolView the overall size of the tall symbol,
     * and the index within that tall symbol that the SymbolView is to show. This allows us to split
     * apart tall symbols visually, if we want to. In this mode, the individual SymbolView remains a
     * single symbol tall, and simply shows a slice of the Tall Symbol.
     * @param numPosInTallSymbol 
     * The number of symbols within the Tall Symbol.
     * @param indexWithinTallSymbol 
     * The index within the Tall Symbol (from the top) that this SymbolView should who. A value of 0
     * would mean that this SymbolView renders as the top 1 tall section of the tall symbol.
     */
    showAsPartiallyTallSymbol(numPosInTallSymbol:number, indexWithinTallSymbol:number) : void;

    /**
     * Indicates if this SymbolView is currently being shown as a Partially Tall symbol.
     */
    isPartiallyTallSymbol() : boolean;
    
    /**
     * Sets the blur level of the symbol, during a Spin. To be called by the Spinner implementation,
     * which will decide if to show blurring / how much blurring needs to be shown, and when. The
     * level parameter sets the amount of blur to show (its a value in the range 0 to 1). This same
     * method is also used to clear any blur effect (simply call setBlurLevel(0)). This method may
     * produce different visual effects, for different SymbolView implementations (some may show no
     * effect). In addition, calling the method will only do something when the SymbolView is set to
     * "SPINNING" VisualState. If the SymbolView is showing some kind of Tall symbol state, then there
     * is also no guarantee it will have any visual effect (even when VisualState is "SPINNING").
     * @param level 
     * The blur level to set, where 0 == no blur, and 1 == maximum blur. The method will clamp the
     * inputs, so values less than 0 will be treated as 0, and values more than 1 will be treated as
     * 1.
     */
    setBlurLevel(blurLevel : number) : void;
    
    /**
     * Updates the symbol id shown on this symbol view. This will trigger an immediate refresh
     * of the rendering of the symbol.
     * @param symbolId
     */
    setId(symbolId : number) : void;
    
    /**
     * @return The current symbol id set on this Symbol View.
     */
    getId() : number;

    /**
     * Sets the value of position index for this symbol, relative to all symbols (visible and non
     * visible) on the reel. This value is for debug purposes only: the symbol can both log, and
     * show, debug information about its current position and status, so it's usefull for it to
     * have some understanding of this value. If debugText is enabled for the symbol, then updating
     * this value, will cause the debug text to immediately update.
     * @param posIndex
     */
    setPosIndex(posIndex : number) : void;

    /**
     *  Sets the value of reel index for this symbol. This is used solely for debug purposes (if
     * provided, and symbol logs are enabled, then the symbol can print more specific information to
     * logs about exactly which symbol - on which reel - is doing some action: with-out this data, the
     * symbol logs are likely to be long and full of hard to understand noise).
     * @param reelIndex
     * The index of the reel that this symbol is attached to.
     */
    setReelIndex(reelIndex : number) : void;

    /**
     * Sets the status of debug info, shown over the symbol. This is text information, which will
     * report both the symbol id, and the currently known position index, of the symbol view. This
     * method validates its parameters, so it's safe to call as required, and will have no harmfull
     * side-effects.
     * @param shouldShow
     * Pass true to show the debug info, and pass false to not show it (which will clear any debug
     * info text that was previously shown).
     */
    showDebugInfo(shouldShow : boolean) : void;
}

/**
 * A ReelView is the general wrapper to a Reel. ReelGroup interacts only with the api of ReelView,
 * which provides broad level methods for triggering different actions.
 */
interface ReelView
{
    /**
     * Returns the active Slot Layout.
     */
    getActiveSlotLayout() : SlotLayoutConfig;

    /**
     * Special method for iterating all symbols: generates meta-data, allowing us to check if a
     * symbol is in the visible area or not.
     * @param iterator
     * A callback method which will be called for each symbol: let's us check explicitly if the
     * symbol is in a visible area, and what its "visible" index is.
     */
    iterateSymbols(iterator : (symbol:SymbolView, isInVisibleArea:boolean, visibleSymIndex?:number) => void) : void;

    /**
     * Starts the infinite spin on the Reel.
     * @param spinPhaseType
     * The type of spin phase being executed.
     * 1 == Spin1
     * 2 == Spin2
     * 3 == FreeSpin
     * So, a value of 1 will be passed for ANY SpinResult that is part of a Spin 1 phase (and
     * the same is true for other phases).
     */
    startInfiniteSpin(spinPhaseType : SpinPhaseType);

    /**
     * Starts the "spin to symbols" phase of a spin.
     * @param symbols 
     * @param spinType 
     * @param positionIsSticky 
     * @param spinPhaseType
     */
    spinToSymbols(
        finalSymbols : SymbolConfig[],
        spinType ? : number,
        positionIsSticky ? : boolean[],
        spinPhaseType ? : SpinPhaseType
    ) : void;

    /**
     * Shows the reel in IDLE state. No spin is happening, all symbols are set to IDLE state.
     */
    showIdleState() : void;

    /**
     * Shows the whole reel as held: this will set all symbols on the reel into held visual
     * state. This should only be used when the slot game uses a "hold reels" mechanic
     * (eg: we hold whole reels): when holding individual symbols, we will effectively spin
     * the reel, but indicate that certain symbols must be held (not all spinners will
     * support this functionality)
     */
    showHeldState() : void;

    /**
     * Shows the whole reel as spinning: this will set all symbols on the reel in to
     * spinning visual state. This does not trigger any spin: it can be invoked before the
     * actual spin for the reel is started. There is one particular use case that this is
     * useful for: 2 spin games, where we are playing in autoplay, and we are showing holds
     * via the symbols: because we skip idle state of the spin (and therefore skip idle
     * visual state for the symbols), this call is useful to explicitly hide the held state
     * of the symbols.
     */
    showSpinningState() : void;

    /**
     * Sets symbols on this Reel into "BigWin" visual state.
     * @param symIsWinning
     * @param fadeNonWinningSymbols 
     */
    showBigWinState(symIsWinning:boolean[], fadeNonWinningSymbols:boolean) : void;
    
    /**
     * Sets symbols on this Reel into "QuickWin" visual state.
     * @param symIsWinning
     * @param fadeNonWinningSymbols 
     */
    showQuickWinState(symIsWinning:boolean[], fadeNonWinningSymbols:boolean) : void;

    /**
     * Sets symbols on this Reel into "IdleWin" visual state.
     * @param symIsWinning
     * @param fadeNonWinningSymbols 
     */
    showIdleWinState(symIsWinning:boolean[], fadeNonWinningSymbols:boolean) : void;

    /**
     * Shows all symbols on the reel in "non-winning" state.
     */
    showFadeState() : void;

    // TODO: I would prefer if this was renamed slightly, to communicate the fact that this represents
    // graphics, and not data.
    /**
     * Returns access to all symbols on the reel (including symbols outside of the "visible" area).
     * We expect this list to be in the correct order - starting from index 0 and moving to the end of
     * the list, gives us the symbols from top to bottom of the reel. However, while a spin is in progress,
     * there is no 100% guarantee that this will always be true. Outside users of the ReelView api (with
     * the exception of a Spinner) should only be accessing symbols through this field, when no spin is
     * in progress.
     */
    symbols : SymbolView[];

    /**
     * Sets the ids shown on all visible symbols.
     * @param symbolIds
     * The list of symbol ids to show for visible symbols.
     */
    setAllSymbolIds(symbolIds : SymbolConfig[]) : void

    /**
     * Aborts any spin currently in progress.
     */
    abortSpin() : void;
    
    /**
     * The index of the reel (0 == left most) within the context of its hand.
     */
    readonly reelIndex : number;

    /**
     * The number of visible symbols shown on this reel.
     */
    readonly numSymVisible : number;

    // TODO: This needs a much better name
    readonly symStep : number;

    /**
     * Returns the total visible height of the reel, in pixels.
     */
    readonly visibleHeight : number;

    /**
     * The idle y positions of all symbols (including hidden symbols)
     */
    readonly symbolIdleYPositions : number[];

    /**
     * Returns the top wrap position of the reel. If the reel was moving upwards, then this
     * is the y position, above which a symbol should be wrapped to bottom. You can compare
     * the symbol's y position to this value directly (symbols are center anchored, and this
     * wrap position already takes this into account: its NOT the y coordinate of the top most
     * visible part of the reel)
     */
    readonly topWrapPosY : number;

    /**
     * Returns the bottom wrap position of the reel. If the reel was moving downwards, then
     * this is the y position, below which a symbol should be wrapped to top. You can compare
     * the symbol's y position to this value directly (symbols are center anchored, and this
     * wrap position already takes this into account: its NOT the y coordinate of the top most
     * visible part of the reel)
     */
    readonly bottomWrapPosY : number;

    // TODO: This can be renamed. The current meaning of yOrigin on Reel is not explained,
    // but its basically just the yOrigin from config, which is... also not well explained,
    // and may not play well with the idea of the reelsMarginTop.
    readonly yOrigin : number;

    /**
     * Sorts the members of the symbols field, in to the correct order, according to their
     * current y positions. Usefull to call after moving symbols physically (from top to bottom,
     * or vice versa), where we want the symbols field to represent the correct new semantic order.
     */
    sortSymbolsByY() : void;

    /**
     * Resets all symbols to their reseting y positions. This method assumes that the symbols are
     * already in the correct top to bottom order (both visually, and from a recent call to
     * "sortSymbolsByY". If these conditions are true, then the method ensures that all of the
     * symbols have their y position updated to exactly the pixel values of the resting positions.
     * If either of these pre-conditions are not true, it may be more appropriate to call
     * "snappAllSymbolsToClosestYPositions".
     * @private
     */
    resetAllSymbolPositions() : void;

    /**
     * Determines a top to bottom position order for all symbols, by finding the symbol with the
     * closest y position to the top-most visible symbol, and then re-positioning all other symbols
     * according to this base-line. This method attempts to ensure that we end up with a valid final
     * ordering of symbols, including having the correct respective numbers of any spare "non visible"
     * symbols (above and below the reels). It guarantees to place the re-ordered symbols at the pixel
     * correct resting y positions, and will also re-order them (so that the symbols array returns the
     * symbols in the same order as they are seen visually). This method is appropriate to call when
     * your spinner implementation has finished its spin, and the symbols are all basically in the
     * correct final positions, but the non-visible symbols above/below the reels may not be correctly
     * positioned. If these pre-conditions are met, then the method should work quite effectively.
     */
    snapAllSymbolsToClosestYPositions () : void;
    
    /**
     * Immediately sets the Tall Symbol state of all symbols (visible and not visible) to one of 3
     * possible values ("not tall" / "tall" / "involved in tall") based on the tallSymbolsData passed to
     * this method. The anchorAtBottom parameter allows us to specify whether the symbols set as Tall will
     * be the bottom symbol of the tall symbol area, or the top symbol. Any other symbols inovled in a tall
     * symbol will be set to "involved in tall", and any other symbols (includsing non-visible symbols
     * outside of the visible area) will all be set to "normal" tall state.
     *
     * This is useful for Spinners to call, when a new spin is starting. If our reels spin from to top to
     * bottom, we will generally set the anchor position of a tall symbol as the bottom symbol (and set the
     * other symbols above it to "involved in tall", ie: hidden). However, when we want to spin the tall
     * symbol off of the bottom of the reel, we have a potential problem: generally, we want to minimize the
     * number of padding symbols above / below the visible area, that a spinner implementation creates. If
     * only 1 padding symbol exists (but we have a tall symbol with 2,3,4,etc, symbols in it), then if the
     * tall symbol is bottom anchored, we want to snap the symbol back to the top when its bottom edge has
     * travelled past 1 symbol below the reel - which means we may see the top part of the tall symbol vanish!
     * 
     * One solution would be to have a large number of padding symbols below the reel, but this would need
     * to be as many symbols as the maximum number that can appear in a tall symbol - if we want large reels,
     * and very large tall symbols, this means the creation of a LOT of padding symbol instances. The
     * alternative, is to "flip" any tall symbols when the spin starts - so that they are not top anchored -
     * meaning "involved in tall" symbols are wrapped back to the top after travelling only 1 position below
     * the visible area (and are available again for feeding back on to the reel), and the tall symbol can
     * also be wrapped in the normal way. This method implicitly does that for us, and so it can be useful
     * for a spinner which supports tall symbols (and uses them in a "standard" way) to call this when a new
     * spin is starting.
     * @param anchorAtBottom
     * @param tallSymbolsData
     */
    setAnyTallSymbolsToTallOrInvolved(anchorAtBottom:boolean, tallSymbolsData:TallSymbolsData[]) : void;

    // TODO: These should be a part of the "internal" api ?
    /**
     * Fires the event notification, to signify that the reel has fully started any
     * infinite spin phase.
     */
    fireInfiniteSpinStartedEvent() : void;

    // TODO: Document better, according to required use cases
    /**
     * Fires the reel settling event, to signify that the reel is settling.
     */
    fireReelSettlingEvent() : void;

    /**
     * Fires the event notification, to signify that the reel has stopped spinning.
     */
    fireReelStoppedEvent() : void;

    /**
     * Triggers any reel-stop sound that should be played for this reel.
     */
    triggerReelStopSound() : void;

    /**
     * A directly settable property, corresponding to the Speed Scale factor for any spin that is
     * in progress. A value of 1 indicates that the spin occurs at normal speed: a value of 2
     * indicates double speed, and 0.5 indicates half speed. This property is directly settable,
     * because it is designed to be tweened. Use it to add special "slow motion" effects on the
     * reel spins.
     */
    speedScaleFactor : number;

    // TODO: Should probably be part of the "internal" api
    /**
     * A pixi container, into which spinners are allowed to write any custom graphics which must
     * be shown above the symbols.
     */
    readonly overlayContainer : PIXI.Container;
}

/**
 * A mechanic for feeding in new symbols during a spin. Not all spinners may show
 */
interface SymbolFeed
{
    /**
     * Returns the id of the next new symbol to show during the infinite spin phase
     * of a spin.
     * @param {number} reelIndex
     * Index of the reel to get an infinite spin symbol id for.
     */
    getNextInfiniteSpinSymbol(reelIndex:number) : number;

    /**
     * Returns the id of the next new symbol to show during the stop phase of a spin (but
     * before we actually show the final symbols). This will be invoked once we already know
     * what the final results of the spin are, so it allows us to tease specific symbol ids
     * on specific reels, based on game specific requirements for spin presentations (eg: if
     * the player has all of the same symbol on reels [1,2,3,4], we could tease this symbol
     * type on reel 5, during an extended spin stop sequence).
     */
    getNextStopPhaseSymbol() : number;
}