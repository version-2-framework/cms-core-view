/**
 * Blob of data about currently playing music.
 */
interface SoundControllerMusicData
{
    musicId : string;

    musicVolume : number;
}

interface SoundControllerApi
{
    /**
     * Toggles the active state of sound: if sound is currently enabled, a call to this will flip it to
     * disabled, and vice-versa. This method will automatically take care of saving the new state of
     * sound to local preferences.
     */
    toggleSound() : void;

    /**
     * Toggle music state.
     */
    //toggleMusic() : void;

    /**
     * Returns a blob of data about currently playing music. Useful for when you want to stop existing
     * music, and restore it at a later point
     */
    getCurrMusicData() : SoundControllerMusicData;

    /**
     * Sets the enabled state of sound to a specific value. Automatically takes care of saving the new
     * state of "is sound enabled" to local preferences.
     * @param newValue 
     * The new value of "is sound enabled".
     */
    setSoundEnabled(newValue : boolean) : void;

    /**
     * Sets the enabled state of music to a specific value. Automatically takes care of saving the new
     * state of "is music enabled" to local preferences. Any background music will be faded in / out
     * with a short transition (but only if the new state of music is different to the old state).
     * @param newValue 
     * The new value of "is music enabled".
     */
    //setMusicEnabled(newValue : boolean) : void;

    /**
     * Returns the current enabled state of sound.
     * @returns
     * True to indicate that sound is currently enabled, false to indicate that it is disabled.
     */
    getSoundEnabled() : boolean;

    /**
     * Returns the current enabled state of music.
     * @returns
     * True to indicate that music is currently enabled, false to indicate that it is disabled.
     */
    //getMusicEnabled() : boolean;

    /**
     * Plays a sound with a specified id.
     * @public
     * @param soundId
     * The string id of the sound file to fetch. This should not include file type, eg: if the
     * file that you loaded was "MySound.mp3", just pass "MySound" to this parameter.
     * @param [loop]
     * Indicates if the played sound should loop. If not specified, defaults to false (no loop).
     * @param [volume]
     * Specifies the volume (a floating point number, in the range of 0 to 1) at which the sound
     * should be played. If not specified, defaults to 1 (maximum volume).
     * @param [sprites]
     * TODO: Still not actually sure what this parameter is used for, or why it needs to exist.
     */
    playSound(soundId:string, loop?:boolean, volume?:number, sprites?:[]|null) : Howl;

    /**
     * Returns a sound instance, preconfigured with the settings passed. When calling this
     * method, the sound is not played: you will need to take care of that. This method is useful
     * to call directly, for cases where you may want to maniupulate a playing sound in real-time
     * (as opposed to playSound, which plays a one-off sound).
     * Sounds returned by this method respect the general api of "isSoundEnabled", eg: if you
     * get a sound, and play it, you do NOT need to check if sound is enabled (the underlying
     * sound API manages this). Toggling the enabled state of the soundController means that the
     * sound will be muted / un-muted automatically, as required.
     * @public
     * @param soundId
     * The string id of the sound file to fetch.
     * @param [loop]
     * Indicates if the played sound should loop. If not specified, defaults to false (no loop).
     * @param [volume]
     * Specifies the volume (a floating point number, in the range of 0 to 1) at which the sound
     * should be played. If not specified, defaults to 1 (maximum volume).
     * @param [sprites]
     * TODO: Still not actually sure what this parameter is used for, or why it needs to exist.
     * @returns {Howl | null}
     */
    getSound(soundId:string, loop?:boolean, volume?:number, sprites?:[]) : Howl;

    /**
     * Starts a piece of music playing, treating it as the main piece of background music to play.
     * The id of the music passed will be cached for future reference, so that we can restore main
     * game music, as required. In general, we only want a single call to this method during the
     * life-cycle of a game client (or no calls, if no main game music is required).
     * @param musicId
     * The id of the main game music to play.
     * @param {number} volume
     * The gain of this piece of music (from 0 to 1 inclusive)
     */
    startMainGameMusic(musicId : string, volume? : number) : void;

    /**
     * Restores any main game music to playing. This method can be safely called, whenever your game
     * is returning to a sound context in which we want the main game music to be available. It will
     * check if any main game music has been configured (it is configured by a call to "start main
     * game music"), if no main game music was configured, then this method is a noop (although it
     * will kill any currently playing music). In addition, if main game music is currently playing,
     * this is also a noop. Basically, it is safe to call at any moment where you want main game
     * music to definitely play - it wont trigger the music to play twice!
     */
    restoreMainGameMusic() : void;

    /**
     * Restores the last music context that was played.
     */
    restoreLastMusic() : void;

    /**
     * Starts a piece of background music playing. The SoundController supports the notion of playing
     * a single piece of background music at a time, and this method takes care of that. If some music
     * is already playing, there will be a cross-fade transition, the duration of which is specified
     * by parameter "crossFadeDuration". The cross-fade is not quite a cross-fade: the old music is
     * actually faded out before the new music is faded in. The total duration of this transition is
     * actually double the value of crossFadeDuration (this duration specifies the length of *each
     * of the 2 parts of the cross fade*). This behaviour could be changed in the future (so that the
     * total duration is actually the value passed), but for now has been kept in order to maintain
     * backwards compatibility with any existing code which uses this api.
     * @public
     * @param musicId
     * The string id of the sound file to be used as music.
     * @param [crossFadeDuration]
     * The duration (in seconds) of any cross-fade, from an existing piece of background music that is
     * playing, into the new piece of background music. If not specified, then a default cross fade
     * duration will be applied. It's valid to pass a value of 0 for instant cross-fading as well.
     * @param [volume]
     * The volume at which the new piece of music will be played.
     */
    playMusic(musicId:string, crossFadeDuration?:number, volume?:number): void;

    /**
     * Stops any current piece of background music from playing. This is done using a cross-fade, the
     * duration of which can be customized (alternatively, it's possible to omit the cross-fade completely).
     * Using this method, no new piece of background music will be started.
     * @public
     * @param [fadeOutDurationSecs]
     * The duration over which the music will be faded out, in seconds. Omitting this parameter
     * means that a default duration will be applied. Specifying a value of 0 means that there will
     * be no cross-fade: current music will be stopped immediately.
     */
    stopMusic(fadeOutDurationSecs?:number) : void;
    
    /**
     * Sets the gain of background music: this is NOT the volume of a specific piece of music,
     * it is the volume that all music will be played at. An individual piece of music plays at
     * a volume, which is the product of this, and of its own relative volume setting. This
     * should not be changed as part of game settings: instead, it is for cases where you do not
     * want to stop music playing during the game (eg: during a custom Big Win sequence), but
     * want to turn the volume down for a bit.
     * @param volume
     * A number between 0 and 1.
     */
    setMusicGain(volume : number) : void;

    /**
     * Returns the current value for Music Gain.
     */
    getMusicGain() : number;
}

/**
 * List of all audio file types that are supported (at present) by Sound Controller.
 */
type SupportedAudioFileType = "mp3" | "flac";