/**
 * Represents a bounded area.
 */
interface Bounds
{
    /**
     * Left-most x position of the area.
     */
    x : number;

    /**
     * Top-most y position of the area.
     */
    y : number;

    /**
     * Central x position of the area.
     */
    centerX : number;

    /**
     * Central y position of the area.
     */
    centerY : number;

    /**
     * Total width of the area.
     */
    width : number;

    /**
     * Total height of the area.
     */
    height : number;
}