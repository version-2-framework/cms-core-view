interface GameFactoryApi
{
    /**
     * Creates and returns an instance of the Loading Screen View to use for the game client.
     */
    getLoadingScreenView() : PIXI.DisplayObject;

    /**
     * Creates and returns an instance of the LoadingSpinnerView that should be used for this
     * Game Client. Loading Spinner View should be added to MainView.
     */
    getLoadingSpinner() : LoadingSpinnerView;

    /**
     * Creates the main UI for the device. Wil automatically select the appropriate implementation
     * (desktop, mobile, table, etc)
     */
    getUI();

    /**
     * Returns default game logo instance to use for the GUI.
     * @param centerPosX 
     * @param centerPosY 
     * @param targetHeight 
     */
    getGameLogo(centerPosX, centerPosY, targetHeight);


    /**
     * Instantiates the implementation of the Services API to use.
     */
    getServices(dependencies : Dependencies) : ServicesApi;

    /**
     * Returns the class which should be instantiated for the Spin Phase model.
     */
    getSpinModel(dependencies : Dependencies) : SpinPhaseModel;

    /**
     * Creates and returns the instance of the Spin Phase Presentation Builder that should be
     * used for this Game Client.
     * @param {Dependencies} dependencies
     */
    getSpinPresentationBuilder(dependencies) : SpinPhasePresentationBuilder;

    /**
     * Returns the class which should be instantiated for the Bonus Phase Model.
     */
    getBonusModel(dependencies : Dependencies) : BonusPhaseModel;

    /**
     * Creates and returns the SpinSoundController instance to use for the game.
     * @param soundController
     * SoundController instance, which gets used by Spin Sound Controller
     */
    getSpinSoundController(soundController : SoundControllerApi) : SlotSpinSoundController;

    /**
     * Creates and returns the instance of the main SlotGameview to use.
     */
    getSlotGameView() : PIXI.DisplayObject;

    /**
     * Creates and returns an instance of the game's main background view.
     */
    getBackgroundView() : PIXI.DisplayObject;

    /**
     * Creates and returns an instance of a Symbol View, that should be used on the
     * primary hand of the game.
     */
    getSymbol() : SymbolView;

    /**
     * Creates and returns a new instance of a ReelGroup.
     */
    getReelGroup() : PIXI.DisplayObject;

    /**
     * Creates and returns an instance of a Reel View
     * @public
     * @param reelGroup
     * The... group the reel is added to. No idea whatsoever why on earth this would be needed:
     * Wiener didn't document it.
     * @param symbolContainer 
     * The container into which symbol graphics are added.
     * @param spinnerOverlayContainer
     * A container, into which spinner implementations can write any custom graphics which must
     * be shown above the symbols.
     * @param reelIdx
     * The index of the reel, within the hand that it is a member of.
     */
    getReel(
        reelGroup,
        symbolContainer:PIXI.Container,
        spinnerOverlayContainer:PIXI.Container,
        reelIndex:number) : ReelView;

    /**
     * Returns the spinner to use for the standard reels.
     * @param reel
     * This is the "reelview" which the spinner needs some kind of knowledge of.
     * However, the public api is not yet clear
     * @param {number} reelIndex
     * The index of the reel to get a spinner for.
     */
    getSpinner(reel:ReelView, reelIndex:number) : Spinner;

    /**
     * Returns a Tall Symbols Generator to use for a specific reel.
     * @param reelIndex 
     * The index of the reel to fetch a Tall Symbols Generator for. It would be valid to
     * use different tall symbol generator mechanics, depending on the reel in use.
     */
    getTallSymbolsGenerator(reelIndex:number) : TallSymbolsGenerator;
    
    /**
     * Returns a new instance of a Win Presentation overlay.
     * @param foreground
     * Layer placed above the reels (and reels-frame), into which Win Presentation View may
     * draw custom graphics.
     * @param background
     * Layer placed below the reels (but above the main game background), into which Win Presentation
     * View may draw custom graphics.
     */
    getWinPresentationView (foreground:PIXI.Container, background:PIXI.Container) : Object

    /**
     * Creates and returns an instance of the Winlines View to use.
     */
    getWinlinesView() : PIXI.DisplayObject;

    /**
     * Creates and returns an instance of a simple Win Value animation.
     * @param winValueToShow
     * The value to show. All wins should be shown as basic numberical values when using
     * the Win Value (it doesn't support formatted text). For example, a credit win must
     * be shown simply as its basic integer value (eg: 100$ == 10000)
     * @param durationInSeconds
     * The duration that the Win Value should be shown for.
     */
    getWinValue(value:number, duration:number) : PIXI.DisplayObject;

    /**
     * Creates and returns an instance of a Big Win animation.
     */
    getBigWin() : BigWinView;

    // TODO: (Russell 08.12.2020) This isn't used anywhere as far as I know: i think it can
    // be removed safely (It's old Wiener code, not sure what it was intended for)
    getPromptView(data, animIn, animOut);

    /**
     * Creates and returns an intsance of the Game Menu view.
     */
    getMenuView() : PIXI.DisplayObject;

    /**
     * Creates and returns an instance of the Session Stats view.
     */
    getSessionStats() : PIXI.DisplayObject;

    /**
     * Creates and returns a Spin Button instance to use for the standard Wmg mobile GUI.
     */
    getSpinButtonMobile() : PIXI.DisplayObject;

    /**
     * Creates and returns a Spin Button instance to use for the standard Wmg desktop GUI.
     */
    getSpinButtonDesktop() : TooltipEnabledGuiButton;

    /**
     * Creates and returns a Bet Button instance to use for the standard Wmg mobile GUI.
     */
    getBetButtonMobile() : PIXI.DisplayObject;

    /**
     * Creates and returns a Bet Button instance to use for the standard Wmg desktop GUI.
     */
    getBetButtonDesktop() : TooltipEnabledGuiButton;

    /**
     * Creates and returns an Autoplay Button instance to use for the standard Wmg mobile GUI.
     */
    getAutoplayButtonMobile() : PIXI.DisplayObject;
    
    /**
     * Creates and returns an Autoplay Button instance to use for the standard Wmg desktop GUI.
     */
    getAutoplayButtonDesktop() : TooltipEnabledGuiButton;

    /**
     * Creates and returns an instance of the Mobile GUI to use for this game client.
     */
    getUiMobile() : PIXI.DisplayObject;

    /**
     * Creates and returns an instance of the Desktop GUI to use for this game client.
     */
    getUiDesktop() : PIXI.DisplayObject;

    /**
     * Creates and returns a Winline Button for a specific winline id.
     * @param lineId
     * The id (in the range of 1+) of the winline to get a button for.
     */
    getWinlineButton(lineId:number):PIXI.DisplayObject;

    /**
     * Creates and returns the Spin Indicator view component (usable either on mobile or desktop layouts)
     */
    getSpinIndicator() : PIXI.DisplayObject;

    /**
     * Creates and return the UITexts view component (usable either on mobile or desktop layouts)
     */
    getUITexts() : PIXI.DisplayObject;

    /**
     * Creates and returns the MessageBar view component (usable either on mobile or desktop GUIs)
     */
    getMessageBar() : PIXI.DisplayObject;

    /**
     * Creates and returns a controller, which will issue orders to any active Message Bars. (Orders are
     * issued via the global events channel, and are themselves based on global events that occur).
     */
    getMessageBarController() : Object;

    /**
     * Creates and rertuns an instance of the Cashier View to use.
     */
    getCashier() : PIXI.DisplayObject;

    /**
     * Creates and returns an instance of the Autoplay Menu to use.
     */
    getAutoplayMenu() : PIXI.DisplayObject;

    /**
     * Creates and returns an instance of the Bet Settings menu to use.
     */
    getBetMenu() : PIXI.DisplayObject;
    
    /**
     * Creates and returns an instance of the Total Winnings popup to use for the Slot View.
     */
    getTotalWinningsPopup() : TotalWinView;

    /**
     * Creates and returns an instance of the Total Winnings popup to use for the Bonus View.
     */
    getBonusTotalWinningsPopup() : TotalWinView;

    // TODO: This almost certainly needs its own basic api.
    /**
     * Creates a new Dialog View, configured with a specific View Model.
     * @param dialogViewModel 
     */
    getDialog(dialogViewModel:DialogViewModel) : PIXI.DisplayObject;

    /**
     * @public
     */
    getFreeSpinStartNotification() : PIXI.DisplayObject;
    
    /**
     * Creates and returns an instance of the URL Selection view to use.
     */
    getURLSelectionScreen() : PIXI.DisplayObject;

    // TODO: Typing on this needs fixing.
    /**
     * Creates a rounded rectangle with properties passed from a Config file.
     * Used by UI stats background and message bar background.
     * Creates a rounded rectangle with properties passed from a Config file.
     * Used by UI stats background and message bar background.
     */
    getRoundedRect(cfg);
}