/**
 * Any component which can generate tall symbols data.
 */
interface TallSymbolsGenerator
{
    /**
     * Takes a set of symbol ids for a single reel, and generates tall symbols data from it.
     * @param symbolIds
     * The symbol ids that will be shown on the reel when it stops.
     * @return
     * A list of "Tall Symbol" data items, representing each tall symbol to show on the reel.
     */
    generateTallSymbolsData(symbolIds : number[]) : TallSymbolsData[];
}

/**
 * Data to describe a tall symbol that has been found on the reels.
 */
interface TallSymbolsData
{
    /**
     * Id of the symbol involved in this tall symbo..
     */
    symbolId : number;

    /**
     * The start index (within the visible reels only) of the tall symbol found. EG:
     * if the reel has 4 visible symbols [3, 7, 7, 7], then the symbol 7 (which is a
     * Tall3) has a startSymIndex of 1.
     */
    startSymIndex : number;

    /**
     * The final symbol index (within the visible reels) of the tall symbol found.
     */
    finalSymIndex : number;

    /**
     * The number of symbols involved in the tall symbol.
     */
    numSymbols : number;
}

/**
 * Defines the animation for a single tall symbol.
 */
interface TallSymAnimData
{
    frames : PIXI.Texture[];

    anchor : { x:number, y:number }

    speed : number;

    loop : boolean;
}