/**
 * Configuratoin object that we pass to our custom Particle Emitter class
 */
type ParticleEmitterConfig =
    SimpleParticleEmitterConfig |
    AnimatedParticleEmitterConfig |
    PathParticleEmitterConfig;

interface BaseParticleEmitterConfig
{
    /**
     * Configuration for the particles.
     */
    config : PIXI.particles.EmitterConfig | PIXI.particles.OldEmitterConfig;
}

interface OldParticleEmitterConfig extends BaseParticleEmitterConfig
{
    textures : *;

    /**
     * Custom particle constructor: this will not be used if a standard type is
     * applied.
     */
    particleConstructor ? : *;
}

/**
 * Configures a Simple Particle emitter. The standard particle constructor will
 * be automatically used, when this configuration is passed.
 */
interface SimpleParticleEmitterConfig extends BaseParticleEmitterConfig
{
    particleType : "simple";

    /**
     * Set of textures to pick from for particles (or the ids of textures)
     */
    textures : PIXI.Texture[] | string[];
}

/**
 * Configures an Animated Particle. The Animated particle constructor will be
 * automatically used, when this configuration is passed.
 */
interface AnimatedParticleEmitterConfig extends BaseParticleEmitterConfig
{
    particleType : "animated";

    /**
     * Configures one or more animations for the animated particle textures
     */
    textures : PIXI.particles.AnimatedParticleArt[];
}

interface PathParticleEmitterConfig extends BaseParticleEmitterConfig
{
    particleType : "path";
}