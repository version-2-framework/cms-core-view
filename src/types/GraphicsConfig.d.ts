/**
 * Simple utlity definition for an x/y coordinate of some kind.
 */
interface Point
{
    /**
     * X position
     */
    x : number;

    /**
     * Y position
     */
    y : number;
}

/**
 * Configuration for a winline, adding additional data to specify how any associated
 * graphics might be drawn.
 */
interface GraphicalWinlineConfig extends WinlineConfig
{
    /**
     * The colour that the winline graphic should be drawn with.
     */
    colour : number | string;

    /**
     * The vertical position of any winline number button associated with the winline. This
     * should be a value, where 1 == 1 * SymbolHeight. 0 represents the very top of the reels:
     * if the reels have 3 symbols, then 3 represents the absolute bottom of the reels. A
     * position of 1.5, would be "vertically half way down the second symbol on the reel".
     * 
     * This field (as well as "btnIsLeft") must be specified, if you want a winline number
     * indicator to be drawn with the winline.
     */
    btnPosY ? : number;

    /**
     * Indicates if the winline number button should be positioned on the left of the reels.
     * If false, it will be positioned on the right of the reels.
     */
    btnIsLeft ? : boolean;

    /**
     * A set of customized positions, that the winline should be drawn through. Each position
     * is a scaled value. An x position of 1 represents "1 symbol wide", and a y position of 1
     * represents "1 symbol tall". So, a position of {x:0.5,y:05} would start at the center of
     * the top left symbol.
     */
    linePoints ? : Point[];
}