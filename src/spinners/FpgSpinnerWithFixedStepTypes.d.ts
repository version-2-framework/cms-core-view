interface FpgSpinnerWithFixedStepConfig
{
    /**
     * The distance to overshoot at the end: this should be in the range 0
     * to 1, where 1 is the maximum (values less than 0 or greater than 1
     * will automatically be clamped to the appropriate min / max value).
     * A value of 0 doesn't mean "no overshoot", it means "minimum overshoot"
     * (the actual scale is internally hard-coded in the spinner)
     * 
     * A value of 0 means an overshoot of about 0.125 symbol heights (including
     * any vertical gap between symbols). A value of 1, means an additional
     * overshoot amount of about 4 * the minimal amount.
     * 
     * When using a slot layout where vertical gap between symbols is minimal,
     * you may well need to use an overshoot amount larger than 0, in order to
     * get the symbols to feel like they travel far enough.
     */
    overshootAmount : number;
}