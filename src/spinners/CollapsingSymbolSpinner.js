/**
 * @typedef SymbolCell
 * @property {SymbolConfig} symbolConfig
 * @property {boolean} occupied
 */

import { getGameViewLogger } from "../../../cms-core-logic/src/logging/LogManager";
import C_ViewEvent from "../const/C_ViewEvent";

const log = getGameViewLogger();

const SpinState =
{
    RESTING : 0,
    DROPPING : 1,
    DROPPED : 2,
    DROP_IN_NEW_SYMBOLS : 3
}

/**
 * Default configuration for the Collapsing Symbol Spinner.
 * @type {CollapsingSymbolSpinnerConfig}
 */
export const DEFAULT_SPINNER_CONFIG =
{
    clearAllSymbolsDropTime : 0.6,
    createNewSymbolsDropTime : 0.6,
    respinDelayBetweenSymbolsDropping : 0.05,
    respinHeldSymbolDropTime : 0.6,
    respinSpawnedSymbolDropTime : 0.15
};

/**
 * A Spinner Implementation, which creates collapsing Symbols across Respins (just like GonzosQuest)
 * @implements {Spinner}
 */
export class CollapsingSymbolSpinner
{
    /**
     * Constructs a new Collapsing Symbol Spinner.
     * @param {ReelView} reel
     * @param {CollapsingSymbolSpinnerConfig} [spinnerConfig]
     * An optional Collapsing Spinner Config to use for this reel. If not specified, the Collapsing
     * Spinner will look for GameConfig.spinnerConfig, which will be applied for all reels (and if that
     * is not specified, it falls back to using default config). Use this parameter, when you want to
     * specify custom spin configs for each reel.
     */
    constructor(reel, spinnerConfig=null)
    {
        log.info('CollapsingSymbolSpinner.created()');

        /**
         * @private
         */
        this._config = PIXI.app.config;

        /**
         * @private
         * @type {CollapsingSymbolSpinnerConfig}
         */
        this._spinnerConfig = DEFAULT_SPINNER_CONFIG;
        if (spinnerConfig) {
            this._spinnerConfig = spinnerConfig;
        }
        else
        if (this._config.spinnerConfig) {
            this._spinnerConfig = this._config.spinnerConfig;
        }

        /**
         * @private
         * @type {TallSymbolsData[]}
         */
        this._tallSymbolsData = [];

        /**
         * @private
         * @type {ReelView}
         */
        this._reel = reel;

        /**
         * Tracks the current spin phase which may be in progress.
         * @private
         * @type {number}
         */
        this._spinState = SpinState.RESTING;

        /**
         * Timeline implementing current animation sequence. Kill this, to abort any action in progress.
         * @private
         * @type {gsap.TimelineMax}
         */
        this._currTimeline = null;

        // Hmm, will using a "state data" object be a bit cleaner ?
        // - This object could be null - implying no spin is in progress.
        // - SpinState.NONE could be dropped
        // - SpinState is a property of this object
        // - this object could be called "currSpin"
        /**
         * @private
         */
        this._spinData =
        {
            /**
             * Indicates if this is a "respin", where some symbols might be held from the
             * previous spin (and not collapsed). We have 2 different simulations available
             * for this scenario.
             * @type {boolean}
             */
            isStickyRespin : false,

            // TODO: Should we cache this? Should it be passed in, or just cached locally ?
            /**
             * @type {SymbolConfig[]}
             */
            startSymbols : null,

            /**
             * The final symbols to show for the current spin.
             * @type {SymbolConfig[]}
             */
            finalSymbols : null,

            /**
             * Indicates if each of the symbol positions is held from the previous spin. By
             * convention, sticky symbol positions are supplied on the spin they are applied
             * to (eg: if we have 2 spins, and we hold some symbols from spin 1 into spin 2 as
             * sticky, then the sticky symbol positions are supplied as part of the 2nd spin
             * result). This makes sense for normal sticky spins - it means we can simulate the
             * 2nd spin using just the 2nd spin result (if we want to show previous symbol ids,
             * of course we still need the 1st spin result). For "collapsing" spins, this makes
             * slightly less sense, but by convention, we will keep the sticky symbols data as
             * part of the 2nd spin result: however, the the data will refer to positions from
             * the 1st spin which are held as sticky (in fact, this is semantically the same as
             * a non-collapsing spin) - its just that those positions will have moved for the
             * 2nd spin. We cannot simulate the collapse independently (without knowing the data
             * from the 1st spin): we may as well continue with the same convention for where
             * the data is placed.
             * @type {boolean[]}
             */
            positionIsHeld : null
        }
    };


    /**
     * @public
     * @inheritDoc
     */
    get tallSymbolsData() {
        return this._tallSymbolsData;
    };


    /**
     * @public
     * @inheritdoc
     * @type {number}
     */
    get numSymAboveVisibleArea() {
        return 0;
    };


    /**
     * @public
     * @inheritdoc
     * @type {number}
     */
    get numSymBelowVisibleArea() {
        return 0;
    };


    /**
     * @inheritDoc
     * @public
     */
    startInfiniteSpin()
    {
        if (this._spinState != SpinState.RESTING) {
            return;
        }

        log.info(`CollapsingSymbolSpinner[${this._reel.reelIndex}].startInfiniteSpin()`);

        // It seems better to show the symbols as idle here, when we are not really
        // performing an actual "spin".
        this.setAllSymbolsToIdleState();
    };


    // TODO: The new api for this method, needs to accept meta-data which tells us something about
    // the nature of the spin being executed. This is mainly to support re-spin cases, where we hold
    // some symbols. We need to know if this is one of those cases, in an abstract way.
    /**
     * @inheritDoc
     * @public
     * @param {SymbolConfig[]} finalSymbols
     * @param {number} spinType
     * @param {boolean[]} positionIsSticky
     */
    spinToSymbols(finalSymbols, spinType=0, positionIsSticky=null)
    {
        log.info(`CollapsingSymbolSpinner[${this._reel.reelIndex}].spinToSymbols(spinType:${spinType},positionIsSticky:[${positionIsSticky}])`);

        this._spinData.finalSymbols = finalSymbols;
        this._spinData.isStickyRespin = spinType > 1;
        this._spinData.positionIsHeld = positionIsSticky;

        this.setAllSymbolsToSpinningState();

        // Here, i am testing out the "execute the spin all in one step", where some
        // symbols are destroyed, some held

        if (this._spinData.isStickyRespin)
        {
            this._spinState = SpinState.DROP_IN_NEW_SYMBOLS;
            this.dropInSomeNewSymbols();
        }
        else
        {
            this._spinState = SpinState.DROP_IN_NEW_SYMBOLS;
            this.dropAwayAllSymbols();
        }
    };


    /**
     * @public
     * @inheritDoc
     * @param {boolean} isEnabled 
     */
    setQuickStopEnabled(isEnabled)
    {
        /**
         * Tracks whether QuickStop is currently enabled for this Spinner.
         * @private
         * @type {boolean}
         */
        this.isQuickStopEnabled = isEnabled;
    };


    /**
     * @public
     * @inheritDoc
     * @return {boolean}
     */
    getIsQuickStopEnabled()
    {
        return this.isQuickStopEnabled;
    };


    /**
     * Drops away all existing symbols from the reels.
     * @private
     */
    dropAwayAllSymbols()
    {
        log.debug(`CollapsingSymbolSpinner[${this._reel.reelIndex}].dropAwayAllSymbols()`);

        let symbolGraphics = this._reel.symbols.slice();
        let numSymOnReel = symbolGraphics.length; // access from the reel property ?
        let symStep = this._reel.symStep;

        let timeline = new TimelineMax();
        timeline.addLabel("start");
        
        // This will animate the symbols dropping, with a stagger
        symbolGraphics.forEach((symbolGraphic, symbolIndex) =>
        {
            // There are a lot of magic numbers here presently, and potentially, this is how it will
            // stay! 
            let mainDropTime = this._spinnerConfig.clearAllSymbolsDropTime; // 0.6;
            let delayBeforeSymbolDrops = 0.05 * (numSymOnReel - 1 - symbolIndex);
            let startPosY = symbolGraphic.y;
            let finalPosY = startPosY + (numSymOnReel * symStep);
            let labelId = `sym${symbolIndex}`;

            log.debug(`[${this._reel.reelIndex}]: symStep:${symStep}, numSymOnReel:${numSymOnReel}, startPosY:${startPosY}, finalPosY:${finalPosY}`);

            // A single eased in tween is working fairly well so far
            timeline.addLabel(labelId, `start+=${delayBeforeSymbolDrops}`);
            timeline.to(symbolGraphic, mainDropTime, { y:finalPosY, ease:"Expo.easeIn" }, labelId);
        });

        // When the timeline completes, if the new symbols are already queued,
        // we can skip straight to dropping them in. Otherwise, we have to wait
        // for the public stop() method to be called - for this reason, we set
        // our spinPhase to DROPPED (when stop is called, if the drop has finished,
        // THAT'S when we skip to dropping in new symbols).
        timeline.call(() => {
            this._spinState = SpinState.DROPPED;
            if (this._spinData.finalSymbols) {
                this.dropInAllNewSymbols();
            }
        });

        this._currTimeline = timeline;

        // TODO: Originally, Wiener didnt like my event for "all spins started" - i remember the conversation - but in fact,
        // this mechanic is a more appropriate way of implementing what is needed.
    };


    


    /**
     * Executes the animation of dropping in an entirely new set of symbols. This particular method
     * must only invoked internally, and is called when all existing symbols have been cleared: the
     * animation to drop in entirely new symbols is very specific (and different from the animation
     * when only certain new symbols are dropped in). This method is private, and does not implement
     * any guards against being called at the wrong time - the calling methods should do this.
     * @private
     */
    dropInAllNewSymbols()
    {
        log.debug(`CollapsingSymbolSpinner[${this._reel.reelIndex}].dropInAllNewSymbols()`);

        this._spinState = SpinState.DROP_IN_NEW_SYMBOLS;

        // We don't really need to re-order the symbols in our symbols array, they are already
        // in the correct basic order. We just need to animate them into place.

        let timeline = new TimelineMax();
        timeline.addLabel("start");

        let newSymbolIds = this._spinData.finalSymbols;
        let symbolGraphics = this._reel.symbols.slice();
        let numSymOnReel = symbolGraphics.length; // Fetch from Reel ??
        let symHeight = this._config.symHeight;
        let symStep = this._reel.symStep;
        
        symbolGraphics.forEach((symbolGraphic, symbolIndex) =>
        {
            // reel.yOrigin is pretty useless in its current form - if we are still required to calculate
            // an offset manually, remembering to take reelsMargin into account. This needs to change.
            let finalPosY = (symStep * symbolIndex) + (symHeight * 0.5) + this._reel.yOrigin + this._config.reelsMarginTop;
            let startPosY = finalPosY - (symStep * numSymOnReel);
            
            let delayBetweenSymbols = 0.05 * (numSymOnReel - 1 - symbolIndex);
            let mainDropTime = this._spinnerConfig.createNewSymbolsDropTime; // 0.6;
            let newIdForSymbol = newSymbolIds[symbolIndex].id;

            symbolGraphic.setId(newIdForSymbol);

            timeline.set(symbolGraphic, { y:startPosY }, "start");
            timeline.to(symbolGraphic, mainDropTime, { y:finalPosY, ease:"Expo.easeIn" }, `start+=${delayBetweenSymbols}`);
        });

        timeline.call(() => {
            this.publishReelStoppedNotification();
            this._spinState = SpinState.RESTING;
            this.clearSpinData();
        });

        this._currTimeline = timeline;
    };


    /**
     * @inheritDoc
     * @public
     */
    abortSpin()
    {
        log.info(`CollapsingSymbolSpinner[${this._reel.reelIndex}].abortSpin()`);

        this.clearCurrentTimeline();
        this._spinState = SpinState.RESTING;
    };


    // TODO: The name needs to express the concept better. This is basically a respin -
    // we know the start and end symbols already. Some symbols are destroyed and removed.
    // This method effectively executes that step as well.
    /**
     * 
     * @private
     */
    dropInSomeNewSymbols()
    {
        log.info(`CollapsingSymbolSpinner[${this._reel.reelIndex}].dropInSomeNewSymbols()`);

        let symStartY = this._reel.yOrigin;
        let symStep = this._reel.symStep;
        let numSymOnReel = this._reel.symbols.length;
        let positionIsHeld = this._spinData.positionIsHeld;

        // If the positionIsHeld (stickySymbols) data doesnt exist or is too short, we fail
        // gracefully, by making sure the rest of this method runs, but with no symbols being
        // treated as sticky. However, we shouldn't end up in this condition if the data passed
        // to the spinner is good: therefore, we need to log the error.
        if (!positionIsHeld || positionIsHeld.length < numSymOnReel) {
            log.error(`CollapsingSymbolSpinner.reel[${this._reel.reelIndex}]: positionIsHeld data = ${positionIsHeld}, expected array with ${numSymOnReel} boolean flags!`);

            positionIsHeld = [];
            for (let i = 0; i < numSymOnReel; i ++) {
                positionIsHeld.push(false);
            }
        }
        
        // Move all symbols to their new starting positions, and then re-sort the order of the
        // symbols array based on this y position. To perform the *real* effect, we would need
        // to show some symbols being destroyed as well. I am not yet 100% sure where we would
        // implement that (is it an effect in the symbol animation layer? I think i prefer an
        // overlay effect). For now, getting the animation looking about right for the drop
        // step will be sufficient.
        let numSymDestroyedSoFar = 0;
        this._reel.symbols.forEach((symbolGraphic, symbolIndex) => {
            let isDestroyed = positionIsHeld[symbolIndex] === false;
            if (isDestroyed) {
                numSymDestroyedSoFar += 1;
                symbolGraphic.y = symStartY + (symStep * 0.5) - (symStep * numSymDestroyedSoFar);
            }
        });

        let numSymbolsDestroyedOnReel = numSymDestroyedSoFar;
        let numSymbolsKeptOnReel = numSymOnReel - numSymbolsDestroyedOnReel;

        this._reel.sortSymbolsByY();

        // Generate an array, containing the number of positions that each symbol has to
        // be dropped. This array has the same length as the number of visible symbols on
        // the reel. It is in the same order (index 0 == top element, index 1 == second
        // element from the top, etc). The value at each index is the number of positions
        // which the symbol must be dropped in the subsequent animation.
        // This is important to understand: the actual indices refer to the order of symbols
        // AFTER we already resorted them.
        // Suppose we have 5 symbols on the reel. We are holding 2 symbols from the previos
        // spin = [NotHeld, Held, NotHeld, NotHeld, Held] - the held symbols @ indices [1,4]
        // are effectively moved to new indices (3,4). We have already moved the other
        // symbols off the top of the screen, and re-ordered the array, so that indices [0,1,2]
        // represent the top 3 symbols (not currently seen).
        // So, this following calculation will produce an output of [3,3,3,2,0] - after the
        // re-sort, the 3 symbols off the top of the screen are all dropping 3 positions,
        // the top-most of the 2 symbols left has to drop 2 positions, and the bottom symbol
        // isn't going to move at all.
        
        let numPosToDropSym = [];
        for (let i = 0; i < numSymOnReel; i ++) {
            numPosToDropSym.push(0);
        }

        // Inside a bracket, just to keep some local declarations inside a scope, and not
        // visible to the later parts of this overall method.
        {
            let symPosIsOccupied = positionIsHeld.slice();
            let numSymDroppedForReel = 0;

            for (let posIndex = numSymOnReel - 1; posIndex >= 0; posIndex --)
            {
                // This symbol is destroyed, we must search upwards until we
                // find the next non-destroyed symbol.
                if (symPosIsOccupied[posIndex] === false) {
                    symPosIsOccupied[posIndex] = true;

                    let numPlacesToMove = 1;
                    let foundSymToDropInToEmptyPos = false;

                    for (let i = posIndex - 1; i >= 0; i --) {
                        if (symPosIsOccupied[i]) {
                            symPosIsOccupied[i] = false
                            foundSymToDropInToEmptyPos = true;
                            numSymDroppedForReel += 1;
                            break;
                        }
                        else
                        {
                            numPlacesToMove += 1;
                        }
                    }

                    // If we didnt find a symbol above this empty one to drop, then we
                    // are going to drop in a newly spawned symbol. How far does the
                    // newly spawned symbol have to drop? Its the number of symbols that
                    // are destroyed (overall) for this reel.
                    if (!foundSymToDropInToEmptyPos) {
                        numPlacesToMove = numSymbolsDestroyedOnReel;
                    }

                    numPosToDropSym[posIndex] = numPlacesToMove;
                }
                else
                {
                    numPosToDropSym[posIndex] = 0;
                }
            }
        }

        log.debug(`CollapsingSymbolSpinner[${this._reel.reelIndex}]: numPosToDropSym = [${numPosToDropSym}]`);

        // When building the timeline, we will first drop all symbols that still exist.
        // New symbols are brought in, for a secondary phase.
        let timeline = new TimelineMax();
        timeline.addLabel("start");

        let delayBetweenSym = this._spinnerConfig.respinDelayBetweenSymbolsDropping; // 0.05;
        let delayBeforeNewlyDroppedSym = 0.2;
        let heldSymbolDropTime = this._spinnerConfig.respinHeldSymbolDropTime; // 0.6;
        let spawnedSymbolDropTime = this._spinnerConfig.respinSpawnedSymbolDropTime; // 0.15;

        // This should work, but it doesn't yet add any delay between the "still existing symbols"
        // and the "newly spawned symbols"
        let numHeldSymAnimationsStarted = 0;
        let numSpawnedSymAnimationsStarted = 0;

        // TODO:
        // It might be easier to do 2 loops - first section is "up to the last held symbol", the second
        // one is only for newly spawned symbols. This should make it more practical to build the timeline
        // that is required for dropping in new symbols differently to the drop on old symbols falling
        for (let symbolIndex = numSymOnReel - 1; symbolIndex >= 0; symbolIndex --)
        {
            let symbolGraphic = this._reel.symbols[symbolIndex]
            let symbolIsNewlySpawned = symbolIndex < numSymbolsDestroyedOnReel;
            let numPosToDrop = numPosToDropSym[symbolIndex];
            
            let startPosY = symbolGraphic.y;
            let finalPosY = startPosY + (numPosToDrop * symStep);
            
            if (symbolIsNewlySpawned)
            {
                // For newly spawned symbols, we will wait until all symbols that already existed finished
                // dropping in, then we drop in the newly spawned symbols VERY quickly. That means that we
                // add our new animations to the end of the timeline.

                // TODO: Its very unlikely that this produces the correct timing... i think i should follow
                // last weeks advice, and generate the timeline from 2 separate loops

                let newIdForSymbol = this._spinData.finalSymbols[symbolIndex].id;
                timeline.call(() => symbolGraphic.setId(newIdForSymbol));
                timeline.to(symbolGraphic, spawnedSymbolDropTime, { y:finalPosY, ease:"Expo.easeIn" })
            }
            else
            {
                let delayForSym = delayBetweenSym * numHeldSymAnimationsStarted;

                timeline.to(symbolGraphic, heldSymbolDropTime, { y:finalPosY, ease:"Expo.easeIn" }, `start+=${delayForSym}`);
            }

            if (numPosToDrop > 0) {
                if (symbolIsNewlySpawned) {
                    numSpawnedSymAnimationsStarted += 1;
                }
                else numHeldSymAnimationsStarted += 1;
            }
       };

        timeline.call(() => {
            this.publishReelStoppedNotification();
        });

        this._currTimeline = timeline;
    };


    /**
     * @private
     */
    clearSpinData()
    {
        this._spinData.finalSymbols = null;
    };


    /**
     * Kills and clears any animation timeline which may be currently in progress.
     * @private
     */
    clearCurrentTimeline()
    {
        if (this._currTimeline != null) {
            this._currTimeline.kill();
            this._currTimeline = null;
        }
    };


    /**
     * Sets all symbols back to their default state. Should be called before each
     * new spin animation starts, in case any animation was still going on from the
     * previous stpin sequence.
     * @private
     */
    setAllSymbolsToIdleState()
    {
        log.debug(`CollapsingSymbolSpinner[${this._reel.reelIndex}].setAllSymbolsToIdleState()`);

        this._reel.symbols.forEach(symbolGraphic => {
            symbolGraphic.showIdleState();
        });
    };


    /**
     * Sets all symbols to Spinning state.
     * @private
     */
    setAllSymbolsToSpinningState()
    {
        log.debug(`CollapsingSymbolSpinner[${this._reel.reelIndex}].setAllSymbolsToSpinningState()`);

        this._reel.symbols.forEach(symbolGraphic => {
            symbolGraphic.showSpinningState();
        });
    };


    /**
     * @private
     */
    publishReelStoppedNotification()
    {
        this._reel.fireReelStoppedEvent();
    };
}