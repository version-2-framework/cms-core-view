/**
 * Configuration for a single PIDSpinner.
 */
interface PIDSpinnerConfig
{
    /**
     * Minimum time (in seconds) that must have elapsed, between the spin starting, and
     * being able to stop on any symbols passed in.
     */
    minStartTime : number;

    /**
     * Maximum speed that the reel may spin at. The PIDSpinner works by accelerating /
     * deccelarating (eg: changing "currentSpeed"), and then moving the reels according
     * to the value of current speed. This is the maximum value of that speed property.
     * @todo: Desparately need some indications of likely range of this value, because
     * typical value right now is "3000" - which is just meaningless to me.
     * @default 3000
     */
    maxReelSpeed : number;

    /**
     * @todo: Don't know what this represents, work out and write some documentation
     * @default 5
     */
    Ki : number;

    /**
     * @todo: Don't know what this represents, work out and write some documentation
     * @default 1
     */
    KiDamp : number;

    /**
     * @todo: Don't know what this represents, work out and write some documentation
     * @default 12
     */
    Kp : number;

    /**
     * @todo: Don't know what this represents, work out and write some documentation
     * @default 15
     */
    fastStopKp : number;

    /**
     * @todo: Don't know what this represents, work out and write some documentation
     * @default 2
     */
    fastStopKi : number;

    /**
     * @todo: Don't know what this represents, work out and write some documentation
     * @default 0.075
     */
    maxIntegrationTime : number;

    /**
     * @todo: Don't know what this represents, work out and write some documentation
     * @default 2.0
     */
    maxStopTime : number;
}