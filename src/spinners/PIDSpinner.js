import C_GameEvent from "../../../cms-core-logic/src/const/C_GameEvent";
import MathUtil from "../utils/MathUtil";
import {getGameViewLogger} from "../../../cms-core-logic/src/logging/LogManager";
import { Howl } from "howler";

/**
 * Default settings for PIDSpinner.
 * @type {PIDSpinnerConfig}
 */
export const DefaultPidSpinnerConfig =
{
    minStartTime : 0.4,
    maxReelSpeed : 3000,
    Ki : 5,
    KiDamp : 1,
    Kp : 12,
    fastStopKp : 15,
    fastStopKi : 2,
    maxIntegrationTime : 0.075,
    maxStopTime : 2.0
};

/**
 * @typedef {0 | 1 | 2} PidSpinState
 */

/**
 * The set of available Spin Phases for the PID Spinner.
 */
const PidSpinState =
{
    IDLE : 0,
    INFINITE_SPIN : 1,
    STOPPING : 2
};

/**
 * Animate reel symbols with mechanical spin animation.
 * PID stands for Proportional Integral Differential - derived from PID controllers. For more info :
 * https://en.wikipedia.org/wiki/PID_controller
 * To stop the reel a PID Controller approach is used where we compare the desired position to the current position
 * and apply control based on the difference.
 * The controlled variable is the reel's speed.
 * @implements Spinner
 */
export class PIDSpinner
{

    /**
     * @constructor
     * @param {ReelView} reel
     * The reel to be controlled
     * @param {PIDSpinnerConfig} [spinnerConfig]
     * An optional PID Spinner Config to use for this reel. If not specified, the PIDSpinner
     * will look for GameConfig.spinnerConfig, which will be applied for all reels (and if that
     * is not specified, it falls back to using default config). Use this parameter, when you
     * want to specify custom spin configs for each reel.
     */
    constructor(reel, spinnerConfig=null)
    {
        /**
         * @private
         */
        this._app = PIXI.app;

        let config = this._app.config;
        let reelIndex = reel.reelIndex;

        /**
         * Tall Symbols Generator to be used for this reel.
         * @private
         * @type {TallSymbolsGenerator}
         */
        this._tallSymbolsGenerator = this._app.gameFactory.getTallSymbolsGenerator(reelIndex);
        
        /**
         * @private
         */
        this.log = getGameViewLogger();

        /**
         * @private
         * @type {ReelView}
         */
        this._reel = reel;

        /**
         * @private
         * @type {PIDSpinnerConfig}
         */
        this._spinnerConfig = DefaultPidSpinnerConfig;
        if (spinnerConfig) {
            this._spinnerConfig = spinnerConfig;
        }
        else
        if (config.spinnerConfig) {
            this._spinnerConfig = config.spinnerConfig;
        }

        this.log.debug(`reel[${reel.reelIndex}]: PIDSpinnerConfig = ${JSON.stringify(this._spinnerConfig)}`);        

        /**
         * Tracks the current y position of the reel.
         * @type {number}
         * @private
         */
        this._currPosY = 0;

        /**
         * Caches the final target y position of the reel (what it should be a the end of the spin).
         * @private
         * @type {number}
         */
        this._finalPosY = 0;

        // TODO: Documentation of speed can be improved a lot: this spinner basically tweens the value
        // of speed, which is then used to update the spin's travel (although I am not 100% certain
        // right now that the spinner always spins at the value of "currSpeed").
        /**
         * Current speed that the reel is spinning at
         * @private
         * @type {number}
         */
        this._currSpeed = 0;
        
        /**
         * The maximum speed at which the spinner should spin when.. TODO: Document correctly
         * @private
         * @type {number}
         * @readonly
         */
        this._maxSpeed = this._spinnerConfig.maxReelSpeed;

        /**
         * @private
         * @type {number}
         */
        this._Ki = 0

        /**
         * @private
         * @type {number}
         */
        this._Kp = 0;

        /**
         * An object, used as the target reference for the spin tween. The object isn't important:
         * we are simply leveraging a tween to repeatedly update an action, and the tween needs a
         * target.
         * @private
         * @type {Object}
         */
        this._spinTweener = {};        

        /**
         * The ids of the symbols that the spinner should be stopping on.
         * @private
         * @type {number[]}
         */
        this._stopSymbols = null;

        /**
         * @private
         * @type {SlotSpinSoundController}
         */
        this._spinSoundController = this._app.spinSoundController;

        let reelRollSound = this._spinSoundController.getReelRollSound(reelIndex);
        if (reelRollSound)
        {
            /**
             * The sound to play when the reels are rolling. This sound is optional, and can be configured
             * differently between games. SpinSoundController basically tells us what sound id to use, if
             * at all.
             * @private
             * @type {Howl}
             */
            this._rollSnd = this._app.sound.getSound(reelRollSound.soundId, true, reelRollSound.maxVolume);
        }

        // TODO: Although it's not documented from wieners original code, the fact that we have a list of
        // previous "deltas" suggests that part of the spin's logic works like a low pass filter of some 
        // kind. It would be good at some point to add improved documentation, to explain better how this
        // works, and what these error fields are used for.
        /**
         * @private
         * @type {number[]}
         */
        this._errors = [];

        /**
         * @private
         * @type {number}
         */
        this._errorIndex = 0;

        /**
         * @private
         * @type {number[]}
         */
        this._errorDeltas = [];

        // TODO: Determine EXACTLY what this represents. I assume its a y value - but i am desperately struggling
        // to parse what it could mean, largely because.. the calculation seems to be nonsense. We seem to have
        // calcuilated a value somewhere partly through the reels. Why would you... unmask it there? I just cannot
        // fathom this yet.
        // TODO: THe access to all of the reel parameters here looks suspicious
        // - its not at all clear that any of these values really need to be a part of reel
        // - however, its not really clear what any of those values actually represent#

        // Orginally this access some public properties from Reel - which represented position constants. To be honest,
        // this approach seems convoluted: it implies, that as we develop more spinners which may need different 
        // calculations based on those same config parameters, we will progressively pollute the public api of reel
        // until its unmaintainable. For now, i want to strip away those public "position constant" values from reel
        // as much as possible, until it starts to become clearer to me what really needs to go there.
        /**
         * @private
         * @type {number}
         */
        this._unmaskThreshold =
            config.reelsMarginTop +
            ((config.symHeight + config.symGap) * (config.numSymsPerReel[reel.reelIndex] - 1)) +
            config.reelYOrigins[reel.reelIndex];

        // TODO: I am still not really 100% sure what disableUnmasking does. The name is a double
        // negative, which immediately makes it hard for me to understand :S !! The method it calls
        // on reel is confusing - it doesnt seem to disable unmasking at all (if i understood right),
        // instead trigger a load of stuff that has nothing directly to do with masking. This probably
        // means that the method was called WHEN "unmasking needs to be disabled", and that the method
        // name just wasn't very descriptive of what was actually hapenning.
        /**
         * @private
         * @type {boolean}
         */
        this._disableUnmasking = this._app.config.disableUnmasking === true;

        /**
         * Timeline, used to carry out the initial "move back" / "move forward" part of the spin.
         * @private
         * @type {gsap.TimelineMax}
         */
        this._startSpinTimeline = null;

        /**
         * @private
         * @type {GSAP.TweenMax}
         */
        this._spinUpdateTween = null;

        /**
         * Tracks whether a spin is currently in progress, and if so, what phase it is in. SpinState.IDLE
         * means "no spin animation is currently on-going". Any other value tells us a spin is going on,
         * and communicates the exact sub-phase of the spin currently being shown. Think of this field as
         * a simple state check: if we are already in "infiniteSpin" phase, then a second call to "show
         * infinite spin" can be safely ignored.
         * @private
         * @type {number}
         */
        this._spinState = PidSpinState.IDLE;

        /**
         * Tracks whether the stop sound has already been triggered for the current reel spin.
         * @private
         * @type {boolean}
         */
        this._stopSoundTriggered = false;

        // TODO: This is slightly separate to the new (not yet fully implemented) "speedScalingFactor",
        // although they are definitely related in how they need to be implemented.
        /**
         * A time scaling factor applied to the spin animation. When quick stopping is enabled, this
         * can be used to speed up the animation, by increasing its value. A value of 1 indicates that
         * spin animations should be played at default speed: a value of 0.5 would slow the spin down
         * to half speed, and a value of 2 would double the spin's speed.
         * @private
         * @type {number}
         */
        this._spinSpeedScalingFactor = 1;

        /**
         * Current tall symbols data (can also be null)
         * @private
         * @type {TallSymbolsData[]}
         */
        this._tallSymbolsData = null;

        /**
         * Tracks the time that the last spin tween update occurred on: can be used to calculate deltas
         * when a new spin update occurs (by comparing the new time to the previous time).
         * @private
         * @type {number}
         */
        this._lastSpinUpdateTime = 0;

        // TODO: Not 100% sure how this is used (or what for) - needs documenting
        /**
         * @private
         * @type {number}
         */
        this._currStopTime = 0;

        // TODO: This relates to the new "spinSpeedScalingFactor" - the externally settable and tweenable
        // one - which needs to both be implemented, and to play nice with the other "quick spin"
        // functionality (whch we are not currently using, but may, in the future)
        /**
         * @private
         * @type {number}
         */
        this._speedScalingFactor = 1;
    };


    /**
     * @public
     * @inheritDoc
     * @see {Spinner.numSymAboveVisibleArea}
     */
    get numSymAboveVisibleArea() {
        return 1;
    };


    /**
     * @public
     * @inheritDoc
     * @see {Spinner.numSymBelowVisibleArea}
     */
    get numSymBelowVisibleArea() {
        return 1;
    };


    /**
     * @public
     * @inheritDoc
     */
    get tallSymbolsData() {
        return this._tallSymbolsData;
    };


    /**
     * Sets current speed (used in the "speed tweening" part of spin)
     * @protected
     * @param {number} value
     */
    set currSpeed(value) {
        this._currSpeed = value;
    };


    /**
     * Returns the current speed (used in the "speed tweening" part of spin)
     * @protected
     * @return {number}
     */
    get currSpeed() {
        return this._currSpeed;
    };


    /**
     * @public 
     * @inheritDoc
     */
    startInfiniteSpin()
    {
        // Starts the "infinite spin" phase in progress.
        if (this._spinState !== PidSpinState.IDLE) {
            return;
        }

        this.log.debug(`PidSpinner[${this._reel.reelIndex}].startInfiniteSpin()`);
        
        this._spinState = PidSpinState.INFINITE_SPIN;

        this._minStartTimeElapsed = false;

        if (this._rollSnd)
        {
            this._rollSnd.play();
            this._rollSnd.volume(0);
        }

        // Set all symbols to spinning state
        this._reel.symbols.forEach(symbolGraphic => {
            symbolGraphic.showSpinningState();
        });

        // If we have tall symbols on the reel, we are going to do something here: we
        // will "flip" the tall symbol. When bringing tall symbols in from the top of
        // the reels, we use "bottom anchoring" (we set the bottom most symbol as tall,
        // and the other symbols as "involved in tall"). When starting a new spin, and
        // moving tall symbols off of the reels, we flip the anchoring of any tall
        // symbols, so that we are now using "top anchoring" (the bottom symbol is set
        // to "involved in tall", and now the top symbol is set as the "tall" symbol).
        // This method call implicitly does that for us (strictly speaking, it simply
        // sets the appropriate state for all symbols, visible and non-visible, with
        // the anchoring that we requested).
        if (this._tallSymbolsData) {
            this._reel.setAnyTallSymbolsToTallOrInvolved(false, this._tallSymbolsData);
        }

        this._errorIndex = 0;
        this._currPosY = 0;
        this._currSpeed = 0;
        this._stopSoundTriggered = false; 
        this._spinSpeedScalingFactor = 1;
        this._stopSymbols = null;

        // Initial timeline moves the reel back, then forward
        this._startSpinTimeline = new TimelineMax();
        this._startSpinTimeline.to(this, 0.15, { currSpeed:-this._maxSpeed/30, ease:Cubic.easeInOut });
        this._startSpinTimeline.to(this, 0.60, { currSpeed: this._maxSpeed,    ease:Cubic.easeIn    });

        // Even if "min start time" is set to 0, we allow the backwards / forwards
        // speed tween to complete, before indicating that the infinite spin has
        // fully commenced.
        let allowReelToStopAbsTime = Math.max(this._startSpinTimeline.duration(), this._spinnerConfig.minStartTime);

        this._startSpinTimeline.call(this._reel.fireInfiniteSpinStartedEvent, null, this._reel, allowReelToStopAbsTime);

        this._spinUpdateTween = TweenMax.to(this._spinTweener, 10000000, { onUpdate: this.spinUpdate, onUpdateScope: this });
        this._lastSpinUpdateTime = this._spinUpdateTween.time();

        // I despearately want to understand what the intention of this method was. The method
        // name looks like it is not accurate relative to what it actually does (and i dont
        // know what "disable unmasking" really means either)
        this._reel.unmaskSymbols();

        this._tallSymbolsData = null;
        this._Kp = this._spinnerConfig.Kp;
        this._Ki = this._spinnerConfig.Ki;
    };


    /**
     * Kills any spin animation dead, and resets the position and visual state of the symbols
     * back to an "idle" state. The values shown on the symbols are not touched. Used when we
     * need to abort a spin, due to error fetching spin results.
     * @public
     */
    abortSpin()
    {
        this.log.debug(`PIDSpinner[${this._reel.reelIndex}].abortSpin()`);

        this._spinState = PidSpinState.IDLE;

        if (this._startSpinTimeline) {
            this._startSpinTimeline.kill();
            this._startSpinTimeline = null;
        }

        if (this._spinUpdateTween) {
            this._spinUpdateTween.kill();
            this._spinUpdateTween = null;
        }

        // Set symbol states back to something neutral, and position them
        // sensibly again.
        this._reel.symbols.forEach((symbolGraphic, symbolIndex) => {
            symbolGraphic.showIdleState();

            // TODO: This needs to remove "tall symbols" as well, as there is a small bug here
            
            // TODO: Are the symbols in the correct places with respect
            // to indexed order ?
            symbolGraphic.y = (this._reel.symStep) * (-1 + symbolIndex)
                + (this._app.config.symHeight * 0.5)
                + this._reel.yOrigin + this._app.config.reelsMarginTop;
        });
    }


    /**
     * The PidSpinner implementation of this method, enforces a "minSpinTime" rule for
     * the reel: a certain time must have elapsed from the moment that *this* reel started
     * to spin, before we can execute the stop animation.
     * @inheritDoc
     * @public
     * @param {SymbolConfig[]} finalSymbols
     * @param {number} spinType
     * @param {boolean[]} positionIsSticky
     * @param {SpinPhaseType} spinPhaseType
     */
    spinToSymbols(finalSymbols, spinType, positionIsSticky, spinPhaseType)
    {
        if (this._spinState !== PidSpinState.STOPPING) {

            // If we are not showing the infinite spin phase, then "spinToSymbols"
            // was probably the first spin-like call to the Reel. Make sure to set
            // all symbols into spinning state, as this will not have been done yet.
            if (this._spinState !== PidSpinState.INFINITE_SPIN) {
                this._reel.symbols.forEach(symbolGraphic => {
                    symbolGraphic.showSpinningState();
                });
            }

            this._spinState = PidSpinState.STOPPING;
        }
        else return;

        let stopSymbolIds = [];
        
        for (let symIdx=0; symIdx < finalSymbols.length; symIdx++)
        {
            let id = finalSymbols[symIdx].id;
            stopSymbolIds.push(id);
        }

        // TODO: Usage of tall symbols could be documented a little better
        this._tallSymbolsData = this._tallSymbolsGenerator.generateTallSymbolsData(stopSymbolIds);

        this.log.debug(`tallSymbolsData for reel ${this._reel.reelIndex} = ${JSON.stringify(this.tallSymbolsData)}`);

        this._stopSymbols = stopSymbolIds.slice();

        
        this._currStopTime = 0;

        if (this._startSpinTimeline) {
            this._startSpinTimeline.kill();
            this._startSpinTimeline = null;
        }

        let numSyms = this._reel.numSymVisible;
        this._finalPosY = Math.ceil((this._currPosY + this._app.config.reelsMarginTop + this._reel.symStep * (numSyms + 2)) / this._reel.symStep) * this._reel.symStep;

        this._lastSpinUpdateTime = this._spinUpdateTween.time();
    };


    /**
     * @public
     * @inheritDoc
     * @param {boolean} isEnabled 
     */
    setQuickStopEnabled(isEnabled)
    {
        /**
         * Tracks whether QuickStop is currently enabled.
         * @private
         * @type {boolean}
         */
        this.isQuickStopEnabled = isEnabled;

        this.log.debug(`reel[${this._reel.reelIndex}].setQuickStopEnabled(isEnabled:${isEnabled})`);

        if (isEnabled)
        {
            // TODO: This was copied from Wieners original code, but I don't yet fully understand it.
            // Nor do i understand if / where this is reverted to normal values.
            this._Kp = this._spinnerConfig.fastStopKp;
            this._Ki = this._spinnerConfig.fastStopKi;
            this._spinSpeedScalingFactor = 1.25;
        }
    };


    /**
     * @public
     * @inheritDoc
     * @return {boolean}
     */
    getIsQuickStopEnabled()
    {
        return this.isQuickStopEnabled;
    };


    // TODO: This method sets a lot of "symbolGraphic.isFinal" flags. I didnt get as far
    // as documenting this flag yet (or deciding if we can actually refactor it out, and
    // replace it with something cleaner). It might be fine.
    /**
     * Update function for the spin animation
     * @private
     */
    spinUpdate()
    {
        // Calculate delta
        let newTime = this._spinUpdateTween.time();
        let deltaSec = (newTime - this._lastSpinUpdateTime) * this._spinSpeedScalingFactor;
        this._lastSpinUpdateTime = newTime;

        // Var init
        let yStep = this._currSpeed * deltaSec;
        let diff = (this._finalPosY - this._currPosY);
        let finalUpdate = false;

        //Volume control of the roll sound does not work because of this:
        if (this._rollSnd)
        {
            this._rollSnd.volume(this._currSpeed/this._maxSpeed * Number(this._app.sound.getSoundEnabled()));
        }

        if (this._spinState === PidSpinState.STOPPING)
        {
            // In the stop phase the reel slows down and sets the final symbols

            this._currStopTime += deltaSec;

            // These calculations are for the integral part of the controller. They accumulate error over time.
            // We use the integral error because we want to overshoot our target position and create
            // a mechanical feeling to the movement
            this._errors[this._errorIndex] = diff;
            this._errorDeltas[this._errorIndex] = deltaSec;

            let integrationTime = 0;
            let totalError = 0;

            const maxIntegrationTime = this._spinnerConfig.maxIntegrationTime;
            const maxStopTime = this._spinnerConfig.maxStopTime;

            for (let errIdx = this._errorIndex; errIdx >= 0; errIdx--)
            {
                let err = this._errors[errIdx];
                let delta = this._errorDeltas[errIdx];

                if (integrationTime + deltaSec > maxIntegrationTime)
                {
                    let deltaFraction = (maxIntegrationTime - integrationTime) / delta;
                    totalError += err * deltaFraction;
                    break;
                }
                else
                {
                    totalError += err * delta;
                }

                integrationTime += deltaSec;
            }
            this._errorIndex++;

            // Modify the speed using proportional and integral components
            // When we get closer to the final position the error (diff) gets smaller , so the speed gets smaller
            this._currSpeed = diff * this._Kp + totalError * this._Ki;
            this._Ki *= this._spinnerConfig.KiDamp;
            this._currSpeed = MathUtil.clamp(this._currSpeed, -this._maxSpeed, this._maxSpeed);

            // Force finish if max time is reached or if the reel movement is very small (animation is over)
            let reelSettled = Math.abs(yStep) < .01 && Math.abs(diff) < .01;
            if ((this._currStopTime >= maxStopTime) || reelSettled)
            {
                yStep = this._finalPosY - this._currPosY;
                finalUpdate = true;
            }

            if (this._stopSoundTriggered === false && this._currSpeed < this._maxSpeed*.2)
            {
                this._stopSoundTriggered = true;

                // TODO: I dont think this is being used, and the attempt to access the information
                // through reelGroup is just not right. Work out if it can be safely removed.
                if (!this._reel.reelGroup.shouldStopImmediately)
                {
                    this._reel.triggerReelStopSound();
                    
                    // TODO: This might be better placed elsewhere, for now it wil go here.
                    this._reel.fireReelSettlingEvent();
                }
            }
        }

        this._currPosY += yStep;

        const numSyms = this._reel.symbols.length;
        for (let symIdx = 0; symIdx < numSyms; symIdx++)
        {
            let symbolGraphic = this._reel.symbols[symIdx];
            symbolGraphic.setBlurLevel(finalUpdate ? 0 : this._currSpeed / this._maxSpeed);
            symbolGraphic.y += yStep;

            let wrapToBottom = symbolGraphic.y > (this._reel.reelBottom + this._reel.yOrigin) && yStep > 0;
            //let wrapToBottom = symbolGraphic.y > this._reel.bottomWrapPosY && yStep > 0;

            // This line looks wrong ("symbolGraphic.height") but its not the cause of my problems
            let wrapToTop = symbolGraphic.y < (-symbolGraphic.height + this._reel.yOrigin)  && yStep < 0;

            if (wrapToBottom || wrapToTop)
            {
                let sign = wrapToTop ? 1 : -1;
                symbolGraphic.y = symbolGraphic.y + sign * this._reel.symbolWrapDistance;
                symbolGraphic.mask = this._reel.maskGraphics;
                
                symbolGraphic.showAsNormalSymbol();

                if (this._spinState === PidSpinState.STOPPING)
                {
                    let symbolIndex = Math.floor((this._finalPosY - this._currPosY + symbolGraphic.y - this._reel.yOrigin) / this._reel.symStep);

                    if (symbolIndex >= 0 && symbolIndex < this._stopSymbols.length)
                    {
                        symbolGraphic.setId(this._stopSymbols[symbolIndex]);

                        // we must enable tall symbols as we "feed them in" from the top of the reel. For this reason,
                        // we set the bottom-most symbol to be the tall one, and we hide every symbol above it that was
                        // involved in the tall symbol.
                        if (this.tallSymbolsData)
                        {
                            // We will check if this new symbol being fed in, is
                            // a) The bottom symbol of a tall symbol (in which case, we set it to tall state)
                            // b) Involved in a tall symbol (but not the bottom symbol), in which case we must
                            //    hide it.
                            this.tallSymbolsData.forEach(tallSymbolData => {
                                if (symbolIndex === tallSymbolData.finalSymIndex) {
                                    symbolGraphic.showAsTallSymbol(tallSymbolData.numSymbols, true);
                                }
                                else
                                if (symbolIndex >= tallSymbolData.startSymIndex &&
                                    symbolIndex <  tallSymbolData.finalSymIndex) {
                                    symbolGraphic.showAsInvolvedInTallSymbol();
                                }
                            });
                        }

                        // Russell's note (17.04.2020): cannot fathom what this means, and its not explained.
                        // It sounds like a marker saying "this is the final symbol on the reel"? Except that
                        // its being set for every single symbol on the reel, as far as i can tell... so it
                        // cannot possibly be working however it was originally intended to work.
                        symbolGraphic.isFinal = true;
                    }
                    else if (symbolIndex === -1 || symbolIndex === this._reel.numSymVisible)
                    {
                        symbolGraphic.setId(this._app.utils.getRandomSymId());
                    }
                    else
                    {
                        symbolGraphic.setId(this._app.utils.getRandomSymId());
                    }
                }
                else if (!symbolGraphic.isFinal)
                {
                    symbolGraphic.setId(this._app.utils.getRandomSymId());
                }

                // Again, this isn't right.
                this._reel.reelGroup.sortSymbols();
            }

            if (symbolGraphic.isFinal && yStep < 0 && !this._disableUnmasking)
            {
                symbolGraphic.mask = null;
            }

            if (symbolGraphic.y > this._unmaskThreshold)
            {
                if (symbolGraphic.isFinal === true && !this._disableUnmasking)
                {
                    symbolGraphic.mask = null;
                }
                else if (yStep > 0)
                {
                    symbolGraphic.mask = this._reel.maskGraphics;
                }
            }
        }

        if (finalUpdate === true)
        {
            if (this._rollSnd)
            {
                this._rollSnd.volume(0);
                this._rollSnd.stop();
            }


            let minY = Number.POSITIVE_INFINITY;
            let maxY = Number.NEGATIVE_INFINITY;
            let bottomSymId, topSymId;
            
            this._reel.sortSymbolsByY();

            for (let symIdx = 0; symIdx < numSyms; symIdx++)
            {
                let symbolGraphic = this._reel.symbols[symIdx];

                if (symbolGraphic.y < minY)
                {
                    bottomSymId = symbolGraphic.getId();
                    minY = symbolGraphic.y;
                }

                if (symbolGraphic.y > maxY)
                {
                    topSymId = symbolGraphic.getId();
                    maxY = symbolGraphic.y;
                }
            }

            for (let symIdx = 0; symIdx < numSyms; symIdx++)
            {
                // This is ensuring positions are correct at end of the spin
                let symbolGraphic = this._reel.symbols[symIdx];
                symbolGraphic.y = (this._reel.symStep) * (-1 + symIdx)
                    + this._app.config.symHeight*.5 + this._reel.yOrigin + this._app.config.reelsMarginTop;

                if (symIdx > 0 && symIdx < (numSyms-1))
                {
                    symbolGraphic.alpha = 1;
                    if (!this._disableUnmasking)
                    {
                        symbolGraphic.mask = null;
                    }
                }
                else if (symIdx === 0)
                {
                    symbolGraphic.setId(bottomSymId);
                    symbolGraphic.mask = this._reel.maskGraphics;
                    symbolGraphic.alpha = 1;
                }
                else
                {
                    symbolGraphic.setId(topSymId);
                    symbolGraphic.mask = this._reel.maskGraphics;
                    symbolGraphic.alpha = 1;
                }
            }

            // TODO: a spinner implementation has no business at all giving instructions to ReelGroup.
            // A spinner should not even need to know about the existing of reelgroup
            this._reel.reelGroup.sortSymbols();
            
            this._reel.fireReelStoppedEvent();

            this._spinUpdateTween.kill();
            
            this._spinState = PidSpinState.IDLE;
        }
    };


    /**
     * @public
     * @inheritDoc
     * @param {number} value
     */
    set speedScaleFactor(value) {
        this._speedScalingFactor = value;
    };


    /**
     * @public
     * @inheritDoc
     * @return {number}
     */
    get speedScaleFactor() {
        return this._speedScalingFactor;
    };
}