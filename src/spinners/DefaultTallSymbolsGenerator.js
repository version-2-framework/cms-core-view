import { getGameViewLogger } from "../../../cms-core-logic/src/logging/LogManager";

const log = getGameViewLogger();

/**
 * Default implementation of Tall Symbols Generator. Finds all multiply tall groups
 * of symbols (more than 1 can exist on the reel), and filters them according to the
 * actual tall symbols configuration that exists for the game.
 * @implements {TallSymbolsGenerator}
 */
export class DefaultTallSymbolsGenerator
{
    /**
     * @param {{config:{symbolsConfig:SymbolsConfiguration}}} dependencies
     */
    constructor(dependencies)
    {
        /**
         * Master configuration for the symbols.
         * @private
         * @type {SymbolsConfiguration}
         */
        this._symbolsConfig = dependencies.config.symbolsConfig;

        /**
         * The maximum number of symbols in any tall symbol.
         * @private
         * @type {number}
         */
        this._tallSymMaxNumSym = this._symbolsConfig.tallSymMaxNumSym;

        /**
         * Animation data for all symbols which are enabled as tall.
         * @private
         * @type {{ [key:number]:SymbolViewConfig }}
         */
        this._tallSymAnimData = this._symbolsConfig.tallSymPerSymbol;

        if (this._tallSymAnimData)
        {
            /**
             * List of all symbol ids which are to be treated as tall.
             * @private
             * @type {number[]}
             */
            this._tallSymbolIds = Object.keys(this._tallSymAnimData).map(id => parseInt(id));
        }

        log.info(`DefaultTallSymbolsGenerator instantiated`);
    };

    /**
     * @public
     * @inheritDoc
     * @param {number[]} symbolIds 
     * @return {TallSymbolsData[]}
     */
    generateTallSymbolsData(symbolIds)
    {
        /** @type {TallSymbolsData[]} */
        let filteredTallSymbolsData = [];

        let tallSymIds = this._tallSymbolIds;
        let tallSymAnimData = this._tallSymAnimData;

        if (tallSymIds)
        {
            /** @type {TallSymbolsData[]} */
            let allPossibleTallSymbolsData = [];

            // Step 1: find all blocks of 1 or more matching symbols.
            let currTargetSymbolId = symbolIds[0];
            let startSymbolIndex = 0;
            let numConsecutiveMatchingSymbols = 0;

            symbolIds.forEach((symbolId, symbolPosIndex) =>
            {
                let isLastSymbolOnReel = symbolPosIndex === symbolIds.length - 1;

                if (symbolId === currTargetSymbolId)
                {
                    numConsecutiveMatchingSymbols += 1;
                }
                
                if (isLastSymbolOnReel || symbolId !== currTargetSymbolId)
                {
                    // We will only cache data about 2 or more matching symbols
                    if (numConsecutiveMatchingSymbols >= 2)
                    {
                        allPossibleTallSymbolsData.push({
                            symbolId      : currTargetSymbolId,
                            numSymbols    : numConsecutiveMatchingSymbols,
                            startSymIndex : startSymbolIndex,
                            finalSymIndex : startSymbolIndex + numConsecutiveMatchingSymbols - 1
                        });
                    }

                    // Cache data for this new symbol type, so we can continue tracking
                    // it for subsequent symbols in the stopSymbols
                    startSymbolIndex = symbolPosIndex;
                    numConsecutiveMatchingSymbols = 1;
                    currTargetSymbolId = symbolId;
                }
            });
            
            // Finally, filter the tallSymbolsData to include only those symbols which we
            // 1) We have the symbolId configured as tall
            // 2) That we know we have a graphic for that number of symbols configured.

            allPossibleTallSymbolsData.forEach(tallSymbolData => {
                let symbolIdIsTall = tallSymIds.indexOf(tallSymbolData.symbolId) > -1
                
                // TODO: We should consider the "tallSymMaxNumSym" property, in order to design
                // a more robust handler here.
                
                if (symbolIdIsTall) {
                    filteredTallSymbolsData.push(tallSymbolData);
                }
            });
        }
        
        return filteredTallSymbolsData;
    }
    
}