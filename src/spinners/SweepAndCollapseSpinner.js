import {getGameViewLogger} from "../../../cms-core-logic/src/logging/LogManager";
import Assets from "../utils/Assets";
import { ReelPositionUtil } from "../views/ReelPositionUtil";

const log = getGameViewLogger();

/**
 * The set of available Spin Phases for the PID Spinner.
 */
const SpinState =
{
    IDLE : 0,
    INFINITE_SPIN : 1,
    INFINITE_SPIN_COMPLETE : 2,
    DROP_IN_NEW_SYMBOLS : 3
};

/**
 * A Spinner implementation, which takes a 2 step approach to showing a spin
 * 1) During infinite spin, a customizable animated overlay is placed onto the reels. This animated
 *    overlay should fully cover the symbols. Once it has disappeared, the symbols will all be hidden.
 *    This animation will be configured to cover the entire visible area of the reel.
 * 2) When it is known what symbols must be shown, a simple collapse animation is played, dropping the
 *    new symbols in.
 * 
 * This Spinner was originally designed for the game Haunted House RIP on the V2 platform, but the
 * approach it takes is generic enough that it is a usefull library class.
 * 
 * @implements Spinner
 */
export class SweepAndCollapseSpinner
{
    /**
     * 
     * @param {ReelView} reel 
     * @param {number} reelIndex
     * @param {SweepAndCollapseSpinnerConfig} config
     * Because this spinner uses animation, a config must be provided.
     */
    constructor(reel, reelIndex, config)
    {
        let app = PIXI.app;

        /**
         * @private
         * @type {Assets}
         */
        let assets = app.assets;

        /**
         * @type {ReelPositionUtil}
         */
        let reelPositionUtil = app.reelPositionUtil;

        /**
         * @private
         * @type {ReelView}
         */
        this._reel = reel;

        /**
         * @type {number}
         * @private
         */
        this._spinState = SpinState.IDLE;

        /**
         * Set of symbols that the current spin is going to stop on.
         * @private
         * @type {SymbolConfig[]}
         */
        this._stopSymbols = null;

        /**
         * Cached reference to any timeline currently in progress to drop the symbols into
         * place.
         * @private
         * @type {gsap.TimelineMax}
         */
        this._dropTimeline = null;

        //------------------------------------------------------------------------------
        // Set up the sweep animation
        //------------------------------------------------------------------------------
        let frameIdToHideSymbols = config.frameIdToHideSymbols;
        let animationFrames = assets.generateFrames(config.animationPrefix, config.padLen, config.startFrame, config.finalFrame);

        let reelBounds = reelPositionUtil.getBoundsOfReelInReelSpace(reelIndex);

        log.debug(`SweepAndCollapseReelSpinner[${reelIndex}]: bounds = ${JSON.stringify(reelBounds)}`);
        
        // TOOD: Where can i add this animation to the stage? This is a detail which needs
        // working out and documenting, because some future spinner implementations really
        // will require this functionality (and it makes sense to allow the spinner to be
        // able to create graphics it needs as part of its animation)
        /**
         * Animation used for the sweep layer.
         * @private
         * @type {PIXI.extras.AnimatedSprite}
         */
        this._sweepAnimation = new PIXI.extras.AnimatedSprite(animationFrames);
        this._sweepAnimation.loop = false;
        this._sweepAnimation.animationSpeed = config.speed > 0? config.speed : 1;
        this._sweepAnimation.x = reelBounds.x;
        this._sweepAnimation.y = reelBounds.y;
        this._sweepAnimation.width = reelBounds.width;
        this._sweepAnimation.height = reelBounds.height;
        this._sweepAnimation.visible = false;

        this._sweepAnimation.onFrameChange = currentFrame =>
        {
            if (currentFrame === frameIdToHideSymbols)
            {
                this.hideAllSymbols();
            }
        };

        this._sweepAnimation.onComplete = () =>
        {
            this.onSweepAnimationComplete();
        }

        this._reel.overlayContainer.addChild(this._sweepAnimation);

        log.debug(`SweepAndCollapseReelSpinner.constructor()`);
    };


    /**
     * @private
     */
    hideAllSymbols()
    {
        log.debug(`SweepAndCollapseSpinner[${this._reel.reelIndex}].hideAllSymbols()`);

        this._reel.symbols.forEach(symbol => {
            symbol.visible = false;
        });
    };


    /**
     * Once the sweep animation is complete, we can report that infinite spin has completed.
     * @private
     */
    onSweepAnimationComplete()
    {
        log.debug(`SweepAndCollapseSymbolSpinner[${this._reel.reelIndex}].onSweepAnimationComplete()`);

        this._sweepAnimation.visible = false;

        this._reel.fireInfiniteSpinStartedEvent();

        this._spinState = SpinState.INFINITE_SPIN_COMPLETE;

        if (this._stopSymbols)
        {
            this.dropInNewSymbols();
        }
    };


    /**
     * Drops in the new set of symbols: brings the "spin to symbols" step to a close. Once
     * the animation of dropping in new symbols has completed, then an event is fired to
     * indicate to ReelGroup that the reel is at rest.
     * @private
     */
    dropInNewSymbols()
    {
        log.debug(`SweepAndCollapseReelSpinner[${this._reel.reelIndex}].dropInNewSymbols()`);

        this._spinState = SpinState.DROP_IN_NEW_SYMBOLS;

        // TODO: Timings of this probably want some configuration options. The animation
        // itself definitely needs to be worked on.

        let delayBetweenSymbols = 0.05;
        let dropDuration = 0.4;
        let numVisibleSym = this._reel.numSymVisible;

        let timeline = new TimelineMax();
        timeline.addLabel("start");

        // Queue up animations in order for all symbols: make each one visible again,
        // set it to show correct symbol id, and animate it dropping from above the
        // reels area, back into its resting position. A little delay is added between
        // each symbol dropping in (with the bottom-most symbol being the first one
        // to land).
        this._reel.symbols.forEach((symbolGraphic, symbolProgressiveIndex) => {
            let finalPosY = symbolGraphic.y;
            let startPosY = finalPosY - this._reel.visibleHeight;
            let symDropIndex = numVisibleSym - 1 - symbolProgressiveIndex;

            let symbolId = this._stopSymbols[symbolProgressiveIndex].id;

            symbolGraphic.visible = true;
            symbolGraphic.setId(symbolId);

            timeline.fromTo(
                symbolGraphic,
                dropDuration,
                { y:startPosY },
                { y:finalPosY, ease:customEase },
                `start+=${symDropIndex * delayBetweenSymbols}`
            );
        });

        // Once the drop is complete, return control to the ReelGroup
        timeline.call(() => {
            this._dropTimeline = null;
            this._spinState = SpinState.IDLE;
            this._reel.fireReelStoppedEvent();
            this._reel.triggerReelStopSound();
        }, null, null, "+=0.1");

        // We will cache this timeline, in case we need to abort it
        this._dropTimeline = timeline;
    };


    


    //--------------------------------------------------------------------------------------------------
    // Standard spinner API methods
    //--------------------------------------------------------------------------------------------------
    /**
     * @public
     * @inheritDoc
     */
    get numSymAboveVisibleArea() {
        return 0;
    };


    /**
     * @public
     * @inheritDoc
     */
    get numSymBelowVisibleArea() {
        return 0;
    };

    
    // TODO: This spinner really should support tall symbols.
    /**
     * @public
     * @inheritdoc
     * @return TallSymbolsData[]
     */
    get tallSymbolsData() {
        return null;
    };
    

    /**
     * @public
     * @inheritDoc
     */
    startInfiniteSpin()
    {
        if (this._spinState !== SpinState.IDLE) {
            log.warn(`SweepAndCollapseSpinner[${this._reel.reelIndex}].startInfiniteSpin: currSpinState is not IDLE`);
            return;
        }

        log.debug(`SweepAndCollapseSpinner[${this._reel.reelIndex}].startInfiniteSpin()`);

        this._state = SpinState.INFINITE_SPIN;
        this._stopSymbols = null;

        // Set all symbols to spinning state
        this._reel.symbols.forEach(symbolGraphic => {
            symbolGraphic.showSpinningState();
        });

        this._sweepAnimation.visible = true;
        this._sweepAnimation.gotoAndPlay(0);
    };


    /**
     * @public
     * @inheritDoc
     * @param {SymbolConfig[]} finalSymbols 
     * @param {number} [spinType]
     * @param {boolean[]} [positionIsSticky]
     * @param {SpinPhaseType} [spinPhaseType]
     */
    spinToSymbols(finalSymbols, spinType, positionIsSticky, spinPhaseType)
    {
        // TODO: Support IDLE needed ? Need to check what ReelGroup now does (it might be
        // that we always go through infinite spin first - i feel that we probably do, but
        // cannot quite remember)

        if (this._spinState === SpinState.DROP_IN_NEW_SYMBOLS) {
            log.debug(`SweepAndCollapseSpinner[${this._reel.reelIndex}].spinToSymbols: already dropping in final symbols`);
            return;
        }

        // Effectively start infinite spin, but with stop symbols queued
        if (this._spinState === SpinState.IDLE) {
            this._stopSymbols = finalSymbols.slice();
            this._state = SpinState.INFINITE_SPIN;
            this._sweepAnimation.gotoAndPlay(0);
        }
        else
        if (this._spinState === SpinState.INFINITE_SPIN) {
            log.debug(`SweepAndCollapseSpinner[${this._reel.reelIndex}].spinToSymbols: INFINITE_SPIN in progress, queuing spinToSymbols`);

            this._stopSymbols = finalSymbols.slice();
        }
        else
        if (this._spinState === SpinState.INFINITE_SPIN_COMPLETE) {
            log.debug(`SweepAndCollapseSpinner[${this._reel.reelIndex}].spinToSymbols: INFINITE_SPIN_COMPLETE, starting spinToSymbols`);

            this._stopSymbols = finalSymbols.slice();
            this.dropInNewSymbols();
        }
    };


    /**
     * @public
     * @inheritDoc
     * @param {boolean} isEnabled 
     */
    setQuickStopEnabled(isEnabled)
    {
        // TODO: supported on this spinner ?
    };


    /**
     * @public
     * @inheritDoc
     */
    getIsQuickStopEnabled()
    {
        return false;
    };


    /**
     * @public
     * @inheritDoc
     */
    abortSpin()
    {
        log.debug(`SweepAndCollapseSpinner[${this._reel.reelIndex}].abortSpin()`);

        if (this._dropTimeline) {
            this._dropTimeline.kill();
            this._dropTimeline = null;
        }

        this._reel.symbols.forEach(symbol => {
            symbol.visible = true;
            symbol.showIdleState();
        });

        this._reel.resetAllSymbolPositions();

        this._spinState = SpinState.IDLE;
    };


    /**
     * @public
     * @inheritDoc
     * @param {number} value
     */
    set speedScaleFactor(value)
    {
        // TODO: Supported on this spinner ?
    };


    /**
     * @public
     * @inheritDoc
     * @return {number}
     */
    get speedScaleFactor()
    {
        return 1;
    };
}


// This is the easing function for the drop from HHRip - unfortunately, i don't have any sensible
// idea what the numbers mean or what kind of range they are in
/**
 * @param {number} p 
 */
function customEase(p)
{
    if (p < 0.7)
    {
        return p * p * 2.0408;
    }
    else
    if (p < 1)
    {
        return 2.222 * p * p - 3.778 * p + 2.556;
    }
      
    return 1;
}