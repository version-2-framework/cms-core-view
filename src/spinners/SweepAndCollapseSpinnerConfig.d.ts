/**
 * Configuration object for the Sweep and Collapse Spinner.
 */
interface SweepAndCollapseSpinnerConfig extends AnimationConfig
{
    /**
     * Prefix string to generate animation frame names from.
     */
    animationPrefix : string;

    /**
     * The id of the frame, on which symbols should be hidden. The sweep animation has
     * N frames, and we need to specify exactly which frame that the symbols should be
     * hidden on: for example, the animation may not visually mask the whole of the
     * reels at all times (there may be only a single frame on which this happens).
     * This field lets us exactly specify which frame id to use.
     */
    frameIdToHideSymbols : number;
}