/**
 * Configuration for the Collapsing Symbol Spinner.
 */
interface CollapsingSymbolSpinnerConfig
{
    /**
     * The time (in seconds) that it takes to drop away all symbols on the reels, when the
     * Spinner is completely clearing the current symbols away. This sequence is shown at
     * the start of a new Spin Round.
     * @default 0.6
     */
    clearAllSymbolsDropTime : number;

    /**
     * The time (in seconds) that it takes to drop in a set of all new symbols on to the
     * reels, after the symbols have been cleared away completely. This sequence is shown at
     * the start of a new Spin Round.
     * @default 0.6
     */
    createNewSymbolsDropTime : number;

    /**
     * Delay (in seconds) between symbols being dropped down, for the respin sequence.
     * @default 0.05
     */
    respinDelayBetweenSymbolsDropping : number;

    /**
     * Time (in seconds) that it takes for a held symbol to be dropped down, for the respin
     * sequence.
     * @default 0.6
     */
    respinHeldSymbolDropTime : number;

    /**
     * Time (in seconds) that it takes for a newly spawned symbol to be dropped down, for the
     * respin sequence.
     * @default 0.15
     */
    respinSpawnedSymbolDropTime : number;
}