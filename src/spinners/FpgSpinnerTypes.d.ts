/**
 * Configuration for the "Fpg Spinner"
 */
interface FpgSpinnerConfig
{
    /**
     * The maximum speed of the reel spin, which is reached during the "infinite spin"
     * phase. This is a value in "number of symbols per second". EG: a value of 1, means
     * that at full speed, the spinner will move the reel by 1 symbol every second.
     * 
     * The FpgSpinner has a highly specific animation, designed to clone the behaviour
     * of the original Fowl Play Gold / Haunted House v1 html game clients. Therefore,
     * this is the only parameter available: acceleration / deceleration phases of the
     * spin are effectively hard coded, and their duration is calibrated according to
     * the speed value set here. A default value of around 13 to 14 (symbols per second)
     * is used: if you set a value deviating from this, acceleration and deceleration
     * will be proportionatly scaled based on the ratio between default speed and your
     * requested target speed.
     * 
     * @default 13.5
     */
    maxSpeed : number;
}