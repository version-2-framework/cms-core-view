import {getGameViewLogger} from "../../../cms-core-logic/src/logging/LogManager";

/**
 * The set of available Spin Phases for the PID Spinner.
 */
const SpinState =
{
    IDLE : 0,
    INFINITE_SPIN : 1,
    STOPPING : 2
};

/**
 * Default value of speed. The only property available for adjusting on the FpgSpinner,
 * is speed: all other timings are calibrated relative to this, to ensure a working
 * spinner.
 */
const DEFAULT_MAX_SPEED = 13.5;

/**
 * Default values for Spinner Config. These are calibrated to reproduce the spins of
 * the original HTML versions of Fowl Play Gold and Haunted House.
 * @type {FpgSpinnerConfig}
 */
export const DefaultConfig =
{
    maxSpeed : DEFAULT_MAX_SPEED,
};

/**
 * Total number of non-visible symbols (above and below the visible area of the reel,
 * combined).
 */
const NUM_HIDDEN_SYM = 1;

const log = getGameViewLogger();

/**
 * Duration (in seconds) that the deceleration ("overshoot") phase of the spin lasts for.
 * This assumes a speed equivalent to DEFAULT_MAX_SPEED
 */
const DEFAULT_DECEL_DURATION = 0.1;

/**
 * The distance that the symbols will "overshoot" their final resting position,
 * before bouncing back and stopping. This is a value expressed in "symbol heights".
 * EG: a value of 0.5, means that the reel will overshoot its final resting position
 * by half a symbol, and then bounce back.
 */
const OVERSHOOT_DISTANCE = 0.45;

/**
 * Duration (in seconds) of the "bounce back", which occurs after the decel ("overshoot")
 * phase of the spin stopping.
 */
const DEFAULT_BOUNCE_BACK_DURATION = 0.25;

// a value of 0.0 would be accurate to the original game, but doesnt work
// a value of 1.0 has the right effect, but is slow
// a value of 0.2 has the problem that some reels stop at the same time, even when
// there is a delay between calling the stop action (the spinner basically has to
// reach a preset point before its ok for it to start the stop, and this causes
// this effect)
/**
 * Total time of the acceleration phase, which occurs at the start of a new infinite
 * spin.
 */
const DEFAULT_ACCEL_DURATION = 0.1;

/**
 * Default duration (in seconds) that must elapse from the spin starting, before we consider
 * the infinite spin phase to be over.
 */
const DEFAULT_MIN_START_SPIN_DURATION = 0.5;

/**
 * A port of the Spinner routine used on the V1 Fowl Play Gold game.
 * @implements {Spinner}
 */
export class FpgSpinner
{
    /**
     * @constructor
     * @param {ReelView} reel
     * The reel to be controlled
     * @param {FpgSpinnerConfig} [spinnerConfig]
     * An optional Fpg Spinner Config to use for this reel. If not specified, the FpgSpinner
     * will look for GameConfig.spinnerConfig, which will be applied for all reels (and if that
     * is not specified, it falls back to using default config). Use this parameter, when you
     * want to specify custom spin configs for each reel.
     */
    constructor(reel, spinnerConfig=null)
    {
        let app = PIXI.app;
        let config = app.config;

        /**
         * @private
         */
        this._app = PIXI.app;

        /**
         * The reelview being controlled by this spinner.
         * @private
         * @type {ReelView}
         * @readonly
         */
        this._reel = reel;

        /**
         * @private
         * @type {FpgSpinnerConfig}
         * @readonly
         */
        this._spinnerConfig = DefaultConfig;
        if (spinnerConfig) {
            this._spinnerConfig = spinnerConfig;
        }
        else
        if (config.spinnerConfig) {
            this._spinnerConfig = config.spinnerConfig;
        }

        let totalSymHeight = config.symHeight + config.symGap;

        /**
         * The ratio of configured max speed, to default max speed: this tells us how much we
         * need to adjust other timings, to keep the spin working as intended. Multiply any
         * duration constants by this value, in order to obtain the required duration.
         * @private
         * @type {number}
         */
        this._spinnerSpeedRatio = DEFAULT_MAX_SPEED / this._spinnerConfig.maxSpeed;

        // NOTE: "number of symbols moved per second" is not how the original worked - it was
        // "num pixels per update". On FPG, its a value of 30, where the height of a symbol was
        // 128 pixels - this gives a speed figure of 0.234375 symbols per update. At 60 fps (which
        // was the target frame-rate of the original games: the original games' spinners did not
        // take time-deltas for frame updates into account at all), this gives us a target speed
        // of 14.0625 symbols per second.
        /**
         * The distance by which the reel will move when it is at full speed. This is a value
         * in pixels per second (pixels in this case, is relative to the internal scale of the
         * reel, and not screen space). This value comes from the Spinner Config object, where
         * it is expressed in "number of symbols moved per second". Here, we calculate and cache
         * the actual pixel value.
         * per second".
         * @private
         * @type {number}
         * @readonly
         */
        this._maximumSpinSpeed = totalSymHeight * this._spinnerConfig.maxSpeed;

        /**
         * The distance (in pixels) by which the reel will overshoot its target final position,
         * before bouncing back and stopping at the final position. This value comes from spinner
         * config, where it is expressed in "numbers of symbols". Here, we calculate and cache
         * the actual pixel value.
         * @private
         * @type {number}
         * @readonly
         */
        this._overshootDistance = Math.max(0, totalSymHeight * OVERSHOOT_DISTANCE);

        /**
         * The target position of the top visible symbol of the reels. This is used as part of the
         * calculations that bring the reel spin to a halt (we need it, to accurately determine the
         * final resting position, and how far to move).
         * @private
         * @type {number}
         * @readonly
         */
        this._targetTopSymbolPosY = config.reelsMarginTop + (totalSymHeight * 0.5);
        
        /**
         * Caches the time (in seconds) of the last update of spin update tween.
         * @private
         * @type {number}
         */
        this._lastUpdateTime = 0;

        /**
         * The currently active Spin State. This field can perhaps be renamed, because "phase" implies
         * that it handles details such as "accelerate", "infinite spin", "slow down", "bounce back",
         * "stopped" - in fact, spinState is not nearly as fine grained as that. Instead, SpinState is
         * more like a state - its used solely to track which input actions to the reel are currently
         * valid. It can be IDLE, INFINITE_SPIN, or STOPPING (on symbols). When IDLE, its valid either
         * to start an INFINITE_SPIN, or to start STOPPING (to a set of target symbols). In INFINITE_SPIN,
         * its valid to change to STOPPING, and when STOPPING its not valid to do anything until the
         * stop sequence has completed.
         * @private
         * @type {number}
         */
        this._spinState = SpinState.IDLE;

        /**
         * A tween, which accounts for the "infinite spin" of the reel.
         * @private
         * @type {gsap.TweenMax}
         */
        this._spinUpdateTween = null;

        /**
         * The set of symbols that the current spin must stop on.
         * @private
         * @type {SymbolConfig[]} finalSymbols
         */
        this._stopSymbols = null;

        /**
         * Indicates if it is now allowed to stop on any final symbols (because minimum time
         * or acceleration conditions have been met).
         * @private
         * @type {boolean}
         */
        this._stopIsAllowed = false;

        // TODO: Wants properly implementing in the future, when we are needing the functionality
        /**
         * @private
         * @type {number}
         */
        this._speedScalingFactor = 1;
    };


    /**
     * @public
     * @inheritdoc
     * @return {number}
     */
    get numSymAboveVisibleArea() {
        return 1;
    };
    

    /**
     * @public
     * @inheritDoc
     * @return {number}
     */
    get numSymBelowVisibleArea() {
        return 0;
    };
    

    /**
     * @public
     * @inheritDoc
     * @return {TallSymbolsData[]}
     */
    get tallSymbolsData() {
        return null; //this._tallSymbolsData;
    };


    /**
     * @public
     * @inheritDoc
     */
    startInfiniteSpin() {
        if (this._spinState !== SpinState.IDLE) {
            log.warn(`FpgSpinner[${this._reel.reelIndex}].startInfiniteSpin: currSpinState is not IDLE`);
            return;
        }

        log.info(`FpgSpinner[${this._reel.reelIndex}].startInfiniteSpin()`);

        this._spinState = SpinState.INFINITE_SPIN;

        this.startSpinFromStationary();
    };


    /**
     * Starts the reel spinning, from resting. Used internally, when starting an infinite spin,
     * or when idle (but immediately skipping to "STOPPING" state)
     * @private
     */
    startSpinFromStationary()
    {
        this._stopIsAllowed = false;
        
        /**
         * Provides data about the spin acceleration phase: will be null when acceleration
         * is not taking place.
         * @private
         */
        this._accelSpinPhase =
        {
            /**
             * The time (in seconds) at which the spin acceleration phase started.
             */
            startTime : 0
        };

        this._reel.symbols.forEach(symbolGraphic => {
            symbolGraphic.showSpinningState();
        });

        // Create the initial infinite spin tween
        this._spinUpdateTween = new TweenMax({}, 10000000, {
            repeat : -1,
            onUpdate : this.updateSpin,
            onUpdateScope : this
        });
    };


    // TODO: This should support spinToSymbols when infinite spin is not in progress, and currently, it does not!!
    /**
     * @public
     * @inheritDoc
     * @param {SymbolConfig[]} finalSymbols 
     * @param {number} [spinType]
     * @param {boolean[]} [positionIsSticky]
     * @param {SpinPhaseType} [spinPhaseType]
     */
    spinToSymbols(finalSymbols, spinType, positionIsSticky, spinPhaseType) {
        if (this._spinState === SpinState.IDLE) {
            log.debug(`FpgSpinner[${this._reel.reelIndex}].spinToSymbols: reel is IDLE, start spin then bring to a halt`);
            this._spinState = SpinState.STOPPING;
            this._stopSymbols = finalSymbols.slice();
            this.startSpinFromStationary();
        }
        else
        if (this._spinState === SpinState.INFINITE_SPIN) {
            log.debug(`FpgSpinner[${this._reel.reelIndex}].spinToSymbols: INFINITE_SPIN in progress, bring it to a halt`);
            this._spinState = SpinState.STOPPING;
            this._stopSymbols = finalSymbols.slice();
        }
        else
        {
            log.warn(`FpgSpinner[${this._reel.reelIndex}].spinToSymbols: currSpinState is not IDLE or INFINITE_SPIN`);
        }
    };


    /**
     * @public
     * @inheritDoc
     * @param {boolean} isEnabled 
     */
    setQuickStopEnabled(isEnabled) {
        log.warn("FpgSpinner.setQuickStopEnabled: Method not implemented.");
    };


    /**
     * @public
     * @inheritDoc
     * @return {boolean}
     */
    getIsQuickStopEnabled() {
        return false; // technically correct, but need's implementing....
    };


    /**
     * @public
     * @inheritDoc
     */
    abortSpin() {
        this._spinUpdateTween.kill();
        this._spinUpdateTween = null;
        this._spinState = SpinState.IDLE;
        this._reel.resetAllSymbolPositions();
        this._reel.symbols.forEach(symbolGraphic => symbolGraphic.showIdleState());
    };


    //--------------------------------------------------------------------------------------------------
    // Key internal functionality
    //--------------------------------------------------------------------------------------------------
    /**
     * This is the main method for making the reel spin around!
     * @private
     */
    updateSpin()
    {
        // Ending the spin is performed in 2 phases
        // - "decel phase": start to deccelerate the reel, aiming for speed to reach 0 at a position
        //   which has overshot the real final resting place by a certain amount
        // - "bounce back phase": now that speed is 0, and we have overshot, bounce back to the final
        //   resting position.

        let lastUpdateTime = this._lastUpdateTime;
        let currUpdateTime = this._spinUpdateTween.totalTime();
        let timeDelta = currUpdateTime - lastUpdateTime;
        this._lastUpdateTime = currUpdateTime;

        // This logic is wrong. We have a target overshoot - we cannot really have both
        // a configurable overshoot, and a configurable time in which it happens (we must
        // pick one or the other) - that's because we could set the time short (requiring
        // acceleration to hit the required overshoot!!). Considering that the original
        // had an overshoot amount, i think we need to go with that. Perhaps the overshoot
        // amount is fixed ? And we allow for configuring the time in which its done.
        if (this._decelSpinPhase)
        {
            let decelTotalTime = DEFAULT_DECEL_DURATION * this._spinnerSpeedRatio;
            let decelStartTime = this._decelSpinPhase.startTime;
            let decelTimeElapsed = this._spinUpdateTween.totalTime() - decelStartTime;
            let decelLinearRatio = Math.min(1, decelTimeElapsed/decelTotalTime);
            let decelEasingRatio = Sine.easeOut.getRatio(decelLinearRatio);
            let currTotalAmountMoved = this._overshootDistance * decelEasingRatio;
            let lastTotalAmountMoved = this._decelSpinPhase.totalAmountMoved;
            let amountToMove = currTotalAmountMoved - lastTotalAmountMoved;

            this._decelSpinPhase.totalAmountMoved = currTotalAmountMoved;

            /*
            if (this._reel.reelIndex === 0) {
                log.debug(`FpgSpinner[${this._reel.reelIndex}]:decel(totalTime:${decelTotalTime},startTime:${decelStartTime},elapsed:${decelTimeElapsed})`);
                log.debug(`FpgSpinner[${this._reel.reelIndex}]:linearRatio:${decelLinearRatio},easingRatio:${decelEasingRatio}`);
                log.debug(`FpgSpinner[${this._reel.reelIndex}]:lastTotalAmountMoved:${lastTotalAmountMoved},currTotalAmountMoved:${currTotalAmountMoved},amountToMove:${amountToMove}`);
            }
            */

            this._reel.symbols.forEach(symbolGraphic => symbolGraphic.setBlurLevel(1 - decelEasingRatio));

            this.moveVertically(amountToMove);
            
            // Deceleration phase of stopping has ended, when we have reached the full time required
            // for the decceleration phase
            if (decelTimeElapsed >= decelTotalTime)
            {
                // Now, we must bounce the reels back to final resting position. If we assume that overshoot
                // is always locked to half a symbol (eg: no new symbol"wrapping" from top to bottom happened),
                // then we can simply compare the position of the symbol at index 0, with its target resting
                // position, to know how far we must bounce back.
                
                let topVisibleSymCurrPosY = this._finalTopVisibleSymbol.y;
                let topVisibleSymTargetPosY = this._reel.symbolIdleYPositions[1];
                let overshootDistance = topVisibleSymCurrPosY - topVisibleSymTargetPosY;
                
                /*
                if (this._reel.reelIndex === 0) {
                    log.debug(`FpgSpinner[${this._reel.reelIndex}]: topSymCurrPosY:${topVisibleSymCurrPosY},topSymTargetPosY:${topVisibleSymTargetPosY}`);
                    log.debug(`FpgSpinner[${this._reel.reelIndex}]: overshootDistance:${overshootDistance}`);
                }
                */

               this._reel.symbols.forEach(symbolGraphic => symbolGraphic.setBlurLevel(0));

                /**
                 * Caches data about the Bounce Back phase of the spin.
                 * @private
                 */
                this._bounceBackSpinPhase =
                {
                    /**
                     * The actual vertical distance, by which we have overshot our required resting position.
                     * @type {number}
                     */
                    overshootDistance,

                    /**
                     * Tracks the total amount moved so far during the bounce back phase.
                     * @type {number}
                     */
                    totalAmountMoved : 0,

                    /**
                     * Caches the time at which "back phase" started
                     * @type {number}
                     */
                    startTime : this._spinUpdateTween.totalTime()
                }

                this._decelSpinPhase = null;
                this._finalTopVisibleSymbol = null;

                this._reel.triggerReelStopSound();
            }
        }
        else
        if (this._bounceBackSpinPhase)
        {
            let bounceTotalTime = DEFAULT_BOUNCE_BACK_DURATION * this._spinnerSpeedRatio;
            let bounceStartTime = this._bounceBackSpinPhase.startTime;
            let bounceTimeElapsed = this._spinUpdateTween.totalTime() - bounceStartTime;
            let bounceLinearRatio = Math.min(1, bounceTimeElapsed/bounceTotalTime);
            let bounceEasingRatio = Sine.easeInOut.getRatio(bounceLinearRatio);
            let currTotalAmountMoved = -this._bounceBackSpinPhase.overshootDistance * bounceEasingRatio;
            let lastTotalAmountMoved = this._bounceBackSpinPhase.totalAmountMoved;
            let amountToMove = currTotalAmountMoved - lastTotalAmountMoved;

            this._bounceBackSpinPhase.totalAmountMoved = currTotalAmountMoved;

            /*
            if (this._reel.reelIndex === 0) {
                log.debug(`FpgSpinner[${this._reel.reelIndex}]:bounce(totalTime:${bounceTotalTime},startTime:${bounceStartTime},elapsed:${bounceTimeElapsed})`);
                log.debug(`FpgSpinner[${this._reel.reelIndex}]:linearRatio:${bounceLinearRatio},bounceEasingRatio:${bounceEasingRatio}`);
                log.debug(`FpgSpinner[${this._reel.reelIndex}]:overshootDistance:${this._bounceBackSpinPhase.overshootDistance},currTotalAmountMoved:${currTotalAmountMoved},lastTotalAmountMoved:${lastTotalAmountMoved}`);
                log.debug(`FpgSpinner[${this._reel.reelIndex}]:amountToMove:${amountToMove}`);
            }
            */

            this.moveVertically(amountToMove);
            
            if (bounceTimeElapsed >= bounceTotalTime)
            {
                this._bounceBackSpinPhase = null;

                this.onSpinComplete();
            }
        }
        // Initial phase of the spin, is some acceleration, up to full speed.
        else
        if (this._accelSpinPhase)
        {
            let accelTotalTime = DEFAULT_ACCEL_DURATION * this._spinnerSpeedRatio;
            let accelStartTime = this._accelSpinPhase.startTime;
            let accelTimeElapsed = this._spinUpdateTween.totalTime() - accelStartTime;
            let accelLinearRatio = accelTotalTime > 0? Math.min(1, accelTimeElapsed/accelTotalTime) : 1;
            let accelEasingRatio = Sine.easeIn.getRatio(accelLinearRatio);
            let currSpinSpeed = this._maximumSpinSpeed * accelEasingRatio;
            let amountToMove = timeDelta * currSpinSpeed;

            this.moveVertically(amountToMove);

            this._reel.symbols.forEach(symbolGraphic => symbolGraphic.setBlurLevel(accelEasingRatio));

            if (accelTimeElapsed >= accelTotalTime)
            {
                this._reel.symbols.forEach(symbolGraphic => symbolGraphic.setBlurLevel(1));

                this._accelSpinPhase = null;
                this._lastUpdateTime = this._spinUpdateTween.totalTime();
            }
        }
        // If not ending the spin, then just spin the reels, as normal
        // TODO: There doesn't seem to be any acceleration on the original. Should there be ??
        // It could probably be inserted as a separate phase, with a dedicated data object (as
        // for the stop phases)
        else
        {
            let amountToMove = timeDelta * this._maximumSpinSpeed;
            this.moveVertically(amountToMove);

            let totalStartSpinTime = this._spinUpdateTween.totalTime();
            if (totalStartSpinTime >= DEFAULT_MIN_START_SPIN_DURATION * this._spinnerSpeedRatio)
            {
                this._stopIsAllowed = true;
                this._reel.fireInfiniteSpinStartedEvent();
            }
        }
    };


    /**
     * Action invoked when the spin has finished.
     * @private
     */
    onSpinComplete()
    {
        log.info(`FpgSpinner[${this._reel.reelIndex}].onSpinComplete()`);

        // Reset some key flags..
        this._lastUpdateTime = 0;
        this._spinState = SpinState.IDLE;

        this._spinUpdateTween.kill();
        this._spinUpdateTween = null;

        this._reel.symbols.forEach(symbolGraphic => {
            symbolGraphic.showIdleState();
        });

        // With this spinner, the simple technique used means the top and bottom
        // "padding" symbols often end up out of alignment. For now, this resolves
        // the issue fairly well, but in a future update, it may be necessary to
        // tweak the deceleration / bounce back mechanics, so that this condition
        // doesn't occur (as if there was a possibility of the visible area of the
        // reel allowing for the hidden symbols to be seen, we might see them appear
        // or disappear when the snap symbols method is invoked)
        this._reel.snapAllSymbolsToClosestYPositions();
        this._reel.fireReelStoppedEvent();
    };


    /**
     * @private
     * @param {number} amountToMove 
     */
    moveVertically(amountToMove)
    {
        let symbols = this._reel.symbols;
        let numSym = symbols.length;
        let rearrangeSymbols = false;

        for (let symbolIndex = 0; symbolIndex < numSym; symbolIndex ++)
        {
            let symbolGraphic = symbols[symbolIndex];

            symbolGraphic.y += amountToMove;

            if (amountToMove > 0 && symbolGraphic.y > this._reel.bottomWrapPosY)
            {
                symbolGraphic.y -= this._reel.visibleHeight + (NUM_HIDDEN_SYM * this._reel.symStep);

                // TODO: This is not a good approach to generating random symbol ids, but I am
                // not quite yet ready to replace it with the new "SymbolFeeder" mechanics, which
                // need some further development.
                symbolGraphic.setId(this._app.utils.getRandomSymId());
                
                // TODO: Here, we must update the symobl value.
                // An algorithm that lets us get this value would be great.
                // Initially, its probably ok to just pick a random symbol.

                if (this._stopIsAllowed && this._stopSymbols)
                {
                    if (this._stopSymbols.length > 0)
                    {
                        let symbol = this._stopSymbols.pop();
                        symbolGraphic.setId(symbol.id);

                        if (this._stopSymbols.length === 0)
                        {
                            /**
                             * Reference to the top-most visible symbol on the reels, when the final symbol ids
                             * are shown.
                             * @private
                             * @type {SymbolView}
                             */
                            this._finalTopVisibleSymbol = symbolGraphic;

                            /**
                             * Queues the number of "symbol loops" (a symbol reaching bottom wrap position, and
                             * getting wrapped to the top) that must now be completed before deceleration can be
                             * initiated. Because this Spinner has 1 padding symbol below the visible symbols,
                             * and one padding symbol above, we must queue 2 additional loops (once all final
                             * symbols have been pushed into the set) before starting deceleration, in order to
                             * end up with the visible symbols in the correct place when deceleration ends.
                             * @private
                             * @type {number}
                             */
                            this._numSymUntilStartDecel = NUM_HIDDEN_SYM;
                        }
                    }
                    // If stop symbols are available, but there are none left to feed in, it means
                    // that we are ready to trigger the deceleration phase (the first phase of the
                    // sequence which brings the spin to a halt)
                    else
                    {
                        /**
                        log.debug(`FpgSpinner[${this._reel.reelIndex}]: stopSymbols used up, ${this._numSymUntilStartDecel} symbols until decel starting`);
                        */

                        this._numSymUntilStartDecel -= 1;

                        if (this._numSymUntilStartDecel === 0)
                        {
                            /**
                             * @private
                             */
                            this._decelSpinPhase =
                            {
                                /**
                                 * Caches the time at which "decel phase" started
                                 * @type {number}
                                 */
                                startTime : this._spinUpdateTween.totalTime(),

                                /**
                                 * Tracks the total distance moved during the deceleration phase.
                                 * @type {number}
                                 */
                                totalAmountMoved : 0
                            }

                            this._stopSymbols = null;
                        }
                    }
                }
                else
                {
                    // TODO: Pick random symbol. For now, its actually ok if nothing is
                    // implemented here, it just means the symbols look wrong, but should
                    // not do anything really bad
                }

                rearrangeSymbols = true;
            }
            // Because this spinner has a bounce-back effect, we also want to be able
            // to loop top symbols down to the bottom, when the direction of reel travel
            // is upwards.
            else
            if (amountToMove < 0 && symbolGraphic.y < this._reel.topWrapPosY)
            {
                symbolGraphic.y += this._reel.visibleHeight + (NUM_HIDDEN_SYM * this._reel.symStep);

                rearrangeSymbols = true;
            }
        }

        if (rearrangeSymbols)
        {
            this._reel.sortSymbolsByY();
        }
    };


    /**
     * Returns a random symbol, while a spin is in progress. This is a temporary method,
     * which is going to get replaced later with some smarter functionality which will
     * allow us to tease certain concepts on specific reels, but that is second stage
     * functioanlity: it will require some thought and work in its own right, and I don't
     * have the time for that yet!
     * @private
     * @return {SymbolConfig}
     */
    getRandomNewSymbol()
    {
        // TODO:
    }


    /**
     * @public
     * @inheritDoc
     * @param {number} value
     */
    set speedScaleFactor(value) {
        this._speedScalingFactor = value;
    };


    /**
     * @public
     * @inheritDoc
     * @return {number}
     */
    get speedScaleFactor() {
        return this._speedScalingFactor;
    };
}