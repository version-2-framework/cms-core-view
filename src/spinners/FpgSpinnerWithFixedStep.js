import { getGameViewLogger } from "../../../cms-core-logic/src/logging/LogManager";

/**
 * The set of available Spin Phases for this Spinner implementation.
 */
const SpinState =
{
    IDLE : 0,
    INFINITE_SPIN : 1,
    STOPPING : 2
};

const SpinSubState = 
{
    NONE : 0,
    ACCELERATING : 1,
    SPINNING : 2,
    DECELERATING : 3,
    BOUNCE_BACK : 4
};

/**
 * @type {FpgSpinnerWithFixedStepConfig}
 */
const DefaultConfig =
{
    overshootAmount : 0
};

/**
 * The number of frame updates required during the Acceleration visual SubState.
 */
const NUM_ACCELERATION_UPDATES = 6;

/**
 * The minimum number of frame updates required during InfiniteSpin visual SubState.
 */
const MIN_NUM_INFINITE_SPIN_UPDATES = 24;

/**
 * The number of frame updates required during the Deceleration visual SubState.
 */
const NUM_DECELERATION_UPDATES = 6;

/**
 * The number of frame updates required during the BounceBack visual SubState.
 */
const NUM_BOUNCE_BACK_UPDATES = 15;

/**
 * Total number of non-visible symbols (above and below the visible area of the reel,
 * combined).
 */
const NUM_HIDDEN_SYM = 1;

/**
 * The distance that the symbols will travel during "acceleration" SubState. This is
 * a value expressed in "symbol heights". EG: a value of 0.5, means that the reel will
 * travel half a symbol height in total during acceleration.
 */
const ACCELERATION_DISTANCE = 0.5;


/**
 * The distance that the symbols will travel in a single update during the "finite spin"
 * SubState. This is a value expressed in "symbol heights". EG: a value of 0.5, means that
 * the reel will travel half a symbol height in each infinite spin update.
 */
const TRAVEL_DISTANCE_AT_FULL_SPEED = 0.25;

/**
 * The distance that the symbols will "overshoot" their final resting position,
 * before bouncing back and stopping. This is a value expressed in "symbol heights".
 * EG: a value of 0.5, means that the reel will overshoot its final resting position
 * by half a symbol, and then bounce back.
 */
const MIN_OVERSHOOT_DISTANCE = 0.125;
const MAX_OVERSHOOT_DISTANCE = MIN_OVERSHOOT_DISTANCE * 4; //3;

const log = getGameViewLogger();

/**
 * An implementation of FpgSpinner, which uses fixed movement per update (no time
 * delta calculations). This implementation has its pros and cons:
 * - pros: it is both simple and stable to implement
 * - cons: when frame rate drops low, it doesn't look great.
 * 
 * The spin always starts with an infinite spin phase, before moving to the "stopping"
 * phase.
 * Infinite spin consists of 2 sub-steps
 * - starting (an acceleration period)
 * - spinning at full speed
 * Stopping consists of 3 sub-steps
 * - bringing final symbols in
 * - overshoot final resting position by a small amount
 * - bounce back by a small amount
 * @implements {Spinner}
 */
export class FpgSpinnerWithFixedStep
{
    /**
     * 
     * @param {ReelView} reel 
     * @param {*} [spinnerConfig]
     */
    constructor(reel, spinnerConfig=null)
    {
        let app = PIXI.app;
        let config = app.config;

        /**
         * @private
         */
        this._app = app;

        /**
         * Reference to the ReelView that this Spinner is attached to.
         * @private
         * @type {ReelView}
         */
        this._reel = reel;

        /**
         * @private
         * @type {FpgSpinnerWithFixedStepConfig}
         * @readonly
         */
        this._spinnerConfig = DefaultConfig;
        if (spinnerConfig) {
            this._spinnerConfig = spinnerConfig;
        }
        else
        if (config.spinnerConfig) {
            this._spinnerConfig = config.spinnerConfig;
        }

        /**
         * The current spin state: this indicate broad-strokes of what we are doing, and it is
         * primarily used to validate external requests to this spinner to do something (eg: we
         * can be in one of a limited number of states, each of which should respond to external
         * requests differently. There are 3 SpinStates - IDLE, INFINITE_SPIN, or STOPPING.
         * @private
         * @type {number}
         */
        this._spinState = SpinState.IDLE;

        /**
         * The SubState of the spin animation. SpinSubState has overlap with SpinState, but its
         * not used to validate external requests: instead, it indicates which visual step is
         * currently being shown. There are more SpinSubStates than there are SpinStates (because
         * a SpinState may have several visual SubStates).
         * @private
         * @type {number}
         */
        this._spinSubState = SpinSubState.NONE;

        /**
         * Tracks the number of updates that have been shown for the current Spin Sub State.
         * Because this implementation of FpgSpinner does not use timeDelta calculations, we use
         * some fairly ugly, old-fashioned tracking of frame updates.
         * @private
         * @type {number}
         */
        this._numUpdatesCompletedForCurrentSubState = 0;

        /**
         * The set of symbols to stop on.
         * @private
         * @type {SymbolConfig[]}
         */
        this._stopSymbols = null;

        /**
         * Indicates if this Spinner is now allowed to stop on any final symbols (because minimum
         * time or acceleration conditions have been met).
         * @private
         * @type {boolean}
         */
        this._stopIsAllowed = false;

        /**
         * @private
         * @type {number}
         */
        this._travelDistanceTotalDuringAcceleration = this._reel.symStep * ACCELERATION_DISTANCE;

        /**
         * The distance that we move, in a single visual re-draw, when we are at full speed.
         * @private
         * @type {number}
         */
        this._travelDistanceAtFullSpeed = this._reel.symStep * TRAVEL_DISTANCE_AT_FULL_SPEED;

        let validatedOvershootAmount = Math.min(1, Math.max(0, this._spinnerConfig.overshootAmount));

        /**
         * The distance that we move (in total) during the overshoot phase.
         * @private
         * @type {number}
         */
        this._travelDistanceTotalDuringOvershoot = this._reel.symStep
            * (MIN_OVERSHOOT_DISTANCE + ((MAX_OVERSHOOT_DISTANCE - MIN_OVERSHOOT_DISTANCE) * validatedOvershootAmount));

        /**
         * Reference to the top "hidden" symbol, for when we are in bouncing back phase:
         * we need to know its position to get an accurate idea of how far we overshot.
         * We cannot just grab the 0 element from the list of symbols, because this may
         * not point to the actual symbol that we want. However, when we wrap this symbol
         * to the top (in anticipation of BounceBack) we can cache the reference to the
         * correct symbol.
         * @private
         * @type {PIXI.DisplayObject}
         */
        this._finalHiddenSymbol = null;

        /**
         * The actual distance that we have overshot our final resting position during the
         * deceleration (overshoot) SubState. Cached when we exit the deceleration state,
         * into the "bounceBack" state. We will always store this as an absolute number
         * (no sign), although the direction of travel for this distance is upwards
         * (backwards).
         * @private
         * @type {number}
         */
        this._actualOvershootDistance = 0;

        /**
         * @private
         * @type {TweenMax}
         */
        this._spinUpdateTween = null;
    };


    /**
     * @public
     * @inheritDoc
     */
    get numSymAboveVisibleArea() {
        return 1;
    };


    /**
     * @public
     * @inheritDoc
     */
    get numSymBelowVisibleArea() {
        return 0;
    };


    /**
     * @public
     * @inheritdoc
     */
    get tallSymbolsData() {
        return null;
    };


    /**
     * @public
     * @inheritDoc
     * @param {SpinPhaseType} [spinPhaseType]
     */
    startInfiniteSpin(spinPhaseType)
    {
        if (this._spinState === SpinState.IDLE) {
            this._spinState = SpinState.INFINITE_SPIN;
            this.startSpinFromStationary();
        }
        else
        {
            log.warn(`FpgSpinner[${this._reel.reelIndex}].startInfiniteSpin: not idle, cannot spin!`);
        }
    };


    /**
     * @public
     * @inheritDoc
     * @param {SymbolConfig[]} finalSymbols 
     * @param {number} [spinType]
     * @param {boolean[]} [positionIsSticky]
     * @param {SpinPhaseType} [spinPhaseType]
     */
    spinToSymbols(finalSymbols, spinType, positionIsSticky, spinPhaseType)
    {
        if (this._spinState === SpinState.STOPPING) {
            log.warn(`FpgSpinner[${this._reel.reelIndex}].spinToSymbols: already stopping!!`);
            return;
        }

        this._stopSymbols = finalSymbols.slice();

        if (this._spinState === SpinState.IDLE) {
            this._spinState = SpinState.INFINITE_SPIN;
            this.startSpinFromStationary();
        }
        else
        if (this._spinState === SpinState.INFINITE_SPIN) {
            this._spinState = SpinState.STOPPING;
        }
    };


    /**
     * @public
     * @inheritdoc
     * @param {boolean} isEnabled 
     */
    setQuickStopEnabled(isEnabled) {
        // TODO: Decide whether to implement !!
    };


    /**
     * @public
     * @inheritdoc
     */
    getIsQuickStopEnabled() {
        return false;
    };


    /**
     * @public
     * @inheritDoc
     */
    abortSpin() {
        log.debug(`FpgSpinner[${this._reel.reelIndex}].abortSpin()`);

        if (this._spinUpdateTween) {
            this._spinUpdateTween.kill();
            this._spinUpdateTween = null;
        }

        this._spinState = SpinState.IDLE;
        this._spinSubState = SpinSubState.NONE;
        this._numUpdatesCompletedForCurrentSubState = 0;
        this._finalHiddenSymbol = null;
        this._reel.resetAllSymbolPositions();
        this._reel.symbols.forEach(symbolGraphic => symbolGraphic.showIdleState());
    };


    /**
     * @public
     * @inheritdoc
     */
    get speedScaleFactor() {
        return 1;
    };


    //--------------------------------------------------------------------------------------------------
    // Internal implementation methods: these do the heavy lifting of the spin.
    //--------------------------------------------------------------------------------------------------
    /**
     * Triggers the infinite spin phase of a spin, from stationary. This can be used in 2 
     * @private
     */
    startSpinFromStationary()
    {
        log.debug(`FpgSpinner[${this._reel.reelIndex}].startSpinFromStationary()`);

        this._reel.symbols.forEach(symbolGraphic => symbolGraphic.showSpinningState());

        this._spinSubState = SpinSubState.ACCELERATING;
        this._numUpdatesCompletedForCurrentSubState = 0;

        if (this._spinUpdateTween) {
            this._spinUpdateTween.kill();
            log.error(`FpgSpinner[${this._reel.reelIndex}].startSpinFromStationary: a spin tween already existed!!`);
        }

        // Set up a tween, which runs indefinitely: we basically use this as a simple way
        // to drive our update animation.
        this._spinUpdateTween = new TweenMax({}, 10000000, {
            repeat : -1,
            onUpdate : this.updateSpin,
            onUpdateScope : this
        });
        

        // Here, we are trying to manually throttle the framerate of the tween,
        // to see if that helps us simulate the problem.
        // TODO: I would like to make both "step through" and "frame-rate-throttling"
        // as permanently enabled debug modes, for the new "debug features" that are
        // planned for the framework.
        /*
        let numUpdatesPerSecond = 10;
        let durationBetweenUpdates = 1 / numUpdatesPerSecond;
        let lastUpdateTime = 0;
        let spinUpdateTween = this._spinUpdateTween = new TweenMax({}, 10000000, {
            repeat : -1,
            onUpdate : () => {
                let timeSinceLastUpdate = this._spinUpdateTween.totalTime() - lastUpdateTime
                if (timeSinceLastUpdate >= durationBetweenUpdates) {
                    this.updateSpin();
                    lastUpdateTime = spinUpdateTween.totalTime();
                }
            }
        });
        */
    };


    /**
     * @private
     */
    updateSpin()
    {
        this._numUpdatesCompletedForCurrentSubState += 1;

        if (this._spinSubState === SpinSubState.ACCELERATING)
        {
            let totalAccelDistance = this._travelDistanceTotalDuringAcceleration;
            let numUpdatesComplete = this._numUpdatesCompletedForCurrentSubState;
            
            let thisFrameRatio = Math.min(1, numUpdatesComplete / NUM_ACCELERATION_UPDATES);
            let lastFrameRatio = numUpdatesComplete > 1 ? Math.min(1, (numUpdatesComplete - 1) / NUM_ACCELERATION_UPDATES) : 0;

            let amountToMove = totalAccelDistance *
                (Sine.easeIn.getRatio(thisFrameRatio) - Sine.easeIn.getRatio(lastFrameRatio));

            // We set the blur for all symbols during the initial
            // acceleration subState (once acceleration is complete,
            // blur will be at full level, if its being used..)
            this._reel.symbols.forEach(symbolGraphic => symbolGraphic.setBlurLevel(thisFrameRatio));
            
            this.moveSymbols(amountToMove);

            if (this._numUpdatesCompletedForCurrentSubState === NUM_ACCELERATION_UPDATES)
            {
                // Ensure at this point that blur is at full
                this._reel.symbols.forEach(symbolGraphic => symbolGraphic.setBlurLevel(1));

                this._spinSubState = SpinSubState.SPINNING;
                this._numUpdatesCompletedForCurrentSubState = 0;
            }
        }
        else
        if (this._spinSubState === SpinSubState.SPINNING)
        {
            this.moveSymbols(this._travelDistanceAtFullSpeed);

            if (this._numUpdatesCompletedForCurrentSubState === MIN_NUM_INFINITE_SPIN_UPDATES)
            {
                this._stopIsAllowed = true;
                this._reel.fireInfiniteSpinStartedEvent();
            }
        }
        else
        if (this._spinSubState === SpinSubState.DECELERATING)
        {
            let totalDecelDistance = this._travelDistanceTotalDuringOvershoot;
            let numUpdatesComplete = this._numUpdatesCompletedForCurrentSubState
            
            let thisFrameRatio = Math.min(1, numUpdatesComplete / NUM_DECELERATION_UPDATES);
            let lastFrameRatio = numUpdatesComplete > 1? Math.min(1, (numUpdatesComplete - 1) / NUM_DECELERATION_UPDATES) : 0;

            let amountToMove = totalDecelDistance * 
                (Sine.easeOut.getRatio(thisFrameRatio) - Sine.easeOut.getRatio(lastFrameRatio));

            this.moveSymbols(amountToMove);

            let blurRatio = 1 - (numUpdatesComplete / NUM_DECELERATION_UPDATES);
            this._reel.symbols.forEach(symbolGraphic => symbolGraphic.setBlurLevel(blurRatio));

            // When deceleration SubState completes, the reel will have
            // come (more or less) to rest in its final place (or has
            // overshot a bit). Now, we must "bounceBack"
            if (this._numUpdatesCompletedForCurrentSubState === NUM_DECELERATION_UPDATES)
            {
                // Ensure all symbols set back to zero blur
                this._reel.symbols.forEach(symbolGraphic => symbolGraphic.setBlurLevel(0));

                this._spinSubState = SpinSubState.BOUNCE_BACK;
                this._numUpdatesCompletedForCurrentSubState = 0;

                // Let's also cache an accurate value for overshoot distance - we can use the
                // difference between where the top, "invisible" symbol is, and where it should
                // be in resting position.

                let hiddenSymbol = this._finalHiddenSymbol;
                let hiddenSymbolActualPosY = hiddenSymbol.y;
                let hiddenSymbolTargetPosY = this._reel.symbolIdleYPositions[0];

                this._actualOvershootDistance = Math.abs(hiddenSymbolTargetPosY - hiddenSymbolActualPosY);

                this._finalHiddenSymbol = null;
                this._reel.fireReelSettlingEvent();
                this._reel.triggerReelStopSound();
            }
        }
        else
        if (this._spinSubState === SpinSubState.BOUNCE_BACK)
        {
            let totalBackDistance = this._actualOvershootDistance;
            let numUpdatesComplete = this._numUpdatesCompletedForCurrentSubState;

            let thisFrameRatio = Math.min(1, numUpdatesComplete / NUM_BOUNCE_BACK_UPDATES);
            let lastFrameRatio = numUpdatesComplete > 1? Math.min(1, (numUpdatesComplete - 1) / NUM_BOUNCE_BACK_UPDATES) : 0;

            let amountToMove = -totalBackDistance *
                (Sine.easeInOut.getRatio(thisFrameRatio) - Sine.easeInOut.getRatio(lastFrameRatio));

            this.moveSymbols(amountToMove);

            if (this._numUpdatesCompletedForCurrentSubState === NUM_BOUNCE_BACK_UPDATES)
            {
                this._spinSubState = SpinSubState.NONE;
                this._numUpdatesCompletedForCurrentSubState = 0;

                this.onSpinComplete()
            }
        }
    };


    /**
     * @private
     * @param {number} amountToMove
     */
    moveSymbols(amountToMove)
    {
        let symbolsNeedReordering = false;

        this._reel.symbols.forEach((symbolGraphic, symbolGraphicIndex) =>
        {
            symbolGraphic.y += amountToMove;

            // Symbol has moved past max bottom position: we must now
            // move it back to the top, and assign it a new symbol id.
            if (amountToMove > 0 && symbolGraphic.y >= this._reel.bottomWrapPosY)
            {
                // Wrap symbol back up to the top
                symbolGraphic.y -= this._reel.visibleHeight + (NUM_HIDDEN_SYM * this._reel.symStep);

                symbolsNeedReordering = true;

                if (this._stopIsAllowed && this._stopSymbols)
                {
                    // We must still feed in some stop symbols
                    if (this._stopSymbols.length > 0)
                    {
                        let symbolData = this._stopSymbols.pop();

                        symbolGraphic.setId(symbolData.id);
                    }
                    // All stop symbols are fed in, now we simply need to check
                    // if its time to move to the next phase. When the final stop
                    // symbol is assigned, its actually above the visible area
                    // of the reel, so in fact we need to wait until the symbol
                    // AFTER has been wrapped to the top before trigger deceleration
                    // SubState (at this point, all the final stop symbols are
                    // somewhere within the visible area).
                    else
                    {
                        let newSymbolId = this._app.utils.getRandomSymId();

                        // TODO: This is not a good approach to generating random symbol ids, but I am
                        // not quite yet ready to replace it with the new "SymbolFeeder" mechanics, which
                        // need some further development.
                        symbolGraphic.setId(newSymbolId);

                        // No more stop symbols to bring in.
                        this._stopSymbols = null;
                        this._numUpdatesCompletedForCurrentSubState = 0;
                        this._spinSubState = SpinSubState.DECELERATING;

                        this._finalHiddenSymbol = symbolGraphic;
                    }
                }
                // Otherwise, pick a random id for the symbol
                else
                {
                    let newSymbolId = this._app.utils.getRandomSymId();

                    // TODO: This is not a good approach to generating random symbol ids, but I am
                    // not quite yet ready to replace it with the new "SymbolFeeder" mechanics, which
                    // need some further development.
                    symbolGraphic.setId(newSymbolId);
                }
                
            }
            // if we are spinning upwards, and have moved past the top wrap
            // point, then we need to snap the symbol down to the bottom.
            else
            if (amountToMove < 0 && symbolGraphic.y <= this._reel.topWrapPosY)
            {
                symbolGraphic.y += this._reel.visibleHeight + (NUM_HIDDEN_SYM + this._reel.symStep);

                symbolsNeedReordering = true;
            }
        });

        if (symbolsNeedReordering)
        {
            this._reel.sortSymbolsByY();
        }
    };


    /**
     * Invoked when the spin has fully completed.
     * @private
     */
    onSpinComplete()
    {
        log.debug(`FpgSpinner[${this._reel.reelIndex}].onSpinComplete()`);

        // We expect the tween to never be null here, but check anyway..
        if (this._spinUpdateTween) 
        {
            this._spinUpdateTween.kill();
            this._spinUpdateTween = null;
        }

        this._reel.symbols.forEach(symbolGraphic => symbolGraphic.showIdleState());

        this._spinState = SpinState.IDLE;

        this._reel.fireReelStoppedEvent();
    };
}