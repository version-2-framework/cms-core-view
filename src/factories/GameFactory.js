/**
 * Default factory providing game objects.
 * Methods can be overridden in games for customizing components.
 */

import C_GameEvent from "../../../cms-core-logic/src/const/C_GameEvent";
import BackgroundView from "../views/BackgroundView";
import GameView from "../views/GameView";
import UIDesktop from "../views/UIDesktop";
import ReelGroup from "../views/ReelGroup";
import Reel from "../views/Reel";
import BaseWinPresentationView from "../views/BaseWinPresentationView";
import BigWin from "../views/BigWin";
import PromptView from "../views/PromptView";
import SettingsView from "../views/MenuView";
import SpinButtonMobile from "../views/SpinButtonMobile";
import SpinButtonDesktop from "../views/SpinButtonDesktop";
import BetButtonMobile from "../views/BetButtonMobile";
import BetButtonDesktop from "../views/BetButtonDesktop";
import AutoplayButtonMobile from "../views/AutoplayButtonMobile";
import AutoplayButtonDesktop from "../views/AutoplayButtonDesktop";
import UIMobile from "../views/UIMobile";
import Symbol from "../views/Symbol";
import Button from "../ui/Button";
import SpinIndicator from "../views/SpinIndicator";
import UITexts from "../views/UITexts";
import MessageBar from "../views/MessageBar";
import Cashier from "../views/Cashier";
import SessionStatsView from "../views/SessionStatsView";
import AutoplayMenuView from "../views/AutoplayMenuView";
import TotalWinningsPopUp from "../views/TotalWinningsPopUp";
import BonusTotalWinningsPopUp from "../views/BonusTotalWinningsPopUp";
import DialogView from "../views/DialogView";
import BetMenuView from "../views/BetMenuView";
import WinValue from "../views/WinValue";
import SpinPhaseModel from "../../../cms-core-logic/src/model/SpinPhaseModel";
import { SlotServicesGen2 } from "../../../cms-core-logic/src/controllers/ServicesGen2";
import BonusPhaseModel from "../../../cms-core-logic/src/model/BonusPhaseModel";
import URLSelection from "../views/URLSelection";
import WinLines from "../views/WinLines";
import ScrollingSelector from "../ui/ScrollingSelector";
import { DefaultSpinPhasePresentationBuilder } from "../../../cms-core-logic/src/presentation/DefaultSpinPhasePresentationBuilder";
import { PIDSpinner } from "../spinners/PIDSpinner";
import LoadingScreen from "../views/LoadingScreen";
import { DefaultLoadingSpinner } from "../views/DefaultLoadingSpinner";
import { PlatformJsPlatformApi } from "../../../cms-core-logic/src/utils/PlatformUtil";
import { DefaultTallSymbolsGenerator } from "../spinners/DefaultTallSymbolsGenerator";
import { ConfigurableSpinSoundController } from "../spinSoundControllers/ConfigurableSpinSoundController";
import DefaultFreeSpinStartNotification from "../views/DefaultFreeSpinStartNotification";
import Assets from "../utils/Assets";
import MainView from "../views/MainView";
import DefaultGameLogo from "../views/DefaultGameLogo";
import { DefaultMessageBarController } from "../../../cms-core-logic/src/controllers/MessageBarController";
import UIDesktopMinimal from "../views/UIDesktopMinimal";

// TODO: GameFactory API is very much a work in progress.
/**
 * Default implementation of the Game Factory API.
 * @implements {GameFactoryApi}
 */
class GameFactory
{
    constructor()
    {
        this.app = PIXI.app;

        /**
         * @protected
         * @type {PlatformApi}
         */
        this._platform = new PlatformJsPlatformApi();
    };


    /**
     * Returns access to the Asset manager. This will only be available once the game is partially
     * configured (which is not a state that is fully percolated through all layers of the app based
     * logic at the moment). So, be careful using this: you may get a null reference error if you
     * invoke it too early!
     * @protected
     * @return {Assets}
     */
    get assets()
    {
        return PIXI.app.assets;
    };


    /**
     * @public
     * @return the Main View Class
     * */
    getMainView()
    {
        return new MainView();
    }

    /**
     * @public
     * @inheritDoc
     * @return {PIXI.DisplayObject}
     */
    getLoadingScreenView()
    {
        return new LoadingScreen();
    };


    /**
     * @public
     * @inheritDoc
     * @return {LoadingSpinnerView}
     */
    getLoadingSpinner()
    {
        return new DefaultLoadingSpinner();
    };


    //TODO: I don't think its any business of the factory picking the sub-implementation,
    // but if this method stays, it needs WAAAYY better documentation.
    /**
     * Creates the main UI for the device. Wil automatically select the appropriate implementation
     * (desktop, mobile, table, etc)
     * @public
     */
    getUI()
    {
        if (this._platform.isMobileOrTablet())
        {
            return this.getUiMobile();
        }
        else
        {
            return this.getUiDesktop();
        }
    }


    /**
     * @public
     * @inheritDoc
     * @param {number} centerPosX 
     * @param {number} centerPosY 
     * @param {number} targetHeight 
     * @returns 
     */
    getGameLogo(centerPosX, centerPosY, targetHeight)
    {
        return new DefaultGameLogo(centerPosX, centerPosY, targetHeight);
    }


    /**
     * @public
     * @inheritDoc
     * @param {Dependencies} dependencies
     * @return {ServicesApi}
     */
    getServices(dependencies) {
        return new SlotServicesGen2(dependencies);
    }


    /**
     * @public
     * @inheritDoc
     * @param {Dependencies} dependencies
     * @return {SpinPhaseModel}
     */
    getSpinModel(dependencies) {
        return new SpinPhaseModel(dependencies);
    };


    /**
     * @public
     * @inheritDoc
     * @param {Dependencies} dependencies 
     * @return {SpinPhasePresentationBuilder}
     */
    getSpinPresentationBuilder(dependencies) {
        return new DefaultSpinPhasePresentationBuilder(dependencies);
    };


    /**
     * @public
     * @inheritDoc
     * @param {Dependencies} dependencies
     * @return {BonusPhaseModel}
     */
    getBonusModel(dependencies) {
        // TODO: maybe this class wants renaming, or refactoring a bit
        return new BonusPhaseModel(dependencies);
    };


    /**
     * @public
     * @inheritDoc
     * @param {SoundControllerApi} soundController 
     * @return {SlotSpinSoundController}
     */
    getSpinSoundController(soundController) {
        return new ConfigurableSpinSoundController(
            this.app.config.spinSoundConfig,
            soundController
        );
    };


    /**
     * @public
     * @inheritDoc
     * @return {PIXI.DisplayObject}
     */
    getSlotGameView()
    {
        return new GameView();
    };


    /**
     * @public
     * @inheritDoc
     * @return {PIXI.DisplayObject}
     */
    getBackgroundView()
    {
        return new BackgroundView();
    };


    /**
     
     * @public
     * @inheritDoc
     * @return {SymbolView}
     */
    getSymbol() {
        return new Symbol();
    };


    /**
     
     * @public
     * @inheritDoc
     * @return {PIXI.DisplayObject}
     */
    getReelGroup() {
        return new ReelGroup();
    };


    /**
     * @public
     * @inheritDoc
     * @param {*} reelGroup
     * @param {PIXI.Container} symbolContainer 
     * @param {PIXI.Container} spinnerOverlayContainer
     * @param {number} reelIdx
     * @return {ReelView}
     */
    getReel(reelGroup, symbolContainer, spinnerOverlayContainer, reelIdx) {
        return new Reel(reelGroup, symbolContainer, spinnerOverlayContainer, reelIdx);
    };


    /**
     * @public
     * @inheritDoc
     * @param {ReelView} reel
     * @param {number} reelIndex
     * @return {Spinner}
     */
    getSpinner(reel, reelIndex) {
        return new PIDSpinner(reel);
    };


    /**
     * @public
     * @param {number} reelIndex 
     * @return {TallSymbolsGenerator}
     */
    getTallSymbolsGenerator(reelIndex) {
        return new DefaultTallSymbolsGenerator(this.app);
    };
    

    /**
     * @public
     * @param {PIXI.Container} foreground
     * @param {PIXI.Container} background
     * @inheritDoc
     * @return {Object}
     */
    getWinPresentationView(foreground, background) {
        return new BaseWinPresentationView(foreground, background);
    };
    

    /**
     * @public
     * @inheritDoc
     * @return {PIXI.DisplayObject}
     */
    getWinlinesView() {
        return new WinLines();
    };


    /**
     * @public
     * @inheritDoc
     * @param {number} winValueToShow
     * @param {number} durationInSeconds
     */
    getWinValue(winValueToShow, durationInSeconds) {
        return new WinValue(winValueToShow, durationInSeconds);
    };


    /**
     * @public
     * @return {BigWinView}
     */
    getBigWin()
    {
        return new BigWin();
    };


    // TODO: Doesnt exist in WMG games ?
    getPromptView(data, animIn, animOut)
    {
        return new PromptView(data, animIn, animOut);
    }


    /**
     * @public
     * @inheritDoc
     * @return {PIXI.DisplayObject}
     */
    getMenuView()
    {
        return new SettingsView();
    };


    /**
     * @public
     * @inheritDoc
     * @return {PIXI.DisplayObject}
     */
    getSessionStats()
    {
        return new SessionStatsView();
    };


    /**
     * @public
     * @inheritdoc
     * @return {PIXI.DisplayObject}
     */
    getSpinButtonMobile()
    {
        return new SpinButtonMobile();
    };


    /**
     * @public
     * @inheritdoc
     * @return {PIXI.DisplayObject}
     */
    getSpinButtonDesktop()
    {
        return new SpinButtonDesktop();
    };


    /**
     * @public
     * @inheritdoc
     * @return {PIXI.DisplayObject}
     */
    getBetButtonMobile()
    {
        return new BetButtonMobile();
    };


    /**
     * @public
     * @inheritdoc
     * @return {PIXI.DisplayObject}
     */
    getBetButtonDesktop()
    {
        return new BetButtonDesktop();
    };


    /**
     * @public
     * @inheritdoc
     * @return {PIXI.DisplayObject}
     */
    getAutoplayButtonMobile()
    {
        return new AutoplayButtonMobile();
    };


    /**
     * @public
     * @inheritDoc
     * @return  {PIXI.DisplayObject}
     */
    getAutoplayButtonDesktop()
    {
        return new AutoplayButtonDesktop();
    };   


    /**
     * @public
     * @inheritDoc
     * @return {PIXI.DisplayObject}
     */
    getUiMobile()
    {
        return new UIMobile();
    };


    /**
     * @public
     * @inheritdoc
     * @return {PIXI.DisplayObject}
     */
    getUiDesktop() {
        return new UIDesktopMinimal();
    };


    /**
     * Creates and returns a new FreeSpin Start Notification.
     * @public
     * @return {PIXI.DisplayObject}
     */
    getFreeSpinStartNotification() {
        return new DefaultFreeSpinStartNotification();
    };


    /**
     
     * @public
     * @inheritdoc
     * @param {number} lineId 
     * @return {PIXI.DisplayObject}
     */
    getWinlineButton(lineId)
    {
        let skin = `wl${lineId}_g`;
        let skinOver = `wl${lineId}_h`;
        let b = new Button(skin, skinOver);
        b.setAnimateScale(true);
        return b;
    };


    /**
     * @public
     * @inheritDoc
     * @return {PIXI.DisplayObject}
     */
    getSpinIndicator()
    {
        return new SpinIndicator();
    };


    /**
     * @public
     * @inheritDoc
     * @return {PIXI.DisplayObject}
     */
    getUITexts()
    {
        return new UITexts();
    };


    /**
     * @public
     * @inheritdoc
     * @return {PIXI.DisplayObject}
     */
    getMessageBar()
    {
        return new MessageBar();
    };


    /**
     * @public
     * @inheritDoc
     * @returns 
     */
    getMessageBarController()
    {
        return new DefaultMessageBarController(PIXI.app);
    }


    /**
     * @public
     * @inheritdoc
     * @return {PIXI.DisplayObject}
     */
    getCashier()
    {
        return new Cashier();
    };


    /**
     * @public
     * @param {ScrollingSelectorDimensions} dimensions
     * @param {*[]} options
     * @param {number} initialIndex
     * The option index that the Selector should initially be set to.
     * @return {PIXI.DisplayObject}
     */
    getScrollingSelector(dimensions, options, initialIndex=0)
    {
        return new ScrollingSelector(dimensions, options, initialIndex);
    };


    /**
     * @public
     * @inheritdoc
     * @return {PIXI.DisplayObject}
     */
    getAutoplayMenu()
    {
        return new AutoplayMenuView();
    };


    /**
     * @public
     * @inheritDoc
     * @return {PIXI.DisplayObject}
     */
    getBetMenu()
    {
        return new BetMenuView();
    };


    /**
     * @public
     * @inheritDoc
     * @return {TotalWinView}
     */
    getTotalWinningsPopup()
    {
        return new TotalWinningsPopUp()
    };


    /**
     * @public
     * @inheritdoc
     * @return {TotalWinView}
     */
    getBonusTotalWinningsPopup()
    {
        return new BonusTotalWinningsPopUp();
    };


    /**
     * @public
     * @inheritdoc
     * @param {DialogViewModel} dialogViewModel
     * @return {PIXI.DisplayObject}
     */
    getDialog(dialogViewModel) {
        return new DialogView(dialogViewModel);
    };


    /**
     * @public
     * @inheritDoc
     * @return {PIXI.DisplayObject}
     */
    getUrlSelectionScreen() {
        return new URLSelection();
    };



    /**
     * @public
     * @inheritDoc
     */
    getRoundedRect(cfg)
    {
        let graphics = new PIXI.Graphics();
        graphics.lineStyle(this.app.layout.relHeight(cfg.lineThickness), cfg.lineColor, cfg.lineAlpha);
        graphics.beginFill(cfg.bgColor, cfg.bgAlpha);
        graphics.drawRoundedRect(0,0, this.app.layout.relWidth(cfg.w), this.app.layout.relHeight(cfg.h), cfg.angle);
        graphics.cacheAsBitmap = true;

        let texture = this.app.renderer.generateTexture(graphics);

        return new PIXI.Sprite(texture);
    };
}

export default GameFactory;