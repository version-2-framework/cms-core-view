import BaseUI from "./BaseUI";
import Scrollbar from "./Scrollbar";
import C_UIEvent from "../const/C_UIEvent";
import C_PointerEvt from "../const/C_PointerEvt";
import MathUtil from "../utils/MathUtil";

/**
 * @typedef ScrollContainerProperties
 * @property {number} posX
 * Left most X position (in screen pixels) that this Scroll Container should start from.
 * @property {number} posY
 * Top Y position (in screen pixels) that this Scroll Container should start from.
 * @property {number} width 
 * The width (in screen pixels) that this Scroll Container should occupy.
 * @property {number} height 
 * The height (in screen pixels) that this Scroll Container should occupy.
 * @property {boolean} [fitContent]
 * If set to true, the Scroll Container will scale the width of the content passed in, to match the
 * width of the Scroll Container. By default, this method will not change the size of the content
 * passed in. Defaults to false if not set.
 * @property {boolean} [useMask]
 * Indicates if the Scroll Container should mask its content. Defaults to false if not set.
 * @property {number | string} [scrollbarBgColour]
 * Colour of the bg component of the scrollbar.
 * @property {number | string} [scrollbarBtnColour]
 * Colour of the button component of the scrollbar.
 */

// TODO: Scroll Container should also support a horizontal mode.
/**
 * Scroll container UI element
 */
class ScrollContainer extends BaseUI
{

    /**
     * Constructs a new Scroll Container, with dimensions set to a specific area.
     * @param {ScrollContainerProperties} properties
     * Configuration properties for the scroll container.
     * @param {PIXI.DisplayObject} content
     * The display object that should be added to the Scroll Container. The size of it will not be
     * changed, unless the "fitContent" parameter is set
     */
    constructor(properties, content)
    {
        super();

        /**
         * @inheritDoc
         */
        this.x = properties.posX;

        /**
         * @inheritDoc
         */
        this.y = properties.posY;

        /**
         * The target width of this Scroll Container instance.
         * @private
         * @type {number}
         */
        this.targetWidth = properties.width;

        /**
         * The target height of this Scroll Container instance.
         * @private
         * @type {number}
         */
        this.targetHeight = properties.height;

        /**
         * Indicates if the Scroll Container should be fitting content to its area, when
         * a layout refresh occurs.
         * @private
         * @type {boolean}
         */
        this.fitContentToArea = properties.fitContent? true : false;

        /**
         * If fitContentToArea is set to true, this is the target width that the content
         * must not exceed.
         * @private
         * @type {number}
         */
        this.targetContentWidth = this.targetWidth * 0.9;

        /**
         * The layer which holds whatever content must currently be shown.
         * @private
         * @type {PIXI.Container}
         */
        this.contentContainer = new PIXI.Container();
        this.addChild(this.contentContainer);

        /**
         * A background positioned in content Container, behind content. Considering that the content
         * may have a lot of alpha in it, we want to provide something by default that is touchable.
         * @private
         * @type {PIXI.Graphics}
         */
        this.contentBg = new PIXI.Graphics();
        this.contentContainer.addChild(this.contentBg);

        let useMask = properties.useMask? true : false;
        if (useMask)
        {
            /**
             * The mask, which is drawn over the content. We will hold a reference to this, but only
             * use it if masking has been explicitly enabled (which can be done dynamically during
             * the lifetime of the Scroll Container instance).
             * @inheritdoc
             */
            this.mask = new PIXI.Graphics();
            this.addChildAt(this.mask, 0);
            this.contentContainer.mask = this.mask;
        }
        
        // TODO: The meaning of this field needs documenting.
        /**
         * @private
         * @type {number}
         */
        this.scrollVector = 0;
        

        let scrollbarWidth = this.layout.relWidth(0.625);

        // TODO: The scrollbar needs to be changeable in dimensions, so that we can support
        // horizontal or vertical scrolling. Both could be useful.
        /**
         * @private
         * @type {Scrollbar}
         */
        this.scrollbar = new Scrollbar(scrollbarWidth, this.targetHeight, properties.scrollbarBgColour, properties.scrollbarBtnColour);
        
        // TODO: The meaning of this field needs documenting.
        /**
         * @private
         * @type {number}
         */
        this.inertialScrollThreshold = this.layout.relHeight(.3);
        ScrollContainer.scrollers.push({fn: this.scroll, context: this});

        this.setContent(content, this.fitContentToArea);
    };


    /**
     * Updates the dimensions of the Scroll Container. The scroll container will adopt these
     * dimensions as its size (including sizing the mask, if it is enabled, to these bounds).
     * @public
     * @param {number} x 
     * Left most X position (in screen pixels) that this Scroll Container should start from.
     * @param {number} y 
     * Top Y position (in screen pixels) that this Scroll Container should start from.
     * @param {number} width 
     * The width (in screen pixels) that this Scroll Container should occupy.
     * @param {number} height 
     * The height (in screen pixels) that this Scroll Container should occupy.
     */
    setDimensions(x, y, width, height)
    {
        this.x = x;
        this.y = y;
        this.targetWidth = width;
        this.targetHeight = height;

        if (this.content) {
            this.refreshDimensions();
        }
    };


    /**
     * Sets the content which will be wrapped by this Scroll Container. Any existing content assigned
     * to the Scroll Container will be removed (but not destroyed). It is valid to pass null for the
     * content argument, in order to clear the scroll container of any visual content.
     * @public
     * @param {PIXI.DisplayObject} [content]
     * The display object that should be added to the Scroll Container. The size of it will not be
     * changed, unless the "fitContent" parameter is set. If a null value is passed, then the Scroll
     * Container will become empty, until content is set again.
     * @param {boolean} [fitContent]
     * If set to true, the Scroll Container will scale the width of the content passed in, to match the
     * width of the Scroll Container. By default, this method will not change the size of the content
     * passed in.
     */
    setContent(content=null, fitContent=false)
    {
        if (this.content) {
            this.contentContainer.removeChild(this.content);
            this.content = null;
        }

        if (content) {
            this.content = content;
            this.content.y = 0;
            this.contentContainer.addChild(this.content);
        }

        this.contentContainer.y = 0;
        this.fitContentToArea = fitContent;

        this.refreshDimensions();
    };


    /**
     * Refreshes the dimensions of this Scroll Container instance.
     * @private
     */
    refreshDimensions()
    {
        this.refreshContentDimensions();
        this.refreshMask();
        this.refreshInteractionState();
    };


    /**
     * Refreshes dimensions of the content, and the content bg (which guarantees that we can always
     * scroll, if the content contains any transparent areas). If the fitContent flag was set when
     * adding content, then this method will size the content horizontally.
     * @private
     */
    refreshContentDimensions()
    {
        if (this.fitContentToArea && this.content) {
            if (this.content.width > this.targetContentWidth) {
                this.layout.setWidth(this.content, this.targetContentWidth);
            }
            this.content.x = this.content.width * 0.05;
        }
        else this.content.x = 0;

        this.contentBg.clear();
        this.contentBg.beginFill(0x555500, 0);
        this.contentBg.drawRect(0, 0, this.targetWidth, this.content.height);
        this.contentBg.endFill();
    };


    /**
     * Refreshes mask layout to match the target area
     * @private
     */
    refreshMask()
    {
        if (this.mask) {
            this.mask.clear();
            this.mask.beginFill(0xffffff, 1);
            this.mask.drawRect(0, 0, this.targetWidth, this.targetHeight);
            this.mask.endFill();
        }
    };


    // TODO: We can add support for horizontal scroll container also.
    /**
     * Updates interactivity state for the Scroll Container. If content exists, and is larger
     * than the dimensions of the Scroll Container
     * @private
     */
    refreshInteractionState()
    {
        let contentTallerThanArea = this.content && (this.content.height >= this.targetHeight);

        if (contentTallerThanArea)
        {
            this.scrollVector = -this.contentContainer.height + this.targetHeight;
            
            this.addChild(this.scrollbar);
            this.scrollbar.x = this.targetWidth - this.layout.relWidth(1);
                
            this.scrollbar.on(C_UIEvent.UPDATE, this.onUpdate, this);
                
            this.interactive = true;
                
            this.minY = this.scrollVector;
            this.maxY = 0;
                
            this.on(C_PointerEvt.DOWN, this.startDrag, this);
            this.on(C_PointerEvt.UP, this.stopDrag, this);
            this.on(C_PointerEvt.UPOUTSIDE, this.stopDrag, this);
        }
        else
        {
            this.removeChild(this.scrollbar);
            this.scrollbar.off(C_UIEvent.UPDATE, this.onUpdate, this);
            this.interactive = false;
            this.off(C_PointerEvt.DOWN, this.startDrag, this);
            this.off(C_PointerEvt.UP, this.stopDrag, this);
            this.off(C_PointerEvt.UPOUTSIDE, this.stopDrag, this);
        }
    };


    /**
     * @private
     * @param {*} e 
     */
    scroll(e)
    {
        if (this.interactive)
        {
            let obj = this;
            while (obj)
            {
                if (!obj.visible)
                {
                    return;
                }
                obj = obj.parent;
            }
            let newY = MathUtil.clamp(this.contentContainer.y - e.deltaY, this.minY, this.maxY);
            this.contentContainer.y = newY || 0;
            this.scrollbar.yPosition = this.contentContainer.y / this.minY;
        }
    };


    /**
     * @private
     * @param {*} e 
     */
    startDrag(e)
    {
        e.stopPropagation();
        TweenMax.killTweensOf(this.contentContainer);
        TweenMax.killTweensOf(this.scrollbar);

        this.lastDeltaY = [];

        this.on(C_PointerEvt.MOVE, this.updatePos, this);

        this.startDragPt = this.layout.getRelCoords(e.data.global);
        this.startPt = new PIXI.Point(this.contentContainer.x, this.contentContainer.y);
    };


    /**
     * @private
     * @param {*} e 
     */
    stopDrag(e)
    {
        e.stopPropagation();

        let deltaAvg = this.lastDeltaY ? MathUtil.average(this.lastDeltaY) : 0;

        if (Math.abs(deltaAvg) > this.inertialScrollThreshold)
        {
            let tweenTime = .2 + Math.min(.6, Math.abs(deltaAvg * 5 / this.layout.relHeight(100)));
            let targetY = MathUtil.clamp(this.contentContainer.y + deltaAvg * 20, this.minY, this.maxY);
            TweenMax.to(this.contentContainer, tweenTime, {y:targetY});
            TweenMax.to(this.scrollbar, tweenTime, {yPosition:targetY/this.minY});
        }

        this.removeListener(C_PointerEvt.MOVE, this.updatePos, this);
    };


    /**
     * @private
     * @param {*} e 
     */
    updatePos(e)
    {
        e.stopPropagation();
        let currDragPt = this.layout.getRelCoords(e.data.global);
        let distanceFromStartPt = this.startDragPt.y - currDragPt.y;
        let newY = this.startPt.y - distanceFromStartPt;
        newY = MathUtil.clamp(newY, this.minY, this.maxY);
        this.lastDeltaY.push(newY - this.contentContainer.y);
        if (this.lastDeltaY.length > 3)
        {
            this.lastDeltaY.shift();
        }
        this.contentContainer.y = newY;
        this.scrollbar.yPosition = this.contentContainer.y / this.minY;
    };


    /**
     * @private
     */
    onUpdate()
    {
        this.contentContainer.y = this.scrollVector * this.scrollbar.getPosition();
    };


    /**
     * @public
     * @inheritDoc
     */
    get width()
    {
        return this.targetWidth;
    };


    /**
     * @public
     * @inheritDoc
     */
    get height()
    {
        return this.targetHeight;
    };


    /**
     * @public
     * @inheritDoc
     */
    destroy()
    {
        super.destroy();

        if (this.mask) {
            this.mask.destroy();
        }
    };
}


ScrollContainer.scrollers = [];
document.addEventListener("wheel", function(e)
{
    ScrollContainer.scrollers.forEach(function(cb)
    {
        cb.fn.apply(cb.context, [e]);
    })
});


export default ScrollContainer