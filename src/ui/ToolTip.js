import { getGameViewLogger } from "../../../cms-core-logic/src/logging/LogManager";
import Layout from "../utils/Layout";

const log = getGameViewLogger();

export default class ToolTip extends PIXI.Sprite
{
    /**
     * @param {string} tooltipText
     * The text string to show on the tooltip.
     * @param {{x:number,y:number}} targetPos
     * The target position - in absolute coordinates, relative to screen space - that the
     * tooltip should point to.
     * @param {TooltipOrientation} orientation
     * The orientation of the tooltip, relative to its target position.
     * @param {TooltipStyle} tooltipStyle
     * The visual style of the tooltip.
     */
    constructor(tooltipText, targetPos, orientation, tooltipStyle)
    {
        super();

        /**
         * @private
         * @type {Layout}
         */
        this._layout = PIXI.app.layout;

        /**
         * The text to show on the tooltip.
         * @private
         * @type {string}
         */
        this._tooltipText = tooltipText;

        /**
         * Target coordinate of the tooltip.
         * @private
         * @type {{x:number,y:number}}
         */
        this._target = targetPos;

        /**
         * @private
         * @type {TooltipOrientation}
         */
        this._orientation = orientation;

        /**
         * @private
         * @type {TooltipStyle}
         */
        this._tooltipStyle = tooltipStyle;

        let bg = new PIXI.Graphics();

        bg.beginFill(tooltipStyle.bgColour);

        if (tooltipStyle.lineColour)
        {
            let lineWidthInPixels = 2; //3;
            let lineColour = tooltipStyle.lineColour;
            let lineAlpha = 1;
            let lineAlignment = 1.0; //0.5;
            let drawWithLines = true; // false
            bg.lineStyle(lineWidthInPixels, lineColour, lineAlpha, lineAlignment, drawWithLines);
        }

        // Now, we draw the actual box and arrow.
        // The box / arrow is composed of 7 points, and can be drawn in a horizontal
        // orientation (eg: the arrow part is pointing left/right) or a vertical
        // orientation (eg: the arrow part is pointing up/down). So, we will draw the
        // box in one of those 2 orientations (and flip it if required)
        
        // When the box is horizontal, our 7 points look like this:
        // X----------X
        // |          | 
        // |          X
        // |           \
        // |            X
        // |           /
        // |          X
        // |          |
        // X----------X
        
        // When the box is vertical, our 7 points look like this:
        // X--------------X
        // |              |
        // X----X   X-----X
        //       \ /
        //        X

        // The arrow need not be in the exact center (vertically / horizontally)
        // of the box - we can add an offset, so the arrow ends up nearer to the
        // edges of the box.

        // If we are right at the end of the screen, we can also set a special mode,
        // where the arrow will be rendered as
        // X X
        // |/
        // X
        // 
        // Instead of
        // X   X
        //  \ /
        //   X

        let maxTextWidth = this._layout.relWidth(30);
        let maxTextHeight = this._layout.relHeight(30);
        let textPadX = this._layout.relWidth(2);
        let textPadY = this._layout.relHeight(2);
        let fontSize = Math.round(this._layout.relHeight(3));
        let lineHeight = Math.round(this._layout.relHeight(4));

        // First, we create the text, size it as best we can: we will use this to
        // determine the basic dimensions of the box (shrinking the text only if
        // we actually cannot fit the size of it in the area available to us).
        // Before we can do that, we have to determine the orientatoin of the 

        let textStyle =
        {
            fontSize : fontSize,
            lineHeight : lineHeight,
            fontFamily: "Arial",    // TODO: Should be configurable ?
            fill : tooltipStyle.textColour,
            align : "center",
            wordWrap : true,
            wordWrapWidth : maxTextWidth
        };
        let textField = new PIXI.Text(tooltipText, textStyle);
        textField.anchor.set(0.5);
        if (textField.height > maxTextHeight) {
            this._layout.setHeight(textField, maxTextHeight);
        }

        let boxLeftBound = this._layout.relWidth(1);
        let boxRightBound = this._layout.relWidth(99);
        let boxTopBound = this._layout.relHeight(1);
        let boxBottomBound = this._layout.relHeight(99);
        let minBoxWidth = this._layout.relWidth(10);
        let minBoxHeight = this._layout.relHeight(6);
        let boxWidth = Math.max(minBoxWidth, Math.round(textField.width + textPadX));
        let boxHeight = Math.max(minBoxHeight, Math.round(textField.height + textPadY));
        let bgSpritePosX, bgSpritePosY;

        let arrowShortWidth = this._layout.relWidth(1.5);
        let arrowLongWidth = this._layout.relWidth(1.5);

        let finalScaleX = 1;
        let finalScaleY = 1;

        // Calculate offset for the box. This basically means - normally, we try and have the arrow
        // exactly half way along whichever side of the box it is on, but if screen space doesn't
        // allow this (eg: we are really near a screen edge), we can offset the box to keep it on
        // screen. In VERY rare circumstances, we can even override the requested dimension, if
        // it would force us to not allow the tooltip or its arrow within screen bounds.

        // if arrow is pointing vertically (eg: tooltip is above or below its target), then
        // we draw the box as if the arrow is pointing downwards (ie: tooltip is above its
        // target). We can later flip the graphic vertically, if the tooltip is actually meant
        // to be below its target. The arrow tip is basically the (0,0) coordinate of the tooltip
        // graphic.
        if (orientation === "vertical")
        {
            let boxOffsetX = 0;

            // If a horizontally centered arrow, means that the left of the box is to the left
            // of the allowed left bound for the box, then shift the box to the right, relative
            // to the arrow position
            if ((targetPos.x - (boxWidth * 0.5)) < boxLeftBound) {
                boxOffsetX = boxLeftBound - (targetPos.x - (boxWidth * 0.5));
            }
            // If a horizontally centered arrow, means that the right edge of the box is to the
            // right of the allowed right bound for the box, then shift the box to the left,
            // relative to the arrow position
            else
            if ((targetPos.x + (boxWidth * 0.5)) > boxRightBound) {
                boxOffsetX = boxRightBound - (targetPos.x + (boxWidth * 0.5));
            }

            let arrowWidth = arrowShortWidth;
            let arrowHeight = arrowLongWidth;
            let screenHeight = this._layout.relHeight(100);
            let spaceAboveTarget = targetPos.y;
            let spaceBelowTarget = screenHeight - spaceAboveTarget;
            let drawAboveTarget = spaceAboveTarget >= spaceBelowTarget;

            // Early versions of this code actually handled orientation in this block, and
            // vertically flipped the coordinates. However - for reasons that I still do not
            // understand - flipping the orientation into positive renderered really badly
            // (giving a bunch of artifacts when the thing was drawn on screen). So now,
            // instead, I have reverted to drawing statically the coordinates here (in the
            // orientation which works), and flipping the cached sprite later on.
            bg.moveTo(0, 0);                                                        // arrow tip
            bg.lineTo(0 + (arrowWidth/2),            0 - arrowHeight);              // arrow right point
            bg.lineTo(0 + boxOffsetX + (boxWidth/2), 0 - arrowHeight);				// box bottom right
		    bg.lineTo(0 + boxOffsetX + (boxWidth/2), 0 - arrowHeight - boxHeight);	// box top right
		    bg.lineTo(0 + boxOffsetX - (boxWidth/2), 0 - arrowHeight - boxHeight);	// box top left
		    bg.lineTo(0 + boxOffsetX - (boxWidth/2), 0 - arrowHeight);				// box bottom left
            bg.lineTo(0 - (arrowWidth/2),            0 - arrowHeight);				// arrow left point
            bg.lineTo(0, 0);                                                        // back to the beginning, close the loop
            bg.endFill();

            // Position the text field, and determine offset positions for rendered
            // bgSprite (which hasn't been created yet)
            textField.x = boxOffsetX;
            bgSpritePosX = 0 + boxOffsetX - (boxWidth * 0.5);

            if (drawAboveTarget) {
                bgSpritePosY = 0 - arrowHeight - boxHeight;
                textField.y = 0 - arrowHeight - (boxHeight * 0.5);
            }
            else
            {
                finalScaleY = -1;
                bgSpritePosY = 0 + arrowHeight + boxHeight;
                textField.y = 0 + arrowHeight + (boxHeight * 0.5);
            }
        }
        // If the arrow is pointing horizontally (eg: tooltip is to the left or right of its target),
        // then we draw the box as if the arrow is pointing to the right (ie: tooltip is on the left of
        // its target). We can later flip the graphic horizontally, if the tooltip is actually meant to
        // be on the right of its target. The arrow tip is basically the (0,0) coordinate of the tooltip
        // graphic.
        else
        if (orientation === "horizontal")
        {
            let boxOffsetY = 0;

            // If a vertically centered arrow, means that the top of the box is above the allowed top
            // bound for the box, then shift the box downwards relative to the arrow
            if ((targetPos.y - (boxHeight * 0.5)) < boxTopBound) {
                boxOffsetY = boxTopBound - (targetPos.y - (boxHeight * 0.5));
            }
            // if a vertical centered arrow, means that the bottom of the box is below the allowed
            // bottom bound for the box, then shift box upwards relative to the arrow
            else
            if ((targetPos.y + (boxHeight * 0.5)) > boxBottomBound) {
                boxOffsetY = boxBottomBound - (targetPos.y + (boxHeight * 0.5));
            }

            let arrowWidth = arrowLongWidth;
            let arrowHeight = arrowShortWidth;
            let screenWidth = this._layout.relWidth(100);
            let spaceToLeftOfTarget = targetPos.x;
            let spaceToRightOfTarget = screenWidth - spaceToLeftOfTarget;
            let drawToLeftOfTarget = spaceToLeftOfTarget >= spaceToRightOfTarget;
            
            // And - as for the vertical tooltip - we pick a single orientation that
            // seems to work best, for whatever voodoo reasons, and then flip it.
            // Still no idea why its like this - rendering mode for webgl is clearly
            // rather tempremental, at least however PIXI implements it.
            bg.moveTo(0, 0);                                                       // arrow tip
            bg.lineTo(0 + arrowWidth,            0 + (arrowHeight/2));             // arrow bottom point
            bg.lineTo(0 + arrowWidth,            0 + boxOffsetY + (boxHeight/2));  // box bottom right
            bg.lineTo(0 + arrowWidth + boxWidth, 0 + boxOffsetY + (boxHeight/2));  // box bottom left
            bg.lineTo(0 + arrowWidth + boxWidth, 0 + boxOffsetY - (boxHeight/2));  // box top left
            bg.lineTo(0 + arrowWidth,            0 + boxOffsetY - (boxHeight/2));  // box top right
            bg.lineTo(0 + arrowWidth,            0 - (arrowHeight/2));             // arrow top point
            bg.lineTo(0, 0);                                                       // back to the beginning, close the loop
            bg.endFill();

            // Position the text field, and determine offset positions for rendered
            // bgSprite (which hasn't been created yet)
            textField.y = boxOffsetY;
            bgSpritePosY = 0 + boxOffsetY - (boxHeight * 0.5);

            if (drawToLeftOfTarget) {
                finalScaleX = -1;
                textField.x = 0 - arrowWidth - (boxWidth * 0.5);
                bgSpritePosX = 0;
            }
            else // to the right
            {
                textField.x = 0 + arrowWidth + (boxWidth * 0.5);
                bgSpritePosX = 0;
            }
        }

        bg.cacheAsBitmap = true;

        let bgSprite = new PIXI.Sprite(PIXI.app.renderer.generateTexture(bg));
        bgSprite.x = bgSpritePosX;
        bgSprite.y = bgSpritePosY;
        bgSprite.scale.set(finalScaleX, finalScaleY);

        this.addChild(bgSprite);
        this.addChild(bg);
        this.addChild(textField);
        this.x = targetPos.x;
        this.y = targetPos.y;

        bg.destroy(true);

        this.animateTooltipAppearing();
    };


    // TODO: Let's improve this tween timing a bit
    /**
     * Animates the box appearing.
     * @private
     */
    animateTooltipAppearing()
    {
        // Fade in tooltip
        TweenMax.fromTo(this, 0.5, { alpha:0 }, { alpha:1 });

        // Wobble its size a bit, so we our attention is drawn to it..
        let startScaleX = this.scale.x;
        let startScaleY = this.scale.y;
        let finalScaleX = startScaleX * 1.02;
        let finalScaleY = startScaleY * 1.02;

        TweenMax.fromTo(
            this.scale,
            0.5,
            { x:startScaleX, y:startScaleY },
            { x:finalScaleX, y:finalScaleY, yoyo:true, repeat: -1, ease:"Sine.easeInOut" });
    };


    /**
     * @public
     * @inheritDoc
     */
    destroy()
    {
        TweenMax.killTweensOf(this);
        TweenMax.killTweensOf(this.scale);
        super.destroy();
    };
}