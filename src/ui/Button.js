import C_PointerEvt from "../const/C_PointerEvt";
import C_Snd from "../const/C_Snd";
import ToolTip from "./ToolTip";
import { getGameViewLogger } from "../../../cms-core-logic/src/logging/LogManager";
import C_ViewEvent from "../const/C_ViewEvent";
import C_TooltipPosition from "../const/C_TooltipPosition";
import Layout from "../utils/Layout";
import Assets from "../utils/Assets";

const log = getGameViewLogger();

/**
 * Basic button with changeable skins based on state
 */
class Button extends PIXI.Container
{
    /**
     * Constructs a new instance of the Button class.
     * @param {(String|Object[])} skin
     * "The default skin"
     * @param {(String|Object[])} skinOver
     * "rollover skin"
     * @param {(String|Object[])} skinDown
     * "down skin"
     * @param {(String|Object[])} skinDisabled
     * "disabled skin"
     */
    constructor(skin, skinOver=skin, skinDown=skinOver, skinDisabled=skin)
    {
        super();

        /**
         * @private
         */
        this._app = PIXI.app;

        /**
         * @private
         * @type {Assets}
         */
        this._assets = this._app.assets;

        /**
         * @private
         * @type {Layout}
         */
        this._layout = this._app.layout;

        /**
         * @private
         * @type {EventEmitter}
         */
        this._dispatcher = this._app.dispatcher;
        
        /**
         * @public
         * @type {boolean}
         */
        this.interactive = true;

        /**
         * Tracks the current state of this button.
         * @private
         * @type {string}
         */
        this._state = "UP";

        /**
         * Indicates if a tooltip is currently shown for this button.
         * @private
         * @type {boolean}
         */
        this._tooltipShown = false;

        /**
         * Indicates if the scale of the button should be animated.
         * @private
         * @type {boolean}
         */
        this._animateScale = false;

        /**
         * The main button graphic.
         * @private
         * @type {PIXI.Sprite}
         */
        this._butImg = this._assets.getSprite(skin);
        this.addChild(this._butImg);
        this.setSkins(skin, skinOver, skinDown, skinDisabled);

        // TODO: Considering that the button class has a state property, these
        // 2 properties seem to be completely redundant. If they are not, then
        // it means that the functionality needs cleaning up: we have a state
        // field, and it should be the single source of truth about the state of
        // the button.
        /**
         * @private
         * @type {boolean}
         */
        this._isDown = false;

        /**
         * @private
         * @type {boolean}
         */
        this._isOver = false;

        this.on('removed', this.destroy, this);
        this.on(C_PointerEvt.OVER, this.onMouseOver, this);
        this.on(C_PointerEvt.ROLLOUT, this.onMouseOut, this);
        this.on(C_PointerEvt.DOWN, this.onMouseDown, this);
        this.on(C_PointerEvt.UP, this.onMouseUp, this);

        /**
         * The id of the sound to play, when this button is pressed.
         * @private
         * @type {string}
         */
        this._buttonPressedSoundId = C_Snd.BTN_DOWN;

        /**
         * The volume at which the button pressed sound should be played.
         * @private
         * @type {number}
         */
        this._buttonPressedVolume = 1;

        // TODO: Document what this is used for
        /**
         * @private
         * @type {PIXI.Rectangle}
         */
        this._layoutRect = new PIXI.Rectangle(0,0, this._butImg.texture.width, this._butImg.texture.height);
    };


    /**
     * @public
     * @param {string} text 
     * @param {*} size 
     * @param {*} tfRect 
     */
    createLabel(text, size, tfRect)
    {
        let butTF = this._assets.getTF(text, this._layout.relHeight(size, this._layoutRect));
        butTF.anchor.set(.5,.5);
        this._layout.limitSize(butTF, tfRect.width, tfRect.height, this._layoutRect);
        this._layout.horizontalAlignCenter(butTF, tfRect.x, this._layoutRect);
        this._layout.verticalAlignMiddle(butTF, tfRect.y, this._layoutRect);
        this.addChild(butTF);

        return butTF;
    };


    /**
     * Set the click sound for the button. Set to null, if you do not what a sound to be
     * played when the button is pressed (by default, the button plays a default sound).
     * @public
     * @param {string | null} soundId
     * The id of the sound that the button should play when it is pressed. Set to null, in
     * order to disable button press sounds for this button.
     * @param {number} soundVolume
     * The volume that the sound should be played at (a value between 0 and 1 inclusive).
     */
    setClickSound(soundId, soundVolume=1)
    {
        this._buttonPressedSoundId = soundId;
        this._buttonPressedVolume = soundVolume;
    };


    /**
     * Over state handler
     * @private
     */
    onMouseOver()
    {
        this._state = "OVER";
        this.stateUpdated();
        this._isOver = true;
        this.emit(C_PointerEvt.BUT_ROLLOVER);

        if (this._tooltipProperties) {
            this.startToolTipDelay();
        }
    }


    /**
     * Out state handler
     * @private
     */
    onMouseOut()
    {
        this._state = "UP";
        this.stateUpdated();
        this._isOver = false;
        this.emit(C_PointerEvt.BUT_ROLLOUT);

        this.hideToolTip();
    };


    /**
     * Action invoked when the button is pressed.
     * @private
     */
    onMouseDown()
    {
        if (this._buttonPressedSoundId) {
            this._app.sound.playSound(this._buttonPressedSoundId, false, this._buttonPressedVolume);
        }

        this._state = "DOWN";
        this._isDown = true;
        this.stateUpdated();

        this.hideToolTip();
    };


    /**
     * Action invoked when the button is released.
     * @private
     */
    onMouseUp()
    {
        if (this._isOver)
        {
            this._state = "OVER";
        }
        else
        {
            this._state = "UP";
        }
        this.stateUpdated();
        this._isDown = false;

        this.hideToolTip();
    };


    /**
     * Set if the button is enabled/interactive
     * @public
     * @param enabled {Boolean}
     */
    setEnabled(enabled)
    {
        this.interactive = enabled;

        if (enabled)
        {
            this._state = this._isOver ? "OVER" : "UP";
        }
        else
        {
            this._isDown = false;
            this._state = "DISABLED";
            this.hideToolTip();
        }

        this.stateUpdated();
    }


    /**
     * Changes the skins
     * @public
     * @param skin {(String|Object[])} default skin
     * @param skinOver {(String|Object[])} rollover skin
     * @param skinDown {(String|Object[])} down skin
     * @param skinDisabled {(String|Object[])} disabled skin
     * @param [base=null] {DisplayObject[]} base display objects (Texts/Sprites)... configure (pos / scale / filters) with skin,
     * skinOver, skinDown and skinDisabled
     */
    setSkins(skin, skinOver=skin, skinDown=skinOver, skinDisabled=skin, base=null)
    {
        /**
         * Map of states to skin
         */
        this._skinMap =
        {
            "UP" : skin,
            "OVER" : skinOver,
            "DOWN" : skinDown,
            "DISABLED" : skinDisabled,
        };

        this.stateUpdated();
    }


    /**
     * Add a text label
     * @param label {PIXI.DisplayObject}
     */
    addLabelTF(label)
    {
        this.addChild(label);
        this.label = label;

        let btn = this._butImg;
        let rect = btn.texture;

        this._layout.limitSize(label, 80, 80, rect);
        
        label.x = (btn.width  * (0.5 - btn.anchor.x)) - (label.width  * (0.5 - label.anchor.x));
        label.y = (btn.height * (0.5 - btn.anchor.y)) - (label.height * (0.5 - label.anchor.y));
    };


    // TODO: Document this method.
    /**
     * @private
     */
    stateUpdated()
    {
        this._butImg.texture = this._assets.getTexture(this._skinMap[this._state]);

        if (this._animateScale)
        {
            let scale = 1 + Number(!this._isOver) * .25;
            TweenMax.to(this._butImg.scale, .075, {x:scale, y:scale, overwrite:1});
        }
    };


    /**
     * This method needs rethinking - it changes the anchor of the main button image. This
     * wasn't functionality documented by Wiener, and its - weird (and unexpected). I cannot
     * yet decide if anchor data should be set through constructor, or on a separate public
     * method.
     * @public
     * @param {*} val 
     */
    setAnimateScale(val)
    {
        this._animateScale = val;
        if (val)
        {
            this._butImg.anchor.set(.5);
        }
        else
        {
            this._butImg.anchor.set(0);
        }
    };


    /**
     * Configures a tooltip for this button.
     * @public
     * @param {string} text
     * The text to show on the tooltip.
     * @param {TooltipOrientation} orientation
     * The orientation of the tooltip (is it pointing up/down or left/right)
     * @param {TooltipPosition} position
     * The position of the tooltip, relative to the button it is targeting.
     * @param {TooltipStyle} [style]
     * The style properties for the tooltip.
     */
    setToolTipProperties(text, orientation, position, style=null)
    {
        style = style || this._app.config.uiStyle.tooltip;

        /**
         * Defines properties for an optional tooltip. If not set, then no tooltip should be shown for
         * this button.
         * @private
         * @type {{text:string, orientation:TooltipOrientation, position:TooltipPosition, style:TooltipStyle}}
         */
        this._tooltipProperties =  { text, orientation, position, style };
    };


    /**
     * Starts a countdown, before showing the Tooltip on the button.
     * @private
     */
    startToolTipDelay()
    {
        TweenMax.delayedCall(1.5, this.showTooltip, null, this);
    };


    /**
     * @private
     */
    showTooltip()
    {
        let properties = this._tooltipProperties;
        let button = this._butImg;
        let stage = this._app.stage;
        let relativeTargetX, relativeTargetY;
        
        if (properties.position === C_TooltipPosition.ABOVE) {
            relativeTargetX = 0.5;
            relativeTargetY = 0;
        }
        else
        if (properties.position === C_TooltipPosition.LEFT) {
            relativeTargetX = 0;
            relativeTargetY = 0.5;
        }
        else
        if (properties.position === C_TooltipPosition.BELOW) {
            relativeTargetX = 0.5;
            relativeTargetY = 1;
        }
        else
        if (properties.position === C_TooltipPosition.RIGHT) {
            relativeTargetX = 1;
            relativeTargetY = 0.5;
        }

        let tooltipTargetPosX = (-button.anchor.x + relativeTargetX) * button.width;
        let tooltipTargetPosY = (-button.anchor.y + relativeTargetY) * button.height
        let tooltipTargetLocalToButton = new PIXI.Point(tooltipTargetPosX, tooltipTargetPosY);
        let tooltipTargetLocalToStage = stage.toLocal(button.toGlobal(tooltipTargetLocalToButton));
        
        this._tooltipShown = true;

        /**
         * Reference to any currently active Tooltip graphic.
         * @private
         * @type {ToolTip}
         */
        this._tooltip = new ToolTip(properties.text, tooltipTargetLocalToStage, properties.orientation, properties.style);

        this._app.stage.addChild(this._tooltip);
    };


    /**
     * Hides any active tooltip, and kills any delayed calls to show a tooltip.
     * @private
     */
    hideToolTip()
    {
        if (this._tooltipShown)
        {
            this._tooltipShown = false;
            this._tooltip.destroy();
            this.removeChild(this._tooltip);
        }
        else
        {
            TweenMax.killDelayedCallsTo(this.showTooltip);
        }
    };
}


export default Button