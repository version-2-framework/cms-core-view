import BaseView from "../views/BaseView";
import Button from "./Button";
import C_GameEvent from "../../../cms-core-logic/src/const/C_GameEvent";
import C_PointerEvt from "../const/C_PointerEvt";

// TODO: This module is potentially very useful, but it needs documenting,
// and bringing into the fold of usable components, as currently, it is not.
// In addition, this should be a specific implementation of a KeyboardView:
// eg: its entirely logical to imagine we might have more than 1 default
// implementation available.
/**
 * A view for an on-screen keyboard.,
 */
class KeyboardView extends BaseView
{

    constructor()
    {
        super();
        this.visible = false;
    }


    /**
     * initialize keyboard
     */
    create()
    {
        this.background = new PIXI.Graphics();
        this.background.beginFill(0x000000, .9);
        this.background.drawRect(0, 0, 1920, 1080);
        this.background.endFill();
        this.background.interactive = true;
        this.addChild(this.background);

        this.keyboard = new PIXI.Container();

        // ADD TEXT FIELD'S BACKGROUND
        let textFieldBg = this.assets.getSprite('kb_textfield');
        this.layout.setHeightAndPos(textFieldBg, 18.52, 0, 0);
        this.keyboard.addChild(textFieldBg);

        // ADD TEXT FIELD
        this.textField = this.assets.getTF("123", this.layout.relHeight(13.89), null, "#000000");
        this.textField.anchor.set(0, 0.5);
        this.layout.setPos(this.textField, 2.60, 9.26);
        this.keyboard.addChild(this.textField);

        // ADD BACKSPACE
        let backspace = new Button(
            [
                {visible: true},
                {visible: false}
            ],
            [
                {visible: true},
                {visible: false}
            ],
            [
                {visible: false},
                {visible: true}
            ],
            [],
            [
                this.assets.getSprite('kb_btn_del_up'),
                this.assets.getSprite('kb_btn_del_down')
            ]
        );
        this.layout.setHeightAndPos(backspace, 9.26, 42.71, 4.63);
        backspace.function = "backspace";
        backspace.on(C_PointerEvt.DOWN, this.onButtonPress(backspace).bind(this));
        this.keyboard.addChild(backspace);

        // ADD NUMERIC BUTTONS
        for(let i = 0; i < 2; i++)
        {
            for(let j = 0; j < 5; j++)
            {
                let btnBg = this.assets.getSprite('kb_btn_num_bg');
                this.layout.setHeightAndPos(btnBg, 18.52, 0, 0);
                let btnDown = this.assets.getSprite('kb_btn_num_down');
                this.layout.setHeightAndPos(btnDown, 18.52, 0, 0);
                let btnUp = this.assets.getSprite('kb_btn_num_up');
                this.layout.setHeightAndPos(btnUp, 18.52, 0, 0);
                let tf = this.assets.getTF((i * 5 + j + 1) % 10, this.layout.relHeight(13.89) , 'GameFont1', 0xffffff);
                tf.anchor.set(0.5);
                let btn = new Button(
                    [
                        {},
                        {visible: false},
                        {visible: true},
                        {x: this.layout.relWidth(5.21), y: this.layout.relHeight(9.26), tint: 0xA8A8A8}
                    ],
                    [
                        {},
                        {visible: false},
                        {visible: true},
                        {x: this.layout.relWidth(5.21), y: this.layout.relHeight(9.26), tint: 0xA8A8A8}
                    ],
                    [
                        {},
                        {visible: true},
                        {visible: false},
                        {x: this.layout.relWidth(5.26), y: this.layout.relHeight(9.35), tint: 0xFFFFFF}
                    ],
                    [],
                    [
                        btnBg,
                        btnDown,
                        btnUp,
                        tf
                    ]
                );
                this.layout.setPos(btn, j * 10.68, 18.98 + i * 18.98);
                btn.on(C_PointerEvt.DOWN, this.onButtonPress(btn).bind(this));
                this.keyboard.addChild(btn);
            }
        }

        // ADD CANCEL, DOT AND APPLY BUTTONS
        for(let i = 0; i < 3; i++)
        {
            let base = [
                this.assets.getSprite('kb_btn_func_bg'),
                this.assets.getSprite('kb_btn_func_'+i+'_down'),
                this.assets.getSprite('kb_btn_func_'+i+'_up'),
            ];
            base.forEach((g) => {
                this.layout.setHeightAndPos(g, 18.52, 0, 0);
            });
            if (i === 1)
            {
                let tf = this.assets.getTF('.', this.layout.relHeight(13.89) , 'GameFont1', 0xffffff);
                tf.anchor.set(0.5);
                base.push(tf);
            }
            let btn = new Button(
                [
                    {},
                    {visible: false},
                    {visible: true},
                    {x: this.layout.relWidth(8.78), y: this.layout.relHeight(9.26), tint: 0xA8A8A8}
                ],
                [
                    {},
                    {visible: false},
                    {visible: true},
                    {x: this.layout.relWidth(8.78), y: this.layout.relHeight(9.26), tint: 0xA8A8A8}
                ],
                [
                    {},
                    {visible: true},
                    {visible: false},
                    {x: this.layout.relWidth(8.83), y: this.layout.relHeight(9.35), tint: 0xFFFFFF}
                ],
                [],
                base
            );
            this.layout.setPos(btn, i * 17.81, 56.94);
            btn.function = i === 0 ? "cancel" : i === 1 ? undefined : "apply";
            btn.on(C_PointerEvt.DOWN, this.onButtonPress(btn).bind(this));
            this.keyboard.addChild(btn);
        }

        this.addChild(this.keyboard);

        this.onKeyDown = function(event)
        {
            if (event.keyCode === 8)
            {
                this.onButtonPress({function: "backspace"}).bind(this)();
            }
            else if (event.keyCode === 13)
            {
                this.onButtonPress({function: "apply"}).bind(this)();
            }
            else if (event.keyCode === 27)
            {
                this.onButtonPress({function: "cancel"}).bind(this)();
            }
            else if (event.key === '.' || event.key === ',')
            {
                this.onButtonPress({children: [null, null, null, {text: '.'}]}).bind(this)();
            }
            else if (!isNaN(event.key) && event.key !== ' ')
            {
                this.onButtonPress({children: [null, null, null, {text: event.key}]}).bind(this)();
            }
        }.bind(this);
    }


    /**
     * update position / size according to resize and landscape / portrait mode
     */
    updatePositions()
    {
        this.background.scale.x = this.app.screen.width / 1920 / this.app.stage.scale.x;
        this.background.scale.y = this.app.screen.height / 1080 / this.app.stage.scale.y;

        if (this.layout.isLandscape())
        {
            this.keyboard.height = this.background.height * .8;
            this.keyboard.scale.x = this.keyboard.scale.y;
        }
        else
        {
            this.keyboard.width = this.background.width * .8;
            this.keyboard.scale.y = this.keyboard.scale.x;
        }

        this.keyboard.x = (this.background.width - this.keyboard.width) / 2;
        this.keyboard.y = (this.background.height - this.keyboard.height) / 2;
    }


    /**
     * Handles key presses
     */
    onButtonPress(btn)
    {
        return function()
        {
            let dotIndex = this.textField.text.indexOf('.');
            if (btn.function === "backspace")
            {
                if (this.textField.text.length === 1)
                {
                    this.textField.text = "0";
                }
                else
                {
                    this.textField.text = this.textField.text.substr(0, this.textField.text.length - 1);
                }
            }
            else if (btn.function === "cancel")
            {
                this.closeKeyboard(true);
            }
            else if (btn.function === "apply")
            {
                this.closeKeyboard();
            }
            else if (btn.children[3].text === ".")
            {
                if (dotIndex === -1 && this.app.model.getCurrencyDecimals() > 0)
                {
                    this.textField.text += '.';
                }
            }
            else if (parseFloat(this.textField.text+btn.children[3].text) <= 10000000 &&
                (dotIndex === -1 || this.textField.text.substr(dotIndex).length <= this.app.model.getCurrencyDecimals()))
            {
                if (this.textField.text === "0")
                {
                    this.textField.text = btn.children[3].text;
                }
                else
                {
                    this.textField.text += btn.children[3].text;
                }
            }
            this.editingField.update(this.textField.text);
        }
    }


    /**
     * Opens the keyboard
     */
    open(field)
    {
        this.editingField = field;
        this.textField.text = isNaN(field.text) ? 0 : field.text;
        this.editingField.update(this.textField.text);
        this.visible = true;
        this.updatePositions();
        this.app.dispatcher.on(C_GameEvent.RESIZE, this.updatePositions, this);
        document.addEventListener("keydown", this.onKeyDown);
    }


    /**
     * Closes the keyboard
     */
    closeKeyboard(discard)
    {
        document.removeEventListener("keydown", this.onKeyDown);
        if (discard)
        {
            this.textField.text = '0';
        }

        if (this.app.model.getCurrencyDecimals()>0)
        {
            if (this.textField.text.indexOf('.') === -1)
            {
                this.textField.text += ".0";
            }

            let n = this.app.model.getCurrencyDecimals() - (this.textField.text.length - 1 - this.textField.text.indexOf('.'));
            while(n > 0)
            {
                this.textField.text += '0';
                n--;
            }
        }

        this.editingField.update(this.textField.text);

        this.visible = false;
        this.app.dispatcher.removeListener(C_GameEvent.RESIZE, this.updatePositions);
    }

}
export default KeyboardView
