//--------------------------------------------------------------------------------------------------
// Scrolling Selector class - a gui component that lets the user scroll through a series of options
// Also has methods to instantly skip left / right (eg: from external buttons). Currently uses
// fixed visual styling (as required for the Bet / Autoplay menus), and only supports horizontal
// orientation.
//--------------------------------------------------------------------------------------------------

/**
 * @typedef ScrollingSelectorDimensions
 * @property {number} width
 * @property {number} height
 * @property {boolean} useExactWidth
 * If set to true, the Scrolling Selector will be sized to exactly the width provided.
 * In this mode, if the number of options provided is small, we may see the selector
 * being physically wider than the actual list of options. If set to false, then width
 * is treated as a maximum: if the number of options is small (and its possible to
 * shrink the width of the selector to match the number of options), then the selectors
 * width will match the number of options. If the number of options is large, the
 * selector's width will be clamped to the value provided.
 */

import BaseView from "../views/BaseView";
import C_PointerEvt from "../const/C_PointerEvt";

/**
 * Events dispatched locally by the Scrolling Selector.
 */
export const Events =
{
    /**
     * Event dispatched by a scrolling selector instance, when its selected value has changed.
     */
    SELECTED_VALUE_CHANGED : "selectedValueChanged"
}

/**
 * Scrolling Selector component. Presents the player with a horizontal area - showing a series of
 * numerical or string values. The "middle" value is treated as the selected value, and has a special
 * frame shown over the top of it. The component also creates a background - visual style is currently
 * hard-coded to the visual style required for the Bet Settings / Autoplay Settings menus.
 * 
 * The player can touch the selector, scrolling it left / right (there are also methods provided in the
 * api to move to the left / right, which could be triggered from buttons external to the selector).
 * The basic selector component does not provide any buttons (and never will), and does not make any
 * assumptions about how the selected value will be used. You can poll the selector to get the currently
 * selected value, and in addition, the selector will dispatch events (locally, on the scrolling selector
 * view itself) when the selected value changes.
 * 
 * The selector is given a "dimensions" object in its constructor (really, a set of config options).
 * You can configure target width and height : target width can either be used exactly, or treated
 * as a guideline for max width (see "useExactWidth" on the dimensions config object). The selector
 * can either show a limited number of options visually (but loop through a much larger set of available
 * values), or it can show many options, but have only a few possible values (so we see those values
 * looped on the selector).
 * 
 * @see Events
 */
export default class ScrollingSelector extends BaseView
{
    /**
     * @param {ScrollingSelectorDimensions} dimensions
     * @param {string[]|number[]} values
     * The full, ordered list of possible values that the player can select with this gui component.
     * @param {number} initialValueIndex
     * The index - from the values array - that should initially be treated as the "selected" value.
     * When the Scrolling Selector component is first shown, this is the value that will be shown at
     * the center position.
     */
    constructor(dimensions, values, initialValueIndex=0)
    {
        super();

        this.log.debug(`ScrollingSelector(dimensions:${JSON.stringify(dimensions)}, options:${JSON.stringify(values)})`);

        /**
         * Current index of the value selected. Defaults to 0, or can be passed in. If passed in,
         * we ensure its within the range specified by param options.
         * @private
         * @type {number}
         */
        this._selectedValueIndex = Math.max(0, Math.min(initialValueIndex, values.length));

        /**
         * The complete, ordered set of values available for selection.
         * @private
         * @type {*[]}
         */
        this._selectableValues = values;

        /**
         * Configuration for dimensions of the selector.
         * @private
         * @type {ScrollingSelectorDimensions}
         */
        this._dimensions = dimensions;

        // TODO: These static widths are probably no good, if some values are going to get large.
        const optionBoxAspectRatio = 1.77;

        /**
         * @private
         * @type {number}
         */
        this._singleOptionHeight = Math.round(dimensions.height * 0.80);
        
        /**
         * @private
         * @type {number}
         */
        this._singleOptionWidth = Math.round(this._singleOptionHeight * optionBoxAspectRatio);

        /**
         * Total visible width - in pixels - of the whole scrolling selector.
         * @private
         * @type {number}
         */
        this._visibleWidth = 0; // we calculate this below, when determining number of boxes

        // OK: We know the maximum width and height, so we know the max number of option boxes we can show
        // at any one time within the visible area. Our "selected option" should always be horizontally
        // centered. Therefore if the number of boxes is odd, then at rest, there are no partial boxes
        // shown at the edges of the selector. If number of boxes is even, we will show half a box at the
        // left / right edges.
        // 

        // At this point, we know the maximum total width of the whole selector, and we know the width
        // of a single option box in the selector. Now, we need to determine actual width, and actual
        // number of option boxes to create. The selector can be in one of 2 modes:
        // - "use Exact Width" - in which case, the selector MUST be exactly the width requested (it is
        //   likely we will have partial option boxes shown at the edges). In this mode, we have a set
        //   number of fully shown option boxes - which will always be an odd number - and an even number
        //   of additional boxes outside of this (likely to be 2 or 4). The only case where we may fail
        //   to set exact width, is if exact width requires us to show less than one complete option
        //   box.
        // - "dont use exact width" - in which case we set the width of the selector to an exact (odd)
        //   number of option boxes, and we will have 2 more option boxes created for padding / looping.

        let maxNumWholeOptionsVisible = Math.floor(dimensions.width / this._singleOptionWidth);
        let minNumWholeOptionsVisible = 1;
        
        this.log.debug(`ScrollingSelector: singleOptionWidth = ${this._singleOptionWidth}`);
        this.log.debug(`ScrollingSelector: maxNumOptionsVisible:${maxNumWholeOptionsVisible}, numValues:${values.length}`);

        let numOptionBoxesToCreate = 1;

        if (dimensions.useExactWidth)
        {
            let numWholeVisibleBoxes = Math.max(minNumWholeOptionsVisible, maxNumWholeOptionsVisible);

            // We need the whole number to be an odd number
            if (numWholeVisibleBoxes % 2 === 0) {
                numWholeVisibleBoxes -= 1;
            }

            numOptionBoxesToCreate = numWholeVisibleBoxes + 2; // 2 extra for padding

            // If our "complete" number of boxes create a width  less than target width,
            // we need another 2 padding boxes - except that these 2, we expect to be
            // partially shown at the edges, most of the time.
            if ((numWholeVisibleBoxes * this._singleOptionWidth) < dimensions.width) {
                numOptionBoxesToCreate += 2;
            }

            this._visibleWidth = Math.max(this._singleOptionWidth, dimensions.width);
        }
        else
        {
            let numVisibleBoxes = Math.min(maxNumWholeOptionsVisible, values.length);

            if (numVisibleBoxes < minNumWholeOptionsVisible) {
                numVisibleBoxes = minNumWholeOptionsVisible;
            }

            // When using "variable width" mode, we never have partial option boxes at the side:
            // we ALWAYS create an odd number, so our target number is exactly centered, and there
            // are only a complete number at the sides
            if (numVisibleBoxes % 2 == 0) {
                numVisibleBoxes -= 1;
            }

            numOptionBoxesToCreate = numVisibleBoxes + 2; // 2 spare boxes for padding

            this._visibleWidth = numVisibleBoxes * this._singleOptionWidth;

            this.log.debug(`ScrollingSelector: use variable width, numVisibleBoxes = ${numVisibleBoxes}`);
        }

        this.log.debug(`ScrollingSelector: numOptionBoxes:${numOptionBoxesToCreate}`);
        this.log.debug(`ScrollingSelector: visibleWidth:${this._visibleWidth}`);

        /**
         * Background to the scrolling selector component. This is also the interactive part
         * of the Scrolling Selector component (eg: the one that triggers touch and drag
         * events).
         * @private
         * @type {PIXI.Sprite}
         */
        this._scrollerBg = this.assets.getSprite('selectorBg');
        this._scrollerBg.width = this._visibleWidth;
        this._scrollerBg.height = dimensions.height;
        this._scrollerBg.interactive = true;
        this._scrollerBg.on(C_PointerEvt.DOWN, this.startDrag, this);
        this._scrollerBg.on(C_PointerEvt.UP, this.stopDrag, this);
        this._scrollerBg.on(C_PointerEvt.UPOUTSIDE, this.stopDrag, this);

        /**
         * A mask, used to hide any options which stray outside of the intended visible area.
         */
        let mask = new PIXI.Graphics();
        mask.beginFill(0xff00ff, .5);
        mask.drawRect(0, 0, this._scrollerBg.width * 0.99, this._scrollerBg.height);
        mask.endFill();
        this.layout.horizontalAlignCenter(mask, 0, this._scrollerBg);
        mask.y = this._scrollerBg.y;

        /**
         * Contains all Option Box components.
         * @private
         * @type {PIXI.Container}
         */
        this._optionBoxesContainer = new PIXI.Container();

        // Create the Option Boxes. As we have more Option Boxes than there are Option values,
        // we assign each Option Box a field, which is the Option Index it currently points to.
        // At any given moment, we expect that some Option Values will have more than one Option
        // Box referencing them (this allows for looping of the Options). The selected Option
        // is shown in the middle, so on initial creating of the Option boxes, we calculate the
        // appropriate Option Index to assign to each box. There will always be an even number
        // of OptionBoxes (an odd number will be fit within the selector, and we have one to
        // spare), so half the total number of optionBoxes, gives us the index of the "selected"
        // option box.
        
        /**
         * Starting horizontal (x) positions, for each of the options boxes. This list will have
         * the same length as the number of option boxes created.
         * @private
         * @type {number[]}
         */
        this._startHorizPositions = [];

        let largestNumCharsInValues = 0;
        
        values.forEach(value => {
            let numChars = value.toString().length;
            largestNumCharsInValues = Math.max(largestNumCharsInValues, numChars);
        });

        let propTextHeight = 1;

        if (largestNumCharsInValues <= 3) {
            propTextHeight = 0.75
        }
        else
        if (largestNumCharsInValues === 4) {
            propTextHeight = 0.68;
        }
        else
        if (largestNumCharsInValues === 5) {
            propTextHeight = 0.60;
        }
        // This should work ok for 6 chars, let's hope we don't go above that, or else this
        // ui mechanic (where each option has a fixed width) perhaps won't work so great.
        // We could always add functionality to adjust the fixed width of each item.
        else propTextHeight = 0.5;

        let optionBoxTextHeight = Math.round(this._singleOptionHeight * propTextHeight);

        for (let optionBoxIndex = 0; optionBoxIndex < numOptionBoxesToCreate; optionBoxIndex ++)
        {
            let optionBox = new PIXI.Container();

            // Empty sprite background, allows us to pad the overall size of each option box
            let optionBoxBg = this.assets.getSprite('empty_sym');
            optionBoxBg.width = this._singleOptionWidth;
            optionBoxBg.height = this._singleOptionHeight;

            this._startHorizPositions.push(optionBox.x);

            let valueTxt = new PIXI.Text(optionBoxIndex.toString(), {
                fontFamily : "Arial",
                fontSize : optionBoxTextHeight,
                fill : '#cfcfcf'
            });
            valueTxt.anchor.set(0.5);
            valueTxt.x = this._singleOptionWidth * 0.5;
            valueTxt.y = this._singleOptionHeight * 0.5;

            optionBox.addChild(optionBoxBg);
            optionBox.addChild(valueTxt);

            optionBox.x = this._singleOptionWidth * optionBoxIndex;
            optionBox.valueTxt = valueTxt;

            this._optionBoxesContainer.addChild(optionBox);
        }

        this.layout.horizontalAlignCenter(this._optionBoxesContainer, 0, this._scrollerBg);
        this.layout.verticalAlignMiddle(this._optionBoxesContainer, 0, this._scrollerBg);

        this.log.debug(`ScrollingSelector: optionsContainer.x = ${this._optionBoxesContainer.x}`);

        /**
         * A little red box, that surrounds the currently selected item.
         * @private
         * @type {PIXI.Sprite}
         */
        this._selectedOptionOverlay = this.assets.getSprite('selector_window');
        this._selectedOptionOverlay.height = this.layout.relHeight(100, this._scrollerBg);
        this._selectedOptionOverlay.width = this._selectedOptionOverlay.height * optionBoxAspectRatio;

        this.layout.horizontalAlignCenter(this._selectedOptionOverlay, 0, this._scrollerBg);
        this.layout.verticalAlignMiddle(this._selectedOptionOverlay, 0, this._scrollerBg);

        this.addChild(this._scrollerBg);
        this.addChild(this._optionBoxesContainer);
        this.addChild(mask);
        this._optionBoxesContainer.mask = mask;
        this.addChild(this._selectedOptionOverlay);

        this.updateOptionBoxAssignedValues();
        this.updateAllOptionBoxTextValues();

        /**
         * @private
         * @type {number}
         */
        this._scrollOffset = 0;
    }


    /**
     * @private
     */
    get scrollOffset()
    {
        return this._scrollOffset;
    }


    /**
     * Sets the value of ScrollOffset. This value is clamped to the size of a single Option Box
     * graphic within the scroller. Whenever the offset is changed by a sufficiently large value,
     * we can update the positions of all components shown.
     * @private
     * @param {number} value
     */
    set scrollOffset(value)
    {
        // We have moved now beyond a single choiceBox's width: we must update the cycling
        // of the choiceBox values.
        if (Math.abs(value) >= this._singleOptionWidth)
        {
            let cycleToRight = value > 0;

            this.log.debug(`set scrollOffset: cycle values. direction = ${cycleToRight? "right" : "left"}`);

            if (cycleToRight)
            {
                this.decrementSelectedValueIndex();
            }
            else
            {
                this.incrementSelectedValueIndex();
            }
        }

        this._scrollOffset = value % this._singleOptionWidth;

        for (let boxIdx = 0; boxIdx < this._optionBoxesContainer.children.length; boxIdx++)
        {
            this._optionBoxesContainer.children[boxIdx].x = this._startHorizPositions[boxIdx] + this._scrollOffset;
        }
    };


    /**
     * @private
     */
    incrementSelectedValueIndex()
    {
        this._selectedValueIndex = (this._selectedValueIndex + 1) % this._selectableValues.length;
        this.updateOptionBoxAssignedValues();
        this.updateAllOptionBoxTextValues();
        this.broadcastValueChangedNotification();
    };


    /**
     * @private
     */
    decrementSelectedValueIndex()
    {
        this._selectedValueIndex = (this._selectedValueIndex - 1 + this._selectableValues.length) % this._selectableValues.length;
        this.updateOptionBoxAssignedValues();
        this.updateAllOptionBoxTextValues();
        this.broadcastValueChangedNotification();
    };


    /**
     * Assigns an valueIndex to each optionBox. This is based on the current value of selectedvalueIndex:
     * the "middle" option always points to this value (and we must calculate the associated option indices
     * for all other Option Boxes, based on this). The method assumes that all options are sorted 
     * 
     * Suppose
     * - we have these possible values [5,10,15,20,25,30,45,60] - 8 in total, with indices 0 to 7
     * - we show 3 options boxes in the visible area, and have 5 in total (2 padding option boxes either
     *   side of the visible area). The option box in the middle (index 2) is the "selected option"
     * - selected value index is 2, eg: the value shown on the middle option is 15
     * - therefore, in the visible area, the values we are going to show are [10,15,20]
     * 
     * The logic in this method simply assigns an updated "value index" to each option box instance, based
     * on the current selected value index (eg: the pointer to the value that is supposed to be shown on
     * the center option box). A separate method will redraw the actual text for each option box, based on
     * the value index assigned to each option box.
     * 
     * NOTE, that we also support the case where there are more option boxes than available values, in
     * which case, the values get looped, eg: you might see (in the visible area) [15,5,10,15,5,10,15,5].
     * @private
     */
    updateOptionBoxAssignedValues()
    {
        this.log.debug(`ScrollingSelector.updateOptionBoxAssignedValues()`);

        // Calculate ordinal index of the selected option box - the one roughly in the
        // middle of our selector visually.
        let numValues = this._selectableValues.length;
        let optionBoxes = this._optionBoxesContainer.children;
        let numOptionBoxes = optionBoxes.length;
        let selectedOptionBoxIndex = (numOptionBoxes - 1) / 2;  // eg: the middle box
        let selectedValueIndex = this._selectedValueIndex;     // eg: index of value shown on middle box
        
        optionBoxes.forEach((optionBox, optionBoxIndex) =>
        {
            // This is a horrible calculation, but i am too tired to think how to simplify it.
            let distance = optionBoxIndex - selectedOptionBoxIndex;
            let valueIndex = (((((distance + numValues) % numValues) + selectedValueIndex) % numValues) + numValues) % numValues;

            optionBox.valueIndex = valueIndex;

            this.log.debug(`ScrollingSelector: setting option[${optionBoxIndex}] to valueIndex ${valueIndex}`);
        });
    };


    /**
     * @private
     * @param {*} event
     */
    startDrag(event)
    {
        event.stopPropagation();
        event.target.on(C_PointerEvt.MOVE, this.updatePos, this);

        /**
         * @private
         * @type {PIXI.Point}
         */
        this._lastDragPt = this._optionBoxesContainer.toLocal(event.data.global);

        // If any tween in progress to bring the scroller back to its nearest box,
        // its important we kill it, or else constant touch / release could end
        // everything offset really messily.
        if (this._snapToClosestBoxTween) {
            this._snapToClosestBoxTween.kill();
            this._snapToClosestBoxTween = null;
        }

        // Cache the current x position for all items: we use this to
        // calculate position while updating a drag.
        for (let boxIdx = 0; boxIdx < this._optionBoxesContainer.children.length; boxIdx++)
        {
            this._startHorizPositions[boxIdx] = this._optionBoxesContainer.children[boxIdx].x;
        }
    };


    /**
     * @private
     * @param {*} event
     */
    stopDrag(event)
    {
        event.stopPropagation();
        event.currentTarget.removeListener(C_PointerEvt.MOVE, this.updatePos, this);

        this.snapToClosestBox();
    };


    /**
     * Updates the position of all Option Boxes within the container, whilst a drag is in progress.
     * @private
     * @param {*} event
     */
    updatePos(event)
    {
        event.stopPropagation();
        let currDragPt = this._optionBoxesContainer.toLocal(event.data.global);

        // TODO: we could perform a modulo on deltaX - this could prevent any
        // drag being too large (possible on desktop), which might break the
        // scrolling selector


        let deltaX = currDragPt.x - this._lastDragPt.x;

        this.scrollOffset += deltaX;

        // TODO: Document correctly
        this._lastDragPt = currDragPt;
    };


    /**
     * Updates the value text shown on all Option Boxes. Each OptionBox has an valueIndex field
     * (points to the index within options): this is how we choose the correct text value to show
     * on the Option Box.
     * @private
     */
    updateAllOptionBoxTextValues()
    {
        this.log.debug(`ScrollingSelector.updateAllOptionBoxTextValues()`);

        let optionBoxes = this._optionBoxesContainer.children;

        optionBoxes.forEach(optionBox =>
        {
            let optionText = this._selectableValues[optionBox.valueIndex];
            optionBox.valueTxt.text = optionText.toString();
        });
    };


    /**
     * Snaps the Scrolling Selector to the value nearest to the center, using a tween.
     * This can be called at the end of a drag, where we want to show a short animation
     * of the Scroller snapping to whatever value is nearest to the "selected value"
     * icon.
     * @private
     */
    snapToClosestBox()
    {
        // To snap the Selector to a central position after a drag,
        // we basically tween "scrollOffset" back to 0
        let maxTime = 0.3;
        let timeCoefficient = Math.abs(this.scrollOffset / this._singleOptionWidth);
        let time = maxTime * timeCoefficient;

        /**
         * A cached reference to a tween in progress, which snaps the Scroller back to the nearest
         * value once it is physically released from a drag by the player.
         * @private
         */
        this._snapToClosestBoxTween = TweenMax.to(
            this,
            time,
            {
                scrollOffset : 0,
                onComplete : () => {
                    this._snapToClosestBoxTween = null;
                    this.broadcastValueChangedNotification();
                }
            }
        );
    };


    /**
     * Kills any tweens that may be in progress, moving the scrolling selector.
     * @private
     */
    killScrollTweens()
    {
        TweenMax.killTweensOf(this, {scrollOffset:true});
    };


    /**
     * @private
     */
    broadcastValueChangedNotification()
    {
        this.emit(Events.SELECTED_VALUE_CHANGED);
    };


    /**
     * Returns the current selected value.
     * @public
     * @return {any}
     */
    getSelectedOptionValue()
    {
        return this._selectableValues[this._selectedValueIndex];
    };


    /**
     * Returns the index (within options) that is currently selected by this component.
     * @public
     * @return {number}
     */
    getSelectedValueIndex()
    {
        return this._selectedValueIndex;
    };


    /**
     * Reports the visible width of the Scrollable Selector. This is the width of what is visible (excludes
     * any masked horizontal content)
     * @public
     * @return {number}
     */
    getVisibleWidth()
    {
        return this._visibleWidth;
    };


    /**
     * Selects the next available option to the right.
     * @public
     */
    selectNextOptionValue()
    {
        this.log.debug('selectNextOptionValue()');

        // TODO: might still need to account for "is currently being dragged"

        this.killScrollTweens();
        this.incrementSelectedValueIndex();
    }


    /**
     * Selects the previous available option to the left.
     * @public
     */
    selectPrevOptionValue()
    {
        this.log.debug('selectPrevOptionValue()');

        // TODO: might still need to account for "is currently being dragged"

        this.killScrollTweens();
        this.decrementSelectedValueIndex();
    }

}