import BaseUI from "./BaseUI";
import C_PointerEvt from "../const/C_PointerEvt";
import MathUtil from "../utils/MathUtil";
import C_UIEvent from "../const/C_UIEvent";

/**
 * Scrollbar UI element
 */
class Scrollbar extends BaseUI
{

    /**
     * 
     * @param {number} width 
     * @param {number} height 
     * @param {number | string} [bgColour]
     * The colour of the bg component.
     * @param {number | string} [btnColour]
     * The colour of the button component.
     */
    constructor(width, height, bgColour=0x999999, btnColour=0xffffff)
    {
        super();

        this.w = width;
        this.h = height;

        this.bg = new PIXI.Graphics();
        this.bg.beginFill(bgColour);
        this.bg.drawRect(0, 0, this.w, this.h);
        this.bg.endFill();
        this.addChild(this.bg);

        this.thumb = new PIXI.Graphics();
        this.thumb.beginFill(btnColour);
        this.thumb.drawRect(0, 0, this.w, this.h/4);
        this.thumb.endFill();
        this.addChild(this.thumb);
        this.thumb.interactive = true;
        this.thumb.on(C_PointerEvt.DOWN, this.startDrag, this);
        this.thumb.on(C_PointerEvt.UP, this.stopDrag, this);
        this.thumb.on(C_PointerEvt.UPOUTSIDE, this.stopDrag, this);

        this.minY = 0;
        this.maxY = this.h - this.thumb.height;
    }


    startDrag(e)
    {
        e.stopPropagation();
        this.thumb.on(C_PointerEvt.MOVE, this.updatePos, this);

        this.startDragPt = this.layout.getRelCoords(e.data.global);
        this.startPt = new PIXI.Point(this.thumb.x, this.thumb.y);
    }


    stopDrag(e)
    {
        e.stopPropagation();
        this.thumb.removeListener(C_PointerEvt.MOVE, this.updatePos, this);
    }


    updatePos(e)
    {
        e.stopPropagation();
        let currDragPt = this.layout.getRelCoords(e.data.global);
        let deltaY = this.startDragPt.y - currDragPt.y;
        this.thumb.y = MathUtil.clamp(this.startPt.y - deltaY, this.minY, this.maxY);
        this.emit(C_UIEvent.UPDATE);
    }


    getPosition()
    {
        return this.thumb.y / this.maxY;
    }


    get yPosition()
    {
        return this.thumb.y / this.maxY;
    }


    set yPosition(pos)
    {
        this.thumb.y = this.maxY * pos;
    }


}

export default Scrollbar