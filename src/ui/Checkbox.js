import BaseUI from "./BaseUI";
import C_PointerEvt from "../const/C_PointerEvt";
import C_UIEvent from "../const/C_UIEvent";

/**
 * Checkbox UI element
 */
class Checkbox extends BaseUI
{


    constructor()
    {
        super();

        this.bg = this.assets.getSprite("checkbox_bg");
        this.addChild(this.bg);

        this.check = this.assets.getSprite("checkbox_selected");
        this.addChild(this.check);
        this.check.alpha = 0;

        this.interactive = true;
        this.on(C_PointerEvt.DOWN, this.onDown, this);
        this.selected = false;
    }


    onDown()
    {
        this.selected = !this.selected;
        TweenMax.to(this.check, .25, {alpha:Number(this.selected)});
        this.emit(C_UIEvent.UPDATE);
    }


    setIsSelected(isSelected)
    {
        this.selected = isSelected;
        this.check.alpha = Number(this.selected);
    }


    isSelected()
    {
        return this.selected;
    }


}


export default Checkbox