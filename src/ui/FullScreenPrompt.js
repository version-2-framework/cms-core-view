import BaseView from "../views/BaseView";
import * as screenfull from "screenfull";

export default class FullScreenPrompt extends BaseView
{
    constructor()
    {
        super();

        /**
         * Method which will activate the appropriate fullscreen gesture.
         * @type {() => void}
         */
        this._activateFullScreenGesture = this.activateSwipeForFullScreenGesture;
        //this._activateFullScreenGesture = this.activateTouchForFullScreenGesture;

        /**
         * Method which will deactivate all fullscreen gestures. May not always be configured: when it is, it will
         * deactive whatever "fullscreen gesture" is currently enabled (usually will be "swipe for fullscreen", but
         * we have some testing alternatives)
         * @private
         * @type {() => void}
         */
        this._deactivateFullScreenGesture = null;

        // Initially, this view should be invisible, until it is explicitly set to visible
        // (which happens only when we need to see it)
        this.visible = false;

        let isAndroid = this.model.isAndroid();
        let isFullScreenAvailable = this.isScreenfullAvailable();
        

        this.log.info(`FullScreenPrompt.constructor: isAndroid:${isAndroid}, isFullScreenAvailable:${isFullScreenAvailable}`);

        // There are currently 2 separate pieces of functionality here
        // 1) Showing a "swipe for full-screen prompt". We can show this on lots of mobile ios
        //    devices: iOS (at least on phones) supports it natively, and on android it's easy
        //    to implement: we can detect the swipe gesture, and programatically trigger the
        //    change to full-screen
        // 2) As can be seen for point (1), we only detect the swipe / attempt to programatically
        //    trigger fullscreen on android (and if fullscreen is available)
        
        // TODO: Currently, this view isn't even shown for iOS (it may be in the future), at which
        // point this logic will need to become air-tight

        // TODO: Above TODO comment clearly wrong, we show the prompt when not android! Looks like i did
        // some changes a year or so ago. Worth reviewing what I did and why
        let enableFullScreenVisualPrompt = (isAndroid && isFullScreenAvailable) || !isAndroid;
        let enableFullScreenGesture = isAndroid && isFullScreenAvailable;

        /**
         * Tracks whether the fullscreen visual prompt is to be shown (not the same thing as whether it IS
         * currently being shown)
         * @private
         * @type {boolean}
         */
        this._isFullScreenVisualPromptEnabled = enableFullScreenVisualPrompt;

        if (enableFullScreenVisualPrompt) {
            this.createSwipeForFullScreenVisualPrompt();
        }
        else
        {
            this.log.info("FullScreenPrompt: fullscreen mode not available, don't show swipeForFullScreen animation");
        }

        if (enableFullScreenGesture) {
            this._activateFullScreenGesture();
        }
        else
        {
            this.log.info("FullScreemPrompt: fullscreen mode not available for android, don't configure swipe gesture");
        }

        // TODO: This listeners may be android specific, when
        // we improve the iOS support, it may have to be addressed again.
        document.addEventListener('visibilitychange', e => this.onVisibilityChanged(e));
        document.addEventListener('fullscreenchange', e => this.onFullScreenChanged(e));

        if (enableFullScreenVisualPrompt)
        {
            if (this.isFullScreenModeActive() === false)
            {
                this.log.debug("FullScreenPrompt.createSwipeForFullScreenPrompt: not in fullscreen, show prompt");

                this.showSwipeForFullScreenVisualPrompt();
            }
            else
            {
                this.log.debug("FullScreenPrompt.constructor: already in fullscreen mode, don't show prompt yet");
            }
        }

        // TESTING
        // is the whole html area getting set to invisible, somehow ?
        new MutationObserver(mutations =>
        {
            let docEl = document.documentElement;
            let docElStyle = getComputedStyle(docEl);

            //this.log.info(`docEl.style.visibility = ${docElStyle.visibility}`)
            //this.log.info(`docEl.style.opacity = ${docElStyle.opacity}`)

            // Let's see all immediate children of the document
            Array.from(document.children).forEach((child, childIndex)=>{
                this.log.info(`doc.child[${childIndex}] = { tag:${child.tagName}, id:${child.id}, className:${child.className}}`)
            });

            // and let's see all immediate children of the Html element
            // Let's see all immediate children of the document
            Array.from(document.documentElement.children).forEach((child, childIndex)=> {
                let childStyle = getComputedStyle(child);

                let childStats =
                {
                    tag : child.tagName,
                    id : child.id,
                    className : child.className,
                    visibility : childStyle.visibility,
                    opacity : childStyle.opacity
                }

                this.log.info(`docEl.child[${childIndex}] = ${JSON.stringify(childStats)}`);
            });

            /*
            mutations.forEach(mutation =>
            {
                this.log.info(`html.style: ${JSON.stringify(mutation)}`);
            });
            */
        }).observe(document.documentElement, { attributes:true, childList:false, subtree:true});
    };


    /**
     * Creates (but does not necessarily show) the "swipe for fullscreen" visual prompt.
     * @private
     */
    createSwipeForFullScreenVisualPrompt()
    {
        this.log.debug('FullScreenPrompt.createSwipeForFullScreenPrompt()');
        
        /**
         * Used to block touch input to layers below - we are trying to force the player to
         * enter fullscreen mode.
         * @private
         * @type {PIXI.Graphics}
         */
        this._touchBlocker = new PIXI.Graphics();
        this._touchBlocker.beginFill(0x000000);
        this._touchBlocker.drawRect(0, 0, this.layout.relWidth(100), this.layout.relHeight(100));
        this._touchBlocker.endFill();
        this._touchBlocker.alpha = 0.3;
        this._touchBlocker.interactive = true;
        this.addChild(this._touchBlocker);

        /**
         * A hand graphic, which we animate moving to indicate to the player that they can
         * swipe for entering fullscreen mode.
         * @private
         * @type {PIXI.Sprite}
         */
        this._handGraphic = this.assets.getSprite('hand');
        this._handGraphic.anchor.set(0.5, 0.5);
        this._handGraphic.x = this.layout.relWidth(50);
            
        this.layout.propHeightScale(this._handGraphic, 25);
        this.addChild(this._handGraphic);
    };


        // TODO: This never gets disabled: to do that, perhaps i adjust the code a little
    /**
     * Configures event listeners, that will listen out for gestures that should be interpreted as "swip for
     * fullscreen". When such a swipe is detected, we then use Screenfull to try and enter fullscreen mode.
     * This method will assign a value to local field "_deactivateFullScreenGesture" - calling that method,
     * will kill the created event listeners (this can be done when we enter fullscreen mode). Calling this
     * method again when we exit fullscreen, sets the gesture event listeners back up.
     * 
     * This method should should only be used on Android, and when we are able to use screenfull. Essentially,
     * we are imitating the kind of swipe-for-fullsreen gestures that should work automatically on iOS.
     * @private
     */
    activateSwipeForFullScreenGesture()
    {
        this.log.debug(`FullScreenPrompt.activateSwipeForFullScreenGesture()`);

        let touchStartPosY;

        /**
         * Handler called when touch starts.
         * @param {TouchEvent} event
         */
        let touchStartHandler = event =>
        {
            event.preventDefault();

            touchStartPosY = event.changedTouches[0].screenY;

            this.log.debug(`FullScreenPrompt: touchStart at pos ${touchStartPosY}`);
        }

        /** @param {TouchEvent} event */
        let touchEndHandler = event =>
        {
            event.preventDefault();

            let touchFinalPosY = event.changedTouches[0].screenY;
            let swipeDeltaY = touchStartPosY - touchFinalPosY;
            let fullScreenMinDeltaY = window.screen.height * 0.02;

            if (swipeDeltaY > fullScreenMinDeltaY)
            {
                this.log.debug(`FullScreenPrompt: swipe was ${swipeDeltaY} pixels, we should try and enter full-screen mode`);

                this.tryAndEnterFullScreen();
            }
            else
            {
                this.log.debug(`FullScreenPrompt: swipe was ${swipeDeltaY} pixels, not far enough to enter full-screen (needs to be ${fullScreenMinDeltaY} pixels)`);
            }
        }
        
        document.body.addEventListener('touchstart', touchStartHandler, false);
        document.body.addEventListener('touchend', touchEndHandler, false);

        // When we enter fullscreen, the gesture actions wil
        this._deactivateFullScreenGesture = () =>
        {
            this.log.debug(`FullScreenPrompt.deactivateSwipeForFullScreenGesture()`);

            document.body.removeEventListener('touchstart', touchStartHandler, false);
            document.body.removeEventListener('touchend', touchEndHandler, false);
        }
    };


    // TODO: Touch on where ?
    /**
     * @private
     */
    activateTouchForFullScreenGesture()
    {
        this.log.debug(`FullScreenPrompt.activateTouchForFullScreenGesture()`);

        let onScreenTouched = event =>
        {
            this.tryAndEnterFullScreen();
        }

        // when we use touchend, we get 
        let touchEventId = 'touchend'; // 'touchstart

        document.body.addEventListener(touchEventId, onScreenTouched, false);

        this._deactivateFullScreenGesture = () =>
        {
            this.log.debug(`FullScreenPrompt.deactivateTouchForFullScreenGesture()`);

            document.body.removeEventListener(touchEventId, onScreenTouched, false);
        }
    };


    /**
     * Checks if we are currently in FullScreen mode.
     * @private
     * @return {boolean}
     * True if the game appears to be in fullscreen, false if not.
     */
    isFullScreenModeActive()
    {
        // document.fullscreenElement will point to the element that
        // is in fullscreen mode if there is one. If not, the value
        // of the property is null.
        return document.fullscreenElement != null;
    };


    /**
     * @private
     * @param {Event} eventData 
     */
    onVisibilityChanged(eventData)
    {
        this.log.info(`FullScreenPrompt.onVisibilityChanged: visibilityState = ${document.visibilityState}`);
    }


    /**
     * Action invoked when fullscreen state has changed.
     * @private
     * @parram {*} eventData
     */
    onFullScreenChanged(eventData)
    {
        if (this.isFullScreenModeActive())
        {
            this.log.info(`FullScreenPrompt.onFullScreenChanged: entered fullscreen mode`);

            if (this._deactivateFullScreenGesture) {
                this._deactivateFullScreenGesture();
                this._deactivateFullScreenGesture = null;
            }

            if (this._isFullScreenVisualPromptEnabled)
            {
                this.hideSwipeForFullScreenVisualPrompt();
            }
        }
        else
        {
            this.log.info(`FullScreenPrompt.onFullScreenChange: exited fullscreen mode`);

            if (this._activateFullScreenGesture) {
                this._activateFullScreenGesture();
            }
            
            if (this._isFullScreenVisualPromptEnabled)
            {
                this.showSwipeForFullScreenVisualPrompt();
            }
        }
    };


    /**
     * Shows the FullScreenPrompt in a visual state, where the player is effectively forced to make
     * a swiping gesture to enter fullscreen
     * - the whole FullScreenPrompt is un-hidden
     * - the fullscreen prompt has a touchblocker in it (sem-transparent, so the player gets a visual
     *   cue), meaning they cannot interact with anything below
     * - an animation is played, of a hand swiping upwards on the screen
     * @private
     */
    showSwipeForFullScreenVisualPrompt()
    {
        this.log.debug('FullScreenPrompt.showSwipeForFullScreen()');

        this.visible = true;

        let handGraphic = this._handGraphic;
        let handStartY = this.layout.relHeight(65);
        let handFinalY = this.layout.relHeight(35);

        /**
         * An infinitely looping timeline, which will move the hand back and forth.
         * @private
         * @type {GSAP.TimelineMax}
         */
        this._timeline = new TimelineMax({ repeat:-1 });
        this._timeline.fromTo(handGraphic, 0.25, { alpha:0 }, { alpha:1 });
        this._timeline.fromTo(handGraphic, 2.00, { y:handStartY }, { y:handFinalY });
        this._timeline.fromTo(handGraphic, 0.20, { alpha:1 }, { alpha:0 });
    };


    /**
     * Clears any swipe for fullscreen prompt which may be showing (kills the animation,
     * hides the hand graphic). The whole FullScreenPrompt is hidden by this method.
     * @private
     */
    hideSwipeForFullScreenVisualPrompt()
    {
        this.log.debug('FullScreenPrompt.hideSwipeForFullScreenVisualPrompt()');

        if (this._timeline) {
            this._timeline.kill();
            this._timeline = null;
        }
        
        this.visible = false;
    };


    // TODO: The swipe in this case is a custom event (currently being generated by code in the html
    // page, but it will probably get moved to a dedicated code module). It only recognizes a general
    // swipe gesture.
    // Rusells note (10.06.2020)
    // My huaweii p-smart has its own "swipe to enter/exit minimal ui" feature on chrome. I am not
    // certain yet if this is a standard feature on android chrome, or something specific to the huaweii
    // build - i cannot find any documentation on it. It doesn't 100% play nicely with the custom swipes
    // (basically, if you swipe to enter minimal ui, I don't yet know how to recognize that this has been
    // done). However, for now, its working acceptably (if you enter minimal ui, you are forced to then
    // enter true fullscreen). I would like to improve this, because the minimal ui gesture works so much
    // better than invoking fullscreen api (which for our games at least, often triggers a series of ugly
    // resize steps, as the browser gets its arse into gear and works out how large and where the view
    // should be drawn). I've seen this behavior on a number of android mobile devices, but it could be
    // peculiar the way our games (fixed size canvas element with fixed positioning) are embeded in the
    // html.

    
    // TODO: This method is named really poorly.. and also not really documented. I cannot tell if
    // a) Its only meant to "enter fullscreen if not already there"
    // b) its meant to toggle fullscreen state (thats what the next implies)
    // seems the answer is (a), but we are clearly calling it overzealously.
    // I think this method is useless - and can basically be merged in with the actual "trigger screenfull"
    /**
     * Triggers the most appropriate mechanism for entering full-screen.
     * @private
     */
    tryAndEnterFullScreen()
    {
        // TODO: We need to move to using "platformUtil" to check this
        // TODO: The actual check - in this method - only makes sense if we have alternate ways
        // of triggering fullscreen (in fact, for certain iOS devices, we probably should, eg:
        // some iOS tablets support such functionality, even on modern iOS versions - hard to tell
        // as Apple change their minds frequently on whether their browsers should work the same
        // way that everyone else's do)
        // Would we be better off with extended classes for Android / iOS? Would that make the
        // logic a bit cleaner ?
        if (this.model.isAndroid())
        {
            if (screenfull && screenfull.enabled)
            {
                this.log.debug(`FullScreenPrompt.tryAndEnterFullScreen.Android: try to use screenfull`);

                screenfull.request();
            }
            else
            {
                this.log.warn(`FullScreenPrompt.tryAndEnterFullScreen.Android: fullscreen / screenfull apparently not available`);
            }
        }
        // TODO: An explicit "iOS" check here would be useful for debugging.
        else
        {
            this.log.debug('FullScreenPrompt.tryAndEnterFullScreen: not an android device, not attempting to trigger fullscreen');
        }
    };


    /**
     * Check if FullScreen appears to be enabled (not sure if screenfull's "enabled" flag is equivalent to
     * this)
     * @private
     * @return {boolean}
     */
    isScreenfullAvailable()
    {
        return (screenfull && screenfull.enabled);
    };
}