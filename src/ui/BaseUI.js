/**
 * Base class for UI components
 */
import { getGameViewLogger } from "../../../cms-core-logic/src/logging/LogManager";
import { KeyboardManager } from "../input/KeyboardManager";
import Assets from "../utils/Assets";
import Layout from "../utils/Layout";

// TODO: This class, and its reason for existence, need explaining and documenting
class BaseUI extends PIXI.Container
{
    constructor()
    {
        super();
        
        /**
         * @protected
         */
        this.app = PIXI.app;

        /**
         * @protected
         * @final
         * @type {Assets}
         */
        this.assets = this.app.assets;

        /**
         * @protected
         * @final
         * @type {Layout}
         */
        this.layout = this.app.layout;

        /**
         * @protected
         * @final
         * @type {EventEmitter}
         */
        this.dispatcher = this.app.dispatcher;

        /**
         * @protected
         * @final
         * @type {Model}
         */
        this.model = this.app.model;

        /**
         * @protected
         * @final
         * @type {KeyboardManager}
         */
        this.keyboardManager = this.app.keyboardManager;

        /**
         * @protected
         * @final
         */
        this.log = getGameViewLogger();
    };


    /**
     * Sets if the UI element is interactive. Executes a short animated tween on this
     * contain's alpha channel.
     * @public
     * @param {boolean} enabled
     */
    setEnabled(enabled)
    {
        this.interactive = enabled;
        TweenMax.to(this, .25, {alpha:.5 + .5 * Number(enabled)});
    };
}

export default BaseUI;