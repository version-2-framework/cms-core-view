//--------------------------------------------------------------------------------------------------
// History Tab
//--------------------------------------------------------------------------------------------------
// Graphics classes that relate to common Menu.HistoryTab behavious, will go here.
// We may need to expand the history behaviour. So, if we require more graphics
// classes, just keep them in this one file.
//--------------------------------------------------------------------------------------------------
import MenuTab from "./MenuTab";
import C_PointerEvt from "../../const/C_PointerEvt";

/**
 * A simple tab graphic, that can be used to show a "loading history" message.
 */
export class HistoryTab extends MenuTab
{
    /**
     * Constructs a new History Tab
     * @param {number} tabWidth
     * @param {number} tabHeight
     */
    constructor(tabWidth, tabHeight)
    {
        super(tabWidth, tabHeight);

        let isRealplay = this._model.getSessionIsRealPlay();
        let isFunBonus = this._model.getSessionIsFunBonus();
        
        this.log.debug(`HistoryTab.created(width:${tabWidth},height:${tabHeight})`);

        let historyInfoTextStyle =
        {
            fontFamily : "Arial",
            fontSize : Math.round(this.textDownScaleFactor * this.layout.relHeight(5)),
            fill : this.config.gameMenuStyle.textColour,
            wordWrap : true,
            wordWrapWidth : Math.round(this.textDownScaleFactor * tabWidth),
            align : 'center'
        };

        this.log.debug(`HistoryTab: textStyle = ${JSON.stringify(historyInfoTextStyle)}`);

        if (isRealplay || isFunBonus)
        {
            this.log.info('HistoryTab: game in realplay mode, show external history link');

            let externalHistoryInfoMsg = this.locale.getString("T_menu_ExternalHistoryInfo");
            let externalHistoryInfoText = new PIXI.Text(externalHistoryInfoMsg, historyInfoTextStyle);
            externalHistoryInfoText.anchor.set(0.5, 0);
            externalHistoryInfoText.x = tabWidth * 0.5;
            externalHistoryInfoText.scale.set(this.textUpScaleFactor);

            let btnMaxWidth = tabWidth * 0.9;
            let openHistoryButton = this.createTextButton("T_menu_OpenHistoryButton", btnMaxWidth);

            let padY = tabHeight * 0.1;

            openHistoryButton.x = (tabWidth - openHistoryButton.width) * 0.5;
            openHistoryButton.y = externalHistoryInfoText.y + externalHistoryInfoText.height + padY;

            this.addChild(externalHistoryInfoText);
            this.addChild(openHistoryButton);

            if (this.height < tabHeight) {
                let offsetY = (tabHeight - this.height) * 0.5;

                externalHistoryInfoText.y += offsetY;
                openHistoryButton.y += offsetY;
            }

            openHistoryButton.on(C_PointerEvt.DOWN, () => {
                this.log.debug('HistoryTab.openExternalHistory()');

                // We send slightly different parameters for V1 / V2
                if (BUILD_TARGET === "v1")
                {
                    this._openLinkUtil.openLink(
                        this._businessConfig.externalHistoryAction,
                        { nickname:this._model.getPlayerNickName() }
                    );
                }
                else
                if (BUILD_TARGET === "v2")
                {
                    this._openLinkUtil.openLink(
                        this._businessConfig.externalHistoryAction,
                        { userId:this._model.getPlayerNickName() }
                    )
                }
            });
        }
        else
        {
            let sessionType = this._model.getSessionType();

            this.log.info(`HistoryTab: game not in realplay mode (sessionType:${sessionType}), show history not available message`);

            let historyNotAvailableMsg = this.locale.getString("T_menu_ExternalHistoryNotAvailable");
            let historyNotAvailableText = new PIXI.Text(historyNotAvailableMsg, historyInfoTextStyle);
            historyNotAvailableText.anchor.set(0.5, 0.5);
            historyNotAvailableText.x = tabWidth * 0.5;
            historyNotAvailableText.y = tabHeight * 0.5;
            historyNotAvailableText.scale.set(this.textUpScaleFactor);

            this.addChild(historyNotAvailableText);
        }
    };
};