import BaseView from "../../views/BaseView";

/**
 * Horizontal padding (from left and right edges of screen) to screen
 * content. This is as a percentage of screen width.
 */
const CONTENT_PAD_X = 5;

/**
 * Horizontal padding that should exist between any 2 consecutive horizontal
 * items
 */
const ITEM_PAD_X = 2;

const ITEM_PAD_Y = 4;

const SECTION_PAD_Y = 8;

/**
 * Horizontal offset (from the left-most point of any Symbol Item) of the Symbol Icon.
 * This value is written as a percentage of screen width.
 */
const SYM_OFFSET_X = 0;

/**
 * Width of a Symbol in the paytable, as a proportion of screen width. This same width
 * is used either for the Special Symbol Info item, or a Regular Symbol Info item.
 */
const SYM_WIDTH = 12;

/**
 * Horizontal offset (from the left-most point of any Symbol Item) of the Symbol Prize
 * text field. This value is written as a percentage of screen width.
 */
const SYM_PRIZE_OFFSET_X = 14;

/**
 * Expected width of Symbol Prize text, as a proportion of screen width. We do not
 * explicitly set the width of this text to this value, but it is the area that we allow
 * when 
 */
const SYM_PRIZE_WIDTH = 14;

/**
 * Horizontal offset (from the left-most point of a Special Symbol Item) of the Special
 * Symbol Info text field. This value is written as a percentage of screen width.
 */
const SPECIAL_SYM_TEXT_OFFSET_X = 30;

/**
 * The width of the Special Symbol description text, as a percentage of screen width.
 */
const SPECIAL_SYM_TEXT_WIDTH = 60;

/**
 * Total width (as a percentage of screen width) of a single Regular Symbol Info item.
 * We use this value, to lay out the regular symbols into columns.
 */
const REGULAR_SYM_INFO_WIDTH = 30;

/**
 * The number of Regular Symbol Info items we can stash side by side in
 * a single row, before we need to start a new row.
 */
const NUM_REGULAR_SYM_INFO_PER_ROW = 3;

/**
 * The number of Winline graphics that will be displayed in a single row.
 */
const NUM_WINLINES_PER_ROW = 5;

/**
 * The total width allotted for the Winline Number on the left of a Winline.
 */
const WINLINE_NUMBER_WIDTH = 3;

/**
 * The total width of a single Winline, as a percentage of screen width. This includes
 * - the Winline Number text on the left
 * - the padding between the Winline Number and the actual Winline
 * - the total width of the winline, including the internal padding between reels.
 * It doesn't include padding between Winlines
 */
const WINLINE_WIDTH = 16;

/**
 * The horizontal padding between each Winline, as a percentage of screen width. This
 * is the padding from the right edge of a previous Winline, to the left edge of the
 * Winline Number on the next winline.
 */
const WINLINE_PAD_X = 2.5;

/**
 * The cvertical padding between each Winline, as a percentage of screen height.
 */
const WINLINE_PAD_Y = 2.5;

/**
 * Vertical padding that should be placed between a piece of Title text, and whatever
 * content that follows it. This is represented as a percentage of Screen height.
 */
const TITLE_PAD_Y = 2.5;

const PARAGRAPH_IMAGE_WIDTH_LARGE = 40;

const PARAGRAPH_IMAGE_WIDTH_SMALL = 20;

const PARAGRAPH_IMAGE_PAD_X = 5;

// TODO: Let's do some static initialization here for now, while we get this working with bitmap fonts.
// OK, problem: PIXI 4 apparently doesn't support automatic loading of bitmap fonts (eg: automatically
// generate texture and XML data from a normal webfont) - its quite an important feature if we are going
// to render with bitmap fonts, i think ? Otherwise, we are forced to use whatever we can pre-create,
// and this limits us in many respects:
// - limits us on styling (no dynamic styling)
// - limits us with regard to language!!!


export class RulesRenderer extends BaseView
{
    /**
     *
     * @param {RulesConfig} rulesConfig
     * @param {number} [targetWidth]
     * Target width that the Rules Renderer should generate its content into. If not passed, then
     * screen width will be assumed instead.
     */
    constructor(rulesConfig, targetWidth=null) {
        super();

        this.log.debug(`RulesRenderer.constructor: ${JSON.stringify(rulesConfig)}`);

        /**
         * Scale factor, which can reduce the resolution that all text is rendered at.
         * @private
         * @type {number}
         */
        this._textDownScaleFactor = this.platform.isDesktop()? 1 : 0.25;

        /**
         * This is the amount by which we must up-scale created text items..
         * @private
         * @type {number}
         */
        this._textUpScaleFactor = 1  / this._textDownScaleFactor;

        /**
         * The target width (in pixels) that the Rules should be drawn on-screen. For sensible
         * functioning of the Rules Renderer, this should be the real screen value (eg: with
         * no further scaling applied)
         * @private
         * @type {number}
         */
        this._targetWidth = targetWidth || this.layout.relWidth(100);

        /**
         * Standard vertical padding between items (eg: within a single section)
         * @private
         * @type {number}
         */
        this._itemPadY = this.layout.relHeight(ITEM_PAD_Y);

        /**
         * Vertical padding between sections.
         * @private
         * @type {number}
         */
        this._sectionPadY = this.layout.relHeight(SECTION_PAD_Y);

        /**
         * Start X position of regular items.
         * @private
         * @type {number}
         */
        this._itemStartX = 0; //this.layout.relWidth(CONTENT_PAD_X);

        /**
         * Final X position of regular items.
         * @private
         * @type {number}
         */
        this._itemEndX = this._targetWidth; //this.layout.relWidth(100 - CONTENT_PAD_X);

        /**
         * The total width of any item. This is the full screen width, minus the left & right padding.
         * @private
         * @type {number}
         */
        this._itemTotalWidth = this._itemEndX - this._itemStartX;

        /**
         * The start X positions of each of the 3 columns of regular sybols. The key
         * in this object is column index.
         * @private
         * @type {{ [columnId:number]:number }}
         */
        this._regularSymPosX = (() => {
            let positionsForColumn = {};
            for (let i = 0; i < NUM_REGULAR_SYM_INFO_PER_ROW; i ++) {
                positionsForColumn[i] = this.layout.relWidth(CONTENT_PAD_X + (i * REGULAR_SYM_INFO_WIDTH))
            };
            return positionsForColumn;
        })();

        /**
         * Vertical padding to place after a piece of Title text, before the content that it is associated with.
         * @private
         * @type {number}
         */
        this._titlePadY = this.layout.relHeight(TITLE_PAD_Y);

        let fontFamily = 'Arial';
        let fontSizeH1 = Math.round(this.layout.relHeight(6) * this._textDownScaleFactor);
        let fontSizeH2 = Math.round(this.layout.relHeight(4.5) * this._textDownScaleFactor);
        let fontSizeParagraph = Math.round(this.layout.relHeight(3) * this._textDownScaleFactor);
        let fontSizeFooter = Math.round(this.layout.relHeight(3) * this._textDownScaleFactor);
        let fontFillTitle = this.config.gameMenuStyle.titleTextColour;
        let fontFillParagraph = this.config.gameMenuStyle.textColour;
        let fontFillFooter = this.config.gameMenuStyle.secondaryTextColour;
        let fontFillWinlineNumber = this.config.gameMenuStyle.winlineWinningColour;
        
        /**
         * Text Style to use for a H1 heading
         * @private
         * @type {PIXI.TextStyle}
         */
        this._textStyleH1 =
        {
            fontFamily,
            fontSize : fontSizeH1,
            fill : fontFillTitle,
            fontWeight : 'bold'
        };

        /**
         * Text Style to use for a H2 heading
         * @private
         * @type {PIXI.TextStyle}
         */
        this._textStyleH2 =
        {
            fontFamily,
            fontSize : fontSizeH2,
            fill : fontFillTitle,
            fontWeight : 'bold'
        };

        /**
         * @private
         */
        this._textStyleFooter =
        {
            fontFamily,
            fontSize : fontSizeFooter,
            fill : fontFillFooter,
            fontWeight : 'bold',
            wordWrap : true,
            wordWrapWidth : Math.round(this._itemTotalWidth * this._textDownScaleFactor)
        };

        /**
         * @private
         * @type {PIXI.TextStyle}
         */
        this._textStyleParagraph =
        {
            fontFamily,
            fontSize: fontSizeParagraph,
            fill : fontFillParagraph,
            wordWrap : true
        };

        /**
         * @private
         * @type {PIXI.TextStyle}
         */
        this._textStyleWinlineNumber =
        {
            fontFamily,
            fontSize: fontSizeParagraph,
            fill : fontFillWinlineNumber,
            wordWrap : true
        };

        /**
         * Text style used for the Symbp Prizes text.
         * @private
         */
        this._textStyleSymbolPrizes =
        {
            fontFamily,
            fontSize : fontSizeParagraph,
            fill : fontFillParagraph,
            wordWrap : true,
            wordWrapWidth : Math.round(this.layout.relWidth(SYM_PRIZE_WIDTH) * this._textDownScaleFactor)
        };

        /**
         * Text style used for the Special Symbols information text.
         * @private
         */
        this._textStyleSpecialSymbolsInfo =
        {
            fontFamily : fontFamily,
            fontSize : fontSizeParagraph,
            fill : fontFillParagraph,
            wordWrap : true,
            wordWrapWidth : Math.round(this.layout.relWidth(SPECIAL_SYM_TEXT_WIDTH) * this._textDownScaleFactor)
        };

        //------------------------------------------------------------------------------
        // Begin creating the actual content.
        //------------------------------------------------------------------------------
        let posY = 0;

        // Paytable is always first.
        if (rulesConfig.paytable) {
            this.log.debug('RulesRenderer: creating Paytable');

            let paytableConfig = rulesConfig.paytable;
            let paytableGraphic = this.createPaytable(paytableConfig);
            paytableGraphic.y = posY;
            this.addChild(paytableGraphic);
            posY += paytableGraphic.height + this._sectionPadY;
        }

       

        // Next, comes "GameFeatures" - an arbitrary list of text items describing the game.
        // There can be images, highlighted text, headers, etc. We basically describe any
        // important "game specific" info in this section.
        if (rulesConfig.gameFeatures) {
            this.log.debug('RulesRenderer: creating GameFeatures graphic');

            let gameFeaturesGraphic = this.createGameInfoItems(rulesConfig.gameFeatures);
            gameFeaturesGraphic.y = posY;
            this.addChild(gameFeaturesGraphic);
            posY += gameFeaturesGraphic.height + this._sectionPadY;
        }

        // We add a Winlines section, only if there are winlines defined on the
        // game's SlotConfig object.
        if (this._config.winlines) {
            this.log.debug('RulesRenderer: creating Winlines graphic');

            let winlinesGraphic = this.createWinlines();
            winlinesGraphic.y = posY;
            this.addChild(winlinesGraphic);
            posY += winlinesGraphic.height + this._sectionPadY;
        }

        //Add list of prize values. This is used to display the minimum and maximum prize values in the quick games
        if (rulesConfig.prizeValues)
        {
            this.log.debug('RulesRenderer: creating Paytable');

            let prizeValueDescription = this.createPrizeValueDescription(rulesConfig.prizeValues)
            prizeValueDescription.y = posY;

            this.addChild(prizeValueDescription);

            posY += prizeValueDescription.height + this._sectionPadY;
        }

        // Finally, add the Rtp Description section at the base of the rules.
        if (rulesConfig.rtpDescription) {
            let rtpDescription = this.createRtpDescription(rulesConfig.rtpDescription);
            rtpDescription.y = posY;
            this.addChild(rtpDescription);
        }

         
    };


    /**
     * @private
     * @param {PaytableConfig} paytable
     */
    createPaytable(paytable) {
        let container = new PIXI.Container();
        let posY = 0;

        let titleText = this._locale.getString("T_rules_SymbolsTitle");
        let titleField = new PIXI.Text(titleText, this._textStyleH1);
        titleField.x = this._itemStartX;
        titleField.y = 0;
        titleField.scale.set(this._textUpScaleFactor);
        posY += titleField.height + this._titlePadY;
        container.addChild(titleField);

        let totalBet = this._model.formatCurrency(this._model.getTotalBet());
        
        // TODO: This message generation currently relies on hard-coded conventions
        // about how currency is converted, which - will not be correct (and does not
        // adequately take into account how the currency conversion is done). Our
        // Currency Formats should be able to tell us this kind of thing. For now,
        // though, this will need to suffice,
        let sessionIsFreeplay = this._model.isSessionFreeplay();
        let pointsMessageLocalizationId = sessionIsFreeplay? "T_rules_PrizesInPointsFreeplay" : "T_rules_PrizesInPoints";
        let pointsMessageLocalizationSubs = {
            "[TOTAL_BET]":totalBet,
            "[NUM_POINTS_TO_CURRENCY_UNIT]":100,
            "[CURRENCY_UNIT]":this._model.formatCurrency(100)
        };

        let pointsMessageText = this._locale.getString(pointsMessageLocalizationId, pointsMessageLocalizationSubs);
        let pointsMessageField = new PIXI.Text(pointsMessageText, this._textStyleFooter);
        pointsMessageField.x = this._itemStartX;
        pointsMessageField.y = posY;
        pointsMessageField.scale.set(this._textUpScaleFactor);
        posY += pointsMessageField.height + this._titlePadY;
        container.addChild(pointsMessageField);

        // Special symbols each get their own row. Special Symbols are defined as "symbols which give
        // a win, which we might want to provide some text for". A wild symbol fits this category.
        paytable.specialSymbols.forEach(specialSymbol => {
            let specialSymbolGraphic = this.createSpecialSymbolGraphic(specialSymbol);
            specialSymbolGraphic.x = this._itemStartX;
            specialSymbolGraphic.y = posY;
            container.addChild(specialSymbolGraphic);
            posY += specialSymbolGraphic.height + this._itemPadY;
        });

        // For regular symbols, we will fit a certain number in a row.
        let numRegSymInCurrRow = 0;
        let currRegSymRowHeight = 0;
        let completedCurrentRow = false;
        paytable.regularSymbols.forEach(regularSymbol => {
            completedCurrentRow = false;

            let regularSymbolGraphic = this.createRegularSymbolGraphic(regularSymbol);
            regularSymbolGraphic.x = this._regularSymPosX[numRegSymInCurrRow];
            regularSymbolGraphic.y = posY;

            if (regularSymbolGraphic.height > currRegSymRowHeight) {
                currRegSymRowHeight = regularSymbolGraphic.height;
            }

            numRegSymInCurrRow += 1;
            if (numRegSymInCurrRow === NUM_REGULAR_SYM_INFO_PER_ROW) {
                posY += currRegSymRowHeight + this._itemPadY;

                numRegSymInCurrRow = 0;
                currRegSymRowHeight = 0;

                completedCurrentRow = true;
            }

            container.addChild(regularSymbolGraphic);
        });

        // If the last row of regular symbols wasn't finished, ensure that our y position
        // gets advanced, ready to start rendering any additional special symbols
        if (!completedCurrentRow) {
            posY += currRegSymRowHeight;
        }

        // TODO: Do we need any extra padding before the additional section ?
        posY += this._itemPadY;

        // We can also add some special symbols AFTER the main set of prize symbols
        paytable.additionalSpecialSymbols.forEach(specialSymbol => {
            let specialSymbolGraphic = this.createSpecialSymbolGraphic(specialSymbol);
            specialSymbolGraphic.x = this._itemStartX;
            specialSymbolGraphic.y = posY;
            container.addChild(specialSymbolGraphic);
            posY += specialSymbolGraphic.height + this._itemPadY;
        });

        return container;
    };


    /**
     * Creates the graphic for a specific symbol, based on its symbol id.
     * @private
     * @param {number} symbolId
     * @return {PIXI.DisplayObject}
     */
    getSymbolGraphic(symbolId) {
        let symbolTextureId = `sym${symbolId}`;
        let symbolGraphic = this.assets.getSprite(symbolTextureId);
        return symbolGraphic;
    };


    /**
     * Generates the prize data to render for a given symbol. The prizes shown are in points,
     * and take current bet settings into account. There are multiple ways we can have prizes
     * in WMG Online games, and there is no backend service available to fetch prize data. So,
     * we need to do a little pre-parsing in order to create the appropriate prize data.
     * @private
     * @param {RegularSymbol} symbol
     * @return { Object.<number,number> }
     */
    getPrizeMapFor(symbol) {
        /** @type {[id:number] : number} */
        let prizeMap = {};
        let numSymbolValues = Object.keys(symbol.prizeInfo);
        if (symbol.prizeMode === "linearBetPerLine") {
            let betPerLine = this._model.getBetPerLine();
            numSymbolValues.forEach(numSym => {
                prizeMap[numSym] = symbol.prizeInfo[numSym] * betPerLine;
            });
        }
        else
        if (symbol.prizeMode === "linearTotalBet") {
            let totalBet = this._model.getTotalBet();
            numSymbolValues.forEach(numSym => {
                prizeMap[numSym] = symbol.prizeInfo[numSym] * totalBet;
            });
        }
        else
        if (symbol.prizeMode === "nonLinearBetPerHand") {
            let betPerHand = this._model.getBetPerHand();
            numSymbolValues.forEach(numSym => {
                prizeMap[numSym] = symbol.prizeInfo[numSym][betPerHand];
            });
        }

        return prizeMap;
    };


    /**
     * Generates the prize string that should be shown for a Regular Symbol.
     * @private
     * @param {RegularSymbol} symbol
     * @return {string}
     */
    getPrizeInfoStringFor(symbol) {
        let prizeString = '';
        let prizeMap = this.getPrizeMapFor(symbol);

        let orderedNumSym = Object.keys(prizeMap).sort((a,b) => parseInt(b) - parseInt(a));
        let numPrizes = orderedNumSym.length;

        orderedNumSym.forEach((numSym, numSymIndex) => {
            let prize = prizeMap[numSym];
            let isLastPrize = numSymIndex + 1 === numPrizes;
            prizeString += `x${numSym} = ${prize}`;

            if (!isLastPrize) {
                prizeString += "\n";
            }
        });

        return prizeString;
    };


    /**
     * @private
     * @param {SpecialSymbol} symbolConfig
     * @return {PIXI.DisplayObject}
     */
    createSpecialSymbolGraphic(symbolConfig) {
        if (symbolConfig.type === "SpecialSymbolWithWin") {
            return this.createSpecialSymbolWithWinGraphic(symbolConfig);
        }
        else
        if (symbolConfig.type === "SpecialSymbolWithNoWin") {
            return this.createSpecialSymbolWithNoWinGraphic(symbolConfig);
        }
    };


    /**
     * @private
     * @param {SpecialSymbolWithWin} symbolConfig
     * @return {PIXI.DisplayObject}
     */
    createSpecialSymbolWithWinGraphic(symbolConfig) {
        let container = new PIXI.Container();
        let symbolImage = this.getSymbolGraphic(symbolConfig.symbolId);
        let prizeString = this.getPrizeInfoStringFor(symbolConfig); //"x5 = 8.00\nx4 = 4.00\nx3 = 2.00";
        let prizeField = new PIXI.Text(prizeString, this._textStyleSymbolPrizes);
        let infoString = this.getTextStringFor(symbolConfig);
        let infoField = new PIXI.Text(infoString, this._textStyleSpecialSymbolsInfo);

        this.layout.propWidthScale(symbolImage, SYM_WIDTH);

        symbolImage.x = this.layout.relWidth(SYM_OFFSET_X);
        
        prizeField.x = this.layout.relWidth(SYM_PRIZE_OFFSET_X);
        prizeField.scale.set(this._textUpScaleFactor);

        infoField.x = this.layout.relWidth(SPECIAL_SYM_TEXT_OFFSET_X);
        infoField.scale.set(this._textUpScaleFactor);

        // We aim to have the 2 pieces of text (prize and info) at the same vertical position
        // (as this looks neater). If both pieces of text are shorter than the symbol image,
        // then we vertically center them relative to the image. If either is taller, we
        // anchor them both to the same vertical position as the image.
        if (prizeField.height < symbolImage.height &&
            infoField.height < symbolImage.height)
        {
            prizeField.y = symbolImage.y + ((symbolImage.height - prizeField.height) * 0.5);
            infoField.y = symbolImage.y + ((symbolImage.height - infoField.height) * 0.5);
        }
        else
        {
            prizeField.y = symbolImage.y;
            infoField.y = symbolImage.y;
        }

        container.addChild(symbolImage);
        container.addChild(prizeField);
        container.addChild(infoField);

        return container;
    };


    /**
     * @private
     * @param {SpecialSymbolNoWin} symbolConfig 
     * @return {PIXI.DisplayObject}
     */
    createSpecialSymbolWithNoWinGraphic(symbolConfig) {
        let container = new PIXI.Container();
        let symbolImage = this.getSymbolGraphic(symbolConfig.symbolId);
        let infoString = this.getTextStringFor(symbolConfig);
        let infoField = new PIXI.Text(infoString, this._textStyleSpecialSymbolsInfo);

        this.layout.propWidthScale(symbolImage, SYM_WIDTH);

        symbolImage.x = this.layout.relWidth(SYM_OFFSET_X);

        infoField.x = this.layout.relWidth(SYM_PRIZE_OFFSET_X);
        infoField.scale.set(this._textUpScaleFactor);

        if (infoField.height < symbolImage.height) {
            infoField.y = symbolImage.y + ((symbolImage.height - infoField.height) * 0.5);
        }
        else infoField.y = symbolImage.y;

        container.addChild(symbolImage);
        container.addChild(infoField);

        return container;
    };


    /**
     * @private
     * @param {RegularSymbol} symbolConfig
     * @return {PIXI.DisplayObject}
     */
    createRegularSymbolGraphic(symbolConfig) {
        let container = new PIXI.Container();
        let symbolImage = this.getSymbolGraphic(symbolConfig.symbolId);
        let prizeString = this.getPrizeInfoStringFor(symbolConfig); //"x5 = 8.00\nx4 = 4.00\nx3 = 2.00";
        let prizeField = new PIXI.Text(prizeString, this._textStyleSymbolPrizes);
        this.layout.propWidthScale(symbolImage, SYM_WIDTH);

        symbolImage.x = this.layout.relWidth(SYM_OFFSET_X);

        prizeField.x = this.layout.relWidth(SYM_PRIZE_OFFSET_X);
        prizeField.scale.set(this._textUpScaleFactor);

        // if prize is taller than the symbol graphic, anchor it to the top.
        // Otherwise, position it vertically in the center of the text.
        if (prizeField.height > symbolImage.height) {
            prizeField.y = symbolImage.y;
        }
        else
        {
            prizeField.y = symbolImage.y + ((symbolImage.height - prizeField.height) * 0.5);
        }

        container.addChild(symbolImage);
        container.addChild(prizeField);

        return container;
    };


    /**
     * Creates a graphic object containing all winlines information, rendered in visual form.
     * The winlines are intepreted from the data present on Slot Config.
     * @private
     * @return {PIXI.DisplayObject}
     */
    createWinlines() {
        /** @type {SlotConfig} */
        let slotConfig = this._config;
        let winlineIds = Object.keys(slotConfig.winlines);
        let maxNumVerticalSym = Math.max(...slotConfig.numSymsPerReel);
        let container = new PIXI.Container();
        let winlinesGraphic = new PIXI.Graphics();
        let numReels = slotConfig.numReels;
        let winlineTotalWidth = this.layout.relWidth(WINLINE_WIDTH);
        let winlineNumberWidth = this.layout.relWidth(WINLINE_NUMBER_WIDTH);
        let symbolPad = 3; // eg: 3 pixels
        let symbolWidth = Math.floor((winlineTotalWidth - winlineNumberWidth - ((numReels - 1) * symbolPad)) / numReels);
        let symbolHeight = symbolWidth;
        let winlineTotalHeight = (maxNumVerticalSym * symbolHeight) + ((maxNumVerticalSym - 1) * symbolPad);
        let winlinePadX = this.layout.relWidth(WINLINE_PAD_X);
        let winlinePadY = this.layout.relHeight(WINLINE_PAD_Y);
        let titleText = new PIXI.Text(this.locale.getString('T_rules_WinlinesTitle'), this._textStyleH1);
        let winlinesStartY = (titleText.height * this._textUpScaleFactor) + this._titlePadY;
        let winlineSymColourWinning = this.config.gameMenuStyle.winlineWinningColour;
        let winlineSymColourStandard = this.config.gameMenuStyle.winlineStandardColour;

        titleText.x = this._itemStartX;
        titleText.scale.set(this._textUpScaleFactor);

        container.addChild(titleText);
        container.addChild(winlinesGraphic);

        // Now, let's create the actual winline graphics
        let numWinlinesInCurrRow = 0;
        let numWinlineRowsCreated = 0;
        winlineIds.forEach(winlineId => {
            /** @type {WinlineConfig} */
            let winline = slotConfig.winlines[winlineId];
            let winlineStartX = this._itemStartX + winlineNumberWidth + ((winlineTotalWidth + winlinePadX) * numWinlinesInCurrRow);
            let winlineStartY = winlinesStartY + (winlineTotalHeight + winlinePadY) * numWinlineRowsCreated;
            let winlineNumberX = winlineStartX - (3 * symbolPad); // we right align the text, so its fine to add a little arbitrary padding
            let winlineNumberY = winlineStartY;

            let winlineNumber = new PIXI.Text(winlineId, this._textStyleWinlineNumber);
            winlineNumber.x = winlineNumberX;
            winlineNumber.y = winlineNumberY;
            winlineNumber.anchor.set(1,0);
            winlineNumber.scale.set(this._textUpScaleFactor);
            container.addChild(winlineNumber);

            for (let reelIndex = 0; reelIndex < numReels; reelIndex ++) {
                let numSymInReel = slotConfig.numSymsPerReel[reelIndex];
                let winningSymForReel = winline.positions[reelIndex];

                for (let symIndex = 0; symIndex < numSymInReel; symIndex ++) {
                    let symIsWinning = symIndex === winningSymForReel;
                    let symColour = symIsWinning ? winlineSymColourWinning : winlineSymColourStandard;
                    let symbolPosX = winlineStartX + (reelIndex * (symbolWidth + symbolPad));
                    let symbolPosY = winlineStartY + (symIndex * (symbolHeight + symbolPad));

                    winlinesGraphic.beginFill(symColour);
                    winlinesGraphic.drawRect(symbolPosX, symbolPosY, symbolWidth, symbolHeight);
                    winlinesGraphic.endFill();
                }
            }

            numWinlinesInCurrRow += 1;
            if (numWinlinesInCurrRow === NUM_WINLINES_PER_ROW) {
                numWinlinesInCurrRow = 0;
                numWinlineRowsCreated += 1;
            }
        });

        return container;
    };


    /**
     * Takes an arbitrary section of game info items, and generates a Display Object containing
     * them all.
     * @private
     * @param {GameInfoItem[]} gameInfoItems
     * @return {PIXI.DisplayObject}
     */
    createGameInfoItems(gameInfoItems) {
        this.log.debug('RulesRendered.createGameInfoItems()');

        let container = new PIXI.Container();
        let posY = 0;

        /**
         * Adds a header into the Game Info Items container.
         * @param {GameInfoHeader} headerConfig
         */
        let addHeader = headerConfig =>
        {
            let textStyle = headerConfig.font === "h2"? this._textStyleH2 : this._textStyleH1;
            let textString = this.getTextStringFor(headerConfig);
            let textField = new PIXI.Text(textString, textStyle);
            textField.x = this._itemStartX;
            textField.y = posY;
            textField.scale.set(this._textUpScaleFactor);
            container.addChild(textField);
            posY += textField.height + this._itemPadY;
        };

        /**
         * Adds a paragraph into the Game Info Items container.
         * @param {GameInfoParagraph} paragraphConfig
         */
        let addParagraph = paragraphConfig =>
        {
            let textString = this.getTextStringFor(paragraphConfig);
            let paragraphStartX = this._itemStartX;
            let paragraphEndX = this._itemEndX;
            let paragraphWidth = this._itemTotalWidth;

            if (paragraphConfig.imageTextureId)
            {
                let useMaxImageWidth = paragraphConfig.imageRelWidth > 0;
                let useMaxImageHeight = paragraphConfig.imageRelHeight > 0;

                let imagePadX = this.layout.relWidth(PARAGRAPH_IMAGE_PAD_X);
                
                let imageOnRight = paragraphConfig.imageOnRight || false;
                let image = this.assets.getSprite(paragraphConfig.imageTextureId);

                if (useMaxImageWidth && useMaxImageHeight)
                {
                    let maxWidth = this.layout.relWidth(paragraphConfig.imageRelWidth);
                    let maxHeight = this.layout.relHeight(paragraphConfig.imageRelHeight);
                    this.layout.setHeightOrWidthToMax(image, maxWidth, maxHeight);
                }
                else
                {
                    let imageIsLarge = paragraphConfig.imageIsLarge || false;
                    let imageRelWidth = imageIsLarge? PARAGRAPH_IMAGE_WIDTH_LARGE : PARAGRAPH_IMAGE_WIDTH_SMALL;
                    this.layout.propWidthScale(image, imageRelWidth);
                }

                let textWidth = Math.round((paragraphWidth - image.width - imagePadX) * this._textDownScaleFactor);
                let textAlign = imageOnRight? 'right' : 'left';
                let textStyle = Object.assign({align:textAlign, wordWrapWidth:textWidth}, this._textStyleParagraph);
                let textField = new PIXI.Text(textString, textStyle);
                textField.scale.set(this._textUpScaleFactor);

                if (imageOnRight)
                {
                    image.x = paragraphEndX - image.width;
                    textField.x = image.x - imagePadX;
                    textField.anchor.set(1, 0);
                }
                else
                {
                    image.x = paragraphStartX;
                    textField.x = paragraphStartX + imagePadX + image.width;
                }

                let textIsTaller = textField.height > image.height;
                if (textIsTaller)
                {
                    textField.y = posY;
                    image.y = posY + ((textField.height - image.height) * 0.5);
                    posY += textField.height + this._itemPadY;
                }
                else
                {
                    image.y = posY;
                    textField.y = posY + ((image.height - textField.height) * 0.5);
                    posY += image.height + this._itemPadY;
                }

                container.addChild(image);
                container.addChild(textField);
            }
            else
            {
                let textWidth = paragraphWidth * this._textDownScaleFactor;
                let textStyle = Object.assign({wordWrapWidth:textWidth}, this._textStyleParagraph);
                let textField = new PIXI.Text(textString, textStyle);
                textField.x = paragraphStartX;
                textField.y = posY;
                textField.scale.set(this._textUpScaleFactor);
                posY += textField.height + this._itemPadY;

                container.addChild(textField);
            }

            // TODO: Add support for the "highlight" visual state for a paragraph
        };

        /**
         * Adds a footer into the Game Info Items container.
         * @param {GameInfoFooter} footerConfig
         */
        let addFooter = footerConfig =>
        {
            let textStyle = this._textStyleFooter;
            let textString = this.getTextStringFor(footerConfig);
            let textField = new PIXI.Text(textString, textStyle);
            textField.x = this._itemStartX;
            textField.y = posY;
            textField.scale.set(this._textUpScaleFactor);
            container.addChild(textField);
            posY += textField.height + this._itemPadY;
        };

        gameInfoItems.forEach(gameInfoItem => {
            if (gameInfoItem.type === "header") {
                addHeader(gameInfoItem);
            }
            else
            if (gameInfoItem.type === "paragraph") {
                addParagraph(gameInfoItem);
            }
            else
            if (gameInfoItem.type === "footer") {
                addFooter(gameInfoItem);
            }
        });

        return container;
    };


    /**
     * Creates an Rtp description. Behind the scenes, this takes advantate of the "Game Info Items" data.
     * The actual text description comes from that provided for each licensee.
     * @private
     * @param {RtpDescription} rtpDescriptionConfig
     * @return
     */
    createRtpDescription(rtpDescriptionConfig) {
        /** @type {GameInfoItem[]} */
        let gameInfoItems = [{
            type : "header",
            text : this._locale.getString("T_rules_RtpDescriptionTitle")
        },
        {
            type : "paragraph",
            text : this._locale.getString("T_rules_RtpDescriptionBody")
        }];

        return this.createGameInfoItems(gameInfoItems);
    };

    /**
     * @private
     *  @param {PrizeValues} prizeValueConfig
     * @return {PIXI.DisplayObject}
     */
    createPrizeValueDescription(prizeValueConfig)
    {
        let totalBet = this._model.getTotalBet();

        let prizeValues = prizeValueConfig[totalBet].sort((a,b)=>a-b);

        let sessionIsFreeplay = this._model.isSessionFreeplay();

        let pointsMessageLocalizationId = sessionIsFreeplay ? "T_rules_PrizeValuesDescriptionBodyFreeplay" : "T_rules_PrizeValuesDescriptionBody";



       
        let pointsMessageLocalizationSubs = {
            "[TOTAL_BET]":this._model.formatCurrency(this._model.getTotalBet()),
            "[NUM_POINTS_TO_CURRENCY_UNIT]":100,
            "[CURRENCY_UNIT]":this._model.formatCurrency(100),
            "[MINIMUM_PRIZE_VALUE_AWARDED]":Math.min(...prizeValues),
            "[MAXIMUM_PRIZE_VALUE_AWARDED]":Math.max(...prizeValues)
        };

        let prizeValueString = this._locale.getString("T_rules_PrizeValueList", pointsMessageLocalizationSubs);

        let lastValueIndex = prizeValues.length - 1;

        for (let valueIndex = 0; valueIndex < prizeValues.length; valueIndex++)
        {
            prizeValueString += `${prizeValues[valueIndex]}`;
            
            if (valueIndex !== lastValueIndex)
            {
                prizeValueString += ', '
            }
            else
            {
                prizeValueString += '.';
            }
             
       }
       


         /** @type {GameInfoItem[]} */
         let gameInfoItems = [{
            type : "header",
            text : this._locale.getString("T_rules_PrizeValuesDescriptionTitle")
        },
        {
            type : "paragraph",
            text : this._locale.getString(pointsMessageLocalizationId, pointsMessageLocalizationSubs)
        },
        {
            type : "paragraph",
            text : prizeValueString
        }];

        return this.createGameInfoItems(gameInfoItems);
    }





    /**
     * Fetches the text string to use for any config object within the Rules view.
     * @private
     * @param {RulesText} rulesText
     * @return {string}
     */
    getTextStringFor(rulesText) {
        if (rulesText.text) {
            return rulesText.text;
        }
        else
        if (rulesText.textLocalizationId) {
            return this.locale.getString(rulesText.textLocalizationId);
        }
        else return "";
    };
};