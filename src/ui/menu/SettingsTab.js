//--------------------------------------------------------------------------------------------------
// Setting Tab (for menu)
//--------------------------------------------------------------------------------------------------
// 
//--------------------------------------------------------------------------------------------------

// @ts-check

import BaseView from "../../views/BaseView";
import MenuTab from "./MenuTab";
import C_PointerEvt from "../../const/C_PointerEvt";
import C_GameEvent from "../../../../cms-core-logic/src/const/C_GameEvent";

//--------------------------------------------------------------------------------------------------
// Static layout constants
//--------------------------------------------------------------------------------------------------
const SETTING_CONTENT_WIDTH = 800;
const SETTING_CONTENT_HEIGHT = 120;
const BUTTON_POS_X = 400;
const TITLE_POS_X = 20;
const VALUE_POS_X = 440;

//--------------------------------------------------------------------------------------------------
// Setting Graphical classes
//--------------------------------------------------------------------------------------------------
/**
 * Setting Tab class
 */
export class SettingsTab extends MenuTab
{
    /**
     * Constructs a new settings tab,
     * @param {number} tabWidth
     * @param {number} tabHeight
     */
    constructor(tabWidth, tabHeight)
    {
        super(tabWidth, tabHeight);

        // HACK ALERT
        // There appear to be issues in pixi 4.8 regarding the reporting of width by containers.
        // If you create an element 800 wide, set its x position to 400, and add it to a container,
        // i expect the container.width field to report 1200, but it seems to report 800. However,
        // the component is still positioned how you expect. I haven't had time to fully debug this,
        // or check if we can safely upgrade pixi versions in order to remove this bug. This is
        // causing layout issues in the menu tab system (as we attempt to layout the content in its
        // parent container, using width as a guideline to whether to scale it or re-position it...
        // but the width info is plain wrong)
        // The short term solution, for a menu tab, is to add some fixed width content (an invisible
        // bg) that resolves this.
        // Russell Wakely, 23.03.2020
        let bg = new PIXI.Graphics();
        bg.beginFill(0xFFFF00, 0);
        bg.drawRect(0, 0, tabWidth, tabHeight);
        bg.endFill();
        this.addChild(bg);

        // Listen out for any events indicating that some settings have changed
        this.dispatcher.addListener(C_GameEvent.SOUND_TOGGLED, this.onSoundToggled, this);

        // By default, we add the sound toggle.
        let soundIsEnabled = this.sound.getSoundEnabled();

        /**
         * The Toggle Control used to change sound status.
         * @private
         * @type {ToggleControl}
         */
        this._soundSetting = this.createOnOffToggleSetting("T_menu_SoundSetting", soundIsEnabled, isEnabled => {
            this.log.debug(`Settings.onSoundToggled(isEnabled:${isEnabled})`);
            this.sound.toggleSound();
            this.dispatcher.emit(C_GameEvent.BUTTON_TOGGLE_SOUND_PRESSED);
        });

        this.addChild(this._soundSetting);
    };


    /**
     * @private
     */
    onSoundToggled()
    {
        let soundIsEnabled = this.sound.getSoundEnabled();

        this.log.debug(`SettingsTab.onSoundToggled: set new state of sound control to ${soundIsEnabled}`);

        this._soundSetting.setToggleControlState(soundIsEnabled);
    };


    /**
     * @public
     * @override
     * @inheritdoc
     */
    destroy()
    {
        this.log.debug(`SettingsTab.destroy()`);

        this.dispatcher.removeListener(C_GameEvent.SOUND_TOGGLED, this.onSoundToggled, this);

        super.destroy();
    };
}