/**
 * Basic Api for a Toggle Control in the menu (toggles a single boolean setting on / off).
 */
interface ToggleControl extends PIXI.DisplayObject
{
    /**
     * Sets the new visual state of the Toggle Control. The control is expeced to kill any
     * visual transitions that may already be taking place, and should show the new state
     * of the toggle either immediately, or very quickly.
     * @param newState 
     * The new enabled / disabled state of the setting represented by this toggle control.
     */
    setToggleControlState(newState : boolean) : void;
}