//--------------------------------------------------------------------------------------------------
// Paytable Tab
//--------------------------------------------------------------------------------------------------
// The paytable tab component for the menu.
// TODO:
// Add some better header documentation here.....
//--------------------------------------------------------------------------------------------------
import BaseView from "../../views/BaseView";

export class PaytableTab extends BaseView
{
    /**
     * Creates a new rules view.
     * @param {{width:number,height:number}} dimensions
     */
    constructor(dimensions)
    {
        super();

        this.name = "PaytableTabContent";

        /**
         * The dimensions (target area) that the PaytableTab is shown in.
         * Note, that the paytable tab CAN definitely exceed these (for height)
         * as it will be placed inside a Scroller.
         */
        this.dimensions = dimensions;

        /**
         * Graphical part of the paytable (common to all languages).
         * @private
         */
        this.paytableGraphic = this.assets.getSprite("PayTable_WINLINES");
        this.paytableGraphic.name = "Paytable.PaytableGraphic";
        this.layout.setWidth(this.paytableGraphic, dimensions.width);

        this.paytableReels = this.assets.getSprite("PayTable_REELS");
        this.layout.propWidthScale(this.paytableReels, 100, this.paytableGraphic);
        this.paytableReels.y = this.paytableGraphic.y + this.paytableGraphic.height;

        this.paytableBonus = this.assets.getSprite("PayTable_BONUS");
        this.layout.propWidthScale(this.paytableBonus, 100, this.paytableGraphic);
        this.paytableBonus.y = this.paytableReels.y + this.paytableReels.height;

        /**
         * Footer text of the paytable (provides some language specific information).
         * @private
         */
        this.paytableText = this.assets.getSprite(getTextTextureId());
        this.paytableText.name = "Paytable.Text";
        this.layout.propWidthScale(this.paytableText, 100, this.paytableGraphic);
        this.paytableText.y = this.paytableBonus.y + this.paytableBonus.height;



        // this.updateLayout();

        this.addChild(this.paytableGraphic);
        this.addChild(this.paytableReels);
        this.addChild(this.paytableBonus);

        this.addChild(this.paytableText);
    };
    
    /**
     * Updates all Paytable localizations.
     * @public
     */
    updateLocalizations()
    {
        this.paytableText.frameName = getTextTextureId();
        this.updateLayout();
    };

    /**
     * Lays out the content of the Paytable Tab.
     * @private
     */
    updateLayout()
    {
        // Check if content exceeds dimensions height (ie: if scrolling would be required).
        // If NOT, then we can center the content vertically, within the height specified
        // in "dimensions". This is bit of a hack, because it relies on us remembering how
        // this PaytableTab graphic is used (ie: it is added to a Scroller instance). If
        // the total height of PaytableTab content is larger or equal to dimensions.height
        // then we lay the content out, starting at y = 0: the content will exceed dimensions
        // height, and the Scroller will go in to "scrolling" mode.
        let totalHeight = this.paytableGraphic.height + this.paytableText.height;

        if (totalHeight < this.dimensions.height)
        {
            let difference = this.dimensions.height - totalHeight;
            this.paytableGraphic.y = difference * 0.5;
        }
        else
        {
            this.paytableGraphic.y = 0;
        }

        this.paytableText.y = this.paytableGraphic.y + this.paytableGraphic.height;
    };
};

/**
 * Returns the sub-texture id (framename) for the paytable's footer text.
 * Takes into account current localization.
 * @return {string}
 */
function getTextTextureId()
{
    return `PayTable_PointsMessage_english`;
};