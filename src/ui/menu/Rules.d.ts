
/**
 * Master configuration for game rules. There is a certain order that things are drawn in:
 * - Paytable
 * - Winlines comes next (RulesConfig cannot determine whether or how to render the Winlines
 *   section: if a Slot Game has a set of winlines configured in its SlotConfig, these are
 *   used to draw the Winlines section. If the game's SlotConfig has no Winlines object
 *   defined, then the Winline section will be absent).
 */
interface RulesConfig
{
    /**
     * Config for the paytable section of the rules.
     */
    paytable ? : PaytableConfig;

    /**
     * List of Game Info Items. These are entirely arbitrary things.
     */
    gameFeatures ? : GameInfoItem[];

    /**
     * Configuration data for the Rtp description at the base of the Rules.
     */
    rtpDescription ? : RtpDescription;

    /**
     * Config for the list of possible prize values. This is used to display the minimum and maximum prize value in the bonus game.
     */
    prizeValues ? : PrizeValues;
};

/**
 * Describes a single piece of rules text.
 */
interface RulesText
{
    /**
     * Localization id that should be used for the text.
     */
    textLocalizationId ? : string;

    /**
     * Text string that should be used directly.
     */
    text ? : string;
};

type GameInfoItem = GameInfoHeader | GameInfoParagraph | GameInfoFooter;

/**
 * Specifies a header to use
 */
interface GameInfoHeader extends RulesText
{
    /**
     * @inheritDoc
     */
    type : "header";

    /**
     * The font weight that should be used: is it a big header (h1), or a little one (h2)?
     * If this property is not specified, we will default to using h1.
     */
    font ? : "h1" | "h2";
};

/**
 * Specifies the rendering of a single paragraph of Game Info text. A text item is rendered
 * with P format. An optional image can be included, which can be positioned on the left or
 * the right of the paragraph. The image can have one of 3 default scales applied, which
 * specifies the proportion of screen width that the image will be formatted to (its height
 * is determined by the actual proportions of the image itself).
 */
interface GameInfoParagraph extends RulesText
{
    /**
     * @inheritDoc
     */
    type : "paragraph";

    /**
     * Texture Id for any image which should be added to the Text Item. If this string is
     * absent, then no image should be added.
     */
    imageTextureId ? : string;

    /**
     * Indicates if any image should be on the right of the Text Block. If this field is
     * absent, the image should be rendered on the left, which is the default.
     */
    imageOnRight ? : boolean;

    /**
     * Indicates the scale of any image. Large image will occupy a larger percentage of screen
     * width (typically about 40%, compared to 20% for small image). This field is best used for large
     * "screenshot" type images in the rules: by setting the flag to true, we indicate this is a large
     * screenshot image (by default, it's treated as a small "screenshot image"). This field is only
     * used if the imageWidth and imageHeight fields are not set (if those fields have been set, then
     * they override any other sizing flags)
     */
    imageIsLarge ? : boolean;

    /**
     * Manually specifies the target width of the image, as a percent of screen width: values are written
     * in the range 0 to 100 (where a field value of 100 would indicate that the image should occupy 100%
     * of screen width). This field will only be used if imageRelHeight is also specified. The value serves
     * as a maximumWidth (the rules screen will attempt to target exactly this width, but the image may end
     * up smaller in cases where imageRelHeight is the enforced height).
     * 
     * imageRelWidth and imageRelHeight are best used when we need to include small icon images in the rules:
     * it also works best when the source images are the same size (this will ensure that we see all of the
     * images formatted in a uniform way).
     * 
     * If both imageRelWidth and imageRelHeight are specified, then the value of "imageIsLarge" will be ignored.
     */
    imageRelWidth ? : number;

    /**
     * Manually specifies the target height of the image, as a percent of screen height: values are written
     * in the range 0 to 100 (where a field value of 100 would indicate that the image should occupy 100%
     * of screen height). This field will only be used if imageRelWidth is also specified. The value serves
     * as a maximumHeight (the rules screen will attempt to target exactly this height, but the image may end
     * up smaller in cases where imageRelWidth is the enforced width).
     * 
     * imageRelWidth and imageRelHeight are best used when we need to include small icon images in the rules:
     * it also works best when the source images are the same size (this will ensure that we see all of the
     * images formatted in a uniform way).
     * 
     * If both imageRelWidth and imageRelHeight are specified, then the value of "imageIsLarge" will be ignored.
     */
    imageRelHeight ? : number;

    /**
     * Indicates if this should be a highlighted text item.
     */
    highlight ? : boolean;
};

/**
 * Specifies a footer in a block of Game Info Text. This only has text, and will be rendered in
 * a special small footer font, with alternate colour scheme.
 */
interface GameInfoFooter extends RulesText
{
    /**
     * @inheritDoc
     */
    type : "footer";
}


/**
 * Configuration for the Paytable section of the rules.
 */
interface PaytableConfig
{
    /**
     * Ordered list of Special Symbols to render. These are always shown at the top of the paytable
     * section. There is no strict requirement for there to be any Special Symbols described for a
     * game.
     */
    specialSymbols : SpecialSymbol[];

    /**
     * Ordered list of Regular Symbols to render. These are always shown after any Special Symbols.
     * There is no strict requirement for there to be any Regular Symbols described for a game.
     */
    regularSymbols : RegularSymbol[];

    /**
     * Ordered list of additional Special Symbols, which are added after the list of regular
     * symbols.
     */
    additionalSpecialSymbols : SpecialSymbol[];
};

interface SymbolInfo
{
    /**
     * Numerical id of the symbol.
     */
    symbolId : number;

    /**
     * Debug name for the symbol.
     */
    symbolName ? : string;
}

/**
 * Types of supported symbol prize modes.
 * - "linearBetPerLine" - in this mode, the basic prize provided for a symbol, is treated as
 *   a value to be multiplied by the number of active winlines. So, if prize for symbol X is N,
 *   N will be the prize shown for 1 line active, 5N will be the prize shown for 5 lines active,
 *   etc.
 */
type SymbolPrizeMode = "linearBetPerLine" | "linearTotalBet" | "nonLinearBetPerHand";

/**
 * Defines prizes for a symbol, where we can multiply a prize value (for a given number of winning
 * symbols) by the currently active value of BetPerLine, to obtain the required prize.
 */
interface SymbolWithLinearBetPerLinePrizeModel extends SymbolInfo
{
    prizeMode : "linearBetPerLine";

    /**
     * Prize value (for this symbol), for each number of winning symbols: to be multiplied by the
     * number of active winlines.
     */
    prizeInfo : { [id:number] : number };
}

/**
 * Defines prizes for a symbol, where we can multiply a prize value (for a given number of winning
 * symbols) by the currently active value of TotalBet, to obtain the required prize.
 */
interface SymbolWithLinearTotalBetPrizeModel extends SymbolInfo
{
    prizeMode : "linearTotalBet";

    /**
     * Prize value (for this symbol), for each number of winning symbols: to be multiplied by the
     * active value of total bet.
     */
    prizeInfo : { [id:number] : number };
}

/**
 * Defines prizes for a symbol, where we must look up the prize according to current value of Bet
 * per Hand.
 */
interface SymbolWithNonLinearBetPerHandPrizeModel extends SymbolInfo
{
    prizeMode : "nonLinearBetPerHand";

    /**
     * Prize value, organized first by Num Symbol, and then by Bet per Hand.
     * To obtain the correct value f or a certain numSym, look up prizeInfo[numSym][betPerHand]
     */
    prizeInfo :
    {
        [id:number] :
        {
            [id:number] : number
        }
    }
}

/**Defines the list of possible prize values based on the players total bet amount */
interface PrizeValues {
    [id:number] : [number]
}

/**
 * Description of a Regular symbol (image only, as well as prizes). A "Regular Symbol" means
 * anything that is non-scatter, and awards some kind of standard win (on winlines or via another
 * mechanic). A regular symbol will award some kind of prize (credit or superbet), and we want to
 * inform the player of the prize relative to the number of winning symbols.
 */
type RegularSymbol =
    SymbolWithLinearBetPerLinePrizeModel |
    SymbolWithLinearTotalBetPrizeModel |
    SymbolWithNonLinearBetPerHandPrizeModel;

type SpecialSymbol = SpecialSymbolWithWin | SpecialSymbolNoWin;

/**
 * Description of a Special Symbol with a win: has an image, prizes, and also has some customizable
 * text, to describe what is actually going on with the symbol.
 */
interface SpecialSymbolWithWin extends RulesText, RegularSymbol
{
    type : "SpecialSymbolWithWin"
}

/**
 * Description of a Special SYmbol without a win: has an image, and customizable text.
 */
interface SpecialSymbolNoWin extends RulesText, SymbolInfo
{
    type : "SpecialSymbolWithNoWin"
}

/**
 * Configuration for the Rtp Description.
 */
interface RtpDescription
{
    /**
     * Numerical value that will be displayed in any Rtp Actual field in the Rtp description test.
     * This should be a number, eg: written as 90.50. The number will be formatted appropriately
     * when rendered into the actual game.
     */
    rtpActual : number;

    /**
     * Numerical value that will be displayed in any Rtp Minimum field in the Rtp description text.
     * This should be a number, eg: written as 90.50. The number will be formatted appropriately
     * when rendered into the actual game.
     */
    rtpMinimum : number;

    /**
     * Numerical value that will be displayed in any Rtp Maximum field in the Rtp description text.
     * This should be a number, eg: written as 90.50. The number will be formatted appropriately
     * when rendered into the actual game.
     */
    rtpMaximum : number;
}