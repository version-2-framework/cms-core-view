//--------------------------------------------------------------------------------------------------
// Rules Tab
//--------------------------------------------------------------------------------------------------
// The rules tab component for the menu, is VERY simple.
// It is basically just a phaser image. It can accept the
// available dimensions, and it will match its width to
// the target width.
//--------------------------------------------------------------------------------------------------

import BaseView from "../../views/BaseView";
import {RulesRenderer} from "./RulesRenderer";

/**
 * Name of a default rules config json file. This is what we fall back to for all cases (desktop,
 * mobile, tablet) in the event that no mobile or tablet option is specified.
 */
const DEFAULT_JSON_CONFIG_ID = "rulesConfig";

/**
 * Name of a tablet specific config file. If this config file has been loaded, and the game is
 * running on a tablet device, this config will be used.
 */
const TABLET_JSON_CONFIG_ID = "rulesConfig.tablet";

/**
 * Name of a mobile specific config file. If this config file is loaded, and the game is running
 * on a mobile device, this config will be used. If the game is running on a tablet device - and
 * a tablet specific config is not supplied - then this config will also be used.
 */
const MOBILE_JSON_CONFIG_ID = "rulesConfig.mobile";


export class RulesTab extends BaseView
{
    /**
     * Creates a new rules view.
     * @param {number} tabWidth
     * @param {number} tabHeight
     */
    constructor(tabWidth, tabHeight)
    {
        super();

        let paytable = new RulesRenderer(this.getRulesConfig(), tabWidth);
        paytable.name = "RulesTabContent";

        // The tab will size itself to the targeted width.
        
        this.addChild(paytable);
    };


    /**
     * Returns the most appropriate config object for the rules renderer.
     * @private
     * @return {Object}
     */
    getRulesConfig()
    {
        if (this.platform.isTablet() && this.assets.hasAsset(TABLET_JSON_CONFIG_ID))
        {
            return this.assets.getAsset(TABLET_JSON_CONFIG_ID);
        }
        else
        if (this.platform.isMobileOrTablet() && this.assets.hasAsset(MOBILE_JSON_CONFIG_ID))
        {
            return this.assets.getAsset(MOBILE_JSON_CONFIG_ID);
        }
        else
        if (this.assets.hasAsset(DEFAULT_JSON_CONFIG_ID))
        {
            return this.assets.getAsset(DEFAULT_JSON_CONFIG_ID);
        }
        else
        {
            // TODO: Load up some kind of default config file ?
            return {};
        }
    }
};