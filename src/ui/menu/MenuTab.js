const REL_TOGGLE_PAD_X = 5;
const REL_TEXT_HEIGHT = 5;
const REL_CORNER_SIZE = 1;
const REL_BUTTON_CONTENT_PAD = 2;
const REL_TOGGLE_BG_WIDTH = 50;

import gsap from "gsap/gsap-core";
import BaseView from "../../views/BaseView";
import C_PointerEvt from "../../const/C_PointerEvt";

/**
 * A base class for Menu Tabs, which provides some additional utility methods for creating
 * content.
 */
export default class MenuTab extends BaseView
{
    /**
     * Constructs a new MenuTab view instance. 
     * @param {number} tabWidth
     * The width of content that the tab should be drawn in to.
     * @param {number} tabHeight 
     * The height of content that the tab should be drawn in to. If required, the Menu Tab is
     * free to allow its content to exceed this height (as the tab view will be placed inside
     * some kind of scrolling component). However, in cases where the content is expected to
     * fit within a single page (no scroll required), this tab height can be usefull in order
     * to layout content to look nice in the available area (ie: to vertically center a small
     * amount of content within the allowed area, or to divide content up into vertical thirds)
     */
    constructor(tabWidth, tabHeight) {
        super();

        /**
         * @private
         * @type {number}
         */
        this._padX = this.layout.relWidth(REL_TOGGLE_PAD_X);

        /**
         * @private
         * @type {number}
         */
        this._textHeight = this.layout.relHeight(REL_TEXT_HEIGHT);
        
        /**
         * @private
         * @type {number}
         */
        this._buttonBgCornerPad = this.layout.relWidth(REL_CORNER_SIZE);

        /**
         * @private
         * @type {number}
         */
        this._buttonContentPad = this.layout.relWidth(REL_BUTTON_CONTENT_PAD);

        /**
         * @private
         * @type {number}
         */
        this._buttonHeight = this._textHeight + (2 * this._buttonContentPad);

        /**
         * @private
         * @type {number}
         */
        this._toggleStartX = tabWidth * 0.5;

        /**
         * @private
         * @type {number}
         */
        this._toggleSettingWidth = tabWidth * 0.5;

        /**
         * @private
         * @type {number}
         */
        this._toggleSettingHeight = this._buttonHeight;

        /**
         * Scale factor, which can reduce the resolution that all text is rendered at. This is
         * usefull on mobile, where we often have small but high resolution screens: we can get
         * away with rendering our canvas text at much lower resolution, then up-scaling it to
         * get back to the target size.
         * @private
         * @type {number}
         */
        this._textDownScaleFactor = this.platform.isDesktop()? 1 : 0.25;

        /**
         * This is the amount by which we must up-scale created text items, in order to return
         * them to the correct on-screen size (but with lower-resolution text)
         * @private
         * @type {number}
         */
        this._textUpScaleFactor = 1  / this._textDownScaleFactor;
    };


    /**
     * @public
     * @readonly
     * @return {number}
     */
    get textDownScaleFactor() {
        return this._textDownScaleFactor;
    };


    /**
     * @public
     * @readonly
     * @return {number}
     */
    get textUpScaleFactor() {
        return this._textUpScaleFactor;
    };


    /**
     * Creates a button with text on it, in the standard menu style. The button is size dynamically,
     * based on the parameters supplied. The button height is pre-set size (which is appropriate for
     * any button in the menu), and if possible, the text is a standard size too.
     * 
     * If no values are supplied to the optional parameters targetWidth and useExactWidth, then the
     * button width is determined by the natural size of the text, with some extra horizontal padding
     * added.
     * 
     * If a value is supplied to the targetWidth parameter, then button will never be wider than the
     * value of targetWidth: if useExactWidth is also set to true, then the button will always be
     * exactly the width specified. If useExactWidth is false (or not passed), then the button can
     * be any size up to targetWidth (its actual width will be determined by the sizing of the text
     * placed on the button)
     * 
     * @protected
     * @param {string} textLocalizationId
     * The text string to display on the button.
     * @param {number} [targetWidth]
     * An optional target width for the button (in pixels).
     * @param {boolean} [useExactWidth]
     * Indicates if any value passed to parameter targetWidth should be treated as EXACTLY the width
     * of the button. If this parameter is not specified (or set to false), then targetWidth is treated
     * as a maximum width instead.
     * @return {PIXI.DisplayObject}
     */
    createTextButton(textLocalizationId, targetWidth=0, useExactWidth=false)
    {
        let buttonContainer = new PIXI.Container();

        let textString = this.locale.getString(textLocalizationId);
        let textField = new PIXI.Text(textString, {
            fontFamily : "Arial",
            fontSize : this._textHeight,
            fill : this.config.gameMenuStyle.btnTextColour,
            align : 'center'
        });

        
        let buttonWidth;

        if (targetWidth) {
            let maxTextWidth = targetWidth - (2 * this._buttonContentPad);
            if (maxTextWidth < textField.width) {
                this.layout.setWidth(textField, maxTextWidth);
            }
        }

        if (targetWidth && useExactWidth) {
            buttonWidth = targetWidth;
        }
        else
        {
            buttonWidth = textField.width + (2 * this._buttonContentPad);
        }

        let buttonBgNormal = new PIXI.Graphics();
        buttonBgNormal.beginFill(this.config.gameMenuStyle.btnColour);
        buttonBgNormal.drawRoundedRect(0, 0, buttonWidth, this._buttonHeight, this._buttonContentPad);
        buttonBgNormal.endFill();

        let buttonBgHighlight = new PIXI.Graphics();
        buttonBgHighlight.beginFill(this.config.gameMenuStyle.btnColourHighlight);
        buttonBgHighlight.drawRoundedRect(0, 0, buttonWidth, this._buttonHeight, this._buttonContentPad);
        buttonBgHighlight.endFill();
        buttonBgHighlight.visible = false;

        textField.anchor.set(0.5);
        textField.x = buttonBgNormal.width * 0.5;
        textField.y = buttonBgNormal.height * 0.5;

        let showNormal = () => {
            buttonBgHighlight.visible = false;
        };

        let showHighlight = () => {
            buttonBgHighlight.visible = true;
        }

        buttonContainer.addChild(buttonBgNormal);
        buttonContainer.addChild(buttonBgHighlight);
        buttonContainer.addChild(textField);
        buttonContainer.interactive = true;
        buttonContainer.on(C_PointerEvt.DOWN, showHighlight);
        buttonContainer.on(C_PointerEvt.OVER, showHighlight);
        buttonContainer.on(C_PointerEvt.UP, showNormal);
        buttonContainer.on(C_PointerEvt.ROLLOUT, showNormal);
        
        return buttonContainer;
    };

    
    /**
     * Creates an on / off toggle setting. This component is assumed to occupy the full width of the
     * Tab view (or rather, to occupt a single vertical row in the tab view with no additional content
     * either side of it: in practise, it will likely not be the full width of the Tab)
     * @protected
     * @param {string} settingLocalizationId
     * The localization id of the text field that sits to the left of the setting.
     * @param {boolean} initialState
     * The initial state of the toggle setting (eg: whether the underlying setting is currently enabled).
     * @param {{(enabled:boolean) : void}} [onSettingToggled]
     * Callback executed whenever the setting is toggled. The new enabled state of the setting will be
     * passed to the callback.
     * @return {ToggleControl}
     */
    createOnOffToggleSetting(settingLocalizationId, initialState, onSettingToggled)
    {
        let tween = null;

        let settingIsEnabled = initialState;
        let settingContainer = new PIXI.Container();

        // Darker BG that is a background to the toggle
        let toggleBgStartX = this._toggleStartX;
        let toggleBgWidth = this._toggleSettingWidth;
        let toggleBgHeight = this._toggleSettingHeight;
        let toggleBgCornerRadius = this._buttonContentPad;
        
        let toggleBg = new PIXI.Graphics();
        toggleBg.beginFill(this.config.gameMenuStyle.toggleBtnBgColour);
        toggleBg.drawRoundedRect(toggleBgStartX, 0, toggleBgWidth, toggleBgHeight, toggleBgCornerRadius);
        toggleBg.endFill();
        toggleBg.interactive = true;

        let cornerRadiusDiff = 0.3;
        let buttonCornerRadius = toggleBgCornerRadius * (1 - cornerRadiusDiff);
        let sizeDiff = cornerRadiusDiff * toggleBgCornerRadius;
        let buttonWidth = (toggleBgWidth * 0.5) - sizeDiff;
        let buttonHeight = toggleBgHeight - (2 * sizeDiff);
        let buttonPosY = sizeDiff;
        
        let buttonEnabledPosX = toggleBgStartX + (toggleBgWidth * 0.50);
        let buttonDisabledPosX = toggleBgStartX + sizeDiff;

        let buttonBg = new PIXI.Graphics();
        buttonBg.beginFill(this.config.gameMenuStyle.btnColour);
        buttonBg.drawRoundedRect(0, buttonPosY, buttonWidth, buttonHeight, buttonCornerRadius);
        buttonBg.endFill();
        buttonBg.x = settingIsEnabled? buttonEnabledPosX : buttonDisabledPosX;

        let textStyle = {
            fontFamily : "Arial",
            fontSize : this._textHeight,
            fill : this.config.gameMenuStyle.toggleBtnTextColour,
            align : 'center'
        };

        let textDisabled = new PIXI.Text(this.locale.getString("T_menu_SettingDisabled"), textStyle);
        textDisabled.anchor.set(0.5);
        textDisabled.x = toggleBgStartX + (toggleBgWidth * 0.75);
        textDisabled.y = toggleBgHeight * 0.5;

        let textEnabled = new PIXI.Text(this.locale.getString("T_menu_SettingEnabled"), textStyle);
        textEnabled.anchor.set(0.5);
        textEnabled.x = toggleBgStartX + (toggleBgWidth * 0.25);
        textEnabled.y = toggleBgHeight * 0.5;

        let settingTitleText = new PIXI.Text(this.locale.getString(settingLocalizationId), {
            fontFamily : "Arial",
            fontSize : this._textHeight,
            fill : this.config.gameMenuStyle.textColour,
            align : 'right'
        });

        settingTitleText.anchor.set(1, 0.5);
        settingTitleText.x = toggleBgStartX - this._padX;
        settingTitleText.y = toggleBgHeight * 0.5;

        let toggleState = () =>
        {
            settingIsEnabled = !settingIsEnabled;
            tweenState();
        }

        // Tween state of toggle button - at the end, we execute any state change flag.
        // There might (probably is) a global event fired in our callback method, which
        // might also try and explicitly set the state of the toggle.
        let tweenState = () =>
        {
            if (tween) {
                tween.kill();
                tween = null;
            }

            let targetTime = 0.2;
            let currentX = buttonBg.x;
            let targetX = settingIsEnabled? buttonEnabledPosX : buttonDisabledPosX;
            let maxMoveX = buttonEnabledPosX - buttonDisabledPosX;
            let actualTime = Math.abs(targetTime * ((targetX - currentX) / maxMoveX));

            tween = gsap.fromTo(
                buttonBg,
                { x:currentX },
                { x:targetX, duration:actualTime, onComplete:() => {
                    if (onSettingToggled) {
                        //this.log.debug(`ToggleControl.setting is now ${settingIsEnabled}`);
                        onSettingToggled(settingIsEnabled);
                    }
                }
            });
        }

        toggleBg.on(C_PointerEvt.DOWN, toggleState);

        settingContainer.addChild(toggleBg);
        settingContainer.addChild(textDisabled);
        settingContainer.addChild(textEnabled);
        settingContainer.addChild(buttonBg);
        settingContainer.addChild(settingTitleText);

        // This allows us to instantly set the control to enabled / disabled externally.
        settingContainer.setToggleControlState = newState => {
            //this.log.debug(`ToggleControl.setVisualStateTo(${newState?"enabled":"disabled"})`);
            
            if (tween) {
                tween.kill();
                tween = null;
            }

            settingIsEnabled = newState;
            buttonBg.x = newState? buttonEnabledPosX : buttonDisabledPosX;
        }

        return settingContainer;
    };
}