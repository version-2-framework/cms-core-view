import BaseUI from "./BaseUI";
import C_PointerEvt from "../const/C_PointerEvt";
import C_UIEvent from "../const/C_UIEvent";

/**
 * Checkbox UI element
 */
class TextInput extends BaseUI
{


    constructor(startText, w, h, required)
    {
        super();

        this.required = required;

        this.inputBg = new PIXI.Graphics();
        this.inputBg.lineStyle(this.layout.relHeight(.2), 0xbbbbbb, 1);
        this.inputBg.beginFill(0xffffff, 1);
        this.inputBg.drawRect(0, 0, w, h);
        this.inputBg.endFill();
        this.inputBg.interactive = true;
        this.inputBg.on(C_PointerEvt.UP, () => {
                this.app.keyboardView.open(this);
        }.bind(this), this);

        this.valueTF = this.assets.getTF(startText, h*.8, null, "#606060");
        this.valueTF.anchor.set(.5,.5);
        this.valueTF.x = (this.inputBg.width) * .5;
        this.valueTF.y = (this.inputBg.height) * .5;

        this.addChild(this.inputBg);
        this.addChild(this.valueTF);

        this.update(startText);
    }


    getCurrVal()
    {
        return parseFloat(this.valueTF.text);
    }


    update(value)
    {
        value = isNaN(value) ? this.app.model.formatCurrency(0, true, false) : value;
        if (this.required && parseFloat(value) === 0)
        {
            this.valueTF.text = this.app.locale.getString("menu_autoplay_required");
        }
        else
        {
            this.valueTF.text = value;
        }
        this.emit(C_UIEvent.UPDATE);
    }
}


export default TextInput;