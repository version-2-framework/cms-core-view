import Utils from "../utils/Utils";

class TextField extends PIXI.Text
{


    constructor(text, style, canvas)
    {
        super(text, style, canvas);
        this.groups = {
            size: null,
            verticalLayout: null,
            horizontalLayout: null,
            anchor: null
        };
        this._skipLimitSize = true;
        Utils.addedToStage(this, this.resize, [false], this);
    }


    get text()
    {
        return this._text;
    }


    set text(text)
    {
        text = String(text === '' || text === null || text === undefined ? ' ' : text);
        if (this._text === text)
        {
            return;
        }
        this._text = text;
        this.dirty = true;
        this.resize();
    }


    skipLimitSize()
    {
        return this._skipLimitSize;
    }


    get sizeGroup()
    {
        return this.groups.size.name;
    }
    set sizeGroup(group)
    {
        this.setSizeGroup(group);
    }


    setSizeGroup(groupName)
    {
        this.removeSizeGroup();
        if (groupName)
        {
            if (!TextField.groups.size[groupName])
            {
                TextField.groups.size[groupName] = [this];
            }
            else if (TextField.groups.size[groupName].indexOf(this) === -1)
            {
                TextField.groups.size[groupName].push(this);
            }
            this.groups.size = {name: groupName, fontSize: this.style.fontSize};
        }
    }


    static updateSizeGroup(groupName)
    {
        let fields = TextField.groups.size[groupName];
        let fontSize, scale, min;
        fields.forEach(function(field)
        {
            let tempScale = Math.min(field.fieldHeightScale(), field.fieldWidthScale());
            if (!min || field.style.fontSize * tempScale < min)
            {
                min = field.style.fontSize * scale;
                scale = tempScale;
                fontSize = field.style.fontSize;
            }
        });
        fields.forEach(function(field)
        {
            field.style.fontSize = fontSize;
            field.scale.set(scale / field.overallScale());
        });
    }


    removeSizeGroup()
    {
        if (this.groups.size)
        {
            let group = TextField.groups.size[this.groups.size.name];
            let idx = group.indexOf(this);
            group.splice(idx, 1);
            this.style.fontSize = this.groups.size.fontSize;
            if (group.length === 0)
            {
                delete TextField.groups.size[this.groups.size.name];
            }
            this.groups.size = null;
        }
    }


    /**
     *
     * @param groupName
     * @param x
     * @param width
     * @param pad
     */
    setVerticalLayoutGroup(groupName, x, width, pad)
    {
        this.removeVerticalLayoutGroup();
        if (groupName)
        {
            if (!TextField.groups.verticalLayout[groupName])
            {
                TextField.groups.verticalLayout[groupName] = {fields: [this]};
            }
            else if (TextField.groups.verticalLayout[groupName].fields.indexOf(this) === -1)
            {
                TextField.groups.verticalLayout[groupName].fields.push(this);
            }

            TextField.groups.verticalLayout[groupName].x = x || TextField.groups.verticalLayout[groupName].x;
            TextField.groups.verticalLayout[groupName].width = width || TextField.groups.verticalLayout[groupName].width;
            TextField.groups.verticalLayout[groupName].pad = pad || TextField.groups.verticalLayout[groupName].pad;

            this.groups.verticalLayout = {name: groupName};
        }
    }


    static updateVerticalLayoutGroup(groupName)
    {
        let relWidth = PIXI.app.layout.relWidth.bind(PIXI.app.layout);

        let group = TextField.groups.verticalLayout[groupName];
        let fields = group.fields;

        let pad = relWidth(group.pad);
        let width = -pad;
        fields.forEach(function (field)
        {
            field.updateText(true);
            width += field._texture.orig.width + pad;
        });

        let x = relWidth(group.x);
        fields.forEach(function (field)
        {
            let scale = Math.min(Math.min(relWidth(group.width) / width, field.fieldHeightScale()), 1);
            let fieldPad = Math.max(relWidth(group.width) - width * scale, 0) / (fields.length - 1);
            field.scale.set(scale / field.overallScale());
            field.x = x + field.width * field.anchor.x;
            x += field.width + pad * scale + fieldPad;
        });
    }


    removeVerticalLayoutGroup()
    {
        if (this.groups.verticalLayout)
        {
            let group = TextField.groups.verticalLayout[this.groups.verticalLayout.name];
            let idx = group.fields.indexOf(this);
            group.fields.splice(idx, 1);
            if (group.fields.length === 0)
            {
                delete TextField.groups.verticalLayout[this.groups.verticalLayout.name];
            }
            this.groups.verticalLayout = null;
        }
    }


    get fieldWidth()
    {
        return this._fieldWidth;
    }


    set fieldWidth(width)
    {
        this._fieldWidth = width;
        this.style.trim = !!this.fieldWidth || !!this.fieldHeight;
        this.resize();
    }


    fieldWidthScale()
    {
        let scale = 1;
        if (this.fieldWidth)
        {
            this.updateText(true);
            scale = Math.min(scale, PIXI.app.layout.relWidth(this.fieldWidth) / this._texture.orig.width);
        }
        return scale;
    }


    get fieldHeight()
    {
        return this._fieldHeight;
    }


    set fieldHeight(height)
    {
        this._fieldHeight = height;
        this.style.trim = !!this.fieldWidth || !!this.fieldHeight;
        this.resize();
    }


    fieldHeightScale()
    {
        let scale = 1;
        if (this.fieldHeight)
        {
            this.updateText(true);
            scale = Math.min(scale, PIXI.app.layout.relHeight(this.fieldHeight) / this._texture.orig.height);
        }
        return scale;
    }


    overallScale()
    {
        let overallScale = 1;
        let parent = this.parent;
        while (parent && parent !== PIXI.app.stage)
        {
            overallScale *= parent.scale.x;
            parent = parent.parent;
        }

        return overallScale;
    }


    resize()
    {
        if (this.groups)
        {
            if (this.groups.size)
            {
                TextField.updateSizeGroup(this.groups.size.name);
            }
            else if (this.groups.verticalLayout)
            {
                TextField.updateVerticalLayoutGroup(this.groups.verticalLayout.name);
            }
            else if (this.fieldWidth || this.fieldHeight)
            {
                this.scale.set(Math.min(this.fieldHeightScale(), this.fieldWidthScale()) / this.overallScale());
            }
        }
        else if (this.fieldWidth || this.fieldHeight)
        {
            this.scale.set(Math.min(this.fieldHeightScale(), this.fieldWidthScale()) / this.overallScale());
        }
    }


    updateTexture()
    {
        const canvas = this.canvas;
        if (this._style.trim)
        {
            const trimmed = this.trimCanvas(canvas);
            canvas.width = trimmed.width;
            canvas.height = trimmed.height;
            this.context.putImageData(trimmed.data, 0, 0);
        }
        const texture = this._texture;
        const style = this._style;
        const padding = style.trim ? 0 : style.padding;
        const baseTexture = texture.baseTexture;
        baseTexture.hasLoaded = true;
        baseTexture.resolution = this.resolution;
        baseTexture.realWidth = canvas.width;
        baseTexture.realHeight = canvas.height;
        baseTexture.width = canvas.width / this.resolution;
        baseTexture.height = canvas.height / this.resolution;
        texture.trim.width = texture._frame.width = canvas.width / this.resolution;
        texture.trim.height = texture._frame.height = canvas.height / this.resolution;
        texture.trim.x = -padding;
        texture.trim.y = -padding;
        texture.orig.width = texture._frame.width - (padding * 2);
        texture.orig.height = texture._frame.height - (padding * 2);
        // call sprite onTextureUpdate to update scale if _width or _height were set
        this._onTextureUpdate();
        baseTexture.emit('update', baseTexture);
        this.dirty = false;
    }


    trimCanvas(canvas)
    {
        let width = canvas.width;
        let height = canvas.height;
        const context = canvas.getContext('2d');
        const imageData = context.getImageData(0, 0, width, height);
        const pixels = imageData.data;
        const len = pixels.length;
        const bound = {
            top: null,
            left: null,
            right: null,
            bottom: null,
        };
        let i;
        let x;
        let y;
        for (i = 0; i < len; i += 4)
        {
            if (pixels[i + 3] !== 0)
            {
                x = (i / 4) % width;
                y = ~~((i / 4) / width);
                if (bound.top === null)
                {
                    bound.top = y;
                }
                // if (bound.left === null)
                // {
                //     bound.left = x;
                // }
                // else if (x < bound.left)
                // {
                //     bound.left = x;
                // }
                // if (bound.right === null)
                // {
                //     bound.right = x + 1;
                // }
                // else if (bound.right < x)
                // {
                //     bound.right = x + 1;
                // }
                if (bound.bottom === null)
                {
                    bound.bottom = y;
                }
                else if (bound.bottom < y)
                {
                    bound.bottom = y;
                }
            }
        }

        let pad = 0;
        if (this.style.strokeThickness)
        {
            pad += this.style.strokeThickness;
        }
        bound.left -= pad;
        bound.top -= pad;
        bound.right += pad;
        bound.bottom += pad;

        // width = bound.right - bound.left;
        // width = width > 0 ? width : 1;
        height = bound.bottom - bound.top + 1;

        const data = context.getImageData(0, bound.top, width, height);
        // const data = context.getImageData(bound.left, bound.top, width, height);

        return {
            height,
            width,
            data,
        };
    }


}

TextField.groups = {size: {}, verticalLayout: {}, horizontalLayout: {}, anchor: {}};
TextField.fontMeasure = {};

export default TextField