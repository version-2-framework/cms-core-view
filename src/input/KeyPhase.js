/**
 * @type {KeyPhase}
 */
export const PRESSED = "PRESSED";

/**
 * @type {KeyPhase}
 */
export const RELEASED = "RELEASED";