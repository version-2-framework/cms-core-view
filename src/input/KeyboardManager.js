
// How would I choose to write this ?
// 1) We need to know when a key was pressed. I would like to be able to trigger actions
//    for that key, maybe even based on its transitional state

import * as KeyPhase from './KeyPhase';
import { getGameViewLogger } from '../../../cms-core-logic/src/logging/LogManager';

const log = getGameViewLogger();

/**
 * Utility class to manage keyboard input. By default, keyboard input starts as disabled,
 * and must be explicitly enabled with a call to "setEnabled".
 */
export class KeyboardManager extends PIXI.utils.EventEmitter
{
    constructor()
    {
        super();

        /**
         * A map of keys currently down: they key is the numerical keycode, the value is a boolean.
         * @private
         * @type {{ [id:number]:boolean }}
         */
        this._keysDown = {};

        /**
         * Indicates whether keyboard input is currently enabled.
         * @private
         * @type {boolean}
         */
        this._isEnabled = false;
    };


    /**
     * Toggles the enabled state of keyboard input. When keyboard input is disabled, all key
     * actions that are currently bound will be ignored, and vice-versa.
     * @public
     * @param {boolean} shouldBeEnabled
     * Sets the required new state of keyboard input (true for enabled, false for disabled).
     */
    setEnabled(shouldBeEnabled)
    {
        if (this._isEnabled !== shouldBeEnabled)
        {
            this._isEnabled = shouldBeEnabled;

            if (shouldBeEnabled) {
                this.enableInput();
            }
            else this.disableInput();
        }
    };


    /**
     * Enables the event listeners which allow keyboard input to work.
     * @private
     */
    enableInput()
    {
        log.info('KeyboardManager.enableInput()');

        window.addEventListener('keydown', this.onKeyDown.bind(this), true);
        window.addEventListener('keyup', this.onKeyUp.bind(this), true);
    };


    /**
     * Disables the event listeners which allow keyboard input to work.
     * @private
     */
    disableInput()
    {
        log.info('KeyboardManager.disableInput()');

        window.removeEventListener('keydown', this.onKeyDown, true);
        window.removeEventListener('keyup', this.onKeyUp, true);
    };


    /**
     * Action invoked when a key is first pressed.
     * @private
     * @param {} keyDownEvent
     */
    onKeyDown(keyDownEvent)
    {
        let keyCode = keyDownEvent.which || keyDownEvent.keyCode;

        /*
        if (this._preventDefaultKeys[keyCode]) {
            keyDownEvent.preventDefault();
        }
        */
        
        if (this.isKeyDown(keyCode) === false)
        {
            log.debug(`KeyboardManager.onKeyDown(keyCode:${keyCode})`);

            this._keysDown[keyCode] = true;

            // This is the very specific event
            this.emit(this.getEventId(keyCode, KeyPhase.PRESSED));
        }
    };


    /**
     * Action invoked when a key is first released.
     * @private
     * @param {} keyUpEvent
     */
    onKeyUp(keyUpEvent)
    {
        let keyCode = keyUpEvent.which || keyUpEvent.keyCode;

        if (this.isKeyDown(keyCode))
        {
            log.debug(`KeyboardManager.onKeyUp(keyCode:${keyCode})`);

            this._keysDown[keyCode] = false;

            // This is the very specific event
            this.emit(this.getEventId(keyCode, KeyPhase.RELEASED));
        }
    };


    /**
     * Adds a key callback.
     * @public
     * @param {KeyCode} keyCode 
     * @param {KeyPhase} keyPhase 
     * @param {function} method 
     * @param {*} [methodContext]
     */
    addKeyAction(keyCode, keyPhase, method, methodContext)
    {
        this.on(this.getEventId(keyCode, keyPhase), method, methodContext);
    };


    /**
     * Removes a key callback.
     * @public
     * @param {KeyCode} keyCode 
     * @param {KeyPhase} keyPhase 
     * @param {Function} method 
     * @param {Object} [methodContext]
     */
    removeKeyAction(keyCode, keyPhase, method, methodContext)
    {
        this.off(this.getEventId(keyCode, keyPhase), method, methodContext);
    };


    /**
     * Adds a key callback, bound to the "pressed" phase of keyboard press.
     * @public
     * @param {KeyCode} keyCode 
     * @param {Function} method 
     * @param {Object} methodContext 
     */
    addKeyActionOnPressed(keyCode, method, methodContext)
    {
        this.addKeyAction(keyCode, KeyPhase.PRESSED, method, methodContext);
    };


    /**
     * Removes a key callback, from the "pressed" phase of keyboard press.
     * @public
     * @param {KeyCode} keyCode 
     * @param {Function} method 
     * @param {Object} methodContext 
     */
    removeKeyActionOnPressed(keyCode, method, methodContext)
    {
        this.removeKeyAction(keyCode, KeyPhase.PRESSED, method, methodContext);
    };


    /**
     * Adds a key callback, bound to the "released" phase of keyboard press.
     * @public
     * @param {KeyCode} keyCode 
     * @param {Function} method 
     * @param {Object} methodContext 
     */
    addKeyActionOnReleased(keyCode, method, methodContext)
    {
        this.addKeyAction(keyCode, KeyPhase.RELEASED, method, methodContext);
    };


    /**
     * Removes a key callback, from the "released" phase of keyboard press.
     * @public
     * @param {KeyCode} keyCode 
     * @param {Function} method 
     * @param {Object} methodContext 
     */
    removeKeyActionOnReleased(keyCode, method, methodContext)
    {
        this.removeKeyAction(keyCode, KeyPhase.RELEASED, method, methodContext);
    };
    

    /**
     * @private
     * @param {KeyCode} keyCode 
     * @param {KeyPhase} keyPhase 
     * @return {string}
     */
    getEventId(keyCode, keyPhase)
    {
        return `${keyCode}_${keyPhase}`;
    };


    /**
     * @public
     * @param {number} keyCode
     * @return {Key}
     */
    /*
    getKey(keyCode)
    {
        let key = this._keys[keyCode];

        if (!key)
        {
            key = new Key();
            this._keys[keyCode] = key;
        }
        
        return key;
    };
    */


    /**
     * Checks if a given key is down.
     * @public
     * @param {KeyCode} keyCode
     * The numerical KeyCode of the key that you want to check.
     * @return {boolean}
     * True if the key is down, false if it is not.
     */
    isKeyDown(keyCode) {
        return this._keysDown[keyCode] === true;
    };
}