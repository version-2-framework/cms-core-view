type KeyPhase = "PRESSED" | "RELEASED";

type KeyCode = number;