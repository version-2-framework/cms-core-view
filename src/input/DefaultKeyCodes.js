import * as KeyCode from './KeyCode';

//--------------------------------------------------------------------------------------------------
// Default keyboard shortcuts for key game actions.
//--------------------------------------------------------------------------------------------------
export const EXECUTE_SPIN_ACTION = KeyCode.SPACE;

export const TOGGLE_GAME_MENU = KeyCode.R;

export const TOGGLE_AUTOPLAY_MENU = KeyCode.A;

export const TOGGLE_BET_MENU = KeyCode.B;

export const TOGGLE_SOUND = KeyCode.M;

export const SHARE_ON_FACEBOOK = KeyCode.F;

export const SHARE_ON_TWITTER = KeyCode.T;

export const CLOSE_GAME_SESSION = KeyCode.Q;

export const ADD_CREDIT = KeyCode.C;

export const EXECUTE_BONUS_SELECTION = KeyCode.SPACE;