# Default Assets
Here is a description of what each set of default assets (in a sub-folder) is for, and when they should be included or excluded in a new game client.

Although some assets will always be required in all game clients, the folders are deliberately broken apart into discrete, logical chunks. This should make it easier to organize sprite-sheets as required for a game client: assets which belong together, are grouped together in a folder, but mandatory assets that do not normally get shown together will be in separate folders (allowing us to distribute them across different spritesheets in a game client, optimizing spritesheet size).

## common_gui_assets
These are assets expected to be included in every game client. They include standard buttons featured in the desktop or mobile guis, as well as any gui graphic which may appear in all contexts. For example, there is a utility graphic - "popup_bg.png" - nothing more than a semi transparent black square, which can be used as the background to any sub-menu that is an overlay.

This folder is mandatory for all game clients.

## default_mobile_game_buttons
The default Bet / Autoplay / Spin buttons for the mobile game gui. Although its possible to use customized game buttons on the mobile gui, in general we should be using the default assets. However, it may make sense to use colour customized versions of the default assets for certain game clients.

So, strictly speaking this folder is not mandatory (but will probably included in the majority of game clients)

## default_spin_indicators
A set of Spin 1 / Spin 2 / FreeSpin indicators, that can be used when required. In most cases, we will want to offer a unique visual style for these indicators for a new game client.

Therefore, this folder is not mandatory in any game client (game client is free to provide a custom rendering as required)

## menu_bet_auto
Assets used in the Bet Settings and Autoplay Settings sub-menus.

Mandatory for all game clients.

## menu_game
Common assets used in the game menu.

Mandatory for all game clients.

## wmg_interface_assets
Assets used for the WMG Cashier, Session Stats view, and utility menus on internal builds (eg: the standard url selection screen)

Mandatory for all game clients.