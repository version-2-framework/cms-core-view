

Code style guidelines :

functionName
{
	if (condition)
	{
	
	}
	else
	{
	
	}
}


Use curly braces, even for single line loops and conditionals


Names:

variableName (Camel case)
CONSTANT_NAME



Constants :

Round constants which are too long :
0.8620689655172414 -> 0.8621

Calculate values whenever their form is more meaningful (for example when you have coordinates in percent as in this case):
47015 / 720 -> 65.3 


Do not make lines too long, for example, break this in 2 lines with the second line indented

this.title = this.assets.getTF(this.locale.getString(this.jsonTitlesPrefix + (this.currentWinShowing + 1)), this.layout.relHeight(15265 / 720), this.config.bigWinFont, "#FFBF20");

this.title = this.assets.getTF(this.locale.getString(this.jsonTitlesPrefix 
	+ (this.currentWinShowing + 1)), this.layout.relHeight(15265 / 720), this.config.bigWinFont, "#FFBF20");



Leave two lines between methods:

method1()
{
    ...
}


method2()
{
    ...
}



Use more descriptive loop variable names 

for example , instead of :

for (let i=0; i<...; i++)
{
}

for (let reelIdx=0; reelIdx<...; reelIdx++)
{
}







