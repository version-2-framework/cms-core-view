// TODO: 25.02.2022
// What on earth is this script doing? I've never actually looked at it before.
// Why is the "isIOS" check commented out ? Suggests that this is intended for
// iOS cases..
// OK, so i comment this script out, and
// - the blackscreen issue goes away
// - however, swipe for fullscreen now doesnt work very well
// NO, it does still happen, but it happens differently..

const SWIPE_HEIGHT = '45%';

(function () {
    var onResize = function () {
        setTimeout(function () {
            bodyEl.scrollTop = 0;
            var isSwipeElVisible = bodyEl.offsetHeight > window.innerHeight ||
                bodyEl.offsetWidth / bodyEl.offsetHeight > 19 / 9;
            swipeElement.style.display = 'none';// = isIOS && isSwipeElVisible ? 'block' : 'none';
        }, 200)
    };

    var isIOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);

    var htmlEl = document.getElementsByTagName('html')[0];
    var bodyEl = document.getElementsByTagName('body')[0];
    var swipeElement = createSwipeDomElement();
    onResize();

    window.addEventListener('resize', onResize);


    function createSwipeDomElement() {
        var swipeDomEl = document.createElement('div');
        swipeDomEl.id = 'swipe-container';
        swipeDomEl.innerHTML = '<svg version="1.1" id="swipe-svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"\n' +
            '\t viewBox="0 0 500 500" style="enable-background:new 0 0 500 500;" xml:space="preserve">\n' +
            '<style type="text/css">\n' +
            '  #swipe-container {\n' +
            '    position: absolute;\n' +
            '    display: none;\n' +
            '    width: 100vw;\n' +
            '    height: 100%;\n' +
            '    left: 0;\n' +
            '    top: 0;\n' +
            '    right: 0;\n' +
            '    bottom: 0;\n' +
            '    background: rgba(0, 0, 0, 0.71);\n' +
            '    z-index: 9999;\n' +
            '  }\n' +
            '  #swipe-svg{\n' +
            '    position:absolute;\n' +
            '    height: ' + SWIPE_HEIGHT + ';\n' +
            '    top:50%;\n' +
            '    left:50%;\n' +
            '    transform: translate(-50%,-50%);\n' +
            '  }\n' +
            '  .st0{\n' +
            '    fill:#FFFFFF;\n' +
            '  }\n' +
            '</style>\n' +
            '<path id="XMLID_2_" class="st0" d="M469.3,416.2c-0.1-0.6-0.1-1.1,0-1.7c0.7-8.6,8.7-78.7-4-102.9c-13.7-26.1-31.5-25-39.7-22.4\n' +
            '\tc-0.2,0.1-0.7-0.6-1-0.5c-0.1,0-0.1,0-0.2,0.1c-2.4,0.8-4.7,1-6.7-0.6c-10.3-8.5-20.3-7.9-28.2-5.3c-4,1.3-7.4,3.4-9.9,5.2\n' +
            '\tc-0.6,0.4-1.2,0.8-1.8,1c-2.4,0.8-4.9,0.3-6.9-1.2c-8.7-7.1-17.6-7.8-25.3-5.8v-94.9c0-17.8-14.4-32.3-32.3-32.3H208.8l4.2-47.3\n' +
            '\tl5.7,5.8c7.1,8.6,19.7,12.1,28.1,2.6c4.8-5.4,5.3-13.3,1.7-19.6l-44.8-56.9c-6.4-8.8-16.2-8.1-22.5,0l-44.9,56.7\n' +
            '\tc-4.7,8.3-1.8,18.7,6.5,23.4c8.3,4.7,17.2,0.9,23.4-6.5l5.7-5.6l4.2,47.3H71.5c-17.8,0-32.3,14.4-32.3,32.3V439h24.2V264.2H186\n' +
            '\tl6.6,73.4l6.5-73.4H230l55.4,76.7c2.3,3.2,1.6,7.8-1.6,10.1c-6.4,4.6-14.6,15-9.4,37.1c8.7,37.4,58.2,65.2,66.4,69.6\n' +
            '\tc0.8,0.4,1.5,0.9,2,1.6l26.3,30c1.9,2.2,5,3,7.8,2.1l94.8-31.2c3.4-1.1,5.4-4.5,4.9-8L469.3,416.2z M63.5,258.4v-55.1h117l4.9,55.1\n' +
            '\tH63.5z M199.6,258.4l4.9-55.1h117v55.1h-41.4L255,223.6c-6.7-9.2-16.1-11-24.2-8.4c-12.7,4.2-22.1,19.4-11.6,34l6.6,9.1H199.6z\n' +
            '\t M311.6,301.9l-27.2-37.7h37.2v39.4c-0.5,0.4-1,0.8-1.7,1C317,305.5,313.6,304.7,311.6,301.9z"/>\n' +
            '</svg>\n';

        bodyEl.appendChild(swipeDomEl);
        return swipeDomEl;
    }
})();
