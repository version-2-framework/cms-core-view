/**
 * Returns the most appropriate endpoint to use for the getGameUrlService.
 * 
 * For "stage 1" ("generic integration"), we simply return a fixed constant
 * value, and ignore the value of operator id.
 * 
 * @param {number} operatorId
 * @return {string}
 * The endpoint to use.
 */
function fetchCorrectGetGameUrlServiceEndpoint(operatorId)
{
    return "https://testv2.wmgaming.it:9090/lms/rest/wmg/external/gamedl";
}