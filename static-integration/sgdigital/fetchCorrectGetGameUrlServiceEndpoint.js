/**
 * Returns the most appropriate endpoint to use for the getGameUrlService.
 * 
 * This is a default implementation for local testing (the file name is the
 * one which the landing page attempts to load). This is idential to "stage
 * 1" ("generic integration") - we get the (current) test url for the service.
 * 
 * @param {number} operatorId
 * @return {string}
 * The endpoint to use.
 */
function fetchCorrectGetGameUrlServiceEndpoint(operatorId)
{
    return "https://testv2.wmgaming.it:9090/lms/rest/wmg/external/gamedl";
}