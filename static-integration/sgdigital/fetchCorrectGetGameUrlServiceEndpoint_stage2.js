/**
 * Returns the most appropriate getGameUrl service endpoint to use, relative to a specific
 * operator id. The basic "licensee" is SGDigital, but they are actually a game provider:
 * they provide our games to various other "operators" (in SGDigital terminology: 
 * "sub-licensee" might be an alternate name that CMS / WMG have used previously). The
 * url (of the WMG Provided service) that returns the full game download url changes,
 * depending on which operatorId we are dealing with. This method maps SGDigital's
 * operatorId to the appropriate service endpoint that we need to call.
 * 
 * For "stage 2" ("licensee testing"), we map operator id to an appropriate endpoint. However,
 * the endpoints are NOT the same as the ones used for production.
 * 
 * @param {number} operatorId
 * @return {string}
 * The endpoint to use, or null if no endpoint is available for the given operator id.
 */
function fetchCorrectGetGameUrlServiceEndpoint(operatorId)
{
    // Key / value map of all operator ids, to the url endpoint for the getGameUrl service.
    // Key, of course, is operator id!! Value is an object, providing us a debug name for the
    // operator, and the getGameUrl service endpoint.
    let operatorIdToEndpointMap =
    {
        // Endpoint for 888
        [819] : 
        {
            operatorName : "888",
            endpoint : "https://testv2.wmgaming.it:9090/lms/rest/wmg/external/gamedl"
        },
        // Endpoint for StarCasino (Betssons)
        [574] :
        {
            operatorName : "StarCasino (Betssons)",
            endpoint : "https://testv2.wmgaming.it:7419/lms/rest/wmg/external/gamedl"
        }
    };

    if (operatorId in operatorIdToEndpointMap)
    {
        let operatorData = operatorIdToEndpointMap[operatorId];

        console.debug(`SGDLandingPage: found data for operatorId ${operatorId}: ${JSON.stringify(operatorData)}`);

        return operatorData.endpoint;
    }
    else
    {
        console.error(`SGDLandingPage: operatorId ${operatorId} does not have a valid mapping`);

        // Method signature indicates that null means "no mapping available"
        return null;
    }
}