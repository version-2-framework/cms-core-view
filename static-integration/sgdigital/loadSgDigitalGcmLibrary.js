/**
 * Loads the GCM library. This implicitly takes in url parameters, in order to pick the appropriate
 * location from which to start the load process. This will create a method at the global scope,
 * called "initGcmInstance" - to which we pass our gameWrapper object (it then returns the gcm
 * instance).
 * @param {()=>void} onGcmAvailable
 * Called when the gcm library is ready, and the "initGcmInstance" method has been appneded to global
 * scope (and is therefore available to use).
 * @param {(errorMsg:string)=>void} onError
 * Callback executed in the event that something goes wrong with the loading process.
 */
//let loadGcmLibrary = (onGcmAvailable, onError) =>
window.wmg = window.wmg || {};
window.wmg.loadGcmLibrary = (onGcmAvailable, onError) =>
{
    /**
     * Dynamically loads a javscript file from a given url. The file will be appended to the html
     * page, making the code available - only once this method successfully completes.
     * @param {string} jsFileUrl
     * @param {()=>void} onLoaded
     */
    let loadJsFileFrom = (jsFileUrl, onLoaded) => {
        console.log(`Sgd.loadJsFileFrom(${jsFileUrl})`);

        let jsFileRef = document.createElement('script');
        jsFileRef.setAttribute('type', 'text/javascript');
        jsFileRef.setAttribute('src', jsFileUrl);
        jsFileRef.onload = onLoaded;

        if (jsFileRef) {
            document.getElementsByTagName("head")[0].appendChild(jsFileRef);
        }
    };

    /**
     * Retrieves the correct url from which to fetch Gcm Config
     * @param {string} envId
     * I "think" this is a string...
     * @param {"dev" | "stage" | "production"} [environment]
     * @return {string}
     */
    let getGcmConfigUrl = (envId, environment) => {
        console.log(`Sgd.getGcmConfigUrl(envId:${envId},environment:${environment})`);

        let envIdMap = {
            eur : {
                dev : "https://ogs-gcm-eu-dev.nyxop.net/gcm/config.js",
                stage : "https://ogs-gcm-eu-stage.nyxop.net/gcm/config.js",
                production : "https://ogs-gcm-eu-prod.nyxop.net/gcm/config.js"
            },
            can : {
                stage : "https://ogs-cdn-stage-ca.nyxop.net/gcm/config.js",
                production : "https://ogs-cdn-ca.nyxop.net/gcm/config.js"
            },
            usnj : {
                stage : "https://ogs-cdn-usnj-stage.nyxop.net/gcm/config.js",
                production : "https://ogs-cdn-usnj.nyxop.net/gcm/config.js"
            },
            pail : {
                stage : "https://ogs-gcm-pail-stage.nyxop.net/gcm/config.js",
                production : "https://ogs-gcm-pail-prod.nyxop.net/gcm/config.js"
            },
            pa : {
                stage : "https://ogs-gcm-pa-stage.nyxop.net/gcm/config.js",
                production : "https://ogs-gcm-pa-prod.nyxop.net/gcm/config.js"
            }
        };

        let domainMap = envIdMap[envId] || envIdMap.eur;

        return domainMap[environment] || domainMap.production;
    };

    let url = new URL(window.location.href);
    let envId = url.searchParams.get("envid");
    let isStaging = url.searchParams.get("stage") === "1";
    let isDev = url.searchParams.get("dev") === "1";
    let env = isStaging? "stage" : (isDev? "dev" : "production");
    let gcmConfigUrl = getGcmConfigUrl(envId, env);

    console.log(`Sgd: gcmConfigUrl = ${gcmConfigUrl}, fetch proper gcm url (envId:${envId},env:${env})`);

    loadJsFileFrom(gcmConfigUrl, () => {
        // "GCMConfig" is a tiny module that we just loaded dynamically. It supplies us with
        // the actual url that we need to load for GCM.
        let gcmLibraryUrl = GCMConfig.getGCMUrl(env);

        console.log(`Sgd: gcmLibraryUrl = ${gcmLibraryUrl}`);

        loadJsFileFrom(gcmLibraryUrl, () => {
            console.log(`Sgd: gcm library is loaded.`);

            // create a gcm instance, start it up. GCM is passed the GameWrapper instance, and at some
            // point it will call "gcmReady", and then "configReady" (passing the OGSParams object).
            // As you can see above - configReady triggers the operation to retrieve our game url. So,
            // calling init on this gcmInstance is the action which triggers everything to spring into
            // life.

            let gameUrl = window.location.href;

            // We append a method to the window.
            window.wmg = window.wmg || {};

            let gcmInstance = new gcm.GcmCore();

            window.wmg.getGcmInstance = () => gcmInstance;
                    
            window.wmg.initGcmInstance = gameWrapper => {
                console.log(`SGD: initGcmInstance(gcmLibraryUrl:${gcmLibraryUrl},gameUrl:${gameUrl})`);
                gcmInstance.init(gameWrapper, gcmLibraryUrl, gameUrl);
            }

            onGcmAvailable();
        });
    });
}