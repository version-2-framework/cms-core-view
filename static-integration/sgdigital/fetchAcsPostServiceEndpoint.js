/**
 * Fetches the endpoint to use for a POST request, to which we send a GET request url, to
 * get the url from which to download the game.
 * @param {number} operatorId 
 */
function fetchAcsPostServiceEndpoint(operatorId)
{
    // This is apparently the TEST environment endpoint
    return "https://downloadv2.wmgaming.it/acs/rest/download/sgd";
}